package com.fliconz.fm.mvc.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.service.DashboardMngService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.MapUtil;

@RestController
@RequestMapping(value =  "/dashboard_mng" )
public class DashboardMngController extends DefaultController{
	@Autowired DashboardMngService service;
	
	@RequestMapping(value =  "/update/dashboard/{itemId}" )
	public void updateSelectDashboard(final HttpServletRequest request, HttpServletResponse response,@PathVariable("itemId") final String ITEM_ID) {
		 
		(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				String[] items = ITEM_ID.split(",");
				param.put("list", items);
				service.deleteByQueryKey("deleteDashboardUser", param);
				List<String> arr = new ArrayList<String>(Arrays.asList(items));
				for(String key : items){
					param.put("key", key);
					if(service.selectCountByQueryKey("chkDashboardUser", param) > 0){
						arr.remove(key);
					}
				}
				items = arr.toArray(new String[]{});
				param.put("list", items);
				return service.insertByQueryKey("insertDashboardUser", param);
			}

		}).run(request);
	}
 
	@RequestMapping(value =  "/insert/dashboard" )
	public Object insertUserDashboard(final HttpServletRequest request, HttpServletResponse response) {
		 
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				try	{
					param.put("ITEM_ID", System.nanoTime());
					service.insertByQueryKey("insertDashboardUserOne", param);
				}
				catch(Exception e) {
					
				}
				return ControllerUtil.getSuccessMap(param);
			}
		}).run(request);
	}
	@RequestMapping(value =  "/update/dashboardSeq/{param}" )
	public void updateDashboardSeq(final HttpServletRequest request, HttpServletResponse response,@PathVariable("param") final String paramStr)  {
		 
		(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map<String, Object> myMap = MapUtil.jsonToMap(paramStr);
				List<Map> list = (List<Map>) myMap.get("param");

				for(Map tmp : list){
					service.updateByQueryKey("updateDashboardSeq", tmp);
				}
				return null;
			}

			 

		}).run(request);


	}

	@Override
	protected DefaultService getService() {
		return service;
	}
}
