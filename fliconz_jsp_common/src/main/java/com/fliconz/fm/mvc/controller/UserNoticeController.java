package com.fliconz.fm.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.service.UserNoticeService;

@RestController
@RequestMapping(value =  "/user_notice" )
public class UserNoticeController extends DefaultController {

	@Autowired UserNoticeService service;
	
	@Override
	protected DefaultService getService() {
		// TODO Auto-generated method stub
		return service;
	}

}
