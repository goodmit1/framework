package com.fliconz.fm.mvc;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fliconz.fm.mvc.util.ControllerUtil;

 

public abstract class ActionRunner {
		
		public Object run(HttpServletRequest request) {
			try {
				Map param = ControllerUtil.getParam(request);
				
				return run(param);
			} catch (Exception e) {
				return ControllerUtil.getErrMap(e);
			}
		}

		abstract protected Object run(Map param) throws Exception;
}
