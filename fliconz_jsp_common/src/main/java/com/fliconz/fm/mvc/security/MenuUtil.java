package com.fliconz.fm.mvc.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fliconz.fm.common.vo.TreeNodeMap;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.usermenu.UserMenuManager;

public class MenuUtil {

	static void addMenuNaviNm(UserMenuManager userMenu, List list, TreeNodeMap menu)
	{
		if(menu.getParentId()==1) return;
		TreeNodeMap parent = userMenu.findMenuById(menu.getParentId());
		list.add(0,parent.getMenuNm(UserVO.getUser().getLocale()));
		addMenuNaviNm(userMenu, list, parent);
	}
	static void addMenuNavi(UserMenuManager userMenu, List list, TreeNodeMap menu)
	{
		if(menu.getParentId()==1) return;
		TreeNodeMap parent = userMenu.findMenuById(menu.getParentId());
		list.add(0,parent.getMenuId());
		addMenuNavi(userMenu, list, parent);
	}
	public static List getSelectedPathMenuId()
	{
		UserMenuManager userMenu = UserVO.getUser().getUserMenuManager();
		TreeNodeMap menu = userMenu.getSelectedMenu();
		if(menu == null)
			return null;
		List navi = new ArrayList();
		
		navi.add(menu.getMenuId());
		addMenuNavi(userMenu, navi, menu);
		return navi;
		
	}
	public static Map getMenuTitleNavi()
	{
		UserMenuManager userMenu = UserVO.getUser().getUserMenuManager();
		TreeNodeMap menu = userMenu.getSelectedMenu();
		Map result = new HashMap();
		if(menu != null)
		{

			result.put("title" , menu.getMenuNm(UserVO.getUser().getLocale()));
			result.put("desc" , menu.getMenuDesc(UserVO.getUser().getLocale()));
			List navi = new ArrayList();
			navi.add(menu.getMenuNm(UserVO.getUser().getLocale()));
			addMenuNaviNm(userMenu, navi, menu);
			result.put("navi" , navi);
		}
		return result;
	}
}
