package com.fliconz.fm.mvc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.mvc.security.SSOAuthFilter;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.UserDetailService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.usermenu.UserMenuService;
import com.fliconz.fw.runtime.util.NumberUtil;

@RestController
@RequestMapping(value =  "/my" )
public class MyController  {
	
	@Autowired UserDetailService userService;
	@Autowired UserMenuService userMenuService;
	@RequestMapping(value =  "/info" )
	public Map info(final HttpServletRequest request, HttpServletResponse response)
	{
		UserVO vo = UserVO.getUser();
		Map result = new HashMap();
		result.put("USER_NAME", vo.getUserName());
		result.put("EMAIL", vo.getEmail());
		result.put("MOBILE_NO", vo.getMobileNo());
		result.put("TEAM_CD", vo.getTeam().getTeamCd());
		result.put("TEAM_NM", vo.getTeam().getTeamNm());
		result.put("LOCALE", vo.getLocale());
		result.put("OFFICE_NO", vo.getOfficeNo());
		return result;
	}
	@RequestMapping(value =  "/save" )
	public Object save(final HttpServletRequest request, HttpServletResponse response)
	{
		try {
			Map param =  ControllerUtil.getParam(request);
			userService.updateMyInfo(param);
			return ControllerUtil.getSuccessMap(null, null);
		} catch (Exception e) {

			e.printStackTrace();
			return ControllerUtil.getErrMap(e);
		}
		
		
	}
	@RequestMapping(value =  "/saveMyMenu" )
	public Object insertMyMenu(final HttpServletRequest request, HttpServletResponse response)
	{
		try {
			Map param =  ControllerUtil.getParam(request);
			userMenuService.addMyMenu(param);
			UserVO.getUser().getUserMenuManager().reloadMyMenu();
			return ControllerUtil.getSuccessMap(null, null);
		} catch (Exception e) {

			e.printStackTrace();
			return ControllerUtil.getErrMap(e);
		}
		
		
	}
	@RequestMapping(value =  "/deleteMyMenu" )
	public Object deleteMyMenu(final HttpServletRequest request, HttpServletResponse response)
	{
		try {
			Map param =  ControllerUtil.getParam(request);
			userMenuService.removeMyMenu(param);
			UserVO.getUser().getUserMenuManager().reloadMyMenu();
			return ControllerUtil.getSuccessMap(null, null);
		} catch (Exception e) {

			e.printStackTrace();
			return ControllerUtil.getErrMap(e);
		}
		
		
	}
	@RequestMapping(value =  "/getSSOURL" )
	public Object getSSOURL(final HttpServletRequest request, HttpServletResponse response)
	{
		try {
			String url = request.getParameter("url");
			 
			int menuId = NumberUtil.getInt(  request.getParameter("menuId"));
			String newUrl = SSOAuthFilter.getSSOUrl4Sender(request, url, menuId );
			Map result = new HashMap();
			result.put("url", newUrl);
			return result;
		} catch (Exception e) {

			e.printStackTrace();
			return ControllerUtil.getErrMap(e);
		}
		
		
	}
}

