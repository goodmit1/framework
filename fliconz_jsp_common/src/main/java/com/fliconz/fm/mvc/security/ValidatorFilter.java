package com.fliconz.fm.mvc.security;

import com.fliconz.fm.common.util.HttpHelper;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ValidatorFilter implements Filter {

    JSONObject jsonSchemas ;
    String[] paths;
    String[] definitionsFields = new String[]{"definitions"};
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String t = filterConfig.getInitParameter("path");
        if(t==null){
            throw new ServletException("init parameter 'path' is empty");
        }
        String[] arr = t.split(",");
        t = filterConfig.getInitParameter("definitionFields");
        if(t != null) {
            definitionsFields= t.split(",");
        }
        setPaths(arr);


    }

    /** 경로 디렉토리 지정 /로 시작, 순서대로 로딩하며, 같은 경로가 있다면 override **/
    public void setPaths(String[] paths) throws ServletException {
        this.paths = paths;
        jsonSchemas = new JSONObject();
        for(String a: paths){
            URL resource = this.getClass().getResource(a.trim());
            if(resource != null){
                File dir = new File (resource.getFile());
                if(dir.exists()){
                    putJsons(jsonSchemas, dir);
                }
                else{
                    throw new ServletException(a.trim() + " not found");
                }
            }
            else{
                throw new ServletException(a.trim() + " not found, base dir=" + this.getClass().getResource("/"));
            }

        }
        validateSchema(jsonSchemas);
    }


    private void validateSchema(JSONObject json) throws ServletException {
        Iterator<String> keys = json.keys();
        while(keys.hasNext()){
            String key = keys.next();
            if(key.equals("#ref")){
                Object  o = getRef(json.getString(key));
                if(o==null){
                    throw new ServletException( json.getString(key) + " Not Found");
                }
            }
            else if(json.get(key) instanceof  JSONObject){
                validateSchema(json.getJSONObject(key));
            }
        }
    }
    /**
     * File의 내용을 로드
     * @param allSchemas
     * @param parent
     * @throws ServletException
     */
    private void putJsons(JSONObject allSchemas, File parent) throws ServletException{

        if(parent.isDirectory()){
            File[] files = parent.listFiles();
            for(File file : files){
                putJsons(allSchemas, file);
            }
        }
        else{
            try {
                JSONTokener tokener = new JSONTokener(new FileReader(parent ));
                JSONObject schema1 = new JSONObject(tokener);
                putAll(schema1, allSchemas);
            }
            catch(Exception e) {
                e.printStackTrace();
                throw new ServletException(parent.getPath() + " parseError " + e.getMessage());
            }
        }
    }
    private   void putAll(JSONObject src, JSONObject dest){

        Iterator it = src.keys();
        while(it.hasNext()){
            String key = (String) it.next();
            for(String putAll1levelField : definitionsFields ) {
                if (!key.equals(putAll1levelField) || !dest.has(putAll1levelField)) {
                    dest.put(key, src.get(key));
                } else {  // definition 필드는 값을 update하지 않고, 합친다.
                    JSONObject dest1 = dest.getJSONObject(putAll1levelField);
                    MapUtil.putAll(src.getJSONObject(putAll1levelField), dest1);
                    dest.put(key, dest1);

                }
            }
        }

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String requestURI = ((HttpServletRequest)request).getRequestURI();
        if(requestURI.endsWith("/reloadValidatorSchema")){
            setPaths(paths);
            return;
        }
        if(jsonSchemas.has(requestURI)){
            StringBuffer errMsg = new StringBuffer();
            chkValidate((HttpServletRequest) request, jsonSchemas.getJSONObject( requestURI), errMsg);
            if(errMsg.length()>0){  //에러가 있다면
                if(HttpHelper.isAjax((HttpServletRequest) request)){
                    ((HttpServletResponse)response).setStatus(400);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");
                    JSONObject error = new JSONObject();
                    error.put("error", errMsg.toString());
                    ((HttpServletResponse)response).getWriter().write(error.toString() );
                    return;

                }
                else {
                    throw new IOException(errMsg.toString());
                }
            }
        }
        if(chain != null) chain.doFilter(request, response);
    }

    protected void chkValidate(HttpServletRequest request, JSONObject schemaJson, StringBuffer errMsg) throws IOException {
        JSONObject properitesJson = schemaJson.has("properties")? schemaJson.getJSONObject("properties"):null;
        if(properitesJson.has("#ref")){
            properitesJson = getRef(properitesJson.getString("#ref"));
        }
        if(schemaJson.has("required")){
            chkRequired(request, properitesJson, schemaJson.getJSONArray("required"), errMsg);

        }
        if(properitesJson != null) {
            Iterator<String> it = properitesJson.keys();
            while (it.hasNext()) {
                String key = it.next();

                if (properitesJson.get(key) instanceof JSONObject) {
                    String value = request.getParameter(key);
                    if(!TextHelper.isEmpty(value)) {
                        JSONObject fieldJson = properitesJson.getJSONObject(key);
                        chkValidate(fieldJson, getTitle(properitesJson, key), value, errMsg);
                    }
                }
            }
        }

    }
    private JSONObject getRef(String ref){
        if(ref.startsWith("#")){
            try {
                String[] arr = ref.substring(1).split("/");
                JSONObject target = jsonSchemas;
                for (String arr1 : arr) {
                    if (arr1.equals("")) {
                        continue;
                    }
                    target = target.getJSONObject(arr1);
                }
                return target;
            }
            catch(JSONException e){
                throw new RuntimeException(ref + "==> ref not found");
            }
        }
        return null;
    }

    /**
     * 필수 체크
     * @param request
     * @param schemaJson
     * @param requireFields
     * @param errMsg
     */
    private void chkRequired(HttpServletRequest request, JSONObject schemaJson, JSONArray requireFields, StringBuffer errMsg)   {
        for(int i=0; i < requireFields.length(); i++){
            String field = requireFields.getString(i);
            chkRequired(schemaJson, getTitle(schemaJson,field), request.getParameter(field), errMsg );
        }
    }
    private String getTitle(JSONObject schemaJson, String field){

        if(schemaJson != null && schemaJson.has(field) && schemaJson.getJSONObject(field).has("title")){
            return schemaJson.getJSONObject(field).getString("title");
        }
        return field;
    }
    private void chkRequired(JSONObject schemaJson, String title, String value,StringBuffer errMsg)   {

        if(  TextHelper.isEmpty(value)){

            errMsg.append(MsgUtil.getMsg("validate.required", new String[]{ title} ) + "\n");
        }
    }
    protected void chkValidate(JSONObject fieldJson ,String title, String value,StringBuffer errMsg)   {
        chkType(fieldJson , title, value, errMsg);
        chkMinMax(fieldJson , title, value, errMsg);
        chkLength(fieldJson , title, value, errMsg);
        chkPattern(fieldJson , title, value, errMsg);
    }
    private void chkPattern(JSONObject fieldJson, String title, String value,StringBuffer errMsg)   {
        if(TextHelper.isEmpty(value)) return;
        if(fieldJson.has("pattern")) {
            if(!value.matches(fieldJson.getString("pattern"))){
                errMsg.append(MsgUtil.getMsg("validate.pattern", new String[]{ title, fieldJson.has("patternDescription") ? fieldJson.getString("patternDescription") : "" } ) + "\n");

            }
        }
    }
    private boolean isNumberType(JSONObject fieldJson){
        try{
            String type = fieldJson.getString("type");
            if(type.equals("number") || type.equals("integer")){
                return true;
            }
            return false;
        }
        catch(Exception e){
            return false;
        }
    }
    private int numberCompare(String value, Object val){
        return (new BigDecimal(value)).compareTo(new BigDecimal(val.toString()));
    }
    private void chkMinMax(JSONObject fieldJson, String title, String value,StringBuffer errMsg)   {
        if(TextHelper.isEmpty(value)) return;
        if(fieldJson.has("min")) {
            Object min = fieldJson.get("min");
            if(isNumberType(fieldJson)){
                if(numberCompare(value, min)<0){
                    errMsg.append(MsgUtil.getMsg("validate.min", new String[]{ title,  min.toString() } ) + "\n");
                }
            }

        }
        if(fieldJson.has("max")) {
            Object max = fieldJson.get("max");
            if(isNumberType(fieldJson)){
                if(numberCompare(value, max)>0){
                    errMsg.append(MsgUtil.getMsg("validate.max", new String[]{ title,  max.toString() } ) + "\n");
                }
            }

        }
    }
    private void chkLength(JSONObject fieldJson, String title, String value,StringBuffer errMsg)   {
        if(TextHelper.isEmpty(value)) return;
        if(fieldJson.has("minLength")) {
            int minLength = fieldJson.getInt("minLength");
            if(value.length()<minLength){
                errMsg.append(MsgUtil.getMsg("validate.minLength", new String[]{ title, "" + minLength} ) + "\n");

            }
        }
        if(fieldJson.has("maxLength")) {
            int maxLength = fieldJson.getInt("maxLength");
            if(value.length()>maxLength){
                errMsg.append(MsgUtil.getMsg("validate.maxLength", new String[]{ title , "" + maxLength} ) + "\n");

            }
        }
    }


    private void chkType(JSONObject fieldJson, String title, String value,StringBuffer errMsg)  {
        if(!fieldJson.has("type")) return;
        if(TextHelper.isEmpty(value)) return;
        String type = fieldJson.getString("type");
        switch (type){
            case "number":
                if(! isNumber(value)){
                    errMsg.append( MsgUtil.getMsg("validate.type", new String[]{ title, type }) + "\n");
                };
                break;
            case "object":
                JSONObject valueJson = null;
                try{
                    valueJson = new JSONObject(value);
                }
                catch(Exception e){
                    errMsg.append( MsgUtil.getMsg("validate.type", new String[]{ title , type }) + "\n");
                }
                if(fieldJson.has("properties")) {
                    JSONObject objectProperties = fieldJson.getJSONObject("properties");
                    chkValidate(objectProperties, valueJson, errMsg);

                }
                break;
            case "array":
                JSONArray jsonArr = null;
                try{
                    jsonArr = new JSONArray(value);
                }
                catch(Exception e){
                    errMsg.append( MsgUtil.getMsg("validate.type", new String[]{  title, type })+ "\n");
                }
                if(fieldJson.has("items")) {
                    JSONObject items = fieldJson.getJSONObject("items");
                    for (int i = 0; i < jsonArr.length(); i++) {
                        JSONObject valueJson1 = jsonArr.getJSONObject(i);
                        chkValidate(items, valueJson1, errMsg);
                    }
                }

        }

    }
    private void chkValidate(JSONObject items,  JSONObject valueJson, StringBuffer errMsg){
        Iterator<String> keys1 = items.keys();
        while (keys1.hasNext()) {
            String key = keys1.next();
            if(items.has(key)) {
                chkValidate(items.getJSONObject(key),  getTitle(items.getJSONObject(key), key), valueJson.has(key) ? valueJson.getString(key) : null, errMsg);
            }
        }
    }

    private boolean isNumber(String value) {
        return NumberUtils.isNumber(value);
    }

    @Override
    public void destroy() {

    }
}
