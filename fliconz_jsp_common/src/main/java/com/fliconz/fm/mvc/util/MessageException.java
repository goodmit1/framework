package com.fliconz.fm.mvc.util;
 
import java.util.Locale;
import java.util.ResourceBundle.Control;

import org.springframework.context.MessageSource;

import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.fliconz.fw.runtime.util.UTF8Control; 
public class MessageException extends Exception{
	
	public static final Control UTF8_CONTROL = new UTF8Control();
	String[]  msgCode;
	public String[] getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String... msgCode) {
		this.msgCode = msgCode;
	}
	
	
 
	public MessageException(String... msgCode)
	{
		this.msgCode  = msgCode;
		
	}
	 
	protected String getLang()
	{
		try
		{
			return UserVO.getUser().getLangKnd();
		}
		catch(Exception e)
		{
			return "ko";
		}
	}
	@Override
	public String getMessage()
	{
		try
		{
			
			MessageSource messageSource = (MessageSource)SpringBeanUtil.getBean("messageSource");
			String[] dest = null;
			if(msgCode.length>1)
			{
				dest = new String[msgCode.length-1];
				System.arraycopy(msgCode, 1, dest, 0, dest.length);
			}
			
			return messageSource.getMessage(msgCode[0] , dest, new Locale(getLang()));
			 
		}
		catch(Exception e)
		{
			return "[" + msgCode[0] + "]";
		}
		
	}
}
