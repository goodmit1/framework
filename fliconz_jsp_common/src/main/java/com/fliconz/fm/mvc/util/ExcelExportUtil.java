package com.fliconz.fm.mvc.util;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook; 
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fliconz.fm.common.util.TextHelper; 

public class ExcelExportUtil {
	Workbook generatedExcel;
	String fileName;
	JSONArray colDef;
	int rowIndex = 1; 
	public ExcelExportUtil(String fileName, JSONArray colDef  )
	{
		this.fileName = fileName;
		this.colDef =  colDef;
	}
	public void export(JSONArray data,HttpServletRequest request ,HttpServletResponse response ) throws  Exception
	{
		
		OutputStream out = getOut(request,response);
		export(out,data);
		out.flush();
		
		
	}
	
	public void export(OutputStream out, JSONArray data) throws IOException{
		export(out, null, data);
	}
	
	public void export(OutputStream out, String sheetName, JSONArray data) throws IOException
	{
		Sheet sheet = makeHeader(sheetName);
		makeData(sheet,data);
		generatedExcel.write(out);
		
	}
	public void exportCSV(JSONArray data,HttpServletRequest request ,HttpServletResponse response ) throws  Exception
	{
		Writer out = getCSVOut(request, response);
		exportCSV(out, data);
		 
		
	}
	public void exportCSV(List<Map> data,HttpServletRequest request ,HttpServletResponse response ) throws  Exception
	{
		Writer out = getCSVOut(request, response);
		exportCSV(out, data);
		 
		
	}
	public void exportCSV(Writer out ,JSONArray data) throws  Exception {
		 
		for (int j=0; j < colDef.length(); j++) {
			 
			JSONObject col = colDef.getJSONObject(j);
			if(!isTarget(col)) continue; 
			out.write(col.getString("headerName"));
			out.write(",");
			 

		}
		out.write("\n");
		for(int i=0; i < data.length(); i++)
		{
			JSONObject data1 = data.getJSONObject(i);
			for (int j=0; j < colDef.length(); j++) {
				 
				JSONObject col = colDef.getJSONObject(j);
				if(!isTarget(col)) continue; 
				Object obj= data1.get(col.getString("field")) ;
				if(obj instanceof String)
				{
					out.write("=\""+ obj.toString().replace("\"", "\"\"") + "\"");
				}
				else
				{	
					out.write(obj.toString());
				}
				 
				out.write(",");
				 

			}
			out.write("\n");
		}
		
	}
	public void exportCSV(Writer out ,List<Map> data) throws  Exception {
		 
		for (int j=0; j < colDef.length(); j++) {
			 
			JSONObject col = colDef.getJSONObject(j);
			if(!isTarget(col)) continue; 
			out.write(col.getString("headerName"));
			out.write(",");
			 

		}
		out.write("\n");
		for(int i=0; i < data.size(); i++)
		{
			Map data1 = data.get(i);
			for (int j=0; j < colDef.length(); j++) {
				 
				JSONObject col = colDef.getJSONObject(j);
				if(!isTarget(col)) continue; 
				Object obj= data1.get(col.getString("field")) ;
				if(obj instanceof String)
				{
					out.write("\""+ obj.toString() + "\"");
				}
				else
				{	
					out.write(obj.toString());
				}
				out.write(",");
				 

			}
			out.write("\n");
		}
		
	}
	public void export(List<Map> data,HttpServletRequest request ,HttpServletResponse response ) throws  Exception
	{ 
		OutputStream out = getOut(request,response);
		export(  out,data);
		out.flush();
		
	}
	public void export(OutputStream out,   List<Map> data) throws IOException
	{
		export(out,null,data);
	}
	public void export( String sheetName, List<Map> data) throws IOException
	{
		export(null, sheetName, data);
		 
	}
	public void export(  OutputStream out, String sheetName, List<Map> data) throws IOException
	{
		Sheet sheet = makeHeader(sheetName);
		makeData(sheet,data);
		if(out != null) {
			generatedExcel.write(out);
		}
		 
	}
	protected void makeData(Sheet sheet, JSONArray data)
	{
		int idx = rowIndex ;
		for(int i=0; i < data.length(); i++)
		{
			JSONObject m = data.getJSONObject(i);
			Row row = sheet.createRow(idx++);
			addExtraRowMap(m);
			addColumnValue(row,   m);
		}
	}
	protected void makeData( Sheet sheet, List<Map> data) throws IOException
	{
		int idx = rowIndex ;
		for(int i=0; i < data.size(); i++)
		{
			Map m = data.get(i);
			Row row = sheet.createRow(idx++);
			addExtraRowMap(m);
			addColumnValue(row,   m);
		}
		
	}
	protected void addExtraRowMap(Map m) {
		 
		
	}
	protected Sheet makeHeader(String sheetName)
	{
		if(generatedExcel == null) {
			generatedExcel = new HSSFWorkbook();	
		}
		
		Sheet sheet = null;
		if(sheetName == null) {
			sheet = generatedExcel.createSheet();
		}
		else {
			sheet = generatedExcel.createSheet(sheetName);
		}
		int idx = 0;
	 
		Row[] headers = new Row[rowIndex];
		for(int i=0; i <rowIndex ; i++)
		{
			headers[i] = sheet.createRow(idx++);
		}


		for(int i=0; i <rowIndex; i++)
		{


			for (int j=0; j < colDef.length(); j++) {
				 
				JSONObject col = colDef.getJSONObject(j);
				if(!isTarget(col)) continue; 
				addColumnValue(headers[i], col.get("headerName"));

				 

			}
		}
		return sheet;
		 
		
	}
	protected void addColumnValue(Row row, JSONObject value) {
		for (int j=0; j < colDef.length(); j++) {
			JSONObject col = colDef.getJSONObject(j);
			if(!isTarget(col)) continue; 
			 
			addColumnValue(row, value.get(col.getString("field")));
		}
	}
	protected boolean isTarget(JSONObject col )
	{
		if(col.has("hide") && col.getBoolean("hide")) return false;
		if(!col.has("field")  || TextHelper.isEmpty(col.getString("field")) ) return false;
		return true;
	}
	protected void addColumnValue(Row row, Map value) {
		for (int j=0; j < colDef.length(); j++) {
			JSONObject col = colDef.getJSONObject(j);
			if(!isTarget(col)) continue; 
			 
			addColumnValue(row, value.get(col.getString("field")));
		}
	}
	protected void addExtraRowMap(JSONObject m) {
		 
		
	}
	protected void addColumnValue(Row row, Object value) {
		int cellIndex = row.getLastCellNum() == -1 ? 0 : row.getLastCellNum();
		Cell cell = row.createCell(cellIndex);
		if(value == null)
		{
			cell.setCellValue("");
		}
		else if(value instanceof String)
		{
			cell.setCellValue(new HSSFRichTextString((String)value));
		}
		else if(value instanceof BigDecimal)
		{
			cell.setCellValue(((BigDecimal)value).doubleValue());
		}
		/*else if(value instanceof java.sql.Timestamp)
		{
			Date d = new Date(((java.sql.Timestamp)value).getTime());
			cell.setCellValue(d);
		}
		else if(value instanceof Date)
		{
			cell.setCellValue((Date) value);
		}*/

		else
		{
			cell.setCellValue(value.toString());
		}
	}
	public OutputStream getOut(HttpServletRequest request,HttpServletResponse response ) throws  Exception {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control",
				"must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		
		setFileName(request, response, fileName + ".xls");
		/*response.setHeader("Content-disposition",
				"attachment;filename=" + new String(fileName.getBytes("utf-8"),"8859_1")  + ".xls");*/

		return response.getOutputStream();
		 
	}
	protected void setFileName(HttpServletRequest request,HttpServletResponse response, String filename) throws Exception
	{
		 String userAgent = request.getHeader("User-Agent");
		 
		    // attachment; 가 붙으면 IE의 경우 무조건 다운로드창이 뜬다. 상황에 따라 써야한다.
		    if (userAgent != null && userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
		      response.setHeader("Content-Disposition", "filename=" + URLEncoder.encode(filename, "UTF-8") + ";");
		    } else { // MS IE (보통은 6.x 이상 가정)
		      response.setHeader("Content-Disposition", "attachment; filename="
		          + java.net.URLEncoder.encode(filename, "UTF-8") + ";");
		    }  
	}
	protected Writer getCSVOut(HttpServletRequest request ,HttpServletResponse response ) throws  Exception {
		response.setContentType("text/csv;charset=MS949");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control",
				"must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		setFileName(request, response, fileName + ".csv");
	 

		return response.getWriter();
		
	}
}
