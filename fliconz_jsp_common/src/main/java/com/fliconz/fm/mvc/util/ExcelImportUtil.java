package com.fliconz.fm.mvc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fliconz.fm.mvc.DefaultService;


public class ExcelImportUtil {
	List<String> fields;
	int titleRowLength = 1;
	int sheetNo = 0;
	public void setSheetNo(int sheetNo) {
		this.sheetNo = sheetNo;
	}
	public ExcelImportUtil( int titleRowLength,List fields)
	{
		this.fields = fields;
		this.titleRowLength = titleRowLength;
		
	}
	public ExcelImportUtil(int titleRowLength, String... fields )
	{
		List list = new ArrayList();
		for(String f:fields)
		{
			list.add(f);
		}
		this.fields = list;
		this.titleRowLength = titleRowLength;
		
	}
	public List importExcel(File file) throws Exception
	{
		Workbook workbook;
	
		if(file.getName().endsWith("xlsx"))
		{
			workbook = new XSSFWorkbook( new FileInputStream(file));
		}
		else
		{
			workbook = new HSSFWorkbook( new FileInputStream(file));
		}
		return importExcel(workbook);
	}
	public List importExcel(String fileName, InputStream in) throws Exception
	{
		Workbook workbook;
	
		if(fileName.endsWith("xlsx"))
		{
			workbook = new XSSFWorkbook(in);
		}
		else
		{
			workbook = new HSSFWorkbook(in);
		}
		return importExcel(workbook);
	}
	protected List importExcel(org.apache.poi.ss.usermodel.Workbook workbook) throws Exception
	{
		return importExcel(workbook, fields, titleRowLength, -1);
		 
	}
	protected List importExcel(org.apache.poi.ss.usermodel.Workbook workbook, List<String> fields, int titleRows, int startCellIdx ) throws Exception
	{
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
		Sheet  sheet = workbook.getSheetAt(this.sheetNo );
		 

		Iterator<Row> rowIterator = sheet.iterator();
		for(int i=0; i < titleRows; i++)
		{
				rowIterator.next(); // title
		}
		
		ArrayList list = new ArrayList();
		int rowIdx = 0;
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			int cellStart = startCellIdx >= 0 ? startCellIdx : row.getFirstCellNum();
			int cellLast = row.getLastCellNum();
			int idx = 0;
			Map newRow = new HashMap();
			for(int i=cellStart; i < cellLast; i++)
			{
				String f = (String)fields.get(i);
				if(f== null || "".equals(f)) continue;
				Cell cell = row.getCell(i);
				try
				{
					
					Object val = "";
					if(cell != null)
					{
						switch (cell.getCellType()) {
						case Cell.CELL_TYPE_NUMERIC:
							if( DateUtil.isCellDateFormatted(cell)) {
								val = cell.getDateCellValue();
	
							}
							else
							{
								val = NumberToTextConverter.toText( cell.getNumericCellValue());
								if(((String)val).endsWith(".0"))
								{
									 val = String.valueOf((int)cell.getNumericCellValue());
								}
							}
							break;
						case Cell.CELL_TYPE_FORMULA:
							if(!(cell.toString()=="") ){
								if(evaluator.evaluateFormulaCell(cell)==Cell.CELL_TYPE_NUMERIC){
	
									val = NumberToTextConverter.toText( cell.getNumericCellValue());
									if(((String)val).endsWith(".0"))
									{
										 val = String.valueOf((int)cell.getNumericCellValue());
									}
								}else if(evaluator.evaluateFormulaCell(cell)==Cell.CELL_TYPE_STRING){
									val = cell.getStringCellValue();
								}else if(evaluator.evaluateFormulaCell(cell)==Cell.CELL_TYPE_BOOLEAN){
									boolean fbdata = cell.getBooleanCellValue();
									val = String.valueOf(fbdata);
								}
								break;
							}
						default:
							val = cell.getStringCellValue();
							break;
						}
						newRow.put(f, val);
					}
					else
					{
						newRow.put(f, val);
					}
					if(f.endsWith("_NM") && newRow.get(f.substring(0, f.length()-3)) == null)
					{
						String f1 = f.substring(0, f.length()-3);
						
						newRow.put(f1, getCodeVal(f1, val));
	
					
					}
				}
				catch(Exception e)
				{
					throw new Exception("Row:" + rowIdx + ",Cell:" + i  + "="+cell + "==>" + e.getMessage());
				}
			}
			onExcelImportRow(newRow);
			list.add(newRow);
		}
		return list;
	}
	protected void onExcelImportRow(Map newRow) {
		newRow.put(DefaultService.IU_FILED, "I");
		
	}
	protected String getCodeVal(String key, Object val) {
		return null;
	}

}
