package com.fliconz.fm.mvc;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.fliconz.fm.security.UserVO;

public class UserLocaleResolver extends SessionLocaleResolver {

	 

	@Override
	public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
		if(UserVO.getUser().isLogin())
		{
			UserVO.getUser().setLocale(locale.getLanguage());
		}
		super.setLocale(request, response, locale); 
		
	}

}
