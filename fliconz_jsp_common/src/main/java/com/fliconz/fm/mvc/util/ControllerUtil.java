package com.fliconz.fm.mvc.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.URLFilter;
import com.fliconz.fw.runtime.util.PropertyManager;

public class ControllerUtil {
	public static final String REQUST_PARAM_KEY = "__REQUEST_PARAM__";

	public static Map<String, Object> json2Map(JSONObject paramJson) {
		Map<String, Object> result = new HashMap<String, Object>();
		for (Object key : paramJson.keySet()) {
			result.put((String) key, paramJson.get(key));
		}
		return result;

	}
	
	public static Map<String,Object> getSuccessMap(Map param, String...keys)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		if(param != null && keys != null)
		{
			for (Object key : keys) {
				result.put((String) key, param.get(key));
			}
		}
		result.put("result", "ok");
		return result;
	}

	public static JSONObject getParamJson(HttpServletRequest request) throws Exception {
		if (request.getContentLength() > 0) {
			JSONParser jsonParser = new JSONParser();
			JSONObject obj = (JSONObject) jsonParser.parse(request.getReader());
			return obj;
		}
		return null;
	}
	
	public static Map getParam(HttpServletRequest request) throws Exception {
		boolean isjsonRequest = PropertyManager.getBoolean("is_json_request", true);
		boolean isMimeJson   = false;
		if(request.getContentType() != null) {
			isMimeJson = request.getContentType().startsWith( "application/json" ) ;
		}
		if(isjsonRequest || isMimeJson)
		{
			return getJsonParam(request);
		}
		else
		{
			
			Enumeration names = request.getParameterNames();
			Map result = new HashMap();
			while(names.hasMoreElements())
			{
				String name = (String)names.nextElement();
				
				String[] values = request.getParameterValues(name);
				
				if(name.endsWith("[]"))
				{
					result.put(name.substring(0,  name.length()-2), values);
				}
				else
				{
					if(values.length==1)
						result.put(name,  values[0]);
					else
						result.put(name, values);	
				}
				
				
			}
			return result;
		}
	}
	public static Map getJsonParam(HttpServletRequest request) throws Exception {
		Map param = (Map) request.getAttribute(REQUST_PARAM_KEY);
		if (param == null) {
			JSONObject paramJson = ControllerUtil.getParamJson(request);

			if (paramJson != null) {
				param = ControllerUtil.json2Map(paramJson);
			} else {
				param = new HashMap();
			}
			
			request.setAttribute(REQUST_PARAM_KEY, param);
		}

		return param;
	}
	public static Map getErrMap(String  msg) {
	 
		Map result = new HashMap();
		 
		result.put("error", msg);
		return result;
	}
	public static ResponseEntity<Object> getErrMap(Throwable e) {
		e.printStackTrace();
		Map result = new HashMap();
		String msg = e.getMessage();
		if(e instanceof org.springframework.dao.DuplicateKeyException)
		{
			msg = MsgUtil.getMsg("msg_err_duplicate", null); // "중복된 값이 있습니다.";
		}
		else if(msg != null && msg.indexOf("SQL")>=0)
		{
			msg = MsgUtil.getMsg("msg_err", null); //"에러가 발생하였습니다. 관리자에게 연락하세요";
		}
		result.put("error", msg);
		return new ResponseEntity<Object>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		 
	}
}