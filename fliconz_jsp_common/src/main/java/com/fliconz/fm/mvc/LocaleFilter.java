package com.fliconz.fm.mvc;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.LocaleResolver;

import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class LocaleFilter   implements Filter{

	String param = "lang";
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		 
		if( filterConfig.getInitParameter("param") != null) {
			param = filterConfig.getInitParameter("param");
		};
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		LocaleResolver resolver = (LocaleResolver )SpringBeanUtil.getBean("localeResolver");
		if(request.getParameter(param) != null)
		{
			Locale locale = new Locale(request.getParameter(param)); 
		
			resolver.setLocale((HttpServletRequest)request, (HttpServletResponse)response, locale);
		}
		request.setAttribute(DispatcherServlet.LOCALE_RESOLVER_ATTRIBUTE, resolver);
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		 
		
	}
	

}
