package com.fliconz.fm.mvc;

import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

import javax.crypto.Cipher;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.*;
import java.security.spec.RSAPublicKeySpec;
import java.util.Set;

public class JsDecryptFilter implements Filter {
    /**
     * 복호화
     *
     *
     * @param securedValue
     * @return
     * @throws Exception
     */
    private static String RSA_WEB_KEY = "_RSA_WEB_Key_"; // 개인키 session key
    private static String RSA_INSTANCE = "RSA"; // rsa transformation

    boolean isJsDecript = false;
    Set<String> jsEncryptParam  ;
    public static String decryptRsa(HttpServletRequest request, String securedValue) throws Exception {
        HttpSession session = request.getSession();
        PrivateKey privateKey = (PrivateKey) session.getAttribute(RSA_WEB_KEY);

        Cipher cipher = Cipher.getInstance(RSA_INSTANCE);
        byte[] encryptedBytes = hexToByteArray(securedValue);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
        String decryptedValue = new String(decryptedBytes, "utf-8"); // 문자 인코딩 주의.
        return decryptedValue;
    }
    /**
     * 16진 문자열을 byte 배열로 변환한다.
     *
     * @param hex
     * @return
     */
    public static byte[] hexToByteArray(String hex) {
        if (hex == null || hex.length() % 2 != 0) { return new byte[] {}; }

        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < hex.length(); i += 2) {
            byte value = (byte) Integer.parseInt(hex.substring(i, i + 2), 16);
            bytes[(int) Math.floor(i / 2)] = value;
        }
        return bytes;
    }

    public void initRsa(HttpServletRequest request) {
        HttpSession session = request.getSession();

        KeyPairGenerator generator;
        try {
            generator = KeyPairGenerator.getInstance(RSA_INSTANCE);
            generator.initialize(1024);



            if(session.getAttribute(RSA_WEB_KEY) == null)
            {
                KeyPair keyPair = generator.genKeyPair();
                KeyFactory keyFactory = KeyFactory.getInstance(RSA_INSTANCE);
                PrivateKey privateKey = keyPair.getPrivate();

                session.setAttribute(RSA_WEB_KEY, privateKey); // session에 RSA 개인키를 세션에 저장

                PublicKey publicKey = keyPair.getPublic();
                RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
                String publicKeyModulus = publicSpec.getModulus().toString(16);
                String publicKeyExponent = publicSpec.getPublicExponent().toString(16);

                session.setAttribute("RSAModulus", publicKeyModulus); // rsa modulus 를 request 에 추가
                session.setAttribute("RSAExponent", publicKeyExponent); // rsa exponent 를 request 에 추가
            }


            request.setAttribute("RSAModulus", session.getAttribute("RSAModulus")); // rsa modulus 를 request 에 추가
            request.setAttribute("RSAExponent", session.getAttribute("RSAExponent")); // rsa exponent 를 request 에 추가
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String param = PropertyManager.getString("security.js_encrypt_param");
        if(param != null && !"".equals(param))
        {
            jsEncryptParam = MapUtil.toSet(param.split(","));
            isJsDecript = true;
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(isJsDecript){
            initRsa((HttpServletRequest)request);
            chain.doFilter(new JsDecryptRequestWrapper((HttpServletRequest) request,jsEncryptParam), response);
        }
        else{
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
