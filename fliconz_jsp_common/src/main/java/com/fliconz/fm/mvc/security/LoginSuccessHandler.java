package com.fliconz.fm.mvc.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.fliconz.fm.security.FMAuthenticationSuccessHandler;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.PropertyManager;

@Component("mvcAuthenticationSuccessHandler")
public class LoginSuccessHandler extends FMAuthenticationSuccessHandler{

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		boolean isRedirect = request.getAttribute("isRedirect") == null ? true: (boolean)request.getAttribute("isRedirect");
		request.setAttribute("isRedirect", false);
		super.onAuthenticationSuccess(request, response, authentication);
		UserVO userVo = (UserVO) authentication.getPrincipal();
		userVo.setUserMenuManager(new UserMenuManagerImpl());
		userVo.addRole("U" + userVo.getUserId()); // 개인별 프로그램 권한 때문에 추가
		if(isRedirect) response.sendRedirect(PropertyManager.getString("login.defaultTargetURL", "/index.jsp"));
	}
	

}
