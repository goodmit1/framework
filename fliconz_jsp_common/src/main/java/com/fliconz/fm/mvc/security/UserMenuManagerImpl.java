package com.fliconz.fm.mvc.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fliconz.fm.common.cache.CacheManager;
import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.common.vo.TreeNodeMap;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.security.SecurityConstant;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.usermenu.UserMenuManager;
import com.fliconz.fm.usermenu.UserMenuService;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class UserMenuManagerImpl implements UserMenuManager, Serializable {
	private static final long serialVersionUID = 1L;

	public UserMenuManagerImpl(){
		this.init();
	}

	private void init() {
		try {
			this.initDefaultPage();
			this.reloadMenu();
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	public void reloadMenu() throws Exception {
		this.initializeUserMenuMap();
		if(PropertyManager.getBoolean("mymenu.has",true))
		{
			this.initializeMyMenuMap();
		}
		 
	}

	private long menuUpdateTime = 0;
	public long getMenuUpdateTime() {
		return menuUpdateTime;
	}

	public UserMenuService getUserMenuService() {
		return (UserMenuService)SpringBeanUtil.getBean("userMenuService");
	}

	
	private void initDefaultPage(){
		String defaultPage = PropertyManager.getString("home.defaultRedirectPage");
		this.setDefaultPage(defaultPage);
		String mobileDefaultPage = PropertyManager.getString("home.defaultRedirectPage.mobile");
		this.setMobileDefaultPage(mobileDefaultPage);
	}

	private String defaultPage;
	public String getDefaultPage() {
		return defaultPage;
	}

	public void setDefaultPage(String defaultPage) {
		this.defaultPage = defaultPage;
	}

	private String mobileDefaultPage;
	public String getMobileDefaultPage() {
		return mobileDefaultPage;
	}

	public void setMobileDefaultPage(String mobileDefaultPage) {
		this.mobileDefaultPage = mobileDefaultPage;
	}

	private Map<Integer, TreeNodeMap> menuElementMap;
	private Map<Integer, TreeNodeMap> mobileMenuElementMap;

	private void initializeUserMenuMap() throws Exception {
		if(menuElementMap == null) menuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		synchronized (menuElementMap) {
			menuElementMap.clear();
			List<Integer> leafMenuList = this.getLeafMenuList();
			if(leafMenuList != null){
				for(int menuId: leafMenuList){
					if(CacheManager.getMenu(menuId)==null) continue;
					TreeNodeMap menuMap = CacheManager.getMenu(menuId).deepCopy();
					if(menuElementMap.get(menuMap.getMenuId()) != null){
						continue;
					}
					menuElementMap.put(menuMap.getMenuId(), menuMap);
					recursiveFindParentMenu(menuElementMap, menuMap);
				}
			}
			if(mobileMenuElementMap == null) mobileMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
			synchronized (mobileMenuElementMap) {
				mobileMenuElementMap.clear();
				List<Integer> mobileLeafMenuList = this.getMobileLeafMenuList();
				if(mobileLeafMenuList != null){
					for(int menuId: mobileLeafMenuList){
						if(CacheManager.getMenu(menuId)==null) continue;
						TreeNodeMap menuMap = CacheManager.getMenu(menuId).deepCopy();
						mobileMenuElementMap.put(menuMap.getMenuId(), menuMap);
						recursiveFindParentMenu(mobileMenuElementMap, menuMap);
					}
				}
			}
			this.menuUpdateTime = CacheManager.menuUpdateTime();
		}
	}

	private void recursiveFindParentMenu(Map<Integer, TreeNodeMap> menuElementMap, TreeNodeMap menuMap) throws CloneNotSupportedException{
		if(menuMap.getParentId() > 0){
			TreeNodeMap parent = menuElementMap.get(menuMap.getParentId());
			if(parent == null) {
				parent = CacheManager.getMenu(menuMap.getParentId()).deepCopy();
				if(parent != null) {
					menuElementMap.put(menuMap.getParentId(), parent);
					recursiveFindParentMenu(menuElementMap, parent);
				}
			}
			if(parent != null) {
				parent.add(menuMap);
			}
		}
	}

	private Map<Integer, TreeNodeMap> myMenuElementMap;
	private Map<Integer, TreeNodeMap> mobileMyMenuElementMap;


	private void initializeMyMenuMap() throws Exception {
		if(myMenuElementMap == null) myMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		if(mobileMyMenuElementMap == null) mobileMyMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		synchronized (menuElementMap) {
			myMenuElementMap.clear();
			
			List<Integer> myMenuList = this.getMyMenuList();
			if(myMenuList != null){
				for(int menuId: myMenuList){
					TreeNodeMap menuMap = CacheManager.getMenu(menuId).deepCopy();
					myMenuElementMap.put(menuMap.getMenuId(), menuMap);
					recursiveFindParentMenu(myMenuElementMap, menuMap);
			
				}
			}

			
			synchronized (mobileMyMenuElementMap) {
				mobileMyMenuElementMap.clear();
				List<Integer> mobileMyMenuList = this.getMobileMyMenuList();
				if(mobileMyMenuList != null){
					for(int menuId: mobileMyMenuList){
						TreeNodeMap menuMap = CacheManager.getMenu(menuId).deepCopy();
						mobileMyMenuElementMap.put(menuMap.getMenuId(), menuMap);
						recursiveFindParentMenu(mobileMyMenuElementMap, menuMap);
						
					}
				}
			}
			isMyMenuUpdated = true;
		}
	}

	/**
	 * 濡쒓렇�����좎���沅뚰븳�덈뒗 Leaf Menu List
	 */
	private List<Integer> getLeafMenuList(){
		List<Integer> leafMenuList = null;
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			if(!UserVO.getUser().hasRole(SecurityConstant.ROLE_SUPER_ADMIN)) paramMap.put("USER_ID", UserVO.getUser().getLoginId());
			paramMap.put("BY_PASS_COMMON_PARAM", "Y");
			paramMap.put("ROLE_NMS", UserVO.getUser().getRoleNames());
			leafMenuList = getUserMenuService().selectLeafMenuListFromUser(paramMap);
		} catch(Exception e){
			e.printStackTrace();
		}
		return leafMenuList;
	}

	/**
	 * 濡쒓렇�����좎���My Menu List
	 */
	private List<Integer> getMyMenuList(){
		List<Integer> myMenuList = null;
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("USER_ID", UserVO.getUser().getUserId());
			paramMap.put("BY_PASS_COMMON_PARAM", "Y");
			myMenuList = getUserMenuService().selectMyMenuListFromUser(paramMap);
		} catch(Exception e){
			e.printStackTrace();
		}
		return myMenuList;
	}

	/**
	 * 濡쒓렇�����좎���沅뚰븳�덈뒗 Mobile Leaf Menu List
	 */
	private List<Integer> getMobileLeafMenuList(){
		List<Integer> leafMenuList = null;
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			if(!UserVO.getUser().hasRole(SecurityConstant.ROLE_SUPER_ADMIN)) paramMap.put("USER_ID", UserVO.getUser().getLoginId());
			paramMap.put("MOBILE_YN", "Y");
			paramMap.put("BY_PASS_COMMON_PARAM", "Y");
			paramMap.put("ROLE_NMS", UserVO.getUser().getRoleNames());
			leafMenuList = getUserMenuService().selectLeafMenuListFromUser(paramMap);
		} catch(Exception e){
			e.printStackTrace();
		}
		return leafMenuList;
	}

	/**
	 * 濡쒓렇�����좎���Mobile My Menu List
	 */
	private List<Integer> getMobileMyMenuList(){
		List<Integer> myMenuList = null;
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("USER_ID", UserVO.getUser().getUserId());
			paramMap.put("MOBILE_YN", "Y");
			paramMap.put("BY_PASS_COMMON_PARAM", "Y");
			myMenuList = getUserMenuService().selectMyMenuListFromUser(paramMap);
		} catch(Exception e){
			e.printStackTrace();
		}
		return myMenuList;
	}

	public TreeNodeMap getFirstSubMenu(){
		if(selectedFirstMenu == 0){
			return this.getFirstMenuList().get(0);
		}
		return (TreeNodeMap)menuElementMap.get(selectedFirstMenu);
	}

	public TreeNodeMap getMobileFirstSubMenu(){
		if(selectedFirstMenu == 0){
			return null;
		}
		return (TreeNodeMap)mobileMenuElementMap.get(selectedFirstMenu);
	}

	public Map<Integer, TreeNodeMap> getMenuElementMap() {
		return menuElementMap;
	}

	public void setMenuElementMap(Map<Integer, TreeNodeMap> menuElementMap) {
		this.menuElementMap = menuElementMap;
	}

	public Map<Integer, TreeNodeMap> getMobileMenuElementMap() {
		return mobileMenuElementMap;
	}

	public void setMobileMenuElementMap(Map<Integer, TreeNodeMap> mobileMenuElementMap) {
		this.mobileMenuElementMap = mobileMenuElementMap;
	}

	public Map<Integer, TreeNodeMap> getMyMenuElementMap() {
		return myMenuElementMap;
	}

	public void setMyMenuElementMap(Map<Integer, TreeNodeMap> myMenuElementMap) {
		this.myMenuElementMap = myMenuElementMap;
	}

	public Map<Integer, TreeNodeMap> getMobileMyMenuElementMap() {
		return mobileMyMenuElementMap;
	}

	public void setMobileMyMenuElementMap(Map<Integer, TreeNodeMap> mobileMyMenuElementMap) {
		this.mobileMyMenuElementMap = mobileMyMenuElementMap;
	}

	/**
	 * �꾩옱 �좏깮��1李�硫붾돱 ID
	 */
	private int selectedFirstMenu;
	public int getSelectedFirstMenu() {
		return selectedFirstMenu;
	}

	public void setSelectedFirstMenu(int selectedFirstMenu) {
		if(this.selectedFirstMenu != selectedFirstMenu) this.setChangedFirstMenu(true);
		this.selectedFirstMenu = selectedFirstMenu;
	}

	public void findSelectedFirstMenu(TreeNodeMap subMenu){
		TreeNodeMap parentMenu = (TreeNodeMap)CacheManager.getMenu(subMenu.getParentId());
		if(parentMenu.getParentId() > 1){
			findSelectedFirstMenu(parentMenu);
		} else {
			this.setSelectedFirstMenu(parentMenu.getMenuId());
		}
	}
	@Override
	public int getParentId(int id)
	{

			TreeNodeMap pNode = (TreeNodeMap)this.menuElementMap.get(id);
			if(pNode != null)
			{
				return pNode.getParentId();
			}
			return -1;
	}
	/**
	 * �꾩옱 �좏깮��Leaf FMMenuVO �ㅻ툕�앺듃
	 */
	private TreeNodeMap selectedMenu;
	public TreeNodeMap getSelectedMenu() {
		try {
			if(this.menuElementMap == null || this.menuElementMap.isEmpty()){
				this.reloadMenu();
			}
			/*if(selectedMenu == null) {
				int menuId = PropertyManager.getInt("", 10);
				HttpServletRequest req = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
				if(FMSecurityContextHelper.isMobile(req)) menuId = PropertyManager.getInt("", 11);
				selectedMenu = (TreeNodeMap)this.menuElementMap.get(CacheManager.getMenu(menuId).getMenuId());
			}*/
		} catch(Exception e){
			e.printStackTrace();
		}
		return selectedMenu;
	}

	public void setSelectedMenu(TreeNodeMap selectedMenu) {
		this.selectedMenu = selectedMenu;
		if(UserVO.getUser() != null){
			if(selectedMenu != null)
			{
				UserVO.getUser().setSelectedPrgmId(selectedMenu.getPrgmId());
			}
			else
			{
				UserVO.getUser().setSelectedPrgmId(1);
			}
		}
	}

	public String getSelectedMenuName(){
		String menuName = null;
		if(selectedMenu != null) {
			return  selectedMenu.getMenuNm(FMSecurityContextHelper.getLang());
			 
		}
		return menuName;
	}

	/**
	 * RequestURI濡�硫붾돱瑜�李얠븘 由ы꽩�쒕떎.
	 * @param requestURI
	 * @return
	 */
	public TreeNodeMap findMenuByPrgmPath(String requestURI) {
		if(menuElementMap == null) menuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		Iterator<Integer> it = menuElementMap.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			TreeNodeMap menu = menuElementMap.get(key);
			if(requestURI.equals(menu.getPrgmPath()) || ((TextHelper.isNotEmpty(menu.getPkgNm()) && requestURI.startsWith(menu.getPkgNm())))){
				return menu;
			}
		}
		return null;
	}

	public TreeNodeMap findMobileMenuByPrgmPath(String requestURI) {
		if(mobileMenuElementMap == null) mobileMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		Iterator<Integer> it = mobileMenuElementMap.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			TreeNodeMap menu = mobileMenuElementMap.get(key);
			if(requestURI.equals(menu.getPrgmPath())  || ((TextHelper.isNotEmpty(menu.getPkgNm()) && requestURI.startsWith(menu.getPkgNm())))){
				return menu;
			}
		}
		return null;
	}

	/**
	 * menuId(Integer)濡�硫붾돱瑜�李얠븘 由ы꽩�쒕떎.
	 * @param menuId
	 * @return
	 */
	public TreeNodeMap findMenuById(int menuId) {
		if(menuElementMap == null) menuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		Iterator<Integer> it = menuElementMap.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			TreeNodeMap menu = menuElementMap.get(key);
			if(menuId == menu.getMenuId()){
				return menu;
			}
		}
		return null;
	}

	public TreeNodeMap findMobileMenuById(int menuId) {
		if(mobileMenuElementMap == null) mobileMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		Iterator<Integer> it = mobileMenuElementMap.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			TreeNodeMap menu = mobileMenuElementMap.get(key);
			if(menuId == menu.getMenuId()){
				return menu;
			}
		}
		return null;
	}

	/**
	 * RequestURI濡�留덉씠硫붾돱瑜�李얠븘 由ы꽩�쒕떎.
	 * @param requestURI
	 * @return
	 */
	public TreeNodeMap findMyMenuByPrgmPath(String requestURI) {
		if(myMenuElementMap == null) myMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		Iterator<Integer> it = myMenuElementMap.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			TreeNodeMap menu = myMenuElementMap.get(key);
			if(requestURI.equals(menu.getPrgmPath())){
				return menu;
			}
		}
		return null;
	}

	public TreeNodeMap findMobileMyMenuByPrgmPath(String requestURI) {
		if(mobileMenuElementMap == null) mobileMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		Iterator<Integer> it = mobileMenuElementMap.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			TreeNodeMap menu = mobileMenuElementMap.get(key);
			if(requestURI.equals(menu.getPrgmPath()) ){
				return menu;
			}
		}
		return null;
	}

	/**
	 * menuId(Integer)濡�留덉씠硫붾돱瑜�李얠븘 由ы꽩�쒕떎.
	 * @param menuId
	 * @return
	 */
	public TreeNodeMap findMyMenuById(int menuId) {
		if(myMenuElementMap == null) myMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		Iterator<Integer> it = myMenuElementMap.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			TreeNodeMap menu = myMenuElementMap.get(key);
			if(menuId == menu.getMenuId()){
				return menu;
			}
		}
		return null;
	}

	public TreeNodeMap findMobileMyMenuById(int menuId) {
		if(mobileMenuElementMap == null) mobileMenuElementMap = new LinkedHashMap<Integer, TreeNodeMap>();
		Iterator<Integer> it = mobileMenuElementMap.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			TreeNodeMap menu = mobileMenuElementMap.get(key);
			if(menuId == menu.getMenuId()){
				return menu;
			}
		}
		return null;
	}

	public String getSelectedParentMenuName(){
		String menuName = null;
		if(this.getSelectedMenu() == null) return menuName;
		TreeNodeMap selectedParentMenu = CacheManager.getMenu(this.getSelectedMenu().getParentId());
		if(selectedParentMenu != null) {
			return  selectedMenu.getMenuNm(FMSecurityContextHelper.getLang());
			 
		}
		return menuName;
	}

	public void reloadMyMenu() throws Exception {
		this.initializeMyMenuMap();
	}

	public TreeNodeMap getMyMenu(int menuId){
		return (TreeNodeMap)myMenuElementMap.get(menuId);
	}

	/*public void recursiveMakeUserMenu(TreeNodeMap root, Object rootMenu) throws CloneNotSupportedException{
		for(TreeNodeMap element: root.getElements()){
			if(element.isSubNode()){
				SubMenuVO subMenu = new SubMenuVO(element.deepCopy());
				((SubMenuVO)rootMenu).addElement(subMenu);
				recursiveMakeUserMenu(element, subMenu);
			} else {
				MenuVO menu = new MenuVO(element.deepCopy());
				((SubMenuVO)rootMenu).addElement(menu);
			}
		}
	}

	public DefaultMenuModel getLeftMenuModel() throws Exception {
		if(CacheManager.menuUpdateTime() > this.getMenuUpdateTime()){
			this.reloadMenu();
		}
		DefaultMenuModel userMenuModel = null;
		TreeNodeMap root = this.getFirstSubMenu();
		if(root != null){
			userMenuModel = new DefaultMenuModel();
			SubMenuVO rootMenu = new SubMenuVO((TreeNodeMap)root.deepCopy());
			recursiveMakeUserMenu(root, rootMenu);
			for(MenuElement menu: rootMenu.getElements()){
				userMenuModel.addElement(menu);
			}
		}
		return userMenuModel;
	}
	
	
	public DefaultMenuModel getAllMenuModel() throws Exception
	{
		 
		DefaultMenuModel userMenuModel = null;
		TreeNodeMap root = menuElementMap.get(1);
		if(root != null){
			userMenuModel = new DefaultMenuModel();
			SubMenuVO rootMenu = new SubMenuVO((TreeNodeMap)root.deepCopy());
			recursiveMakeUserMenu(root, rootMenu);
			for(MenuElement menu: rootMenu.getElements()){
				userMenuModel.addElement(menu);
			}
		}
		userMenuModel.addElement(getMyMenuElement());
		return userMenuModel;
	}
	private void setChildRendered(SubMenuVO menu, boolean rendered)
	{
		List<MenuElement> children = menu.getElements();
		for(MenuElement element: children){
			if(element instanceof SubMenuVO){
				((SubMenuVO)element).setRendered(rendered);
				if(!PropertyManager.getBoolean("menu.sub.icon.each", false)){
					((SubMenuVO) element).setIcon(PropertyManager.getString("menu.sub.icon", "fa fa-fw fa-folder-o"));
				}
				setChildRendered((SubMenuVO)element,rendered);
			} else {
				((MenuVO)element).setRendered(rendered);
			}
		}
	}
	private MenuElement getMyMenuElement() throws Exception
	{
		SubMenuVO vo = new SubMenuVO();
		vo.setElements(this.getLeftMyMenuModel().getElements());
		vo.setStyleClass("subRoot_myMenu");
		vo.setLabel("MyMenu");
		vo.setIcon("icon-star");
		setChildRendered(vo,true);
		return vo;

	}
	public DefaultMenuModel getLeftMyMenuModel() throws Exception {
		DefaultMenuModel myMenuModel = null;
		TreeNodeMap root = this.getMyMenuElementMap().get(this.getFirstSubMenu().getMenuId());
		if(root != null){
			myMenuModel = new DefaultMenuModel();
			SubMenuVO rootMenu = new SubMenuVO(root.deepCopy());
			recursiveMakeUserMenu(root, rootMenu);
			for(MenuElement menu: rootMenu.getElements()){
				myMenuModel.addElement(menu);
			}
			isMyMenuUpdated = false;
		}
		return myMenuModel;
	}


	public DefaultMenuModel getLeftMobileMenuModel() throws Exception {
		if(CacheManager.menuUpdateTime() > this.getMenuUpdateTime()){
			this.reloadMenu();
		}
		DefaultMenuModel mobileUserMenuModel = null;
		TreeNodeMap root = this.getMobileFirstSubMenu();
		if(root != null){
			mobileUserMenuModel = new DefaultMenuModel();
			SubMenuVO rootMenu = new SubMenuVO(root.deepCopy());
			recursiveMakeUserMenu(root, rootMenu);
			for(MenuElement menu: rootMenu.getElements()){
				mobileUserMenuModel.addElement(menu);
			}
		}
		return mobileUserMenuModel;
	}

	public DefaultMenuModel getLeftMobileMyMenuModel() throws Exception {
		DefaultMenuModel mobileMyMenuModel = null;
		TreeNodeMap root = this.getMobileMyMenuElementMap().get(2);
		if(root != null){
			mobileMyMenuModel = new DefaultMenuModel();
			SubMenuVO rootMenu = new SubMenuVO(root.deepCopy());
			recursiveMakeUserMenu(root, rootMenu);
			for(MenuElement menu: rootMenu.getElements()){
				mobileMyMenuModel.addElement(menu);
			}
		}
		return mobileMyMenuModel;
	}

	private boolean isMenuClicked = false;
	public boolean isMenuClicked(){
		return isMenuClicked;
	}
	public void setMenuClicked(boolean isMenuClicked){
		this.isMenuClicked = isMenuClicked;
	}

	private boolean isMyMenuClicked = false;
	public boolean isMyMenuClicked(){
		return isMyMenuClicked;
	}
	public void setMyMenuClicked(boolean isMyMenuClicked){
		this.isMyMenuClicked = isMyMenuClicked;
	}

	private MenuVO logoutMenu = null;
	public void addLogoutMenu(Object menuModel){
		if(logoutMenu == null){
			logoutMenu = new MenuVO();
			logoutMenu.setUrl(SessionUtil.getRequest().getContextPath() + PropertyManager.getString("defaultURL.logout", "/logout/logout.do"));
			String theme = SessionUtil.getPrimeFaceTheme();
			logoutMenu.setIcon("ui-icon-"+theme+" ui-icon-"+theme+"-menu");
			logoutMenu.setValue(MessageBundle.getBundle(UserVO.getUser().getLocale()).getString("COMMON_LOGOUT"));
			((DefaultMenuModel)menuModel).addElement(logoutMenu);
		} else {
			if(!((DefaultMenuModel)menuModel).getElements().contains(logoutMenu)){
				((DefaultMenuModel)menuModel).addElement(logoutMenu);
			}
		}
	}
	*/

	private boolean isChangedFirstMenu = false;
	public boolean isChangedFirstMenu(){
		return this.isChangedFirstMenu;
	}
	public void setChangedFirstMenu(boolean isChangedFirstMenu){
		this.isChangedFirstMenu = isChangedFirstMenu;
	}

	public boolean isCacheUpdated(){
		return CacheManager.menuUpdateTime() > this.getMenuUpdateTime();
	}

	private boolean isMyMenuUpdated = false;
	public boolean isMyMenuUpdated(){
		return isMyMenuUpdated;
	}

	@Override
	public boolean isAdminMenuSelected() {
		return UserVO.getUser().getUserMenuManager().getSelectedFirstMenu() == PropertyManager.getInt("home.topmenu.admin", 3);
	}


	public List<TreeNodeMap> getFirstMenuList(){
		List<TreeNodeMap> firstMenuMapList = new ArrayList<TreeNodeMap>();
		//TreeNodeMap selectedFirstMenu = this.getFirstSubMenu();
		//if(selectedFirstMenu != null){
			TreeNodeMap root = menuElementMap.get(1);
			for(TreeNodeMap firstMenuMap: root.getElements()){
				if(firstMenuMap.isSubNode()){
					firstMenuMapList.add(firstMenuMap);
				}
			}
		//}
		return firstMenuMapList;
	}

	@Override
	public void recursiveMakeUserMenu(TreeNodeMap root, Object rootMenu) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getLeftMenuModel() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getAllMenuModel() throws Exception {
		return menuElementMap.get(1);
		
	}

	@Override
	public Object getLeftMyMenuModel() throws Exception {
		return myMenuElementMap;
	}

	@Override
	public Object getLeftMobileMenuModel() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getLeftMobileMyMenuModel() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isMenuClicked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setMenuClicked(boolean isMenuClicked) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isMyMenuClicked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setMyMenuClicked(boolean isMyMenuClicked) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addLogoutMenu(Object menuModel) {
		// TODO Auto-generated method stub
		
	}
}
