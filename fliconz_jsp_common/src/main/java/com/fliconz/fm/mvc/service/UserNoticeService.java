package com.fliconz.fm.mvc.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class UserNoticeService extends  DefaultService {
   protected    String getNameSpace() {
	     return "com.fliconz.fm.usernotice.UserNotice";
	   }

			@Override
	public Map getInfo(Map param) throws Exception {
		this.updateByQueryKey("increase_FM_NOTICE_HITS", param);
		return super.getInfo(param);
	}

		@Override
		protected String getTableName() {
			return "FM_NOTICE";
		}
	    @Autowired
	     
		@Override
		public	String[] getPks() {
			return new String[]{"ID"};
		}
		
			

}
