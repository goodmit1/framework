package com.fliconz.fm.mvc;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.crypto.Cipher;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fm.security.FMAuthenticationSuccessHandler;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.security.SSOHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.security.filter.FMURLInterceptor;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

public class URLFilter extends FMURLInterceptor  implements Filter{

	List<String> internalIps;
	String[] ipCheckRoles = null;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
		String i = filterConfig.getInitParameter("access_ip");
		if(i==null) {
			i = System.getenv("WEB_ACCESS_IP");
		}
		if(i != null && !"".equals(i))
		{
			internalIps = TextHelper.splitList(i, "\n");
		}
		String t =  filterConfig.getInitParameter("ip_chk_role");
		if(t != null) {
			ipCheckRoles = t.split(",");
		}
		
		 

		 
	}
	protected boolean chkRole() {
		if(ipCheckRoles == null) {
			return true;
		}
		for(String r:ipCheckRoles) {
			if(UserVO.getUser() != null && UserVO.getUser().hasRole(r)) {
				return true;
			}
		}
		return false;
	}
	protected boolean ipcheck(HttpServletRequest request)
	{
		if(internalIps != null)
		{
			if(chkRole()) {
				String current_ip = FMSecurityContextHelper.getFirstRemoteAddr(request); 
				boolean isOK = false; 
				if(current_ip.equals("127.0.0.1") || current_ip.equals("0:0:0:0:0:0:0:1") )
				{
					isOK = true;
				}
				else
				{	
					for(String ip: internalIps){
						if(current_ip.matches(ip)){
							isOK = true;
							break;
						}
					}
				}
				if(!isOK)
				{
					System.out.println(current_ip + " access deny.");
					return false;
				}
			}
		}
		return true;
	}

 
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if(!ipcheck((HttpServletRequest)request))
		{
			((HttpServletResponse)response).sendError(401);
			return;
		}

		boolean isAjax = "XMLHttpRequest".equals(((HttpServletRequest)request).getHeader("X-Requested-With"));
		HttpSession session = ((HttpServletRequest)request).getSession();
		Object fromSSO=session.getAttribute("from_sso");
		if(fromSSO==null && !isAjax && UserVO.isLogin() )
		{
			int changeCycle = PropertyManager.getInt("password.security.changeCycle",0);
			
			Date pwdChgDate = UserVO.getUser().getPwdChangeTms();
			if(pwdChgDate == null || (changeCycle>0 && (new Date()).compareTo(DateUtil.addDate(pwdChgDate, Calendar.DATE, changeCycle))>=0))
			{
				request.setAttribute("__MSG__", MsgUtil.getMsg("msg_pwd_change_guide", new String[] { "" + changeCycle})); // "비밀번호가 reset되었거나 비밀번호 변경 후 " + changeCycle + "일이 지나면 비밀번호를 재설정해야 합니다.");
				 
				((HttpServletResponse)response).sendError(700);
				return;
			}
			
		}
		if(session.getAttribute(FMAuthenticationSuccessHandler.KEY_OTHER_LOGIN) != null)
		{
			session.invalidate();
			if(!isAjax)
			{
				((HttpServletResponse)response).sendRedirect("/login/login.jsp?fail=other");
			}
			else
			{
				response.setCharacterEncoding("utf-8");
				response.setContentType("text/json; charset=UTF-8");
				((HttpServletResponse)response).setStatus(701);
				JSONObject obj = new JSONObject(ControllerUtil.getErrMap(new Exception(MsgUtil.getMsg("msg_other_login", null)))); // 다른 곳에서 로그인 했습니다.
				
				
				response.getWriter().print(obj.toString());
			}
			return;
		}
		
		super.doPostHandle_main((HttpServletRequest)request, (HttpServletResponse)response);
		boolean fromApi  = this.loginByKey((HttpServletRequest)request, (HttpServletResponse)response);

		chain.doFilter(request, response);

		if(fromApi) {
			session.setMaxInactiveInterval(1);
		}
	}

	protected boolean loginByKey(HttpServletRequest request, HttpServletResponse response ) {
		//("Authorization", "Bearer " + token);
		String authStr = request.getHeader("Authorization");
		if(authStr == null) {
			return false;
		}
		else {
			if(UserVO.isLogin()) {
				return false;
			}
			int pos = authStr.lastIndexOf(" ");
			SSOHelper.ssoLogin4APIById(authStr.substring(pos+1));
			return true;
		}
	}
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
