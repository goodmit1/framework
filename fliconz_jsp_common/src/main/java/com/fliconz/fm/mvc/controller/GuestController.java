package com.fliconz.fm.mvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.security.SSOAuthFilter;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.FMAuthenticationSuccessHandler;
import com.fliconz.fm.security.SSOHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.security.password.PasswordService;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;


@RestController
@RequestMapping(value =  "/guest" )
public class GuestController  {
	
	@Autowired PasswordService service;
	
	@RequestMapping(value =  "/ping" )
	public Map ping() {
		return ControllerUtil.getSuccessMap(null, null);
	}
	@RequestMapping(value =  "/codeReload" )
	public Object codeReload() {
		PropertyManager.getInstance().init();
		CodeHandler code = (CodeHandler)SpringBeanUtil.getBean("codeHandler");
		try {
			code.reload();
			return ControllerUtil.getSuccessMap(null, null);
		} catch (Exception e) {
			 
			return ControllerUtil.getErrMap(e);
		}
		
	}
	@RequestMapping(value =  "/reqPwdInitEmail" )
	public Object reqPwdInitEmail(final HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			service.requestEmail(request.getParameter("userId"), request.getParameter("email"));
			return ControllerUtil.getSuccessMap(null, null);
		}
		catch(Exception e)
		{
			return ControllerUtil.getErrMap(e);
		}

		
	}	
	@RequestMapping(value =  "/login" )
	public Object apiLogin(final HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			Map param = ControllerUtil.getParam(request);
			SSOHelper.ssoLogin4API((String)param.get("id"), (String)param.get("token"),  request,   response);
			Map result = new HashMap();
			result.put("key", request.getSession().getId());
			return result;
		}
		catch(Exception e)
		{
			 
			response.setStatus(500);
			return ControllerUtil.getErrMap(e);
		}
	}
	@RequestMapping(value =  "/chgPwd" )
	public Object chgPwd(final HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			if(!TextHelper.isEmpty(request.getParameter("authToken")))
			{
				service.authenticationEmailLink(request.getParameter("authToken"), request.getParameter("password"), request.getParameter("password1"));
			}
			else if(UserVO.isLogin())
			{
				service.chgPassword(UserVO.getUser(), request.getParameter("password"), request.getParameter("password1"));
			}
			else
			{
				throw new Exception("잘못된 정보입니다.") ;
			}
			return ControllerUtil.getSuccessMap(null, null);
		}
		catch(Exception e)
		{
			return ControllerUtil.getErrMap(e);
		}

	}	
	@RequestMapping(value =  "/chkSSO" )
	public Object chkSSO(final HttpServletRequest request, HttpServletResponse response)
	{
		try {
			 
			return SSOAuthFilter.chkSSOPermission(request);
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
	}
}
