package com.fliconz.fm.mvc.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.service.MiscService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.util.EscapeUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@RestController

public class MiscController extends DefaultController{

	@Autowired MiscService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	
	@RequestMapping(value =  "/code_list" )
	public Object codeList(HttpServletRequest request, HttpServletResponse response)
	{
		return new ActionRunner(){

			@Override
			protected Object run(Map param) throws Exception {
				param.put("_ROLES_", UserVO.getUser().getRoleNames());
				param.put("_IS_ADMIN_", UserVO.getUser().isAdmin()?"Y":"N");
				param.put("_SITE_CD_", PropertyManager.getString("site.code", ""));
				return service.getCode((String)param.remove("grp"), param);
			}
			
		}.run(request);
		
	}
	@RequestMapping(value =  "/popup_list" )
	public Object popupList(HttpServletRequest request, HttpServletResponse response)
	{
		return new ActionRunner(){

			@Override
			protected Object run(Map param) throws Exception {
				return service.getPopupList( (String)param.remove("POPUP_ID"), param);
			}
			
		}.run(request);
		
	}
	
	@RequestMapping(value =  "/send_mail" )
	public Object send_mail(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			Map param  = ControllerUtil.getParam(request);
			
			String[] receiverMails = (String[])param.get("receiverMails");
			String contents = (String)param.get("CONTENTS");
			Map<String, Object> emailParam = new DataMap<String, Object>();
			
			emailParam.put("CONTENTS", EscapeUtil.unescape(contents));
			emailParam.put("TITLE", param.get("content_title"));
			emailParam.put("SENDER", UserVO.getUser().getUserName());
			
			service.sendMail(receiverMails, (String)param.get("email_title"), (String)param.get("email_template"), "ko", emailParam);
			return ControllerUtil.getSuccessMap(param);
		
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
		
	}
	
}

