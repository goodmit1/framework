package com.fliconz.fm.mvc.security;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.buffer.CircularFifoBuffer;
import org.json.JSONObject;

import com.fliconz.fm.common.vo.TreeNodeMap;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.FMAuthenticationSuccessHandler;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.util.RequestHelper;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class SSOAuthFilter  implements Filter {

	String authServer;
	public String getAuthServer() {
		return authServer;
	}

	public void setAuthServer(String authServer) {
		this.authServer = authServer;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		authServer = filterConfig.getInitParameter("auth-server");
		if(authServer == null)
		{
			throw new ServletException ("SSOAuthFilter must have auth-server parameter");
		}
	}
	public static String getSSOUrl4Sender(HttpServletRequest request, String url, int menuId ) throws Exception
	{
		 
		Collection sendSSO = (Collection )request.getSession().getAttribute("sendSSO");
		if(sendSSO == null)
		{
			sendSSO = new CircularFifoBuffer(10);
			request.getSession().setAttribute("sendSSO",sendSSO);
		}
		int pos = url.lastIndexOf("?");
		if(pos<0) pos = url.length();
		String cmd = "";
		if(!FMSecurityContextHelper.checkMethodPermissionByMenuId("update", menuId))
		{
			cmd += "update";
		}
		if(!FMSecurityContextHelper.checkMethodPermissionByMenuId("delete", menuId))
		{
			cmd += "delete";
		}
		String ssoParam = "_mid_=" +menuId + "&_uid_=" + UserVO.getUser().getUserId() + "&_cmd_=" + cmd ;
		if(url.indexOf("?")>0) url =  url +"&" + ssoParam;
		else url =  url +"?" + ssoParam;
		sendSSO.add(url);
		
		return url +  "&_sid_=" + request.getSession().getId() ;
		
		
	}
	public static UserVO chkSSOPermission(HttpSession session , String sessionid, String userid,  String url) throws Exception
	{
		
		 
		if(session != null && session.getId().equals(sessionid))
		{
			Collection sendSSO = (Collection )session.getAttribute("sendSSO");
			if(sendSSO == null) throw new Exception("권한이 없습니다.0");
			int pos = url.lastIndexOf("&_sid_=");
			if(pos<0) throw new Exception("권한이 없습니다.1");
			if( sendSSO.contains(url.substring(0, pos) ))
			{
				UserVO user = (UserVO)session.getAttribute(UserVO.class.getName());
				if(user==null)  
				{
					throw new Exception("권한이 없습니다.2");
				}
				 
				return user;
			}
			else
			{
				throw new Exception("권한이 없습니다.4");
			}
		}
		else
		{
			throw new Exception("권한이 없습니다.1");
		}
	}
	public static Map chkSSOPermission(HttpServletRequest request) throws Exception
	{
		FMAuthenticationSuccessHandler fmAuthenticationSuccessHandler = (FMAuthenticationSuccessHandler)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationSuccessHandler", "fmAuthenticationSuccessHandler"));
		
		String url = request.getHeader("Referer") ;
		 
		String sessionid = request.getParameter("_sid_");
		String userid = request.getParameter("_uid_");
		 
		HttpSession session = fmAuthenticationSuccessHandler.getSession(userid);
		UserVO user  = chkSSOPermission(session,   sessionid,   userid,      url) ;
		Map result = new HashMap();
		result.put("LOGIN_ID", user.getLoginId());
		result.put("USER_NAME", user.getUserName());
		if(user.getTeam() != null)
		{
			result.put("TEAM_CD", user.getTeam().getTeamCd());
			result.put("TEAM_NM", user.getTeam().getTeamNm());
		}
		return result;
		 
	}
	 
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String id = request.getParameter("_uid_");
		String sessionId = request.getParameter("_sid_");
		 
		HttpServletRequest request1= ((HttpServletRequest)request) ;
		
		
		Set map = (Set)((HttpServletRequest)request).getSession().getAttribute("_authSet_");
		String queryString = ((HttpServletRequest)request).getQueryString();
		int pos = queryString.indexOf("&_sid_=");
		int pos1 = queryString.indexOf("&", pos+1);
		if(pos1<0) pos1 = queryString.length();
		
		queryString = queryString.substring(0, pos1);
		
		String currentUri = request1.getProtocol().startsWith("HTTPS") ? "https":"http" + "://" + request1.getServerName() +  (request1.getServerPort()==80? "" : ":" + request1.getServerPort()) +   request1.getRequestURI() + "?" + queryString;
		if(map != null && map.contains(queryString))
		{
			if(chain != null) chain.doFilter(request, response);
		}
		else
		{
			Map header = new HashMap();
			Map data = new HashMap();
			data.put("_uid_", id);
			data.put("_sid_", sessionId);
			 
			header.put("Referer", currentUri);
			try {
				JSONObject result = RequestHelper.postRequest(authServer, header, data);
				if(result != null && !result.has("error"))
				{
					if(map == null)
					{
						map = new HashSet();
						((HttpServletRequest)request).getSession().setAttribute("_authSet_",map);
					}
					map.add(queryString);
					if(chain != null) chain.doFilter(request, response);
				}
				else
				{
					if(result.has("error"))
					{
						throw new  ServletException(result.getString("error"));
					}
					else
					{
						throw new  ServletException("response is null");
					}
				}
			} catch (Exception e) {
				 
				e.printStackTrace();
				throw new ServletException(e);
			}
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
