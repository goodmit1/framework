package com.fliconz.fm.mvc.tag;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.fliconz.fw.runtime.util.PropertyManager;

/**
 * @author Jacob Hookom
 */
public class Include4Site extends SimpleTagSupport  {

  
	String page;
	String siteCode;
	public Include4Site()
	{
		siteCode = PropertyManager.getString("site.code");
	}
   
    public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	@Override 
	public void doTag() throws JspException, IOException 
	{
		PageContext pageContext = (PageContext) getJspContext();
		if(this.siteCode==null)
		{
			throw new IOException("site code not found");
		}
		try
		{
			
			
			int pos = this.page.lastIndexOf("/");
			String path = this.page;
			if(pos<0)
			{
				String currentPage = ((HttpServletRequest)pageContext.getRequest()).getServletPath();
				int pos2 = currentPage.lastIndexOf("/");
						
				 path =	currentPage.substring(0, pos2+1)	+ this.siteCode + "/" + this.page;
			}
			else
			{
				path = this.page.substring(0, pos) + "/" + this.siteCode + this.page.substring(pos);
			}
			File realPath = new File( pageContext.getRequest().getRealPath(path));
			if(realPath.exists())
			{
				pageContext.include(path, false);
			}
			else
			{
				pageContext.include(this.page, false);
			}
		}
		catch(ServletException ignore)
		{
			throw new JspException( ignore);
		}
	}
     
}
