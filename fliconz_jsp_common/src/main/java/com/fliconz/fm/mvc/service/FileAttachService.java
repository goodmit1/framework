package com.fliconz.fm.mvc.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class FileAttachService extends  DefaultService {
	   protected    String getNameSpace() {
		     return "com.fliconz.fm.fileAttach";
		   }

	@Override
	protected String getTableName() {
		return "FM_FILE_ATTACH";
	}

	@Override
	public String[] getPks() {
		return new String[]{"TABLE_NM", "ID", "SEQ"};
	}

	public void insert(String tableName, Object id, List<MultipartFile> files) throws Exception {
		Map param = new HashMap();
		param.put("TABLE_NM", tableName);
		param.put("ID", id);
		int idx = 1;
		for(MultipartFile file : files)
		{
			param.put("SEQ", idx);
			param.put("FILE_NM", file.getOriginalFilename());
			param.put("FILE_SIZE", file.getSize());
			param.put("FILE_CONTENT", file.getBytes());
			this.insertDBTable(getTableName(), param);
			idx++;
		}
		
	}


}
