package com.fliconz.fm.mvc.service;

import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class DashboardMngService  extends  DefaultService{

	@Override
	protected String getTableName() {
		 
		return "FM_DASHBOARD";
	}

	@Override
	public String[] getPks() {
		 
		return new String[]{"ITEM_ID"};
	}

	@Override
	protected String getNameSpace() {
		 
		return "com.fliconz.fm.dashboard_mng";
	}

}
