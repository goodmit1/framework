package com.fliconz.fm.mvc;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;

public class JsDecryptRequestWrapper  extends HttpServletRequestWrapper {

	Set<String> jsEncryptParam;
	public JsDecryptRequestWrapper(HttpServletRequest request,Set<String> jsEncryptParam) {
		super(request);
		this.jsEncryptParam = jsEncryptParam;
	}
	@Override
	public String getParameter(String key)
	{
		String[] arr = getParameterValues(key) ;
		if(arr != null) return arr[0];
		return null;
	}
	@Override
	public String[] getParameterValues(String key)
	{
		boolean isJsDecrypt=false;
		if(jsEncryptParam != null &&  jsEncryptParam.contains(key))
		{
			isJsDecrypt = true;
		}
		String [] values = super.getParameterValues(key);
		if(isJsDecrypt && values != null)
		{
			try {
				for(int i=0; i < values.length; i++)
				{
					values[i] =  JsDecryptFilter.decryptRsa( (HttpServletRequest)super.getRequest(),values[i] );
				}
				return values;
			} catch (Exception e) {
				 
				e.printStackTrace();
				return new String[]{"에러"};
			}
			 
		}
		return values;
	}
}
