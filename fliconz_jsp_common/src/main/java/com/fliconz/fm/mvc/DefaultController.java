package com.fliconz.fm.mvc;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fliconz.fm.common.core.RequestParamInterceptor;
import com.fliconz.fm.common.util.DataKey;
import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.mvc.service.FileAttachService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.mvc.util.ExcelExportUtil;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.NumberUtil;

public abstract class DefaultController {

	public final static String MNV_JSON_VIEW = "jsonView";

	public final static String MNV_EXCEL_VIEW = "excelView";
	
	public static final String MNV_JSONP_VIEW = "jsonpView";
	
	protected abstract DefaultService getService();
	public static final String SELECTED_JSON_FILED="selected_json";
	@RequestMapping(value =  "/list_excel" )
	public void excelExport(final HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		String fileName= request.getParameter("fileName");
		
		JSONArray colDef = new JSONArray(request.getParameter("colDef"));
		ExcelExportUtil util = new ExcelExportUtil(fileName, colDef );
		Map param = ControllerUtil.getParam(request);
		util.export(getService().list(param), request, response);
	}
	@RequestMapping(value =  "/list_csv" )
	public void csvExport(final HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		String fileName= request.getParameter("fileName");
		
		JSONArray colDef = new JSONArray(request.getParameter("colDef"));
		ExcelExportUtil util = new ExcelExportUtil(fileName, colDef );
		Map param = ControllerUtil.getParam(request);
		util.exportCSV(getService().list(param), request, response);
	}
	@RequestMapping(value =  "/list/{queryKey}/" )
	public Object list(final HttpServletRequest request, HttpServletResponse response,@PathVariable("queryKey") final String queryKey)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return getService().list(queryKey, param);
				 
			}

		}).run(request);
			 
	}
	@RequestMapping(value =  "/info/{queryKey}/" )
	public Object info(final HttpServletRequest request, HttpServletResponse response,@PathVariable("queryKey") final String queryKey)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return getService().getInfo(queryKey, param);
				 
			}

		}).run(request);
			 
	}
	@RequestMapping(value =  "/list" )
	public Object list(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				int pageSize = NumberUtil.getInt(param.get("pageSize"),0) ;
				if(pageSize != 0)
				{
					param.put("pageSize", pageSize);
					param.put("first", pageSize * (NumberUtil.getInt(param.get("pageNo"),0)-1) );
				}
				List list = getService().list(param);
				result.put("list", list);
				if(pageSize  != 0)
				{
					result.put("total", getService().list_count(param));
				}
				else
				{
					result.put("total", list.size());
				}
				return result;
			}

		}).run(request);

	}
	@RequestMapping(value =  "/info" )
	public Object info(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return getService().getInfo(param);
			}

		}).run(request);

	}
	@RequestMapping(value =  "/delete" )
	public Object delete(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				getService().delete(param);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}

		}).run(request);

	}
	@RequestMapping(value =  "/delete_multi" )
	public Object deleteMulti(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				String jsonArrStr = (String)param.get("selected_json");
				if(jsonArrStr == null || "".equals(jsonArrStr)) throw new Exception("NO DATA");
				
				getService().deleteMulti(ListUtil.makeJson2List(jsonArrStr));
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}

		}).run(request);

	}
	@RequestMapping(value =  "/save_multi" )
	public Object saveMulti(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				String jsonArrStr = (String)param.get("selected_json");
				if(jsonArrStr == null || "".equals(jsonArrStr)) throw new Exception("NO DATA");
				
				getService().insertOrUpdateMulti(ListUtil.makeJson2List(jsonArrStr));
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}

		}).run(request);

	}
	 
	@RequestMapping(value =  "/save" )
	public Object save(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				getService().insertOrUpdate(param);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}

			

		}).run(request);

	}

	@Autowired FileAttachService fileAttachService;
	
	@RequestMapping(value =  "/save_fileAttach/{file_id}" )
	public Object saveFiles(@PathVariable("file_id") final String file_id, final MultipartHttpServletRequest  mrequest,final HttpServletRequest  request, HttpServletResponse response)
	{
		final List<MultipartFile> files = mrequest.getFiles(file_id);
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				
				getService().insertOrUpdate(param);
				if(files != null && files.size()>0)
				{
					fileAttachService.insert(getService().getTableName(), param.get(getService().getPks()[0]), files);
				}
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}

			

		}).run(request);

	}
	@RequestMapping(value =  "/delete_fileAttach/{id}/{seq}" )
	public Object deleteFileAttach(final HttpServletRequest request, HttpServletResponse response,@PathVariable("id") final String id, @PathVariable("seq") final String seq)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				
				param.put("TABLE_NM", getService().getTableName());
				param.put("ID", id);
				param.put("SEQ", seq);
				fileAttachService.delete(param);
				return ControllerUtil.getSuccessMap(param);
				 
			}

			

		}).run(request);
	}
	@RequestMapping(value =  "/list_fileAttach/{id}" )
	public Object listFileAttach(final HttpServletRequest request, HttpServletResponse response,@PathVariable("id") final String id )
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				
				param.put("TABLE_NM", getService().getTableName());
				param.put("ID", id);
				 
				return fileAttachService.list(param);
			}
		}).run(request);
	}
	@RequestMapping(value =  "/get_fileAttach/{id}/{seq}" )
	public void downloadFileAttach(final HttpServletRequest request, HttpServletResponse response,@PathVariable("id") final String id, @PathVariable("seq") final String seq)
	{
		Map param = new HashMap();
		param.put("TABLE_NM", getService().getTableName());
		param.put("ID", id);
		param.put("SEQ", seq);
		try {
			Map info = fileAttachService.getInfo(param);
			response.setContentType("application/octet-stream");
			response.setContentLength(NumberUtil.getInt(info.get("FILE_SIZE")));
			String userAgent = request.getHeader("User-Agent");

			boolean ie = userAgent.indexOf("MSIE") > -1;
			String fileName = null;
			String realName = (String)info.get("FILE_NM");
			// 실제 파일이름
			if(ie)
			{
				fileName = URLEncoder.encode(realName, "utf-8");
			} 
			else
			{
				fileName = new String(realName.getBytes("utf-8"),"iso-8859-1");
			}
			response.setHeader("Content-Disposition", "attachment;filename=\""+ fileName+"\";");
			response.getOutputStream().write((byte[])info.get("FILE_CONTENT"));
			response.getOutputStream().flush();
		} catch (Exception e) {
			 
			e.printStackTrace();
			
		}
	}
	
	 
	@RequestMapping(value =  "/save_move" )
	public Object move(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				getService().updateByQueryKey(  "chg_order", param);
				getService().updateByQueryKey(  "chg_parent", param);
				return ControllerUtil.getSuccessMap(param, "id");
			}

		}).run(request);
	 
		
	}
	
	public final JSONObject makeJsonResponse(String resCode, String resMsg, Object resData)
	{
		try
		{
			//DataMap<String, Object> reqParam = (DataMap<String, Object>) resData;
			if (resData != null && resData instanceof java.util.Map)
			{
				java.util.Map mm = ((java.util.Map)resData);
				if (mm.containsValue(mm))
				{
					java.util.List<Object> keyList = new LinkedList<Object>();
					for (java.util.Iterator it = mm.keySet().iterator(); it.hasNext();)
					{
						Object key = it.next();
						Object value = mm.get(key);
						if (value == mm)
						{
							keyList.add(key);
						}
					}

					for (Object key : keyList)
					{
						mm.remove(key);
					}
				}
			}
			//reqParam.remove("_delivery_info_");
			org.json.JSONObject resJSON = new org.json.JSONObject();
			resJSON.put(DataKey.KEY_RESPONSE_CODE, resCode);
			resJSON.put(DataKey.KEY_RESPONSE_MESSAGE, resMsg);
			if (resData != null) resJSON.put(DataKey.KEY_RESPONSE_DATA, resData);

			return resJSON;
		}
		catch (org.json.JSONException e)
		{
			throw new RuntimeException(e);
		}
	}

	public final org.json.JSONObject makeJsonResponse(String resCode, String resMsg)
	{
		return makeJsonResponse(resCode, resMsg, null);
	}
}
