package com.fliconz.fm.mvc;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.fliconz.fm.common.BaseService;
import com.fliconz.fm.common.CodeHandler;

public abstract class DefaultService extends BaseService{
	protected abstract String getTableName();
	public static final String IU_FILED="IU";
	public abstract String[] getPks() ;
	
	
	@Autowired CodeHandler codeHandler;
	
	protected void setPKVal(Map param) throws Exception
	{
		
	}
	
	
	protected Map<String,String> getCodeConfig()
	{
		return null;
	}
	protected Map getCodeMap(String grp, String langKnd){
		return codeHandler.getCodeList(grp, langKnd);
	}
	public int insert(Map param) throws Exception
	{
		this.setPKVal(param);
		return this.insertDBTable(this.getTableName(), param);
	}
	public int update(Map param) throws Exception
	{
		return this.updateDBTable(this.getTableName(), param);
	}
	public int delete(Map param) throws Exception
	{
		return this.deleteDBTable(this.getTableName(), param);
	}
	public int deleteMulti(List<Map> params) throws Exception
	{
		int row = 0;
	
		for(Map param : params)
		{
			if("I".equals(param.get(IU_FILED))) continue;
			row += this.delete(param);
		}
		return row;
	}
	public int insertOrUpdateMulti(List<Map> params) throws Exception
	{
		int row = 0;
	
		for(Map param : params)
		{
			if("I".equals(param.get(IU_FILED)))
			{
				row += this.insert(param);
			}
			else if("U".equals(param.get(IU_FILED)))
			{
				row += this.update(param);
			}
		}
		return row;
	}
	public void addCodeNames(Map m, Map<String,String> codeConfig )
	{
		if(codeConfig == null || m==null) return;
		for(String code : codeConfig.keySet())
		{
			if(m.get(code)!= null)
			{
				Map codeMap = codeHandler.getCodeList(codeConfig.get(code), this.getLang());
				if(codeMap != null && codeMap.get(m.get(code)) != null)
				{	
					
					m.put(code + "_NM",codeMap.get(m.get(code).toString()));
				}
				else
				{
					//System.out.println(m.get(code) + ":" + this.getLang() + ":" + codeConfig.get(code));
				}
			}
		}
	}
	public void addCodeNames(List<Map> list)
	{
		Map<String, String> codeConfig = this.getCodeConfig();
		if(codeConfig != null && codeConfig.size()>0)
		{
			for(Map m : list)
			{
				addCodeNames(m, codeConfig);
			}
		}
	}
	
	 
	protected Map removeEmpty(Map<Object,Object> param){
		Iterator it = param.keySet().iterator();
		while(it.hasNext())
		{
			Object key = it.next();
			if("".equals(param.get(key)))
			{
				param.put(key,null);
			}
		}
		return param;
		
	}
	public List list(Map searchParam) throws Exception
	{
		List<Map> list= this.selectList(getTableName(), removeEmpty(searchParam));
		addCodeNames(list);
		
		return list;
	}
	public List list(String queryKey, Map searchParam) throws Exception
	{
		List<Map> list= this.selectListByQueryKey(queryKey, searchParam);
		addCodeNames(list);
		
		return list;
	}
	public Map getInfo(String queryKey, Map  param) throws Exception
	{
		Map info = this.selectOneByQueryKey(queryKey, param);
		this.addCodeNames(info, this.getCodeConfig());
		return info;
	}
	public int list_count(Map searchParam) throws Exception
	{
		return super.selectListCount(this.getTableName(), removeEmpty(searchParam));
	}
	public Map getInfo(Map  param) throws Exception
	{
		Map info = super.selectInfo(this.getTableName(), param);
		this.addCodeNames(info, this.getCodeConfig());
		return info;
	}
	private boolean hasPKField(Map param)
	{
		for(String pk : this.getPks())
		{
			if(param.get(pk) == null || "".equals(param.get(pk)))
			{
				return false;
			}
		}
		return true;
	}
	public int insertOrUpdate(Map param) throws Exception{
		int row = 0;
		if(hasPKField(param))
		{
			row = this.update(param);
		}
		if(row==0)
		{
			row =  this.insert(param);
		}
		return row;
	}

}
