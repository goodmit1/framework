package com.fliconz.fm.mvc.service;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.security.util.ICrypt;
import com.fliconz.fm.util.IMailSender;
import com.fliconz.fw.runtime.util.PropertyManager;
@Service
public class MiscService extends DefaultService{

	@Autowired CodeHandler codeHandler;
	@Autowired
	ICrypt crypt;
	
	@Autowired
	IMailSender mailSender;
	org.apache.log4j.Logger maillog = LogManager.getLogger("mail");
	
	Set<String> availableNs;
	@Override
	protected String getNameSpace() {
		return "";
	}
	
	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@PostConstruct
	public void init()
	{
		availableNs=new HashSet();
		String[] arr = PropertyManager.getStringArray("code_available_ns" , new String[]{"popup","com.clovirsm.common.Component"});
		for(String arr1:arr)
		{
			availableNs.add(arr1);
		}
	}
	public Map getPopupList(String popupId,  Map searchParam) throws Exception
	{
		String d =  (String)searchParam.get("default_search_field");
		if(d != null)
		{
			searchParam.put("default_search_field", d.split(","));
		}
		
		processEncrypt(searchParam);
		Map result=new HashMap();
		List list = super.selectListByQueryKey("popup","list_popup_" + popupId, searchParam);
		result.put("list", list);
		if(searchParam.get("pageSize") != null)
		{
			result.put("total", super.selectOneObjectByQueryKey("popup","list_count_popup_" + popupId, searchParam));
		}
		else
		{
			result.put("total",  list.size());
		}
		return result ;
	}
	
	private  void processEncrypt(Map searchParam) throws Exception
	{
		
		String enField = (String)searchParam.get("encrypt_field");
		if(enField != null)
		{
			String[] arr = enField.split(",");
			for(String a : arr)
			{
				String v = (String) searchParam.get(a);
				if(v != null && !v.equals(""))
				{
					searchParam.put(a + "_encrypt", crypt.encrypt(v));
				}
			}
		}
	}
	protected boolean isAvailable(String key)
	{
		int pos = key.lastIndexOf(".");
		String ns = key.substring(0,pos);
		return availableNs.contains(ns);
	}
	public Object getCode(String grp, Map param) throws Exception
	{
		if(grp.startsWith("db."))
		{
			int pos = grp.indexOf(".");
			this.addCommonParam(param);
			String key = grp.substring(pos+1);
			if(!isAvailable(key)) 
			{
				throw  new Exception("not available");
			}
			return super.getDAO().getListMap(key  , param);
			
		}
		else if(grp.startsWith("dblist."))
		{
			int pos = grp.indexOf(".");
			this.addCommonParam(param);
			String key = grp.substring(pos+1);
			if(!isAvailable(key)) 
			{
				throw  new Exception("not available");
			}
			return super.getDAO().selectList(key, param);
			
		}
		else
		{
			return  codeHandler.getCodeList(grp, getLang());
			
		}
	}
	
	public void sendMail(String toEmail, String title, String template,  String lang, Map paramMap)  {
		try {
			mailSender.send(toEmail ,  title,template, lang, paramMap);
			maillog.info((new Date()) + "----- mail send " + toEmail  + "  " +     title);
		}
		catch(Exception e){
			maillog.error((new Date()) + "----- mail send error " +   toEmail  + "  " +  title + ":" + e.getMessage());
		}
	}

	public void sendMail(String[] toEmails, String title, String template,  String lang, Map paramMap)  {
		try {
			mailSender.send(toEmails ,  title,template, lang, paramMap);
			maillog.info((new Date()) + "----- mail send " + toEmails  + "  " +     title);
		}
		catch(Exception e){
			e.printStackTrace();
			maillog.error((new Date()) + "----- mail send error " +   toEmails  + "  " +  title + ":" + e.getMessage());
		}
	}

}
