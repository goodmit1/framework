package com.fliconz.fm.mvc.util;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.MessageSource;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.mvc.service.MiscService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class MsgUtil {

	public static String getMsgWithDefault(String key, String[] args,  String defaultStr)
	{
		try
		{
			MessageSource messageSource = (MessageSource)SpringBeanUtil.getBean("messageSource");
			return messageSource.getMessage(key, args, new Locale( UserVO.getUser().getLangKnd()));
		}
		catch(Exception ignore)
		{
			System.out.println(key + " message not found");
			return defaultStr;
		}
	}
	public static String getMsg(String key, String[] args, String language)
	{
		MessageSource messageSource = (MessageSource)SpringBeanUtil.getBean("messageSource");
		return messageSource.getMessage(key, args, new Locale(language));
	}
	public static String getMsg(String key, String[] args)
	{
		String lang = "ko";
		try
		{
			lang = UserVO.getUser().getLangKnd();
		}
		catch(Exception ignore)
		{
			
		}
		return getMsg(key, args, lang);
		
	}
	
	public static String getCodeJsonArr(String grp, Map param) throws Exception
	{
		
		MiscService service = (MiscService)SpringBeanUtil.getBean("miscService");
		Object o = service.getCode(grp, param);
		if(o != null) {
			if( o instanceof Map){
				Map<String,Object> map = (Map)o;
			 	 
				JSONArray json  = new JSONArray( );
				for(String key:map.keySet()) {
					json.put( (new JSONObject()).put("name", key).put("value", map.get(key)));
				}
				return json.toString();
			}
			else {
				JSONArray json  = new JSONArray( (List)o);
				return json.toString();
			}
		}
		else
		{
			throw new Exception("code(" + grp + "," + UserVO.getUser().getLangKnd() +") not found");
		}
	}
	public static String getCodeJson(String grp) throws Exception
	{
		CodeHandler codeHandler = (CodeHandler)SpringBeanUtil.getBean("codeHandler");
		Map map = codeHandler.getCodeList(grp, UserVO.getUser().getLangKnd());
		if(map != null)
		{
			JSONObject json  = new JSONObject(map);
			return json.toString();
		}
		else
		{
			throw new Exception("code(" + grp + "," + UserVO.getUser().getLangKnd() +") not found");
		}
	}
}
