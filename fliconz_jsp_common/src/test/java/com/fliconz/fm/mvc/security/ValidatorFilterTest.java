package com.fliconz.fm.mvc.security;

import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.junit.Assert;
import org.junit.Before;

import org.junit.BeforeClass;
import org.junit.Test;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;


import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;
public class ValidatorFilterTest {
    HttpServletRequest request;
    HttpServletResponse response;
    ValidatorFilter filter;
    ReloadableResourceBundleMessageSource messageSource;

@BeforeClass
    public static void init(){
        mockStatic(SpringBeanUtil.class);
    }

    @Before
    public  void  setup() throws ServletException {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        filter = new ValidatorFilter();
        filter.setPaths(new String[]{"/schemas/default", "/schemas/site"});


        messageSource =  new ReloadableResourceBundleMessageSource();
        messageSource.setDefaultEncoding("utf-8");
        messageSource.setBasename("classpath:com/fliconz/fm/common/message");


        when(SpringBeanUtil.getBean("messageSource")).thenReturn(messageSource);
        when(request.getRequestURI()).thenReturn("/api/user/save");
    }
    @Test
    public void filterDefinitions(){
        when(request.getRequestURI()).thenReturn("/api/approve/deny");
        when(request.getParameter("APPR_COMMENT")).thenReturn("1");


        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("이상")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }

        Assert.fail("에러 발생 안 함.");

    }

    @Test
    public void filterMax(){
        when(request.getRequestURI()).thenReturn("/api/vm/save");
        when(request.getParameter("CPU")).thenReturn("1000");
        

        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("작거나")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }

        Assert.fail("에러 발생 안 함.");

    }
    @Test
    public void filterMin(){
        when(request.getRequestURI()).thenReturn("/api/vm/save");
        when(request.getParameter("CPU")).thenReturn("-1");

        when(request.getParameter("id")).thenReturn("1");

        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("크거나")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }
        Assert.fail("에러 발생 안 함.");

    }
    @Test
    public void testRef(){
        // default는 minLength가 2이만 site는 minLength가 3이라서 에러 발생해야 함.
        when(request.getRequestURI()).thenReturn("/api/vm/insertReq");
        when(request.getParameter("VM_NM")).thenReturn("me");
        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("이상")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }

        Assert.fail("에러 발생 안 함.");
    }
    @Test
    public void chkSiteOverride(){
    // default는 minLength가 2이만 site는 minLength가 3이라서 에러 발생해야 함.
        when(request.getRequestURI()).thenReturn("/api/vm/save");
        when(request.getParameter("VM_NM")).thenReturn("me");
        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("이상")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }

        Assert.fail("에러 발생 안 함.");
    }
    @Test
    public void filterMaxLength(){
        when(request.getRequestURI()).thenReturn("/api/user/save");
        when(request.getParameter("name")).thenReturn("me");
        when(request.getParameter("id")).thenReturn("123454654634563565463546");

        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("이하")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }

        Assert.fail("에러 발생 안 함.");

    }
    @Test
    public void filterMinLength(){
        when(request.getRequestURI()).thenReturn("/api/user/save");
        when(request.getParameter("name")).thenReturn("me");

        when(request.getParameter("id")).thenReturn("1");

        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("이상")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }
        Assert.fail("에러 발생 안 함.");

    }
    @Test
    public void notCheck(){
        when(request.getRequestURI()).thenReturn("/api/test/save");
        when(request.getParameter("name")).thenReturn("me");
        when(request.getParameter("id")).thenReturn("id");
        when(request.getParameter("selected_json")).thenReturn("");
        Assert.assertTrue("체크 안 해야 함", true);

    }
    @Test
    public void filterType()   {

        when(request.getParameter("name")).thenReturn("me");
        when(request.getParameter("id")).thenReturn("id");
        when(request.getParameter("selected_json")).thenReturn("");
        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("number")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }
        Assert.fail("에러 발생 안 함.");

    }
    @Test
    public void filterRequired()   {


        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("email")).thenReturn("syk@naver.com");
        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("입력해 주세요")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }
        Assert.fail("에러 발생 안 함.");

    }
    @Test
    public void filterPattern()   {


        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("name")).thenReturn("1");
        when(request.getParameter("email")).thenReturn("syk");
        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("잘못된 형식")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }
        Assert.fail("에러 발생 안 함.");

    }
    @Test
    public void filterObject()   {
        when(request.getParameter("name")).thenReturn("1");
        String jsonStr = "[{ \"DS_NM\":\"ds_mgmt3\",\"DISK_TYPE_ID\":\"a\",\"IU\":\"U\"},{\"DS_NM\":\"ds_mgmt2\",\"DC_ID\":\"1707555219069200\",\"DISK_TYPE_ID\":\"1\",\"IU\":\"I\"}]";
        when(request.getParameter("selected_json")).thenReturn(jsonStr);
        try {
            filter.doFilter(request, response, null);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("입력해 주세요")>=0);
            Assert.assertTrue(e.getMessage(), e.getMessage().indexOf("불일치")>=0);
            return;
        }
        catch(ServletException e1){
            Assert.fail(e1.getMessage());
            return;
        }
        Assert.fail("에러 발생 안 함.");
    }

}
