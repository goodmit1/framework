package com.fliconz.fm.admin.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class MultilangPopupService extends DefaultService {

	protected String getNameSpace() {
		return "com.fm.admin.popup.Multilang";
	}

	@Override
	public String[] getPks() {
		return new String[]{"TABLE_NM","FIELD_NM","ID","LANG_TYPE"};
	}

	@Override
	protected String getTableName() {
		return "FM_NL";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		return config;
	}

}