package com.fliconz.fm.admin.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.mvc.DefaultService;

@Service
public class FmDdicService extends  DefaultService {
	@Autowired CodeHandler codeHandler;
   protected    String getNameSpace() {
     return "com.fliconz.fm.admin.ddic.Ddic";
   }

	@Override
	protected String getTableName() {
		return "FM_DDIC";
	}
	@Override
	public	String[] getPks() {
		return new String[]{"DD_ID","DD_VALUE"};
	}

	@Override
	public int insertOrUpdateMulti(List<Map> params) throws Exception {
		 
		int row =  super.insertOrUpdateMulti(params);
		codeHandler.reload();
		return row;
	}

	 

}
