package com.fliconz.fm.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.LogPrgmService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.mvc.util.ExcelExportUtil;
import com.fliconz.fw.runtime.util.NumberUtil;

@RestController
@RequestMapping(value =  "/log_prgm" )
public class LogPrgmController extends DefaultController{

	@Autowired LogPrgmService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	@RequestMapping(value =  "/list_excel" )
	public void excelExport(final HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		String fileName= request.getParameter("fileName");
		
		JSONArray colDef = new JSONArray(request.getParameter("colDef"));
		ExcelExportUtil util = new ExcelExportUtil(fileName, colDef );
		Map param = ControllerUtil.getParam(request);
		util.export(getService().selectListByQueryKey("list_FM_CONNECTLOG_ALL", param), request, response);
	}
	@RequestMapping(value =  "/list_sub" )
	public Map listSub(final HttpServletRequest request, HttpServletResponse response)
	{
		return (Map)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				List list = getService().selectList("FM_CONNECTLOG",param);
				result.put("list", list);
				result.put("total", list.size());
				
				return result;
			}

		}).run(request);

	}
}
