package com.fliconz.fm.admin.service;

 
	import java.util.HashMap;
	import java.util.Map;

	import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.security.UserVO;

	 
	@Service
	public class QnaService extends  DefaultService {

		protected String getNameSpace() {
			return "com.fliconz.fm.admin.qna";
		}

		@Override
		protected String getTableName() {

			return "FM_INQUIRY";
		}
		@Override
		protected int updateDBTable(String tableNm, Map param) throws Exception {
			Map info = (Map)this.selectInfo(this.getTableName(), param);
			if(UserVO.getUser().isAdmin() || UserVO.getUser().getUserId().toString().equals(info.get("INS_ID").toString())) {
				return super.updateDBTable(tableNm, param);
			}
			else {
				throw new Exception("Invalid right");
			}
		}
		@Override
		protected int deleteDBTable(String tableNm, Map param) throws Exception {
			Map info = (Map)this.selectInfo(this.getTableName(), param);
			if(UserVO.getUser().isAdmin() || UserVO.getUser().getUserId().toString().equals(info.get("INS_ID").toString())) {
				return super.deleteDBTable(tableNm, param);
			}
			else {
				throw new Exception("Invalid right");
			}
		}
		@Override
		protected int insertDBTable(String tableNm, Map param) throws Exception {
			if(param.get("INQ_ID") == null){
				param.put("INQ_ID", System.nanoTime());
			}
			return super.insertDBTable(tableNm, param);
		}

		@Override
		public String[] getPks() {
			return new String[]{"INQ_ID"};
		}

		@Override
		protected Map<String, String> getCodeConfig() {
			Map conf = new HashMap();
			conf.put("SVC_CD", "SVC");
			conf.put("INQ_CD", "INQ");
			return conf;
		}

	 

}
