package com.fliconz.fm.admin.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class UserPrgmService extends  DefaultService {
	protected    String getNameSpace() { 
	    return "com.fliconz.fm.admin.userPrgm"; 
	  }

	@Override
	protected String getTableName() {
		 
		return "FM_USER_PRGM";
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return new String[]{"PRGM_ID","USER_ID","ROLE_ID"};
	}

	@Override
	protected void addCommonParam(Map param) {
		 
		super.addCommonParam(param);
		if(param.get("ROLE_ID")==null)
		{
			param.put("ROLE_ID", 0);
		}
	} 
}
