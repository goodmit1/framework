package com.fliconz.fm.admin.service;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.cache.CacheManager;
import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@Service
public class MenuService extends  DefaultService {
	
   @Autowired PrgmService prgmService;
   
   protected    String getNameSpace() {
     return "com.fliconz.fm.admin.menu.Menu";
   }

	@Override
	protected String getTableName() {
		return "FM_MENU";
	}
    
	@Override
	public	String[] getPks() {
		return new String[]{"ID"};
	}

	
	
	@Override
	public int insert(Map param) throws Exception {
		param.put("PRGM_NM", param.get( "MENU_NM"));
		prgmService.insertOrUpdate(param);
		if(param.get("PARENT_ID") == null)
		{
			param.put("PARENT_ID",1);
		}
		if(param.get("MENU_ORDER") == null)
		{
			param.put("MENU_ORDER", NumberUtil.getInt( this.selectOneObjectByQueryKey("selectLastOdr", param),0)+1);
		}
		int row = super.insert(param);
		
		 
		return row;
	}

	@Override
	public int update(Map param) throws Exception {
		param.put("PRGM_NM", param.get( "MENU_NM"));
		prgmService.insertOrUpdate(param);
		int row =  super.update(param);

		 
		return row;
	}

	@Override
	public int delete(Map param) throws Exception {
		
		int row =  super.delete(param);
		
		 
		return row;
	}
	
	
  
  

}
