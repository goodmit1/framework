package com.fliconz.fm.admin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.fliconz.fm.mvc.service.FileAttachService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.PropertyManager;

import javax.servlet.http.HttpServletRequest;

@Service
public class NoticeService extends  DefaultService {

	@Override
	public Map getInfo(Map param) throws Exception {
		this.updateByQueryKey("update_HITS", param);

		return super.getInfo(param);
	}

	protected String getNameSpace() {
		return "com.fliconz.fm.admin.notice.Notice";
	}

	@Override
	protected String getTableName() {
		return "FM_NOTICE";
	}
	@Autowired

	@Override
	public	String[] getPks() {
		return new String[]{"ID"};
	}

	public Object deleteFileNotice(HttpServletRequest request) throws Exception {
		Map param = ControllerUtil.getParam(request);
		//게시판 삭제
		int result = delete(param);

		//게시판 삭제 성공일 경우만 첨부파일 삭제
		if (result > 0) {
			Map delFileAttachParam = new HashMap();
			delFileAttachParam.put("ID", param.get("ID"));
			delFileAttachParam.put("TABLE_NM", this.getTableName());
			super.deleteByQueryKey("com.fliconz.fm.fileAttach", "delete_FM_FILE_ATTACH", delFileAttachParam);
		}

		return result;
	}
}
