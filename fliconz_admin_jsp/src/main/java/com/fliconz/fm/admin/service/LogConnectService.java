package com.fliconz.fm.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class LogConnectService extends  DefaultService {

	@Override
	protected String getTableName() {
		return "FM_CONNECTLOG";
	}

	@Override
	public String[] getPks() {
		return null;
	}

	@Override
	protected String getNameSpace() {
		return "com.fliconz.fm.admin.log.Log";
	}
	
}
