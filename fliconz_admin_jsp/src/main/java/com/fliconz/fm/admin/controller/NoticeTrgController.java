package com.fliconz.fm.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.NoticeTrgService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.ListUtil;

@RestController
@RequestMapping(value =  "/noticeTrg" )
public class NoticeTrgController extends DefaultController {

	@Autowired NoticeTrgService service;
	@Override
	protected NoticeTrgService getService() {
		return service;
	}
	@RequestMapping(value =  "/save_multi" )
	public Map saveMulti(final HttpServletRequest request, HttpServletResponse response)
	{
		return (Map)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Object t = param.get("TEAM_CD");
				String[] teamCd;
				if(t instanceof String)
				{
					teamCd = new String[]{(String)t};
				}
				else
				{
					teamCd = (String[])t;
				}
				
				service.insertAll((String)param.get("ID"), teamCd);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}

		}).run(request);

	}
}