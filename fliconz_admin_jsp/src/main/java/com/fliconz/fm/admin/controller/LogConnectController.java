package com.fliconz.fm.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.LogConnectService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/log_connect" )
public class LogConnectController extends DefaultController{

	@Autowired LogConnectService service;
	@Override
	protected DefaultService getService() {
		return service;
	}


}
