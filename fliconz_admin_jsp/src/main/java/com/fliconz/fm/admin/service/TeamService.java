package com.fliconz.fm.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.mvc.DefaultService;

@Service
public class TeamService extends  DefaultService {
	@Autowired TeamRoleService teamRoleService;
	@Autowired UserService userService;
   @Override
	public Map getInfo(Map param) throws Exception {
		Map info = super.getInfo(param);
		List<Map> list =  super.selectList("FM_TEAM_ROLE", param);
		List roleId =new ArrayList();
		for(Map m : list)
		{
			roleId.add(m.get("ROLE_ID"));
			
		}
		info.put("ROLE_ID", roleId);
		return info;
	}

   protected int insertTeamRole(Map param) throws Exception
   {
	   Object o = param.get("ROLE_ID");
	   if(o==null) return 0;
	   String[] role;
	   if(o instanceof String[])
	   { 
		   role = (String[])param.get("ROLE_ID");
	   }
	   else
	   {
		   role = new String[]{(String)o};
	   }
	   int row =0;
	   for(String r:role)
	   {
		   param.put("ROLE_ID", r);
		   row += teamRoleService.insert(param);
	   }
	   return row;
   }
   
   protected void chgTeamOwner(Map param) throws Exception
   {
	   if(param.get("TEAM_OWNER") != null && !"".equals(param.get("TEAM_OWNER")))
		{
		   	this.updateByQueryKey("resetTeamOwner", param);
			Map param1 = new HashMap<>();
			param1.put("USER_ID", param.get("TEAM_OWNER"));
			param1.put("TEAM_CD", param.get("TEAM_CD"));
			userService.update(param1);
		}
   }
   
	   @Override
	public int insert(Map param) throws Exception {
		if(param.get("TEAM_CD") == null || "".equals(param.get("TEAM_CD")))
		{
			param.put("TEAM_CD",this.selectOneObjectByQueryKey("getNewId", param));
		}
		
		int row =  super.insert(param);
		chgTeamOwner(param);
		
		insertTeamRole(param);
		
	
		return row;
	}
   @Override
	public int delete(Map param) throws Exception {
	    int cnt =  this.selectCountByQueryKey("countUser", param);
	    if(cnt>0) {
	    	ResourceBundle resourceBundle = MessageBundle.getBundle("com.fliconz.fm.common.message", this.getLang());
	    	throw new Exception(resourceBundle.getString("USER_EXIST_IN_TEAM"));
	    }
	    this.deleteByQueryKey("delete_FM_TEAM_by_PARENT", param);
		int row = super.delete(param);
		try {
			this.deleteDBTable("FM_TEAM_ROLE", param);
		}
		catch(Exception ignore) {
			
		}
		return row;
	}
	
	@Override
	public int update(Map param) throws Exception {
		int row = super.update(param);
		if(row>0)
		{	
			chgTeamOwner(param);
			try {
				this.deleteDBTable("FM_TEAM_ROLE", param);
				this.insertTeamRole(param);
			}
			catch(Exception ignore) {
				
			}
		}
		return row;
	}


	protected    String getNameSpace() {
     return "com.fliconz.fm.admin.team.Team";
   }

	@Override
	protected String getTableName() {
		return "FM_TEAM";
	}
	@Override
	public	String[] getPks() {
		return new String[]{"TEAM_CD"};
	}

}
