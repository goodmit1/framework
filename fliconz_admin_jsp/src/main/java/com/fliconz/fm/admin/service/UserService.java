package com.fliconz.fm.admin.service;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.PropertyManager;

@Service
public class UserService extends  DefaultService {
   protected    String getNameSpace() {
     return "com.fliconz.fm.admin.user.User";
   }

	@Override
	protected String getTableName() {
		return "FM_USER";
	}
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SaltSource saltSource;

	@Override
	public int insert(Map param) throws Exception {
		
		List<Map<String, Object>> list = super.selectList("FM_USER_VALID", param);
		if(list.size()>0)
		{
			ResourceBundle resourceBundle = MessageBundle.getBundle("com.fliconz.fm.common.message", this.getLang());
			throw new Exception(resourceBundle.getString("FM_USER_ID_VALIDATION_MESSAGE"));
		}
		int row= super.insert(param);
		if(row==1)
		{
			if(TextHelper.isEmpty((String)param.get("PASSWORD"))) {
				param.put("PASSWORD", param.get("LOGIN_ID"));
			}
			chgPassword(param);
			
		}
		return row;
	}

	@Override
	public int delete(Map param) throws Exception {
		// delete user_prgrm
		super.deleteByQueryKey("com.fliconz.fm.admin.userPrgm", "delete_FM_USER_PRGM", param);
		
		// update fm_team set team_owner = null where 
		this.updateByQueryKey("resetTeamOwner", param);
		
		return super.delete(param);
	}
	public void chgPassword(Map param) throws Exception
	{
		  String hashedPassword = (String)param.get("PASSWORD");

		 UserVO vo= new UserVO(param);

		  boolean isEncrypted = PropertyManager.getBoolean("password.security.encryption", false);
	      if(isEncrypted){
	         hashedPassword = passwordEncoder.encodePassword(hashedPassword, saltSource.getSalt(vo));
	      } else {
	         hashedPassword = hashedPassword;
	      }
	      param.put("PASSWORD", hashedPassword);
	      super.updateByQueryKey("update_pass_FM_USER", param);
	}
	@Override
	public	String[] getPks() {
		return new String[]{"USER_ID"};
	}
	
	
  
  

}
