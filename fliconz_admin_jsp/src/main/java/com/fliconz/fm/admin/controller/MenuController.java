package com.fliconz.fm.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.MenuService;
import com.fliconz.fm.admin.service.UserService;
import com.fliconz.fm.common.cache.CacheManager;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/menu" )
public class MenuController extends DefaultController{

	@Autowired MenuService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	@RequestMapping(value =  "/save_memory" )
	public int memoryReload(final HttpServletRequest request, HttpServletResponse response)
	{
		CacheManager.reloadMenu();
		return 1;
	}
	
}
