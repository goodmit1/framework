package com.fliconz.fm.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.FmDdicService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/fm_ddic" )
public class FmDdicController extends DefaultController{

	@Autowired FmDdicService service;
	@Override
	protected DefaultService getService() {
		return service;
	}


}
