package com.fliconz.fm.admin.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
 
import com.fliconz.fm.mvc.DefaultService;

@Service
public class NoticeTrgService extends  DefaultService {

	protected String getNameSpace() {
		return "com.fliconz.fm.admin.notice.Notice";
	}

	@Override
	protected String getTableName() {
		return "FM_NOTICE_TRG";
	}
	@Override
	public String[] getPks() {
		return new String[]{"ID", "TRG_ID"};
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		return config;
	}
	
	public void insertAll(String id, String[] teamCds) throws Exception
	{
		Map param = new HashMap();
		param.put("ID", id);
		this.delete(param);
		for(String teamCd:teamCds)
		{
			if(teamCd==null || "".equals(teamCd)) continue;
			param.put("TRG_ID", teamCd);
			this.insert(param);
		}
	}
}