package com.fliconz.fm.admin.controller;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import com.fliconz.fm.admin.service.UserPrgmService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.PermissionService;
import com.fliconz.fm.security.filter.FMUrlSecurityMetadataSource;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/user_prgm" )
public class UserPrgmController extends DefaultController{

	@Autowired UserPrgmService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	@RequestMapping(value =  "/save_memory" )
	public Object memoryReload(final HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			LinkedHashMap methodMap = (LinkedHashMap)SpringBeanUtil.getBean("methodMap");
	
			PermissionService permissionService = (PermissionService)SpringBeanUtil.getBean("permissionService");
			methodMap.clear();
			methodMap.putAll(permissionService.getRoleMethodList());
	
	
			FMUrlSecurityMetadataSource urlSecurityMetadataSource =(FMUrlSecurityMetadataSource) SpringBeanUtil.getBean("urlSecurityMetadataSource");
			urlSecurityMetadataSource.reload();
			return ControllerUtil.getSuccessMap(null);
		}
		catch(Exception e)
		{
			return ControllerUtil.getErrMap(e);
		}
	}
}
