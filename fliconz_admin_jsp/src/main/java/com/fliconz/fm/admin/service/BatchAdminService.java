package com.fliconz.fm.admin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fm.admin.schedule.ScheduleService;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service

public class BatchAdminService extends  DefaultService {

	ScheduleService service;
	
	protected void init()
	{
		service = (ScheduleService)SpringBeanUtil.getBean("scheduleService");
	}
	@Override
	protected String getTableName() {
		return "FM_BATCH_PRGM";
	}

	@Override
	public String[] getPks() {
		return new String[]{"BATCH_PRGM_ID"};
	}

	

	@Override
	public int insert(Map param) throws Exception {
		param.put("BATCH_PRGM_ID", System.nanoTime());	
		int row = super.insert(param);
		service.onAfterInsert(param);
		return row;
	}

	@Override
	public int update(Map param) throws Exception {
		int row = super.update(param);
		service.onAfterUpdate(param);
		return row;
	}

	@Override
	public int delete(Map param) throws Exception {
		int row = super.delete(param);
		service.onAfterDelete(param);
		return row;
	}

	@Override
	protected String getNameSpace() {
		return service.NS; 
	}
	@Override
	protected Map<String,String> getCodeConfig()
	{
		Map m = new HashMap();
		m.put("BATCH_EXE_RESULT_CD", "BATCH_EXE_RESULT_CD");
		return m;
	}
	public List listExecHistory(Map param)  throws  Exception{
		List<Map> list= super.selectList("FM_BATCH_EXE_RESULT", param);
		addCodeNames(list);
		return list;
	}

}
