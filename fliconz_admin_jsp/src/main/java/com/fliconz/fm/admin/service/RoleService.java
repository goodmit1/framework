package com.fliconz.fm.admin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.NumberUtil;

@Service
public class RoleService  extends  DefaultService {
	   protected    String getNameSpace() {
		     return "com.fliconz.fm.admin.role.Role";
		   }

	@Override
	protected String getTableName() {
		
		return "FM_ROLE";
	}

	@Override
	public String[] getPks() {
		return new String[]{"ROLE_ID"};
	}

	@Override
	public int insert(Map param) throws Exception {
		int row = super.insert(param);

		insertRolePrgm(param);
		return row;
	}

	@Override
	public int update(Map param) throws Exception {
		int row = super.update(param);
		param.remove("PRGM_ID");
		super.deleteDBTable("FM_ROLE_PRGM", param);
		
		insertRolePrgm(param);
		return row;
	}
	public int deleteRolePrgm(Map param) throws Exception
	{
		return super.deleteDBTable("FM_ROLE_PRGM", param);
	}

	protected void insertRolePrgm(Map param) throws Exception
	{
		Object o = param.get("ROLE_PRGM");
		List<Map> list = null;
		if(o instanceof List)
		{
			list = (List)o;
		}
		else
		{
			list = ListUtil.makeJson2List((String)o);
		}
		if(list != null)
		{
			for(Map m : list)
			{
				m.put("ROLE_ID",param.get("ROLE_ID"));
				this.insertDBTable("FM_ROLE_PRGM", m);
			}
		}
	}
	@Override
	public int delete(Map param) throws Exception {
		int row = super.delete(param);
		param.remove("PRGM_ID");
		super.deleteDBTable("FM_ROLE_PRGM", param);
		return row;
	}

	@Override
	public Map getInfo(Map param) throws Exception {
		Map info = super.getInfo(param);
		info.put("ROLE_PRGM", super.selectList("FM_ROLE_PRGM", param));
		return info;
	}

	

}
