package com.fliconz.fm.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.UserService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/user" )
public class UserController extends DefaultController{

	@Autowired UserService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

	@RequestMapping(value =  "/save_reset_password" )
	public Map resetPassWord(final HttpServletRequest request, HttpServletResponse response){
		return (Map)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				param.put("PASSWORD", param.get("LOGIN_ID"));
				((UserService)getService()).chgPassword(param);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}

		}).run(request);

	}

}
