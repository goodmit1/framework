package com.fliconz.fm.admin.service;

import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class TeamRoleService extends DefaultService{

	@Override
	protected String getTableName() {
		return "FM_TEAM_ROLE";
	}

	@Override
	public String[] getPks() {
		return new String[]{"TEAM_CD","ROLE_ID"};
	}

	@Override
	protected String getNameSpace() {
		return "com.fliconz.fm.admin.teamRole.TeamRole";
	}

}
