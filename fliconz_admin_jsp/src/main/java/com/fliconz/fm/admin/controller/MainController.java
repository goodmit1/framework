package com.fliconz.fm.admin.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@RequestMapping(value =  "/test" )
	public Map test(HttpServletRequest request,HttpServletResponse response)
	{
		Map result = new HashMap();
		result.put("result", "ok");
		return result;
		
	}
}
