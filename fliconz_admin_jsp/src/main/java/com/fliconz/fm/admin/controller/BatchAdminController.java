package com.fliconz.fm.admin.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.schedule.ScheduleRunner;
import com.fliconz.fm.admin.schedule.ScheduleService;
import com.fliconz.fm.admin.service.BatchAdminService;
import com.fliconz.fm.admin.service.TeamService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/batch_admin" )
public class BatchAdminController extends DefaultController{

	@Autowired BatchAdminService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	@RequestMapping(value =  "/list_exec_history" )
	public Map listExecHistory(final HttpServletRequest request, HttpServletResponse response)
	{
		return (Map)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				List list = service.listExecHistory(param);
				result.put("list", list);
				
				return result;
			}

		}).run(request);

	}
	@RequestMapping(value =  "/exec" )
	public Map exec(final HttpServletRequest request, HttpServletResponse response)
	{
		return (Map)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				ScheduleRunner runner = (ScheduleRunner) SpringBeanUtil.getBean("scheduleRunner");
				if(runner != null)
				{
					int row = runner.exec((String)param.get("BATCH_PRGM_ID"), (String)param.get("CLASS_NM"), new Date());
					result.put("COUNT", row);
				}
				else
				{
					throw new Exception("NOT FOUND");
				}
				
				return result;
			}

		}).run(request);

	}

}
