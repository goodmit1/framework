package com.fliconz.fm.admin.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.QnaService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.UserVO;

@RestController
@RequestMapping(value =  "/qna" )
public class QnaController extends DefaultController {

	@Autowired QnaService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

	@RequestMapping(value =  "/saveAnswer" )
	public Map saveAnswer(HttpServletRequest request, HttpServletResponse response) {
		return ((Map) new ActionRunner() {
			protected Object run(Map param) throws Exception {
				if(UserVO.getUser().isAdmin()) {
					getService().updateByQueryKey("update_FM_INQUIRY_ANSWER", param);
					return ControllerUtil.getSuccessMap(param, getService().getPks());
				}
				else {
					throw new Exception("Invalid right");
				}
			}
		}.run(request));
	}

	@RequestMapping(value =  "/read" )
	public Object read(final Locale locale,HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map info =  service.getInfo(param);
				if(UserVO.getUser() != null && info.get("INS_ID").equals(UserVO.getUser().getUserId())) {
					service.updateByQueryKey("read_FM_INQUIRY", param);
				}
				return info;
			}}).run(request);
	}

}
