package com.fliconz.fm.admin.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.NumberUtil;

@Service
public class PrgmService  extends  DefaultService {
	   protected    String getNameSpace() {
		     return "com.fliconz.fm.admin.prgm.Prgm";
		   }

	@Override
	protected String getTableName() {
		
		return "FM_PRGM";
	}

	@Override
	public String[] getPks() {
		return new String[]{"PRGM_ID"};
	}

	Map getPrgmByPath(String path) throws Exception
	{
		Map param  = new HashMap();
		param.put("PRGM_PATH", path);
		java.util.List<Map> list = super.selectList(this.getTableName(), param);
		if(list != null && list.size()>0)
		{
			return list.get(0);
		}
		return null;
	}
	@Override
	public int insertOrUpdate(Map param) throws Exception {
		if(NumberUtil.getLong(param.get("PRGM_ID"),0)==0)
		{
			if(TextHelper.isEmpty((String)param.get("PRGM_PATH")))
			{
				return 0;
			}
			Map oldInfo = getPrgmByPath((String)param.get("PRGM_PATH")); 
			if(oldInfo != null)
			{
				param.put("PRGM_ID", oldInfo.get("PRGM_ID"));
			}
			
		}
		else
		{
			if(TextHelper.isEmpty((String)param.get("PRGM_PATH")))
			{
				int row =  this.delete(param);
				param.put("PRGM_ID",0);
				return row;
			}
			 
		}
		return super.insertOrUpdate(param);
		
	}


}
