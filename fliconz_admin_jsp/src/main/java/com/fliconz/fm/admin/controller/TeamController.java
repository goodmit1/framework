package com.fliconz.fm.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.TeamService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/team" )
public class TeamController extends DefaultController{

	@Autowired TeamService service;
	@Override
	protected DefaultService getService() {
		return service;
	}


}
