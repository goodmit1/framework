package com.fliconz.fm.admin.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.service.MultilangPopupService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.ListUtil;

@RestController
@RequestMapping(value =  "/popup/multilang" )
public class MultilangPopupController extends DefaultController{

    @Autowired MultilangPopupService service;
    @Override
    protected DefaultService getService() {
        return service;
    }

    @Override
    @RequestMapping(value =  "/save" )
    public Map save(HttpServletRequest request, HttpServletResponse response) {
        return (Map)(new ActionRunner() {
            @Override
            protected Object run(Map param) throws Exception {
                List<Map> selectedList = ListUtil.makeJson2List((String) param.get("selected_json"));
                for(Map loop : selectedList){
                    loop.put("TABLE_NM", param.get("TABLE_NM"));
                    loop.put("FIELD_NM", param.get("FIELD_NM"));
                    loop.put("ID", param.get("ID"));
                    if("I".equals(loop.get("IU"))){
                        service.insert(loop);
                    }else{
                        service.update(loop);
                    }
                }
                return ControllerUtil.getSuccessMap(param );
            }

        }).run(request);
    }

    @Override
    @RequestMapping(value =  "/delete" )
    public Map delete(HttpServletRequest request, HttpServletResponse response) {
        return (Map)(new ActionRunner() {
            @Override
            protected Object run(Map param) throws Exception {
                List<Map> selectedList = ListUtil.makeJson2List((String) param.get("selected_json"));
                for(Map loop : selectedList){
                    loop.put("TABLE_NM", param.get("TABLE_NM"));
                    loop.put("FIELD_NM", param.get("FIELD_NM"));
                    loop.put("ID", param.get("ID"));
                    if("I".equals(loop.get("IU"))){
                        continue;
                    }
                    service.delete(loop);
                }
                return ControllerUtil.getSuccessMap(param );
            }
        }).run(request);
    }

}
