package com.fliconz.fm.admin.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import com.fliconz.fm.admin.service.RoleService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.PermissionService;
import com.fliconz.fm.security.filter.FMUrlSecurityMetadataSource;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil; 

@RestController
@RequestMapping(value =  "/role" )
public class RoleController extends DefaultController{

	@Autowired RoleService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

	@RequestMapping(value =  "/save_memory" )
	public Object memoryReload(final HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			LinkedHashMap methodMap = (LinkedHashMap)SpringBeanUtil.getBean("methodMap");
	
			PermissionService permissionService = (PermissionService)SpringBeanUtil.getBean("permissionService");
			methodMap.clear();
			methodMap.putAll(permissionService.getRoleMethodList());
	
	
			FMUrlSecurityMetadataSource urlSecurityMetadataSource =(FMUrlSecurityMetadataSource) SpringBeanUtil.getBean("urlSecurityMetadataSource");
			urlSecurityMetadataSource.reload();
			return ControllerUtil.getSuccessMap(null);
		}
		catch(Exception e)
		{
			return ControllerUtil.getErrMap(e);
		}
	}
	@RequestMapping(value =  "/delete_prgm" )
	public int prgmDelete(final HttpServletRequest request, HttpServletResponse response)
	{
		return (int)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				String jsonArrStr = (String)param.get("list");
				if(jsonArrStr == null || "".equals(jsonArrStr)) throw new Exception("NO DATA");
				
				List<Map> list = ListUtil.makeJson2List(jsonArrStr);
				int row=0;
				for(Map m : list)
				{
					row += ((RoleService) getService()).deleteRolePrgm(m);
				}
				return row;
			}

		}).run(request);

	}
}
