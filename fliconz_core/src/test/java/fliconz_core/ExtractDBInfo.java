package fliconz_core;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class ExtractDBInfo {
	FileWriter writer ;
	public Connection getConn() throws Exception {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Properties prop = new Properties();
		//remarksReporting=true", "clovir", "vmware1"
		prop.put("user", "iog");
		prop.put("password", "vmware1");
		prop.put("remarksReporting", "true");
		return DriverManager.getConnection("jdbc:oracle:thin:@172.16.33.3:1521:orcl", prop);
	}
	
	public static void main(String[] args) {
		ExtractDBInfo db = new ExtractDBInfo();
		try {
			db.run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void run() throws Exception {
		Connection conn = null;
		try
		{
			conn = getConn();
			DatabaseMetaData meta = conn.getMetaData();
			ResultSet tables = meta.getTables(null, "IOG", "IOG%LOG", new String[] {"TABLE"});
			
			
			while(tables.next()) {
				makeTableInfo(meta, tables.getString("TABLE_SCHEM"),tables.getString("TABLE_NAME"));
			}
			tables.close();
		}
		finally {
			if(conn != null) conn.close();
		}
	}
	private String nvl(String v) {
		if(v==null) return "";
		return v;
	}
	private Map getPK(DatabaseMetaData meta,String schema, String tableName) throws SQLException {
		ResultSet pk = meta.getPrimaryKeys(null, schema, tableName);
		Map pks = new HashMap();
		while(pk.next()) {
			pks.put(pk.getString("COLUMN_NAME"), pk.getInt("KEY_SEQ"));
		}
		pk.close();
		return pks;
	}
	private Set getFK(DatabaseMetaData meta,String schema, String tableName) throws SQLException {
		ResultSet fk = meta.getImportedKeys(null, schema, tableName);
		Set fks = new HashSet();
		while(fk.next()) {
			fks.add(fk.getString("FKCOLUMN_NAME") );
		}
		fk.close();
		return fks;
	}
	public void makeTableInfo(DatabaseMetaData meta, String schema, String tableName) throws Exception {
		
		Map pks = getPK(meta,schema, tableName);
		Set fks = getFK(meta,schema, tableName);
		ResultSet columns = meta.getColumns(null, schema, tableName, "%");
		while(columns.next()) {
			//테이블명	컬럼한글명	컬럼명	데이터타입	데이터길이	소수점	PK여부	FK여부	POSITION	PK POSITION	NULL여부	DEFAULT	정의
			String def = columns.getString("COLUMN_DEF");
			System.out.print(columns.getString("TABLE_NAME") + "," + nvl(columns.getString("REMARKS")) + "," +columns.getString("COLUMN_NAME") + "," + columns.getString("TYPE_NAME") + "," +  columns.getString("COLUMN_SIZE") + "," + columns.getInt("DECIMAL_DIGITS") + ",");
			Object pkSeq = pks.get(columns.getString("COLUMN_NAME"));
			// pk여부, fk 여부
			System.out.print((pkSeq==null?"N":"Y") + "," + (fks.contains(columns.getString("COLUMN_NAME")) ? "Y":"N" ) + ",");
			System.out.print(columns.getInt("ORDINAL_POSITION") + "," + (pkSeq==null ? "":pkSeq) + ",");
			//pk순서
			System.out.print(("YES".equals(columns.getString("IS_NULLABLE"))?"Y":"N") + "," + nvl(def) + "," + nvl(columns.getString("REMARKS")) + "\n");
		}
		columns.close();
	}
}
