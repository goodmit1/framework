package com.fliconz.fw.runtime.dao;

import javax.annotation.PreDestroy;

import org.apache.ibatis.session.SqlSession;

public class BatchDAO extends BaseDAO{
	public BatchDAO() throws Exception {
		super();
		 
	}
	SqlSession sqlSession;
	@Override
	public   SqlSession getSqlSession()   throws Exception
	{
		if(sqlSession == null)
		{
			sqlSession = FCSqlSessionFactory.getInstance().getSession(  true);
		}
		return sqlSession;
	}
	@PreDestroy
	public void close()
	{
		if(sqlSession != null) sqlSession.close();
		
	}
}
