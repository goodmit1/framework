package com.fliconz.fw.runtime.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	/**
	 * date를 yyyy-MM-dd format으로 바꾼다.
	 * @param date
	 * @return
	 */
	public static String convertStr(Date date)
	{
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd" );
		return transFormat.format(date);
	}
	public static int getLastDay(int yy, int mm)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(yy, mm+1, 1);
		cal.add(Calendar.DATE, -1);
		return cal.get(Calendar.DATE);
	}
	/**
	 * 오늘날짜를 pattern으로 format을 바꾼다.
	 * @param pattern
	 * @return
	 */
	public static String formatToday(String pattern)
	{
		SimpleDateFormat transFormat = new SimpleDateFormat(pattern );
		return transFormat.format(new Date());
	}
	
	public static String format(Date date, String pattern)
	{
		SimpleDateFormat transFormat = new SimpleDateFormat(pattern );
		return transFormat.format(date);
	}
	
	/**
	 * str을 pattern을 이용하여 날짜로 변환
	 * @param str
	 * @param pattern
	 * @return
	 * @throws Exception
	 */
	public static Date toDate(String str, String pattern) throws Exception
	{
		SimpleDateFormat transFormat = new SimpleDateFormat(pattern);
		return transFormat.parse(str);
	}
	/**
	 * 날짜 더하는 함수
	 * @param dt
	 * @param type 일: Calendar.DAY_OF_YEAR, 월:Calendar.MONTH
	 * @param d
	 * @return
	 */

	public static Date addDate(Date dt, int type , int d)
	{
		 Calendar c = Calendar.getInstance();
		 c.setTimeInMillis(dt.getTime());
		 c.add(type, d);
		 return c.getTime();
	}
	public static void main(String[] args)
	{
		try {
			System.out.println(DateUtil.toDate("20140501102359","yyyyMMddHHmmss"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(DateUtil.addDate(new Date(), Calendar.MONTH, -1));
	}
}

