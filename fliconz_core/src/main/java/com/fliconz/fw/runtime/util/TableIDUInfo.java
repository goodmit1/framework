package com.fliconz.fw.runtime.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * grid에 insert/update/delete되었는지 관리
 * @author 윤경
 *
 */
public class TableIDUInfo {
	 
	int insertCount;
	List<Map> gridList;
	public static final String IDU_FIELD = "IDU";
	 
	
	public TableIDUInfo( )
	{
		 
		insertCount=0;
	}
	/**
	 * insert된 갯수
	 * @return
	 */
	public int getInsertCount()
	{
		return insertCount;
	}
	 /**
	  * 정보 clear
	  */
	public void clear()
	{
		insertCount=1;
		if(updateRows != null)
		{
			 for(Map m : updateRows)
			 {
				 clear(m);
			 }
		}
		if(deleteRows != null)
		{
			 for(Map m : deleteRows)
			 {
				 clear(m);
			 }
		}
	}
	 
	/**
	 * selRow를 update , IDU값을 U로 변경
	 * @param selRow
	 */
	public void addUpdate(Map selRow )
	{
	 
		if( selRow.get(IDU_FIELD) == null ||  "D".equals(selRow.get(IDU_FIELD)))
		{
			setIDU(selRow,     "U");
			selRow.put("CHK_ID", true);
		}
		// 추가인 경우 추가로 유지
	}
	/**
	 * updateRows에 추가
	 * @param selRow
	 * @return updateRows return
	 */
	public Set addUpdateRow(Map selRow)
	{
		if(updateRows == null)
		{
			updateRows = new LinkedHashSet();
		}
		addUpdate(selRow);
		updateRows.add(selRow);
		return updateRows;
	}
	/**
	 * selRow를 insert, IDU값을 I로 변경
	 * @param selRow
	 */
	public void addInsert(Map selRow )
	{
		addInsert(selRow, false);
	}
	
	/**
	 * selRow를 insert, IDU값을 I로 변경
	 * @param selRow
	 * @param isOverride 기존에 Update되거나 delete되어도 I로 변경여부
	 */
	public void addInsert(Map selRow , boolean isOverride)
	{
	 
		if(isOverride || selRow.get(IDU_FIELD) == null )
		{
			setIDU(selRow,    "I");
			insertCount++;
		}
		selRow.put("CHK_ID", true);
	}
	private void setIDU(Map selRow,    String kubun)
	{
		selRow.put("IDU", kubun);
	 
	}
	/**
	 * selRow를 삭제, IDU필드에 D 
	 * @param selRow
	 * @return 추가한 후 삭제한 건지 여부
	 */
	public boolean addDelete(Map selRow )
	{
	 
		if( selRow.get(IDU_FIELD) == null || "U".equals(selRow.get(IDU_FIELD)))
		{
			setIDU(selRow,   "D");
		}
		else if("I".equals(selRow.get(IDU_FIELD)))
		{
			 
			return true;
		}
		return false; 
	}
	
	Set<Map> deleteRows;
	public Set<Map> getDeleteRows() {
		return deleteRows;
	}
	public Set<Map> getUpdateRows() {
		return updateRows;
	}
	Set<Map> updateRows;
	public Set<Map> setDeleteRows(Set<String> idxs)
	{
		return setDeleteRows(idxs, false);
	}
	/**
	 * 화면에서 삭제  체크한 목록
	 * @param idxs 화면에서 삭제  체크한 checkbox 순서
	 * @param forRowDelete 추가한 후 삭제한 것이라면 grid에서 삭제여부
	 * @return
	 */
	public Set<Map> setDeleteRows(Set<String> idxs, boolean forRowDelete)
	{
		deleteRows = new HashSet();
		if(idxs.size()==0) return null;
		if(gridList==null) return null;
	
		Object[] it = gridList.toArray();
		
		for( String idx : idxs)
		{
			int i = Integer.parseInt(idx);
			Map m = (Map) it[i];
			//if(true==(Boolean)m.get("DEL_CHK_ID"))
			{	
				boolean isNewRow = addDelete(m );
				
				
				if(forRowDelete)
				{
					if(!isNewRow) deleteRows.add(m);
					gridList.remove(m);
				}
				else 
				{
					deleteRows.add(m);
				}
			}
			 
		}
		return deleteRows;
	}
	/**
	 * 화면에서 수정 체크된 목록
	 * @param idxs 화면에서 수정 체크된 순서
	 * @return
	 */
	public Set<Map> setUpdateRows(Set<String> idxs)
	{
		updateRows = new LinkedHashSet<Map>();
		if(idxs.size()==0) return null;
		if(gridList == null || gridList.size()==0) return null;
		
		 
		
		for( String idx : idxs)
		{
			int i = Integer.parseInt(idx);
			
			Map m = (Map) gridList.get(i);
			//if("I".equals(m.get(IDU_FIELD)) || "U".equals(m.get(IDU_FIELD)))
			{
				updateRows.add(m);
			}
		 
			 
		}
		return updateRows;
	}
	public void setGridList(List list) {
		this.gridList = list;
		
	}
	
	/**
	 * selRow가 추가 된 것인지 여부
	 * @param selRow
	 * @return
	 */
	public boolean isInsert(Map selRow) {
		return "I".equals(selRow.get(IDU_FIELD));
	}
	
	/**
	 * selRow가 수정된 것인지 여부
	 * @param selRow
	 * @return
	 */
	public boolean isUpdate(Map selRow) {
		return "U".equals(selRow.get(IDU_FIELD));
	}
	
	/**
	 * selRow가 삭제된 것인지 여부
	 * @param selRow
	 * @return
	 */
	public boolean isDelete(Map selRow) {
		return "D".equals(selRow.get(IDU_FIELD));
	}
	public static void setError(Map p)
	{
		p.put("hasError", true);
	}
	
	/**
	 * m에서 IDU,CHK_ID, DEL_CHK_ID 필드 제거 
	 * @param m
	 */
	public void clear(Map m) {
		 m.remove(IDU_FIELD);
		 m.remove("CHK_ID");
		 m.remove("DEL_CHK_ID");
		 m.remove("hasError");
		
	}
	private boolean getBoolean(Object o)
	{
		if( o instanceof Boolean)
		{
			return (Boolean)o;
		}
		else if(o instanceof String)
		{
			return "true".equals(o);
		}
		return false;
	}
	public void setDeleteUpdateRowsLoop()
	{
		deleteRows = new LinkedHashSet();
		updateRows = new LinkedHashSet();
		for(Map m : gridList)
		{
			if(isInsert(m) || isUpdate(m) || (m.get("CHK_ID") != null && getBoolean(m.get("CHK_ID"))))
			{
				if(!isInsert(m)) setIDU(m,   "U");
				updateRows.add(m);
			}
			else if((isDelete(m) || (m.get("DEL_CHK_ID") != null && getBoolean(m.get("DEL_CHK_ID")))))
			{
				setIDU(m,   "D");
				deleteRows.add(m);
			}
		}
	}
}
