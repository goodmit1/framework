package com.fliconz.fw.runtime.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @deprecated
 * @author 윤경
 *
 */
public class SelectRowMap extends HashMap{

	Set<String> numberField;
	public SelectRowMap()
	{
		numberField = new HashSet();
	}
	@Override
	public Object put(Object key, Object val) {
		 
		if(val instanceof BigDecimal)
		{
			numberField.add((String)key);
		}
		else if( numberField.contains(key) && val instanceof String)
		{
			return super.put(key, new BigDecimal( (String)val));
		}
		return super.put(key, key);
	}

}
