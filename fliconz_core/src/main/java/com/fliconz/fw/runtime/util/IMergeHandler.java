package com.fliconz.fw.runtime.util;

import java.util.Map;

public interface IMergeHandler {

	void merge(Map m, Map m2);
	Object getEtcInfo();

}
