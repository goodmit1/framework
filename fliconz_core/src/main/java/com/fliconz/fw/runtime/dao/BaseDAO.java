package com.fliconz.fw.runtime.dao;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.QueryKeyHelper;


public abstract class BaseDAO  {

	public abstract SqlSession getSqlSession() throws Exception;

	public SqlSession getSqlSession(boolean isBatch) throws Exception {
		return getSqlSession();
	}

	public Map getListMap(String queryKey, Map param) throws Exception{
		MapResultHandler resultHandler = new MapResultHandler("ID", "TITLE");
		getSqlSession().select(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), param, resultHandler);
		return resultHandler.getResult();
	}

	public BaseDAO() throws Exception {
		super();
	}

	public List selectList(String queryKey, Map param) throws Exception{
		List result = getSqlSession().selectList(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), param);
		return result;
	}

	public Map selectOne(String queryKey, Map param) throws Exception{
		Map result = getSqlSession().selectOne(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), param);
		return result;
	}

	public int selectNumber(String queryKey, Map param) throws Exception {
		int row = getSqlSession().selectOne(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), param);
		return row;
	}

	public Map selectOne(String queryKey, String id) throws Exception{
		Map result = getSqlSession().selectOne(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), id);
		return result;
	}

	public int insert(String queryKey, Map map) throws Exception{
		int row = getSqlSession().insert(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), map);
		return row;
	}

	public int insert(String queryKey, Map map, boolean isBatch) throws Exception{
		int row = getSqlSession(isBatch).insert(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), map);
		return row;
	}

	public int update(String queryKey, Map map) throws Exception{
		MapUtil.setNullToEmpty(map);
	 	int row = getSqlSession().update(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), map);
		return row;
	}

	public int update(String queryKey, Map map, boolean isBatch) throws Exception{
		MapUtil.setNullToEmpty(map);
	 	int row = getSqlSession(isBatch).update(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), map);
		return row;
	}

	public int delete(String queryKey, Map map) throws Exception{
		int row = getSqlSession().delete(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), map);
		return row;
	}

	public int delete(String queryKey, Map map, boolean isBatch) throws Exception{
		int row = getSqlSession(isBatch).delete(QueryKeyHelper.queryKeyCheckByDB(this, queryKey), map);
		return row;
	}

	public Connection getConnection() throws Exception{
		return getSqlSession().getConnection();
	}
}
