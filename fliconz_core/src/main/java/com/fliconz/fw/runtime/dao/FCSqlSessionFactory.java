package com.fliconz.fw.runtime.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class FCSqlSessionFactory {

	private static FCSqlSessionFactory me1;
 
	org.apache.ibatis.session.SqlSessionFactory f;
	public static synchronized FCSqlSessionFactory getInstance() throws Exception {
		 if(me1==null) me1 = new FCSqlSessionFactory();
		 return me1;
	}
	private FCSqlSessionFactory() throws Exception
	{
		init();
	}
	private void init() throws  Exception
	{

		org.apache.ibatis.session.SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
		f = builder.build(Resources.getResourceAsStream("sqlMapConfig.xml"));
		 
	}
	public void reload(String resource) throws Exception
	{
		XMLMapperBuilder mapperParser = new XMLMapperBuilder
				( Resources.getResourceAsStream(resource), f.getConfiguration(), resource, f.getConfiguration().getSqlFragments());
        mapperParser.parse();
	}
	public SqlSession getSession(java.sql.Connection conn) throws Exception
	{
						
		SqlSession s = f.openSession(conn);
		 
		return s;
	}
	public void closeAll()
	{
		
	}
	public SqlSession getSession(String resource, boolean isAutoCommit) throws Exception
	{
		if(resource != null && !f.getConfiguration().isResourceLoaded(resource))
		{
			reload(resource);
		}
				
		SqlSession s = f.openSession(isAutoCommit);
		 
		return s;
	}
	public SqlSession getSession( boolean isAutoCommit) throws Exception
	{
		 
				
		SqlSession s = f.openSession(isAutoCommit);
		 
		return s;
	}
	public void close(SqlSession session)
	{
		session.close();
	}
}
