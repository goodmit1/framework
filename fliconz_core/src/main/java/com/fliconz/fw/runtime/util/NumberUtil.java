package com.fliconz.fw.runtime.util;

import java.math.BigDecimal;

public class NumberUtil {

	private static String removeComma(String s)
	{
		return s.replaceAll(",", "");
	}
	public static Long getLong(Object o)
	{
		if(o == null) return null;
		if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).longValue();
		}
		else if(o instanceof Double)
		{
			return ((Double)o).longValue();
		}
		else if(o instanceof Integer)
		{
			return ((Integer)o).longValue();
		}
		else if(o instanceof String)
		{
			try
			{
				String s = removeComma((String)o);
				return Long.parseLong(s);
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else 
		{
			return (Long)o;
		}
	}
	public static Float getFloat(Object o)
	{
		if(o == null) return null;
		if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).floatValue();
		}
		else if(o instanceof Long)
		{
			return ((Long)o).floatValue();
		}
		else if(o instanceof String)
		{
			try
			{
				String s = removeComma((String)o);
				return Float.parseFloat(s);
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else if(o instanceof Double)
		{
			try
			{
				return ((Double)o).floatValue();
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else 
		{
			return (Float)o;
		}
	}
	public static long getLong(Object o, long def)
	{
		
		Long r =  getLong(o);
		if(r == null) return def;
		return r;
	}
	public static int getInt(Object o, int def)
	{
		
		Integer r =  getInt(o);
		if(r == null) return def;
		return r;
	}
	public static Integer getInt(Object o)
	{
		 
		if(o == null) return null;
		if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).intValue();
		}
		else if(o instanceof Double)
		{
			return ((Double)o).intValue();
		}
		else if(o instanceof Long)
		{
			return ((Long)o).intValue();
		}
		else if(o instanceof String)
		{
			try
			{
				String s = removeComma((String)o);
				return Integer.parseInt(s);
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else 
		{
			return (Integer)o;
		}
	}
	
	public static Double getDouble(Object o)
	{
		 
		if(o == null) return null;
		if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).doubleValue();
		}
		else if(o instanceof Integer)
		{
			return ((Integer)o).doubleValue();
		}
		else if(o instanceof Long)
		{
			return ((Long)o).doubleValue();
		}
		else if(o instanceof String)
		{
			try
			{
				String s = removeComma((String)o);
				return Double.parseDouble(s);
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else 
		{
			return (Double)o;
		}
	}
}
