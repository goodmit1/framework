package com.fliconz.fw.runtime.util;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

public class Date2StrTypeHandler implements TypeHandler<Object> {

	public static void main(String[] args)
	{
		 
	}
	public void setParameter(PreparedStatement ps, int i, Object p, JdbcType jdbcType) throws SQLException {

		try
		{
			if(p instanceof String) {
				ps.setString(i, ((String)p).replace("-",""));
			}
			else
			{
				String dateStr = DateUtil.format((Date)p, "yyyyMMdd");
				ps.setString(i, dateStr);
			}
		}
		catch(Exception e)
		{
			if(p != null) 
			{
				ps.setString(i, p.toString());
			}
			else
			{
				ps.setString(i, "");
			}
		}
		
	}

	public Object getResult(ResultSet rs, String columnName) throws SQLException {
		  return rs.getString(columnName);
	}

	public Object getResult(ResultSet rs, int columnIndex) throws SQLException {
		   return rs.getString(columnIndex);
	}

	public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
	    return cs.getString(columnIndex);
	}

}
