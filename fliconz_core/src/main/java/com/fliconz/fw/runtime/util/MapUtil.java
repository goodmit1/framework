package com.fliconz.fw.runtime.util;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;


public class MapUtil {
	/**
	 * map1과 map2의 값들등 field에 속한 값들이 모두 같은지 여부
	 * @param map1
	 * @param map2
	 * @param field
	 * @return
	 */
	public static boolean isSame(Map map1, Map map2 , String... field)
	{
		for(String f:field)
		{
			 if(map1.get(f)==null && map2.get(f) ==null)
			 {
				 continue;
			 }
			 else if(map1.get(f)==null || map2.get(f) ==null || !map1.get(f).toString().equals(map2.get(f).toString()))
			 {
				  return false;
			 }
		}
		return true;
	}
	public static boolean hasSame(Map map1, Map map2 , String... field)
	{
		for(String f:field)
		{
			  if(map1.get(f) != null && map2.get(f) != null && map1.get(f).toString().equals(map2.get(f).toString()))
			  {
				  return true;
			  }
		}
		return false;
	}
	public static Map jsonToMap(String json) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper(); 

		Map<String, Object> map = new HashMap<String, Object>(); // convert JSON string to 
		map = mapper.readValue(json, new TypeReference<Map<String, Object>>(){}); 
		return map;
	}

	public static void putAll(JSONObject src, JSONObject dest){
		Iterator it = src.keys();
		while(it.hasNext()){
			String key = (String) it.next();
			dest.put(key, src.get(key) );

		}

	}
	/**
	 * oldStr과 newStr이 같은지 여부
	 * @param oldStr
	 * @param newStr
	 * @return
	 */
	public static boolean isSame(Object oldStr, Object newStr )
	{
		if(oldStr==null && newStr==null) return true;
		if(oldStr != null && newStr == null ) return false;
		if(oldStr == null && newStr != null ) return false;
		if(oldStr.getClass() != newStr.getClass())
		{
			return oldStr.toString().equals(newStr.toString());
		}
		return oldStr.equals(newStr);
	}
	/**
	 * list에 map값들 중 map의 field가 일치하는 map이 존재하는지 여부
	 * @param list
	 * @param m
	 * @param field
	 * @return
	 */
	public static boolean isContains(List<Map> list, Map m, String field)
	{
		for(Map lm : list)
		{
			if(isSame(lm.get(field), m.get(field)))
			{
				return true;
			}
		}
		return false;
	}
	public static Map getSameInList(List<Map> list, Map m, String field)
	{
		for(Map lm : list)
		{
			if(isSame(lm.get(field), m.get(field)))
			{
				return lm;
			}
		}
		return null;
	}
	public static Map getSameInList(List<Map> list, String field, String value)
	{
		for(Map lm : list)
		{
			if(isSame(lm.get(field),value))
			{
				return lm;
			}
		}
		return null;
	}
	public static boolean isContains(List<Map> list, String field, String value)
	{
		for(Map lm : list)
		{
			if(isSame(lm.get(field), value))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 *  list에 map값들 중 map의 field가 일치하는 map이 존재하는 갯수
	 * @param list
	 * @param m
	 * @param field
	 * @return
	 */
	public static int getContainCount(List<Map> list, Map m, String[] field)
	{
		int idx = 0;
		for(Map lm : list)
		{
			for( String f: field)
			{
				if(!isSame(lm.get(f), m.get(f)))
				{
					break;
				}
				idx++;
			}
		}
		return idx;
	}
	/**
	 * src로부터 field항목만 dest에 복사
	 * @param src
	 * @param dest
	 * @param field
	 */
	public static void copy(Map  src, Map dest, String[] field)
	{
		if(src == null || dest == null || field==null )
		{
			return;
		}
		for(String f:field)
		{
			if(!"".equals(src.get(f)) && src.get(f) != null)
			{
				dest.put(f, src.get(f));
			}
		}
	}
	/**
	 * src로부터 field항목만 dest에 복사
	 * @param src
	 * @param dest
	 * @param field
	 */
	public static void copyExcludeField(Map  src, Map dest, String[] exclude_field)
	{
		if(src == null || dest == null   )
		{
			return;
		}
		Set<String> field = src.keySet();
		for(String f:field)
		{
			if(ArrUtil.indexOf(exclude_field, f)<0)
			{
				if(!"".equals(src.get(f)) && src.get(f) != null)
				{
					dest.put(f, src.get(f));
				}
			}
		}
	}
	
	/**
	 * src를 target에 넣음. 
	 * @param target
	 * @param src
	 */
	public static void convertArrayToSet(Set target, Object[] src)
	{
		for(Object s : src)
		{
			target.add(s);
		}
	}
	/**
	 * map에서 값이 val가 일치하는 key
	 * @param map
	 * @param val
	 * @return
	 */
	public static String getKeyByVal(Map map, Object val)
	{
		if(map == null) return "";
		Set<String>keys = map.keySet();
		for(String k : keys)
		{
			if(val == null) continue;
			if(val.toString().equals(map.get(k)))
			{
				return k;
			}
		}
		return null;
	}
	/**
	 * dest에서 field가 키인 항목 제거
	 * @param dest
	 * @param field
	 */
	public static void remove(Map  dest, String[] field)
	{
		for(String f:field)
		{
			dest.remove(f);
			
		}
	}
	/**
	 * collection s를 set으로 바꿔 return
	 * @param s
	 * @return
	 */
	public static Set toSet(Collection s)
	{
		 Set result = new HashSet();
		 result.addAll(s);
		 return result;
	}
	public static Set toSet(String[] s)
	{
		 if(s==null) return null;
		 Set result = new HashSet();
		
		 for(String s1 : s)
		 {
			 result.add(s1);
		 }
		 return result;
	}
	/**
	 * dest 값들 중에 null이면 ""으로 바꾼다.
	 * @param dest
	 */
	public static void setNullToEmpty(  Map dest )
	{
		Set<String> keys = dest.keySet();
		for(String k : keys)
		{
			if(dest.get(k) == null)
			{
				dest.put(k, "");
			}
		}
	}
	/**
	 * src중에서 dest에 없는 항목만 copy
	 * @param src
	 * @param dest
	 */
	public static void copyNotExist(Map  src, Map dest )
	{
		Iterator field = src.keySet().iterator();
		while(field.hasNext())
		{
			Object t = field.next();
			if(!(t instanceof String)) continue;
			String f = (String)t;
         	if(dest.get(f) == null)
			{
				dest.put(f, src.get(f));
			}
		}
	}
	public static Map strToMap(String str)
	{
		Map  result = new HashMap();
		str = str.substring(1, str.length()-1);
		String[] arr = str.split(", ");
		for(String a : arr)
		{
			String[] arr1 = a.split("=");
			String val = arr1[1];
			//val = arr1[1].substring(1, arr1[1].length()-1);
			
			result.put(arr1[0], val);
		}
		return result;
	}
}
