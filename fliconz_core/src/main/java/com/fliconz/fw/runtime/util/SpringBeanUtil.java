package com.fliconz.fw.runtime.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
 



import org.apache.ibatis.session.SqlSession;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.fliconz.fw.runtime.dao.FCSqlSessionFactory;

public class SpringBeanUtil {
/**
 * name으로 등록된 spring bean을 찾아 return 
 * @param name
 * @return
 */
	public static Object getBean(String name)
	{
		 
		ApplicationContext appContext = ContextLoaderListener.getCurrentWebApplicationContext();
		return appContext.getBean(name);
	}
	public static SqlSession getSqlSession() throws Exception
	{
		try
		{
			return  (SqlSession)getBean("sqlSessionTemplate");
		}
		catch(NullPointerException e)
		{
			return FCSqlSessionFactory.getInstance().getSession(null, true);
		}
		
	}
}
