package com.fliconz.fw.runtime.util;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.w3c.dom.Document;

import com.fliconz.fm.common.util.SecurityHelper;

public class LicenseReader {

	private Document doc;

	private String xmlPath;

	 

	public void check() throws Exception {
		read();
	}

	public String get(String field) throws Exception {
		return XmlUtil.getNodeText(doc, field);
	}

	public String getXmlPath() {
		return xmlPath;
	}

	public void setXmlPath(String path) {
		xmlPath = path;
	}

	public String getContent(String key) throws Exception {
		return XmlUtil.nodeToString(doc, key);
	}
	private void read() throws Exception {
		File xml = null;
		 
		xml = new File((xmlPath));
		if(!xml.exists()) {
			xml = new File((getClass().getResource(xmlPath).getFile()));
		}
		 
		//System.out.println(privateKey);
		doc = XmlUtil.getDocument(xml);
		String site = XmlUtil.getNodeText(doc, "site");
		String decStr = XmlUtil.nodeToString(doc, "decription");
		String key = XmlUtil.getNodeText(doc, "key");
		String expireDt = XmlUtil.getNodeText(doc, "expire-date");
		
		
		String enc = SecurityHelper.encryptSha256(decStr + site);
		//System.out.println( decStr + privateKey );
		//System.out.println(enc );
		
		if (!key.equals(enc)) {
			throw new Exception("invalid license");
		} else if (!"-1".equals(expireDt)) {
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(expireDt);
			if (date.before(new Date())) {
				throw new Exception("expired license");
			}
		}
		if(!site.equals("*")) {
			String site1 = PropertyManager.getString("site.code");
			if(!site.equals(site1)) {
				throw new Exception("invalid site");
			}	
		}
	}
	
	public static void main(String[] args) {
		LicenseReader reader = new LicenseReader();
		reader.setXmlPath("C:\\workspace\\fliconz_core\\src\\main\\java\\com\\fliconz\\fw\\runtime\\util\\license.xml");
		try {
			reader.check();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
