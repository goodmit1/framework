package com.fliconz.fw.runtime.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ListUtil {

	public static List makeJson2List(String json) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();

		List<Map<String, Object>> dtlList;
		dtlList = mapper.readValue(json, new TypeReference<List<Map<String, Object>>>(){});
		return dtlList;
	}
	public static void makeJson2List(List list, String jsonArrStr, Map commonData, String... fieldArr)
	{
		 
		JSONArray arr = new JSONArray(jsonArrStr);
		 for(int i=0; i < arr.length(); i++)
		 {
			 JSONArray arr1 = (JSONArray)arr.get(i);
			 Map data = new HashMap();
			 if(commonData != null) data.putAll(commonData);
			 int idx=0;
			 for(String field : fieldArr)
			 {
				 data.put(field, arr1.get(idx++));
			 }
			 list.add(data);
		 }
		 
	}
	public static void makeArr2List(List list, Object[][]arr, Map commonData, String... fieldArr)
	{
		 
		 for(int i=0; i < arr.length; i++)
		 {
			 Object[] arr1 =  arr[i];
			 Map data = new HashMap();
			 if(commonData != null) data.putAll(commonData);
			 int idx=0;
			 for(String field : fieldArr)
			 {
				 data.put(field, arr1[idx++]);
			 }
			 list.add(data);
		 }
		 
	}
	public static void setSize(List list, int size)
	{
		if(list==null) return;
		 
		while(list.size()>size)
		{
			list.remove(size);
		}
		 
	}
	public static void putAllNotExist( List<Map> list1, List<Map> list2, String compareField)
	{
		for(Map m : list2)
		{
			Object val = m.get(compareField);
			if(val == null) continue;
			boolean isFind=false;
			for(Map m2 : list1)
			{
				Object val2 = m2.get(compareField);
				
				if(val.equals(val2))
				{
					isFind = true;
					break;
				}
			}
			if(!isFind)
			{
				list1.add(m);
			}
		}
	}
	public static void merge(List<Map> list1, List<Map> list2, String mergeField, IMergeHandler mergeFun)
	{
		for(Map m : list1)
		{
			Object val = m.get(mergeField);
			if(val == null) continue;
			for(Map m2 : list2)
			{
				Object val2 = m2.get(mergeField);
				if(val.equals(val2))
				{
					mergeFun.merge(m, m2);
				}
			}
		}
	}
	private static void sum(Map result, Object key, Object val) 
	{
		Object sum1 = result.get(key);
		if(sum1==null)
		{
			result.put(key, val);
		}
		else
		{
			if(sum1 instanceof Float)
			{
				result.put(key, NumberUtil.getFloat(sum1)+NumberUtil.getFloat(val));
			}
			else
			{
				result.put(key, NumberUtil.getLong(sum1)+NumberUtil.getLong(val));
			}
		}
	}
	public static Map sum(List<Map> list , String keyField, String valField )
	{
		Map result = new HashMap();
		for(Map m : list)
		{
			 
			sum(result, m.get(keyField), valField.equals("1")?1:m.get(valField));
				
			 
		}
		return result;
	}
	public static Map sum(JSONArray list , String keyField, String valField )
	{
		Map result = new HashMap();
		for(int i=0; i < list.length(); i++)
		{
			JSONObject m = list.getJSONObject(i); 
			sum(result, m.get(keyField), valField.equals("1")?1:m.get(valField));
				
			 
		}
		return result;
	}
	public static List map2List(Map map, String keyName, String valName)
	{
		List result = new ArrayList();
		for(Object key: map.keySet())
		{
			Map m = new HashMap();
			m.put(keyName, key);
			m.put(valName, map.get(key));
			result.add(m);
		}
		return result;
	}
	public static void main(String[] args)
	{
		 List list = new ArrayList();
		 for(int i=0; i < 100; i++)
		 {
			 list.add(i);
		 }
		 ListUtil.setSize(list, 30);
		 System.out.println(list.size() + ":" + list);
	}
}
