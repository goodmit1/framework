package com.fliconz.fw.runtime.util;

public class ArrUtil {
	/**
	 * array를 ,로 이어서 string으로 바꿔준다.
	 * @param arr
	 * @return
	 */
	 public static String join(Object[] arr)
	  {
		  StringBuffer sb = new StringBuffer();
		  for(Object a: arr)
		  {
			  if(sb.length()>0) sb.append(",");
			  sb.append(a);
		  }
		  return sb.toString();
	  }
	 public static int indexOf(String[] arr, String str)
	  {
		 if(arr==null) return -1;
		 int len = arr.length;
		 for(int idx = 0; idx < len; idx++)
		 {
			 if(arr[idx].equals(str))
			 {
				 return idx;
			 }
			 
		 }
		 return -1;
	  }
	 public static String[] split(String str)
	  {
		 if(str==null || "".equals(str)) return null;
		  return str.split(",");
	  }
}
