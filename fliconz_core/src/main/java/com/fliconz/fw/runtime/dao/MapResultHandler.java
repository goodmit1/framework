package com.fliconz.fw.runtime.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;

import com.fliconz.fw.runtime.util.PropertyManager;

public class MapResultHandler implements ResultHandler {

	Map  result ;
	String keyField;
	String valField;
	public MapResultHandler(String keyField, String valField)
	{
		this();
		this.keyField = keyField;
		this.valField = valField;
		
	}
	public MapResultHandler()
	{
		result = new LinkedHashMap();
	}
	public Map getResult()
	{
		return result;
	}
	 
	public void handleResult(ResultContext context) {
		Map map = (Map)context.getResultObject();
		if(keyField == null && result.size()==0)
		{
			Iterator it = map.keySet().iterator();
			keyField = (String)it.next();
			valField = (String)it.next();
		}
		if(!PropertyManager.getBoolean("code_key_first", false))
		{
			result.put( map.get(valField).toString(),map.get(keyField).toString());
				
		}
		else
		{
			result.put( map.get(keyField).toString(),map.get(valField).toString());
				
		}
		
		 

	}

}
