package com.fliconz.fw.runtime.util;

import java.io.File;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class XmlUtil {

	public static String getNodeText(Document doc, String target) throws Exception{
		Node node = doc.getElementsByTagName(target).item(0);
		if(node == null){
			throw new Exception("invalid node name");
		}
		return node.getTextContent();
	}

	public static String nodeToString(Node node) throws Exception{
		StringWriter writer = new StringWriter();
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(new DOMSource(node), new StreamResult(writer));
		String output = writer.toString();
		return xmlFormat(output);
	}

	public static String nodeToString(Document doc, String target) throws Exception{
		Node node = doc.getElementsByTagName(target).item(0);
		StringWriter writer = new StringWriter();
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(new DOMSource(node), new StreamResult(writer));
		String output = writer.toString();
		return xmlFormat(output);
	}

	public static String xmlFormat(String xml) {
		if(xml.indexOf("\t") >= 0){
			xml = xml.replace("\t", "");
		}
		if(xml.indexOf("\n") >= 0){
			xml = xml.replace("\n", "");
		}
		return xml;
	}

	public static Document getDocument(File xml) throws Exception{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder.parse(xml);
	}
}
