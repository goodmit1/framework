package com.fliconz.fw.runtime.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.inject.Singleton;

import org.springframework.stereotype.Component;


@Singleton
@Component("propertyManager")
public class PropertyManager {

	private static Properties serverProps;

	Set cryptField;
	Set cryptTable;
	Set jsCrypeField;
	private Map<String,Map> codeMap;

	public   Set<String> getCryptFields() {
		if(cryptField == null){
			return null;
		}
		HashSet result = new HashSet();
		result.addAll(cryptField);
		return result;

	}
	public   Set<String> getCryptTables() {
		if(cryptTable == null){
			return null;
		}
		HashSet result = new HashSet();
		result.addAll(cryptTable);
		return result;


	}
	public   boolean isJsCrypeFields(String field) {

		return jsCrypeField != null && jsCrypeField.contains(field);

	}
	private static PropertyManager propertyManager = null;

	public static PropertyManager getInstance()
	{
		synchronized(PropertyManager.class)
		{
			if(propertyManager == null)
			{
				propertyManager = new PropertyManager();
			}
		}

		return propertyManager;
	}
	private PropertyManager()
	{
		if(serverProps == null) init();
		cryptField = new HashSet<String>();
		String[] temp = getStringArray("crypt_fields");
		if(temp != null) MapUtil.convertArrayToSet(cryptField,temp );
		cryptTable = new HashSet<String>();
		temp = getStringArray("crypt_tables");
		if(temp != null) MapUtil.convertArrayToSet(cryptTable,temp );
		jsCrypeField = new HashSet<String>();
		temp = getStringArray("security.js_encrypt_param");
		if(temp != null) MapUtil.convertArrayToSet(jsCrypeField,temp );
	}

	public static void init() {
		if(serverProps == null) serverProps = new Properties();
		else serverProps.clear();
		try {
			propertiesLoad("fliconz.properties");
			if(System.getProperty("site") != null)
			{
				serverProps.put("site.code", System.getProperty("site"))	;
			}
			if(serverProps.get("site.code") != null)
			{
				propertiesLoad("fliconz_" + serverProps.get("site.code") + ".properties");
			}
			if(getBoolean("password.security.invalidWords.use")){
				propertiesLoad(getString("password.security.invalidWords.fileName", "prohibitedWords.properties"));
			}
		} catch(Exception e){

		}
	}

	private static void propertiesLoad(String propertiesFileName) throws Exception {

		java.io.InputStream in = null;
		try {
			in = PropertyManager.class.getClassLoader().getResourceAsStream("config/" + propertiesFileName);
			Properties props = new Properties();
			props.load(new java.io.InputStreamReader(in, "UTF-8"));
			serverProps.putAll(props);
			props = null;
		}
		finally {
			try {
				if (in != null) {
					in.close();
				}
			}
			catch (Exception e) {}
		}
	}

	public static Object get(String key){
		if(serverProps == null) init();
		Object obj = serverProps.get(key);
		return (obj == null || "".equals(obj.toString())) ? null : obj;
	}


	public static String getString(String key){
		return getString(key, null);
	}

	public static String getString(String key, String defaultValue){
		Object obj = get(key);
		if(obj == null) return defaultValue;
		else return (String)obj;
	}

	public static String[] getStringArray(String key){
		return getStringArray(key, null);
	}

	public static String[] getStringArray(String key, String[] defaultValue){
		return getStringArray(key, ",", defaultValue);
	}

	public static String[] getStringArray(String key, String delemiter, String[] defaultValue){
		String string = getString(key);
		if(string == null) return defaultValue;
		else {
			return string.split(delemiter);
		}
	}

	public static int getInt(String key){
		return getInt(key, 0);
	}

	public static int getInt(String key, int defaultValue){
		Object obj = get(key);
		if(obj == null) return defaultValue;
		else return Integer.parseInt(obj.toString());
	}

	public static long getLong(String key){
		return getLong(key, 0);
	}

	public static long getLong(String key, long defaultValue){
		Object obj = get(key);
		if(obj == null) return defaultValue;
		else return Long.parseLong(obj.toString());
	}

	public static List<String> getList(String key){
		return getList(key, null);
	}

	public static List<String> getList(String key, List<String> defaultValue){
		String[] array = getStringArray(key);
		if(array == null) return defaultValue;
		else {
			List<String> list = new ArrayList<String>();
			for(String value: array){
				list.add(value);
			}
			return list;
		}
	}

	public static boolean getBoolean(String key, boolean defaultValue) {
		String text = getString(key);
		if(text == null) return defaultValue;
		else return new Boolean(text);
	}

	public static boolean getBoolean(String key) {
		return getBoolean(key, false);
	}

}
