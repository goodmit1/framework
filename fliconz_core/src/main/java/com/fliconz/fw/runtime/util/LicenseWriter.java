package com.fliconz.fw.runtime.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fliconz.fm.common.util.SecurityHelper;

public class LicenseWriter {

	public static void main(String[] args) {
		if(args.length!=1) {
			System.out.println("fileName required");
			return;
		}
		String fileName = args[0]; 
		 
		File file = new File(fileName);
		 
		Document doc;
		try {
			doc = XmlUtil.getDocument(file);
		
			String decStr = XmlUtil.nodeToString(doc, "decription");
			String site = XmlUtil.getNodeText(doc, "site");
			//System.out.println(privateKey);
			// String key = XmlUtil.getNodeText(doc, "key");
			String enc = SecurityHelper.encryptSha256(decStr + site);
			//System.out.println( decStr + privateKey );
			//System.out.println( enc );
			
			NodeList list = doc.getElementsByTagName("key");
			Node key = null;
			if(list == null ||  list.getLength() == 0) {
				key =  doc.createElement("key");
				doc.getFirstChild().appendChild(key);
			}
			else {
				key = list.item(0);
			}
			key.setTextContent(enc);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	         
	        StreamResult result = new StreamResult(file);
		    transformer.transform(source, result);
		     
		    LicenseReader reader = new LicenseReader();
			reader.setXmlPath(fileName);
		 
			reader.check();
			System.out.println("OK");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
