package com.fliconz.fw.runtime.util;

import java.util.HashMap;
import java.util.List;

/**
 * null이면 remove하는 map
 * @author 윤경
 *
 */
public class NullSkipMap extends HashMap{

	@Override
	public Object put(Object arg0, Object arg1) {
		if(arg1==null || "".equals(arg1))
		{
			return super.remove(arg0);
		}
		else if(arg1 instanceof String[])
		{
			String[] l = (String[])arg1;
			if(l.length == 0)
			{
				return super.remove(arg0);
			}
		}
		else if(arg1 instanceof List)
		{
			List l = (List)arg1;
			if(l.size()==0)
			{
				return super.remove(arg0);
			}
		}
		return super.put(arg0, arg1);

	}

}
