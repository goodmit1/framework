package com.fliconz.fw.runtime.util;

import org.apache.ibatis.session.Configuration;

import com.fliconz.fw.runtime.dao.BaseDAO;

public class QueryKeyHelper {

	/**
	 * 기본 쿼리키를 받아서, 현재 사용중인 DB타입의 쿼리를 찾은 후, 파라미터로 받은 defaultQueryKey를 DB에 맞게 변경해준다.
	 * @param configuration
	 * @param defaultQueryKey
	 * @return
	 */
	public static String queryKeyCheckByDB(Configuration configuration, String defaultQueryKey){
		if(configuration == null || defaultQueryKey == null || "".equals(defaultQueryKey)) return defaultQueryKey;
		if(configuration.hasStatement(defaultQueryKey + "_" + PropertyManager.getString("server.use.dbtype"))){
			defaultQueryKey = defaultQueryKey + "_" + PropertyManager.getString("server.use.dbtype");
		}
		return defaultQueryKey;
	}

	public static String queryKeyCheckByDB(BaseDAO dao, String defaultQueryKey) throws Exception{
		Configuration configuration = dao.getSqlSession().getConfiguration();
		return queryKeyCheckByDB(configuration, defaultQueryKey);
	}

	/**
	 * 기본 쿼리키를 받아서, 현재 사용중인 DB타입의 쿼리를 찾은 후, 파라미터로 받은 defaultQueryKey를 DB에 맞게 변경해준다.
	 * @param configuration
	 * @param defaultQueryKey
	 * @return
	 */
	public static String queryKeyCheckByDB(Configuration configuration, String namespace, String defaultQueryKey){
		if(namespace != null && !"".equals(namespace)){
			defaultQueryKey = String.format("%s.%s", namespace, defaultQueryKey);
		}
		return queryKeyCheckByDB(configuration, defaultQueryKey);
	}

	public static String queryKeyCheckByDB(BaseDAO dao, String namespace, String defaultQueryKey) throws Exception{
		Configuration configuration = dao.getSqlSession().getConfiguration();
		return queryKeyCheckByDB(configuration, namespace, defaultQueryKey);
	}
}
