package com.fliconz.fm.connectlog;

import java.io.Serializable;
import java.util.Map;

import javax.inject.Singleton;

import org.springframework.stereotype.Service;

import com.fliconz.fm.common.BaseService;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Singleton
@Service(value="connectLogService")
public class ConnectLogManager extends BaseService implements Serializable {

	private static final long serialVersionUID = 1L;

	private static enum LOG_TABLE_COLUMNS { PRGM_ID, PRGM_NM, USER_ID, USER_NAME, TEAM_CD, TEAM_NM, IPADDRESS }

	private static ConnectLogManager instance;

	@Override
	protected String getNameSpace() {
		return "com.fliconz.fm.connectlog.ConnectLog";
	}

	private static ConnectLogManager getInstance(){
		if(instance == null) instance = (ConnectLogManager)SpringBeanUtil.getBean("connectLogService");
		return instance;
	}

    public static void loggingDB(Class<?> clazz, Map<String, Object> paramMap) throws Exception{
    	if(PropertyManager.getBoolean("connectLog.use", true)) {
	    	paramMap.put("INS_PGM", clazz.getName());
	    	ConnectLogManager.validationAndTuningParamMap(paramMap);
	    	ConnectLogManager.getInstance().insertByQueryKey(ConnectLogConstant.INSERT_QUERY_KEY, paramMap);
    	}
    }

    private static void validationAndTuningParamMap(Map<String, Object> paramMap){
		for(int i = 0 ; i < LOG_TABLE_COLUMNS.values().length; i++){
			String key = LOG_TABLE_COLUMNS.values()[i].toString();
			if(!paramMap.containsKey(key) || paramMap.get(key) == null) {
				paramMap.put(key, "");
			}
		}
	}


}
