package com.fliconz.fm.connectlog;

import com.fliconz.fw.runtime.util.PropertyManager;

public class ConnectLogConstant {

	public static final int LOGIN_PRGM_ID = PropertyManager.getInt("connectLog.login.prgmId", 0);
	public static final String LOGIN_PRGM_NM = PropertyManager.getString("connectLog.login.prgmNm", "LOGIN");
	public static final String INSERT_QUERY_KEY = PropertyManager.getString("connectLog.login.queryKey", "insertLog");
}
