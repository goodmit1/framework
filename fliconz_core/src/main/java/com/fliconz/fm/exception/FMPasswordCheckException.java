package com.fliconz.fm.exception;

import org.springframework.security.core.AuthenticationException;

public class FMPasswordCheckException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	//~ Constructors ===================================================================================================

    /**
     * Constructs a <code>FMPasswordInvaildException</code> with the specified
     * message.
     *
     * @param msg the detail message.
     */
    public FMPasswordCheckException(String msg) {
        super(msg);
    }

    /**
     * Constructs a {@code FMPasswordInvaildException}, making use of the {@code extraInformation}
     * property of the superclass.
     *
     * @param msg the detail message
     * @param extraInformation additional information such as the username.
     */
    @Deprecated
    public FMPasswordCheckException(String msg, Object extraInformation) {
        super(msg, extraInformation);
    }

    /**
     * Constructs a {@code FMPasswordInvaildException} with the specified message and root cause.
     *
     * @param msg the detail message.
     * @param t root cause
     */
    public FMPasswordCheckException(String msg, Throwable t) {
        super(msg, t);
    }

}
