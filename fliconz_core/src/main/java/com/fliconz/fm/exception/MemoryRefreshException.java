package com.fliconz.fm.exception;

public class MemoryRefreshException extends Exception{

	private static final long serialVersionUID = 1L;

	private String code;

	public MemoryRefreshException() {}

	public MemoryRefreshException(String code, String msg){
		super(msg);
		this.code = code;
	}

	public String getCode(){
		return this.code;
	}
}
