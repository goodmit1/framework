package com.fliconz.fm.exception;

public class ExceptionConstants {

	public static final int USERNAME_NOT_FOUND_EXCEPTION = 0;
	public static final int SNS_NOT_LINKED_EXCEPTION = 1;
	public static final int BAD_CREDENTIALS_EXCEPTION = 2;
	public static final int SNS_INFO_NOT_MATCHED_EXCEPTION = 3;


	public static final int EXCEPTION = 99;

}
