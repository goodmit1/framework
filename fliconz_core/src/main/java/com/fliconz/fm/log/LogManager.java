package com.fliconz.fm.log;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fliconz.fm.common.BaseService;

@Singleton
@Component("logManager")
public class LogManager extends BaseService {

	public static final String LOGGER_CONSOLE = "console";
	public static final String LOGGER_AUTHENTICATION = "authentication";
	public static final String LOGGER_SECURITY = "security";
	public static final String LOGGER_JASPER_REPORT = "jasperReports";
	public static final String LOGGER_DELETE_DB = "deleteDB";

	private static LogManager instance;

	public LogManager() {
		instance = this;
	}

	@Override
	protected String getNameSpace() {
		return "com.fliconz.fm.log.Log";
	}

	@PostConstruct
	public void init() throws Exception {
		if (true) {

		}
	}

	@SuppressWarnings("unchecked")
	private String loadLogConfig() throws Exception {
		StringBuffer sb = new StringBuffer();
		List<LoggerVO> allLoggerList = (List<LoggerVO>) super.selectListByQueryKey(LogConstant.LOGGER_ALL_KEY, null);
		for(LoggerVO logger: allLoggerList){
			if(!"Y".equals(logger.getUse_yn())) continue;
			sb.append("log4j.logger." + logger.getLogger() + "=" + logger.getLogger_level());
			if(logger.getAppender() != null && logger.getFile_path() != null && logger.getDate_pattern() != null &&
					logger.getThreshold() != null && logger.getLayout() != null && logger.getLayout_pattern() != null) {
				sb.append(", " + logger.getAppender());
				sb.append("\n");

				sb.append("\nlog4j.appender." + logger.getAppender() + "=" + logger.getClass_name());
				sb.append("\nlog4j.appender." + logger.getAppender() + ".File=" + logger.getFile_path());
				sb.append("\nlog4j.appender." + logger.getAppender() + ".DatePattern=" + logger.getDate_pattern());
				sb.append("\nlog4j.appender." + logger.getAppender() + ".Threshold=" + logger.getThreshold());
				sb.append("\nlog4j.appender." + logger.getAppender() + ".layout=" + logger.getLayout());
				sb.append("\nlog4j.appender." + logger.getAppender() + ".layout.ConversionPattern=" + logger.getLayout_pattern());
				if(logger.getDate_pattern() != null) sb.append("\nlog4j.appender." + logger.getAppender() + ".datePattern=" + logger.getDate_pattern());
				sb.append("\n");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

    public static void configure(HttpServletRequest request){
	    	try {
	    		if(request != null){
	    			String config = instance.loadLogConfig();
	    			String log4jConfigLocation = (String)request.getServletContext().getAttribute("log4jConfigLocation");
	    			if (log4jConfigLocation == null) {
	    				log4jConfigLocation = (String)request.getServletContext().getAttribute("log4jConfigLocation");
	    			}
	    			if (log4jConfigLocation == null) {
	    				log4jConfigLocation = "/WEB-INF/config/log4j.properties";
	    			}
	    			String template = request.getServletContext().getRealPath(String.format("%s.template", log4jConfigLocation));
	    			FileReader reader = new FileReader(template);
	    			StringBuffer sb = new StringBuffer();
	    			while(true){
	    				int data = reader.read();
	    				if(data < 0){
	    					break;
	    				}
	    				char ch = (char)data;
	    				sb.append(ch);
	    			}
	    			reader.close();
	    			sb.append("\n");
	    			sb.append(config);
	    			String configFile = request.getServletContext().getRealPath(log4jConfigLocation);
	    			FileWriter writer = new FileWriter(configFile, false);
	    			writer.write(sb.toString());
	    			writer.close();

	    			PropertyConfigurator.configure(configFile);
	    		}
	    	} catch(Exception e){
	    		e.printStackTrace();
	    	}
    }


    @Deprecated
    public static Logger getAuthenticationLogger(){
    	return LoggerFactory.getLogger(LOGGER_AUTHENTICATION);
    }

    @Deprecated
    public static Logger getDeleteDBLogger()
    {
    	return LoggerFactory.getLogger(LOGGER_DELETE_DB);
    }
    
    @Deprecated
    public static Logger getSecurityLogger(){
    	return LoggerFactory.getLogger(LOGGER_SECURITY);
    }

    @Deprecated
    public static Logger getJasperReportLogger(){
    	return LoggerFactory.getLogger(LOGGER_JASPER_REPORT);
    }

    public static void trace(String process, String msg){
    	Logger logger = LoggerFactory.getLogger(process);
    	if(logger != null && logger.isTraceEnabled()){
    		logger.trace(msg);
    	}
    }

    public static void debug(String process, String msg){
    	Logger logger = LoggerFactory.getLogger(process);
    	if(logger != null && logger.isDebugEnabled()){
    		logger.debug(msg);
    	}
    }

    public static void info(String process, String msg){
    	Logger logger = LoggerFactory.getLogger(process);
    	if(logger != null && logger.isInfoEnabled()){
    		logger.info(msg);
    	}
    }

    public static void warn(String process, String msg){
    	Logger logger = LoggerFactory.getLogger(process);
    	if(logger != null && logger.isWarnEnabled()){
    		logger.warn(msg);
    	}
    }

    public static void error(String process, String msg){
    	Logger logger = LoggerFactory.getLogger(process);
    	if(logger != null && logger.isErrorEnabled()){
    		logger.error(msg);
    	}
    }

    public static void trace(String msg){
    	trace(LOGGER_CONSOLE, msg);
    }

    public static void debug(String msg){
    	debug(LOGGER_CONSOLE, msg);
    }

    public static void info(String msg){
    	info(LOGGER_CONSOLE, msg);
    }

    public static void warn(String msg){
    	warn(LOGGER_CONSOLE, msg);
    }

    public static void error(String msg){
    	error(LOGGER_CONSOLE, msg);
    }



}
