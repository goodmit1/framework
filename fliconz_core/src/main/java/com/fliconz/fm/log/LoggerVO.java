package com.fliconz.fm.log;

import java.io.Serializable;

public class LoggerVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String logger;
	private String logger_level;
	private String use_yn;
	private String appender;
	private String class_name;
	private String file_path;
	private String threshold; 
	private String layout;
	private String layout_pattern;
	private String date_pattern;
	
	public LoggerVO() { }

	public String getLogger() {
		return logger;
	}

	public void setLogger(String logger) {
		this.logger = logger;
	}

	public String getLogger_level() {
		return logger_level;
	}

	public void setLogger_level(String logger_level) {
		this.logger_level = logger_level;
	}

	public String getUse_yn() {
		return use_yn;
	}

	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}

	public String getAppender() {
		return appender;
	}

	public void setAppender(String appender) {
		this.appender = appender;
	}

	public String getClass_name() {
		return class_name;
	}

	public void setClass_name(String class_name) {
		this.class_name = class_name;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getThreshold() {
		return threshold;
	}

	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getLayout_pattern() {
		return layout_pattern;
	}

	public void setLayout_pattern(String layout_pattern) {
		this.layout_pattern = layout_pattern;
	}

	public String getDate_pattern() {
		return date_pattern;
	}

	public void setDate_pattern(String date_pattern) {
		this.date_pattern = date_pattern;
	}

}
