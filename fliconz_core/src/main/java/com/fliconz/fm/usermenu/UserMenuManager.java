package com.fliconz.fm.usermenu;

import java.util.List;

 

import com.fliconz.fm.common.vo.TreeNodeMap;

public interface UserMenuManager {

	public void recursiveMakeUserMenu(TreeNodeMap root, Object rootMenu) throws CloneNotSupportedException;

	public Object getLeftMenuModel() throws Exception;
	
	public Object getAllMenuModel() throws Exception;
	
	public Object getLeftMyMenuModel() throws Exception;

	public Object getLeftMobileMenuModel() throws Exception;

	public Object getLeftMobileMyMenuModel() throws Exception;

	public boolean isMenuClicked();
	public void setMenuClicked(boolean isMenuClicked);

	public boolean isMyMenuClicked();
	public void setMyMenuClicked(boolean isMyMenuClicked);

	public void addLogoutMenu(Object menuModel);

	public void reloadMenu() throws Exception;

	public TreeNodeMap getFirstSubMenu();

	public TreeNodeMap getMobileFirstSubMenu();

	public int getSelectedFirstMenu();

	public void setSelectedFirstMenu(int selectedFirstMenu);

	public void findSelectedFirstMenu(TreeNodeMap subMenu);

	/**
	 * 현재 선택된 Leaf FMMenuVO 오브젝트
	 */
	public TreeNodeMap getSelectedMenu();

	public void setSelectedMenu(TreeNodeMap selectedMenu);

	public String getSelectedMenuName();

	/**
	 * RequestURI로 메뉴를 찾아 리턴한다.
	 * @param requestURI
	 * @return
	 */
	public TreeNodeMap findMenuByPrgmPath(String requestURI);

	public TreeNodeMap findMobileMenuByPrgmPath(String requestURI);

	/**
	 * menuId(Integer)로 메뉴를 찾아 리턴한다.
	 * @param menuId
	 * @return
	 */
	public TreeNodeMap findMenuById(int menuId);

	public TreeNodeMap findMobileMenuById(int menuId);

	/**
	 * RequestURI로 마이메뉴를 찾아 리턴한다.
	 * @param requestURI
	 * @return
	 */
	public TreeNodeMap findMyMenuByPrgmPath(String requestURI);

	public TreeNodeMap findMobileMyMenuByPrgmPath(String requestURI);

	/**
	 * menuId(Integer)로 마이메뉴를 찾아 리턴한다.
	 * @param menuId
	 * @return
	 */
	public TreeNodeMap findMyMenuById(int menuId);

	public TreeNodeMap findMobileMyMenuById(int menuId);

	public String getSelectedParentMenuName();

	public void reloadMyMenu() throws Exception;

	public TreeNodeMap getMyMenu(int menuId);

	public String getDefaultPage();

	public void setDefaultPage(String defaultPage);

	public String getMobileDefaultPage();

	public void setMobileDefaultPage(String mobileDefaultPage);

	public boolean isChangedFirstMenu();
	public void setChangedFirstMenu(boolean isChangedFirstMenu);

	public boolean isCacheUpdated();
	
	public boolean isMyMenuUpdated();

	public boolean isAdminMenuSelected();

	public List<TreeNodeMap> getFirstMenuList();

	 

	int getParentId(int id);
}
