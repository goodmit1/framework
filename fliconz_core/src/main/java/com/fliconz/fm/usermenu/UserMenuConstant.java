package com.fliconz.fm.usermenu;

import com.fliconz.fw.runtime.util.PropertyManager;

public class UserMenuConstant {

	public static final String LEAF_MENU_LIST_QUERY_KEY = PropertyManager.getString("login.leafMenuList.customQueryKey","com.fliconz.fm.usermenu.UserMenu.leafMenuList");
	
	public static final String MY_MENU_LIST_QUERY_KEY = "myMenuList";

	public static final String FIRST_CHILD_MENU_QUERY_KEY = "firstChildMenu";

	public static final String INSERT_MY_MENU_QUERY_KEY = "insertMyMenu";
	public static final String DELETE_MY_MENU_QUERY_KEY = "deleteMyMenu";

}
