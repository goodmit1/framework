package com.fliconz.fm.usermenu;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.common.BaseService;

@Service(value="userMenuService")
public class UserMenuService extends BaseService {

	@Override
	protected String getNameSpace() {
		return "com.fliconz.fm.usermenu.UserMenu";
	}

	public List<Integer> selectLeafMenuListFromUser(Map<String, Object> paramMap) throws Exception{
		this.addCommonParam(paramMap);
		List<Integer> leafMenuList = super.getDAO().selectList(UserMenuConstant.LEAF_MENU_LIST_QUERY_KEY, paramMap);
		return leafMenuList;
	}

	public List<Integer> selectMyMenuListFromUser(Map<String, Object> paramMap) throws Exception{
		List<Integer> myMenuList = super.selectListByQueryKey(UserMenuConstant.MY_MENU_LIST_QUERY_KEY, paramMap);
		return myMenuList;
	}

	public void addMyMenu(Map<String, Object> paramMap) throws Exception {
		super.insertByQueryKey(UserMenuConstant.INSERT_MY_MENU_QUERY_KEY, paramMap);
	}

	public void removeMyMenu(Map<String, Object> paramMap) throws Exception {
		super.deleteByQueryKey(UserMenuConstant.DELETE_MY_MENU_QUERY_KEY, paramMap);
	}
}
