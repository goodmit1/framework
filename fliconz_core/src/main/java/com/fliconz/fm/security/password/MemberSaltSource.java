package com.fliconz.fm.security.password;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

import javax.inject.Singleton;

import org.springframework.beans.BeanUtils;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.dao.ReflectionSaltSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;

import com.fliconz.fw.runtime.util.PropertyManager;

@Singleton
public class MemberSaltSource extends ReflectionSaltSource {

	private static MemberSaltSource instance = new MemberSaltSource();

	public static MemberSaltSource getInstance(){
		return instance;
	}

   //~ Instance fields ================================================================================================

   private String userPropertyToUse = PropertyManager.getString("password.security.userPropertyToUse", "loginId");

   //~ Methods ========================================================================================================

   public void afterPropertiesSet() throws Exception {
       Assert.hasText(userPropertyToUse, "A userPropertyToUse must be set");
   }

   /**
    * Performs reflection on the passed <code>User</code> to obtain the salt.
    * <p>
    * The property identified by <code>userPropertyToUse</code> must be available from the passed <code>User</code>
    * object. If it is not available, an {@link AuthenticationServiceException} will be thrown.
    *
    * @param user which contains the method identified by <code>userPropertyToUse</code>
    *
    * @return the result of invoking <tt>user.userPropertyToUse()</tt>, or if the method doesn't exist,
    * <tt>user.getUserPropertyToUse()</tt>.
    *
    * @throws AuthenticationServiceException if reflection fails
    */
   public Object getSalt(UserDetails user) {
       Method saltMethod = findSaltMethod(user);

       try {
           return saltMethod.invoke(user);
       } catch (Exception exception) {
           throw new AuthenticationServiceException(exception.getMessage(), exception);
       }
   }

   private Method findSaltMethod(UserDetails user) {
       Method saltMethod = ReflectionUtils.findMethod(user.getClass(), userPropertyToUse, new Class[0]);

       if (saltMethod == null) {
           PropertyDescriptor pd = BeanUtils.getPropertyDescriptor(user.getClass(), userPropertyToUse);

           if (pd != null) {
               saltMethod = pd.getReadMethod();
           }

           if (saltMethod == null) {
               throw new AuthenticationServiceException("Unable to find salt method on user Object. Does the class '" +
                   user.getClass().getName() + "' have a method or getter named '" + userPropertyToUse + "' ?");
           }
       }

       return saltMethod;
   }

   protected String getUserPropertyToUse() {
       return userPropertyToUse;
   }

   /**
    * The method name to call to obtain the salt. Can be either a method name or a bean property name. If your
    * <code>UserDetails</code> contains a <code>UserDetails.getSalt()</code> method, you should set this property to
    * "getSalt" or "salt".
    *
    * @param userPropertyToUse the name of the <b>getter</b> to call to obtain the salt from the
    *        <code>UserDetails</code>
    */
   public void setUserPropertyToUse(String userPropertyToUse) {
       this.userPropertyToUse = userPropertyToUse;
   }

   public String toString() {
       return "ReflectionSaltSource[ userPropertyToUse='" + userPropertyToUse + "'; ]";
   }
}
