package com.fliconz.fm.security;


import java.util.Map;

import com.fliconz.fm.common.vo.BaseVO;

public class TeamVO extends BaseVO {

	private static final long serialVersionUID = 1L;

	private String compId;
	private Object teamCd;
	private String teamNm;
	private String useYn;

	public TeamVO(){ }

	public TeamVO(Map<String, Object> resultMap){
		if(resultMap.get("TEAM_CD") != null)
		{
		
			this.teamCd =  resultMap.get("TEAM_CD");
			this.teamNm = (String)resultMap.get("TEAM_NM") == null ? (String)resultMap.get("dept_nm") : (String)resultMap.get("TEAM_NM");
			this.useYn = (String)resultMap.get("TEAM_USE_YN") == null ? (String)resultMap.get("dept_state") : (String)resultMap.get("TEAM_USE_YN");
		}
		else
		{
			this.useYn = "N";
		}
		this.compId = (String)resultMap.get("COMP_ID");
	}

	public String getCompId() {
		return compId;
	}

	public void setCompId(String compId) {
		this.compId = compId;
	}

	public Object getTeamCd() {
		return teamCd;
	}

	public void setTeamCd(int teamCd) {
		this.teamCd = teamCd;
	}

	public String getTeamNm() {
		return teamNm;
	}

	public void setTeamNm(String teamNm) {
		this.teamNm = teamNm;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUsageYn(String useYn) {
		this.useYn = useYn;
	}

	public String toString(){
		return "Team [teamCd=" + this.teamCd + "], teamNm=["+this.teamNm+"]";
	}
}
