package com.fliconz.fm.security;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.fliconz.fm.common.util.TextHelper;

public class ExtChkMultipartResolver extends CommonsMultipartResolver{
	public ExtChkMultipartResolver() {
		super();
	}
	String[] exts;
	public void setExcludeExt(String[] exts) {
		this.exts = exts;
	}
	
	@Override
	protected MultipartParsingResult parseRequest(HttpServletRequest request) throws MultipartException {
		MultipartParsingResult result = super.parseRequest(request);
		MultiValueMap<String, MultipartFile> files = result.getMultipartFiles();
		Set<String> keys = files.keySet();
		for(String key : keys) {
			List<MultipartFile> fileList = files.get(key);
			for(int i=0 ; i < fileList.size(); i++) {
				String fileName = fileList.get(i).getOriginalFilename();
				int pos = fileName.lastIndexOf(".");
				if(pos>0) {
					String ext = fileName.substring(pos+1);
					if(TextHelper.indexOf(exts, ext)>=0) {
						throw new MultipartException("Invalid extension");
					}
				}
			}
		
			
		}
		return result;
	}
}
