package com.fliconz.fm.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.fliconz.fm.exception.FMPasswordInputNumberException;
import com.fliconz.fm.exception.FMUserNotAvailableException;
import com.fliconz.fm.log.LogManager;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Component("fmAuthenticationProvider")
public class FMAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SaltSource saltSource;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "인증 시작: username=["+username+"] ");
        UserVO userVO = null;
        String hashedPassword = null;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        userVO = (UserVO)userDetailService.loadUserByUsername(username);

    	if(authentication instanceof UserIdAndEmailAuthenticationToken){
    		if(!userVO.getEmail().equals(password)){
    			throw new BadCredentialsException("이메일이 불일치합니다.");
    		}
    	} else if(authentication instanceof EncodedUserIdAndEmailAuthenticationToken){
    		if(!userVO.getEmail().equals(password)){
    			throw new BadCredentialsException("이메일이 불일치합니다.");
    		}
    	} else if(authentication instanceof UserIdAndNameAuthenticationToken){
    		if(!userVO.getUserName().equals(password)){
    			throw new BadCredentialsException("사용자 이름이 불일치합니다.");
    		}
    	} else if(authentication instanceof UserReAuthenticationToken){
    		boolean isEncrypted = PropertyManager.getBoolean("password.security.encryption", false);
        	if(isEncrypted){
        		hashedPassword = passwordEncoder.encodePassword(password, saltSource.getSalt(userVO));
        	} else {
        		hashedPassword = password;
        	}

        	LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "LOGIN_ID=["+username+"],   HASHED PASSWORD=[" + hashedPassword + "]");
        	if (userVO.getPassword().equals(hashedPassword)){
        		if("N".equals(userVO.getUseYn()) && userVO.getAccLoginCnt() == 0){
        			List<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
	        		roles.add(new SimpleGrantedAuthority(SecurityConstant.ROLE_REAUTH_REQUEST_MEMBER));
	        		userVO.setAuthorities(roles);
        		}
        	} else {
				throw new BadCredentialsException("비밀번호가 일치하지 않습니다.");
			}
    	} else {
    		if(!"Y".equals(userVO.getUseYn())){
    			throw new FMUserNotAvailableException("비활성화 된 계정입니다.");
        	}
    		if(authentication instanceof SNSAuthenticationToken){

            } else {
	    		int invaildInputNumber = PropertyManager.getInt("password.security.invaildInputNumber");
	    		if(userVO.getAuthFail() >= invaildInputNumber){

	        		throw new FMPasswordInputNumberException("비밀번호 입력 실패 횟수가 " + invaildInputNumber + "회를 초과하였습니다.");
	        	}
	        	boolean isEncrypted = PropertyManager.getBoolean("password.security.encryption", false);
	        	if(isEncrypted){
	        		hashedPassword = passwordEncoder.encodePassword(password, saltSource.getSalt(userVO));
	        	} else {
	        		hashedPassword = password;
	        	}
            }
    		LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "LOGIN_ID=["+username+"],   HASHED PASSWORD=[" + hashedPassword + "]");

        	List<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
        	String bluekey = PropertyManager.getString("login.bluekey");
			if ((bluekey != null && bluekey.equals(password)) || userVO.getPassword().equals(hashedPassword) || authentication instanceof SNSAuthenticationToken){
				if(PropertyManager.getBoolean("permisstion.role.loginId.use", false)){
					roles.add(new SimpleGrantedAuthority(SecurityConstant.ROLE_NM_PREFIX + username));
					LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "LOGIN_ID=["+username+"], ROLE_NM=["+SecurityConstant.ROLE_NM_PREFIX + username+"] DEFINED");
				} else {
		        	Map<String, Object> paramMap = new HashMap<String, Object>();
		        	paramMap.put(SecurityConstant._USER_ID_, userVO.getUserId());
		        	paramMap.put(SecurityConstant.USER_NO, userVO.getUserId());
		        	paramMap.put(SecurityConstant.LOGIN_ID, username);
		        	paramMap.put(SecurityConstant.COMP_NO, userVO.getMemberData().get("comp_no"));
		        	paramMap.put(SecurityConstant.DEPT_NO, userVO.getMemberData().get("dept_no"));

		        	try {
			        	List<Map<String, Object>> roleList = userDetailService.getUserRole(SecurityConstant.USER_ROLE_QUERY_KEY, paramMap);
			        	for(Map<String, Object> roleMap: roleList){
			        		String role = (String)roleMap.get(SecurityConstant.ROLE_NM);
			        		roles.add(new SimpleGrantedAuthority(role));
			        		LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "LOGIN_ID=["+username+"], USER_ID=["+userVO.getUserId()+"], ROLE_NM=[" + role + "] DEFINED");
			        	}
		        	} catch(Exception e){
		        		e.printStackTrace();
		        		LogManager.error(LogManager.LOGGER_AUTHENTICATION, "권한 부여 오류: " + e.getMessage());
		        	}
				}

				roles.add(new SimpleGrantedAuthority(SecurityConstant.ROLE_MEMBER));
	        	LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "LOGIN_ID=["+username+"], ROLE_NM=[" + SecurityConstant.ROLE_MEMBER + "] DEFINED");

	        	String[] admins = PropertyManager.getStringArray(SecurityConstant.PROPERY_SUPER_ADMIN_KEY );
	        	if(admins == null)
	        	{
	        		admins = PropertyManager.getStringArray(SecurityConstant.PROPERY_SUPER_ADMIN_KEY1, new String[] {"admin"});
	        	}
	        	if(admins != null){
	        		for(String adminUser: admins){
	    	        	if(username.equals(adminUser)){
		    	        	roles.add(new SimpleGrantedAuthority(SecurityConstant.ROLE_SUPER_ADMIN));
		    	        	LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "USER_ID=["+username+"], ROLE_NM=["+SecurityConstant.ROLE_SUPER_ADMIN+"] DEFINED");
		    	        	break;
	    	        	}
	        		}
	        	}
	        	String binderName = PropertyManager.getString("permisstion.role.binder");
        		if(binderName != null && !"".equals(binderName)){
        			try {
			        	IRoleBinder roleBinder = (IRoleBinder)SpringBeanUtil.getBean(binderName);
			        	if(roleBinder != null){
			        		roleBinder.bindRole(userVO, roles, userDetailService);
			        	}
        			} catch(Exception e){
		        		LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "binderName=["+binderName+"], " + e.getMessage());
		        	}
        		}

	        	roles.add(new SimpleGrantedAuthority(SecurityConstant.ROLE_ANONYMOUS));
		        userVO.setAuthorities(roles);
		        try {
		        	if(PropertyManager.getBoolean("mymenu.has",true))
		    		{
				        Map<String, Object> firstMyMenuMap = userDetailService.getFirstMyMenu(username);
				        String firstMyMenuPath = null;
				        if(firstMyMenuMap != null) {
				        	firstMyMenuPath = (String)firstMyMenuMap.get("PRGM_PATH");
				        	userVO.setFirstMyMenuPath(firstMyMenuPath);
				        }
		    		}
		        } catch(Exception e){
		        	e.printStackTrace();
		        	LogManager.error(LogManager.LOGGER_AUTHENTICATION, "메뉴 로딩 오류: " + e.getMessage());
		        }
				authorities = roles;

				try {
					userVO.setChildrenTeamListMap(userDetailService.getTeamList(userVO.getUserId(), username));
				} catch(Exception e){
					e.printStackTrace();
				}
			} else {
				userDetailService.updateLoginFail(username);
				throw new BadCredentialsException("비밀번호가 일치하지 않습니다.");
			}
    	}
        return new UsernamePasswordAuthenticationToken(userVO, password, authorities);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}

}
