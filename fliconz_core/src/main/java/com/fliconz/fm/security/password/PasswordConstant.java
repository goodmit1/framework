package com.fliconz.fm.security.password;

import com.fliconz.fw.runtime.util.PropertyManager;

public class PasswordConstant {

	protected static final String UPDATE_PASSWORD_QUERY_KEY = "updatePassword";
	protected static final String UPDATE_USE_YN_QUERY_KEY = PropertyManager.getString("member_use_yn.customQueryKey","com.fliconz.fm.security.password.Password.updateUseYn");;

	protected static final String MAIL_SMTP_HOST = PropertyManager.getString("mail.smtp.host");
	protected static final String MAIL_SMTP_START_TLS_ENABLE = PropertyManager.getString("mail.smtp.starttls.enable");
	protected static final String MAIL_TRANSPORT_PROTOCOL = PropertyManager.getString("mail.transport.protocol");
	protected static final String MAIL_SMTP_PORT = PropertyManager.getString("mail.smtp.port");
	protected static final String MAIL_SMTP_USER = PropertyManager.getString("mail.smtp.user");
	protected static final String MAIL_SMTP_PASSWORD = PropertyManager.getString("mail.smtp.password");
	protected static final boolean MAIL_SMTP_AUTH = PropertyManager.getBoolean("mail.smtp.auth");
	protected static final boolean MAIL_SMTP_SSL_USE = PropertyManager.getBoolean("mail.smtp.ssl.use");
	protected static final String MAIL_SMTP_SOCKETFACTORY_PORT = PropertyManager.getString("mail.smtp.socketFactory.port");


}
