package com.fliconz.fm.security;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.fliconz.fm.common.cache.CacheManager;

public class SSOSessionListener implements HttpSessionListener{

	public static final String prefix="sso_";
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		 
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		CacheManager.remove(SSOSessionListener.prefix + se.getSession().getId());
		
	}

}
