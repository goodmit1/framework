package com.fliconz.fm.security.util;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;


public class DBParamMap extends DataMap{

	Set<String> cryptFields = null;
	public DBParamMap()
	{
		super();
		try
		{

			cryptFields = PropertyManager.getInstance().getCryptFields();
		}
		catch(Exception ignore){}
	}

	public DBParamMap(String[] fields)
	{
		super();
		try
		{

			cryptFields = new HashSet<String>();
			for(String field: fields){
				cryptFields.add(field.toUpperCase());
			}
		}
		catch(Exception ignore){}
	}
	transient ICrypt crypt;
	private ICrypt getCrypt()
	{
		try
		{
			if(crypt == null)
			{
				crypt =(ICrypt)SpringBeanUtil.getBean("crypt");
			}

		}
		catch(Exception e)
		{
			crypt = new DefaultCrypt();
		}

		return crypt;
	}
	public void setCrypt(ICrypt crypt)
	{
		this.crypt = crypt;
	}






	@Override
	public Object put(Object key, Object value) {

		if(cryptFields != null && value != null && cryptFields.contains(key))
		{
			try {
				value = getCrypt().encrypt(value.toString());
			} catch (Exception e) {

				e.printStackTrace();
			}

		}
		return super.put(key, value);
	}







	@Override
	public void putAll(Map m) {
		for(Object k : m.keySet())
		{
			put(k, m.get(k));
		}

	}

}
