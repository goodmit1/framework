package com.fliconz.fm.security.api;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fliconz.fm.security.FMAuthenticationProvider;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Controller(value="authAPIController")
public class AuthAPIController {

	public AuthAPIController() {

	}

	@RequestMapping(value={"/api/request/view.do"})
	public void requestView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String referercheckyn = PropertyManager.getString("security.api.auth.referer.check.yn");

		String referer = request.getHeader("Referer");
		String[] authReferers = PropertyManager.getStringArray("security.api.auth.referers", null);

		boolean isAuthReferer = false;

		if (referercheckyn != null && referercheckyn.equals("N"))
			isAuthReferer = true;

		if(isAuthReferer == false && referer != null && authReferers != null){
			for(String authReferer: authReferers){
				if(referer.equals(authReferer)){
					isAuthReferer = true;
					break;
				}
			}
		}

		if(isAuthReferer){
			String redirectURI = request.getParameter("redirect_uri");
			String[] authRedirectUris = PropertyManager.getStringArray("security.api.auth.redirect_uris", null);
			if(redirectURI != null && authRedirectUris != null) {
				boolean isAuthRedirectUri = false;
				for(String authRedirectUri: authRedirectUris){
					if(redirectURI.equals(authRedirectUri)){
						isAuthRedirectUri = true;
						break;
					}
				}
				if(isAuthRedirectUri){
					String id = request.getParameter("id");
					String pw = request.getParameter("pw");

					AuthenticationProvider authenticationProvider = (FMAuthenticationProvider)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationProvider", "fmAuthenticationProvider"));
					Authentication authentication = authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(id, pw));
					SecurityContext context = SecurityContextHolder.getContext();
					context.setAuthentication(authentication);

					Map paramMap = new HashMap(request.getParameterMap());
					Iterator params = paramMap.keySet().iterator();

					String paramstring = null;
					while(params.hasNext())
					{
						 String paramkey = (String)params.next();
						 if (paramkey.equals("redirect_uri") || paramkey.equals("id") || paramkey.equals("pw")) continue;

						 String paramvalue = ((String[])paramMap.get(paramkey)).length > 0 ? ((String[])paramMap.get(paramkey))[0] : null;

						 if (paramvalue != null)
							 if (paramstring == null)
								 paramstring = String.format("?%s=%s", paramkey, paramvalue);
							 else
								 paramstring += String.format("&%s=%s", paramkey, paramvalue);
					}

					//response.sendRedirect(redirectURI + (paramstring == null ? "" : paramstring));
					//RequestDispatcher dispatcher = request.getRequestDispatcher(redirectURI + (paramstring == null ? "" : paramstring));
					RequestDispatcher dispatcher = request.getRequestDispatcher(redirectURI);
					dispatcher.forward(request,response);

					return;
				}
			}
		}

		response.setStatus(404);
		response.sendRedirect("/error/error.xhtml");
	}

}
