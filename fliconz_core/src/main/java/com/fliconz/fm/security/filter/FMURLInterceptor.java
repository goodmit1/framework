package com.fliconz.fm.security.filter;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fliconz.fm.common.cache.CacheManager;
import com.fliconz.fm.common.vo.TreeNodeMap;
import com.fliconz.fm.connectlog.ConnectLogManager;
import com.fliconz.fm.log.LogManager;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.usermenu.UserMenuManager;
import com.fliconz.fw.runtime.util.PropertyManager;

public class FMURLInterceptor extends HandlerInterceptorAdapter {

	private List<String> exceptNotLoginPathList = null;
	 
	public FMURLInterceptor(){
		super();
		exceptNotLoginPathList = new LinkedList<String>();
	}

	public void setExcludeNotLoginPathList(List<String> pathList){
		exceptNotLoginPathList.clear();
		if (pathList != null){
			exceptNotLoginPathList.addAll(pathList);
		}
	}

	public void doInit(){ }
	public void doDestroy(){ }

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		this.doPostHandle_main(request, response);
		return true;
	}


	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mnv) {

	}
	protected TreeNodeMap getMenu(UserMenuManager userMenuManager, HttpServletRequest request, String requestURI)
	{
		if(userMenuManager == null) return null;
		TreeNodeMap requestMenu = null;
		if(!FMSecurityContextHelper.isMobile(request)){
			requestMenu = userMenuManager.findMenuByPrgmPath(requestURI);
		} else {
			requestMenu = userMenuManager.findMobileMenuByPrgmPath(requestURI);
		}
		return requestMenu;
	}
	protected String[] getInternalIps()
	{
		return PropertyManager.getStringArray("permission.ip.check.internal", new String[] {});
	}
	protected void doPostHandle_main(HttpServletRequest request, HttpServletResponse response){
		String requestURI = ((HttpServletRequest)request).getRequestURI();
		boolean isAjax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
		LogManager.debug(LogManager.LOGGER_SECURITY, "============= FMURLInterceptor.doPostHandle_main=["+isAjax+"]");
		UserVO userVO = UserVO.getUser();
		UserMenuManager userMenuManager = null;
		if((PropertyManager.getBoolean("mail.auth.use", false) && !requestURI.startsWith("/auth") && !requestURI.startsWith("/signup")) && !requestURI.startsWith("/bbs") && !requestURI.startsWith("/error") && !UserVO.isLogin()){
			boolean isPass = false;
			for(String excludedURL: exceptNotLoginPathList){
				LogManager.debug(LogManager.LOGGER_SECURITY, "requestURI =["+requestURI+"]");
				LogManager.debug(LogManager.LOGGER_SECURITY, "excludedURL=["+excludedURL+"]");
				LogManager.debug(LogManager.LOGGER_SECURITY, "requestURI.matches(excludedURL)=["+(requestURI.matches(excludedURL))+"]");
				if(requestURI.startsWith(excludedURL)){
					isPass = true;
					break;
				}
			}
			if(!isPass && isAjax) throw new RuntimeException("NOT_LOGIN");
		}
		else if(userVO != null && !requestURI.startsWith("/login")){
			userMenuManager = userVO.getUserMenuManager();
			LogManager.debug(LogManager.LOGGER_SECURITY, "user["+userVO.getLoginId()+"] was requested uri: " + requestURI);
			if(requestURI.equals("/")){
				try {
					String redirectPage = null;
					if(!FMSecurityContextHelper.isMobile(request)){
						redirectPage = userMenuManager.getDefaultPage();
					} else {
						redirectPage = userMenuManager.getMobileDefaultPage();
					}
					this.setMenuClicked(false, false);
					if(redirectPage == null){
						SecurityContext context = SecurityContextHolder.getContext();
						if(context.getAuthentication().getPrincipal() instanceof UserVO) {
							UserVO userVo = (UserVO)context.getAuthentication().getPrincipal();
							redirectPage = userVo.getFirstMyMenuPath();
						}
						if(redirectPage != null){
							this.setMenuClicked(false, true);
						}
					}
//					if(redirectPage != null) response.sendRedirect(redirectPage);
				} catch (Exception e){
					e.printStackTrace();
				}
			}
			UserVO.getCurrentLoginUser().getMemberData().put("requestURI", requestURI);
			TreeNodeMap requestMenu = this.getMenu(userMenuManager, request, requestURI);
			if(requestMenu != null){
				LogManager.debug(LogManager.LOGGER_SECURITY, "user["+userVO.getLoginId()+"] requestedMenu: " + requestMenu.toString());
				 
				
			}

			/**
			 * 내부접속 필수가 'Y'이면 IP 대역 체크
			 */
			if(requestMenu != null &&requestMenu.isInternalOnly()){
				boolean isInternal = false;
				if(FMSecurityContextHelper.getFirstRemoteAddr().equals("127.0.0.1") || FMSecurityContextHelper.getFirstRemoteAddr().equals("0:0:0:0:0:0:0:1") )
				{
					isInternal = true;
				}
				else
				{
					String[] internalIps = this.getInternalIps();
					String current_ip=FMSecurityContextHelper.getFirstRemoteAddr(request); 
					for(String ip: internalIps){
						if(current_ip.matches(ip)){
							isInternal = true;
							break;
						}
					}
				}
				if(!isInternal){
					try {
						response.sendRedirect(PropertyManager.getString("permission.access.denied.url", "/login/access_denied.xhtml"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			if(!isAjax && userMenuManager != null) {
				TreeNodeMap selectedMenu = userMenuManager.getSelectedMenu();
				if(requestMenu != null && requestMenu != selectedMenu) {
					/**
					 * URL을 다이렉트로 치고 들어왔을 경우, url 패턴 베이스로 찾은 프로그램을 지정
					 * 사용자의 메뉴 권한을 읽어와서 현재 프로그램과 매칭시켜보고, 매치된 메뉴를 현재 선택된 메뉴로 지정
					 * 팝업 메뉴가 아닐 때만, 메뉴 셀렉트 클래스 삽입
					 */
					if(!requestMenu.isPopup()) {
						requestMenu.put("StyleClass", "menu-selected");

						TreeNodeMap requestedMyMenu = this.getMenu(userMenuManager, request, requestURI);
						if(requestedMyMenu != null) requestedMyMenu.put("StyleClass", "menu-selected");
					}

					/**
					 * 선택된 메뉴의 스타일 초기화
					 */
					if(selectedMenu != null) {
						selectedMenu.remove("StyleClass");
						TreeNodeMap selectedMyMenu = userMenuManager.findMyMenuById(selectedMenu.getMenuId());
						if(selectedMyMenu != null) selectedMyMenu.remove("StyleClass");
					}
					if(!requestMenu.isPopup()){
						userMenuManager.setSelectedMenu(requestMenu);
					}
					this.initUpperMenus(userMenuManager, requestMenu.getParentId());
					try {
						/**
						 * 현재보고 있는 메뉴와 다른 메뉴가 요청되었을 경우에만 접속로그를 등록한다.
						 * JSF내부적으로 PhaseListener를 여러번 호출하기 때문에 이렇게 해주어야 불 필요한 접속로그가 남지 않게 된다.
						 */
						ConnectLogManager.loggingDB(this.getClass(), UserVO.getLogParamMap(requestMenu.getPrgmId(), requestMenu.getMenuNm(), request.getRemoteAddr()));
					} catch(Exception e){
						e.printStackTrace();
					}
				}
				else if(requestMenu == null )
				{
					userMenuManager.setSelectedMenu(null);
				}
				
			}
		}
	}

	protected void initUpperMenus(UserMenuManager userMenuManager, int parentMenuId){
		TreeNodeMap parentSubMenu = (TreeNodeMap)CacheManager.getMenu(parentMenuId);
		if(parentSubMenu.getParentId() > 1) {
			userMenuManager.findSelectedFirstMenu(parentSubMenu);
		} else {
			userMenuManager.setSelectedFirstMenu(parentSubMenu.getMenuId());
		}
	}

	protected void setMenuClicked(boolean isMenuClick, boolean isMyMenuClicked){}

	protected void setSelectedCurrentMenu(String requestURI, UserMenuManager userMenuManager, TreeNodeMap selectedMenu, TreeNodeMap requestMenu){
		this.initUpperMenus(userMenuManager, requestMenu.getParentId());
	}
}