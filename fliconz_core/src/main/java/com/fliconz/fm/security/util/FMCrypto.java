package com.fliconz.fm.security.util;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
//import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;

public class FMCrypto
{

    public enum CRYPTO_TYPE {
        AES, DES
    }


	private static String md5hex(String value) throws NoSuchAlgorithmException
	{
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] resultBytes = md.digest(value.getBytes());

		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < resultBytes.length ; i++)
		{
			sb.append(Integer.toString((resultBytes[i]&0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}


	/**
	 * 인증키 생성
	 * @param deviceID
	 * @param mobile
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String makeAuthKey(String deviceID, String mobile) throws NoSuchAlgorithmException
	{
		String currentTime = String.valueOf(System.currentTimeMillis());
		String authKey = String.format("%s:%s:%s", deviceID, mobile, currentTime);
		authKey = FMCrypto.md5hex(authKey);

		authKey = String.format("%s:%s:%s", authKey, deviceID, mobile);
		authKey = Base64.encodeBase64String(authKey.getBytes());

		return authKey;
	}


	private static SecretKey getDESKey(String key) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException
	{
		String encryptKey = String.format("%1$-8s", key);

		// 입력값을 bytes로 변환
		byte[] desKeyData = encryptKey.getBytes();
		// 암호화 키 생성
		DESKeySpec desKeySpec = new DESKeySpec(desKeyData);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		return keyFactory.generateSecret(desKeySpec);
	}


	/**
	 * 메시지를 개인키를 이용하여 DES 알고리즘으로 암호화한다.
	 *
	 * @param message 원본메시지
	 * @param privateKey 개인키
	 * @return 암호화된 문자열
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 */
	public static String encryptDES(String message, String privateKey) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException
	{
		SecretKey skeySpec = getDESKey(privateKey);
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

		//return String.valueOf(Hex.encodeHex(cipher.doFinal(message.getBytes())));

		byte[] arr = cipher.doFinal(message.getBytes());
		return new String(Base64.encodeBase64(arr));

		//byte[] arr = cipher.doFinal(message.getBytes());
		//return new String(Base64.encodeBase64(String.valueOf(Hex.encodeHex(arr)).getBytes()));
	}

	/**
	 * 메시지를 개인키를 이용하여 DES 알고리즘으로 복호화한다.
	 *
	 * @param message 원본메시지
	 * @param privateKey 개인키
	 * @return 복호화된 문자열
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 * @throws DecoderException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	public static String decryptDES(String message, String privateKey) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, DecoderException
	{
		SecretKey skeySpec = getDESKey(privateKey);
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);

		//return new String(cipher.doFinal(Hex.decodeHex(message.toCharArray())));

		byte[] arr = Base64.decodeBase64(message.getBytes());
		return new String(cipher.doFinal(arr));

		//byte[] arr1 = Base64.decodeBase64(message.getBytes());
		//byte[] arr2 = cipher.doFinal(Hex.decodeHex(new String(arr1).toCharArray()));
		//return new String(arr2);
	}


	/**
	 * 메시지를 개인키를 이용하여 AES 알고리즘으로 암호화한다.
	 *
	 * @param message 원본메시지
	 * @param privateKey 개인키
	 * @return 암호화된 문자열
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	public static String encryptAES(String message, String privateKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{
		String encryptKey = String.format("%1$-32s", privateKey);
		SecretKeySpec skeySpec = new SecretKeySpec(encryptKey.getBytes(), "AES");

		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

		//return String.valueOf(Hex.encodeHex(cipher.doFinal(message.getBytes())));

		byte[] arr = cipher.doFinal(message.getBytes());
		return new String(Base64.encodeBase64(arr));

		//byte[] arr = cipher.doFinal(message.getBytes());
		//return new String(Base64.encodeBase64(String.valueOf(Hex.encodeHex(arr)).getBytes()));
	}

	/**
	 * 메시지를 개인키를 이용하여 AES 알고리즘으로 복호화한다.
	 *
	 * @param message 원본메시지
	 * @param privateKey 개인키
	 * @return 복호화된 문자열
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws DecoderException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	public static String decryptAES(String message, String privateKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, DecoderException
	{
		String encryptKey = String.format("%1$-32s", privateKey);
		SecretKeySpec skeySpec = new SecretKeySpec(encryptKey.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);

		//return new String(cipher.doFinal(Hex.decodeHex(message.toCharArray())));

		byte[] arr = Base64.decodeBase64(message.getBytes());
		return new String(cipher.doFinal(arr));

		//byte[] arr1 = Base64.decodeBase64(message.getBytes());
		//byte[] arr2 = cipher.doFinal(Hex.decodeHex(new String(arr1).toCharArray()));
		//return new String(arr2);
	}







	public static String encryptAuthKey(CRYPTO_TYPE type, String source, String salt) throws Exception
	{
		//java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
		org.json.JSONObject json = new org.json.JSONObject();
		json.put("source", source);

		String encrypted = null;
        if(type == CRYPTO_TYPE.AES){
            encrypted = encryptAES(json.toString(), salt);
        } else if(type == CRYPTO_TYPE.DES){
            encrypted = encryptDES(json.toString(), salt);
        }
        return encrypted;
	}

	public static String decryptAuthKey(CRYPTO_TYPE type, String source, String salt) throws Exception
	{
		String decrypted = null;
        if(type == CRYPTO_TYPE.AES){
            decrypted = decryptAES(source, salt);
        } else if(type == CRYPTO_TYPE.DES){
            decrypted = decryptDES(source, salt);
        }
        return decrypted;
	}


	public static void main() throws Exception
	{
		System.out.printf("java.home : [%s]\n", System.getProperty("java.home"));

		String email = "aaa@aaa.com";
		 String requestPath = "/api/member/modifyPushToken.do";

		String encrypted = encryptAuthKey(CRYPTO_TYPE.AES, email, requestPath);
		System.out.printf("encrypted : [%s]\n", encrypted);

		String decrypted = decryptAuthKey(CRYPTO_TYPE.AES, encrypted, requestPath);
		System.out.printf("decrypted : [%s]\n", decrypted);
	}


}
