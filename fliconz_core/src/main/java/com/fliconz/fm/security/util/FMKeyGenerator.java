package com.fliconz.fm.security.util;

import java.util.Map;

import com.fliconz.fw.runtime.util.PropertyManager;

public abstract class FMKeyGenerator implements KeyGenerator{

	@Override
	public String generateKey(Map<String, Object> paramMap) {
		return paramMap.get(PropertyManager.getString("generator.key.name","key")).toString() + System.nanoTime();
	}

}
