package com.fliconz.fm.security.filter;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;

import com.fliconz.fm.security.PermissionService;


public class FMUrlResourceFactoryBean implements FactoryBean<LinkedHashMap<String, List<ConfigAttribute>>>{
	
	@Autowired
	private PermissionService permissionService;
	
	private LinkedHashMap<String, List<ConfigAttribute>> requestMap;
	
	public void init() throws Exception{
		try {
			requestMap = permissionService.getRoleUrlList();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public LinkedHashMap<String, List<ConfigAttribute>> getObject() throws Exception {
		if(requestMap == null){
			requestMap = permissionService.getRoleUrlList();
		}
		return requestMap;
	}
	@Override
	public Class<?> getObjectType() {
		return LinkedHashMap.class;
	}
	@Override
	public boolean isSingleton() {
		return true;
	}

}
