package com.fliconz.fm.security.password;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;

import com.fliconz.fm.common.BaseService;
import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.exception.FMPasswordCheckException;
import com.fliconz.fm.log.LogManager;
import com.fliconz.fm.security.FMAuthenticationProvider;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.security.SecurityConstant;
import com.fliconz.fm.security.UserIdAndEmailAuthenticationToken;
import com.fliconz.fm.security.UserIdAndNameAuthenticationToken;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.util.IMailSender;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;



public class PasswordService extends BaseService {
	
	IMailSender mailSender;

	public IMailSender getMailSender() {
		return mailSender;
	}

	public void setMailSender(IMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public SaltSource getSaltSource() {
		return saltSource;
	}

	public void setSaltSource(SaltSource saltSource) {
		this.saltSource = saltSource;
	}

	private Map<String, Object> authTokenMap = new HashMap<String, Object>();
	
	
	private PasswordEncoder passwordEncoder;
	
	
	private SaltSource saltSource;

	private void changePassword(UserVO userVo, String password) throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("USER_ID", userVo.getLoginId());
		String hashedPassword = null;
		boolean isEncrypted = PropertyManager.getBoolean("password.security.encryption", false);
    	if(isEncrypted){
    		hashedPassword = passwordEncoder.encodePassword(password, saltSource.getSalt(userVo));
    	} else {
    		hashedPassword = password;
    	}
		paramMap.put("PASSWORD", hashedPassword);
		super.updateByQueryKey(PasswordConstant.UPDATE_PASSWORD_QUERY_KEY, paramMap);
		userVo.setAuthFail(0);
		userVo.setPassword(hashedPassword);
		userVo.setPwdChangeTms(new Date());
	}

	public void authenticationEmailLink(String authToken, String password, String passwordConfirm) throws Exception{
		if(password.equals(passwordConfirm)){
			String authKey = SCrypto.getInstance().decoding(authToken);
			if(authKey != null && !"".equals(authKey)){
				String[] authInfos = authKey.split(SCrypto.CONCAT_TOKEN);
				String[] userInfos = authInfos[0].split(":");
				String authType = userInfos[0];
				String userId = userInfos[1];
				String userName = userInfos[2];
				if(authToken != null && !"".equals(authToken) && authTokenMap.get(userId) != null && authToken.equals(authTokenMap.get(userId))){
					String time = authInfos[1];
					int limitTime = 1000 * PropertyManager.getInt("mail.auth.limit.second", 60*60);
					long authTime = Long.parseLong(time) + limitTime;

					long current = System.currentTimeMillis();
					if(current > authTime){
						authTokenMap.remove(userId);
						throw new Exception("인증 실패: " + "유효시간이 초과하였습니다.");
					}
					try {
						AuthenticationProvider authenticationProvider = (FMAuthenticationProvider)SpringBeanUtil.getBean("fmAuthenticationProvider");
						Authentication token = null;
						if(SecurityConstant.EMAIL_AUTH_TYPE.IE.toString().equals(authType)){
							
							token = new UserIdAndEmailAuthenticationToken(userId, userName);
						} else if(SecurityConstant.EMAIL_AUTH_TYPE.IN.toString().equals(authType)){
							token = new UserIdAndNameAuthenticationToken(userId, userName);
						}
						Authentication authentication = authenticationProvider.authenticate(token);
						UserVO userVo = (UserVO)authentication.getPrincipal();
						chgPassword(userVo, password, passwordConfirm);
						this.changeUserActive(userId, "Y");
						authTokenMap.remove(userId);
					} catch(FMPasswordCheckException e){
						throw new Exception("비밀번호 체크 오류: " + e.getMessage());
					} catch(Exception e){
						throw new Exception("비밀번호 변경 실패: " + e.getMessage());
					}
				} else {
					throw new Exception("인증 실패: " + "유효하지 않는 토큰입니다.");
				}
			} else {
				throw new Exception("인증 실패: " + "유효하지 않는 토큰입니다.");
			}
		} else {
			throw new Exception("인증 실패: " + "비밀번호를 확인하세요");
		}
	}
	public void chgPassword(UserVO userVo,String password, String passwordConfirm ) throws Exception
	{
		FMSecurityContextHelper.checkPasswordSecuritySetting(userVo, password, passwordConfirm);
		this.changePassword(userVo, password);
	}
	public void authenticationEmailLink(String authToken) throws Exception{
		String authKey = SCrypto.getInstance().decoding(authToken);
		if(authKey != null && !"".equals(authKey)){
			String[] authInfos = authKey.split(SCrypto.CONCAT_TOKEN);
			String userId = authInfos[0].split(":")[0];
			if(authToken != null && !"".equals(authToken) && authTokenMap.get(userId) != null && authToken.equals(authTokenMap.get(userId))){
				String time = authInfos[1];
				int limitTime = 1000 * PropertyManager.getInt("mail.auth.limit.second", 60*60);
				long authTime = Long.parseLong(time) + limitTime;

				long current = System.currentTimeMillis();
				if(current > authTime){
					authTokenMap.remove(userId);
					throw new Exception("인증 실패: " + "유효시간이 초과하였습니다.");
				}
				this.changeUserActive(userId, "Y");
				authTokenMap.remove(userId);
			} else {
				throw new Exception("인증 실패: " + "유효하지 않는 토큰입니다.");
			}
		} else {
			throw new Exception("인증 실패: " + "유효하지 않는 토큰입니다.");
		}
	}

	private Map getPasswordAuthContents(String authType, UserVO userVo, long currentTime) throws Exception {
		Map param = new HashMap();
		param.put("USER_NAME",  userVo.getUserName());
		String authToken;
		if(authType.equals(SecurityConstant.EMAIL_AUTH_TYPE.IE.toString()))
		{
			authToken = SCrypto.getInstance().encoding(authType + ":" + userVo.getLoginId() + ":" + userVo.getEmail(), Long.toString(currentTime));
		}
		else
		{
			authToken = SCrypto.getInstance().encoding(authType + ":" + userVo.getLoginId() + ":" + userVo.getUserName(), Long.toString(currentTime));
		}
		String encoedToken = URLEncoder.encode(authToken, "UTF-8");
		authTokenMap.put(userVo.getLoginId(), authToken);

		String domain = FMSecurityContextHelper.getServerDomain();
		String link = domain + PropertyManager.getString("mail.auth.pwd_chg.link","/auth/passwordChange.xhtml") + "?at=" + encoedToken;
		param.put("TOKEN", encoedToken);
		param.put("LINK", link);


		return param;
	}
	private void changeUserActive(String userId, String use_yn) throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("USER_ID", userId);
		paramMap.put("USE_YN", use_yn);
		super.getDAO().update(PasswordConstant.UPDATE_USE_YN_QUERY_KEY, paramMap);
	}
	public void requestPwdInitEmail(Authentication authentication, String authType) throws Exception{
		UserVO userVo = (UserVO)authentication.getPrincipal();

		String to = userVo.getEmail();

		long currentTime = System.currentTimeMillis();
		Map param = this.getPasswordAuthContents(authType, userVo, currentTime);

        //보내는 시간은 암호화 하는 시간과 같게 유지
        mailSender.send( to,  MessageBundle.getBundle(this.getLang()).getString("PASSWORD_AUTH_MAIL_SUBJECT"), "initPwd", this.getLang(), param );

        this.changeUserActive(userVo.getLoginId(), "N");
		LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "비밀번호 초기화 이메일 요청 완료: userId=[" + userVo.getLoginId() + "], email=[" + to + "]" + "");
	}
	public void requestEmail(String userId, String email) throws Exception {
		
			AuthenticationProvider authenticationProvider = (FMAuthenticationProvider)SpringBeanUtil.getBean("fmAuthenticationProvider");
			Authentication token = null;
			String authType = SecurityConstant.EMAIL_AUTH_TYPE.IE.toString();
			if(email != null){
				authType = SecurityConstant.EMAIL_AUTH_TYPE.IE.toString();
				token = new UserIdAndEmailAuthenticationToken(userId, email);
			} /*else if(userName != null){
				authType = SecurityConstant.EMAIL_AUTH_TYPE.IN.toString();
				token = new UserIdAndNameAuthenticationToken(userId, userName);
			}*/
			Authentication authentication = authenticationProvider.authenticate(token);
			requestPwdInitEmail(authentication, authType);
			
		 
	}
	
	@Override
	protected String getNameSpace() {
		return "com.fliconz.fm.security.password.Password";
	}

}
