package com.fliconz.fm.security.util;
 

import java.util.HashMap;

 

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

 

public class DBResultMap extends DataMap{
	boolean isCrypt=true;
	
	
	public boolean isCryptYN() {
		return isCrypt;
	}
	public void setCryptYN(boolean isCrypt) {
		this.isCrypt = isCrypt;
	}
	public DBResultMap()
	{
		super();
		 
		try
		{
			 
			cryptFields =  new HashSet();
			cryptFields.addAll(PropertyManager.getInstance().getCryptFields());
		}
		catch(Exception ignore){}
		
	}
	Set<String> cryptFields = null;
	transient ICrypt crypt;
	private ICrypt getCrypt()
	{
		try
		{
			if(crypt == null)
			{
				crypt =(ICrypt)SpringBeanUtil.getBean("crypt");
			}
			
		}
		catch(Exception e)
		{
			crypt = new DefaultCrypt();
		}
		
		return crypt;
	}
	protected String getKey(Object key)
	{
		return key.toString();
	}
	@Override
	public Object put(Object key, Object value) {
		
		String newKey = getKey(key);
		
		
		if(isCrypt && cryptFields != null && value != null && cryptFields.contains(newKey))
		{
			try {
				
				value = getCrypt().decrypt(value.toString());
				super.put(newKey + "_mask", getMask(newKey, (String)value));
				cryptFields.remove(newKey);
			} catch (Exception e) {
				 
				//e.printStackTrace();
			}
		}
		
		return super.put(newKey, value);
	}

	//2018.10.19:추가
	@Override
	public void putAll(Map copy) {
		Iterator entrys = copy.entrySet().iterator();
	    while (entrys.hasNext())
	    {
	        Map.Entry entry = (Map.Entry)entrys.next();
	        this.put( entry.getKey(),entry.getValue() );
	    }
	}
	
	protected String getMask(String field, String value) {
		int len = value.length();
		int idx = (len - len / 3) / 2;
		return value.substring(0,idx) + "***" + value.substring(value.length()-idx);
	}
	
	public static void main(String[] args)
	{
		DBResultMap map = new DBResultMap();
		System.out.println( map.getMask("test", "syk"));
	}

}
