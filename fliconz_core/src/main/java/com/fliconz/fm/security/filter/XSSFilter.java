package com.fliconz.fm.security.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

public class XSSFilter implements Filter {
 
    
	Set<String> allowHtmlUrlList;
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String param = PropertyManager.getString("security.xss.allow_html_param");
		if(param != null && !"".equals(param))
		{
			allowHtmlUrlList = MapUtil.toSet(param.split(","));
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		 chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request, allowHtmlUrlList), response);
		
	}
	 
	@Override
	public void destroy() {
		 
		
	}

 
 
}
