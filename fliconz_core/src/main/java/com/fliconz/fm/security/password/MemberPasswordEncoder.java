package com.fliconz.fm.security.password;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.encoding.PasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;

import com.fliconz.fm.common.util.SecurityHelper;

@Singleton
public class MemberPasswordEncoder implements PasswordEncoder
{
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	private static MemberPasswordEncoder instance = new MemberPasswordEncoder();

	public static MemberPasswordEncoder getInstance(){
		return instance;
	}

	// ========================================================================================================================
	// org.springframework.security.authentication.encoding.PasswordEncoder
	@Override
	public String encodePassword(String rawPass, Object salt)
	{

		if (salt == null)
		{
			if (theLogger.isDebugEnabled()) theLogger.debug("encodePassword bbb ====> salt is null");
			return rawPass;
		}

		try
		{
			String memberId = salt.toString().trim();
			String encoded = SecurityHelper.encrypt(rawPass, memberId.toLowerCase());
			if (theLogger.isDebugEnabled()) theLogger.debug("encodePassword ccc ====> [ [{}] -> [{}]", new Object[]{  memberId, encoded});

			return encoded;
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	@Override
	public boolean isPasswordValid(String encPass, String rawPass, Object salt)
	{

		if (salt == null)
		{
			if (theLogger.isDebugEnabled()) theLogger.debug("isPasswordValid bbb ====> salt is null");
			return false;
		}

		try
		{
			String memberId = salt.toString().trim();
			String encoded = SecurityHelper.encrypt(rawPass, memberId.toLowerCase());
			if (theLogger.isDebugEnabled()) theLogger.debug("isPasswordValid ccc ====>  [{}] -> [{}]", new Object[]{  salt, encoded});

			return encoded.equals(encPass);
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			throw new IllegalStateException(e.getMessage(), e);
		}
	}





/*
	// ========================================================================================================================
	// org.springframework.security.crypto.password.PasswordEncoder

	@Override
	public String encode(CharSequence rawPassword)
	{
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword)
	{
		// TODO Auto-generated method stub
		return false;
	}
*/
}
