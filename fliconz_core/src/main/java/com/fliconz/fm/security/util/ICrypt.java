package com.fliconz.fm.security.util;

/**
 * 암호화
 * @author 윤경
 *
 */
public interface ICrypt {
	/**
	 * 패스워드 암호화(단반향)
	 * @param pwd
	 * @return
	 * @throws Exception
	 */
	public String encryptPwd(String pwd) throws Exception;
	
	/**
	 * 동일 패스워드인지 확인
	 * @param pwd1
	 * @param pwd2
	 * @return
	 * @throws Exception
	 */
	public boolean isSamePwd(String pwd1, String pwd2) throws Exception;
	
	/**
	 * 패스워드가 아닌 필드 암호화
	 * @param field
	 * @return
	 * @throws Exception
	 */
	public String encrypt(String field) throws Exception;
	
	/**
	 * 패스워드가 아닌 필드 복호화
	 * @param field
	 * @return
	 * @throws Exception
	 */
	public String decrypt(String field) throws Exception;
}
