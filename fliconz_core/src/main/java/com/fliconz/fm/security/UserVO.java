package com.fliconz.fm.security;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.fliconz.fm.common.vo.BaseVO;
import com.fliconz.fm.usermenu.UserMenuManager;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@Component("UserVO")
public class UserVO extends BaseVO implements UserDetails {

	private static final long serialVersionUID = 1L;

	protected List<SimpleGrantedAuthority> authorities;
	protected Map<String, Object> resultMap;

	protected Map<String, Object> childrenTeamListMap;

	public UserVO(){
		authorities = new ArrayList<SimpleGrantedAuthority>();
		this.resultMap = new HashMap<String, Object>();
	}

	public UserVO(Map<String, Object> resultMap){
		this.resultMap = resultMap;
		this.authorities = new ArrayList<SimpleGrantedAuthority>();
	}
	public Object getUserType() {
		Object userType = resultMap.get("USER_TYPE");
		if(userType == null){
			userType = resultMap.get("mem_tp");
			this.setUserType(userType);
		}
		return userType;
	}

	public void setUserType(Object userType) {
		this.resultMap.put("USER_TYPE", userType);
	}

	public void setUserData(Map<String, Object> userData){
		this.resultMap = userData;
	}

	public String getLoginId() {
		String loginId = (String)resultMap.get("LOGIN_ID");
		if(loginId == null){
			loginId = (String)resultMap.get("mem_id");
			this.setLoginId(loginId);
		}
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.resultMap.put("LOGIN_ID", loginId);
	}

	public Object getUserId(){
		Object userId = resultMap.get("USER_ID");
		if(userId == null){
			userId = resultMap.get("mem_no");
			this.setUserId(userId);
		}
		return userId;
	}
	public boolean isMobile()
	{
		return FMSecurityContextHelper.isMobile(FMSecurityContextHelper.getRequest());
	}

	public boolean isAdmin() {
		String[] adminRole = PropertyManager.getStringArray("admin.role");
		if(adminRole == null) return false;
		if(UserVO.getUser().isSuperAdmin()) return true;
		return hasRole(adminRole);
		 
	}
	public UserVO setUserId(Object userId) {
		resultMap.put("USER_ID", userId);
		return this;
	}

	public String getPassword() {
		String password = (String)resultMap.get("PASSWORD");
		if(password == null){
			password = (String)resultMap.get("mem_pass");
			this.setPassword(password);
		}
		return password;
	}
	public void setPassword(String password) {
		this.resultMap.put("PASSWORD", password);
	}

	public String getUserName(){
		String USER_NAME = (String)resultMap.get("USER_NAME");
		if(USER_NAME == null){
			USER_NAME = (String)resultMap.get("mem_nm");
			this.setUserName(USER_NAME);
		}
		return USER_NAME;
	}

	public void setUserName(String userName){
		this.resultMap.put("USER_NAME", userName);
	}

	public boolean hasRole(String... role)
	{
		List<SimpleGrantedAuthority> list = getAuthorities();
		for(SimpleGrantedAuthority s : list)
		{
			for(String r : role)
			{
				if(s.getAuthority().equals(r))
				{
					return true;
				}
			}
		}
		return false;
	}
	@Override
	public List<SimpleGrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<SimpleGrantedAuthority> authorities){
		this.authorities = authorities;
	}

	public List<String> getRoleNames(){
		List<String> roleNames = new ArrayList<String>();
		if(this.authorities != null){
			for(SimpleGrantedAuthority auth: this.authorities){
				roleNames.add(auth.getAuthority());
			}
		}
		return roleNames;
	}

	public String getLocale() {
		String locale = (String)resultMap.get("LOCALE");
		if(locale == null) locale = FMSecurityContextHelper.getRequest().getLocale().getLanguage();
		this.setLocale(locale);
		return locale;
	}

	public void setLocale(String locale) {
		this.resultMap.put("LOCALE", locale);
	}

	public String getUseYn(){
		String USE_YN = (String)resultMap.get("USE_YN");
		if(USE_YN == null){
			USE_YN = "1".equals(String.valueOf((Integer)resultMap.get("mem_state"))) ? "Y" : "N";
			this.setUseYn(USE_YN);
		}
		return USE_YN;
	}

	public void setUseYn(String useYn){
		this.resultMap.put("USE_YN", useYn);
	}

	public Date getPwdChangeTms() {
		return toDate(resultMap.get("PWD_CHANGE_TMS"));
	}

	private Date toDate(Object o)
	{
		if(o instanceof java.sql.Timestamp)
		{
			return new Date(((java.sql.Timestamp)o).getTime()) ;
		}
		else if(o instanceof java.util.Date)
		{
			return (java.util.Date)o;
		}
		return null;
	}
	public void setPwdChangeTms(Object pwdChangeTms) {
		this.resultMap.put("PWD_CHANGE_TMS", pwdChangeTms);
	}

	public Date getLastAccessTms() {
		Object LAST_ACCESS_TMS = resultMap.get("LAST_ACCESS_TMS");
		if(LAST_ACCESS_TMS == null){
			LAST_ACCESS_TMS = resultMap.get("last_login_dttm" );
			this.setLastAccessTms(LAST_ACCESS_TMS);
		}
		return toDate(LAST_ACCESS_TMS);
	}

	public void setLastAccessTms(Object lastAccessTms) {
		this.resultMap.put("LAST_ACCESS_TMS", lastAccessTms);
	}

	public String getLastLoginIp() {
		String LAST_LOGIN_IP = (String)resultMap.get("LAST_LOGIN_IP");
		if(LAST_LOGIN_IP == null){
			LAST_LOGIN_IP = (String)resultMap.get("last_login_ip");
			this.setLastLoginIp(LAST_LOGIN_IP);
		}
		return LAST_LOGIN_IP;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.resultMap.put("LAST_LOGIN_IP", lastLoginIp);
	}

	public int getAccLoginCnt() {
		int ACC_LOGIN_CNT = 0;
		if(resultMap.get("ACC_LOGIN_CNT") == null){
			ACC_LOGIN_CNT = (Integer)resultMap.get("acc_login_cnt");
			this.setAccLoginCnt(ACC_LOGIN_CNT);
		}
		return ACC_LOGIN_CNT;
	}

	public void setAccLoginCnt(int accLoginCnt) {
		this.resultMap.put("ACC_LOGIN_CNT", accLoginCnt);
	}

	public int getAuthFail() {
		if(this.resultMap.get("AUTH_FAIL") == null) this.setAuthFail(0);
		return Integer.parseInt(this.resultMap.get("AUTH_FAIL").toString());
	}

	public void setAuthFail(int authFail) {
		this.resultMap.put("AUTH_FAIL", authFail);
	}

	public String getOfficeNo() {
		String OFFICE_NO = (String)resultMap.get("OFFICE_NO");
		if(OFFICE_NO == null){
			OFFICE_NO = (String)resultMap.get("phone");
			this.setOfficeNo(OFFICE_NO);
		}
		return OFFICE_NO;
	}

	public void setOfficeNo(String officeNo) {
		this.resultMap.put("OFFICE_NO", officeNo);
	}

	public String getMobileNo() {
		String MOBILE_NO = (String)resultMap.get("MOBILE_NO");
		if(MOBILE_NO == null){
			MOBILE_NO = (String)resultMap.get("mobile");
			this.setMobileNo(MOBILE_NO);
		}
		return MOBILE_NO;
	}

	public void setMobileNo(String mobileNo) {
		this.resultMap.put("MOBILE_NO", mobileNo);
	}

	public String getEmail() {
		String EMAIL = (String)resultMap.get("EMAIL");
		if(EMAIL == null){
			EMAIL = (String)resultMap.get("email");
			this.setEmail(EMAIL);
		}
		return EMAIL;
	}

	public void setEmail(String email) {
		this.resultMap.put("EMAIL", email);
	}

	public TeamVO getTeam() {
		if(this.resultMap.get("TEAM") == null) this.setTeam(new TeamVO(resultMap));
		return (TeamVO)this.resultMap.get("TEAM");
	}

	public void setTeam(TeamVO team) {
		this.resultMap.put("TEAM", team);
	}

	public String getUserIP(){
		return (String)this.resultMap.get("USER_IP");
	}

	public void setUserIP(String userIP){
		this.resultMap.put("USER_IP", userIP);
	}

	public String getLangKnd(){
		return (String)this.resultMap.get("LOCALE");
	}

	public void setLangKnd(String langKnd){
		this.resultMap.put("LOCALE", langKnd);
	}

	@Override
	public String getUsername() {
		return this.getLoginId();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public Map<String, Object> getMemberData(){
		return this.resultMap;
	}

	public String toString(){
		return this.getLoginId() + "("+this.getUserName()+")";
	}

	private String firstMyMenuPath;
	public void setFirstMyMenuPath(String firstMyMenuPath) {
		this.firstMyMenuPath = firstMyMenuPath;
	}
	public String getFirstMyMenuPath() {
		return firstMyMenuPath;
	}

	private int selectedPrgmId;
	public void setSelectedPrgmId(int selectedPrgmId){
		this.selectedPrgmId = selectedPrgmId;
	}

	public int getSelectedPrgmId(){
		return this.selectedPrgmId;
	}

	@Deprecated
	public static UserVO getCurrentLoginUser(){
		SecurityContext context = SecurityContextHolder.getContext();
		if(context.getAuthentication() != null){
			if(context.getAuthentication().getPrincipal() != null){
				if (context.getAuthentication().getPrincipal() instanceof UserVO) {
					return (UserVO)context.getAuthentication().getPrincipal();
				}
			}
			if (context.getAuthentication().getDetails() != null) {
				if (context.getAuthentication().getDetails() instanceof UserVO) {
					return (UserVO)context.getAuthentication().getDetails();
				}
			}
		}
		return null;
	}

	public static UserVO getUser(){
		SecurityContext context = SecurityContextHolder.getContext();
		if(context.getAuthentication() != null){
			if(context.getAuthentication().getPrincipal() != null){
				if (context.getAuthentication().getPrincipal() instanceof UserVO) {
					return (UserVO)context.getAuthentication().getPrincipal();
				}
			}
			if (context.getAuthentication().getDetails() != null) {
				if (context.getAuthentication().getDetails() instanceof UserVO) {
					return (UserVO)context.getAuthentication().getDetails();
				}
			}
		}
		return null;
	}

	private UserMenuManager userMenuManager;
	public void setUserMenuManager(UserMenuManager userMenuManager) {
		this.userMenuManager = userMenuManager;
	}
	public UserMenuManager getUserMenuManager(){
		return userMenuManager;
	}

	/**
	 * 현재 사용자의 로그인 여부를 리턴
	 * @return
	 */
	public static boolean isLogin(){
		return UserVO.getUser() != null;
	}

	/**
	 * 현재 로그인 한 유저가 Super Admin인지 여부를 리턴
	 * @return
	 */
	public static boolean isSuperAdmin(){
		return UserVO.getUser().hasRole(SecurityConstant.ROLE_SUPER_ADMIN);
	}

	/**
	 * 접속 로그 삽입 시 기본 정보를 리턴해주는 함수
	 * @param prgmId
	 * @param prgmNm
	 * @param ip
	 * @return
	 */
	public static Map<String, Object> getLogParamMap(int prgmId, String prgmNm, String ip){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("PRGM_ID", prgmId);
		paramMap.put("PRGM_NM", prgmNm);
		paramMap.put("IPADDRESS", ip);

	   	SecurityContext context = SecurityContextHolder.getContext();
	   	if(context.getAuthentication().getPrincipal() instanceof UserVO){
		    UserVO userVo = (UserVO)context.getAuthentication().getPrincipal();
		   	paramMap.put("USER_ID", userVo.getLoginId());
		   	paramMap.put("USER_NAME", userVo.getUserName());
		   	paramMap.put("TEAM_CD", userVo.getTeam().getTeamCd());
		   	paramMap.put("TEAM_NM", userVo.getTeam().getTeamNm());
	   	} else {
	   		paramMap.put("USER_ID", SecurityConstant.GUEST);
		   	paramMap.put("USER_NAME", SecurityConstant.GUEST);
	   	}

	   	String firstRemoteAddr = FMSecurityContextHelper.getFirstRemoteAddr();
		if(firstRemoteAddr != null){
			paramMap.put("IPADDRESS", firstRemoteAddr);
		}
		return paramMap;
	}

	public void addRole(String roleName){
		List<SimpleGrantedAuthority> authorities = this.getAuthorities();
		authorities.add(new SimpleGrantedAuthority(roleName));
		Authentication authentication = new UsernamePasswordAuthenticationToken(this, this.getPassword(), authorities);
		SecurityContext context = SecurityContextHolder.getContext();
		context.setAuthentication(authentication);
	}

	public void removeRole(String roleName){
		List<SimpleGrantedAuthority> authorities = this.getAuthorities();
		for(SimpleGrantedAuthority auth: authorities){
			if(auth.getAuthority().equals(roleName)){
				authorities.remove(auth);
				break;
			}
		}
		Authentication authentication = new UsernamePasswordAuthenticationToken(this, this.getPassword(), authorities);
		SecurityContext context = SecurityContextHolder.getContext();
		context.setAuthentication(authentication);
	}

	public Map<String, Object> getChildrenTeamListMap() {
		return childrenTeamListMap;
	}

	public void setChildrenTeamListMap(Map<String, Object> childrenTeamListMap) {
		this.childrenTeamListMap = childrenTeamListMap;
	}

	public List<Map<String, Object>> getChildrenTeamList() {
		Map<String, Object> map = getChildrenTeamListMap();
		List<Map<String, Object>> childrenTeamList = new ArrayList<Map<String, Object>>();
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String key = it.next();
			String value = map.get(key).toString();
			Map<String, Object> child = new HashMap<String, Object>();
			child.put(value, key);
			childrenTeamList.add(child);
		}
		return childrenTeamList;
	}

	public String getAppToken(){
		return (String)this.resultMap.get("app_token");
	}

	public void setAppToken(String app_token){
		this.resultMap.put("app_token", app_token);
	}

	public Timestamp getAppTokenDttm(){
		return (Timestamp)this.resultMap.get("app_token_dttm");
	}

	public void setAppTokenDttm(String app_token_dttm){
		this.resultMap.put("app_token_dttm", app_token_dttm);
	}

	public String getUserImage(){
		return (String)this.resultMap.get("mem_img");
	}

	public void setUserImage(String mem_img){
		this.resultMap.put("mem_img", mem_img);
	}

	public String getDeviceId(){
		return (String)this.resultMap.get("device_id");
	}
	public void setDeviceId(String deviceId){
		this.resultMap.put("device_id", deviceId);
	}

}
