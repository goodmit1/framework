package com.fliconz.fm.security.util;

import java.util.Map;

public interface KeyGenerator {

	public String generateKey(Map<String, Object> paramMap);

	public void beforeInsert(Map<String, Object> paramMap);

	public void afterInsert(Map<String, Object> paramMap);

	public void beforeUpdate(Map<String, Object> paramMap);

	public void afterUpdate(Map<String, Object> paramMap);

	public void beforeDelete(Map<String, Object> paramMap);

	public void afterDelete(Map<String, Object> paramMap);
}
