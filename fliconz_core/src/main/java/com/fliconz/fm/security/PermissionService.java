package com.fliconz.fm.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.BaseService;
import com.fliconz.fm.log.LogManager;
import com.fliconz.fw.runtime.dao.BaseDAO;
import com.fliconz.fw.runtime.util.PropertyManager;

@Service(value="permissionService")
public class PermissionService extends BaseService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	protected String getNameSpace() {
		return "com.fliconz.fm.security.Permission";
	}

	@Autowired
	@Qualifier("commonDAO")
	private BaseDAO dao;

	@PostConstruct
	public void init(){
		super.setDAO(this.dao);
	}

	public LinkedHashMap<Object, List<ConfigAttribute>> getRoleResources(String resourceType) throws Exception{
		LinkedHashMap<Object, List<ConfigAttribute>> resourcesMap = new LinkedHashMap<Object, List<ConfigAttribute>>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("_SYSTEM_CODE_", PropertyManager.getInt("system.code",0));
		List<Map<String, Object>> resultList = null;
		if(SecurityConstant.RESOURCE_TYPE_METHOD.equals(resourceType)){
			resultList = this.getDAO().selectList(SecurityConstant.METHOD_QUERY_KEY, paramMap);
		} else if(SecurityConstant.RESOURCE_TYPE_POINTCUT.equals(resourceType)){
			resultList = this.getDAO().selectList(SecurityConstant.POINTCUT_QUERY_KEY, paramMap);
		} else {
			resultList = this.getDAO().selectList(SecurityConstant.URL_QUERY_KEY, paramMap);
			Map<String, Object> authUrlMap = new HashMap<String, Object>();
			authUrlMap.put("URL", "/auth/**");
			authUrlMap.put("ORDERS", 2);
			if(PropertyManager.getBoolean("mail.auth.use", false)){
				authUrlMap.put("AUTHORITY", "ROLE_ANONYMOUS");
			}else {
				authUrlMap.put("AUTHORITY", "ROLE_SUPER_ADMIN");
			}
			resultList.add(authUrlMap);
		}
		if(resultList != null) {
			Iterator<Map<String, Object>> it = resultList.iterator();
			Map<String, Object> tempMap;
			String presentResource;
			while(it.hasNext()){
				tempMap = it.next();
				presentResource = (String)tempMap.get(resourceType);

				List<ConfigAttribute> configList = resourcesMap.get(presentResource);
				if(configList == null) configList = new ArrayList<ConfigAttribute>();

				//모든 URL 리소스에 ROLE_SUPER_ADMIN 권한을 추가해준다.
				boolean isExists = false;
				for(ConfigAttribute config: configList){
					if(config.getAttribute().equals((String)tempMap.get(SecurityConstant.AUTHORITY))){
						isExists = true;
						break;
					}
				}
				if(!isExists) configList.add(new SecurityConfig((String)tempMap.get(SecurityConstant.AUTHORITY)));

				//모든 URL 리소스에 ROLE_SUPER_ADMIN 권한을 추가해준다.
				isExists = false;
				for(ConfigAttribute config: configList){
					if(config.getAttribute().equals(SecurityConstant.ROLE_SUPER_ADMIN)){
						isExists = true;
						break;
					}
				}
				if(!isExists) configList.add(new SecurityConfig(SecurityConstant.ROLE_SUPER_ADMIN));
				resourcesMap.put(presentResource, configList);
			}
		}
		LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "resourceType=" + resourceType + ", resourcesMap=" + resourcesMap);
		return resourcesMap;

	}

	/**
	 * 롤에 대한 URL의 매핑 정보를 얻어온다.
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, List<ConfigAttribute>> getRoleUrlList() throws Exception{
		LinkedHashMap<String, List<ConfigAttribute>> ret = new LinkedHashMap<String, List<ConfigAttribute>>();
		LinkedHashMap<Object, List<ConfigAttribute>> data = this.getRoleResources(SecurityConstant.RESOURCE_TYPE_URL);
		Set<Object> keys = data.keySet();
		for(Object key: keys){
			ret.put((String)key, data.get(key));
		}
		return ret;
	}

	/**
	 * 롤에 대한 메소드의 매핑 정보를 얻어온다.
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, List<ConfigAttribute>> getRoleMethodList() throws Exception{
		LinkedHashMap<String, List<ConfigAttribute>> ret = new LinkedHashMap<String, List<ConfigAttribute>>();
		LinkedHashMap<Object, List<ConfigAttribute>> data = this.getRoleResources(SecurityConstant.RESOURCE_TYPE_METHOD);
		Set<Object> keys = data.keySet();
		for(Object key: keys){
			ret.put((String)key, data.get(key));
		}
		return ret;
	}

	/**
	 * 롤에 대한 AOP Pointcut 매핑 정보를 얻어온다.
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, List<ConfigAttribute>> getRolePointcutList() throws Exception{
		LinkedHashMap<String, List<ConfigAttribute>> ret = new LinkedHashMap<String, List<ConfigAttribute>>();
		LinkedHashMap<Object, List<ConfigAttribute>> data = this.getRoleResources(SecurityConstant.RESOURCE_TYPE_POINTCUT);
		Set<Object> keys = data.keySet();
		for(Object key: keys){
			ret.put((String)key, data.get(key));
		}
		return ret;
	}

//	/**
//	 * Best 매칭 정보를 얻어온다.
//	 * @param url
//	 * @return
//	 * @throws Exception
//	 */
//	public List<ConfigAttribute> getMatchRequestMapping(String url) throws Exception{
//		Map<String, String> paramMap = new HashMap<String, String>();
//		paramMap.put("url", url);
//
//		List<Map<String, Object>> resultList = super.selectListByQueryKey(SecurityConstant.BEST_MATCHED_QUERY_KEY, paramMap);
//
//		Iterator<Map<String, Object>> it = resultList.iterator();
//		Map<String, Object> tempMap;
//		List<ConfigAttribute> configList = new ArrayList<ConfigAttribute>();
//		while(it.hasNext()){
//			tempMap = it.next();
//			configList.add(new SecurityConfig((String)tempMap.get("authority")));
//		}
//
//		if(configList.size() > 0){
//
//		}
//		return configList;
//	}
//
//	/**
//	 * 롤의 계층적 구조를 얻어온다.
//	 * @return
//	 * @throws Exception
//	 */
//	public String getHierarchicalRoles() throws Exception{
//		List<Map<String, Object>> resultList = super.selectListByQueryKey(SecurityConstant.HIERARCHCICAL_ROLES_QUERY_KEY, null);
//		Iterator<Map<String, Object>> it = resultList.iterator();
//		Map<String, Object> tempMap;
//		StringBuffer concatedRoles = new StringBuffer();
//		while(it.hasNext()){
//			tempMap = it.next();
//			concatedRoles.append(tempMap.get("child"));
//			concatedRoles.append(" > ");
//			concatedRoles.append(tempMap.get("parent"));
//			concatedRoles.append("\n");
//		}
//		return concatedRoles.toString();
//	}


}
