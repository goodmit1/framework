package com.fliconz.fm.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.BaseService;
import com.fliconz.fm.security.util.DBParamMap;
import com.fliconz.fm.security.util.FMPasswordManager;
import com.fliconz.fw.runtime.util.NumberUtil;

@Service(value="userDetailService")
public class UserDetailService extends BaseService implements UserDetailsService, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public String getNameSpace() {
		return "com.fliconz.fm.security.userDetail";
	}

	@Override
    public UserVO loadUserByUsername(String username) throws UsernameNotFoundException {
		Map<String, Object> paramMap = new DBParamMap();
		paramMap.put(SecurityConstant.LOGIN_ID, username);
		try {
			Map<String, Object> resultMap = this.getDAO().selectOne(SecurityConstant.LOGIN_QUERY_KEY, paramMap);
			//System.out.println(resultMap);
			if(resultMap != null){
		        return new UserVO(resultMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		throw new UsernameNotFoundException("등록되지 않은 사용자입니다.");
    }

	public int updateMyInfo(Map param) throws Exception
	{
		if(param.get("OLD_PASSWORD") != null && !"".equals(param.get("OLD_PASSWORD")))
		{
			String oldPwd = FMPasswordManager.getPassword(UserVO.getUser(), (String)param.get("OLD_PASSWORD"));
			if(!UserVO.getUser().getPassword().equals(oldPwd))
			{
				throw new Exception("Invalid Current Password");
			}



		}
		if(param.get("PASSWORD") != null && !"".equals(param.get("PASSWORD")))
		{
			FMSecurityContextHelper.checkPasswordSecuritySetting(UserVO.getUser(), (String)param.get("PASSWORD"), (String)param.get("PASSWORD"));
			param.put("PASSWORD",FMPasswordManager.getPassword(UserVO.getUser(), (String)param.get("PASSWORD")));
		}
		Map param1 = new DBParamMap();
		param1.putAll(param);
		addCommonParam(param1);
		int row =  super.getDAO().update(SecurityConstant.UPDATE_MYINFO_QUERY_KEY, param1);
		if(row==1)
		{
			if(param.get("EMAIL") != null) UserVO.getUser().setEmail((String)param.get("EMAIL"));
			if(param.get("USER_NAME") != null) UserVO.getUser().setUserName((String)param.get("USER_NAME"));
			if(param.get("MOBILE_NO") != null) UserVO.getUser().setMobileNo((String)param.get("MOBILE_NO"));
			if(param.get("OFFICE_NO") != null) UserVO.getUser().setOfficeNo((String)param.get("OFFICE_NO"));
			if(param.get("LOCALE") != null) UserVO.getUser().setLocale((String)param.get("LOCALE"));
			if(param.get("TEAM_CD") != null) UserVO.getUser().getTeam().setTeamCd(NumberUtil.getInt(param.get("TEAM_CD")));
			if(param.get("TEAM_NM") != null) UserVO.getUser().getTeam().setTeamNm((String)(param.get("TEAM_NM")));
			if(param.get("PASSWORD") != null) UserVO.getUser().setPassword((String)param.get("PASSWORD"));
		}
		return row;
	}
	public Map<String, Object> getFirstMyMenu(String username) throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(SecurityConstant.LOGIN_ID, username);
		Map<String, Object> firstMyMenuMap = super.selectOneByQueryKey(SecurityConstant.FIRST_MY_MENU_QUERY_KEY, paramMap);
		return firstMyMenuMap;
	}

	public void updateLastAccessInfo(String userId) throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(SecurityConstant.LOGIN_ID, userId);
		super.updateByQueryKey(SecurityConstant.UPDATE_LOGIN_INFO_QUERY_KEY, paramMap);
	}

	public List getUserRole(String queryKey, Map param) throws Exception{
		return getDAO().selectList(queryKey, param);
	}

	public Map getTeamList(Object userId, String loginId) throws Exception {
		Map<String, Object> paramMap =  new HashMap<String, Object>();
		paramMap.put("_USER_ID_", userId);
		paramMap.put("_LOGIN_ID_", loginId);
		Map rootTeam = this.selectOneByQueryKey("selectRootTeam", paramMap);
		Map teamMap = new LinkedHashMap<String, Object>();
		if(rootTeam != null){
			rootTeam.put("TEAM_NM", rootTeam.get("TEAM_NM"));
			List<Map> teamList = new ArrayList();
			selectChildrenList(rootTeam, teamList, paramMap, "- ");
			for(Map team: teamList){
				teamMap.put(team.get("TEAM_NM").toString(), team.get("TEAM_CD").toString());
			}
		}
		return teamMap;
	}

	private void selectChildrenList(Map parentTeam, List parentList, Map searchParam, String prefix) throws Exception{
		List returnList = new ArrayList();
		parentList.add(parentTeam);
		searchParam.put("PARENT_CD", parentTeam.get("TEAM_CD"));

		List<Map> childrenList = null;
		try {
			childrenList = this.selectListByQueryKey("selectChildrenList", searchParam);
		} catch (Exception ex) {
			throw new Exception("로그인 사용자의 하위부서목록 조회 중 오류가 발생했습니다.");
		}

		if(childrenList != null && childrenList.size() > 0){
			for(Map child: childrenList){
				child.put("TEAM_NM", prefix + child.get("TEAM_NM"));
				selectChildrenList(child, returnList, searchParam, "-" + prefix);
			}
		} else {
			//2019.06.05:하위목록이 없는 경우 재귀호출 종료
			return;
		}

		parentList.addAll(returnList);
	}


	public boolean linkSNS(String snsId, String type, String email) throws Exception {
		Map<String, Object> param = new DBParamMap();
		param.put(SecurityConstant.SNS_ID, snsId);
		param.put(SecurityConstant.SNS_TYPE, type);
		param.put(SecurityConstant.EMAIL, email);
		int result = this.insertByQueryKey("linkSNS", param);
		return result == 1;
	}

	public boolean unlinkSNS(String snsId, String type, String userId) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(SecurityConstant.SNS_ID, snsId);
		param.put(SecurityConstant.SNS_TYPE, type);
		param.put(SecurityConstant.USER_ID, userId);
		int result = this.insertByQueryKey("unlinkSNS", param);
		return result == 1;
	}

	public Map<String, Object> getSNSLinkInfo(String snsId, String type, String email) throws Exception {
		Map<String, Object> param = new DBParamMap();
		param.put(SecurityConstant.SNS_ID, snsId);
		param.put(SecurityConstant.SNS_TYPE, type);
		param.put(SecurityConstant.EMAIL, email);
		Map result = this.selectOneByQueryKey("checkLink", param);
		return result;
	}

	public int updateLoginFail(Object loginId)   {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(SecurityConstant.LOGIN_ID, loginId);
		try {
			return this.getDAO().update(SecurityConstant.LOGIN_FAIL_QUERY_KEY, param);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}
}
