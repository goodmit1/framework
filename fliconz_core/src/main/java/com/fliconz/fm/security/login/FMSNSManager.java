package com.fliconz.fm.security.login;

import java.util.Map;

import com.fliconz.fm.security.SecurityConstant;
import com.fliconz.fm.security.UserDetailService;
import com.fliconz.fm.security.util.DBParamMap;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class FMSNSManager {

	public static Map<String, Object> getSNSLinkInfo(String snsId, String type, String email) throws Exception {
		UserDetailService service = (UserDetailService)SpringBeanUtil.getBean("userDetailService");

		Map<String, Object> param = new DBParamMap();
		param.put(SecurityConstant.SNS_ID, snsId);
		param.put(SecurityConstant.SNS_TYPE, type);
		param.put(SecurityConstant.EMAIL, email);
		Map result = service.selectOneByQueryKey("checkLink", param);
		return result;
	}

}

