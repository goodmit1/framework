package com.fliconz.fm.security.login;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.fliconz.fm.common.snslogin.SNSUser;
import com.fliconz.fm.exception.SNSNotLinkedException;
import com.fliconz.fm.security.SNSAuthenticationToken;
import com.fliconz.fm.security.UserDetailService;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class FMLoginManager {

	public static void login(HttpServletRequest request, HttpServletResponse response, String loginId, String password) throws Exception{
		login(request, response, loginId, password, false);
	}

	public static void login(HttpServletRequest request, HttpServletResponse response, String loginId, String password, boolean isRedirect) throws Exception {
		request.setAttribute("isRedirect", false);

		SecurityContext context = SecurityContextHolder.getContext();
		AuthenticationProvider authenticationProvider = (AuthenticationProvider)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationProvider", "fmAuthenticationProvider"));
		Authentication authentication = authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(loginId, password));
		context.setAuthentication(authentication);

		AuthenticationSuccessHandler authenticationSuccessHandler = (AuthenticationSuccessHandler)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationSuccessHandler", "fmAuthenticationSuccessHandler"));
		authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
		request.removeAttribute("isRedirect");
	}

	public static void snsLogin(HttpServletRequest request, HttpServletResponse response, SNSUser snsUser, String type) throws Exception{
		Map<String, Object> linkMap = FMSNSManager.getSNSLinkInfo(snsUser.getId(), type, snsUser.getEmail());
		if(linkMap == null) throw new SNSNotLinkedException(type);
		request.setAttribute("isRedirect", false);
		doLogin(request, response, new SNSAuthenticationToken((String)linkMap.get("LOGIN_ID"), snsUser.getEmail()));
		request.removeAttribute("isRedirect");
	}

	private static void doLogin(HttpServletRequest request, HttpServletResponse response, AbstractAuthenticationToken token) throws Exception{
		SecurityContext context = SecurityContextHolder.getContext();
		AuthenticationProvider authenticationProvider = (AuthenticationProvider)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationProvider", "fmAuthenticationProvider"));
		Authentication authentication = authenticationProvider.authenticate(token);
		context.setAuthentication(authentication);

		AuthenticationSuccessHandler authenticationSuccessHandler = (AuthenticationSuccessHandler)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationSuccessHandler", "fmAuthenticationSuccessHandler"));
		authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
	}

	public static UserDetailService getUserDetailService() {
		UserDetailService service = (UserDetailService)SpringBeanUtil.getBean("userDetailService");
		return service;
	}

}

