package com.fliconz.fm.security.filter;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.method.MapBasedMethodSecurityMetadataSource;
import org.springframework.security.access.method.MethodSecurityMetadataSource;

import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.security.PermissionService;


public class FMMethodSecurityMetadataSource  extends MapBasedMethodSecurityMetadataSource implements MethodSecurityMetadataSource{

	private final Map<String, Collection<ConfigAttribute>> methodMap;

	@Autowired
	private PermissionService permissionService;

	public FMMethodSecurityMetadataSource(LinkedHashMap<String, Collection<ConfigAttribute>> methodMap) {
		this.methodMap = methodMap;
	}

	@Override
	public Collection<ConfigAttribute> getAttributes(Method method, Class<?> targetClass) {
		if(targetClass.getPackage() == null) return null;
		String expression = FMSecurityContextHelper.getExpression(targetClass.getPackage().getName(), method.getName());
		return methodMap.get(expression);
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		Set<ConfigAttribute> allAttributes = new HashSet<ConfigAttribute>();
		for(Map.Entry<String, Collection<ConfigAttribute>> entry: methodMap.entrySet()){
			allAttributes.addAll(entry.getValue());
		}
		return allAttributes;
	}

	public void reload() throws Exception {
		synchronized (methodMap) {
			LinkedHashMap<String, List<ConfigAttribute>> reloadedMap = permissionService.getRoleMethodList();
			Iterator<Entry<String, List<ConfigAttribute>>> it = reloadedMap.entrySet().iterator();
			while(it.hasNext()){
				Entry<String, List<ConfigAttribute>> entry = it.next();
				reloadedMap.put(entry.getKey(), entry.getValue());
			}
			methodMap.clear();
			methodMap.putAll(reloadedMap);
		}
	}

}
