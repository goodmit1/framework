package com.fliconz.fm.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.fliconz.fm.common.cache.CacheManager;
import com.fliconz.fm.security.token.UserAuthenticationToken;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class SSOHelper {
	public static UserVO ssoLogin4API(String id, String name, HttpServletRequest request, HttpServletResponse response) throws Exception{
		 request.setAttribute("isRedirect", false);
		 ssoLogin(id, name, request, response);
		 UserVO user = UserVO.getUser();
		 if(!name.equals(user.getUserName())) {
			 throw new Exception("invalid token");
		 }
		 CacheManager.put(SSOSessionListener.prefix +  request.getSession().getId(), user);
		 return user;
	}
	public static  UserVO ssoLogin4APIById(String sessionId) {
		 UserVO user = (UserVO)CacheManager.get(SSOSessionListener.prefix +  sessionId);
		 if(user != null) {
			 SecurityContext context = SecurityContextHolder.getContext();
			 UserAuthenticationToken auth = new UserAuthenticationToken(user, "sessionId", user.getAuthorities());
			 context.setAuthentication(auth);
		 }
		 return user;
	}
	public  static  void ssoLogin(String id, String name, HttpServletRequest request, HttpServletResponse response) throws Exception{
		AbstractAuthenticationToken token = new SNSAuthenticationToken(id, name);
		SecurityContext context = SecurityContextHolder.getContext();
		AuthenticationProvider authenticationProvider = (AuthenticationProvider)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationProvider", "fmAuthenticationProvider"));
		Authentication authentication = authenticationProvider.authenticate(token);
		context.setAuthentication(authentication);

		AuthenticationSuccessHandler authenticationSuccessHandler = (AuthenticationSuccessHandler)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationSuccessHandler", "fmAuthenticationSuccessHandler"));
		authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
		request.getSession().setAttribute("from_sso","Y");
	}
}
