package com.fliconz.fm.security.password;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class SCrypto {

	private static SCrypto instance;

	public static final String CONCAT_TOKEN = "_-_";

	private boolean isUseRandomKey = false;
	private SecretKey sKey;
	private byte[] raw;
	private SecretKeySpec skeySpec;

	private KeyGenerator keygen;

	private Cipher cipher;

	private String algorithm;
	private int algorithmLength;
	private String[] algorithmSet = { "AES:256","DES:56" };

	private byte[] key = new byte[]{
			(byte)0x00, (byte)0x01, (byte)0x02, (byte)0x03, (byte)0x04, (byte)0x05, (byte)0x06, (byte)0x07,
			(byte)0x08, (byte)0x09, (byte)0x0a, (byte)0x0b, (byte)0x0c, (byte)0x0d, (byte)0x0e, (byte)0x0f
	};

	private SCrypto(String algorithm) {
		this.isUseRandomKey = false;
		this.algorithm = algorithm;
		this.init();
	}

	private SCrypto(String algorithm, boolean isUseRandomKey) {
		this.isUseRandomKey = isUseRandomKey;
		this.algorithm = algorithm;
		this.init();
	}

	private SCrypto(boolean isUseRandomKey) {
		this("AES", isUseRandomKey);
	}

	private SCrypto() {
		this("AES");
	}

	private final void init(){
		if(algorithm == null || "".equals(algorithm)){
			this.algorithm = "AES";
		}
		for(int i = 0 ; i < algorithmSet.length ; i++){
			String[] tmpAlSet = algorithmSet[i].split(":");
			String tmpAl = tmpAlSet[0];
			int tmpLength = Integer.parseInt(tmpAlSet[1]);
			if(tmpAl.equals(algorithm)){
				this.algorithm = tmpAl;
				this.algorithmLength = tmpLength;
			}
		}
		if(!isUseRandomKey){
			this.raw = this.key;
		}else{
			this.sKey = keygen.generateKey();
			this.raw = sKey.getEncoded();
		}

		try {
			keygen = KeyGenerator.getInstance(this.algorithm);
			keygen.init(this.algorithmLength);
			skeySpec = new SecretKeySpec(raw, this.algorithm);
			cipher = Cipher.getInstance(this.algorithm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static final SCrypto newInstance(String algorithm){
		instance = new SCrypto(algorithm);
		return instance;
	}

	public static final SCrypto getInstance(){
		return getInstance("AES");
	}

	public static final SCrypto getInstance(String algorithm){
		if(instance == null){
			instance = newInstance(algorithm);
		}
		return instance;
	}

	protected void setRaw(String enBaseKey) throws Exception{
		byte[] deBaseKey = Base64.decodeBase64(enBaseKey);
		this.raw = deBaseKey;
	}

	protected byte[] getRaw(){
		return this.raw;
	}

	protected byte[] encode(String target, String time) throws Exception{
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		return cipher.doFinal((target + CONCAT_TOKEN + time).getBytes());
	}

	protected byte[] decode(byte[] deBase) throws Exception{
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		return cipher.doFinal(deBase);
	}

	public final String encoding(String target, String time) throws Exception{
		byte[] encrypted = SCrypto.getInstance().encode(target, time);
		String enBase = new String(Base64.encodeBase64(encrypted)).replace("\n", "SCryptoN").replace("\r", "SCryptoR").replace("\t", "SCryptoT").replace("/", "SCryptoS").replace("\\", "SCryptoB");
		return enBase;
	}

	public final String encoding(String target) throws Exception{
		String time = Long.toString(System.currentTimeMillis());
		byte[] encrypted = SCrypto.getInstance().encode(target, time);
		String enBase = new String(Base64.encodeBase64(encrypted)).replace("\n", "SCryptoN").replace("\r", "SCryptoR").replace("\t", "SCryptoT").replace("/", "SCryptoS").replace("\\", "SCryptoB");
		return enBase;
	}

	public final String decoding(String enBase) throws Exception {
		byte[] deBase = Base64.decodeBase64(enBase.replace("SCryptoN", "\n").replace("SCryptoR", "\r").replace("SCryptoT", "\t").replace("SCryptoS", "/").replace("SCryptoB", "\\").getBytes());
		byte[] decrypted = SCrypto.getInstance().decode(deBase);
		return new String(decrypted);
	}

	public final String decoding(String enBase, String salt) throws Exception {
		byte[] deBase = Base64.decodeBase64(enBase.replace("SCryptoN", "\n").replace("SCryptoR", "\r").replace("SCryptoT", "\t").getBytes());
		byte[] decrypted = SCrypto.getInstance().decode(deBase);
		return new String(decrypted);
	}

	public static void main(String[] args) throws Exception {

	}
}
