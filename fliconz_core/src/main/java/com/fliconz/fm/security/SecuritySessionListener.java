package com.fliconz.fm.security;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class SecuritySessionListener   implements HttpSessionListener{

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		 
		 
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		 
		FMAuthenticationSuccessHandler fmAuthenticationSuccessHandler = (FMAuthenticationSuccessHandler)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationSuccessHandler", "fmAuthenticationSuccessHandler"));
		fmAuthenticationSuccessHandler.removeLoginInfo4Single(se.getSession());
	}
	
}