package com.fliconz.fm.security.util;

import javax.inject.Singleton;

import org.springframework.stereotype.Component;

import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.security.password.MemberPasswordEncoder;
import com.fliconz.fm.security.password.MemberSaltSource;
import com.fliconz.fw.runtime.util.PropertyManager;

@Component(value="fmPasswordManager")
@Singleton
public class FMPasswordManager {

	private static FMPasswordManager instance = new FMPasswordManager();

	public static FMPasswordManager getInstance(){
		return instance;
	}

	public static String getPassword(UserVO userVo, String password){
		String hashedPassword = null;
		boolean isEncrypted = PropertyManager.getBoolean("password.security.encryption", false);
    	if(isEncrypted){
    		hashedPassword = MemberPasswordEncoder.getInstance().encodePassword(password, MemberSaltSource.getInstance().getSalt(userVo));
    	} else {
    		hashedPassword = password;
    	}
		return hashedPassword;
	}

	public static String getPassword(String userId, String password){
		UserVO userVo = new UserVO();
		userVo.setLoginId(userId);
		return getPassword(userVo, password);
	}
}
