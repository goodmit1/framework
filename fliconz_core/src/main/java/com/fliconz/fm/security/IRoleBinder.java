package com.fliconz.fm.security;

import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

public interface IRoleBinder {

	public void bindRole(UserVO userVO, List<SimpleGrantedAuthority> roles, UserDetailService userDetailService) throws Exception;
}
