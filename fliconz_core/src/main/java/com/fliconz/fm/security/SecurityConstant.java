package com.fliconz.fm.security;

import com.fliconz.fw.runtime.util.PropertyManager;

public class SecurityConstant {

	public static final String LOGIN_FAIL_QUERY_KEY = PropertyManager.getString("login.fail.customQueryKey", "com.fliconz.fm.security.userDetail.updateAuthFail");

	public static final String LOGIN_QUERY_KEY = PropertyManager.getString("login.customQueryKey", "com.fliconz.fm.security.userDetail.login");
	public static final String UPDATE_MYINFO_QUERY_KEY = PropertyManager.getString("update.myInfo.customQueryKey", "com.fliconz.fm.security.userDetail.update_FM_USER");
	public static final String SNS_LOGIN_QUERY_KEY = PropertyManager.getString("login.sns.customQueryKey", "com.fliconz.fm.security.userDetail.login_sns");
	public static final String TOKEN_LOGIN_QUERY_KEY = PropertyManager.getString("login.token.customQueryKey", "com.fliconz.fm.security.userDetail.tokenLogin");
	public static final String USER_ROLE_QUERY_KEY = PropertyManager.getString("login.role.customQueryKey", "com.fliconz.fm.security.userDetail.getUserRole");
	public static final String URL_QUERY_KEY = PropertyManager.getString("permission.role.url.customQueryKey", "com.fliconz.fm.security.Permission.urlList");
	public static final String METHOD_QUERY_KEY = PropertyManager.getString("permission.role.method.customQueryKey", "com.fliconz.fm.security.Permission.methodList");
	public static final String POINTCUT_QUERY_KEY = PropertyManager.getString("permission.role.pointcut.customQueryKey", "com.fliconz.fm.security.Permission.pointcutList");

	public static final String FIRST_MY_MENU_QUERY_KEY = "firstMyMenuPath";

	public static final String BEST_MATCHED_QUERY_KEY = "bestMatchedList";
	public static final String HIERARCHCICAL_ROLES_QUERY_KEY = "hierarchcicalRoleList";

	public static final String UPDATE_LOGIN_INFO_QUERY_KEY = "updateLoginInfo";

	public static final String GUEST = PropertyManager.getString("permission.anonymousUser.id", "guest");

	public static final String LOGIN_ID = "LOGIN_ID";
	public static final String _USER_ID_ = "_USER_ID_";
	public static final String PASSOWRD = "PASSWORD";
	public static final String SNS_ID = "SNS_ID";
	public static final String SNS_TYPE = "SNS_TYPE";
	public static final String USER_ID = "USER_ID";

	public static final String EMAIL = "EMAIL";
	public static final String EMAIL_ADDR = "EMAIL_ADDR";
	public static final String USER_NO = "USER_NO";
	public static final String USER_IP = "USER_IP";

	public static final String COMP_NO = "COMP_NO";
	public static final String DEPT_NO = "DEPT_NO";

	public static final String ROLE_NM = "ROLE_NM";
	public static final String ROLE_NM_PREFIX = "";

	public static final String PROPERY_SUPER_ADMIN_KEY = "permission.role.super_admin";
	public static final String PROPERY_SUPER_ADMIN_KEY1 = "permission.role.spuer_admin";
	// 사용자 권한
	// - Annonymous > Member > Dept Member > Dept Master
	// - Annonymous > Member > Company Member > Company Master
	// - Annonymous > Member > Merchant Member > Merchant Master
	// - Annonymous > Member > Supervisor
	// - Annonymous > Member > Rider
	public final static String ROLE_ANONYMOUS 	= "ROLE_ANONYMOUS";			// 모든 사용자
	public final static String ROLE_GUEST 		= "ROLE_GUEST";				// 비회원 주문확인 사용자
	public final static String ROLE_MEMBER 		= "ROLE_MEMBER";			// 회원
	public final static String ROLE_RIDER 		= "ROLE_RIDER";				// 기사
	public final static String ROLE_GUEST_RIDER = "ROLE_GUEST_RIDER";		// 로그인하지 않은 기사
	public final static String ROLE_DEPT_MEMBER = "ROLE_DEPT_MEMBER";		// 부서 회원
	public final static String ROLE_DEPT_MASTER = "ROLE_DEPT_MASTER";		// 부서 담당자
	public final static String ROLE_COMPANY_MEMBER = "ROLE_COMPANY_MEMBER";	// 업체 회원
	public final static String ROLE_COMPANY_MASTER = "ROLE_COMPANY_MASTER";	// 업체 담당자
	public final static String ROLE_SALES_MASTER = "ROLE_SALES_MASTER";		// 영업 대표
	public final static String ROLE_SALES_MEMBER = "ROLE_SALES_MEMBER";		// 영업 회원
	public final static String ROLE_CC_MEMBER = "ROLE_CC_MEMBER";			// 콜센터 직원
	public final static String ROLE_CC_MANAGER = "ROLE_CC_MANAGER";			// 콜센터 매니저
	public final static String ROLE_CC_MASTER = "ROLE_CC_MASTER";			// 콜센터 담당자
	public final static String ROLE_MANAGER = "ROLE_MANAGER";				// 오토링크 배송관리
	public final static String ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";		// 시스템 관리자

	public final static String ROLE_CONTROL_DELIVERY = "ROLE_CONTROL_DELIVERY";		// 배송관리

	public final static String ROLE_REAUTH_REQUEST_MEMBER = "ROLE_REAUTH_REQUEST_MEMBER"; //재인증 요청유저

	public final static int MEMBER_TYPE_GUEST 	= -1;		// 비회원 손님
	public final static int MEMBER_TYPE_GUEST_RIDER = -3;	// 비회원 기사

	public final static int MEMBER_TYPE_MEMBER 	= 0;		// 회원
	/**
	 * @deprecated
	 */
	public final static int MEMBER_TYPE_MERCHANT = 1;		// 배송업체 회원
	public final static int MEMBER_TYPE_CALLCENTER = 1;		// 콜센타 회원
	public final static int MEMBER_TYPE_SALES = 2;			// 영업 회원
	public final static int MEMBER_TYPE_RIDER = 3;			// 기사
	public final static int MEMBER_TYPE_MANAGER = 90;		// 배송 관리자
	public final static int MEMBER_TYPE_SUPERVISOR = 100;	// 시스템 관리자


	public static final String AUTHORITY = "AUTHORITY";
	public static final String RESOURCE_TYPE_METHOD = "METHOD";
	public static final String RESOURCE_TYPE_URL = "URL";
	public static final String RESOURCE_TYPE_POINTCUT = "POINTCUT";


	public static enum EMAIL_AUTH_TYPE {IP, IE, IN} //이메일 인증 타입 ==> IP: Id & password, IE:Id & Email, IN: Id & Name
}
