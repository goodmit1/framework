package com.fliconz.fm.security;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fliconz.fm.connectlog.ConnectLogConstant;
import com.fliconz.fm.connectlog.ConnectLogManager; 
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@Component("fmAuthenticationSuccessHandler")
public class FMAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

//    protected final Log logger = LogFactory.getLog(this.getClass());
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static final String KEY_OTHER_LOGIN="__OTHER_LOGIN__";

	@Autowired
	private UserDetailService userDetailService;
	Hashtable allLoginInfo = null;
	public FMAuthenticationSuccessHandler()
	{
		allLoginInfo = new Hashtable();
		 
	}
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		
		this.connectLog(request, response, authentication);

		boolean isRedirect = request.getAttribute("isRedirect") == null ? true: (boolean)request.getAttribute("isRedirect");
		if(isRedirect){
			if(PropertyManager.getBoolean("login.alwaysUseDefaultTargetURL", true)){
				this.setAlwaysUseDefaultTargetUrl(PropertyManager.getBoolean("login.alwaysUseDefaultTargetURL", true));
				this.setDefaultTargetUrl(PropertyManager.getString("login.defaultTargetURL", "/index.jsp"));
			}
			super.onAuthenticationSuccess(request, response, authentication);
		}

		this.initUserData(request, response, authentication);
		
	}

	protected void putLoginInfo4Single(UserVO userVo, HttpServletRequest request) {
		if(PropertyManager.getBoolean("login.single",false))
		{
			processPrevLogin4Single(userVo, request);
			allLoginInfo.put(userVo.getUserId().toString(), request.getSession());
			 
		}
		
	}
	/** 세션 종료시 호출 **/
	public void removeLoginInfo4Single( HttpSession session) {
		if(PropertyManager.getBoolean("login.single",false))
		{
			if(UserVO.isLogin())
			{
				HttpSession old_session  = (HttpSession)allLoginInfo.get(UserVO.getUser().getUserId().toString());
				if(old_session != null && session.getId().equals(old_session.getId()))
				{
					allLoginInfo.remove(UserVO.getUser().getUserId().toString());
					 
				}
				 
				 
			}
			 
		}
		
	}
	protected void processPrevLogin4Single( UserVO userVo, HttpServletRequest request) {
			HttpSession session  = (HttpSession)allLoginInfo.get(UserVO.getUser().getUserId().toString());
			if(session != null)
			{
				try
				{
					session.setAttribute(KEY_OTHER_LOGIN, "Y");
				}
				catch(Exception ignore)
				{
					
				}
			}
			 
		 
		
	}
	public void initUserData(HttpServletRequest request, HttpServletResponse response, Authentication authentication){
		UserVO userVo = (UserVO) authentication.getPrincipal();
		String locale = request.getParameter("locale");
		if(locale==null)
		{
			locale = request.getLocale().getLanguage();
		}
		if(locale != null){
			userVo.setLocale(locale);
			userVo.setLangKnd(locale);
		}

		String firstRemoteAddr = FMSecurityContextHelper.getFirstRemoteAddr();
		if(firstRemoteAddr != null){
			userVo.setUserIP(firstRemoteAddr);
		}
//		userVo.setUserMenuBean(new UserMenuBean());

		try {
			String username = authentication.getName();
			request.getSession().setAttribute(UserVO.class.getName(), userVo);
			userDetailService.updateLastAccessInfo(username);
		} catch (Exception e) {
			LoggerFactory.getLogger("authentication").warn("사용자 접속 정보 업데이트 실패");
		}
		LoggerFactory.getLogger("authentication").debug("로그인 성공!");
		putLoginInfo4Single(userVo, request);
	}

	protected void connectLog(HttpServletRequest request, HttpServletResponse response, Authentication authentication){
		try {
			if(PropertyManager.getBoolean("connectLog.login.use", true)){
				UserVO userVO = (UserVO)authentication.getPrincipal();
				String[] roles = PropertyManager.getStringArray("connectLog.login.roles", null);
				int prgmId = ConnectLogConstant.LOGIN_PRGM_ID;
				String prgmNm = ConnectLogConstant.LOGIN_PRGM_NM;
				boolean isLoging = (roles == null || roles.length == 0);
				if(roles != null){
					for(String role: roles){
						if(userVO.hasRole(role)) {
							isLoging = true;
							break;
						}
					}
				}
				if(isLoging) ConnectLogManager.loggingDB(this.getClass(), this.getLogParamMap(prgmId, prgmNm, request.getRemoteAddr()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 한서버에서만 로그인 하게 할 경우만 사용가능
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public HttpSession getSession( String userid ) throws Exception
	{
		
		return (HttpSession)allLoginInfo.get(userid);
		 
	}
	/**
	 * 접속 로그 삽입 시 기본 정보를 리턴해주는 함수
	 * @param prgmId
	 * @param prgmNm
	 * @param ip
	 * @return
	 */
	public Map<String, Object> getLogParamMap(int prgmId, String prgmNm, String ip){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("PRGM_ID", prgmId);
		paramMap.put("PRGM_NM", prgmNm);
		paramMap.put("IPADDRESS", ip);

	   	SecurityContext context = SecurityContextHolder.getContext();
	   	if(context.getAuthentication().getPrincipal() instanceof UserVO){
		    UserVO userVo = (UserVO)context.getAuthentication().getPrincipal();
		   	paramMap.put("USER_ID", userVo.getLoginId());
		   	paramMap.put("USER_NAME", userVo.getUserName());
		   	paramMap.put("TEAM_CD", userVo.getTeam().getTeamCd());
		   	paramMap.put("TEAM_NM", userVo.getTeam().getTeamNm());
	   	} else {
	   		paramMap.put("USER_ID", SecurityConstant.GUEST);
		   	paramMap.put("USER_NAME", SecurityConstant.GUEST);
	   	}

	   	String firstRemoteAddr = FMSecurityContextHelper.getFirstRemoteAddr();
		if(firstRemoteAddr != null){
			paramMap.put("IPADDRESS", firstRemoteAddr);
		}
		return paramMap;
	}
}
