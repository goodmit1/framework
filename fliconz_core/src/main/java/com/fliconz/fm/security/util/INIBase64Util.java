package com.fliconz.fm.security.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Base64?�코?�과 ?�코??처리�??�다. <br>
 * <br>
 *
 * ?�문?�호??�?복호?�시 ?�요??Base64 ?�복?�화 메서?��? ?�공?�다.<br>
 *
 * @version 1.0
 * @author rywkim@inicis.com
 *
 */

public class INIBase64Util {

	private static int lineLength;

	private static boolean lineCut;

	public static final int RFC_MAX_LINE_LENGTH = 76;

	public static final int USUAL_LINE_LENGTH = 64;

	static {
		reset();
	}

	/**
     *
     */
	public INIBase64Util() {
	}

	/**
	 * decode.
	 *
	 * @param inputstream
	 * @param outputstream
	 * @throws IOException
	 */
	public static void decode(InputStream inputstream, OutputStream outputstream) throws IOException {
		byte abyte0[] = new byte[4];
		byte abyte1[] = new byte[3];
		while (inputstream.available() != 0) {
			int i = 0;
			while (inputstream.available() != 0) {
				byte byte0 = (byte) inputstream.read();
				if (!isDecodDomain(byte0))
					continue;
				abyte0[i] = byte0;
				if (++i == 4)
					break;
			}
			if (i != 4 && i != 0)
				throw new IOException("Base64 decode fail, last: " + i);
			if (i != 0) {
				int j = decodeBlock(abyte0, abyte1);
				outputstream.write(abyte1, 0, j);
			}
		}
	}

	/**
	 * decode.
	 *
	 * @param abyte0
	 * @return byte[]
	 * @throws IOException
	 */
	public static byte[] decode(byte abyte0[]) throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		decode(new ByteArrayInputStream(abyte0), bytearrayoutputstream);
		bytearrayoutputstream.close();
		return bytearrayoutputstream.toByteArray();
	}

	/**
	 * decodeBlock.
	 *
	 * @param abyte0
	 * @param abyte1
	 * @return int
	 */
	public static final int decodeBlock(byte abyte0[], byte abyte1[]) {
		byte byte0 = 3;
		int ai[] = new int[4];
		for (int i = 0; i < 4; i++) {
			byte byte1 = abyte0[i];
			if (byte1 == 61) {
				byte0 = (i != 2) ? (byte) 2 : (byte) 1;
				break;
			}
			if (byte1 > 96)
				ai[i] = byte1 - 71;
			else if (byte1 > 64)
				ai[i] = byte1 - 65;
			else if (byte1 > 47)
				ai[i] = byte1 + 4;
			else if (byte1 == 43)
				ai[i] = 62;
			else if (byte1 == 47)
				ai[i] = 63;
		}

		abyte1[0] = (byte) ((ai[0] & 0xff) << 2 | (ai[1] & 0xff) >> 4);
		abyte1[1] = (byte) ((ai[1] & 0xff) << 4 | (ai[2] & 0xff) >> 2);
		abyte1[2] = (byte) ((ai[2] & 0xff) << 6 | ai[3] & 0xff);
		return byte0;
	}

	/**
	 * encode.
	 *
	 * @param inputstream
	 * @param outputstream
	 * @throws IOException
	 */
	public static void encode(InputStream inputstream, OutputStream outputstream) throws IOException {
		byte abyte0[] = new byte[3];
		byte abyte1[] = new byte[4];
		int i = 0;
		while (inputstream.available() != 0) {
			int j = inputstream.read(abyte0);
			encodeBlock(abyte0, j, abyte1);
			outputstream.write(abyte1);
			i += 4;
			if (lineCut && lineLength <= i) {
				outputstream.write(10);
				i = 0;
			}
		}
		if (lineCut && i != 0)
			outputstream.write(10);
	}

	/**
	 * encode.
	 *
	 * @param abyte0
	 * @return byte[]
	 * @throws IOException
	 */
	public static byte[] encode(byte abyte0[]) throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		encode(new ByteArrayInputStream(abyte0), bytearrayoutputstream);
		bytearrayoutputstream.close();
		return bytearrayoutputstream.toByteArray();
	}

	/**
	 * encode. line cut option
	 *
	 * @param inputstream
	 * @param outputstream
	 * @throws IOException
	 */
	public static void encode(InputStream inputstream, OutputStream outputstream, boolean cut) throws IOException {
		byte abyte0[] = new byte[3];
		byte abyte1[] = new byte[4];
		int i = 0;
		while (inputstream.available() != 0) {
			int j = inputstream.read(abyte0);
			encodeBlock(abyte0, j, abyte1);
			outputstream.write(abyte1);
			i += 4;
			if (cut && lineLength <= i) {
				outputstream.write(10);
				i = 0;
			}
		}
		if (cut && i != 0)
			outputstream.write(10);
	}

	/**
	 * encode. line cut option
	 *
	 * @param abyte0
	 * @return byte[]
	 * @throws IOException
	 */
	public static byte[] encode(byte abyte0[], boolean cut) throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		encode(new ByteArrayInputStream(abyte0), bytearrayoutputstream, cut);
		bytearrayoutputstream.close();
		return bytearrayoutputstream.toByteArray();
	}

	/**
	 * encodeBlock.
	 *
	 * @param abyte0
	 * @param i
	 * @param abyte1
	 */
	public static final void encodeBlock(byte abyte0[], int i, byte abyte1[]) {
		if (abyte1 == null)
			abyte1 = new byte[4];
		for (int j = 3; i < j;)
			abyte0[--j] = 0;

		abyte1[0] = (byte) ((abyte0[0] & 0xfc) >>> 2);
		abyte1[1] = (byte) ((abyte0[0] & 0x3) << 4 | (abyte0[1] & 0xf0) >>> 4);
		abyte1[2] = (byte) ((abyte0[1] & 0xf) << 2 | (abyte0[2] & 0xc0) >>> 6);
		abyte1[3] = (byte) (abyte0[2] & 0x3f);
		for (int k = 0; k < 4; k++)
			if (abyte1[k] < 26)
				abyte1[k] = (byte) (abyte1[k] + 65);
			else if (abyte1[k] < 52)
				abyte1[k] = (byte) (abyte1[k] + 71);
			else if (abyte1[k] < 62)
				abyte1[k] = (byte) (abyte1[k] - 4);
			else if (abyte1[k] == 62)
				abyte1[k] = 43;
			else if (abyte1[k] == 63)
				abyte1[k] = 47;

		if (i < 3)
			abyte1[3] = 61;
		if (i < 2)
			abyte1[2] = 61;
	}

	/**
	 * isDecodDomain.
	 *
	 * @param byte0
	 */
	private static boolean isDecodDomain(byte byte0) {
		if (byte0 > 122)
			return false;
		if (byte0 == 43)
			return true;
		if (byte0 < 47)
			return false;
		if (byte0 <= 57)
			return true;
		if (byte0 == 61)
			return true;
		if (byte0 < 65)
			return false;
		if (byte0 <= 90)
			return true;
		if (byte0 < 97)
			return false;
		return byte0 <= 122;
	}

	/**
	 * reset.
	 *
	 */
	public static void reset() {
		lineLength = 64;
		lineCut = true;
	}

	/**
	 * setLineCut.
	 *
	 * @param flag
	 */
	public static void setLineCut(boolean flag) {
		lineCut = flag;
	}

	/**
	 * setLineLength.
	 *
	 * @param i
	 */
	public static void setLineLength(int i) {
		lineLength = i;
	}

}