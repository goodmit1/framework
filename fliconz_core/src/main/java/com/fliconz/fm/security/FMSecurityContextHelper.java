package com.fliconz.fm.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.intercept.aopalliance.MethodSecurityInterceptor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.common.vo.TreeNodeMap;
import com.fliconz.fm.exception.FMPasswordCheckException;
import com.fliconz.fm.log.LogManager;
import com.fliconz.fm.security.filter.FMDelegatingMethodSecurityMetadataSource;
import com.fliconz.fm.security.filter.FMMethodSecurityMetadataSource;
import com.fliconz.fm.security.filter.FMUrlSecurityMetadataSource;
import com.fliconz.fm.security.util.FMPasswordManager;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Singleton
@Component("fmSecurityContextHelper")
public class FMSecurityContextHelper {

	
	
	public static void clearAuthentication() {
		SecurityContextHolder.getContext().setAuthentication(null);
	}

	public static void configureAuthentication(String role) {
		Collection  authorities = AuthorityUtils.createAuthorityList(role);
		Authentication authentication = new UsernamePasswordAuthenticationToken(
				"user",
				role,
				authorities
				);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	public void reloadPermissions() throws Exception {
		
		 

	 
		FMUrlSecurityMetadataSource urlSecurityMetadataSource = (FMUrlSecurityMetadataSource) SpringBeanUtil.getBean("urlSecurityMetadataSource");
		
		urlSecurityMetadataSource.reload();
		FMMethodSecurityMetadataSource methodSecurityMetadataSources=(FMMethodSecurityMetadataSource) SpringBeanUtil.getBean("methodSecurityMetadataSources");
		methodSecurityMetadataSources.reload();
		
		FMDelegatingMethodSecurityMetadataSource fmDelegatingMethodSecurityMetadataSource = (FMDelegatingMethodSecurityMetadataSource) SpringBeanUtil.getBean("delegatingMethodSecurityMetadataSource");
		fmDelegatingMethodSecurityMetadataSource.clearCache();
	}
	public static boolean chkPattern(String password, String patterns)
	{
			String[] arr = patterns.split("\n");
			for(String arr1 : arr)
			{
				if(!password.matches( arr1  ))
				{
					System.out.println(arr1 + "invalid password");
					return false;
					
				}
					
			}
					
		return true;
	}
	public static boolean findRepeat(String password, int repeatCnt)
	{
		for(int i=0; i < password.length();i++)
		{
			char c = password.charAt(i);
			boolean isFind=true;
			if(i+repeatCnt<=password.length())
			{
				for(int j=0;j<repeatCnt-1;j++)
				{
					if(c  != (int)(password.charAt(i+j+1)) )
					{
						isFind=false;
						break;
					}
					c=password.charAt(i+j+1);
				}
				if(isFind) return true;
			}
		}
		return false;
	}
	public static boolean findSequence(String password, int cnt)
	{
		for(int i=0; i < password.length();i++)
		{
			char c = password.charAt(i);
			boolean isFind=true;
			if(i+cnt<=password.length())
			{
				for(int j=0;j<cnt-1;j++)
				{
					if(c+1 != (int)(password.charAt(i+j+1)) )
					{
						isFind=false;
						break;
					}
					c=password.charAt(i+j+1);
				}
				if(isFind) return true;
			}
		}
		return false;
	}
	public static void checkPasswordSecuritySetting(UserVO userVO, String password,String confirmPassWord) throws FMPasswordCheckException{
		FMSecurityContextHelper fmSecurityContextHelper = (FMSecurityContextHelper)SpringBeanUtil.getBean("fmSecurityContextHelper");
		fmSecurityContextHelper.checkPasswordSecurity(userVO, password, confirmPassWord);
	}

	public   void checkPasswordSecurity(UserVO userVO, String password,String confirmPassWord) throws FMPasswordCheckException{
		String pattern = PropertyManager.getString("password.security.pattern");
		if(!password.equals(confirmPassWord)) {
			throw new FMPasswordCheckException(MessageBundle.getString("FM_USER_PASS_VALIDATION_COMPARE", FMSecurityContextHelper.getLang()));
		}
		if(!chkPattern(password,pattern)) {
			throw new FMPasswordCheckException(MessageBundle.getString("FM_USER_PASS_VALIDATION_CHECK", FMSecurityContextHelper.getLang()));
		}

		 
		if(PropertyManager.getBoolean("password.security.disableSameUserId", true) && password.contains( userVO.getLoginId() )) {
			throw new FMPasswordCheckException(MessageBundle.getString("FM_USER_PASS_VALIDATION_USER_ID_CHECK", FMSecurityContextHelper.getLang()));
		}

		if(PropertyManager.getBoolean("password.security.disableSameUserInfo", true)){
			if(  ( !TextHelper.isEmpty(userVO.getUserName()) && password.contains(userVO.getUserName()))
				|| (!TextHelper.isEmpty(userVO.getMobileNo()) && password.contains(userVO.getMobileNo()) )
				|| (!TextHelper.isEmpty(userVO.getOfficeNo()) && password.contains(userVO.getOfficeNo() ))
				|| (!TextHelper.isEmpty(userVO.getEmail()) && password.contains( userVO.getEmail())))
			{
				throw new FMPasswordCheckException(MessageBundle.getString("FM_USER_PASS_VALIDATION_PI_CHECK", FMSecurityContextHelper.getLang()));
			}
		}
		/*if(PropertyManager.getBoolean("password.security.disableSameUserTeamInfo", true)){
			if(userVO.getTeam().getTeamNm().contains(password)
				|| userVO.getTeam().getActiveTeamNm().contains(password)
			)throw new Exception(MessageBundle.getString("FM_USER_PASS_VALIDATION_DEPT_NO_CHECK"));

		}*/
		if(PropertyManager.getBoolean("password.security.invalidWords.use", true)){
			String[] invalidWords = PropertyManager.getStringArray("password.security.invalidWords");
			for(String word: invalidWords){
				if(password.contains(word)){
					throw new FMPasswordCheckException(MessageBundle.getString("FM_USER_PASS_VALIDATION_KEY_WORD_CHECK", FMSecurityContextHelper.getLang()));
				}
			}
		}
		
		int repeatCount = PropertyManager.getInt("password.security.seqCharCount", 0);
		if(repeatCount>1)
		{
			if(findSequence(password,repeatCount))
			{
				throw new FMPasswordCheckException(MessageBundle.getString("FM_USER_PASS_VALIDATION_CONTINUE_CHAR", FMSecurityContextHelper.getLang())); //123,abc처럼 연속문자가 있습니다.");
			}
		}
		// 동일 문자 반복 제한
		repeatCount = PropertyManager.getInt("password.security.sameCharCount", 0);
		if(repeatCount>1)
		{
			if(findRepeat(password,repeatCount))
			{
				throw new FMPasswordCheckException(MessageBundle.getString("FM_USER_PASS_VALIDATION_SAME_REPEAT", new String[] { ""+repeatCount }, FMSecurityContextHelper.getLang())); //동일 문자가 "  + repeatCount + "회 이상 반복되었습니다.");
			}
		}
		boolean disableSamePwd=PropertyManager.getBoolean("password.security.disableSamePwd", false);
		if(disableSamePwd){
			if(userVO.getPassword().equals(FMPasswordManager.getPassword(userVO, password)))
			{
				throw new FMPasswordCheckException(MessageBundle.getString("FM_USER_PASS_VALIDATION_SAME_PREV", FMSecurityContextHelper.getLang())); //이전과 같은 비밀번호를 사용할 수 없습니다.");
			}
		}
	}
	public static String getRedirectPage(HttpServletRequest request) throws Exception {
		UserVO user = UserVO.getUser();
		String errorCode = request.getParameter("e");
		String redirectPage = PropertyManager.getString("defaultURL.login", "/login/login.xhtml");

		if(FMSecurityContextHelper.isMobile(request)){
			if(user != null){
				if(errorCode == null) {
					redirectPage = PropertyManager.getString("defaultURL.home.mobile", "/home/mobile/home.xhtml");
				} else {
					redirectPage = PropertyManager.getString("defaultURL.error.mobile", "/error/mobile/error.xhtml");
				}
			} else {
				redirectPage = PropertyManager.getString("defaultURL.login.mobile", "/login/mobile/login.xhtml");
			}
		} else {
			if(user != null){
				if(errorCode == null) {
					redirectPage = PropertyManager.getString("defaultURL.home", "/home/home.xhtml");
				} else {
					redirectPage = PropertyManager.getString("defaultURL.error", "/error/error.xhtml");
				}
			}
		}
		return request.getContextPath() + redirectPage;
	}

	public static enum OS_TYPE {android, iPhone, window, etc}
	public static boolean isMobile(HttpServletRequest request) {
		if(!PropertyManager.getBoolean("server.runtime.mobile.use", true)) return false;
		String userAgent = request.getHeader("User-Agent");
		boolean isMobile = false;
		if(!PropertyManager.getBoolean("server.runtime.isMobile", isMobile)){
			if(userAgent != null && !"".equals(userAgent)){
				String os = getOS(request);
				isMobile = (os.equals(OS_TYPE.android.toString()) || os.equals(OS_TYPE.iPhone.toString())
							|| userAgent.toLowerCase().contains("mobile"));
			}
		} else {
			isMobile = true;
		}
		return isMobile;
	}


	public static String getOS(HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		if(userAgent.toLowerCase().contains(OS_TYPE.android.toString())) return OS_TYPE.android.toString();
		else if(userAgent.toLowerCase().contains(OS_TYPE.iPhone.toString())) return OS_TYPE.iPhone.toString();
		else if(userAgent.toLowerCase().contains(OS_TYPE.window.toString())) return OS_TYPE.window.toString();
		return OS_TYPE.etc.toString();
	}
	public static boolean checkMethodPermission(String cmdType){
		UserVO user = UserVO.getUser();
		if(user.isSuperAdmin()) return false;
		TreeNodeMap menu = user.getUserMenuManager().getSelectedMenu() ;
		if(menu == null) {
			String requestURI = (String) UserVO.getCurrentLoginUser().getMemberData().get("requestURI");
			if(requestURI != null){
			 
				return FMSecurityContextHelper.checkMethodPermissionByURL(cmdType, requestURI);
			 
			}
			else {
				return true;
			}
		}	
		else {
			return checkMethodPermission(cmdType, menu.getPkgNm() );	
		}
		
	}
	public static boolean checkMethodPermissionByMenuId(String cmdType, int menuId){
		UserVO user = UserVO.getUser();
		return checkMethodPermissionByMenuId(user, cmdType, menuId);
	}
	
	public static boolean checkMethodPermissionByURL(String cmdType, String url){
		UserVO user = UserVO.getUser();
		TreeNodeMap menu = user.getUserMenuManager().findMenuByPrgmPath(url);
		if(menu==null) return true;
		return checkMethodPermission(  cmdType, menu.getPkgNm());
	}
	
	public static boolean checkMethodPermissionByMenuId(UserVO user,String cmdType, int menuId){
		 
		TreeNodeMap menu = user.getUserMenuManager().findMenuById(menuId);
		return checkMethodPermission(user, cmdType, menu != null ? menu.getPkgNm() : null);
	}
	public static boolean checkMethodPermission(  String cmdType, String pkgNm){
		if(pkgNm == null) {
			
		}
		return checkMethodPermission(UserVO.getUser(),cmdType, pkgNm);
	}
	public static boolean checkMethodPermission(UserVO user, String cmdType, String pkgNm){
		LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "--------- BEGIN: checkMethodPermission: method=["+cmdType+"]");
		boolean isDisabled = true;
		 
		if(pkgNm != null && !user.hasRole(SecurityConstant.ROLE_SUPER_ADMIN) && PropertyManager.getString("permission.method.check.types", "select,insert,update,delete,print").contains(cmdType)){
			LinkedHashMap<String, List<ConfigAttribute>> methodMap = (LinkedHashMap<String, List<ConfigAttribute>>)SpringBeanUtil.getBean("methodMap");
			String currentButtonKey = pkgNm + "..*@";
			if("print".equals(cmdType)){
				currentButtonKey = currentButtonKey.replace("@", "View.*.print");
			} else {
				currentButtonKey = currentButtonKey.replace("@", "Service.*." + cmdType + ".*");
			}
			LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "currentButtonKey=["+currentButtonKey+"]");
			List<ConfigAttribute> configs = methodMap.get(currentButtonKey);
			if(configs != null){
				if(PropertyManager.getBoolean("permisstion.role.loginId.use", false)){
					for(ConfigAttribute config: configs){
						if(config.getAttribute().equals(user.getLoginId())){
							isDisabled = false;
							break;
						}
					}
				} else {
					for(ConfigAttribute config: configs){
						if(user.hasRole(config.getAttribute())){
							isDisabled = false;
							break;
						}
					}
				}
			}
			 
		} else if(user.hasRole(SecurityConstant.ROLE_SUPER_ADMIN)) {
			isDisabled = false;
		} 
		else if( !PropertyManager.getString("permission.method.check.types", "select,insert,update,delete,print").contains(cmdType))
		{
			isDisabled = false;
		}
		LogManager.debug(LogManager.LOGGER_AUTHENTICATION, "--------- END: checkMethodPermission: method=["+cmdType+"], isDisabled=["+isDisabled+"]");
		return isDisabled;
	}

    private static final String HEADER_X_FORWARDED_FOR = "X-FORWARDED-FOR";

    public static String getFirstRemoteAddr(HttpServletRequest request) {
        String remoteAddr = request.getRemoteAddr();
        String x;
        if ((x = request.getHeader(HEADER_X_FORWARDED_FOR)) != null) {
            remoteAddr = x;
            int idx = remoteAddr.indexOf(',');
            if (idx > -1) {
                remoteAddr = remoteAddr.substring(0, idx);
            }
        }
        return remoteAddr;
	}

    public static String getFirstRemoteAddr() {
    	HttpServletRequest request = null;
    	if(RequestContextHolder.currentRequestAttributes() instanceof ServletRequestAttributes){
    		request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
    		return getFirstRemoteAddr(request);
    	} else {
    		return null;
    	}

	}

    public static HttpServletRequest getRequest(){
    	return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static String getLang(){
		if(UserVO.getUser() != null){
			return UserVO.getUser().getLocale();
		}
		return FMSecurityContextHelper.getRequest().getLocale().getLanguage();
	}

    public static String getExpression(String pkgNm, String method){
		String[] methods = PropertyManager.getStringArray("permission.method.check.types", new String[]{"select,insert,update,delete,print"});
		for(String pMethod: methods){
			if(method.startsWith(pMethod)){
				String expression = pkgNm + "..*@";
				if("print".equals(pMethod)){
					expression = expression.replace("@", "View.*.print");
				} else {
					expression = expression.replace("@", "Service.*." + pMethod + ".*");
				}
				return expression;
			}
		}
		return null;
	}

    public static String getServerDomain(){
    	return getServerDomain(getRequest());
    }

    public static String getServerDomain(HttpServletRequest request){
    	String protocolVer = request.getProtocol();
    	String protocol = protocolVer.substring(0, protocolVer.indexOf("/")).toLowerCase();
    	return protocol + (request.getServerPort() == 443 ? "s" : "") + "://" + request.getServerName() + (request.getServerPort() == 80 || request.getServerPort() == 443 ? "": ":" + request.getServerPort());
    }

}
