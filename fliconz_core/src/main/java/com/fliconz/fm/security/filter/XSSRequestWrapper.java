package com.fliconz.fm.security.filter;

import com.nhncorp.lucy.security.xss.XssFilter;
import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XSSRequestWrapper extends HttpServletRequestWrapper
{
  Set<String> allowHTMLField;

  public XSSRequestWrapper(HttpServletRequest httpServletRequest, Set<String> allowHTMLField)
  {
    super(httpServletRequest);
    this.allowHTMLField = allowHTMLField;
  }

  public String[] getParameterValues(String name)
  {
    String[] values = super.getParameterValues(name);
    if (values == null) return null;
    boolean doPreventer = getPreventerFlag(name).booleanValue();
    String[] result = new String[values.length];
    int idx = 0;
    for (String v : values)
    {
      result[(idx++)] = doFilter(v, doPreventer);
    }
    return result;
  }

  public String getParameter(String name)
  {
    String value = super.getParameter(name);
    if (value == null) return null;
    return doFilter(value, getPreventerFlag(name).booleanValue());
  }

  public String getHeader(String name)
  {
    String value = super.getHeader(name);
    if (value == null) return null;
    return doFilter(value, getPreventerFlag(name).booleanValue());
  }

  private String doFilter(String value, boolean doPreventer) {
    if (doPreventer) {
      return XssPreventer.escape(value);
    }
    XssFilter xssFilter = XssFilter.getInstance("lucy-xss.xml", true);
    return xssFilter.doFilter(value);
  }

  private Boolean getPreventerFlag(String name)
  {
    if (name == null) return Boolean.valueOf(true);
    return Boolean.valueOf((this.allowHTMLField == null) || (!this.allowHTMLField.contains(name)));
  }
}