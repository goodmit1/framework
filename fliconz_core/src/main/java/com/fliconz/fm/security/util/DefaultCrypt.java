package com.fliconz.fm.security.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.fliconz.fw.runtime.util.PropertyManager;

public class DefaultCrypt implements ICrypt {

	private String transformation = "RC4";

	public String getTransformation() {
		return transformation;
	}

	public void setTransformation(String transformation) {
		this.transformation = transformation;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	private String algorithm = "RC4";

	public DefaultCrypt() {
		transformation = PropertyManager.getString("crypt.transformation","RC4");
		algorithm = PropertyManager.getString("crypt.algorithm","RC4");
	}
	public static void main(String[] args) {
		DefaultCrypt crypt = new DefaultCrypt();
		try {
			System.out.println(crypt.encrypt("01000000000"));
			String str = "+5FMtBi8H6c5oJl0keAmEvNpvBC5c2jJRr4es812NIg=\n";
			System.out.println(crypt.encrypt("01000000000"));
			System.out.println(crypt.decrypt(str));
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public String encryptPwd(String pwd) throws Exception {
		return PasswordHash.createHash(pwd);

	}

	public boolean isSamePwd(String inputPwd, String pwdHash) throws Exception {
		return PasswordHash.validatePassword(inputPwd, pwdHash);

	}

	protected String encrypt(String plainText, String mrechant_seedkey, String merchant_iv) throws Exception {

		byte[] merchantKey = INIBase64Util.decode(mrechant_seedkey.getBytes());
		SecretKeySpec keyspec = new SecretKeySpec(merchantKey, algorithm);

		Cipher cipher = Cipher.getInstance(transformation);
		if (merchant_iv != null) {
			cipher.init(Cipher.ENCRYPT_MODE, keyspec, new IvParameterSpec(merchant_iv.getBytes()));
		} else {
			cipher.init(Cipher.ENCRYPT_MODE, keyspec);
		}

		return new String(INIBase64Util.encode(cipher.doFinal(plainText.getBytes())));
	}

	public String encrypt(String field) throws Exception {

		return encrypt(field, PropertyManager.getString("seed_key", "OjK6mHE1ajPt5XUuoTvOvw=="),
				PropertyManager.getString("ivParameter"));

	}

	protected String decrypt(String encryptedText, String mrechant_seedkey, String merchant_iv) throws Exception {
		byte[] merchantKey = INIBase64Util.decode(mrechant_seedkey.getBytes());
		SecretKeySpec keyspec = new SecretKeySpec(merchantKey, algorithm);

		Cipher cipher = Cipher.getInstance(transformation);
		if (merchant_iv != null) {
			cipher.init(Cipher.DECRYPT_MODE, keyspec, new IvParameterSpec(merchant_iv.getBytes()));
		} else {
			cipher.init(Cipher.DECRYPT_MODE, keyspec);
		}

		return new String(cipher.doFinal(INIBase64Util.decode(encryptedText.getBytes())));
	}

	public String decrypt(String field) throws Exception {

		return decrypt(field, PropertyManager.getString("seed_key", "OjK6mHE1ajPt5XUuoTvOvw=="),
				PropertyManager.getString("ivParameter"));

	}

}
