package com.fliconz.fm.security.filter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fliconz.fm.security.PermissionService;


public class FMUrlSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

	private final Map<String, Collection<ConfigAttribute>> requestMap;

	@Autowired
	private PermissionService permissionService;

	public FMUrlSecurityMetadataSource(LinkedHashMap<String, Collection<ConfigAttribute>> requestMap) {
		this.requestMap = requestMap;
	}

	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
		HttpServletRequest request = ((FilterInvocation)object).getRequest();
		Collection<ConfigAttribute> result = null;
		for(Map.Entry<String, Collection<ConfigAttribute>> entry: requestMap.entrySet()){
			if(new AntPathRequestMatcher(entry.getKey().toString()).matches(request)){
				result = entry.getValue();
				break;
			}
		}
		return result;
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		Set<ConfigAttribute> allAttributes = new HashSet<ConfigAttribute>();
		for(Map.Entry<String, Collection<ConfigAttribute>> entry: requestMap.entrySet()){
			allAttributes.addAll(entry.getValue());
		}
		return allAttributes;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return FilterInvocation.class.isAssignableFrom(clazz);
	}

	public void reload() throws Exception {
		synchronized (requestMap) {
			LinkedHashMap<String, List<ConfigAttribute>> reloadedMap = permissionService.getRoleUrlList();
			Iterator<Entry<String, List<ConfigAttribute>>> it = reloadedMap.entrySet().iterator();
			while(it.hasNext()){
				Entry<String, List<ConfigAttribute>> entry = it.next();
				reloadedMap.put(entry.getKey(), entry.getValue());
			}
			requestMap.clear();
			requestMap.putAll(reloadedMap);
		}
	}


}
