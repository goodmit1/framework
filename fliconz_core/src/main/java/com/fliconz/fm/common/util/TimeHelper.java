/*
 * @(#)TimeHelper.java
 */
package com.fliconz.fm.common.util;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * <pre>
 * [개요]
 * 날짜/시간 관련 유틸리티 클리스
 *
 * [상세설명]
 *
 *
 *
 * [변경이력]
 * 2008.06.11::이경구::최초작성
 *
 *
 * @author 이경구
 * </pre>
 */
public class TimeHelper extends TextHelper {

	private final static int GREGORIAN_CUT_YEAR = 1582;

	private final static int[] MONTH_LAST_DAY = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };


	//십간(十干), 천간(天干) : the ten calendar signs
	private final static String[] KR_TEN_SIGN = {"갑", "을", "병", "정", "무", "기", "경", "신", "임", "계" };

	private final static String[] CH_TEN_SIGN = {"甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"};

	//십이지(十二支), 지지(地支) : the towelve calendar signs
	private final static String[] KR_TOWELVE_SIGN = {"자", "축", "인", "묘", "진", "사", "오", "미", "신", "유", "술", "해" };

	private final static String[] CH_TOWELVE_SIGN = {"子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"};


//	private final static String[] EN_MONTH_NAME = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
//	private final static String[] KR_MONTH_NAME = {"1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"};
//	private final static String[] CH_MONTH_NAME = {"1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"};

//	private final static String[] EN_DAY_OF_WEEK = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
//	private final static String[] KR_DAY_OF_WEEK = {"일", "월", "화", "수", "목", "금", "토"};
//	private final static String[] CH_DAY_OF_WEEK = {"日", "月", "火", "水", "木", "金", "土"};

	//##############################################################################################################################

	/**
	* Construct a <code>TimeHelper</code>
	*/
	protected TimeHelper() {
		super();
	}



	//##############################################################################################################################


	public static String format(java.util.Date date, String pattern)
	{
		return format(date, pattern, null);
	}

	public static String format(java.sql.Date date, String pattern)
	{
		return format(date, pattern, null);
	}

	public static String format(long dateSince1970, String pattern)
	{
		return format(dateSince1970, pattern, null);
	}

	public static String format(Time date, String pattern)
	{
		return format(date, pattern, null);
	}

	public static String format(Timestamp date, String pattern)
	{
		return format(date, pattern, null);
	}


	public static String format(String date, String from, String to) throws ParseException
	{
		return format(date, from, to, null);
	}

	/**
	 * 해당 일자를 주어진 패턴으로 일자 형식을 변환한다.
	 *
	 * @param date 일자
	 * @param pattern 일자 패턴
	 *
	 * @return 변환된 날자 문자열
	 */
	public static String format(java.util.Date date, String pattern, Locale loc)
	{
		SimpleDateFormat sdf = loc == null ? new SimpleDateFormat(pattern) : new SimpleDateFormat(pattern, loc);
		return date == null
			? null
			: sdf.format(date);
	}

	/**
	 * 해당 일자를 주어진 패턴으로 일자 형식을 변환한다.
	 *
	 * @param date 일자
	 * @param pattern 일자 패턴
	 *
	 * @return 변환된 날자 문자열
	 */
	public static String format(java.sql.Date date, String pattern, Locale loc)
	{
		return date == null
			? null
			: format( (java.util.Date)date, pattern, loc);
	}


	public static String format(long dateSince1970, String pattern, Locale loc)
	{
		return format(new java.util.Date(dateSince1970), pattern, loc);
	}


	/**
	 * 해당 일자를 주어진 패턴으로 일자 형식을 변환한다.
	 *
	 * @param date 일자
	 * @param pattern 일자 패턴
	 *
	 * @return 변환된 날자 문자열
	 */
	public static String format(Time date, String pattern, Locale loc)
	{
		return date == null
			? null
			: format( (java.util.Date)date, pattern, loc);
	}

	/**
	 * 해당 일자를 주어진 패턴으로 일자 형식을 변환한다.
	 *
	 * @param date 일자
	 * @param pattern 일자 패턴
	 *
	 * @return 변환된 날자 문자열
	 */
	public static String format(Timestamp date, String pattern, Locale loc)
	{
		return date == null
			? null
			: format( (java.util.Date)date, pattern, loc);
	}

	/**
	 * 해당 일자를 from 패턴에서 to 패턴으로 일자 형식을 변환한다.
	 *
	 * @param date 일자
	 * @param from 소스 일자 패턴
	 * @param to 타겟 일자 패턴
	 *
	 * @return 변환된 날자 문자열
	 * @throws java.text.ParseException
	 */
	public static String format(String date, String from, String to, Locale loc) throws ParseException
	{
		return date == null
			? null
			: format(parseDate(date, from), to, loc);
	}


	//------------------------------------------------------------------------------------------------------------------------------

	/**
	 * 해당 문자열 일자를 일자 패턴으로 읽어 일자 객체로 변환한다.
	 *
	 *
	 * @param date 문자열 일자
	 * @param pattern 일자 패턴
	 * @return 변환된 날자
	 *
	 * @throws java.text.ParseException
	 */
	public static java.util.Date parseDate(String date, String pattern) throws ParseException {
		return date == null
			? null
			: new SimpleDateFormat(pattern).parse(date);
	}

	/**
	 * 해당 문자열 일자를 일자 패턴으로 읽어 일자 객체로 변환한다.
	 *
	 *
	 * @param date 문자열 일자
	 * @param pattern 일자 패턴
	 * @return 변환된 날자
	 *
	 * @throws java.text.ParseException
	 */
	public static java.sql.Date parseSQLDate(String date, String pattern) throws ParseException {
		return date == null
			? null
			: new java.sql.Date( parseDate(date, pattern).getTime() );
	}

	/**
	 * 해당 문자열 일자를 일자 패턴으로 읽어 일자 객체로 변환한다.
	 *
	 *
	 * @param date 문자열 일자
	 * @param pattern 일자 패턴
	 * @return 변환된 날자
	 *
	 * @throws java.text.ParseException
	 */
	public static Time parseTime(String date, String pattern) throws ParseException {
		return date == null
			? null
			: new Time( parseDate(date, pattern).getTime() );
	}

	/**
	 * 해당 문자열 일자를 일자 패턴으로 읽어 일자 객체로 변환한다.
	 *
	 *
	 * @param date 문자열 일자
	 * @param pattern 일자 패턴
	 * @return 변환된 날자
	 *
	 * @throws java.text.ParseException
	 */
	public static Timestamp parseTimestamp(String date, String pattern) throws ParseException {
		return date == null
			? null
			: new Timestamp( parseDate(date, pattern).getTime() );
	}


	//##############################################################################################################################


	public static java.util.Date parseDate(int year, int month, int day, int hour, int minute, int second) {
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(year, month-1, day, hour, minute, second);

		return aCalendar.getTime();
	}


	public static java.util.Date parseDate(int year, int month, int day, int hour, int minute) {
		return parseDate(year, month, day, hour, minute, 0);
	}


	public static java.util.Date parseDate(int year, int month, int day, int hour) {
		return parseDate(year, month, day, hour, 0, 0);
	}


	public static java.util.Date parseDate(int year, int month, int day) {
		return parseDate(year, month, day, 9, 0, 0);
	}


	public static java.util.Date parseDate(long millisecond) {
		return new java.util.Date(millisecond);
	}


	//------------------------------------------------------------------------------------------------------------------------------


	public static java.sql.Date parseSQLDate(int year, int month, int day, int hour, int minute, int second) {
		return new java.sql.Date( parseDate(year, month, day, hour, minute, second).getTime() );
	}


	public static java.sql.Date parseSQLDate(int year, int month, int day, int hour, int minute) {
		return new java.sql.Date( parseDate(year, month, day, hour, minute).getTime() );
	}


	public static java.sql.Date parseSQLDate(int year, int month, int day, int hour) {
		return new java.sql.Date( parseDate(year, month, day, hour).getTime() );
	}


	public static java.sql.Date parseSQLDate(int year, int month, int day) {
		return new java.sql.Date( parseDate(year, month, day).getTime() );
	}


	public static java.sql.Date parseSQLDate(long millisecond) {
		return new java.sql.Date(millisecond);
	}


	//------------------------------------------------------------------------------------------------------------------------------


	public static Time parseTime(int year, int month, int day, int hour, int minute, int second) {
		return new Time( parseDate(year, month, day, hour, minute, second).getTime() );
	}


	public static Time parseTime(int year, int month, int day, int hour, int minute) {
		return new Time( parseDate(year, month, day, hour, minute).getTime() );
	}


	public static Time parseTime(int year, int month, int day, int hour) {
		return new Time( parseDate(year, month, day, hour).getTime() );
	}


	public static Time parseTime(int year, int month, int day) {
		return new Time( parseDate(year, month, day).getTime() );
	}


	public static Time parseTime(long millisecond) {
		return new Time(millisecond);
	}


	//------------------------------------------------------------------------------------------------------------------------------


	public static Timestamp parseTimestamp(int year, int month, int day, int hour, int minute, int second) {
		return new Timestamp( parseDate(year, month, day, hour, minute, second).getTime() );
	}


	public static Timestamp parseTimestamp(int year, int month, int day, int hour, int minute) {
		return new Timestamp( parseDate(year, month, day, hour, minute).getTime() );
	}


	public static Timestamp parseTimestamp(int year, int month, int day, int hour) {
		return new Timestamp( parseDate(year, month, day, hour).getTime() );
	}


	public static Timestamp parseTimestamp(int year, int month, int day) {
		return new Timestamp( parseDate(year, month, day).getTime() );
	}


	public static Timestamp parseTimestamp(long millisecond) {
		return new Timestamp(millisecond);
	}


	//##############################################################################################################################


	public static int getLastDate(int year, int month) {
		return (month == 2 && isLeapYear(year)) ? 29 : MONTH_LAST_DAY[month-1];
	}


	public static java.util.Date getLastDate(java.util.Date date) {
		if (date == null) {
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		cal.set(
			Calendar.DATE,
			getLastDate( cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1 )
		);

		return cal.getTime();
	}


	public static java.sql.Date getLastDate(java.sql.Date date) {
		return date == null
			? null
			: new java.sql.Date( getLastDate( (java.util.Date)date ).getTime() );
	}


	public static Time getLastDate(Time date) {
		return date == null
			? null
			: new Time( getLastDate( (java.util.Date)date ).getTime() );
	}


	public static Timestamp getLastDate(Timestamp date) {
		return date == null
			? null
			: new Timestamp( getLastDate( (java.util.Date)date ).getTime() );
	}


	//##############################################################################################################################


	public static java.util.Date addField(java.util.Date date, int field,  int amount) {
		if (date == null) {
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(field, amount);
		return cal.getTime();
	}


	public static java.util.Date addYear(java.util.Date date, int year) {
		return date == null
			? null
			: addField(date, Calendar.YEAR, year);
	}


	public static java.util.Date addMonth(java.util.Date date, int month) {
		return date == null
			? null
			: addField(date, Calendar.MONTH, month);
	}


	public static java.util.Date addWeek(java.util.Date date, int week) {
		return date == null
			? null
			: addField(date, Calendar.DATE, week*7);
	}


	public static java.util.Date addDate(java.util.Date date, int day) {
		return date == null
			? null
			: addField(date, Calendar.DATE, day);
	}

	/**
	 * 반나절을 더해서 날짜를 계산함. 오후에 더하면 다음날 오전.
	 * @param date
	 * @param halfDay
	 * @return
	 */
	public static java.util.Date addHalfDate(java.util.Date date, int halfDay) {
		return date == null
			? null
			: addField(date, Calendar.AM_PM, halfDay);
	}


	public static java.util.Date addHour(java.util.Date date, int hour) {
		return date == null
			? null
			: addField(date, Calendar.HOUR, hour);
	}


	public static java.util.Date addMinute(java.util.Date date, int min) {
		return date == null
			? null
			: addField(date, Calendar.MINUTE, min);
	}


	public static java.util.Date addSecond(java.util.Date date, int sec) {
		return date == null
			? null
			: addField(date, Calendar.SECOND, sec);
	}


	public static java.util.Date addMilliSecond(java.util.Date date, int ms) {
		return date == null
			? null
			: addField(date, Calendar.MILLISECOND, ms);
	}



	//##############################################################################################################################


	public static java.util.Date addFieldToCurrentTime(int field,  int amount) {
		return addField(new java.util.Date(), field, amount);
	}


	public static java.util.Date addYearToCurrentTime(int year) {
		return addField(new java.util.Date(), Calendar.YEAR, year);
	}


	public static java.util.Date addMonthToCurrentTime(int month) {
		return addField(new java.util.Date(), Calendar.MONTH, month);
	}


	public static java.util.Date addWeekToCurrentTime(int week) {
		return addField(new java.util.Date(), Calendar.DATE, week*7);
	}


	public static java.util.Date addDateToCurrentTime(int day) {
		return addField(new java.util.Date(), Calendar.DATE, day);
	}

	/**
	 * 반나절을 더해서 날짜를 계산함. 오후에 더하면 다음날 오전.
	 * @param date
	 * @param halfDay
	 * @return
	 */
	public static java.util.Date addHalfDateToCurrentTime(int halfDay) {
		return addField(new java.util.Date(), Calendar.AM_PM, halfDay);
	}


	public static java.util.Date addHourToCurrentTime(int hour) {
		return addField(new java.util.Date(), Calendar.HOUR, hour);
	}


	public static java.util.Date addMinuteToCurrentTime(int min) {
		return addField(new java.util.Date(), Calendar.MINUTE, min);
	}


	public static java.util.Date addSecondToCurrentTime(int sec) {
		return addField(new java.util.Date(), Calendar.SECOND, sec);
	}


	public static java.util.Date addMilliSecondToCurrentTime(int ms) {
		return addField(new java.util.Date(), Calendar.MILLISECOND, ms);
	}


	//##############################################################################################################################


	public static long getMilliSecondGap(java.util.Date from, java.util.Date to) {
		if (from == null || to == null) {
			return 0;
		}
		return to.getTime() - from.getTime();
	}


	public static double getSecondGap(java.util.Date from, java.util.Date to) {
		if (from == null || to == null) {
			return 0;
		}
		return getMilliSecondGap(from, to) / 1000;
	}


	public static double getMinuteGap(java.util.Date from, java.util.Date to) {
		if (from == null || to == null) {
			return 0;
		}
		return getMilliSecondGap(from, to) / (60*1000);
	}


	public static double getHourGap(java.util.Date from, java.util.Date to) {
		if (from == null || to == null) {
			return 0;
		}
		return getMilliSecondGap(from, to) / (60*60*1000);
	}


	public static double getDateGap(java.util.Date from, java.util.Date to) {
		if (from == null || to == null) {
			return 0;
		}
		return getMilliSecondGap(from, to) / (24*60*60*1000);
	}


	//##############################################################################################################################


	public static int getField(java.util.Date date, int field) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return field == Calendar.MONTH ? cal.get(field)+1 : cal.get(field);
	}


	public static int getYear(java.util.Date date) {
		return date == null
			? 0
			: getField(date, Calendar.YEAR);
	}


	public static int getMonth(java.util.Date date) {
		return date == null
			? 0
			: getField(date, Calendar.MONTH);
	}


	public static int getDate(java.util.Date date) {
		return date == null
			? 0
			: getField(date, Calendar.DATE);
	}


	public static int getHour(java.util.Date date) {
		return date == null
			? 0
			: getField(date, Calendar.HOUR);
	}


	public static int getMinute(java.util.Date date) {
		return date == null
			? 0
			: getField(date, Calendar.MINUTE);
	}


	public static int getSecond(java.util.Date date) {
		return date == null
			? 0
			: getField(date, Calendar.SECOND);
	}


	public static int getMilliSecond(java.util.Date date) {
		return date == null
			? 0
			: getField(date, Calendar.MILLISECOND);
	}

	//1 : 일, 2 : 월, 3 : 화, 4 : 수, 5 : 목, 6 : 금, 7 : 토

	public static int getDayOfWeek(java.util.Date date) {
		return date == null
			? 0
			: getField(date, Calendar.DAY_OF_WEEK);
	}


	//##############################################################################################################################

	public static String getCurrentTime(String pattern, Locale loc)
	{
		return format(new java.util.Date(), pattern, loc);
	}

	public static String getCurrentTime(String pattern)
	{
		return format(new java.util.Date(), pattern);
	}


	public static String getCurrentTime17()
	{
		return format(new java.util.Date(), "yyyyMMddHHmmssSSS");
	}


	public static String getCurrentTime12()
	{
		return format(new java.util.Date(), "yyyyMMddHHmm");
	}

	public static String getCurrentTime14()
	{
		return format(new java.util.Date(), "yyyyMMddHHmmss");
	}
	public static String getCurrentTime8()
	{
		return format(new java.util.Date(), "yyyyMMdd");
	}


	public static boolean isLeapYear(int year) {
		return year >= GREGORIAN_CUT_YEAR
			? ( year%4 == 0 && (year%100 != 0 || year%400 == 0) )	//Gregorian
			: ( year%4 == 0 ); //Julian
	}


	public static String getSixtySign(int year, boolean isChinese) {
		return isChinese
			? new StringBuilder( CH_TEN_SIGN[(year+6) % 10] )
				.append( CH_TOWELVE_SIGN[(year+8) % 12] )
				.append("年")
				.toString()
			: new StringBuilder( KR_TEN_SIGN[(year+6) % 10] )
				.append( KR_TOWELVE_SIGN[(year+8) % 12] )
				.append("년")
				.toString();
	}


	/**
	 * format 형태의 문자열 날짜 정보를 Calendar로 변환
	 * @param dateStr
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static Calendar toCalendar(String dateStr, String format) throws ParseException {

		if (dateStr == null || dateStr.equals(""))
		{
			return null;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.parse(dateStr);

		return dateFormat.getCalendar();
	}


}
