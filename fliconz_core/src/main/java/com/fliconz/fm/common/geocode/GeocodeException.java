package com.fliconz.fm.common.geocode;

public class GeocodeException extends Exception
{
	public GeocodeException(String msg, Throwable cause)
	{
		super(msg, cause);
	}


	public GeocodeException(Throwable cause)
	{
		super(cause);
	}


	public GeocodeException(String msg)
	{
		super(msg);
	}


	public GeocodeException()
	{
		super();
	}
}

