/*
 * @(#)ArrayEnumeration..java
 */
package com.fliconz.fm.common.util;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * <pre>
 * [개요]
 * 배열 목록을 java.util.Enumeration, java.util.Iterator 인터페이스로 표현
 *
 * [상세설명]
 *
 *
 * [변경이력]
 * 2008.06.11::이경구::최초작성
 *
 *
 * @author 이경구
 * </pre>
 * @param <E>
 */
public class ArrayEnumeration<E> implements Enumeration<E>, Iterator<E>
{
	private Object[] m_arr = null;
	private boolean m_isReverse = false;

	private int m_intLen = 0;
	private int m_intIndex = 0;

	//##############################################################################################################################

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>java.lang.Object[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(Object[] arr, boolean reverse) {
		setArray(arr, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>int[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(int[] arr, boolean reverse) {
		if (arr == null) {
			return;
		}

		Integer[] aArray = new Integer[arr.length];
		for (int i=0; i<arr.length; i++) {
			aArray[i] = new Integer(arr[i]);
		}

		setArray(aArray, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>long[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(long[] arr, boolean reverse) {
		if (arr == null) {
			return;
		}

		Long[] aArray = new Long[arr.length];
		for (int i=0; i<arr.length; i++) {
			aArray[i] = new Long(arr[i]);
		}

		setArray(aArray, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>float[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(float[] arr, boolean reverse) {
		if (arr == null) {
			return;
		}

		Float[] aArray = new Float[arr.length];
		for (int i=0; i<arr.length; i++) {
			aArray[i] = new Float(arr[i]);
		}

		setArray(aArray, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>double[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(double[] arr, boolean reverse) {
		if (arr == null) {
			return;
		}

		Double[] aArray = new Double[arr.length];
		for (int i=0; i<arr.length; i++) {
			aArray[i] = new Double(arr[i]);
		}

		setArray(aArray, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>short[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(short[] arr, boolean reverse) {
		if (arr == null) {
			return;
		}

		Short[] aArray = new Short[arr.length];
		for (int i=0; i<arr.length; i++) {
			aArray[i] = new Short(arr[i]);
		}

		setArray(aArray, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>char[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(char[] arr, boolean reverse) {
		if (arr == null) {
			return;
		}

		Character[] aArray = new Character[arr.length];
		for (int i=0; i<arr.length; i++) {
			aArray[i] = new Character(arr[i]);
		}

		setArray(aArray, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>boolean[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(boolean[] arr, boolean reverse) {
		if (arr == null) {
			return;
		}

		Boolean[] aArray = new Boolean[arr.length];
		for (int i=0; i<arr.length; i++) {
			aArray[i] = new Boolean(arr[i]);
		}

		setArray(aArray, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>byte[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	public ArrayEnumeration(byte[] arr, boolean reverse) {
		if (arr == null) {
			return;
		}

		Byte[] aArray = new Byte[arr.length];
		for (int i=0; i<arr.length; i++) {
			aArray[i] = new Byte(arr[i]);
		}

		setArray(aArray, reverse);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>java.lang.Object[]</code>.
	*/
	public ArrayEnumeration(Object[] arr) {
		this(arr, false);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>int[]</code>.
	*/
	public ArrayEnumeration(int[] arr) {
		this(arr, false);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>long[]</code>.
	*/
	public ArrayEnumeration(long[] arr) {
		this(arr, false);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>float[]</code>.
	*/
	public ArrayEnumeration(float[] arr) {
		this(arr, false);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>double[]</code>.
	*/
	public ArrayEnumeration(double[] arr) {
		this(arr, false);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>short[]</code>.
	*/
	public ArrayEnumeration(short[] arr) {
		this(arr, false);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>char[]</code>.
	*/
	public ArrayEnumeration(char[] arr) {
		this(arr, false);
	}

	/**
	* Array enumeration constructor.
	*
	* @param arr an array <code>byte[]</code>.
	*/
	public ArrayEnumeration(byte[] arr) {
		this(arr, false);
	}

	//##############################################################################################################################

	/**
	* Set an array.
	*
	* @param arr an array<code>java.lang.Object[]</code>.
	* @param reverse ascending <code>true</code>, descending <code>false</code>.
	*/
	private void setArray(Object[] arr, boolean reverse) {
		m_arr = arr;
		m_isReverse = reverse;

		m_intIndex = ( m_intLen = arr == null ? 0 : arr.length ) > 0
			? reverse ? m_intLen-1 : 0
			: -1;
	}



	/**
	* Tests if this enumeration contains more elements.
	*
	* @return <code>true</code> if and only if this enumeration object contains at least one more element to provide; <code>false</code> otherwise.
	*/
	public boolean hasMoreElements() {
		return m_intIndex >= 0 && m_intIndex < m_intLen;
	}

	/**
	* Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
	*
	* @return the next element of this enumeration.
	* @exception <code>java.util.NoSuchElementException</code> if no more elements exist.
	*/
	public E nextElement() {
		if ( !hasMoreElements() ) {
			throw new NoSuchElementException("The next element is not found.");
		}

		return (E) m_arr[m_isReverse ? m_intIndex-- : m_intIndex++];
	}

	//##############################################################################################################################

	/**
	* Calculates the number of times that this enumeration's nextElement method can be called before it generates an exception.
	* The current position is not advanced.
	*
	* @return the number of elements remaining in this enumeration.
	*/
	public int countElements() {
		return hasMoreElements()
			? m_isReverse ? m_intIndex+1 : m_intLen - m_intIndex
			: 0;
	}

	//##############################################################################################################################

	/**
	* Tests if this enumeration contains more elements.
	*
	* @return <code>true</code> if and only if this enumeration object contains at least one more element to provide; <code>false</code> otherwise.
	*/
	public boolean hasNext() {
		return hasMoreElements();
	}

	/**
	* Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
	*
	* @return the next element of this enumeration.
	* @exception <code>java.util.NoSuchElementException</code> if no more elements exist.
	*/
	public E next() {
		return nextElement();
	}

	/**
	 * Removes from the underlying collection the last element returned by the iterator (optional operation).
	 * This method can be called only once per call to next.
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling this method.
	 */
	public void remove() {
		throw new UnsupportedOperationException("remove() method is not supported");
	}
}
