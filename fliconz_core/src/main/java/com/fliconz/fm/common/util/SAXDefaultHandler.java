/*
 * @(#)SAXDefaultHandler.java
 */
package com.fliconz.fm.common.util;

import java.io.Writer;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.IOException;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.TransformerException;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXDefaultHandler extends DefaultHandler
{
	private PrintWriter m_pw = null;

	//##############################################################################################################################

	/**
	 * SAXDefaultHandler constructor
	 *
	 * @param pw
	 */
	public SAXDefaultHandler(PrintWriter pw) {
		m_pw = pw;
	}

	/**
	 * SAXDefaultHandler constructor
	 *
	 * @param ps
	 */
	public SAXDefaultHandler(PrintStream ps) {
		this(new PrintWriter(ps, true));
	}

	/**
	 * SAXDefaultHandler constructor
	 */
	public SAXDefaultHandler() {
		//this(new PrintWriter(System.out, true));
	}

	//##############################################################################################################################

	protected PrintWriter getWriter() {
		return m_pw;
	}

	//------------------------------------------------------------------------------------------------------------------------------------------------
	//org.xml.sax.ErrorHandler

	/**
	* Receive notification of a recoverable parser error.
	*
	* @param cause The warning information encoded as an exception
	*
	* @throws SAXException The warning information encoded as an exception.
	*/
	public void error(SAXParseException cause) throws SAXException {
		if (m_pw != null) {
			m_pw.println("[org.xml.sax.ErrorHandler-Error]");
			_printLocation(m_pw, cause);
		}
		throw cause;
	}

	/**
	* Report a fatal XML parsing error.
	*
	* @param cause The error information encoded as an exception.
	*
	* @throws SAXException The warning information encoded as an exception.
	*/
	public void fatalError(SAXParseException cause) throws SAXException {
		if (m_pw != null) {
			m_pw.println("[org.xml.sax.ErrorHandler-FatalError]");
			_printLocation(m_pw, cause);
		}
		throw cause;
	}

	/**
	* Receive notification of a parser warning.
	*
	* @param cause The warning information encoded as an exception.
	*
	* @throws SAXException The warning information encoded as an exception.
	*/
	public void warning(SAXParseException cause) throws SAXException {
		if (m_pw != null) {
			m_pw.println("[org.xml.sax.ErrorHandler-Warning]");
			_printLocation(m_pw, cause);
		}
		throw cause;
	}

	//------------------------------------------------------------------------------------------------------------------------------------------------
	//org.xml.sax.EntityResolver

	/**
	*  Allow the application to resolve external entities.
	*
	* @param publicId The public identifier of the external entity being referenced, or null if none was supplied.
	* @param systemId The system identifier of the external entity being referenced.
	*
	* @return An InputSource object describing the new input source, or null to request that the parser open a regular URI connection to the system identifier.
	*
	* @throws SAXException Any SAX exception, possibly wrapping another exception.
	* @throws IOException A Java-specific IO exception, possibly the result of creating a new InputStream or Reader for the InputSource.
	*/
	public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
		return super.resolveEntity(publicId, systemId);
	}

	//------------------------------------------------------------------------------------------------------------------------------------------------
	//org.xml.sax.DTDHandler


	public void notationDecl(String name, String publicId, String systemId) throws SAXException {
		super.notationDecl(name, publicId, systemId);
	}


	public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName) throws SAXException {
		super.unparsedEntityDecl(name, publicId, systemId, notationName);
	}

	//------------------------------------------------------------------------------------------------------------------------------------------------
	//org.xml.sax.ContentHandler


	public void characters(char[] ch, int start, int len) throws SAXException {
		super.characters(ch, start, len);
	}


	public void endDocument() throws SAXException {
		super.endDocument();
	}


	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		super.endElement(namespaceURI, localName, qName);
	}


	public void endPrefixMapping(String prefix) throws SAXException {
		super.endPrefixMapping(prefix);
	}


	public void ignorableWhitespace(char[] ch, int start, int len) throws SAXException {
		super.ignorableWhitespace(ch, start, len);
	}


	public void processingInstruction(String target, String data) throws SAXException {
		super.processingInstruction(target, data);
	}


	public void setDocumentLocator(Locator loc) {
		super.setDocumentLocator(loc);
	}


	public void skippedEntity(String name) throws SAXException {
		super.skippedEntity(name);
	}


	public void startDocument() throws SAXException {
		super.startDocument();
	}


	public void startElement(String namespaceURI, String localName, String qName, Attributes attrs) throws SAXException {
		super.startElement(namespaceURI, localName, qName, attrs);
	}


	public void startPrefixMapping(String prefix, String namespaceURI) throws SAXException {
		super.startPrefixMapping(prefix, namespaceURI);
	}


	//##############################################################################################################################

	public static void printLocation(Writer out, TransformerException cause) {
		_printLocation(
			out instanceof PrintWriter
				? (PrintWriter)out
				: new PrintWriter(out, true),
			(Throwable)cause
		);
	}


	public static void printLocation(OutputStream out, TransformerException cause) {
		_printLocation(
			new PrintWriter(out, true),
			(Throwable)cause
		);
	}


	public static void printLocation(Writer out, SAXParseException cause) {
		_printLocation(
			out instanceof PrintWriter
				? (PrintWriter)out
				: new PrintWriter(out, true),
			(Throwable)cause
		);
	}


	public static void printLocation(OutputStream out, SAXParseException cause) {
		_printLocation(
			new PrintWriter(out, true),
			(Throwable)cause
		);
	}



	//##############################################################################################################################

	private static void _printLocation(PrintWriter pw, Throwable cause) {
		SourceLocator aLocator = null;
		Throwable aCause = cause;

		do {
			if (aCause instanceof SAXParseException) {
				aLocator = new SAXSourceLocator((SAXParseException)aCause);
				aCause = ((SAXParseException)aCause).getException();
			}

			else if (aCause instanceof TransformerException) {
				aLocator = ((TransformerException)aCause).getLocator();
				aCause = ((TransformerException)aCause).getCause();
			}

			else {
				aCause = aCause instanceof SAXException
					? ((SAXException)aCause).getException()
					: null;
			}
		}
		while (aCause != null);


		if(aLocator != null) {
			pw.print("- Location : ");

			if (aLocator.getPublicId() != null && aLocator.getSystemId() == null) {
				pw.print(aLocator.getPublicId());
				pw.print(" ");
			}

			else if (aLocator.getPublicId() == null && aLocator.getSystemId() != null) {
				pw.print(aLocator.getSystemId());
				pw.print(" ");
			}

			else if(aLocator.getPublicId() != null && aLocator.getSystemId() != null) {
				pw.print(aLocator.getPublicId());
				pw.print(", ");
				pw.print(aLocator.getSystemId());
				pw.print(" ");
			}

			pw.print("(Line=");
			pw.print(aLocator.getLineNumber());
			pw.print(", Column=");
			pw.print(aLocator.getColumnNumber());
			pw.println(")");
		}

		pw.print("- Message : ");
		pw.println( cause.getMessage() );
	}
}
