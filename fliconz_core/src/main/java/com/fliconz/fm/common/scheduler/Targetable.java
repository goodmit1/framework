package com.fliconz.fm.common.scheduler;

import java.util.Iterator;

public interface Targetable
{
	public Iterator<String> getTargetNames();

	public boolean hasTarget(String targetName);


	public String getTargets();

	public void setTargets(String targetName);
}
