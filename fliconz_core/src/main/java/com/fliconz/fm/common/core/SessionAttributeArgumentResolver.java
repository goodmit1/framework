package com.fliconz.fm.common.core;

import java.lang.annotation.Annotation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

public class SessionAttributeArgumentResolver implements WebArgumentResolver
{
	@Override
	public Object resolveArgument(MethodParameter param, NativeWebRequest request) throws Exception
	{
		Annotation[] paramAnns = param.getParameterAnnotations();
		Class<?> paramType = param.getParameterType();

		for (Annotation paramAnn : paramAnns)
		{
			if (SessionAttribute.class.isInstance(paramAnn))
			{
				SessionAttribute aSessionAttribute = (SessionAttribute)paramAnn;
				HttpSession aSession = ((HttpServletRequest)request.getNativeRequest()).getSession(true);
				Object attrValue = aSession.getAttribute(aSessionAttribute.value());

				if (attrValue == null && aSessionAttribute.required())
				{
					raiseMissingParameterException(aSessionAttribute.value(), paramType);
				}
				else
				{
					return attrValue;
				}
			}
		}

		return WebArgumentResolver.UNRESOLVED;
	}

	protected void raiseMissingParameterException(String paramName, Class<?> paramType) throws Exception
	{
		throw new IllegalStateException(
				new StringBuilder()
					.append("Missing parameter '")
					.append(paramName)
					.append("' of type [")
					.append(paramType.getName())
					.append("]")
					.toString()
				);
	}

}
