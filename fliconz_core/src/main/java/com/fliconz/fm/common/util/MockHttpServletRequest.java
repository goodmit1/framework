package com.fliconz.fm.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

public class MockHttpServletRequest implements HttpServletRequest
{
	private ServletContext m_ctx = null;
	private Hashtable<String,String[]> m_param = null;
	private Hashtable<String,Object> m_attr = null;

	public MockHttpServletRequest(ServletContext ctx, Map<String,Object> attr, Map<String,String[]> param)
	{
		this.m_ctx = ctx;
		this.m_attr = new Hashtable<String,Object>();
		this.m_param = new Hashtable<String,String[]>();

		if (attr != null) this.m_attr.putAll(attr);
		if (param != null) this.m_param.putAll(param);

	}
	public MockHttpServletRequest(ServletContext ctx)
	{
		this(ctx, null, null);
	}

	public MockHttpServletRequest()
	{
		this(null, null, null);
	}




	@Override
	public String getParameter(String name)
	{
		Object aValue = this.m_param.get(name);
		if (aValue == null)
		{
			return null;
		}

		if (aValue instanceof String[])
		{
			String[] arr = (String[])aValue;
			if (arr.length <= 0)
			{
				return null;
			}
			return arr[0];
		}
		else if (aValue instanceof String)
		{
			return (String)aValue;
		}

		return aValue.toString();
	}

	@Override
	public Enumeration<String> getParameterNames()
	{
		return this.m_param.keys();
	}

	@Override
	public String[] getParameterValues(String name)
	{
		return this.m_param.get(name);
	}

	@Override
	public Map<String, String[]> getParameterMap()
	{
		return m_param;
	}




	@Override
	public Object getAttribute(String name)
	{
		return this.m_attr.get(name);
	}

	@Override
	public Enumeration<String> getAttributeNames()
	{
		return this.m_attr.keys();
	}

	@Override
	public void setAttribute(String name, Object o)
	{
		if (o == null)
		{
			this.m_attr.remove(name);
			return;
		}
		this.m_attr.put(name, o);
	}

	@Override
	public void removeAttribute(String name)
	{
		this.m_attr.remove(name);
	}





	@Override
	public String getCharacterEncoding()
	{
		return null;
	}

	@Override
	public void setCharacterEncoding(String env) throws UnsupportedEncodingException
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public int getContentLength()
	{
		return 0;
	}

	@Override
	public String getContentType()
	{
		return null;
	}

	@Override
	public ServletInputStream getInputStream() throws IOException
	{
		return null;
	}


	@Override
	public String getProtocol()
	{
		return null;
	}

	@Override
	public String getScheme()
	{
		return null;
	}

	@Override
	public String getServerName()
	{
		return null;
	}

	@Override
	public int getServerPort()
	{
		return 0;
	}

	@Override
	public BufferedReader getReader() throws IOException
	{
		return null;
	}

	@Override
	public String getRemoteAddr()
	{
		return null;
	}

	@Override
	public String getRemoteHost()
	{
		return null;
	}

	@Override
	public Locale getLocale()
	{
		return null;
	}

	@Override
	public Enumeration<Locale> getLocales()
	{
		return new ArrayEnumeration(new Locale[]{});
	}

	@Override
	public boolean isSecure()
	{
		return false;
	}

	@Override
	public RequestDispatcher getRequestDispatcher(String path)
	{
		return null;
	}

	@Override
	public String getRealPath(String path)
	{
		if (m_ctx == null)
		{
			return null;
		}
		return m_ctx.getRealPath(path);
	}

	@Override
	public int getRemotePort()
	{
		return 0;
	}

	@Override
	public String getLocalName()
	{
		return null;
	}

	@Override
	public String getLocalAddr()
	{
		return null;
	}

	@Override
	public int getLocalPort()
	{
		return 0;
	}

	@Override
	public ServletContext getServletContext()
	{
		return m_ctx;
	}

	@Override
	public AsyncContext startAsync() throws IllegalStateException
	{
		return null;
	}

	@Override
	public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException
	{
		return null;
	}

	@Override
	public boolean isAsyncStarted()
	{
		return false;
	}

	@Override
	public boolean isAsyncSupported()
	{
		return false;
	}

	@Override
	public AsyncContext getAsyncContext()
	{
		return null;
	}

	@Override
	public DispatcherType getDispatcherType()
	{
		return null;
	}

	@Override
	public String getAuthType()
	{
		return null;
	}

	@Override
	public Cookie[] getCookies()
	{
		return null;
	}

	@Override
	public long getDateHeader(String name)
	{
		return 0;
	}

	@Override
	public String getHeader(String name)
	{
		return null;
	}

	@Override
	public Enumeration<String> getHeaders(String name)
	{
		return new ArrayEnumeration(new String[]{});
	}

	@Override
	public Enumeration<String> getHeaderNames()
	{
		return new ArrayEnumeration(new String[]{});
	}

	@Override
	public int getIntHeader(String name)
	{
		return 0;
	}

	@Override
	public String getMethod()
	{
		return null;
	}

	@Override
	public String getPathInfo()
	{
		return null;
	}

	@Override
	public String getPathTranslated()
	{
		return null;
	}

	@Override
	public String getContextPath()
	{
		if (m_ctx == null)
		{
			return null;
		}

		return m_ctx.getContextPath();
	}

	@Override
	public String getQueryString()
	{
		return null;
	}

	@Override
	public String getRemoteUser()
	{
		return null;
	}

	@Override
	public boolean isUserInRole(String role)
	{
		return false;
	}

	@Override
	public Principal getUserPrincipal()
	{
		return null;
	}

	@Override
	public String getRequestedSessionId()
	{
		return null;
	}

	@Override
	public String getRequestURI()
	{
		return null;
	}

	@Override
	public StringBuffer getRequestURL()
	{
		return null;
	}

	@Override
	public String getServletPath()
	{
		return null;
	}

	@Override
	public HttpSession getSession(boolean create)
	{
		return null;
	}

	@Override
	public HttpSession getSession()
	{
		return null;
	}

	@Override
	public boolean isRequestedSessionIdValid()
	{
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromCookie()
	{
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromURL()
	{
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromUrl()
	{
		return false;
	}

	@Override
	public boolean authenticate(HttpServletResponse response) throws IOException, ServletException
	{
		return false;
	}

	@Override
	public void login(String username, String password) throws ServletException
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void logout() throws ServletException
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public Collection<Part> getParts() throws IOException, ServletException
	{
		return null;
	}

	@Override
	public Part getPart(String name) throws IOException, ServletException
	{
		return null;
	}

}
