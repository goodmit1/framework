package com.fliconz.fm.common.snslogin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fliconz.fm.util.RequestHelper;

public class NaverLogin extends ISNSLogin {

	public NaverLogin(HttpSession session, String clientId, String clientSecret, String redirectUrl) throws Exception {
		super(session, clientId, clientSecret, redirectUrl);
	}

	public NaverLogin(HttpSession session, String clientId, String clientSecret, String adminKey, String redirectUrl) throws Exception {
		super(session, clientId, clientSecret, adminKey, redirectUrl);
	}

	@Override
	protected String getFirstRequestUrl(String state) {

	   	 return "https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=" + clientId + "&redirect_uri=" + this.redirectUrl + "&state=" + state;

	}
	protected SNSUser getSNSUser(JSONObject returnJson)
	{
		if(returnJson.has("response")){
			JSONObject json = returnJson.getJSONObject("response");
			SNSUser user = new SNSUser();
			user.id = Integer.toString(json.getInt("id"));
			user.email = json.has("email") ? json.getString("email"): null;
			user.name = json.has("name") ? json.getString("name"): null;
			user.hasEmail = user.email!=null;
			user.accessToken = json.has("access_token") ? json.getString("access_token"): null;
			return user;
		} else {
			return null;
		}

	}
	protected String getAccessTokenUrl(String state, String code )
	{
		return "https://nid.naver.com/oauth2.0/token?client_id="+ clientId + "&client_secret=" + clientSecret+ "&grant_type=authorization_code&state=" + state+ "&code=" + code;
	}
	protected  String getProfileUrl(String access_token) {
		return "https://openapi.naver.com/v1/nid/me";
	}

	protected String getDynamicAgreeRequestUrl(String state, String scope) {
		return "https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=" + clientId + "&redirect_uri=" + this.redirectUrl + "&state=" + state + "&auth_type=reprompt";
	}

	public String getScope(){
		return "profile,account_email";
	}

	@Override
	protected String getUnlinkUrl() {
		return "https://nid.naver.com/oauth2.0/token";
	}

	@Override
	public boolean unlink(String sns_id, String access_token) throws Exception {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("client_id", this.clientId);
		data.put("client_secret", this.clientSecret);
		data.put("access_token", access_token);
		data.put("grant_type", "delete");
		data.put("service_provider", "NAVER");
		JSONObject json = RequestHelper.sendRequest(this.getUnlinkUrl(), RequestMethod.GET, "", data);
		String result = json.getString("result");
		return "success".equals(result);
	}

	@Override
	protected String getAccessTokeInfoUrl() {
		return "https://openapi.naver.com/v1/nid/me";
	}

	@Override
	protected String getRefreshTokeUrl() {
		return "https://nid.naver.com/oauth2.0/token";
	}

	public JSONObject refreshToken(String access_token, String refresh_token) throws Exception {
		String accessTokeInfoUrl = this.getAccessTokeInfoUrl();
		Map<String, String> header = new HashMap<String, String>();
		header.put("Authorization", "Bearer " + access_token);
		JSONObject json = null;

		json = RequestHelper.getJSON(accessTokeInfoUrl, header);
		String resultcode = json.getString("resultcode");
		//1분 미만일 때 리프레시
		if("024".equals(resultcode)){
			String refreshTokenUrl = this.getRefreshTokeUrl();
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("grant_type", "refresh_token");
			data.put("client_id", clientId);
			data.put("refresh_token", refresh_token);
			data.put("client_secret", clientSecret);
			json = RequestHelper.sendRequest(refreshTokenUrl, RequestMethod.GET, "", data);
		} else if("00".equals(resultcode)){
			json.put("access_token", access_token);
		}
		return json;

	}
}
