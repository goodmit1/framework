package com.fliconz.fm.common.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class AbstractTargetObject implements ApplicationContextAware
{
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	private ApplicationContext m_appContext = null;
	private Object m_executeParam = null;

	public AbstractTargetObject()
	{
		super();
	}

	public AbstractTargetObject(Object executeParam)
	{
		this();
		this.setExecuteParam(executeParam);
	}



	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException
	{
		m_appContext = ctx;
		//if (theLogger.isDebugEnabled()) theLogger.debug("===> setApplicationContext: {}", ctx.getClass().getName());
		this.refineExecuteParam();
	}

	public ApplicationContext getApplicationContext()
	{
		return m_appContext;
	}

	/**
	 * ApplicationContext, ExecuteParam 설정 이후 ExecuteParam 설정을 변경할 때 하위 클래스에서 구현한다.
	 */
	protected void refineExecuteParam()
	{

	}

	public void setExecuteParam(Object param)
	{
		m_executeParam = param;
		this.refineExecuteParam();
	}

	public Object getExecuteParam()
	{
		return m_executeParam;
	}


	public abstract void doExecute();
}
