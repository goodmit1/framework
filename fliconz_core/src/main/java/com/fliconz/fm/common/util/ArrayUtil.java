package com.fliconz.fm.common.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ArrayUtil {
	public  static int indexOf(String[] arr, String val)
	{
		int idx = 0;
		for(String a:arr)
		{
			if(a.equals(val)) return idx;
			idx++;
		}
		return -1;

	}
	public static String[] remove(String[] arr, String... val)
	{
		 
		List<String> list = new ArrayList();
		
		for(String a:arr)
		{
			if(ArrayUtil.indexOf(val, a)>=0) continue;
			list.add(a);
		}
		String[] result = new String[list.size()];
		list.toArray(result);
		return result;
		 
	}
	public static String[] concat(String[] arr1, String[] arr2)
	{
		String[] result = new String[arr1.length+arr2.length];
		System.arraycopy(arr1, 0, result, 0, arr1.length);
		System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
		return result;
	}
	
	
	public static void main(String[] args)
	{
		System.out.println(TextHelper.join(ArrayUtil.remove(new String[]{"mobis","hynix"}, "mobis2")));
		System.out.println(TextHelper.join(ArrayUtil.concat(new String[]{"mobis","hynix"}, new String[]{"mobis2","hynix2"})));
		
	}

}
