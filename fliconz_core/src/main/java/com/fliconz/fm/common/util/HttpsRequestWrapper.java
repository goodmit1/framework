package com.fliconz.fm.common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpsRequestWrapper extends HttpServletRequestWrapper
{
	private final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	private final static String KEY_COOKIE_OVERWRITTEN_FLAG = String.format("%s.COOKIE_OVERWRITTEN_FLAG", HttpsRequestWrapper.class.getName());
	private HttpServletResponse m_response = null;
	private String m_sessionCookieName = null;

	public HttpsRequestWrapper (HttpServletRequest request, String sessionCookieName)
	{
		super(request);
		this.m_sessionCookieName = sessionCookieName;
	}

	public void setResponse(HttpServletResponse response)
	{
		this.m_response = response;
	}

	public HttpSession getSession()
	{
		HttpSession session = super.getSession();
		processSessionCookie(session);
		return session;
	}

	public HttpSession getSession(boolean create)
	{
		HttpSession session = super.getSession(create);
		processSessionCookie(session);
		return session;
	}

	// http, https 세션공유를 위한 처리
	public void processSessionCookie(HttpSession session)
	{
		if (null == this.m_response || null == session)  return;

		Object cookieOverWritten = getAttribute(KEY_COOKIE_OVERWRITTEN_FLAG);
//		if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-000 > cookieOverWritten: {}", cookieOverWritten);
//		if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-000 > isSecure: {}", isSecure());
//		if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-000 > isRequestedSessionIdFromCookie: {}", isRequestedSessionIdFromCookie());
//		if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-000 > isRequestedSessionIdFromURL: {}", isRequestedSessionIdFromURL());
//		if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-000 > session.isNew: {}", session.isNew());
//		if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-000 > session.getId: {}", session.getId());
//		if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-000 > session.getId: {}", session.getId());
		if ((cookieOverWritten == null || cookieOverWritten.toString().equals("true") == false)
			//&& isSecure()
			&& isRequestedSessionIdFromCookie()
			//&& session.isNew()
			)
		{
//			if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-111");
//			String sessionId = null;
//			if (isRequestedSessionIdFromURL())
//			{
//				sessionId = this.getRequestedSessionId();
//			}
//			else if (isRequestedSessionIdFromCookie())
//			{
//				sessionId = session.getId();
//			}

			String sessionId = session.getId();
			if (sessionId != null)
			{
				String contextPath = getContextPath();
				Cookie cookie = new Cookie(this.m_sessionCookieName == null ? "JSESSIONID" : this.m_sessionCookieName, sessionId);
				//cookie.setMaxAge(24*3600);
				cookie.setMaxAge(-1);

				if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie > sessionCookie: [{}]=[{}]", this.m_sessionCookieName,cookie.getValue());
				if ((contextPath != null) && (contextPath.length() > 0))
				{
					cookie.setPath(contextPath);
				}
				else
				{
					cookie.setPath("/");
				}

				//cookie.setSecure(this.getScheme().equals("https"));
				this.m_response.addCookie(cookie);
				setAttribute(KEY_COOKIE_OVERWRITTEN_FLAG, "true");
				//if (theLogger.isDebugEnabled()) theLogger.debug("===> processSessionCookie-333");
			}
		}
	}
}
