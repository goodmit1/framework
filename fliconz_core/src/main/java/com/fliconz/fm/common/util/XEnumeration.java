/*
 * @(#)XEnumeration.java
 */
package com.fliconz.fm.common.util;

import java.util.*;
/**
 * <pre>
 * [개요]
 * 여러형태의 목록을 java.util.Enumeration/java.util.Iterator 인터페이스로 표현하기 위한 클래스
 *
 * [상세설명]
 *
 *
 *
 * [변경이력]
 * 2008.06.11::이경구::최초작성
 *
 *
 * @author 이경구
 * </pre>
 */
public class XEnumeration<E> implements Enumeration<E>, Iterator<E>
{
	private Iterator<E> m_iterator = null;


	//##############################################################################################################################

	/**
	* iterator enumeration constructor.
	*
	* @param it <code>java.util.Iterator</code> interface.
	*/
	public XEnumeration(Iterator<E> it) {
		m_iterator = it;
	}

	/**
	* collection enumeration constructor.
	*
	* @param c <code>java.util.Collection</code> interface.
	*/
	public XEnumeration(Collection<E> c) {
		if (c != null) {
			m_iterator = c.iterator();
		}
	}

	/**
	* map enumeration constructor.
	* keys or values elements.
	*
	* @param m <code>java.util.Map</code> interface.
	* @param isKey if <code>true</code>, keys element; if <code>false</code> values element.
	*/
	public XEnumeration(Map<E,?> m, boolean isKey) {
		if (m != null)
		{
			if (isKey)
			{
				m_iterator = m.keySet().iterator();
			}
			else
			{
				m_iterator = (Iterator<E>) m.values().iterator();
			}
		}
//		if (m != null) {
//			m_iterator = isKey ? m.keySet().iterator() : m.values().iterator();
//		}
	}

	/**
	* map enumeration constructor.
	* keys elements.
	*
	* @param m <code>java.util.Map</code> interface.
	*/
	public XEnumeration(Map<E,?> m) {
		this(m, true);
	}

	//##############################################################################################################################

	/**
	* Tests if this enumeration contains more elements.
	*
	* @return <code>true</code> if and only if this enumeration object contains at least one more element to provide; <code>false</code> otherwise.
	*/
	public boolean hasMoreElements() {
		return m_iterator == null
			? false
			: m_iterator.hasNext();
	}

	/**
	* Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
	*
	* @return the next element of this enumeration.
	* @exception <code>java.util.NoSuchElementException</code> if no more elements exist.
	*/
	public E nextElement() {
		if (m_iterator == null) {
			throw new NoSuchElementException();
		}

		return m_iterator.next();
	}

	//##############################################################################################################################

	/**
	* Tests if this enumeration contains more elements.
	*
	* @return <code>true</code> if and only if this enumeration object contains at least one more element to provide; <code>false</code> otherwise.
	*/
	public boolean hasNext() {
		return hasMoreElements();
	}

	/**
	* Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
	*
	* @return the next element of this enumeration.
	* @exception <code>java.util.NoSuchElementException</code> if no more elements exist.
	*/
	public E next() {
		return nextElement();
	}

	/**
	 * Removes from the underlying collection the last element returned by the iterator (optional operation).
	 * This method can be called only once per call to next.
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling this method.
	 */
	public void remove() {
		throw new UnsupportedOperationException("remove() method is not supported");
	}
}
