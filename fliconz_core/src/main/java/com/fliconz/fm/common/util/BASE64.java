/**
* A Base64 Encoder/Decoder.
*
* <p>
* This class is used to encode and decode data in Base64 format as described in RFC 1521.
*
* <p>
* This is "Open Source" software and released under the <a href="http://www.gnu.org/licenses/lgpl.html">GNU/LGPL</a> license.<br>
* It is provided "as is" without warranty of any kind.<br>
* Copyright 2003: Christian d'Heureuse, Inventec Informatik AG, Switzerland.<br>
* Home page: <a href="http://www.source-code.biz">www.source-code.biz</a><br>
*
* <p>
* Version history:<br>
* 2003-07-22 Christian d'Heureuse (chdh): Module created.<br>
* 2005-08-11 chdh: Lincense changed from GPL to LGPL.<br>
* 2006-11-21 chdh:<br>
*  &nbsp; Method encode(String) renamed to encodeString(String).<br>
*  &nbsp; Method decode(String) renamed to decodeString(String).<br>
*  &nbsp; New method encode(byte[],int) added.<br>
*  &nbsp; New method decode(String) added.<br>
*/
package com.fliconz.fm.common.util;

import org.apache.commons.codec.binary.Base64;


public class BASE64 {

	public static String encodeString (String s) {
		return new String(Base64.encodeBase64(s.getBytes()));
	}

	public static String encodeString (byte[] s) {
		return new String(Base64.encodeBase64(s));
	}

	public static String decodeString (String s) {
		return new String(Base64.decodeBase64(s.getBytes()));
	}

	public static String decodeString (byte[] s) {
		return new String(Base64.decodeBase64(s));
	}

	public static byte[] encodeByte (String s) {
		return Base64.encodeBase64(s.getBytes());
	}

	public static byte[] endcodeByte (byte[] s) {
		return Base64.encodeBase64(s);
	}

	public static byte[] decodeByte (String s) {
		return Base64.decodeBase64(s.getBytes());
	}

	public static byte[] decodeByte (byte[] s) {
		return Base64.decodeBase64(s);
	}
}
