/*
 * @(#)GetDataFromDataTransferTag.java
 */
package com.fliconz.fm.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;

public class GetDataFromDataMapTag extends BodyTagSupport
{
	private String m_strName = null;
	private String m_strClass = null;
	@SuppressWarnings("unused")
	private boolean m_isDeclare = true;
	private DataMap m_aDataMap = null;

	//##############################################################################################################################

	/**
	* Construct a <code>GetDataFromDataTransferTag</code>.
	*/
	public GetDataFromDataMapTag() {
		super();
	}


	//##############################################################################################################################

	/**
	* Release a value attribute.
	*
	* Called on a Tag handler to release state.
	* The page compiler guarantees that JSP page implementation objects will invoke this method on all tag handlers,
	* but there may be multiple invocations on <code>doStartTag</code> and <code>doEndTag</code> in between.
	*/
	public void release() {
		super.release();
		super.setId(null);
		m_strName = null;
		m_strClass = null;
		m_isDeclare = true;
		m_aDataMap = null;
	}



	public void setId(String id) {
		super.setId(id);
	}

	public void setName(String name) {
		m_strName = TextHelper.evl( TextHelper.trim(name), null );
	}

	public void setClass(String klass) {
		m_strClass = klass;
	}

	public void setDeclare(boolean isDeclare) {
		m_isDeclare = isDeclare;
	}

	public void setDataMap(DataMap data) {
		m_aDataMap = data;
	}

	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{
		try
		{
			if (m_aDataMap == null) {
				if (super.getId() != null) {
					pageContext.removeAttribute( super.getId() );
				}
				return SKIP_BODY;
			}

			if (m_strClass == null) {
				m_strClass = "java.lang.Object";
			}

			Object aValue = null;
			if ( m_strClass.equals("java.lang.String") ) {
				aValue = m_aDataMap.getString(m_strName);
			}

			else if ( m_strClass.equals("dataMap") || m_strClass.equals("com.fliconz.fm.common.util.DataMap") ) {
				aValue = m_aDataMap.getDataMap(m_strName);
			}


			else if ( m_strClass.equals("java.lang.Integer") ) {
				aValue = m_aDataMap.asInteger(m_strName);
			}

			else if ( m_strClass.equals("java.lang.Boolean") ) {
				aValue = m_aDataMap.asBoolean(m_strName);
			}

			else if ( m_strClass.equals("java.lang.Long") ) {
				aValue = m_aDataMap.asLong(m_strName);
			}

			else if ( m_strClass.equals("java.lang.Float") ) {
				aValue = m_aDataMap.asFloat(m_strName);
			}

			else if ( m_strClass.equals("java.lang.Double") ) {
				aValue = m_aDataMap.asDouble(m_strName);
			}

			else if ( m_strClass.equals("java.util.Date") ) {
				aValue = m_aDataMap.getDate(m_strName);
			}

			else if ( m_strClass.equals("java.sql.Date") ) {
				aValue = m_aDataMap.getSQLDate(m_strName);
			}

			else if ( m_strClass.equals("java.sql.Time") ) {
				aValue = m_aDataMap.getTime(m_strName);
			}

			else if ( m_strClass.equals("java.sql.Timestamp") ) {
				aValue = m_aDataMap.getTimestamp(m_strName);
			}


			else if ( m_strClass.equals("java.lang.Byte") ) {
				aValue = m_aDataMap.asByte(m_strName);
			}

			else if ( m_strClass.equals("java.math.BigDecimal") ) {
				aValue = m_aDataMap.getBigDecimal(m_strName);
			}

			else if ( m_strClass.equals("java.math.BigInteger") ) {
				aValue = m_aDataMap.getBigInteger(m_strName);
			}

			else if ( m_strClass.equals("byte[]") ) {
				aValue = m_aDataMap.getBytes(m_strName);
			}

			else if ( m_strClass.equals("char[]") ) {
				aValue = m_aDataMap.getChars(m_strName);
			}

			else
			{
				aValue = m_aDataMap.getObject(m_strName);
			}


			if (aValue == null) {
				if (super.getId() != null) {
					pageContext.removeAttribute( super.getId() );
				}
			}
			else
			{
				if (super.getId() == null) {
					JspWriter jw = pageContext.getOut();
					jw.print(aValue);
				}
				else
				{
					pageContext.setAttribute(super.getId(), aValue);
				}
			}

			return SKIP_BODY;	//EVAL_BODY_BUFFERED;
		}
		catch (IOException e) {
			throw new JspTagException( e.getMessage() );
		}
	}

//	public int doAfterBody() throws JspTagException
//	{
//		try
//		{
//			bodyContent.writeOut(bodyContent.getEnclosingWriter());
//
//			return SKIP_BODY;
//		}
//		catch (IOException e)
//		{
//			//Debug.trace(e);
//			throw new JspTagException(e.getMessage());
//		}
//	}
}
