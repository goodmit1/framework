package com.fliconz.fm.common;

import org.apache.ibatis.session.SqlSession;

import com.fliconz.fw.runtime.dao.BaseDAO;

public class CommonDAO  extends BaseDAO  {

	public CommonDAO() throws Exception {
		super();

	}
	private SqlSession sqlSession;
	public SqlSession getSqlSession()
	{
		return sqlSession;
	}
	public void setSqlSession(SqlSession sqlSession){
		this.sqlSession = sqlSession;
	}

	public void clearCache(String nameSpace)
	{
		this.sqlSession.getConfiguration().getCache(nameSpace).clear();
	}

	private SqlSession batchSqlSession;
	public SqlSession getBatchSqlSession()
	{
		return batchSqlSession;
	}
	public void setBatchSqlSession(SqlSession batchSqlSession){
		this.batchSqlSession = batchSqlSession;
	}

	public void clearBatchCache(String nameSpace)
	{
		this.batchSqlSession.getConfiguration().getCache(nameSpace).clear();
	}

	@Override
	public SqlSession getSqlSession(boolean isBatch){
		if(isBatch){
			return getBatchSqlSession();
		} else {
			return getSqlSession();
		}
	}

}
