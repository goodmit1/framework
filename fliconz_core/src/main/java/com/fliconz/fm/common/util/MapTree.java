package com.fliconz.fm.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

public class MapTree{
	String idField;
	String parentField;
	Object rootVal;
	MapTreeNode root;
	Map<Object, MapTreeNode> idMap ;
	public MapTree(String idField, String parentField, Object rootVal)
	{
		this.idField = idField;
		this.parentField = parentField;
		this.rootVal = rootVal;
		idMap = new HashMap();
	}
	public Map get(Object key)
	{
		return (Map)idMap.get(key).getUserObject();
	}
	public MapTreeNode getNode(Object key)
	{
		return idMap.get(key);
	}
	public void addAll(List<Map> list) throws Exception
	{
		for(Map m : list)
		{
			MapTreeNode node = new MapTreeNode(m);
			idMap.put(m.get(idField), node);
		}
		root = idMap.get(rootVal);
		Collection<MapTreeNode> nodes = idMap.values();
		for( MapTreeNode  m: nodes)
		{
			Object parentVal = ((Map)m.getUserObject()).get(parentField);
			if(parentVal != null && !"".equals(parentVal) && !rootVal.equals(((Map)m.getUserObject()).get(idField)))
			{
				DefaultMutableTreeNode parentNode = idMap.get(parentVal);
				if(parentNode == null)
				{
					throw new Exception(parentVal + " Not found");
				}
				parentNode.add(m);
			}
		}
		
	}
	
	private static Map makeMap(String id, String parent, String content){
		Map result = new HashMap();
		result.put("id", id);
		result.put("parent", parent);
		result.put("content", content);
		return result;
	}
	public static void main(String[] args)
	{
		List list = new ArrayList();
		list.add(makeMap("0","","root"));
		list.add(makeMap("1","0","c1"));
		list.add(makeMap("2","0","c2"));
		list.add(makeMap("11","1","c11"));
		list.add(makeMap("12","1","c12"));
		list.add(makeMap("21","2","c21"));
		list.add(makeMap("22","2","c22"));
		list.add(makeMap("111","11","c111"));
		MapTree tree = new MapTree("id","parent", "0");
		try {
			tree.addAll(list);
			MapTreeNode node = tree.getNode("11");
			System.out.println(node.getParent().getMap());
			
			System.out.println( ((MapTreeNode)node.getParent().getChildAfter(node)).getMap());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
