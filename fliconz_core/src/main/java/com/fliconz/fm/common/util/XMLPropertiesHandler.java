/*
 * @(#)XMLPropertiesHandler.java
 */
package com.fliconz.fm.common.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;


public final class XMLPropertiesHandler extends SAXDefaultHandler
{
	private final static int MAX_ELEMENT_LEVEL = 2;
	private final static int PROP_BLOCK_COUNT = 10;
	private final static String TAB_TEXT = "\t";

	private final static String EN_PROPERTIES = "properties";
	private final static String EN_PROPERTY = "property";

	private final static String ATTR_NAME = "name";
	private final static String ATTR_TYPE = "type";
	private final static String ATTR_ENCODING = "encoding";

//	private final static String EN_PROPERTY_NAME = "name";
//	private final static String EN_PROPERTY_VALUE = "value";
//	private final static String EN_DESCRIPTION = "description";

	private final static String AMP_QUOT_TEXT = "&quot;";
	private final static String AMP_LT_TEXT = "&lt;";
	private final static String AMP_GT_TEXT = "&gt;";
	private final static String AMP_AMP_TEXT = "&amp;";
	private final static String AMP_APOS_TEXT = "&apos;";

	private final static char AMP_QUOT_CHAR = '\"';
	private final static char AMP_LT_CHAR = '<';
	private final static char AMP_GT_CHAR = '>';
	private final static char AMP_AMP_CHAR = '&';
	private final static char AMP_APOS_CHAR = '\'';

	private Properties m_pt = null;

	private LinkedList<String> m_lstElement = null;
	private StringBuilder m_sbText = null;

	private String m_strPropertiesName = null;
	private ServletContext m_servletContext = null;

	private String m_strPropName = null;
	private String m_strPropType = null;
	private String m_strPropEncoding = null;
	private String m_strPropValue = null;

	private int m_intDummy = 0;

//	private final Log theLogger = LogFactory.getLog(XMLPropertiesHandler.class);
	private final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	//##############################################################################################################################

	protected XMLPropertiesHandler(Properties pt) {
		super(System.out);
		m_pt = pt;
	}

	public String getPropertiesName()
	{
		return m_strPropertiesName;
	}
	public void setPropertiesName(String propName)
	{
		m_strPropertiesName = propName;
	}


	public ServletContext getServletContext()
	{
		return m_servletContext;
	}

	public void setServletContext(ServletContext context)
	{
		m_servletContext = context;
	}


	//##############################################################################################################################

	/**
	 *  Receive notification of the beginning of the document.
	 *
	 * @throws SAXException Any SAX exception, possibly wrapping another exception.
	 */
	public void startDocument() throws SAXException {
		super.startDocument();
		m_lstElement = new LinkedList<String>();
		m_sbText = new StringBuilder();
	}

	/**
	 * <pre>
	 * 개요 : Receive notification of the end of the document.
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @throws SAXException Any SAX exception, possibly wrapping another exception.
	 */
	public void endDocument() throws SAXException {
		super.endDocument();
		m_lstElement.clear(); m_lstElement = null;
		m_sbText.setLength(0); m_sbText = null;
		m_strPropName = null;
		m_strPropValue = null;
	}

	/**
	 * <pre>
	 * 개요 : Receive notification of the beginning of the document.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param namespaceURI The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not being performed.
	 * @param localName The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @param attrs The attributes attached to the element. If there are no attributes, it shall be an empty Attributes object.
	 *
	 * @throws SAXException Any SAX exception, possibly wrapping another exception.
	 */
	public void startElement(String namespaceURI, String localName, String qName, Attributes attrs) throws SAXException {
		super.startElement(namespaceURI, localName, qName, attrs);
//		try
//		{
			m_lstElement.add(qName);
			int intElementLevel = getElementLevel();
			switch (intElementLevel) {
				case 1:
					// properties
					if ( EN_PROPERTIES.equals(qName) ) {
						m_strPropertiesName = attrs.getValue(ATTR_NAME);
						m_intDummy = 0;
					}
					else {
						throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
					}
					break;

				case 2:
					// property
					if ( EN_PROPERTY.equals(qName) ) {
						m_strPropName = attrs.getValue(ATTR_NAME);
						m_strPropType = attrs.getValue(ATTR_TYPE);
						m_strPropEncoding = attrs.getValue(ATTR_ENCODING);
						//System.out.println("startElement ======> property name : ["+m_strPropName+"]");
						m_strPropValue = null;
					}
					else {
						throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
					}
					break;

//				case 3:
//					// name
//					if ( EN_PROPERTY_NAME.equals(qName) ) {
//						// no operation
//						m_intDummy = 0;
//					}
//					// value
//					else if ( EN_PROPERTY_VALUE.equals(qName) ) {
//						// no operation
//						m_intDummy = 0;
//					}
//					// description
//					else if ( EN_DESCRIPTION.equals(qName) ) {
//						// no operation
//						m_intDummy = 0;
//					}
//					else {
//						throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
//					}
//					break;

				default:
					throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
			}
//		}
//		finally
//		{
//
//		}
	}


	/**
	 * <pre>
	 * 개요 :  Receive notification of the end of an element.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param namespaceURI The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not being performed.
	 * @param localName The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
	 *
	 * @throws SAXException Any SAX exception, possibly wrapping another exception.
	 */
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		super.endElement(namespaceURI, localName, qName);
		try {
			int intElementLevel = getElementLevel();
			switch (intElementLevel) {
				case 1:
					// properties
					if ( EN_PROPERTIES.equals(qName) ) {
						// no operation
						m_intDummy = 0;
					}
					else {
						throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
					}
					break;

				case 2:
					// property
					if ( EN_PROPERTY.equals(qName) ) {
						m_strPropValue = m_sbText.toString().trim();
						//System.out.println("endElement ======> property value : ["+m_strPropName+"]=["+m_strPropValue+"]["+m_sbText+"]");
						if (m_strPropName != null && m_strPropValue != null)
						{
							if (m_strPropType != null && m_strPropType.equals("file") && m_pt instanceof XMLProperties && ((XMLProperties)m_pt).theMetaData != null)
							{
								String strFileText = loadFileText(m_strPropValue, m_strPropEncoding);
								((XMLProperties)m_pt).theMetaData.setString(new StringBuilder().append("#").append(m_strPropName).append(".path").toString(), m_strPropValue);
								((XMLProperties)m_pt).theMetaData.setString(new StringBuilder().append("#").append(m_strPropName).append(".encoding").toString(), m_strPropEncoding);

								if (strFileText != null)
								{
									m_strPropValue = strFileText;
								}
							}
							m_pt.setProperty(m_strPropName, m_strPropValue);
						}
						m_strPropName = null;
						m_strPropValue = null;
						m_strPropType = null;
						m_strPropEncoding = null;
					}
					else {
						throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
					}
					break;

//				case 3:
//					// name
//					if ( EN_PROPERTY_NAME.equals(qName) ) {
//						m_strPropName = m_sbText.toString().trim();
//						m_sbText.setLength(0);
//					}
//					// value
//					else if ( EN_PROPERTY_VALUE.equals(qName) ) {
//						m_strPropValue = m_sbText.toString().trim();
//						m_sbText.setLength(0);
//					}
//					// description
//					else if ( EN_DESCRIPTION.equals(qName) ) {
//						// no operation
//						m_intDummy = 0;
//					}
//					else {
//						throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
//					}
//					break;

				default:
					throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
			}

		}
		catch (IllegalStateException e) {
			throw e;
		}
		finally {
			m_sbText.setLength(0);
			m_lstElement.remove(qName);
		}
	}


	/**
	 * <pre>
	 * 개요 : Receive notification of character data inside an element.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param ch The characters.
	 * @param start The start position in the character array.
	 * @param len The number of characters to use from the character array.
	 *
	 * @throws SAXException Any SAX exception, possibly wrapping another exception.
	 */
	public void characters(char[] ch, int start, int len) throws SAXException {
		super.characters(ch, start, len) ;
		String strCurrentElementName = getElementName(0);
		int intElementLevel = getElementLevel();
		//System.out.println("characters ======> ["+intElementLevel+"]["+strCurrentElementName+"]["+new String(ch, start, len)+"]");
		if (intElementLevel <= MAX_ELEMENT_LEVEL) {
			// properties
			if ( EN_PROPERTIES.equals(strCurrentElementName) ) {
				return;
			}
			// property
			else if ( EN_PROPERTY.equals(strCurrentElementName) ) {
//				return;
				String strText = len == 0 ? null : new String(ch, start, len);
				if (strText != null) {
					m_sbText.append(strText);
					//System.out.println("characters ======> property value : ["+m_sbText.toString()+"]");
				}
			}
			else {
				throw new IllegalStateException( getNotSupportedElementMessage(strCurrentElementName, intElementLevel) );
			}
		}

//		if ( EN_PROPERTY.equals(strCurrentElementName) ) {
//			String strText = len == 0 ? null : new String(ch, start, len);
//			if (strText != null) {
//				m_sbText.append(strText);
//				System.out.println("**************** property value : ["+m_sbText.toString()+"]");
//			}
//		}

//		// name
//		if ( EN_PROPERTY_NAME.equals(strCurrentElementName) ) {
//			String strText = len == 0 ? null : new String(ch, start, len);
//			if (strText != null) {
//				m_sbText.append(strText);
//			}
//		}
//		// value
//		else if ( EN_PROPERTY_VALUE.equals(strCurrentElementName) ) {
//			String strText = len == 0 ? null : new String(ch, start, len);
//			if (strText != null) {
//				m_sbText.append(strText);
//			}
//		}
//		// description
//		else if ( EN_DESCRIPTION.equals(strCurrentElementName) ) {
//			// no operation
//						m_intDummy = 0;
//		}
//
//		else {
//			throw new IllegalStateException( getNotSupportedElementMessage(strCurrentElementName, intElementLevel) );
//		}
	}



	//##############################################################################################################################

	private String getElementName(int back) {
		return m_lstElement.isEmpty()
			? null
			: (String)m_lstElement.get(m_lstElement.size() - 1 - back);
	}

	private int getElementLevel() {
		return m_lstElement.size();
	}


	private String getNotSupportedElementMessage(String qName, int level) {
		return new StringBuilder()
			.append("[")
			.append(qName)
			.append("] element name is not supported at the element level [")
			.append(level)
			.append("].")
			.toString();
	}




	/**
	 * <pre>
	 *
	 * 개요 : 해당 PrintWriter에 XML Properties를 저장한다.
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param pw PrintWriter
	 * @param encoding encoding name
	 */
	//public void store(PrintWriter pw, String encoding, StringComparator comp) {
	public void store(PrintWriter pw, String encoding) {
		if (encoding == null) {
			pw.println("<?xml version=\"1.0\"?>");
		}
		else {
			pw.print("<?xml version=\"1.0\" encoding=\""); pw.print(encoding); pw.println("\"?>");
		}

		pw.println();
		pw.print("<"); pw.print(EN_PROPERTIES);
		if (m_strPropertiesName != null)
		{
			pw.print(" name=\"");
			pw.print(normalize(m_strPropertiesName));
			pw.print("\"");
		}
		pw.println(">");
		pw.println();


		if (m_pt.isEmpty() == false) {
			LinkedList<Object> lst = new LinkedList<Object>();
			lst.addAll(m_pt.keySet());
			//Collections.sort(lst, comp == null ? new LowerTextComparator() : comp);
			int intCount = 0;

			String strPropName = null;
			String strFilePath = null;
			String strFileEncoding = null;

			for ( Iterator<Object> it = lst.iterator(); it.hasNext(); )
			{
				strFilePath = null;
				strFileEncoding = null;
				strPropName = (String)it.next();

				if (m_pt instanceof XMLProperties && ((XMLProperties)m_pt).theMetaData != null)
				{
					strFilePath = ((XMLProperties)m_pt).theMetaData.getString( new StringBuilder().append("#").append(m_strPropName).append(".path").toString() );
					strFileEncoding = ((XMLProperties)m_pt).theMetaData.getString( new StringBuilder().append("#").append(m_strPropName).append(".encoding").toString() );
				}

				pw.print(TAB_TEXT);
				pw.print("<"); pw.print(EN_PROPERTY);
				pw.print(" ");
				pw.print(ATTR_NAME); pw.print("=\"");
				pw.print( normalize(strPropName) ); pw.print("\"");

				if (strFilePath == null)
				{
					pw.print(">");
					pw.print( normalize(m_pt.getProperty(strPropName)) );
				}
				else
				{
					pw.print(" ");
					pw.print(ATTR_TYPE); pw.print("=\"file\"");
					if (strFileEncoding != null)
					{
						pw.print(" ");
						pw.print(ATTR_ENCODING); pw.print("=\"");
						pw.print( normalize(strFileEncoding) );
						pw.print("\"");
					}
					pw.print(">");
					pw.print( normalize(m_pt.getProperty(strPropName)) );
				}

				pw.print("</"); pw.print(EN_PROPERTY); pw.print(">");
				pw.println();

				intCount++;
				if (it.hasNext() && intCount % PROP_BLOCK_COUNT == 0) {
					pw.println();
				}
			}
		}

		pw.println();
		pw.print("</"); pw.print(EN_PROPERTIES); pw.println(">");
	}





	private static String normalize(String value) {
		if (value == null) {
			return "";
		}

		char arr[] = value.toCharArray();
		StringBuilder sb = new StringBuilder();

		for(int i = 0; i < arr.length; i++) {
			switch(arr[i]) {
				case AMP_QUOT_CHAR:	//'\"'
					sb.append(AMP_QUOT_TEXT);
					break;

				case AMP_LT_CHAR:	//'<'
					sb.append(AMP_LT_TEXT);
					break;

				case AMP_GT_CHAR:	//'>'
					sb.append(AMP_GT_TEXT);
					break;

				case AMP_AMP_CHAR:	//'&'
					sb.append(AMP_AMP_TEXT);
					break;

				case AMP_APOS_CHAR: // '\''
					sb.append(AMP_APOS_TEXT);
					break;

				default:
					sb.append(arr[i]);
					break;
			}
		}

		return sb.toString();
	}


	private String loadFileText(String filePath, String encoding)
	{
		//System.out.println("###################################### AAA");
		if (getServletContext() == null)
		{
			System.out.println("000");
			return null;
		}

		StringWriter sw = null;
		BufferedReader buf = null;

		try
		{
			String strLine = null;
			buf = new BufferedReader( new InputStreamReader(getServletContext().getResourceAsStream(filePath), encoding == null ? "UTF-8" : encoding) );

			sw = new StringWriter();
			while ((strLine = buf.readLine()) != null)
			{
				//strLine = strLine.trim();
				if ( strLine.startsWith("@#") )
				{
					continue;
				}

				sw.write(strLine);
				sw.write("\n");
			}

			//System.out.println("###################################### BBB");
			//System.out.println(sw.toString());
			return sw.toString();
		}
		catch (Exception e)
		{
			//System.out.println("###################################### CCC");
			if (theLogger.isWarnEnabled()) theLogger.warn(e.getMessage(), e);
			return null;
		}
		finally
		{
			try
			{
				if (buf != null)
				{
					buf.close();
				}
			}
			catch (Exception e) {}

			try
			{
				if (sw != null)
				{
					sw.close();
				}
			}
			catch (Exception e) {}
		}
	}
}
