package com.fliconz.fm.common.core;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public abstract class AbstractExcelObject<T> {

	List<T> excelList = null;
	List<String[]> headLineList = null;
	String excelSheetName = null;
	String fileName = null;
	HttpServletRequest request = null;

	public AbstractExcelObject(List<T> excelList, String excelSheetName, String fileName, HttpServletRequest request) {
		this.excelList = excelList;
		this.excelSheetName = excelSheetName;
		this.fileName = fileName;
		this.request = request;
	}

	public AbstractExcelObject(List<T> excelList, String excelSheetName) {
		this(excelList, excelSheetName, null, null);
	}


	public AbstractExcelObject(List<T> excelList, String excelSheetName, String fileName) {
		this(excelList, excelSheetName, fileName, null);
	}


	public abstract List<String> getExcelTitle();

	public abstract List<String> getExcelValue(T vo, int num);

	public List<T> getExcelList() {
		return excelList;
	}



	public void setHeadLineList(List<String[]> headLines)
	{
		headLineList = headLines;
	}
	public List<String[]> getHeadLineList()
	{
		return headLineList;
	}


	public void setExcelList(List<T> excelList) {
		this.excelList = excelList;
	}

	public String getExcelSheetName() {
		return excelSheetName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public HttpServletRequest getRequest()
	{
		return this.request;
	}

//    public String nvl(String str) {
//        if (str == null) {
//            return "";
//        }
//        return skipNBSP(str);
//    }
//
//    public String nvl(String str, String defaultValue) {
//        if (str == null) {
//            return defaultValue;
//        }
//        return skipNBSP(str);
//    }
//
//    public String nvl(int str) {
//        return String.valueOf(str);
//    }
//
//    public String nvl(float str) {
//        return String.valueOf(str);
//    }
//
//    public String nvl(double str) {
//        return String.valueOf(str);
//    }
//
//    public String skipNBSP(String str) {
//        return str.replaceAll("&nbsp;", "");
//    }

}
