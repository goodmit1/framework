package com.fliconz.fm.common.core;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;


public class CaptchaServlet extends HttpServlet
{
	public final static String KEY_CAPTCHA_DATA = String.format("%s.data", CaptchaServlet.class.getName());

	public final static String KEY_CAPTCHA_ANSWER = "answer";
	public final static String KEY_CAPTCHA_TEXT_SOURCE = "text.source";
	public final static String KEY_CAPTCHA_TEXT_LENGTH = "text.length";
	public final static String KEY_CAPTCHA_IMAGE_WIDTH = "image.width";
	public final static String KEY_CAPTCHA_IMAGE_HEIGHT = "image.height";

	private final static int DEFAULT_CAPTCHA_LENGTH = 6;
	private final static int DEFAULT_IMAGE_HEIGHT = 60;
	private final static String DEFAULT_CAPTCHA_SOURCE = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";	//.toCharArray();

	private final static java.awt.Color[] CHAPCHAR_COLORS = new java.awt.Color[] {
			java.awt.Color.cyan,
			java.awt.Color.orange,
			java.awt.Color.green,
			java.awt.Color.gray,
			java.awt.Color.lightGray,
			java.awt.Color.magenta,
			java.awt.Color.pink,
			java.awt.Color.yellow
		};

	private final Logger theLogger = LoggerFactory.getLogger(CaptchaServlet.class);

	private char[] m_textSource = null;
	private int m_textLength = 0;
	private int m_imageWidth = 0;
	private int m_imageHeight = 0;



	@Override
	public void init() throws ServletException
	{
		super.init();
		m_textSource = TextHelper.evl(this.getInitParameter(CaptchaServlet.KEY_CAPTCHA_TEXT_SOURCE), DEFAULT_CAPTCHA_SOURCE).toCharArray();
		m_textLength = TextHelper.parseInt(this.getInitParameter(CaptchaServlet.KEY_CAPTCHA_TEXT_LENGTH), DEFAULT_CAPTCHA_LENGTH);
		m_imageWidth = TextHelper.parseInt(this.getInitParameter(CaptchaServlet.KEY_CAPTCHA_IMAGE_WIDTH), m_textLength * m_imageHeight);
		m_imageHeight = TextHelper.parseInt(this.getInitParameter(CaptchaServlet.KEY_CAPTCHA_IMAGE_HEIGHT), DEFAULT_IMAGE_HEIGHT);

//		if (theLogger.isDebugEnabled())
//		{
//			theLogger.debug("===> text.source : ["+new String(m_textSource)+"]");
//			theLogger.debug("===> text.length : ["+m_textLength+"]");
//			theLogger.debug("===> image.width : ["+m_imageWidth+"]");
//			theLogger.debug("===> image.height : ["+m_imageHeight+"]");
//		}
	}


	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			//if (theLogger.isDebugEnabled()) theLogger.debug("===> service");
			// 자동생성방지
			// CAPTCHA : Completely Automated Public Turing test to tell Computers and Humans Apart

//			nl.captcha.Captcha.Builder aCaptchaBuilder = new nl.captcha.Captcha.Builder(CAPTCHA_LENGTH * 30, 60);
//			aCaptchaBuilder.addText( new nl.captcha.text.producer.DefaultTextProducer(CAPTCHA_LENGTH, CAPTCHA_SOURCE) );
//			aCaptchaBuilder.gimp( new nl.captcha.gimpy.DropShadowGimpyRenderer() );
//			aCaptchaBuilder.addBackground();
//			aCaptchaBuilder.addNoise();
//			aCaptchaBuilder.addBorder();
//			nl.captcha.Captcha aCaptcha = aCaptchaBuilder.build();

			int textLength = TextHelper.parseInt(request.getParameter(CaptchaServlet.KEY_CAPTCHA_TEXT_LENGTH), m_textLength);
			int imageWidth = TextHelper.parseInt(request.getParameter(CaptchaServlet.KEY_CAPTCHA_IMAGE_WIDTH), m_imageWidth);
			int imageHeight = TextHelper.parseInt(request.getParameter(CaptchaServlet.KEY_CAPTCHA_IMAGE_HEIGHT), m_imageHeight);

			java.awt.Color toColor = CHAPCHAR_COLORS[ Calendar.getInstance().get(Calendar.SECOND) % CHAPCHAR_COLORS.length ];
			nl.captcha.CaptchaBean aCaptcha = new nl.captcha.CaptchaBean(imageWidth, imageHeight);
			aCaptcha.setGimpy( new nl.captcha.gimpy.DropShadowGimpyRenderer() );
			aCaptcha.setTxtProd( new nl.captcha.text.producer.DefaultTextProducer(textLength, m_textSource) );
			aCaptcha.setBgProd( new nl.captcha.backgrounds.GradiatedBackgroundProducer(java.awt.Color.white, toColor) );
//			aCaptcha.setBgProd( new nl.captcha.backgrounds.FlatColorBackgroundProducer() );
//			aCaptcha.setBgProd( new nl.captcha.backgrounds.SquigglesBackgroundProducer() );
//			aCaptcha.setBgProd( new nl.captcha.backgrounds.GradiatedBackgroundProducer() );
//			aCaptcha.setBgProd( new nl.captcha.backgrounds.TransparentBackgroundProducer() );

			aCaptcha.setNoiseProd( new nl.captcha.noise.CurvedLineNoiseProducer() );
			aCaptcha.setAddBorder(true);
			aCaptcha.build();

			java.awt.image.BufferedImage aCaptchaImage = aCaptcha.getImage();
			DataMap<String,Object> aCaptchaInfo = new DataMap<String,Object>();

			aCaptchaInfo.setString(CaptchaServlet.KEY_CAPTCHA_ANSWER, aCaptcha.getAnswer());
			aCaptchaInfo.setString(CaptchaServlet.KEY_CAPTCHA_TEXT_SOURCE, new String(m_textSource));
			aCaptchaInfo.setInt(CaptchaServlet.KEY_CAPTCHA_TEXT_LENGTH, textLength);
			aCaptchaInfo.setInt(CaptchaServlet.KEY_CAPTCHA_IMAGE_WIDTH, aCaptchaImage.getWidth());
			aCaptchaInfo.setInt(CaptchaServlet.KEY_CAPTCHA_IMAGE_HEIGHT, aCaptchaImage.getHeight());

			request.getSession(true).setAttribute(CaptchaServlet.KEY_CAPTCHA_DATA, aCaptchaInfo);
//			if (theLogger.isDebugEnabled())
//			{
//				theLogger.debug("===> captcha-info : ["+aCaptchaInfo+"]");
//			}

			response.resetBuffer();
			response.setHeader("Cache-Control", "no-store");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setContentType("image/jpeg");

			if (theLogger.isDebugEnabled())
			{
				theLogger.debug("===> captcha-info : ["+aCaptchaInfo+"]");
			}

			nl.captcha.servlet.CaptchaServletUtil.writeImage(response, aCaptchaImage);
		}
		finally
		{

		}

	}
}
