/*
 * @(#)GetAttributeTag.java
 */
package com.fliconz.fm.common.tag;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.fliconz.fm.common.util.TextHelper;

public class GetAttributeTag extends BodyTagSupport
{
	private String m_strName = null;
	@SuppressWarnings("unused")
	private String m_strClassName = null;
	@SuppressWarnings("unused")
	private boolean m_isDeclare = true;
	private String m_strScopeName = null;

	//##############################################################################################################################

	/**
	* Construct a <code>GetAttributeTag</code>.
	*/
	public GetAttributeTag() {
		super();
	}


	//##############################################################################################################################

	/**
	* Release a value attribute.
	*
	* Called on a Tag handler to release state.
	* The page compiler guarantees that JSP page implementation objects will invoke this method on all tag handlers,
	* but there may be multiple invocations on <code>doStartTag</code> and <code>doEndTag</code> in between.
	*/
	public void release() {
		super.release();
		super.setId(null);
		m_strName = null;
		m_strClassName = null;
		m_isDeclare = true;
		m_strScopeName = null;
	}



	public void setId(String id) {
		super.setId(id);
	}

	public void setName(String name) {
		m_strName = TextHelper.evl( TextHelper.trim(name), null );
	}

	public void setClassName(String className) {
		m_strClassName = className;
	}

	public void setDeclare(boolean isDeclare) {
		m_isDeclare = isDeclare;
	}

	public void setScope(String scopeName) {
		m_strScopeName = TextHelper.evl( TextHelper.trim(scopeName), null );
	}

	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{

		if (m_strScopeName == null) {
			m_strScopeName = "request";
		}

		if (m_strName == null) {
			throw new JspTagException("name must be not null");
		}

		Object aValue = null;
		if ( m_strScopeName.equals("request") ) {
			aValue = pageContext.getAttribute(m_strName, PageContext.REQUEST_SCOPE);
		}

		else if ( m_strScopeName.equals("session") ) {
			aValue = pageContext.getAttribute(m_strName, PageContext.SESSION_SCOPE);
		}

		else if ( m_strScopeName.equals("page") ) {
			aValue = pageContext.getAttribute(m_strName, PageContext.PAGE_SCOPE);
		}

		else if ( m_strScopeName.equals("application") ) {
			aValue = pageContext.getAttribute(m_strName, PageContext.APPLICATION_SCOPE);
		}

		if (aValue == null) {
			pageContext.removeAttribute( super.getId() );
		}
		else
		{
			pageContext.setAttribute(super.getId(), aValue);
		}

		return SKIP_BODY;	//EVAL_BODY_BUFFERED;
	}

//	public int doAfterBody() throws JspTagException
//	{
//		try
//		{
//			bodyContent.writeOut(bodyContent.getEnclosingWriter());
//
//			return SKIP_BODY;
//		}
//		catch (IOException e)
//		{
//			//Debug.trace(e);
//			throw new JspTagException(e.getMessage());
//		}
//	}
}
