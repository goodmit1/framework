package com.fliconz.fm.common.cache;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fliconz.fm.common.BaseService;
import com.fliconz.fm.common.Constants;
import com.fliconz.fm.common.vo.TreeNodeMap;
import com.fliconz.fw.runtime.dao.BaseDAO;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@Singleton
@Component("fmCacheManager")
public class CacheManager extends BaseService implements Serializable {

	private static final long serialVersionUID = 1L;

	private static CacheManager instance;

	private long menuUpdateTime = 0;



	public CacheManager() {
		instance = this;
	}

	@Override
	protected String getNameSpace() {
		return "com.fliconz.fm.common";
	}

	@Autowired
	@Qualifier("commonDAO")
	private BaseDAO dao;

	@PostConstruct
	public void init() throws Exception {
		super.setDAO(this.dao);
		this.loadMenu();
		this.initPublicCache();
	}

	public static long menuUpdateTime() {
		return instance.menuUpdateTime;
	}

	private Map<Integer, TreeNodeMap> allMenuMap;
	public static Map<Integer, TreeNodeMap> getAllMenuMap(boolean isReload) throws Exception {
		if(instance.allMenuMap == null || isReload) instance.loadMenu();
		return instance.allMenuMap;
	}
	public static Map<Integer, TreeNodeMap> getAllMenuMap() throws Exception{
		return getAllMenuMap(false);
	}

	/**
	 * @param menuService
	 * @throws Exception
	 * 1차 메뉴를 불러와서 DefaultMenuModel(RootMenu)에 addElement를 한다.
	 * 2차 메뉴를 불러와서 1차메뉴에 addElement한다.
	 */
	private TreeNodeMap rootMenuMap;
	private void loadMenu() throws Exception {
		if(allMenuMap == null) allMenuMap = new LinkedHashMap<Integer, TreeNodeMap>();
		synchronized (allMenuMap) {
			allMenuMap.clear();
			rootMenuMap = (TreeNodeMap)super.selectOneByQueryKey(Constants.ROOT_MENU_KEY, null);
			if(rootMenuMap != null){
				recursiveFindChildrenMenu(rootMenuMap);
				cleanNeedlessMenus(rootMenuMap);
				allMenuMap.put(rootMenuMap.getMenuId(), rootMenuMap);
				
				if(PropertyManager.getBoolean("support_multi_lang",false))
				{
					this.addMenuNL();
				}
			}
			
			
		}
		menuUpdateTime = System.currentTimeMillis();
	}

	public TreeNodeMap getRootMenuMap(){
		return rootMenuMap;
	}

	@SuppressWarnings("unchecked")
	public void recursiveFindChildrenMenu(TreeNodeMap rootMenuMap) throws Exception {
		List<TreeNodeMap> childrenMenu = instance.selectListByQueryKey(Constants.CHILDEREN_MENU_KEY, rootMenuMap);
		for(TreeNodeMap child: childrenMenu){
			allMenuMap.put(child.getMenuId(), child);
			recursiveFindChildrenMenu(child);
		}
	}

	public static void cleanNeedlessMenus(TreeNodeMap rootMenuMap){
		if(rootMenuMap != null && rootMenuMap.getElements() != null){
			for(TreeNodeMap child: rootMenuMap.getElements()){
				if(!child.containsKey("PRGM_ID") && child.isLeafNode()){
					child.put("RENDERED", "N");
				} else {
					cleanNeedlessMenus(child);
				}
			}
		}
	}

	public static TreeNodeMap getMenu(Integer menuId){
		return instance.allMenuMap.get(menuId);
	}

	public static boolean reloadMenu() {
		try {
			instance.loadMenu();
			return true;
		} catch(Exception e){
			return false;
		}
	}


	private HashMap<String, Object> publicCacheMap;
	public void initPublicCache() throws Exception {
		if(publicCacheMap == null) publicCacheMap = new HashMap<String, Object>();
		synchronized (publicCacheMap) {
			publicCacheMap.clear();
		}
	}

	public static void put(String key, Object value){
		if(instance.publicCacheMap == null) instance.publicCacheMap = new HashMap<String, Object>();
		synchronized (instance.publicCacheMap) {
			instance.publicCacheMap.put(key, value);
		}
	}
	public static void remove(String key ){
		if(instance.publicCacheMap == null) instance.publicCacheMap = new HashMap<String, Object>();
		synchronized (instance.publicCacheMap) {
			instance.publicCacheMap.remove(key );
		}
	}
	public static Object get(String key){
		if(instance.publicCacheMap == null) instance.publicCacheMap = new HashMap<String, Object>();
		synchronized (instance.publicCacheMap) {
			return instance.publicCacheMap.get(key);
		}
	}


	public void addMenuNL() throws Exception
	{
		Map param = new HashMap();
		param.put("TABLE_NM", "FM_MENU");
		List<Map> list = instance.selectListByQueryKey(Constants.LIST_NL_KEY, param);
		for(Map m : list)
		{
			 TreeNodeMap menu = allMenuMap.get(NumberUtil.getInt(m.get("ID")));
			 if(menu !=null && "ID".equals(m.get("FIELD_NM")))
			 {
				 menu.addNLNames((String)m.get("LANG_TYPE"), (String)m.get("NM"));
			 }
			 else if(menu !=null && "DESC".equals(m.get("FIELD_NM")))
			 {
				 menu.addNLDescs((String)m.get("LANG_TYPE"), (String)m.get("NM"));
			 }
		}
	}
	@Override
	public String getLang() {
		return null;
	}

}
