package com.fliconz.fm.common.snslogin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.fliconz.fm.util.RequestHelper;

public class KakaoLogin extends ISNSLogin {

	public KakaoLogin(HttpSession session, String clientId, String clientSecret, String redirectUrl) throws Exception {
		super(session, clientId, clientSecret, redirectUrl);
	}

	public KakaoLogin(HttpSession session, String clientId, String clientSecret, String adminKey, String redirectUrl) throws Exception {
		super(session, clientId, clientSecret, adminKey, redirectUrl);
	}

	@Override
	protected String getProfileUrl(String access_token) {
		return "https://kapi.kakao.com/v2/user/me";
	}

	@Override
	protected SNSUser getSNSUser(JSONObject json)
	{
		SNSUser user = new SNSUser();
		user.id = Integer.toString(json.getInt("id"));
		user.email = json.has("kaccount_email") ? json.getString("kaccount_email"): (json.has("kakao_account") ? (json.getJSONObject("kakao_account").has("email") ? json.getJSONObject("kakao_account").getString("email"): null): null);
		user.name = json.getJSONObject("properties").getString("nickname");
		user.hasEmail = json.has("kakao_account") ? json.getJSONObject("kakao_account").getBoolean("has_email"): user.email!=null;
		user.accessToken = json.getString("access_token");
		return user;
	}

	//코드받기
	@Override
	protected String getFirstRequestUrl(String state) {
		return "https://kauth.kakao.com/oauth/authorize?client_id=" + this.clientId + "&redirect_uri=" + this.redirectUrl + "&response_type=code&state=" + state;
	}

	//코드를 받은 후, 토큰 받기
	protected String getAccessTokenUrl(String state, String code ){
		return "https://kauth.kakao.com/oauth/token?client_id="+ this.clientId + "&client_secret=" + clientSecret+ "&grant_type=authorization_code&state=" + state+ "&code=" + code;
	}

	protected String getAccessTokenUrl(String code ){
		return "https://kauth.kakao.com/oauth/token?client_id="+ this.clientId + "&client_secret=" + clientSecret+ "&grant_type=authorization_code&code=" + code;
	}

	public String getScope(){
		return "profile,account_email";
	}

	protected String getDynamicAgreeRequestUrl(String state, String scope) {
		return "https://kauth.kakao.com/oauth/authorize?client_id=" + this.clientId + "&redirect_uri=" + this.redirectUrl + "&response_type=code&scope=" + scope;
	}

	protected String getEmailUrl(String user_id){
		return "https://kapi.kakao.com/v2/user/me?target_id_type=user_id&target_id=" + user_id + "&property_keys=[\"kakao_account.email\"]";
	}

	@Override
	protected String getUnlinkUrl() {
		return "https://kapi.kakao.com/v1/user/unlink";
	}

	@Override
	public boolean unlink(String sns_id, String access_token) throws Exception {
		Map<String, String> header = new HashMap<String, String>();
		header.put("Authorization", "Bearer " + access_token);
		JSONObject json = RequestHelper.postJSON(this.getUnlinkUrl(), header, new HashMap<String, String>());
		try {
			String id = String.valueOf(json.get("id"));
			return sns_id.equals(id);
		} catch(Exception e){
			return false;
		}
	}

	@Override
	protected String getAccessTokeInfoUrl() {
		return "https://kapi.kakao.com/v1/user/access_token_info";
	}

	@Override
	protected String getRefreshTokeUrl() {
		return "https://kauth.kakao.com/oauth/token";
	}

	public JSONObject refreshToken(String access_token, String refresh_token) throws Exception {
		String accessTokeInfoUrl = this.getAccessTokeInfoUrl();
		Map<String, String> header = new HashMap<String, String>();
		header.put("Authorization", "Bearer " + access_token);
		boolean isRefreshRequired = false;
		long expiresInMillis = 9999999999l;
		JSONObject json = RequestHelper.getJSON(accessTokeInfoUrl, header);
		try {
			expiresInMillis = isRefreshRequired ? 0: (json.get("expiresInMillis") == null ? json.getLong("expires_in"): json.getLong("expiresInMillis"));
		} catch(Exception e){
			isRefreshRequired = true;
		}

		//1분 미만일 때 리프레시
		if(isRefreshRequired || expiresInMillis <= (1000 * 60)){
			String refreshTokenUrl = this.getRefreshTokeUrl();
			Map<String, String> data = new HashMap<String, String>();
			data.put("grant_type", "refresh_token");
			data.put("client_id", clientId);
			data.put("refresh_token", refresh_token);
			data.put("client_secret", clientSecret);
			json = RequestHelper.postJSON(refreshTokenUrl, new HashMap<String, String>(), data);
			return json;
		} else {
			return null;
		}

	}
}
