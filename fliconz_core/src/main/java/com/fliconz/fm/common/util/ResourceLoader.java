/*
 * (@)# ResourceLoader
 */
package com.fliconz.fm.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class ResourceLoader extends SAXDefaultHandler implements ServletContextListener
{

	/*
		<listener>
			<listener-class>com.fliconz.fm.common.util.ResourceLoader</listener-class>
		</listener>
		<context-param>
			<param-name>toc.resource-config</param-name>
			<param-value>/WEB-INF/config-resource/resource-config.xml</param-value>
		</context-param>



	<?xml version="1.0" encoding="UTF-8"?>

	<properties name="test">
		<property name="abc">가나다</property>
	</properties>

	*/
	//private final static String KEY_CONFIG_CONTAINER = new StringBuilder().append(XMLResourceLoader.class.getName()).append(".container.config").toString();
	//private final static String KEY_RESOURCE_CONTAINER = new StringBuilder().append(XMLResourceLoader.class.getName()).append(".container.resource").toString();

	private final Logger m_logger = LoggerFactory.getLogger(ResourceLoader.class);
	private final static int MAX_ELEMENT_LEVEL = 2;
	private final static String EN_RESOURCE_CONFIG = "resource-config";
	private final static String EN_RESOURCE = "resource";

	private final static String KEY_NAME = "name";
	private final static String KEY_LOCALE = "locale";
	private final static String KEY_PATH = "path";
	private final static String KEY_NAME_TRIM = "nameTrim";
	private final static String KEY_VALUE_TRIM = "valueTrim";

	private final static String KEY_REFERENCE_NAME = "reference.name";
	private final static String KEY_REFERENCE_LOCALE = "reference.locale";
	private final static String DEFAULT_ENCODING = "UTF-8";

	private LinkedList<String> m_lstElement = null;
	private StringBuilder m_sbText = null;
	private XProperties m_pt = null;
	private String m_encoding = null;

	// <String>			: resource name
	// <Hashtable>		: resource configurations
	//    <String>		: resource locale
	//    <XProperties>	: resource config properties
	//       resource-name
	//       resource-locale
	//       resource-path
	//       [reference-resource-name]
	//       [reference-resource-locale]
	private static Hashtable<String,Hashtable<String,XProperties>> sm_htConfig = new Hashtable<String,Hashtable<String,XProperties>>();

	// <String>			: resource name
	// <Hashtable>		: resource configurations
	//    <String>		: resource locale
	//    <XProperties>	: resource properties
	private static Hashtable<String,Hashtable<String,XProperties>> sm_htResource = new Hashtable<String,Hashtable<String,XProperties>>();

	private static Hashtable<String,Object> sm_htAttr = new Hashtable<String,Object>();

	public ResourceLoader()
	{
		super(System.out);
	}



	protected static Object setAttribute(String name, Object value)
	{
		synchronized(sm_htAttr)
		{
			if (value == null)
			{
				return sm_htAttr.remove(name);
			}

			return sm_htAttr.put(name, value);
		}
	}

	protected static Object getAttribute(String name)
	{
		synchronized(sm_htAttr)
		{
			return sm_htAttr.get(name);
		}
	}



	//##############################################################################################################################

	@Override
	public void contextInitialized(ServletContextEvent sce)
	{
		ServletContext ctx = sce.getServletContext();
		this.load(ctx);
	}

	public String getEncoding()
	{
		return m_encoding == null ? DEFAULT_ENCODING : m_encoding;
	}

	public void reload(ServletContext ctx)
	{
		this.load(ctx);
	}

	private void load(ServletContext ctx)
	{
		InputStream in = null;
		try
		{
			String configLocation= ctx.getInitParameter("toc.resource-config");
			if (m_logger.isDebugEnabled()) m_logger.debug("===> resource-config : [{}]", configLocation);
			if (configLocation == null)
			{
				return;
			}


			in = ctx.getResourceAsStream(configLocation);
			SAXParserFactory aSAXParserFactory = SAXParserFactory.newInstance();
			aSAXParserFactory.setValidating(false);

			SAXParser aSAXParser = aSAXParserFactory.newSAXParser();
			InputSource aInputSource = new InputSource(in);

			sm_htConfig.clear();
			sm_htResource.clear();
			aSAXParser.parse(aInputSource, this);

			if (m_logger.isWarnEnabled()) m_logger.warn("sm_htConfig : {}", sm_htConfig);

			List<XProperties> refConfigList = new LinkedList<XProperties>();
			for (Iterator<String> it = sm_htConfig.keySet().iterator(); it.hasNext();)
			{
				String resourceName = it.next();
				Hashtable<String,XProperties> htConfig_perLocale = sm_htConfig.get(resourceName);
				for (Iterator<String> itLanguage = htConfig_perLocale.keySet().iterator(); itLanguage.hasNext();)
				{
					String locale = itLanguage.next();
					XProperties ptConfig = htConfig_perLocale.get(locale);
					String path = ptConfig.getString(KEY_PATH);

					if (m_logger.isWarnEnabled()) m_logger.warn("resource config : {}", ptConfig);

					// reference resource name/locale
					if (ptConfig.containsKey(KEY_REFERENCE_NAME))
					{
						refConfigList.add(ptConfig);
						continue;
					}

					XMLProperties ptResource = this.loadXMLProperties(ctx, getEncoding(), resourceName, locale, path,
							ptConfig.getBoolean(KEY_NAME_TRIM), ptConfig.getBoolean(KEY_VALUE_TRIM));
					if (ptResource.getName() == null)
					{
						ptResource.setName(resourceName);
					}
					else
					{
						if (ptResource.getName() != null && ptResource.getName().equals(resourceName) == false)
						{
							ptResource.setName(resourceName);
							if (m_logger.isWarnEnabled())
							{
								m_logger.warn("resource name ({}) is different from the xml properties name ({}).", resourceName, resourceName);
							}
						}
					}

					Hashtable<String,XProperties> htResource_perLocale = sm_htResource.get(resourceName);
					if (htResource_perLocale == null)
					{
						htResource_perLocale = new Hashtable<String,XProperties>();
						sm_htResource.put(resourceName, htResource_perLocale);
					}
					htResource_perLocale.put(locale, ptResource);
				}
			}

			for (Iterator<XProperties> it = refConfigList.iterator(); it.hasNext();)
			{
				XProperties ptConfig = it.next();

				String resName = ptConfig.getString(KEY_NAME);
				String resLocale = ptConfig.getString(KEY_LOCALE);
				String refResName = ptConfig.getString(KEY_REFERENCE_NAME);
				String refResLocale = ptConfig.getString(KEY_REFERENCE_LOCALE);

				Hashtable<String,XProperties> htResource_perLocale = sm_htResource.get(refResName);
				if (htResource_perLocale == null)
				{
					throw new IllegalStateException("["+refResName+"] reference-resource for ["+resName+"] resource is not found.");
				}
//				else
//				{
//					if (m_logger.isDebugEnabled())
//					{
//						m_logger.debug("[{}] reference-resource for [{}] resource", refResName, resName);
//					}
//				}

				XProperties ptResource = htResource_perLocale.get(refResLocale);
				if (ptResource == null)
				{
					throw new IllegalStateException("["+refResName+(refResLocale.equals("") ? "" : "@"+refResLocale)+"] reference-resource for ["
							+resName+(resLocale.equals("") ? "" : "@"+resLocale)+"] resource is not found.");
				}
//				else
//				{
//					if (m_logger.isDebugEnabled())
//					{
//						m_logger.debug("[{}] reference-resource for [{}] resource"
//								, refResName+(refResLocale.equals("") ? "" : "@"+refResLocale)
//								, resName+(resLocale.equals("") ? "" : "@"+resLocale));
//					}
//				}
			}
			//ctx.setAttribute(KEY_CONFIG_CONTAINER, sm_htConfig);
			//ctx.setAttribute(KEY_RESOURCE_CONTAINER, sm_htResource);

			ctx.setAttribute(this.getClass().getName(), this);


			if (m_logger.isDebugEnabled())
			{
				//m_logger.debug("=====> config : {}", sm_htConfig);
				//m_logger.debug("=====> resource : {}", sm_htResource);
				for (Iterator<String> itResourceName = sm_htConfig.keySet().iterator(); itResourceName.hasNext();)
				{
					String resourceName = itResourceName.next();
					Hashtable<String,XProperties> htConfig_perLocale = sm_htConfig.get(resourceName);
					for (Iterator<String> itLocale = htConfig_perLocale.keySet().iterator(); itLocale.hasNext();)
					{
						String locale = itLocale.next();
						XProperties ptConfig = htConfig_perLocale.get(locale);
						//m_logger.debug("=====> ptConfig : {}", ptConfig);

						if (ptConfig.containsKey(KEY_REFERENCE_NAME))
						{
							Hashtable<String,XProperties> htResource_perLocale = sm_htResource.get(ptConfig.get(KEY_REFERENCE_NAME));
							m_logger.debug("resource [{}] -> [{}] :\n{}"
									, new Object[]{
												resourceName+(ptConfig.getString(KEY_LOCALE).equals("") ? "" : "@"+ptConfig.get(KEY_LOCALE))
											,	ptConfig.get(KEY_REFERENCE_NAME)+(ptConfig.getString(KEY_REFERENCE_LOCALE).equals("") ? "" : "@"+ptConfig.get(KEY_REFERENCE_LOCALE))
											,	htResource_perLocale.get(ptConfig.get(KEY_REFERENCE_LOCALE))
									}
								);
						}
						else
						{
							Hashtable<String,XProperties> htResource_perLocale = sm_htResource.get(resourceName);
							m_logger.debug("resource [{}] :\n {}"
									, resourceName+(ptConfig.getString(KEY_LOCALE).equals("") ? "" : "@"+ptConfig.get(KEY_LOCALE))
									, htResource_perLocale.get(ptConfig.get(KEY_LOCALE))
								);
						}

						//m_logger.debug("=====> resource : ({}/{})\n{}", new Object[]{locale, name, XMLResourceLoader.getXProperties(locale, name)});
					}
				}
				//m_logger.debug("=====> resource : {}", sm_htResource);
			}




//			for (Iterator<String> itResourceName = ResourceHelper.getResourceNames(); itResourceName.hasNext();)
//			{
//				String resourceName = itResourceName.next();
//				Map<String,XProperties> htConfig_perLocale = ResourceHelper.getResourceConfig(resourceName);
//				for (Iterator<String> itLocale = htConfig_perLocale.keySet().iterator(); itLocale.hasNext();)
//				{
//					String locale = itLocale.next();
//					XProperties ptConfig = htConfig_perLocale.get(locale);
//
//					System.out.println("<b>"+resourceName+"</b> : "+ptConfig);
//					XProperties ptResource = ResourceHelper.getXProperties(locale, resourceName);
//					ptResource.store(System.out);
//				}
//			}

			if (m_logger.isDebugEnabled()) m_logger.debug("=====> {} resource is loaded.", sm_htConfig.keySet().size());
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		catch (ParserConfigurationException e)
		{
			throw new RuntimeException(e);
		}
		catch (SAXException e)
		{
			throw new RuntimeException(e);
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (Exception e) {}
		}
	}


	private XMLProperties loadXMLProperties(ServletContext ctx, String encoding, String name, String locale, String path,
			boolean nameTrim, boolean valueTrim) throws IOException
	{
		InputStream in = null;

		try
		{
			in = ctx.getResourceAsStream(path);
			if (in == null)
			{
				if (m_logger.isWarnEnabled()) m_logger.warn("resource is not found. path={}", path);
			}
			XMLProperties ptResource = new XMLProperties();
			ptResource.setServletContext(ctx);

			ptResource.load(new java.io.InputStreamReader(in, encoding));

//			if (m_logger.isDebugEnabled())
//			{
//				m_logger.debug("properties : name=[{}] / locale=[{}] / path=[{}]", name, locale, path);
//				ptResource.store(System.out);
//			}

			if (nameTrim || valueTrim)
			{
				for (Enumeration<String> en = ptResource.propertyNames(); en.hasMoreElements();)
				{
					String pName = en.nextElement();
					String pValue = ptResource.getString(pName);
					if (nameTrim == true && valueTrim == true)
					{
						ptResource.remove(pName);
						ptResource.setString(pName.trim(), pValue.trim());
					}
					else if (nameTrim == true && valueTrim == false)
					{
						ptResource.remove(pName);
						ptResource.setString(pName.trim(), pValue);
					}
					else if (nameTrim == false && valueTrim == true)
					{
						//ptResource.remove(pName);
						ptResource.setString(pName, pValue.trim());
					}
				}
			}

			return ptResource;
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (Exception e) {}
		}

	}


	@Override
	public void contextDestroyed(ServletContextEvent sce)
	{
		ServletContext ctx = sce.getServletContext();
		//ctx.removeAttribute(KEY_CONFIG_CONTAINER);
		//ctx.removeAttribute(KEY_RESOURCE_CONTAINER);
	}


	//##############################################################################################################################

//	protected static Hashtable<String,XProperties> getResource(String resourceName)
//	{
//		Hashtable<String,XProperties> htConfig_perLocale = sm_htConfig.get(resourceName);
//		return sm_htResource.get(resourceName);
//	}

	protected static XProperties getXProperties(String locale, String resourceName)
	{
		Hashtable<String,XProperties> htConfig_perLocale = sm_htConfig.get(resourceName);
		if (htConfig_perLocale == null)
		{
			return null;
		}

		String strLocale = locale == null ? "" : locale;
		XProperties ptConfig_perLocale = htConfig_perLocale.get(strLocale);
		Hashtable<String,XProperties> htReource_perLocale = null;
		XProperties ptResource = null;

		if (ptConfig_perLocale == null)
		{
			// default locale에 대한 resource를 얻는다.
			if (strLocale.equals("") == false && sm_htResource.containsKey(resourceName))
			{
				htReource_perLocale = sm_htResource.get(resourceName);
				ptResource = htReource_perLocale.get("");
			}
		}
		else
		{
			if ( ptConfig_perLocale.containsKey(KEY_REFERENCE_NAME) )
			{
				htReource_perLocale = sm_htResource.get(ptConfig_perLocale.getString(KEY_REFERENCE_NAME));
				ptResource = htReource_perLocale.get(ptConfig_perLocale.getString(KEY_REFERENCE_LOCALE));
			}
			else
			{
				htReource_perLocale = sm_htResource.get(resourceName);
				ptResource = htReource_perLocale.get(strLocale);
			}
		}

		return ptResource;
	}

	protected static XProperties getXProperties(String resourceName)
	{
		return ResourceLoader.getXProperties(null, resourceName);
	}

	protected static Iterator<String> getResourceNames()
	{
		return sm_htConfig.keySet().iterator();
	}

	protected static Map<String,XProperties> getResourceConfig(String resourceName)
	{
		// resource config per locale
		return sm_htConfig.get(resourceName);
	}

	protected static boolean isRefResource(String locale, String resourceName)
	{
		Map<String,XProperties> htConfig = sm_htConfig.get(resourceName);
		if (htConfig == null)
		{
			throw new NoSuchElementException("["+resourceName+"] resource is not found.");
		}

		XProperties ptConfig_perLocale = htConfig.get(locale);
		if (ptConfig_perLocale == null)
		{
			throw new NoSuchElementException("["+locale+"] resource is not found at the ["+resourceName+"] resource.");
		}
		return ptConfig_perLocale.containsKey(KEY_REFERENCE_NAME);
	}

	//##############################################################################################################################

	@Override
	public void startDocument() throws SAXException
	{
		super.startDocument();
		m_pt = new XProperties();
		m_lstElement = new LinkedList<String>();
		m_sbText = new StringBuilder();
	}

	@Override
	public void endDocument() throws SAXException
	{
		m_pt.clear(); m_pt = null;
		m_lstElement.clear(); m_lstElement = null;
		m_sbText.setLength(0); m_sbText = null;
		super.endDocument();
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes attrs) throws SAXException
	{
		m_lstElement.add(qName);
		int intElementLevel = getElementLevel();
		switch (intElementLevel)
		{
			case 1:
				if ( EN_RESOURCE_CONFIG.equals(qName) )
				{
					m_encoding = attrs.getValue("encoding");
				}
				else
				{
					throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
				}
				break;

			case 2:
				if ( EN_RESOURCE.equals(qName) ) {
					m_pt.setString(KEY_NAME, attrs.getValue(KEY_NAME));
//					m_pt.setString(KEY_ENCODING, attrs.getValue(KEY_ENCODING));
					m_pt.setString(KEY_LOCALE, attrs.getValue(KEY_LOCALE) == null ? "" : attrs.getValue(KEY_LOCALE));
					m_pt.setBoolean(KEY_NAME_TRIM, attrs.getValue(KEY_NAME_TRIM) == null ? false : TextHelper.parseBoolean(attrs.getValue(KEY_NAME_TRIM), false));
					m_pt.setBoolean(KEY_VALUE_TRIM, attrs.getValue(KEY_VALUE_TRIM) == null ? false : TextHelper.parseBoolean(attrs.getValue(KEY_VALUE_TRIM), false));
				}
				else {
					throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
				}
				break;

			default:
				throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
		}
	}


	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException
	{
		try
		{
			int intElementLevel = getElementLevel();
			switch (intElementLevel) {
				case 1:
					if ( EN_RESOURCE_CONFIG.equals(qName) )
					{
						// no operation
					}
					else
					{
						throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
					}
					break;

				case 2:
					if ( EN_RESOURCE.equals(qName) )
					{
						m_pt.setString(KEY_PATH, m_sbText.toString().trim());
						//System.out.println("======> resource path : ["+m_arr[2]+"]");
						if (m_pt.containsKey(KEY_NAME) && m_pt.containsKey(KEY_PATH))
						{

							XProperties ptConfig = new XProperties();
							ptConfig.putAll(m_pt);
							m_pt.clear();

							if (ptConfig.getString(KEY_PATH).startsWith("#"))
							{
								String[] arr = TextHelper.split(ptConfig.getString(KEY_PATH).trim().substring(1), "@");
								ptConfig.setString(KEY_REFERENCE_NAME, arr[0]);
								ptConfig.setString(KEY_REFERENCE_LOCALE, arr.length >= 2 ? arr[1] : "");
							}

							Hashtable<String,XProperties> htConfig_perLocale = sm_htConfig.get(ptConfig.getString(KEY_NAME));
							if (htConfig_perLocale == null)
							{
								htConfig_perLocale = new Hashtable<String,XProperties>();
								sm_htConfig.put(ptConfig.getString(KEY_NAME), htConfig_perLocale);
							}
							htConfig_perLocale.put(ptConfig.getString(KEY_LOCALE, ""), ptConfig);
						}
						m_sbText.setLength(0);
					}
					else
					{
						throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
					}
					break;

				default:
					throw new IllegalStateException( getNotSupportedElementMessage(qName, intElementLevel) );
			}

		}
		catch (IllegalStateException e) {
			throw e;
		}
		finally {
			m_lstElement.remove(qName);
		}
	}


	@Override
	public void characters(char[] ch, int start, int len) throws SAXException
	{
		String strCurrentElementName = getElementName(0);
		int intElementLevel = getElementLevel();

		if (intElementLevel <= MAX_ELEMENT_LEVEL)
		{
			// resource-config
			if ( EN_RESOURCE_CONFIG.equals(strCurrentElementName) )
			{
				return;
			}
			// resource
			else if ( EN_RESOURCE.equals(strCurrentElementName) )
			{
				String strText = len == 0 ? null : new String(ch, start, len);
				if (strText != null)
				{
					m_sbText.append(strText);
				}
			}
			else
			{
				throw new IllegalStateException( getNotSupportedElementMessage(strCurrentElementName, intElementLevel) );
			}
		}
	}



	//##############################################################################################################################

	private String getElementName(int back) {
		return m_lstElement.isEmpty()
			? null
			: (String)m_lstElement.get(m_lstElement.size() - 1 - back);
	}

	private int getElementLevel() {
		return m_lstElement.size();
	}


	private String getNotSupportedElementMessage(String qName, int level) {
		return new StringBuilder()
			.append("[")
			.append(qName)
			.append("] element name is not supported at the element level [")
			.append(level)
			.append("].")
			.toString();
	}

}
