package com.fliconz.fm.common.core;

import java.lang.annotation.Annotation;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

public class RequestParamMapArgumentResolver implements WebArgumentResolver
{
	public Object resolveArgument(MethodParameter param, NativeWebRequest request) throws Exception
	{
		Annotation[] paramAnns = param.getParameterAnnotations();
		Class<?> paramType = param.getParameterType();
		if (Map.class.isAssignableFrom(paramType) == false)
		{
			throw new IllegalStateException(
					new StringBuilder()
						.append("Missing parameter '")
						.append(paramType.getName())
						.append("' of type [")
						.append(Map.class.getName())
						.append("]")
						.toString()
					);
		}

		for (Annotation paramAnn : paramAnns)
		{
			if (RequestParamMap.class.isInstance(paramAnn))
			{
				RequestParamMap aRequestParamMap = (RequestParamMap)paramAnn;
				HttpServletRequest nRequest = (HttpServletRequest) request.getNativeRequest();

				Map reqMap = (Map)paramType.newInstance();
				reqMap.putAll(nRequest.getParameterMap());

				return reqMap;
			}
		}

		return WebArgumentResolver.UNRESOLVED;
	}
}
