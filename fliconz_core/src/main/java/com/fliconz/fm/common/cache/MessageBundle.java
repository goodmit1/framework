package com.fliconz.fm.common.cache;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javax.inject.Singleton;

import org.springframework.stereotype.Component;

import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.UTF8Control;

@Singleton
@Component("messageBundle")
public class MessageBundle {

	public static final Control UTF8_CONTROL = new UTF8Control();
	public static ResourceBundle getBundle(String baseName, String language){
		ResourceBundle resourceBundle = ResourceBundle.getBundle(baseName, new Locale(language), UTF8_CONTROL);
		return resourceBundle;
	}
	public static ResourceBundle getBundle(String language){
		ResourceBundle resourceBundle = ResourceBundle.getBundle(PropertyManager.getString("message.default.namespace"), new Locale(language), UTF8_CONTROL);
		return resourceBundle;
	}
	public static ResourceBundle getBundle(Class<?> clazz, String language){
		ResourceBundle resourceBundle = ResourceBundle.getBundle(clazz.getPackage().getName() + ".message", new Locale(language), UTF8_CONTROL);
		return resourceBundle;
	}

	public static String getString(String key, String language  ){
		try
		{
			return getBundle(language).getString(key);
		}
		catch(java.util.MissingResourceException e)
		{
			return  ResourceBundle.getBundle("com.fliconz.fm.common.message", new Locale(language), UTF8_CONTROL).getString(key);
		}
	}

	public static String getString( String key, String replace,String language){
		return getString(key, language).replace("{0}", replace);
	}

	public static String getString(String key, String[] replaces,String language){
		String text = getString(key, language);
		for(int i = 0 ; i < replaces.length ; i++){
			String replace = replaces[i];
			text = text.replace("{" + i + "}", replace);
		}
		return text;
	}

}
