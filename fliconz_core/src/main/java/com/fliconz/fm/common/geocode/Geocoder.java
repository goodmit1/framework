package com.fliconz.fm.common.geocode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fliconz.fm.common.util.DataMap;

public abstract class Geocoder
{
	public final static String KEY_ENCODING = "encoding";
	public final static String KEY_REQUEST_PAGE = "req_page";
	public final static String KEY_REQUEST_COUNT = "req_count";

	public final static String KEY_RESPONSE_CODE = "res_code";
	public final static String KEY_RESPONSE_MESSAGE = "res_msg";

	public final static String KEY_RESPONSE_COUNT = "res_count";
	public final static String KEY_TOTAL_COUNT = "total_count";

	public final static String KEY_LOCATION_LIST = "location.list";
	
	//2018.11.19:서울역 기본좌표
	public final static float DEFAULT_LATITUDE = 37.57352368818343f;
	public final static float DEFAULT_LONGITUDE = 126.97698695087433f;


	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 좌표 --> 주소 (geocode), 주소 --> 좌표 (reverse geocode) 변환여부
	 * @param toCoordinate true : 좌표 --> 주소, false : 주소 --> 좌표
	 * @return
	 */
	public boolean canConvert(boolean isGeocode)
	{
		return true;
	}

	/**
	 * 페이징 기능 지원여부
	 *
	 * @param isGeocode true : 주소 --> 좌표 (geocode), false : 좌표 --> 주소 (reverse geocode)
	 * @return
	 */
	public boolean canPaging(boolean isGeocode)
	{
		return false;
	}


	// 주소 --> 좌표
	/**
	 * 주소를 좌표로 변환한다.
	 *
	 * @param addressInfo 주소정보 (address)
	 * @return
	 * @throws Exception
	 */
	public abstract DataMap<String,Object> geocode(GeoLocation addr) throws GeocodeException;


	// 좌표 --> 주소
	/**
	 * 좌표를 주소로 변환한다.
	 *
	 * @param coordinateInfo 좌표정보 (latitude, longitude, altitude) 위도/경도 정보는 반드시 포함
	 * @return
	 */
	public abstract DataMap<String,Object> reverseGeocode(GeoLocation coord) throws GeocodeException;
}
