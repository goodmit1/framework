package com.fliconz.fm.common.scheduler;

import java.util.Iterator;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class TestJobBean extends QuartzJobBean
{
//	private ApplicationContext m_appContext;

	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

//	public void setApplicationContext(ApplicationContext appContext){
//		//System.out.println("BackupQuartzService ApplicationContext setting" + new Date());
//		m_appContext = appContext;
//	}

	@Override
	protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException
	{
		if (theLogger.isDebugEnabled())
		{
			theLogger.debug("==> {} : class=[{}], ThreadGroup=[{}], ThreadName=[{}], ThreadId=[{}]",
					ctx.getJobDetail().getDescription()
					,	getClass().getName()
					,	Thread.currentThread().getThreadGroup()
					,	Thread.currentThread().getName()
					,	Thread.currentThread().getId()
			);

			if (theLogger.isDebugEnabled()) theLogger.debug("===> job info : [{}], [{}]"
					, ctx.getJobDetail().getKey().getGroup()
					, ctx.getJobDetail().getKey().getName());

//			theLogger.debug("==> {} : class=[{}], ThreadGroup=[{}], ThreadName=[{}], ThreadId=[{}]",
//					new Object[]{
//					ctx.getJobDetail().getDescription()
//					,	getClass().getName()
//					,	Thread.currentThread().getThreadGroup()
//					,	Thread.currentThread().getName()
//					,	Thread.currentThread().getId()
//			});

			JobDataMap aJobDataMap = ctx.getJobDetail().getJobDataMap();
			for (Iterator<String> it = aJobDataMap.keySet().iterator(); it.hasNext();)
			{
				Object aName = it.next();
				Object aValue = aJobDataMap.get(aName);
				theLogger.debug("==> [{}] = [{}]", aName, aValue);
				//theLogger.debug("==> [{}]/[{}] = [{}]/[{}]", aName, aName.getClass().getName(), aValue, aValue.getClass().getName());
			}
		}
	}
}

