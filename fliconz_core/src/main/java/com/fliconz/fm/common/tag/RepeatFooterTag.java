/*
 * @(#)RepeatFooterTag.java
 */
package com.fliconz.fm.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.TagSupport;


public class RepeatFooterTag extends BodyTagSupport
{
	private boolean m_isSkipEmpty = false;

	//##############################################################################################################################

	public RepeatFooterTag() {
		super();
	}


	//##############################################################################################################################

	public void release() {
		super.release();
		m_isSkipEmpty = false;
	}

	public void setSkipEmpty(boolean skipEmpty) {
		m_isSkipEmpty = skipEmpty;
	}

	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{
		RepeatTag aRepeatTag = (RepeatTag)TagSupport.findAncestorWithClass(this, RepeatTag.class);
		if ( aRepeatTag == null) {
			throw new JspTagException("parent repeat tag is not found.");
		}

		if ( !aRepeatTag.isRepeated() ) {
			throw new JspTagException("not repeated.");
		}
		if ( m_isSkipEmpty && aRepeatTag.isEmpty() ) {
			return SKIP_BODY;
		}

		return EVAL_BODY_BUFFERED;
	}


	public int doAfterBody() throws JspTagException
	{
		try
		{

			bodyContent.writeOut(getPreviousOut());
			bodyContent.clearBody();

			return SKIP_BODY;
		}
		catch(IOException e) {
			throw new JspTagException( e.getMessage() );
		}
	}
}
