package com.fliconz.fm.common.core;

public interface KGApplicationEventDelegate<KGApplicationEvent> {

	public void onDelegatedApplicationEvent(KGApplicationEvent appEvent);

}
