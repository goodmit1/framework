package com.fliconz.fm.common.geocode;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;
//import net.sf.json.JSONObject;
//import net.sf.json.JSONSerializer;
import java.util.LinkedList;
import java.util.List;

// address --> location
// http://kr.open.gugi.yahoo.com/document/poisearch.php

// location --> address
// http://kr.open.gugi.yahoo.com/document/geocooder.php

public class YahooKoreaGeocoder extends Geocoder
{
	private String m_consumerKey = null;
	private boolean m_isReverseAddress = false;

	public YahooKoreaGeocoder(String apiKey, boolean isReverseAddress)
	{
		super();
		m_consumerKey = apiKey;
		m_isReverseAddress = isReverseAddress;
	}

	public YahooKoreaGeocoder(String apiKey)
	{
		this(apiKey, true);
	}

	public String getAPIKey()
	{
		return m_consumerKey;
	}
	public boolean isReverseAddress()
	{
		return m_isReverseAddress;
	}


	private String makeAddress(org.json.JSONObject item, boolean isGeocode) throws JSONException
	{
		StringBuilder sb = new StringBuilder();
		if (isReverseAddress())
		{
			sb.append(item.getString("country")).append(" ");
			sb.append(item.getString("state")).append(" ");
			sb.append(item.getString("county")).append(" ");
			if (isGeocode) sb.append(item.getString("city")).append(" ");
			sb.append(item.getString(isGeocode ? "street" : "town"));
		}
		else
		{
			sb.append(item.getString(isGeocode ? "street" : "town"));
			if (isGeocode) sb.append(item.getString("city")).append(" ");
			sb.append(item.getString("county")).append(" ");
			sb.append(item.getString("state")).append(" ");
			sb.append(item.getString("country")).append(" ");
		}

		return sb.toString().trim();
	}




	@Override
	public boolean canPaging(boolean isGeocode)
	{
		return false;
	}

	@Override
	public DataMap<String, Object> geocode(GeoLocation addr) throws GeocodeException
	{
		// 주소 --> 좌표
		HttpClient aHttpClient = null;

		try
		{
			DataMap<String,Object> locAttr = addr.getAttributes();
			int rowCount = locAttr.getInt(KEY_REQUEST_COUNT, 100);
			String encoding = locAttr.getString(KEY_ENCODING,"UTF-8");

			StringBuilder sb = new StringBuilder()
				.append("http://kr.open.gugi.yahoo.com/service/poi.php")
				.append("?appid=").append(TextHelper.nvl(getAPIKey(), ""))
				.append("&q=").append(URLEncoder.encode(TextHelper.nvl(addr.getAddress(), ""), encoding))
				.append("&output=json")
				.append("&results=").append(rowCount);

			aHttpClient = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(sb.toString());

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);
//System.out.println(responseString);

			org.json.JSONObject response = new org.json.JSONObject( responseString );
			org.json.JSONObject resultSet = response.getJSONObject("ResultSet");
			String responseCode = TextHelper.nvl(resultSet.getJSONObject("head").getString("Error"), "");
			String responseMessage = TextHelper.nvl(resultSet.getJSONObject("head").getString("ErrorMessage"), "");
			int count = TextHelper.parseInt(resultSet.getJSONObject("head").getString("Found"), 0);

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (responseCode.equals("0") == false || count <= 0)
			{
				resultData.setString(KEY_RESPONSE_CODE, responseCode);
				resultData.setString(KEY_RESPONSE_MESSAGE, responseMessage);
				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, 0);
				resultData.setInt(KEY_TOTAL_COUNT, 0);
				return resultData;
			}


			List<GeoLocation> aList = new LinkedList<GeoLocation>();
			org.json.JSONArray arr = resultSet.getJSONObject("locations").getJSONArray("item");
			for (int i=0; i<arr.length(); i++)
			{
				org.json.JSONObject item = arr.getJSONObject(i);
				if (item.getString("latitude").equals("null") || item.getString("longitude").equals("null"))
				{
					//System.out.println(item);
					continue;
				}

				GeoLocation aLocation = new GeoLocation();
				String address = makeAddress(item, true);

				aLocation.setAddress(address);
				aLocation.setLatitude((float)item.getDouble("latitude"));
				aLocation.setLongitude((float)item.getDouble("longitude"));
				aList.add(aLocation);
			}
//            for (Iterator<JSONObject> it = resultSet.getJSONObject("locations").getJSONArray("item").iterator(); it.hasNext();)
//            {
//            	JSONObject item = it.next();
//            	if (item.getString("latitude").equals("null") || item.getString("longitude").equals("null"))
//            	{
//            		//System.out.println(item);
//            		continue;
//            	}
//
//            	GeoLocation aLocation = new GeoLocation();
//            	String address = makeAddress(item, true);
//
//            	aLocation.setAddress(address);
//            	aLocation.setLatitude((float)item.getDouble("latitude"));
//            	aLocation.setLongitude((float)item.getDouble("longitude"));
//            	aList.add(aLocation);
//            }

			if (aList.isEmpty())
			{
				resultData.setString(KEY_RESPONSE_CODE, "no-data");
				resultData.setString(KEY_RESPONSE_MESSAGE, "no data");

				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, 0);
				resultData.setInt(KEY_TOTAL_COUNT, 0);
			}
			else
			{
				resultData.setString(KEY_RESPONSE_CODE, "ok");
				resultData.setString(KEY_RESPONSE_MESSAGE, responseMessage);
				resultData.setObject(KEY_LOCATION_LIST, aList);

				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, aList.size());
				resultData.setInt(KEY_TOTAL_COUNT, aList.size());
			}

			return resultData;
		}
		catch (UnsupportedEncodingException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}

	@Override
	public DataMap<String, Object> reverseGeocode(GeoLocation coord) throws GeocodeException
	{
		// 좌표 --> 주소
		HttpClient aHttpClient = null;

		try
		{
			DataMap<String,Object> locAttr = coord.getAttributes();
			int rowCount = locAttr.getInt(KEY_REQUEST_COUNT, 100);
			String encoding = locAttr.getString(KEY_ENCODING,"UTF-8");

			StringBuilder sb = new StringBuilder()
				.append("http://kr.open.gugi.yahoo.com/service/rgc.php")
				.append("?appid=").append(TextHelper.nvl(getAPIKey(), ""))
				.append("&latitude=").append(coord.getLatitude())
				.append("&longitude=").append(coord.getLongitude())
				.append("&output=json");

			aHttpClient = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(sb.toString());

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);

			org.json.JSONObject response = new org.json.JSONObject( responseString );
			org.json.JSONObject resultSet = response.getJSONObject("ResultSet");
			String responseCode = TextHelper.nvl(resultSet.getJSONObject("head").getString("Error"), "");
			String responseMessage = TextHelper.nvl(resultSet.getJSONObject("head").getString("ErrorMessage"), "");
			int count = TextHelper.parseInt(resultSet.getJSONObject("head").getString("Found"), 0);

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (responseCode.equals("0") == false || count <= 0)
			{
				resultData.setString(KEY_RESPONSE_CODE, responseCode);
				resultData.setString(KEY_RESPONSE_MESSAGE, responseMessage);
				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, 0);
				resultData.setInt(KEY_TOTAL_COUNT, 0);
				return resultData;
			}


			List<GeoLocation> aList = new LinkedList<GeoLocation>();

			org.json.JSONObject item = resultSet.getJSONObject("location");
			GeoLocation aLocation = new GeoLocation();
			String address = makeAddress(item, false);

			aLocation.setAddress(address);
			aLocation.setLatitude(coord.getLatitude());
			aLocation.setLongitude(coord.getLongitude());
			aList.add(aLocation);


			resultData.setString(KEY_RESPONSE_CODE, "ok");
			resultData.setString(KEY_RESPONSE_MESSAGE, responseMessage);
			resultData.setObject(KEY_LOCATION_LIST, aList);

			resultData.setInt(KEY_REQUEST_PAGE, 1);
			resultData.setInt(KEY_RESPONSE_COUNT, aList.size());
			resultData.setInt(KEY_TOTAL_COUNT, count);

			return resultData;
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}



/*
WEB_INF_HOME=/Volumes/data/projects/SmartJM/development/web/smartjm/WebContent/WEB-INF

THIS_CLASSPATH=$WEB_INF_HOME/classes:$WEB_INF_HOME/lib/httpclient-4.0.3.jar:$WEB_INF_HOME/lib/httpcore-4.0.1.jar:$WEB_INF_HOME/lib/commons-logging-1.1.1.jar:$WEB_INF_HOME/lib/json-lib-2.3-jdk15.jar:$WEB_INF_HOME/lib/commons-lang-2.4.jar:$WEB_INF_HOME/lib/ezmorph-1.0.6.jar:$WEB_INF_HOME/lib/commons-collections-3.2.1.jar:$WEB_INF_HOME/lib/commons-beanutils-1.8.0.jar

java -cp $THIS_CLASSPATH com.fliconz.fm.common.geocode.YahooKoreaGeocoder
*/
	public static void main(String[] args) throws Exception
	{
		YahooKoreaGeocoder geocoder = new YahooKoreaGeocoder("dj0yJmk9dnl5MFozQWdZdmEwJmQ9WVdrOVUwNU5PVWRTTkhFbWNHbzlPVGsxT0Rrd01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTkw");
		GeoLocation aLocation = new GeoLocation();
		DataMap<String,Object> resultData = null;



		System.out.println("=========================");
		System.out.println(geocoder.getClass().getName());

		// http://kr.open.gugi.yahoo.com/service/poi.php?appid=dj0yJmk9dnl5MFozQWdZdmEwJmQ9WVdrOVUwNU5PVWRTTkhFbWNHbzlPVGsxT0Rrd01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTkw&q=효자동&output=json&results=100
		aLocation.setAddress("서울 용산구 보광동 210-8 1층");
		resultData = geocoder.geocode(aLocation);
		System.out.println("=========================");
		System.out.println("geocode : "+ resultData.toString());

		// http://kr.open.gugi.yahoo.com/service/rgc.php?appid=dj0yJmk9dnl5MFozQWdZdmEwJmQ9WVdrOVUwNU5PVWRTTkhFbWNHbzlPVGsxT0Rrd01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTkw&latitude=37.87015110&longitude=127.73687180&output=json
		aLocation.setLatitude(37.87015110F);
		aLocation.setLongitude(127.73687180F);
		resultData = geocoder.reverseGeocode(aLocation);

		System.out.println();
		System.out.println("=========================");
		System.out.println("reverse geocode : "+ resultData.toString());
	}

}
