package com.fliconz.fm.common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;

public class CookieHelper {
	//private static final String COOKIE_PATH = "/";
	private final static int COOKIE_MAX_AGE = 7*24*3600;
	/**
	 * log4j to log.
	 */
//	private static final Log logger = LogFactory.getLog(CookieHelper.class);
	private final static Logger logger = LoggerFactory.getLogger(CookieHelper.class);

	public static Cookie getCookie(HttpServletRequest request, String cookieName)
	{
		if (cookieName == null)
		{
			return null;
		}

		Cookie [] cookies = request.getCookies( );

		if ( cookies != null && cookies.length > 0 ){
			for (Cookie cookie : cookies) {

				if ( cookieName.equals( cookie.getName() )) {
					return cookie;
				}
			}
		}

		return null;
	}

	public static String getCookieValue(HttpServletRequest request, String cookieName) {
		Cookie cookie = CookieHelper.getCookie(request, cookieName);

		if ( cookie != null ){
			String value = cookie.getValue();
			try {
				return SecurityHelper.decrypt(value);
			} catch (Exception e) {
				logger.error("쿠키 로드 중에 에러가 발생하였습니다.", e);
			}
		}

		return null;
	}

	public static void setCookieValue(HttpServletResponse response, String key, String userId) {
		String encryptUserId = null;
		try {
			encryptUserId = SecurityHelper.encrypt(userId);
			Cookie cookie = new Cookie(key, encryptUserId);
			//cookie.setPath(COOKIE_PATH);
			cookie.setMaxAge(COOKIE_MAX_AGE);
			response.addCookie(cookie);
		} catch (Exception e) {
			logger.error("쿠키 생성 중에 에러가 발생하였습니다.", e);
		}
	}
}
