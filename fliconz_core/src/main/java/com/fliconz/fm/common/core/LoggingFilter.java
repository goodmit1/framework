package com.fliconz.fm.common.core;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.security.UserVO;

public class LoggingFilter implements Filter {
	private Set<String> m_URIList = null;
	private Set<String> m_ignoreParamList = null;
	private Map<Pattern,String> m_seperateSave = null;
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());
	public LoggingFilter()
	{
	 
		m_URIList = new HashSet<String>();
		m_URIList.add(".*\\.jsp");
		m_ignoreParamList = new HashSet<String>();
		m_ignoreParamList.add("mem_pass");
		m_ignoreParamList.add("PASSWORD");
	}
	private Set makeSet(String list)
	{
		Set<String> result = new HashSet();
		if (TextHelper.isNotEmpty(list))
		{
			List<String> aDebugIgnoreRequesURIList = TextHelper.splitList(list, "\n");
			
			for (String uri : aDebugIgnoreRequesURIList)
			{
				uri = uri.trim();
				if (TextHelper.isEmpty(uri) == false)
				{
					result.add(uri);
				}
			}
		}
		return result;
	}
	int paramSize = 5;
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String debugIgnoreURIList = filterConfig.getInitParameter("URIList");
		if(debugIgnoreURIList != null) m_URIList = makeSet(debugIgnoreURIList.trim());
		
		String sepTarget = filterConfig.getInitParameter("saveSeperatePattern");
		if(sepTarget != null) {
			Set<String> targetSet = makeSet(sepTarget.trim());
			m_seperateSave = new HashMap();
			for(String s: targetSet) {
				int pos = s.lastIndexOf("|");
				 
				m_seperateSave.put(  Pattern.compile(s.substring(0,pos)), s.substring(pos+1));
			}
		}
		
		String ignoreParam = filterConfig.getInitParameter("ignoreParamList");
		if(ignoreParam != null) m_ignoreParamList  = makeSet(ignoreParam.trim());
		String size = filterConfig.getInitParameter("paramSize");
		if(size != null)
		{
			try
			{
				paramSize = Integer.parseInt(size);
			}
			catch(Exception ignore)
			{
				
			}
		}
		
	}
	private boolean isTarget(String uri)
	{
		for(String l:m_URIList)
		{
			if(uri.matches(l))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void doFilter(ServletRequest _request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)_request;
		String requestURI = request.getRequestURI();
		if (request.getContextPath().equals("") == false && requestURI.indexOf(request.getContextPath()) == 0)
		{
			requestURI = requestURI.substring(request.getContextPath().length());
		}
		// debug ignore
		if (isTarget(requestURI))
		{
			String user = "guest";
			UserVO userVO = UserVO.getUser();
			String menuName = null;
			try
			{
				
				user = userVO.getUserId() + ":" + userVO.getLoginId() + ":" + userVO.getUserName();
				menuName = userVO.getUserMenuManager().getSelectedMenuName();
			}
			catch(Exception ignore)
			{}
			
			String text = 	new StringBuilder()
					.append("start==============================================================================================").append("\n")
					.append("menu : [{6}]"				).append("\n")
					.append("sessionId : [{0}]"				).append("\n")
					.append("user : [{1}]"				).append("\n")
					.append("requestURI : [{2}]"				).append("\n")
					.append("requestHeader : [{3}]"				).append("\n")
					.append("remoteAddr : [{4}]"			).append("\n")
					.append("query : [{5}]"					).append("\n")
					.append("end==============================================================================================")
					.toString();
			String result = MessageFormat.format(text,  
				  new Object[]{
						 request.getSession().getId()
						,user
						,request.getRequestURI()
						,this.getRequestHeaderMap(request)
						,request.getRemoteAddr()
						,TextHelper.nvl(getParam(request), "")
						,TextHelper.nvl(menuName, "")
						
					});
				theLogger.debug(result);
				saveSeperateFile(request.getRequestURI(), result);
		}
		chain.doFilter(request, response);
	}
	protected void saveSeperateFile(String uri, String text) {
		if(m_seperateSave != null) {
			Set<Pattern> keys = m_seperateSave.keySet();
			for(Pattern k:keys) {
		        Matcher matcher =   k.matcher(uri);
		        if(matcher.find()) {
		        	LoggerFactory.getLogger(m_seperateSave.get(k)).info(text);
		        	return;
		        }
			}
		}
	}
	private String getParam(HttpServletRequest request)
	{
		//if(!TextHelper.isEmpty(request.getQueryString())) return request.getQueryString(); 
		Enumeration<String> names = request.getParameterNames();
		StringBuffer sb = new StringBuffer();
		int idx=0;
		while(names.hasMoreElements())
		{
			String name = names.nextElement();
			if(m_ignoreParamList != null && m_ignoreParamList.contains(name)) continue;
			if(idx>0) sb.append("&");
			sb.append(name).append("=").append(request.getParameter(name));
			idx++;
			if(idx==paramSize) break;
		}
		return sb.toString();
	}

	private Map<String,String> getRequestHeaderMap(HttpServletRequest request)
	{
		Map<String,String> mm = new HashMap<String,String>();
		for (Enumeration<String> en = request.getHeaderNames(); en.hasMoreElements();)
		{
			String name = en.nextElement();
			mm.put(name, request.getHeader(name));
		}

		return mm;
	}
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
