package com.fliconz.fm.common.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fliconz.fm.exception.MemoryRefreshException;
import com.fliconz.fm.util.LogHelper;
import com.fliconz.fm.util.RequestHelper;
import com.fliconz.fw.runtime.util.PropertyManager;

@Singleton
@Component("memoryRefreshManager")
public class MemoryRefreshManager implements IMemoryRefresh, Serializable {

	private static final long serialVersionUID = 1L;

	protected List<String> clientList = null;
	protected String[] kubuns = null;

	public MemoryRefreshManager() {
		clientList = new ArrayList<String>();
		kubuns = PropertyManager.getStringArray("sync.kubuns", new String[]{"prop","menu"});
	}

	protected void setKubuns(String[] kubuns){
		this.kubuns = kubuns;
	}

	@PostConstruct
	private void init() throws Exception{
		clientList.clear();
		if(PropertyManager.getString("sync.servers") != null){
			String[] clientlist = PropertyManager.getString("sync.servers").split(",");
			for(String c: clientlist){
				this.registClient(c);
			}
		}
	}

	@Override
	public void registClient(String client) throws MemoryRefreshException{
		for(String c: clientList){
			if(c.equals(client)){
				LogHelper.println("Client ["+client+"] is already registered.");
				return;
			}
		}
		clientList.add(client);
		LogHelper.println("Client ["+client+"] is successfully registered.", true);
	}

	@Override
	public JSONObject refresh(String kubun){
		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		for(String client: clientList){
			jsonArray.put(refreshClient(client, kubun));
		}
		json.put("code", "000");
		json.put("msg", "메모리 갱신 요청 완료");
		json.put("results", jsonArray);
		return json;
	}

	@Override
	public JSONObject refreshClient(String client, String kubun){
		return RequestHelper.sendRequest(client + "/api/cache/refresh/" + kubun, RequestMethod.GET, "utf-8");
	}
}
