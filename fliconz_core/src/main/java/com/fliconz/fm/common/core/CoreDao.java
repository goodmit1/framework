package com.fliconz.fm.common.core;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fliconz.fm.common.util.DataKey;
import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.PageScroll;

public class CoreDao extends SqlSessionDaoSupport
{
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	public CoreDao()
	{
		super();
	}

	protected int getStartRow(PageScroll pScroll)
	{
		return pScroll.getStartRow(pScroll.getViewPage()) - 1;
	}



	/**
	 * execute insert query
	 *
	 * @param query
	 * @param param
	 * @return affected count
	 */
	public int insert(String query, DataMap<String,Object> param)
	{
		return super.getSqlSession().insert(query, param);
	}


	/**
	 * execute update query
	 *
	 * @param query
	 * @param param
	 * @return affected count
	 */
	public int update(String query, DataMap<String,Object> param)
	{
		return super.getSqlSession().update(query, param);
	}


	/**
	 * execute delete query
	 *
	 * @param query
	 * @param param
	 * @return affected count
	 */
	public int delete(String query, DataMap<String,Object> param)
	{
		return super.getSqlSession().update(query, param);
	}



	/**
	 * execute select query and get page row
	 *
	 * @param countQuery
	 * @param listQuery
	 * @param param
	 * @return multi row data
	 */
	public DataMap<String,Object> selectPageList(String countQuery, String listQuery, DataMap<String,Object> param)
	{
		Integer totalCount = (Integer)super.getSqlSession().selectOne(countQuery, param);
		if (totalCount.intValue() <= 0)
		{
			return null;
		}

		int pageNo = param.getInt(DataKey.KEY_PAGE_NO, 1);
		int rowCount = param.getInt(DataKey.KEY_ROW_COUNT, PageScroll.DEFAULT_ROW_COUNT);

		boolean isReverse = false;	//param.getBoolean(DataKey.KEY_IS_REVERSE, false);
		PageScroll aPageScroll = new PageScroll(pageNo, totalCount.intValue(), rowCount, isReverse);

		//int startRow = aPageScroll.getStartRow(aPageScroll.getViewPage());
		param.setInt(DataKey.KEY_PAGE_NO, aPageScroll.getViewPage());
		param.setInt(DataKey.KEY_START_ROW, getStartRow(aPageScroll));
		param.setInt(DataKey.KEY_ROW_COUNT, rowCount);

		List<DataMap<String,Object>> aList = super.getSqlSession().selectList(listQuery, param);
		if (aList == null || aList.isEmpty())
		{
			return null;
		}

		DataMap<String,Object> resultMap = new DataMap<String,Object>();
		resultMap.setObject(aPageScroll.getClass().getName(), aPageScroll);
		resultMap.setInt(DataKey.KEY_TOTAL_COUNT, totalCount.intValue());
		resultMap.put(DataKey.KEY_PAGE_LIST, aList);

		return resultMap;
	}




	/**
	 * execute select query and get multi row
	 *
	 * @param query
	 * @param param
	 * @return multi row data
	 */
	public List<DataMap<String,Object>> selectList(String query, DataMap<String,Object> param)
	{
		List<DataMap<String,Object>> aList = super.getSqlSession().selectList(query, param);
		if (aList == null || aList.isEmpty())
		{
			return null;
		}

		return aList;
	}


	/**
	 * execute select query and get multi row
	 *
	 * @param query
	 * @param param
	 * @return multi row
	 */
	public List<Object> selectObjectList(String query, DataMap<String,Object> param)
	{
		List<Object> aList = super.getSqlSession().selectList(query, param);
		if (aList == null || aList.isEmpty())
		{
			return null;
		}

		return aList;
	}



	/**
	 * execute select query and get single row
	 *
	 * @param query
	 * @param param
	 * @return single row data
	 */
	public DataMap<String,Object> selectOne(String query, DataMap<String,Object> param)
	{
		DataMap<String,Object> aData = super.getSqlSession().selectOne(query, param);
		if (aData == null || aData.isEmpty())
		{
			return null;
		}

		return aData;
	}


	/**
	 * execute select query and get single row column data
	 *
	 * @param query
	 * @param param
	 * @return single row column data
	 */
	public Object selectObject(String query, DataMap<String,Object> param)
	{
		return super.getSqlSession().selectOne(query, param);
	}


	/**
	 * execute select query and get single row int value
	 *
	 * @param query
	 * @param param
	 * @return single row column data
	 */
	public int selectInt(String query, DataMap<String,Object> param)
	{
		return super.getSqlSession().selectOne(query, param);
	}
}
