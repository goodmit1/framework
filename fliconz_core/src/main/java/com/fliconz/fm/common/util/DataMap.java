package com.fliconz.fm.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Iterator;


public class DataMap<K, V> extends HashMap<K,V>
{

	/**
	 *
	 */
	private static final long serialVersionUID = 4963265815128651012L;

//	public final static int KEY_CONVERT_NONE = 0;
//	public final static int KEY_CONVERT_UPPER = 1;
//	public final static int KEY_CONVERT_LOWER = -1;
//
//	private int m_keyConvertMode = KEY_CONVERT_NONE;

	public DataMap() {
		super();
	}

	public DataMap(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	public DataMap(int initialCapacity) {
		super(initialCapacity);
	}

	public DataMap(Map<? extends K, ? extends V> m) {
		super();
		if (m != null)
		{
			super.putAll(m);
		}
	}

	public static DataMap toDataMap(Map mm)
	{
		if (mm == null)
		{
			return null;
		}

		if (mm instanceof DataMap)
		{
			return (DataMap)mm;
		}

		return new DataMap(mm);
	}

	private static List<String> ms_number2StringKeyList = new LinkedList<String>();
	static
	{
		BufferedReader buf = null;
		try
		{
			buf = new BufferedReader( new InputStreamReader( DataMap.class.getResourceAsStream("/META-INF/dataMap-number2stringKey.txt")) );
			String strLine = null;
			while ( (strLine = buf.readLine()) != null )
			{
				strLine = strLine.trim();
				if (TextHelper.isEmpty(strLine) || strLine.startsWith("#"))
				{
					continue;
				}

				ms_number2StringKeyList.add(strLine);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new IllegalStateException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (buf != null)
				{
					buf.close();
				}
			}
			catch (Throwable e) {}
		}
	}

//	public final int getKeyConvertMode()
//	{
//		return m_keyConvertMode;
//	}
//
//	public final void setKeyConvertMode(int keyConvertMode)
//	{
//		if (keyConvertMode == KEY_CONVERT_NONE)
//		{
//			m_keyConvertMode = KEY_CONVERT_NONE;
//		}
//		else if (keyConvertMode > KEY_CONVERT_NONE)
//		{
//			m_keyConvertMode = KEY_CONVERT_UPPER;
//		}
//		else
//		{
//			m_keyConvertMode = KEY_CONVERT_LOWER;
//		}
//	}

//	private String convertKey(String key)
//	{
//		switch (m_keyConvertMode)
//		{
//			case KEY_CONVERT_UPPER:
//				return key.toUpperCase();
//
//			case KEY_CONVERT_LOWER:
//				return key.toLowerCase();
//
//			case KEY_CONVERT_NONE:
//			default:
//				return key;
//		}
//	}

//	public ServletRequest getServletRequest()
//	{
//		ServletRequest aServletRequest = (ServletRequest)super.get(ServletRequest.class.getName());
//		if (aServletRequest != null)
//		{
//			return aServletRequest;
//		}
//
//		return this.getHttpServletRequest();
//	}
//
//	public HttpServletRequest getHttpServletRequest()
//	{
//		return (HttpServletRequest)super.get(HttpServletRequest.class.getName());
//	}
//
//	public ServletResponse getServletResponse()
//	{
//		ServletResponse aServletResponse = (ServletResponse)super.get(ServletResponse.class.getName());
//		if (aServletResponse != null)
//		{
//			return aServletResponse;
//		}
//
//		return this.getHttpServletResponse();
//	}
//
//	public HttpServletResponse getHttpServletResponse()
//	{
//		return (HttpServletResponse)super.get(HttpServletResponse.class.getName());
//	}





	public Enumeration<K> keys() {
		return new XEnumeration(super.keySet());
	}

	public boolean isNotEmpty() {
		return this.isEmpty() == false;
	}

	public boolean containsKey(String key)
	{
		// key = convertKey(key);
		return this.get(key) != null;
	}

	public boolean isNullValue(String key) {
		// key = convertKey(key);
		return containsKey(key) == false || this.get(key) == null;
	}
	public boolean isNotNullValue(String key) {
		return this.isNullValue(key) == false;
	}
	public boolean isEmptyValue(String key) {
		// key = convertKey(key);
		return containsKey(key) == false || this.get(key) == null || this.get(key).equals("");
	}
	public boolean isNotEmptyValue(String key) {
		return this.isEmptyValue(key) == false;
	}

	public String[] getStringArray(String key) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof String[]) {
			return (String[])aValue;
		}
		else if (aValue instanceof List)
		{
			List aList = (List)aValue;
			String[] arr = new String[aList.size()];
			for (int i=0; i<aList.size(); i++)
			{
				arr[i] = aList.get(i) == null ? null : aList.get(i).toString();
			}
			return arr;
		}

		return new String[]{aValue.toString()};
	}

	public int[] getIntArray(String key, int defaultElementValue) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof int[]) {
			return (int[])aValue;
		}


		String[] arr = getStringArray(key);
		if (arr == null || arr.length <= 0) return null;

		int[] arrResult = new int[arr.length];
		for (int i=0; i<arr.length; i++)
		{
			arrResult[i] = TextHelper.parseInt(arr[i], defaultElementValue);
		}

		return arrResult;
	}

	public String getString(String key, String defaultValue) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null || aValue.equals("")) {
			return defaultValue;
		}

		if (aValue instanceof String[]) {
			String[] arr = (String[])aValue;
			String aValue2 = arr.length <= 0 ? null : arr[0];
			return aValue2 == null ? defaultValue : aValue2;
		}

		else if (aValue instanceof List)
		{
			List aList = (List)aValue;
			return aList.isEmpty() ? null : aList.get(0).toString();
		}
		return aValue.toString();
	}

	public String getString(String key) {
		// key = convertKey(key);
		return getString(key, null);
	}

	public boolean getBoolean(String key, boolean defaultValue) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null || aValue.equals("")) {
			return defaultValue;
		}

		if (aValue instanceof Boolean) {
			return ((Boolean) aValue).booleanValue();
		}

		return TextHelper.parseBoolean(aValue.toString(), defaultValue);
	}

	public boolean getBoolean(String key) {
		// key = convertKey(key);
		return getBoolean(key, false);
	}


	public Boolean asBoolean(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof Boolean) {
			return (Boolean)aValue;
		}

		else if (aValue instanceof byte[]) {
			return new Boolean( TextHelper.parseBoolean( new String( (byte[])aValue ) ) );
		}

		else if (aValue instanceof char[]) {
			return new Boolean( TextHelper.parseBoolean( new String( (char[])aValue ) ) );
		}

		return new Boolean( TextHelper.parseBoolean( aValue.toString() ) );
	}


	public char getChar(String key, char defaultValue)
	{
		String aValue = this.getString(key, null);
		if (TextHelper.isEmpty(aValue))
		{
			return defaultValue;
		}

		return aValue.charAt(0);
	}

	public char getChar(String key)
	{
		return this.getChar(key, (char)0);
	}

	public char[] getCharArray(String key, char defaultElementValue) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof char[]) {
			return (char[])aValue;
		}


		String[] arr = getStringArray(key);
		if (arr == null || arr.length <= 0) return null;

		char[] arrResult = new char[arr.length];
		for (int i=0; i<arr.length; i++)
		{
			try
			{
				arrResult[i] = arr[i].charAt(0);
			}
			catch (Throwable e)
			{
				arrResult[i] = defaultElementValue;
			}
		}

		return arrResult;
	}



	public int getInt(String key, int defaultValue) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null || aValue.equals("")) {
			return defaultValue;
		}

		if (aValue instanceof Number) {
			return ((Number) aValue).intValue();
		}

		return TextHelper.parseInt(aValue.toString(), defaultValue);
	}

	public int getInt(String key) {
		// key = convertKey(key);
		return getInt(key, 0);
	}


	public Integer asInteger(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof Integer) {
			return (Integer)aValue;
		}

		else if (aValue instanceof Number) {
			return new Integer( ((Number)aValue).intValue() );
		}

		else if (aValue instanceof byte[]) {
			return new Integer( TextHelper.parseInt( new String( (byte[])aValue ) ) );
		}

		else if (aValue instanceof char[]) {
			return new Integer( TextHelper.parseInt( new String( (char[])aValue ) ) );
		}

		return new Integer( TextHelper.parseInt(aValue.toString()) );
	}



	public long getLong(String key, long defaultValue) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null || aValue.equals("")) {
			return defaultValue;
		}

		if (aValue instanceof Number) {
			return ((Number) aValue).longValue();
		}

		return TextHelper.parseLong(aValue.toString(), defaultValue);
	}

	public long getLong(String key) {
		// key = convertKey(key);
		return getLong(key, 0L);
	}

	public float getFloat(String key, float defaultValue) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null || aValue.equals("")) {
			return defaultValue;
		}

		if (aValue instanceof Number) {
			return ((Number) aValue).floatValue();
		}

		return TextHelper.parseFloat(aValue.toString(), defaultValue);
	}

	public float getFloat(String key) {
		// key = convertKey(key);
		return getFloat(key, 0.0F);
	}

	public Float asFloat(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof Float) {
			return (Float)aValue;
		}

		else if (aValue instanceof Number) {
			return new Float( ((Number)aValue).floatValue() );
		}

		else if (aValue instanceof byte[]) {
			return new Float( TextHelper.parseFloat( new String( (byte[])aValue ) ) );
		}

		else if (aValue instanceof char[]) {
			return new Float( TextHelper.parseFloat( new String( (char[])aValue ) ) );
		}

		return new Float( TextHelper.parseFloat(aValue.toString()) );
	}


	public Long asLong(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof Long) {
			return (Long)aValue;
		}

		else if (aValue instanceof Number) {
			return new Long( ((Number)aValue).longValue() );
		}

		else if (aValue instanceof byte[]) {
			return new Long( TextHelper.parseLong( new String( (byte[])aValue ) ) );
		}

		else if (aValue instanceof char[]) {
			return new Long( TextHelper.parseLong( new String( (char[])aValue ) ) );
		}

		return new Long( TextHelper.parseLong(aValue.toString()) );
	}



	public double getDouble(String key, double defaultValue) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null || aValue.equals("")) {
			return defaultValue;
		}

		if (aValue instanceof Number) {
			return ((Number) aValue).doubleValue();
		}

		return TextHelper.parseDouble(aValue.toString(), defaultValue);
	}

	public double getDouble(String key) {
		// key = convertKey(key);
		return getDouble(key, 0.0);
	}

	public Double asDouble(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof Double) {
			return (Double)aValue;
		}

		else if (aValue instanceof Number) {
			return new Double( ((Number)aValue).doubleValue() );
		}

		else if (aValue instanceof byte[]) {
			return new Double( TextHelper.parseDouble( new String( (byte[])aValue ) ) );
		}

		else if (aValue instanceof char[]) {
			return new Double( TextHelper.parseDouble( new String( (char[])aValue ) ) );
		}

		return new Double( TextHelper.parseDouble(aValue.toString()) );
	}



	public BigDecimal getBigDecimal(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof BigDecimal) {
			return (BigDecimal)aValue;
		}

		else if (aValue instanceof Number) {
			return new BigDecimal( ((Number)aValue).doubleValue() );
		}

		else if (aValue instanceof byte[]) {
			return new BigDecimal( new String( (byte[])aValue ) );
		}

		else if (aValue instanceof char[]) {
			return new BigDecimal( new String( (char[])aValue ) );
		}

		return new BigDecimal( aValue.toString() );
	}

	public BigInteger getBigInteger(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof BigInteger) {
			return (BigInteger)aValue;
		}

		else if (aValue instanceof Number) {
			return BigInteger.valueOf( ((Number)aValue).longValue() );
		}

		else if (aValue instanceof byte[]) {
			return new BigInteger( new String( (byte[])aValue ) );
		}

		else if (aValue instanceof char[]) {
			return new BigInteger( new String( (char[])aValue ) );
		}

		return new BigInteger( aValue.toString() );
	}


	public java.util.Date getDate(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof java.util.Date) {
			return (java.util.Date)aValue;
		}

		else if (aValue instanceof java.sql.Date) {
			return (java.util.Date)aValue;
		}

		else if (aValue instanceof Time) {
			return (java.util.Date)aValue;
		}

		else if (aValue instanceof Timestamp) {
			return (java.util.Date)aValue;
		}

		else if (aValue instanceof Calendar) {
			return ((Calendar)aValue).getTime();
		}

		return (java.util.Date)aValue;
	}

	public java.sql.Date getSQLDate(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof java.util.Date) {
			return new java.sql.Date( ((java.util.Date)aValue).getTime() );
		}

		else if (aValue instanceof java.sql.Date) {
			return (java.sql.Date)aValue;
		}

		else if (aValue instanceof Time) {
			return new java.sql.Date( ((Time)aValue).getTime() );
		}

		else if (aValue instanceof Timestamp) {
			return new java.sql.Date( ((Timestamp)aValue).getTime() );
		}

		else if (aValue instanceof Calendar) {
			return new java.sql.Date( ((Calendar)aValue).getTimeInMillis() );
		}

		return (java.sql.Date)aValue;
	}

	//------------------------------------------------------------------------------------------------------------------------------------------------

	public Time getTime(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof java.util.Date) {
			return new Time( ((java.util.Date)aValue).getTime() );
		}

		else if (aValue instanceof java.sql.Date) {
			return new Time( ((java.sql.Date)aValue).getTime() );
		}

		else if (aValue instanceof Time) {
			return (Time)aValue;
		}

		else if (aValue instanceof Timestamp) {
			return new Time( ((Timestamp)aValue).getTime() );
		}

		else if (aValue instanceof Calendar) {
			return new Time( ((Calendar)aValue).getTimeInMillis() );
		}

		return (Time)aValue;
	}

	public Timestamp getTimestamp(String key) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return null;
		}

		if (aValue instanceof java.util.Date) {
			return new Timestamp( ((java.util.Date)aValue).getTime() );
		}

		else if (aValue instanceof java.sql.Date) {
			return new Timestamp( ((java.sql.Date)aValue).getTime() );
		}

		else if (aValue instanceof Time) {
			return new Timestamp( ((Time)aValue).getTime() );
		}

		else if (aValue instanceof Timestamp) {
			return (Timestamp)aValue;
		}

		else if (aValue instanceof Calendar) {
			return new Timestamp( ((Calendar)aValue).getTimeInMillis() );
		}

		return (Timestamp)aValue;
	}



	public byte[] getBytes(String key, byte[] defaultValue) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return defaultValue;
		}

		else if (aValue instanceof byte[]) {
			return (byte[])aValue;
		}

		else if (aValue instanceof char[]) {
			char[] arrChar = (char[])aValue;
			byte[] arrByte = new byte[arrChar.length];
			for (int i=0; i<arrChar.length; i++) {
				arrByte[i] = (byte)arrChar[i];
			}

			return arrByte;
		}

		return aValue.toString().getBytes();
	}

	public byte[] getBytes(String key) {
		// key = convertKey(key);
		return getBytes(key, null);
	}

	public char[] getChars(String key, char[] defaultValue) {
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null) {
			return defaultValue;
		}

		else if (aValue instanceof char[]) {
			return (char[])aValue;
		}

		else if (aValue instanceof String) {
			return ((String)aValue).toCharArray();
		}

		else if (aValue instanceof byte[]) {
			byte[] arrByte = (byte[])aValue;
			char[] arrChar = new char[arrByte.length];
			for (int i=0; i<arrByte.length; i++) {
				arrChar[i] = (char)arrByte[i];
			}

			return arrChar;
		}

		return aValue.toString().toCharArray();
	}

	public char[] getChars(String key) {
		// key = convertKey(key);
		return getChars(key, null);
	}







	public DataMap<K, V> getDataMap(String key, DataMap<K, V> defaultValue) {
		// key = convertKey(key);
		DataMap aValue = (DataMap) this.get(key);
		return aValue == null ? defaultValue : aValue;
	}





	//------------------------------------------------------------------------------------------------------------------------------------------------

	public Byte asByte(String key)
	{
		// key = convertKey(key);
		Object aValue = getObject(key);
		if (aValue == null)
		{
			return null;
		}

		if (aValue instanceof Byte)
		{
			return (Byte)aValue;
		}

		else if (aValue instanceof byte[])
		{
			return Byte.valueOf( new String( (byte[])aValue ) );
		}

		else if (aValue instanceof char[])
		{
			return Byte.valueOf( new String( (char[])aValue ) );
		}

		return Byte.valueOf( aValue.toString() );
	}

	public byte getByte(String key, byte defaultValue)
	{
		// key = convertKey(key);
		Byte aValue = asByte(key);
		if (aValue == null) {
			return defaultValue;
		}

		return aValue.byteValue();
	}

	public byte getByte(String key) {
		// key = convertKey(key);
		return getByte(key, (byte)0);
	}


	public DataMap<K, V> getDataMap(String key) {
		// key = convertKey(key);
		return getDataMap(key, null);
	}

	public List getList(String key) {
		// key = convertKey(key);
		Object aValue = this.get(key);
		if (aValue == null) return null;

		if (aValue instanceof List)
		{
			return (List)aValue;
		}
		else if (aValue instanceof String[])
		{
			String[] arr = (String[])aValue;
			List<String> lst = new LinkedList<String>();
			for (int i=0; i<arr.length; i++)
			{
				lst.add(arr[i]);
			}

			return lst;
		}
		else if (aValue instanceof String)
		{
			List<String> lst = new LinkedList<String>();
			lst.add((String)aValue);

			return lst;
		}
		else if (aValue instanceof int[])
		{
			int[] arr = (int[])aValue;
			List<Integer> lst = new LinkedList<Integer>();
			for (int i=0; i<arr.length; i++)
			{
				lst.add(new Integer(arr[i]));
			}

			return lst;
		}
		else if (aValue instanceof char[])
		{
			char[] arr = (char[])aValue;
			List<String> lst = new LinkedList<String>();
			for (int i=0; i<arr.length; i++)
			{
				lst.add(String.valueOf(arr[i]));
			}

			return lst;
		}

		return null;
	}

	public Object getObject(String key) {
		// key = convertKey(key);
		return this.get(key);
	}


    public DataMap xput(K key, V value) {
        this.put(key, value);
        return this;
    }

	@Override
	public V put(K key, V value)
	{
		if (value ==null)
		{
			return super.remove(key);
		}

		if (key instanceof String && ms_number2StringKeyList.indexOf((String)key) >= 0)
		{
			if (value instanceof Number)
			{
				return super.put(key, (V)value.toString());
			}
		}
//		if (value instanceof Number)
//		{
//			if ( ((Number)value).doubleValue() > Integer.MAX_VALUE)
//			//if (false == (value instanceof Integer || value instanceof Float || value instanceof Short))
//			{
//				return super.put(key, (V)value.toString());
//			}
//
//		}

		// if (key instanceof String) key = (K)convertKey((String)key);
		return super.put(key, value);
	}

	@Override
	public void putAll(Map<? extends K,? extends V> m)
	{
		if (m != null)
		{
			super.putAll(m);
		}
	}

	public void putAll(Properties prop)
	{
		for (Enumeration en = prop.propertyNames(); en.hasMoreElements();)
		{
			String name = (String) en.nextElement();
			this.setString(name, prop.getProperty(name));
		}
	}

	public Object setBoolean(String key, boolean value) {
		// key = convertKey(key);
		return this.put((K)key, (V)new Boolean(value));
	}

	public Object setChar(String key, char value) {
		// key = convertKey(key);
		return this.put((K)key, (V)String.valueOf(value));
	}
	public Object setInt(String key, int value) {
		// key = convertKey(key);
		return this.put((K)key, (V)new Integer(value));
	}

	public Object setLong(String key, long value) {
		// key = convertKey(key);
		return this.put((K)key, (V)new Long(value));
	}

	public Object setFloat(String key, float value) {
		// key = convertKey(key);
		return this.put((K)key, (V)new Float(value));
	}

	public Object setDouble(String key, double value) {
		// key = convertKey(key);
		return this.put((K)key, (V)new Double(value));
	}

	public Object setString(String key, String value) {
		// key = convertKey(key);
		return this.put((K)key, (V)value);
	}

	public Object setDataMap(String key, DataMap value) {
		// key = convertKey(key);
		return this.put((K)key, (V)value);
	}

	public Object setList(String key, List value) {
		// key = convertKey(key);
		return this.put((K)key, (V)value);
	}

	public Object setObject(String key, Object value) {
		// key = convertKey(key);
		return this.put((K)key, (V)value);
	}

	public Object setStringArray(String key, String[] value) {
		// key = convertKey(key);
		return this.put((K)key, (V)value);
	}}
