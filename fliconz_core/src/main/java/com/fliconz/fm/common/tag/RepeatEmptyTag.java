/*
 * @(#)RepeatEmptyTag.java
 */
package com.fliconz.fm.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

public class RepeatEmptyTag extends BodyTagSupport
{
	//##############################################################################################################################

	public RepeatEmptyTag() {
		super();
	}


	//##############################################################################################################################

	public void release() {
		super.release();
	}


	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{
		RepeatTag aRepeatTag = (RepeatTag)TagSupport.findAncestorWithClass(this, RepeatTag.class);
		if ( aRepeatTag == null) {
			throw new JspTagException("parent repeat tag is not found.");
		}

		if ( !aRepeatTag.isEmpty() ) {
			return SKIP_BODY;
		}

		return EVAL_BODY_BUFFERED;
	}


	public int doAfterBody() throws JspTagException
	{
		try
		{
			bodyContent.writeOut(getPreviousOut());
			bodyContent.clearBody();

			return SKIP_BODY;
		}
		catch(IOException e) {
			throw new JspTagException( e.getMessage() );
		}
	}
}
