package com.fliconz.fm.common.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpsFilter implements Filter
{
	private FilterConfig m_filterConfig = null;

	public HttpsFilter()
	{
		super();
	}


	@Override
	public void destroy()
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		String sessionCookieName = this.m_filterConfig.getInitParameter("sessionCookieName");

		// http, https 세션공유를 위한 처리
		HttpsRequestWrapper httpsRequest = null;
		if (request instanceof HttpsRequestWrapper)
		{
			httpsRequest = (HttpsRequestWrapper)request;
		}
		else
		{
			httpsRequest = new HttpsRequestWrapper((HttpServletRequest)request, sessionCookieName==null? "JSESSIONID" : sessionCookieName);
			httpsRequest.setResponse((HttpServletResponse)response);
			//httpsRequest.processSessionCookie(((HttpServletRequest)request).getSession());
		}
		chain.doFilter(httpsRequest, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException
	{
		this.m_filterConfig = config;
	}
}
