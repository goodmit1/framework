package com.fliconz.fm.common.util;

import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ResourceHelper
{
//	static Log m_logger = LogFactory.getLog(ResourceHelper.class);
	private final static Logger logger = LoggerFactory.getLogger(ResourceHelper.class);

	public static Object setAttribute(String name, Object value)
	{
		return ResourceLoader.setAttribute(name, value);
	}

	public static Object getAttribute(String name)
	{
		return ResourceLoader.getAttribute(name);
	}


	public static boolean isProductionMode()
	{
		return System.getProperty("TOCProductionMode", "false").equals("true");
	}

	public static Iterator<String> getResourceNames()
	{
		return ResourceLoader.getResourceNames();
	}

	public static Map<String,XProperties> getResourceConfig(String resourceName)
	{
		// resource config per locale
		return ResourceLoader.getResourceConfig(resourceName);
	}


	public static XProperties getXProperties(String locale, String resourceName)
	{
		return ResourceLoader.getXProperties(locale, resourceName);
	}
	public static XProperties getXProperties(String resourceName)
	{
		return ResourceLoader.getXProperties(resourceName);
	}

	public static boolean isRefResource(String locale, String resourceName)
	{
		return ResourceLoader.isRefResource(locale, resourceName);
	}

	public static String formatString(String locale, String resourceName, String propertyName, String defaultFormat, Object... args)
	{
		String formatString = null;
		XProperties ptResource = ResourceHelper.getXProperties(locale, resourceName);
		if (ptResource == null)
		{
			formatString = defaultFormat;
		}
		else
		{
			formatString = ptResource.getString(propertyName, defaultFormat);
		}

		return String.format(formatString, args);
	}

	public static String formatString(String resourceName, String propertyName, String defaultFormat, Object... args)
	{
		return ResourceHelper.formatString((String)null, resourceName, propertyName, defaultFormat, args);
	}

	public static String formatString(String resourceName, String propertyName, Object... args)
	{
		return ResourceHelper.formatString((String)null, resourceName, propertyName, null, args);
	}




	public static String getString(String locale, String resourceName, String propertyName, String defaultValue)
	{
		XProperties ptResource = ResourceHelper.getXProperties(locale, resourceName);
		if (ptResource == null)
		{
			if ( logger.isWarnEnabled() )
			{

				if (defaultValue == null)
				{
					logger.warn(
							new StringBuilder()
								.append("resource is not found.").append(FileHelper.LINE_SEPARATOR)
								.append("resourc-name : [").append(resourceName).append("]").append(FileHelper.LINE_SEPARATOR)
								.append("<property name=\"").append(propertyName).append("\"/>")
								.toString()
						);
				}
				else
				{
					logger.warn(
						new StringBuilder()
							.append("resource is not found. the next resource/property is applied.").append(FileHelper.LINE_SEPARATOR)
							.append("resourc-name : [").append(resourceName).append("]").append(FileHelper.LINE_SEPARATOR)
							.append("<property name=\"").append(propertyName).append("\">").append(defaultValue).append("</property>")
							.toString()
					);
				}
			}

			return defaultValue;
		}



		String resultValue = ptResource.getString(propertyName);
		if (resultValue == null)
		{
			if (defaultValue == null)
			{
				logger.warn(
						new StringBuilder()
							.append("resource property is not set.").append(FileHelper.LINE_SEPARATOR)
							.append("resourc-name : [").append(resourceName).append("]").append(FileHelper.LINE_SEPARATOR)
							.append("<property name=\"").append(propertyName).append("\"/>")
							.toString()
					);
			}
			else
			{
				logger.warn(
					new StringBuilder()
						.append("resource property is not set. the next property is applied.").append(FileHelper.LINE_SEPARATOR)
						.append("resourc-name : [").append(resourceName).append("]").append(FileHelper.LINE_SEPARATOR)
						.append("<property name=\"").append(propertyName).append("\">").append(defaultValue).append("</property>")
						.toString()
				);
				resultValue = defaultValue;
			}
		}

		return resultValue;
	}

	public static String getString(String resourceName, String propertyName, String defaultValue)
	{
		return ResourceHelper.getString((String)null, resourceName, propertyName, defaultValue);
	}

	public static String getString(String resourceName, String propertyName)
	{
		return ResourceHelper.getString((String)null, resourceName, propertyName, null);
	}




	public static boolean getBoolean(String locale, String resourceName, String propertyName, boolean defaultValue)
	{
		XProperties ptResource = ResourceHelper.getXProperties(locale, resourceName);
		if (ptResource == null)
		{
			return defaultValue;
		}

		return ptResource.getBoolean(propertyName, defaultValue);
	}

	public static boolean getBoolean(String resourceName, String propertyName, boolean defaultValue)
	{
		return ResourceHelper.getBoolean((String)null, resourceName, propertyName, defaultValue);
	}

	public static boolean getBoolean(String resourceName, String propertyName)
	{
		return ResourceHelper.getBoolean((String)null, resourceName, propertyName, false);
	}





	public static int getInt(String locale, String resourceName, String propertyName, int defaultValue)
	{
		XProperties ptResource = ResourceHelper.getXProperties(locale, resourceName);
		if (ptResource == null)
		{
			return defaultValue;
		}

		return ptResource.getInt(propertyName, defaultValue);
	}

	public static int getInt(String resourceName, String propertyName, int defaultValue)
	{
		return ResourceHelper.getInt((String)null, resourceName, propertyName, defaultValue);
	}

	public static int getInt(String resourceName, String propertyName)
	{
		return ResourceHelper.getInt((String)null, resourceName, propertyName, 0);
	}





	public static long getLong(String locale, String resourceName, String propertyName, long defaultValue)
	{
		XProperties ptResource = ResourceHelper.getXProperties(locale, resourceName);
		if (ptResource == null)
		{
			return defaultValue;
		}

		return ptResource.getLong(propertyName, defaultValue);
	}

	public static long getLong(String resourceName, String propertyName, long defaultValue)
	{
		return ResourceHelper.getLong((String)null, resourceName, propertyName, defaultValue);
	}

	public static long getLong(String resourceName, String propertyName)
	{
		return ResourceHelper.getLong((String)null, resourceName, propertyName, 0L);
	}





	public static float getFloat(String locale, String resourceName, String propertyName, float defaultValue)
	{
		XProperties ptResource = ResourceHelper.getXProperties(locale, resourceName);
		if (ptResource == null)
		{
			return defaultValue;
		}

		return ptResource.getFloat(propertyName, defaultValue);
	}

	public static float getFloat(String resourceName, String propertyName, float defaultValue)
	{
		return ResourceHelper.getFloat((String)null, resourceName, propertyName, defaultValue);
	}

	public static float getFloat(String resourceName, String propertyName)
	{
		return ResourceHelper.getFloat((String)null, resourceName, propertyName, 0.0F);
	}





	public static double getDouble(String locale, String resourceName, String propertyName, double defaultValue)
	{
		XProperties ptResource = ResourceHelper.getXProperties(locale, resourceName);
		if (ptResource == null)
		{
			return defaultValue;
		}

		return ptResource.getDouble(propertyName, defaultValue);
	}

	public static double getDouble(String resourceName, String propertyName, double defaultValue)
	{
		return ResourceHelper.getDouble((String)null, resourceName, propertyName, defaultValue);
	}

	public static double getDouble(String resourceName, String propertyName)
	{
		return ResourceHelper.getDouble((String)null, resourceName, propertyName, 0.0);
	}










	public static String getString(HttpServletRequest request, String resourceName, String propertyName, String defaultValue)
	{
		return ResourceHelper.getString(request == null ? (String)null : request.getLocale().toString(), resourceName, propertyName, defaultValue);
	}

	public static String getString(HttpServletRequest request, String resourceName, String propertyName)
	{
		return ResourceHelper.getString(request, resourceName, propertyName, null);
	}



	public static boolean getBoolean(HttpServletRequest request, String resourceName, String propertyName, boolean defaultValue)
	{
		return ResourceHelper.getBoolean(request == null ? (String)null : request.getLocale().toString(), resourceName, propertyName, defaultValue);
	}

	public static boolean getBoolean(HttpServletRequest request, String resourceName, String propertyName)
	{
		return ResourceHelper.getBoolean(request, resourceName, propertyName, false);
	}



	public static int getInt(HttpServletRequest request, String resourceName, String propertyName, int defaultValue)
	{
		return ResourceHelper.getInt(request == null ? (String)null : request.getLocale().toString(), resourceName, propertyName, defaultValue);
	}

	public static int getInt(HttpServletRequest request, String resourceName, String propertyName)
	{
		return ResourceHelper.getInt(request, resourceName, propertyName, 0);
	}



	public static long getLong(HttpServletRequest request, String resourceName, String propertyName, long defaultValue)
	{
		return ResourceHelper.getLong(request == null ? (String)null : request.getLocale().toString(), resourceName, propertyName, defaultValue);
	}

	public static long getLong(HttpServletRequest request, String resourceName, String propertyName)
	{
		return ResourceHelper.getLong(request, resourceName, propertyName, 0L);
	}



	public static float getFloat(HttpServletRequest request, String resourceName, String propertyName, float defaultValue)
	{
		return ResourceHelper.getFloat(request == null ? (String)null : request.getLocale().toString(), resourceName, propertyName, defaultValue);
	}

	public static float getFloat(HttpServletRequest request, String resourceName, String propertyName)
	{
		return ResourceHelper.getFloat(request, resourceName, propertyName, 0.0f);
	}



	public static double getDouble(HttpServletRequest request, String resourceName, String propertyName, double defaultValue)
	{
		return ResourceHelper.getDouble(request == null ? (String)null : request.getLocale().toString(), resourceName, propertyName, defaultValue);
	}

	public static double getDouble(HttpServletRequest request, String resourceName, String propertyName)
	{
		return ResourceHelper.getDouble(request, resourceName, propertyName, 0.0);
	}
}
