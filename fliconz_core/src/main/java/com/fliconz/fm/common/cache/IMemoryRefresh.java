package com.fliconz.fm.common.cache;

import org.json.JSONObject;

import com.fliconz.fm.exception.MemoryRefreshException;

public interface IMemoryRefresh {

	public void registClient(String client) throws MemoryRefreshException;

	public JSONObject refresh(String kubun);
	public JSONObject refreshClient(String client, String kubun);

}
