package com.fliconz.fm.common.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.fliconz.fw.runtime.util.PropertyManager;
import org.apache.commons.codec.binary.Base64;

public class SecurityHelper
{
	private final static String DEFAULT_SECURITY_KEY = SecurityHelper.class.getName();
//	/*
//	* 자바시큐리티 르로그램밍 책 693참조
//	* byte[] desKeyData = generateHash(strkey);
//	*/
	private static SecretKey getKey(String strkey) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException
	{
		// 입력값을 bytes로 변환
		byte[] desKeyData = strkey.getBytes();
		// 암호화 키 생성
		DESKeySpec desKeySpec = new DESKeySpec(desKeyData);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		return keyFactory.generateSecret(desKeySpec);
	}
//
//	private static final Random RANDOM = new SecureRandom () ;
//	   /** Length of password. @see #generateRandomPassword() */
//	   public static final int PASSWORD_LENGTH = 8 ;
//	   /**
//	    * Generate a random String suitable for use as a temporary password.
//	    *
//	    * @return String suitable for use as a temporary password
//	    * @since 2.4
//	    */
//	   public static String generateRandomPassword ()
//	   {
//	       // Pick from some letters that won't be easily mistaken for each
//	       // other. So, for example, omit o O and 0, 1 l and L.
//	       String letters = "abcdefghjkmnpqrstuvwxyz1234567890" ;
//
//	       String pw = "" ;
//	       for ( int i= 0 ; i<PASSWORD_LENGTH; i++ )
//	       {
//	           int index = ( int )( RANDOM.nextDouble () *letters.length ()) ;
//	           pw += letters.substring ( index, index+ 1 ) ;
//	       }
//	       return pw;
//	   }




	private static final Random RANDOM = new SecureRandom () ;
//	/** Length of password. @see #generateRandomPassword() */
//	public static final int PASSWORD_LENGTH = 8 ;
//	public static final int SECURE_NUMBER_LENGTH = 6 ;
/**
	* Generate a random String suitable for use as a temporary password.
	*
	* @return String suitable for use as a temporary password
	* @since 2.4
	*/
	public static String generateAuthNo (int secureLength)
	{
		// Pick from some letters that won't be easily mistaken for each
		// other. So, for example, omit o O and 0, 1 l and L.
		String letters = "1234567890" ;

		StringBuilder pw = new StringBuilder() ;
		for ( int i= 0 ; i<secureLength; i++ )
		{
			int index = ( int )( RANDOM.nextDouble () *letters.length ()) ;
			pw.append( letters.substring ( index, index+ 1 ) );
		}
		return pw.toString();
	}

	public static String generateRandomPassword(int passwordLength)
	{
		 // Pick from some letters that won't be easily mistaken for each
		// other. So, for example, omit o O and 0, 1 l and L.
		String letters = "abcdefghjkmnpqrstuvwxyz1234567890" ;

		StringBuilder pw = new StringBuilder() ;
		for ( int i= 0 ; i<passwordLength; i++ )
		{
			int index = ( int )( RANDOM.nextDouble () *letters.length ()) ;
		   pw.append( letters.substring ( index, index+ 1 ) );
		}

		return pw.toString();
	}

	public static String generateRandomUserId(int userIdLength)
	{
		 // Pick from some letters that won't be easily mistaken for each
		// other. So, for example, omit o O and 0, 1 l and L.
		String letters = "abcdefghjkmnpqrstuvwxyz1234567890" ;

		StringBuilder pw = new StringBuilder() ;
		for ( int i= 0 ; i<userIdLength; i++ )
		{
			int index = ( int )( RANDOM.nextDouble () *letters.length ()) ;
		   pw.append( letters.substring ( index, index+ 1 ) );
		}

		return pw.toString();
}



	// 주민등록번호 처럼 영구적인 값을 암호화 하고 다시 복호할 때는 키값이 바뀌면 안되기 때문에 고정된 키값을 갖는 메소드를 사용한다.
	public static String encrypt(String data) throws Exception
	{
		return encrypt(data, DEFAULT_SECURITY_KEY);
	}

	// 주민등록번호 처럼 영구적인 값을 암호화 하고 다시 복호할 때는 키값이 바뀌면 안되기 때문에 고정된 키값을 갖는 메소드를 사용한다.
	public static String decrypt(String data) throws Exception
	{
		return decrypt(data, DEFAULT_SECURITY_KEY);
	}

	public static String encrypt(String data, String key) throws Exception
	{
		if (data == null || data.length() == 0 || key == null || key.length() == 0) return null;
		String algorithm = PropertyManager.getString("crypt.algorithm.password","DES");
		if("DES".equals(algorithm)){
			return encryptDES(data, key);
		}
		else if(algorithm.startsWith("SHA")){
			return encryptSha(algorithm, data, key);
		}
		else{
			throw new Exception("algorithm(" + algorithm  + ") not found");
		}

	}

	public static String encryptDES(String data, String key) throws Exception {
		// space character ' '로 길이 8의 right padding
		String encryptKey = String.format("%1$-8s", key);

		// 암호화를 위한 세팅
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, getKey(encryptKey));

		byte[] inputBytes1 = data.getBytes("UTF8");
		byte[] outputBytes1 = cipher.doFinal(inputBytes1);

//		sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
//		return encoder.encode(outputBytes1);

		return new String(Base64.encodeBase64(outputBytes1));
	}
	public static String encryptSha(String algorithm, String raw, String salt) throws Exception{
		MessageDigest md = MessageDigest.getInstance(algorithm);
		md.update(salt.getBytes());
		md.update(raw.getBytes());
		return String.format("%064x", new BigInteger(1, md.digest()));
	}
	public static String encryptSha256(String target) throws Exception{
		MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(target.getBytes());
        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            String hex = Integer.toHexString(0xff & byteData[i]);
            if(hex.length() == 1){
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
	}
	public static String decrypt(String data, String key) throws Exception
	{
		if (data == null || data.length() == 0 || key == null || key.length() == 0) return null;

		String algorithm = PropertyManager.getString("crypt.algorithm.password","DES");
		if("DES".equals(algorithm)){
			return decryptDES(data, key);
		}
		else{
			throw new Exception("algorithm(" + algorithm  + ") not found");
		}

	}
	public static String decryptDES(String data, String key) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException
	{
		if (data == null || data.length() == 0 || key == null || key.length() == 0) return null;

		// space character ' '로 길이 8의 right padding
		String encryptKey = String.format("%1$-8s", key);

		javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(javax.crypto.Cipher.DECRYPT_MODE, getKey(encryptKey));

//		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
//		byte[] inputBytes1 = decoder.decodeBuffer(data);
		byte[] inputBytes1 = Base64.decodeBase64(data.getBytes());
		byte[] outputBytes2 = cipher.doFinal(inputBytes1);

		return new String(outputBytes2, "UTF8");

	}


//	public static String padRight(String s, int n) {
//	     return String.format("%1$-" + n + "s", s);
//	}
//
//	public static String padLeft(String s, int n) {
//	    return String.format("%1$#" + n + "s", s);
//	}

	private static void printUsage()
	{
		System.out.println("usage : "+SecurityHelper.class.getName()+" <encrypt|decrypt> <value> <key>");
	}


	/*
WEB_INF_HOME=/Volumes/data/projects/MBill/development/WebContent/WEB-INF

java -cp $WEB_INF_HOME/classes:$WEB_INF_HOME/lib/commons-codec-1.6.jar com.fliconz.fm.common.util.SecurityHelper encrypt 11111 member
encrypted : [i5I/VxqFUhqVXOGymwW4+Q==]
decrypted : [64346f3ed9]

java -cp $WEB_INF_HOME/classes:$WEB_INF_HOME/lib/commons-codec-1.6.jar com.fliconz.fm.common.util.SecurityHelper decrypt i5I/VxqFUhqVXOGymwW4+Q== saturna
decrypted : [64346f3ed9]
encrypted : [i5I/VxqFUhqVXOGymwW4+Q==]
	 */
	public static void main(String[] args) throws Exception
	{
//		System.out.println("===["+padRight("Howto", 10) + "]==");
//		System.out.println("===["+padLeft("Howto", 5) + "]==");

		if (args == null || args.length < 3)
		{
			printUsage();
			System.exit(1);
			return;
		}
		String data = args[1];
		String key = args[2];

		if (args[0].equals("encrypt"))
		{
			String encrypted = SecurityHelper.encrypt(data, key);
			System.out.println("encrypted : ["+encrypted+"]");
			System.out.println("decrypted : ["+SecurityHelper.decrypt(encrypted, key)+"]");

		}
		else if (args[0].equals("decrypt"))
		{
			String decrypted = SecurityHelper.decrypt(data, key);
			System.out.println("decrypted : ["+decrypted+"]");
			System.out.println("encrypted : ["+SecurityHelper.encrypt(decrypted, key)+"]");
		}
		else
		{
			printUsage();
			System.exit(1);
			return;
		}
	}
	/*
java.security.InvalidKeyException: Illegal key size 오류는 Java 기본 패키지의 key size 크기 제한 때문입니다. 이를 해결하는 방법은 다음과 같습니다.
* 아래의 웹페이지에서 Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files 6 를 다운로드 받는다.
- http://www.oracle.com/technetwork/java/javase/downloads/index.html
* 위에서 다운로드 받은 파일의 압축을 해제한 후, java 실행 폴더 하위의 lib\security 폴더에 복사한다.
- java.exe 가 여러 곳에 존재할 수 있으므로 현재 실행되는 java 의 폴더를 확인한 후, 복사해 넣어야 한다.


(Mac OS) /Library/Java/Home/lib/security

	 */




	public static String getMD5String(String data)
	{
		if (data == null || data.length() == 0)
		{
			return data;
		}

		try
		{
			byte[] arrData = new byte[data.length()];
			for (int i=0; i<arrData.length; i++)
			{
				arrData[i] = (byte)(data.charAt(i) & 0xff);
			}

			MessageDigest md5er = MessageDigest.getInstance("MD5");
			byte[]  arrHash = md5er.digest(arrData);

			StringBuilder sb = new StringBuilder(32);
			for (int i=0; i<arrHash.length; i++)
			{
				String x = Integer.toHexString(arrHash[i] & 0xff).toUpperCase();
				if (x.length()<2) sb.append("0");
				sb.append(x);
			}

			return sb.toString();
		}
		catch (GeneralSecurityException e)
		{
			throw new RuntimeException(e);
		}
	}
}
