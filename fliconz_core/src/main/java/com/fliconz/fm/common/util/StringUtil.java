package com.fliconz.fm.common.util;

public class StringUtil {
	
	public static boolean isEmpty(String value){
		return value == null || "".equals(value);
	}
	
	public static boolean isEmpty(String... values){
		boolean isEmpty = true;
		for(int i = 0; i < values.length; i++){
			if(!isEmpty(values[i])) {
				isEmpty = false;
				break;
			}
		}
		return isEmpty;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println(isEmpty("", null, ""));
	}

}
