package com.fliconz.fm.common.scheduler;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

public class XSchedulerFactoryBean extends SchedulerFactoryBean
{
	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	public XSchedulerFactoryBean()
	{
		super();
		if (theLogger.isDebugEnabled()) theLogger.debug("===> XSchedulerFactoryBean: {}", getClass().getName());
	}


	@Override
	public void setQuartzProperties(Properties quartzProperties)
	{
		if (theLogger.isDebugEnabled()) theLogger.debug("===> quartzProperties : {}", quartzProperties);
		super.setQuartzProperties(quartzProperties);
	}



	@Override
	public void setDataSource(DataSource dataSource)
	{
		if (theLogger.isDebugEnabled()) theLogger.debug("===> dataSource : {}", dataSource);

		// dataSource 설정을 못하게 하여 QUARTZ DB에 연결하지 못하게 한다.
		//super.setDataSource(dataSource);
	}

	// server.name system property와  BaseCronTriggerBean의 targets 을 적절히 사용하여 trigger 를 분산시킬 수 있다.
	@Override
	public void setTriggers(Trigger[] triggers)
	{
		if (theLogger.isWarnEnabled()) theLogger.warn("====> setTriggers");
		String serverName = System.getProperty("server.name");
		if (serverName == null)
		{
			if (theLogger.isWarnEnabled()) theLogger.warn("server.name property is not set.");
			//if (theLogger.isInfoEnabled()) theLogger.info("No trigger is applied at this server.");
			serverName = "";
		}

		List<Trigger> triggerList = new LinkedList<Trigger>();
		for (int i=0; i<triggers.length; i++)
		{
			if (theLogger.isInfoEnabled()) theLogger.debug("====> trigger : {}", triggers[i]);
			if (triggers[i] instanceof Targetable)
			{
				Targetable aTargetable = (Targetable)triggers[i];
				if (aTargetable.hasTarget("*") || aTargetable.hasTarget(serverName))
				{
					triggerList.add(triggers[i]);
					if (theLogger.isInfoEnabled()) theLogger.info("[{}] trigger is applied at the [{}] target."
							, triggers[i], serverName);
				}
				else
				{
					if (theLogger.isInfoEnabled()) theLogger.info("[{}] trigger is ignored at the [{}] target."
							, triggers[i], serverName);
				}
			}
			else
			{
				triggerList.add(triggers[i]);
				if (theLogger.isInfoEnabled()) theLogger.info("[{}] trigger is applied at the [{}] target."
						, triggers[i].getJobDataMap(), serverName);
			}
		}

		if (triggerList.isEmpty())
		{
			if (theLogger.isInfoEnabled()) theLogger.info("No trigger is applied at the [{}] target.", serverName);
		}
		else
		{
			Trigger[] arrTrigger = new Trigger[triggerList.size()];
			triggerList.toArray(arrTrigger);

			super.setTriggers(arrTrigger);
			if (theLogger.isInfoEnabled()) theLogger.info("{}/{} triggers are applied at the [{}] target."
					, triggerList.size()
					, triggers.length
					, serverName);
		}
		/*
		super.setTriggers(triggers);
		if (theLogger.isWarnEnabled()) theLogger.warn("====> setTriggers");
		for (int i=0; i<triggers.length; i++)
		{
			Class[] arrClass = triggers[i].getClass().getInterfaces();
			if (arrClass == null || arrClass.length <= 0)
			{
				if (theLogger.isWarnEnabled()) theLogger.warn("====> no impl");
				continue;
			}
			for (int k=0; k<arrClass.length; k++)
			{
				if (theLogger.isWarnEnabled()) theLogger.warn("====> {} > {}", triggers[i], arrClass[k]);
			}
		}
		*/
		/*
		if (theLogger.isWarnEnabled()) theLogger.warn("====> setTriggers");
		String serverName = System.getProperty("server.name");
		if (serverName == null)
		{
			if (theLogger.isWarnEnabled()) theLogger.warn("server.name property is not set.");
			//if (theLogger.isInfoEnabled()) theLogger.info("No trigger is applied at this server.");
			serverName = "";
		}

		List<Trigger> triggerList = new LinkedList<Trigger>();
		for (int i=0; i<triggers.length; i++)
		{
			if (theLogger.isInfoEnabled()) theLogger.debug("====> trigger : {}", triggers[i]);
			if (triggers[i] instanceof Targetable)
			{
				Targetable aTargetable = (Targetable)triggers[i];
				if (aTargetable.hasTarget("*") || aTargetable.hasTarget(serverName))
				{
					triggerList.add(triggers[i]);
					if (theLogger.isInfoEnabled()) theLogger.info("[{}] trigger is applied at the [{}] target."
							, triggers[i], serverName);
				}
				else
				{
					if (theLogger.isInfoEnabled()) theLogger.info("[{}] trigger is ignored at the [{}] target."
							, triggers[i], serverName);
				}
			}
			else if (triggers[i] instanceof org.quartz.CronTrigger)
			{
				org.quartz.CronTrigger aCronTrigger = (org.quartz.CronTrigger)triggers[i];
				if (aCronTrigger instanceof Targetable)
				{
					Targetable aTargetable = (Targetable)aCronTrigger;
					if (aTargetable.hasTarget("*") || aTargetable.hasTarget(serverName))
					{
						triggerList.add(triggers[i]);
						if (theLogger.isInfoEnabled()) theLogger.info("[{}] trigger is applied at the [{}] target."
								, aCronTrigger, serverName);
					}
					else
					{
						if (theLogger.isInfoEnabled()) theLogger.info("[{}] trigger is ignored at the [{}] target."
								, aCronTrigger, serverName);
					}
				}
				else
				{
					triggerList.add(triggers[i]);
				}


//				JobDataMap aJobDataMap = aCronTrigger.getJobDataMap();
//				for (Iterator<String> it = aJobDataMap.keySet().iterator(); it.hasNext();)
//				{
//					Object aName = it.next();
//					Object aValue = aJobDataMap.get(aName);
//					theLogger.debug("==> [{}] = [{}]", aName, aValue);
//					//theLogger.debug("==> [{}]/[{}] = [{}]/[{}]", aName, aName.getClass().getName(), aValue, aValue.getClass().getName());
//				}
			}
			else
			{
				triggerList.add(triggers[i]);
				if (theLogger.isInfoEnabled()) theLogger.info("[{}] trigger is applied at the [{}] target."
						, triggers[i].getJobDataMap(), serverName);
			}
		}

		if (triggerList.isEmpty())
		{
			if (theLogger.isInfoEnabled()) theLogger.info("No trigger is applied at the [{}] target.", serverName);
		}
		else
		{
			Trigger[] arrTrigger = new Trigger[triggerList.size()];
			triggerList.toArray(arrTrigger);

			super.setTriggers(arrTrigger);
			if (theLogger.isInfoEnabled()) theLogger.info("{}/{} triggers are applied at the [{}] target."
					, triggerList.size()
					, triggers.length
					, serverName);
		}
		*/
	}
}
