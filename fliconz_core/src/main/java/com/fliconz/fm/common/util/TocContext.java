package com.fliconz.fm.common.util;

import java.util.Map;

public final class TocContext
{
	private final ThreadLocal<DataMap<String,Object>> m_threadLocal = new ThreadLocal<DataMap<String,Object>>();
	private static TocContext ms_tocContext = null;

	private TocContext()
	{
		super();
		m_threadLocal.set(new DataMap<String,Object>());
	}

	public static TocContext getInstance()
	{
		synchronized(TocContext.class)
		{
			if(ms_tocContext == null)
			{
				ms_tocContext = new TocContext();
			}
		}

		return ms_tocContext;
	}




	public DataMap<String,Object> getContext()
	{
		DataMap<String,Object> ctx = m_threadLocal.get();
		if (ctx == null)
		{
			ctx = new DataMap<String,Object>();
			m_threadLocal.set(ctx);
		}
		return ctx;
	}

	public Map<String,Object> getMapAttribute(String name)
	{
		return (Map<String,Object>)this.getAttribute(name);
	}

	public Object getAttribute(String name)
	{
		return getContext().get(name);
	}

	public void setAttribute(String name, Object value)
	{
		getContext().put(name, value);
	}

	public Object removeAttribute(String name)
	{
		return getContext().remove(name);
	}


	public void clearContext()
	{
		synchronized(m_threadLocal)
		{
			m_threadLocal.get().clear();
		}
	}
}
