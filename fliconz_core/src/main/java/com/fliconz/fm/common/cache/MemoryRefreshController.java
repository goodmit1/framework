package com.fliconz.fm.common.cache;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.fliconz.fm.exception.MemoryRefreshException;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController(value = "memoryRefreshController")
public class MemoryRefreshController extends MultiActionController {

	 

	@RequestMapping(value = { "/cache/refresh/{kubun}" }, method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void refresh(@PathVariable("kubun") String kubun, HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject jsonObject = new JSONObject();
		try{
			jsonObject.put("kubun", kubun);
			jsonObject.put("code", "000");
			jsonObject.put("msg", "refresh done.");
			if("prop".equals(kubun)){
				PropertyManager.init();
			} else if("menu".equals(kubun)){
				CacheManager.reloadMenu();
			} else if("perm".equals(kubun)){
				FMSecurityContextHelper fmSecurityContextHelper = (FMSecurityContextHelper) SpringBeanUtil.getBean("fmSecurityContextHelper");
				fmSecurityContextHelper.reloadPermissions();
			} else {
				try {
					ICacheManager extraCacheManager = (ICacheManager)SpringBeanUtil.getBean("extraCacheManager");
					if(extraCacheManager != null){
						try {
							extraCacheManager.reload(kubun);
						} catch(Exception e){
							String code = "001";
							String msg = "kubun ["+kubun+"] is not supprted.";
							if(e instanceof MemoryRefreshException){
								MemoryRefreshException me = (MemoryRefreshException)e;
								code = me.getCode();
								msg = me.getMessage();
							}
							jsonObject.put("code", code);
							jsonObject.put("msg", msg);
						}
					}
				} catch(Exception e){
					jsonObject.put("code", "001");
					jsonObject.put("msg", e.getMessage());
				}
			}
		} catch(Exception e){
			e.printStackTrace();
			jsonObject.put("code", "001");
			jsonObject.put("msg", e.getMessage());
		}
		response.getWriter().write(jsonObject.toString());
	}

}
