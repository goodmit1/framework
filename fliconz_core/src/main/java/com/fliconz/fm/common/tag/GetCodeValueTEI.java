/*
* @(#)GetPropertyTEI.java
 */
package com.fliconz.fm.common.tag;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;

import com.fliconz.fm.common.util.TextHelper;

public class GetCodeValueTEI extends TagExtraInfo
{
	public GetCodeValueTEI() {
		super();
	}

	//##############################################################################################################################

	public VariableInfo[] getVariableInfo(TagData data)
	{
		if (data.getId() == null)
		{
			return null;
		}

		boolean isDeclare = TextHelper.parseBoolean( data.getAttributeString("declare"), true );
		VariableInfo[] result = { new VariableInfo(data.getId(), "java.lang.String", isDeclare, VariableInfo.AT_END) };
		return result;
	}

	public boolean isValid(TagData data) {
		return true;
	}
}
