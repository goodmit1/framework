package com.fliconz.fm.common.geocode;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.LinkedList;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONException;
//import net.sf.json.JSONObject;
//import net.sf.json.JSONSerializer;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;

//http://dev.vworld.kr/dev/dv_geocoderguide_s002.do

// 지번주소 -> 좌표
// http://apis.vworld.kr/jibun2coord.do?q=[지번주소]&apiKey=[인증키]&domain=[도메인]&output=[리턴타입]&epsg=[좌표계]&callback=[func]
// 도로명주소 -> 좌표
// http://apis.vworld.kr/new2coord.do?q=[도로명주소]&apiKey=[인증키]&domain=[도메인]&output=[리턴타입]&epsg=[좌표계]&callback=[func]

// 좌표 -> 지번주소
// http://apis.vworld.kr/coord2jibun.do?x=[X좌표]&y=[Y좌표]&apiKey=[인증키]&domain=[도메인]&output=[리턴타입]&epsg=[좌표계]&callback=[func]
// 좌표 -> 도로명주소
// http://apis.vworld.kr/coord2new.do?x=[X좌표]&y=[Y좌표]&apiKey=[인증키]&domain=[도메인]&output=[리턴타입]&epsg=[좌표계]&callback=[func]

public class VWorldGeocoder extends Geocoder
{
	private String m_mapAPIKey = null;


	public VWorldGeocoder(String mapAPIKey)
	{
		super();
		m_mapAPIKey = mapAPIKey;
	}

	public VWorldGeocoder()
	{
		super();
	}

	public String getAPIKey()
	{
		return m_mapAPIKey;
	}
	public void setAPIKey(String apiKey)
	{
		m_mapAPIKey = apiKey;
	}

	@Override
	public boolean canPaging(boolean isGeocode)
	{
		// 주소-->좌표 변환만 페이징 처리
		return false;
	}



	// 지번주소 -> 좌표
	// http://apis.vworld.kr/jibun2coord.do?q=[지번주소]&apiKey=[인증키]&domain=[도메인]&output=[리턴타입]&epsg=[좌표계]&callback=[func]
	// 도로명주소 -> 좌표
	// http://apis.vworld.kr/new2coord.do?q=[도로명주소]&apiKey=[인증키]&domain=[도메인]&output=[리턴타입]&epsg=[좌표계]&callback=[func]


	@Override
	public DataMap<String,Object> geocode(GeoLocation addr) throws GeocodeException
	{
		// 주소 --> 좌표
		HttpClient aHttpClient = null;

		try
		{
			String strURL = null;
			//String addressKey = null;

			DataMap<String,Object> locAttr = addr.getAttributes();
			String addressType = locAttr.getString("addr_tp", "");
			String encoding = locAttr.getString(KEY_ENCODING,"UTF-8");

			if (addressType.equals("jibun"))
			{
				// 지번주소 -> 좌표
				strURL = String.format("http://apis.vworld.kr/jibun2coord.do?apiKey=%s&q=%s&domain=%s&output=json&epsg=EPSG:4326"
						, m_mapAPIKey
						, URLEncoder.encode(TextHelper.nvl(addr.getAddress(), ""), encoding)
						, locAttr.getString("domain")
						);
				// {"EPSG_4326_X" : "126.878715", "EPSG_4326_Y" : "37.47875", "JUSO" : "서울특별시 금천구 가산동 426-5 한울시스템"}
			}
			else if (addressType.equals("doro"))
			{
				// 도로명주소 -> 좌표
				strURL = String.format("http://apis.vworld.kr/new2coord.do?apiKey=%s&q=%s&domain=%s&output=json&epsg=EPSG:4326"
						, m_mapAPIKey
						, URLEncoder.encode(TextHelper.nvl(addr.getAddress(), ""), encoding)
						, locAttr.getString("domain")
						);
				// {"EPSG_4326_X" : "126.878898491", "EPSG_4326_Y" : "37.481918319", "JUSO" : "서울특별시 금천구 가산디지털2로 156 대한통운 가산동 택배터널"}
			}
			else
			{
				throw new IllegalArgumentException(String.format("unsupported addr_tp: %s", addressType));
			}

			aHttpClient = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(strURL);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);
			if (theLogger.isDebugEnabled()) theLogger.debug("===> responseString: {}", responseString);

			org.json.JSONObject response = new org.json.JSONObject( responseString );

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (response.has("EPSG_4326_X") && response.has("EPSG_4326_Y"))
			{

				resultData.setString(KEY_RESPONSE_CODE, "ok");
				resultData.setString(KEY_RESPONSE_MESSAGE, "success");
				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, 1);
				resultData.setInt(KEY_TOTAL_COUNT, 1);


				List<GeoLocation> aList = new LinkedList<GeoLocation>();
				GeoLocation aLocation = new GeoLocation();
				aLocation.setAddress(addr.getAddress());
				aLocation.setLatitude(TextHelper.parseFloat(response.getString("EPSG_4326_Y")));
				aLocation.setLongitude(TextHelper.parseFloat(response.getString("EPSG_4326_X")));
				aLocation.setAttribute("address", response.getString("JUSO"));

				aList.add(aLocation);
				resultData.setObject(KEY_LOCATION_LIST, aList);

				return resultData;
			}

			resultData.setString(KEY_RESPONSE_CODE, "failed");
			resultData.setString(KEY_RESPONSE_MESSAGE, "failed");
			resultData.setInt(KEY_REQUEST_PAGE, 1);
			resultData.setInt(KEY_RESPONSE_COUNT, 0);
			resultData.setInt(KEY_TOTAL_COUNT, 0);

			return resultData;
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}


	// 좌표 -> 지번주소
	// http://apis.vworld.kr/coord2jibun.do?x=[X좌표]&y=[Y좌표]&apiKey=[인증키]&domain=[도메인]&output=[리턴타입]&epsg=[좌표계]&callback=[func]
	// 좌표 -> 도로명주소
	// http://apis.vworld.kr/coord2new.do?x=[X좌표]&y=[Y좌표]&apiKey=[인증키]&domain=[도메인]&output=[리턴타입]&epsg=[좌표계]&callback=[func]

	@Override
	public DataMap<String,Object> reverseGeocode(GeoLocation coord) throws GeocodeException
	{
		// 좌표 --> 주소
		HttpClient aHttpClient = null;

		try
		{
			DataMap<String,Object> locAttr = coord.getAttributes();

			String strURL = null;
			String addressKey = null;
			String addressType = locAttr.getString("addr_tp", "");

			if (addressType.equals("jibun"))
			{
				// 좌표 -> 지번주소
				addressKey = "ADDR";
				strURL = String.format("http://apis.vworld.kr/coord2jibun.do?apiKey=%s&x=%f&y=%f&domain=%s&output=json&epsg=EPSG:4326"
						, m_mapAPIKey
						, coord.getLongitude()
						, coord.getLatitude()
						, locAttr.getString("domain")
						);
			}
			else if (addressType.equals("doro"))
			{
				// 좌표 -> 도로명주소
				addressKey = "NEW_JUSO";
				strURL = String.format("http://apis.vworld.kr/coord2new.do?apiKey=%s&x=%f&y=%f&domain=%s&output=json&epsg=EPSG:4326"
						, m_mapAPIKey
						, coord.getLongitude()
						, coord.getLatitude()
						, locAttr.getString("domain")
						);
			}
			else
			{
				throw new IllegalArgumentException(String.format("unsupported addr_tp: %s", addressType));
			}

			aHttpClient = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(strURL);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);
			if (theLogger.isDebugEnabled()) theLogger.debug("===> responseString: {}", responseString);

			//JSONObject response = (JSONObject) JSONSerializer.toJSON( responseString );
			org.json.JSONObject response = new org.json.JSONObject( responseString );

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (response.has(addressKey))
			{
				resultData.setString(KEY_RESPONSE_CODE, "ok");
				resultData.setString(KEY_RESPONSE_MESSAGE, "success");

				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, 1);
				resultData.setInt(KEY_TOTAL_COUNT, 1);

				List<GeoLocation> aList = new LinkedList<GeoLocation>();

				GeoLocation aLocation = new GeoLocation();
				aLocation.setAddress(response.getString(addressKey));
				aLocation.setLatitude(coord.getLatitude());
				aLocation.setLongitude(coord.getLongitude());

				aList.add(aLocation);
				resultData.setObject(KEY_LOCATION_LIST, aList);
			}
			else
			{
				resultData.setString(KEY_RESPONSE_CODE, "fail");
				resultData.setString(KEY_RESPONSE_MESSAGE, "fail");
				resultData.setInt(KEY_TOTAL_COUNT, 0);
			}

			return resultData;
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}
/*
WEB_INF_HOME=/Volumes/data/projects/SmartJM/development/web/smartjm/WebContent/WEB-INF
THIS_CLASSPATH=$WEB_INF_HOME/classes:$WEB_INF_HOME/lib/httpclient-4.0.3.jar:$WEB_INF_HOME/lib/httpcore-4.0.1.jar:$WEB_INF_HOME/lib/commons-logging-1.1.1.jar:$WEB_INF_HOME/lib/json-lib-2.3-jdk15.jar:$WEB_INF_HOME/lib/commons-lang-2.4.jar:$WEB_INF_HOME/lib/ezmorph-1.0.6.jar:$WEB_INF_HOME/lib/commons-collections-3.2.1.jar:$WEB_INF_HOME/lib/commons-beanutils-1.8.0.jar

java -cp $THIS_CLASSPATH com.fliconz.fm.common.geocode.DaumGeocoder
*/
	public static void main(String[] args) throws Exception
	{
		VWorldGeocoder geocoder = new VWorldGeocoder("767B7ADF-10BA-3D86-AB7E-02816B5B92E9");
		GeoLocation aLocation = new GeoLocation();
		DataMap<String,Object> resultData = null;


		System.out.println("=========================");
		System.out.println(geocoder.getClass().getName());


		aLocation.setAddress("서울시 금천구 가산동");
		aLocation.setAttribute("addr_tp", "jibun");
		aLocation.setAttribute("domain", "http://map.vworld.kr");
		resultData = geocoder.geocode(aLocation);
		System.out.println("=========================");
		System.out.println("geocode : "+ resultData.toString());




		aLocation.setLatitude(37.87015110F);
		aLocation.setLongitude(127.73687180F);
		aLocation.setAttribute("domain", "http://map.vworld.kr");
		aLocation.setAttribute("addr_tp", "jibun");

		resultData = geocoder.reverseGeocode(aLocation);

		System.out.println();
		System.out.println("=========================");
		System.out.println("reverse geocode : "+ resultData.toString());
	}
}
