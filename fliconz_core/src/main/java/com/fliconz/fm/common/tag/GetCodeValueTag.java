/*
* @(#)GetPropertyTag.java
 */
package com.fliconz.fm.common.tag;

import java.io.IOException;
import java.util.Map;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fliconz.fm.common.util.CodeHelper;
import com.fliconz.fm.common.util.MapMessageFormat;

public class GetCodeValueTag extends BodyTagSupport
{
	private Logger m_logger = LoggerFactory.getLogger(GetCodeValueTag.class);

	private String m_strPath = null;
	private String m_strKey = null;
	@SuppressWarnings("unused")
	private boolean m_isDeclare = true;

	private String m_strDefaultValue = null;
	//private Object[] m_arrArgs = null;
	private Map m_args = null;

	//##############################################################################################################################

	/**
	* Construct a <code>GetPropertyAsStringTag</code>.
	*/
	public GetCodeValueTag() {
		super();
	}


	//##############################################################################################################################


	/**
	* Release a value attribute.
	*
	* Called on a Tag handler to release state.
	* The page compiler guarantees that JSP page implementation objects will invoke this method on all tag handlers,
	* but there may be multiple invocations on <code>doStartTag</code> and <code>doEndTag</code> in between.
	*/
	public void release() {
		super.release();
		super.setId(null);

		m_strPath = null;
		m_strKey = null;
		m_strDefaultValue = null;
		m_isDeclare = true;
		//m_arrArgs = null;
		m_args = null;
	}

	public void setId(String id) {
		super.setId(id);
	}

	public void setPath(String path) {
		m_strPath = path;
	}

	public void setKey(String key) {
		m_strKey = key;
	}

	public final void setDefaultValue(String defaultValue) {
		m_strDefaultValue = defaultValue;
	}

	public void setDeclare(boolean isDeclare) {
		m_isDeclare = isDeclare;
	}

//	public void setArgs(String[] args) {
//		m_arrArgs = args;
//	}
//	public void setArgs(List args) {
//		m_arrArgs = TextHelper.toStringArray(args);
//	}

	public void setArgs(Map args)
	{
		m_args = args;
	}

	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{
		try
		{
			String strValue = CodeHelper.getValue(m_strPath, m_strKey, m_strDefaultValue);

			if (strValue == null) {
				if ( super.getId() != null ) {
					pageContext.removeAttribute( super.getId() );
				}
			}

			else
			{
//				if (m_arrArgs != null) {
//					String strMsg = String.format(strValue, m_arrArgs);
//					strValue = strMsg;
//				}
				if (m_args != null)
				{
					String msg = MapMessageFormat.format(strValue, m_args);
					strValue = msg;
				}

				if ( super.getId() == null ) {
					JspWriter jw = pageContext.getOut();
					jw.print(strValue);
				}
				else
				{
					pageContext.setAttribute(super.getId(), strValue);
				}
			}

			return SKIP_BODY;
		}
		catch (IOException e) {
			throw new JspTagException( e.getMessage() );
		}
	}

}
