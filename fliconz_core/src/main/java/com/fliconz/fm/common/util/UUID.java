package com.fliconz.fm.common.util;

public class UUID
{
	public static String randomUUID(int genLength)
	{
		String uuid = java.util.UUID.randomUUID().toString().replace("-", "");
		int len = uuid.length();

		if (len > genLength)
		{
			return uuid.substring(0, genLength);
		}
		else if (len < genLength)
		{
			while (len < genLength)
			{
				uuid = new StringBuilder()
					.append(uuid)
					.append(java.util.UUID.randomUUID().toString().replace("-", "")).toString();
				len = uuid.length();
			}

			uuid = uuid.substring(0, genLength);
		}

		return uuid;
	}
}
