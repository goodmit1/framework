/*
 * @(#)FileHelper.java
 */
package com.fliconz.fm.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.activation.FileTypeMap;


/**
 * <pre>
 * [개요]
 * 파일관련 유틸리티 클래스
 *
 * [상세설명]
 *
 *
 * [변경이력]
 * 2008.06.11::이경구::최초작성
 *
 *
 * @author 이경구
 * </pre>
 */
public class FileHelper extends TextHelper
{
	/**
	* line separator string.
	*
	* <code>System.getProperty(&quot;line.separator&quot);</code>.
	*/
	public final static String LINE_SEPARATOR = System.getProperty("line.separator");


	/**
	* Invalid file name charactors.
	*
	* &quot;\/:*?&quot;&lt;&gt&quot;
	*/
	public final static String INVALID_FILE_NAME = "\\/:*?\"<>";


	/**
	* Default Content-Type : &quot;application/octet-stream&quot;
	*/
	public final static String DEFAULT_CONTENT_TYPE = "application/octet-stream";
	//excel : application/vnd.ms-excel,	application/x-msexcel
	//power point : application/vnd.ms-powerpoint
	//ms word : application/msword


//	public final static String DEFAULT_BACKUP_TIME_PATTERN = "yyyyMMdd_HHmmss";


	//##############################################################################################################################

	/**
	* Construct a <code>FileHelper</code>
	*/
	protected FileHelper() {
		super();
	}


	//##############################################################################################################################

	public static String getExtension(File aFile) {
		return aFile.isDirectory()
			? null
			: getExtension(aFile.getName());
	}

	public static String getExtension(String aFile) {
		try {
			return ( aFile.substring( aFile.lastIndexOf(".")+1 ) ).toLowerCase();
		}
		catch (Exception e) {
			return null;
		}
	}


	//##############################################################################################################################

	public static String getContentType(File aFile) {
		return FileTypeMap.getDefaultFileTypeMap().getContentType(aFile);
	}

	public static String getContentType(String aFile) {
		return FileTypeMap.getDefaultFileTypeMap().getContentType(aFile);
	}

	public static boolean isValidFileName(String fileName) {
		if ( fileName == null || "".equals(fileName) ) {
			return false;
		}

		for (int i=0; i<fileName.length(); i++) {
			if ( INVALID_FILE_NAME.indexOf(fileName.charAt(i)) >= 0 ) {
				return false;
			}
		}

		return true;
	}

	public static boolean removeFile(String path)
	{
		File aFile = new File(path);
		return aFile.delete();
	}

	public static boolean createDirectory(String path, boolean createIntermediates)
	{
		File aFile = new File(path);
		return createIntermediates ? aFile.mkdirs() : aFile.mkdir();
	}

	//##############################################################################################################################

	/**
	 * 해당하는 경로에 디렉토리가 있는지 체크합니다.
	 *
	 * @param dirName
	 * @return boolean
	 * @throws Exception
	 */
	public static boolean isDirectory(String dirName) throws Exception {
		boolean ret = false;
		File file = null;

		try {
			file = new File(dirName);
			if (file.isDirectory())
				ret = true;
		} catch (Exception e) {
			throw e;
		} finally {
			file = null;
		}
		return ret;
	}

	/**
	 * 파라메터로 넘어온 경로에 디렉토리를 만듭니다. a/b/c 에서 a/b 가 없다면 a/b/를 만든 후 c를 만듭니다.
	 *
	 * @param dirName
	 * @return boolean
	 * @throws Exception
	 */
	public static boolean makeDirectory(String dirName) throws Exception {
		boolean ret = false;
		String dirs[] = null;
		File file = null;

		try {
			dirs = dirName.split("/");
			String tmp = "";

			for (int i = 0; i < dirs.length; i++) {
				tmp = tmp + dirs[i] + "/";
				if (!isDirectory(tmp)) {
					file = new File(tmp);
					file.mkdir();
				}
			}

			ret = true;

		} catch (Exception e) {
			throw e;
		} finally {
			dirs = null;
			file = null;
		}
		return ret;
	}

	/**
	 * 파라메터로 넘어온 디렉토리, 파일명에 파일 컨텐츠를 기록합니다.
	 *
	 * @param dirName
	 * @param fileName
	 * @param fileContent
	 * @param isAppend
	 * @return boolean
	 * @throws Exception
	 */
	public static boolean fileWrite(String dirName, String fileName, String fileContent, boolean isAppend) throws Exception {

		boolean ret = false;
		String dirName2 = dirName;
		FileOutputStream fos = null;
		PrintStream ps = null;

		try {
			if (makeDirectory(dirName2)) {
				try {
					fos = new FileOutputStream(dirName + "/" + fileName, isAppend);
					ps = new PrintStream(fos);
					ps.print(fileContent);
					ret = true;
				} catch (Exception e) {
					throw e;
				} finally {
					if (ps != null)
						ps.close();
					ps = null;
					if (fos != null)
						try {
							fos.close();
						} catch (IOException e) {
							throw e;
						}
					fos = null;
				}
			}
		} catch (Exception e) {
			throw e;
		}

		return ret;
	}

	/**
	 * 파라메터로 넘어온 파일명에 파일 컨텐츠를 기록합니다.
	 *
	 * @param fileName
	 * @param fileContent
	 * @param isAppend
	 * @return
	 * @throws Exception
	 */
	public static boolean fileWrite(String fileName, String fileContent, boolean isAppend)
			throws Exception {

		boolean ret = false;
		FileOutputStream fos = null;
		PrintStream ps = null;

		try {
			try {
				fos = new FileOutputStream(fileName, isAppend);
				ps = new PrintStream(fos);
				ps.print(fileContent);
				ret = true;
			} catch (Exception e) {
				throw e;
			} finally {
				if (ps != null)
					ps.close();
				ps = null;
				if (fos != null)
					try {
						fos.close();
					} catch (IOException e) {
						throw e;
					}
				fos = null;
			}
		} catch (Exception e) {
			throw e;
		}

		return ret;
	}

	/**
	 * fileWrite와 동일한 기능이지만 파일이 이미 존재하면 컨텐츠를 append 시킨다.
	 *
	 * @param dirName
	 * @param fileName
	 * @param content
	 * @param mode
	 *            "rw" 파일을 읽고 쓰는 용도로 오픈합니다. "rws" 파일을 읽고 쓰고 업데이트 하는 용도로 오픈.
	 *            "rwd" rws와 동일
	 * @return boolean
	 * @throws CoreException
	 */
	public static boolean fileAppendWrite(String dirName, String fileName,
			String content, String mode) throws Exception {

		boolean ret = false;
		FileChannel fc = null;

		if (mode.equals("r") || mode.equals("rw") || mode.equals("rws")
				|| mode.equals("rwd")) {
			try {
				fc = (new RandomAccessFile(dirName + "/" + fileName, mode))
						.getChannel();
				fc.position(fc.size());
				content = content + "\n";
				fc.write(ByteBuffer.wrap(content.getBytes()));
				ret = true;
			} catch (Exception e) {
				throw e;
			} finally {
				if (fc != null)
					try {
						fc.close();
					} catch (IOException e) {
						throw e;
					}

				fc = null;
			}
		} else {
			throw new Exception(
					"FileHandler.fileAppendWrite() error:\n you must type your mode like as 'rw','rwd','rws'");
		}
		return ret;
	}

	/**
	 * 해당하는 디렉토리의 파일을 읽습니다.
	 *
	 * @param dirName
	 * @param fileName
	 * @return String (파일 내용)
	 * @throws CoreException
	 */
	public static String fileLoad(String dirName, String fileName)
			throws Exception {
		FileInputStream fis = null;
		BufferedReader bs = null;
		String ret = null;
		Long.toString(1);
		// Map map = new HashMap();
		// map.put( )
		try {
			fis = new FileInputStream(dirName + "/" + fileName);
			bs = new BufferedReader(new InputStreamReader(fis));
			StringBuffer sb = new StringBuffer();

			for (String buf = null; (buf = bs.readLine()) != null;) {
				sb.append(buf + "\n");
			}

			ret = sb.toString().substring(0, sb.toString().length() - 1);

		} catch (Throwable e) {
			throw new Exception(e);
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (IOException e) {
					throw new Exception(e);
				}

			fis = null;
			if (bs != null)
				try {
					bs.close();
				} catch (IOException e) {
					throw e;
				}
			bs = null;
		}
		return ret;
	}


}
