package com.fliconz.fm.common.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MybatisHelper
{
	private static Logger m_logger = LoggerFactory.getLogger(MybatisHelper.class);

	public static boolean isEmpty(Object value, boolean isTrim)
	{
		//if (m_logger.isDebugEnabled()) m_logger.debug("isEmpty ==> [{}] / [{}]", new Object[] {value, String.valueOf(isTrim)});

		if (value == null) return true;

		if (value instanceof CharSequence )
		{
			if (isTrim)
			{
				return ((CharSequence)value).toString().trim().equals("");
			}
			return ((CharSequence)value).toString().equals("");
		}
		else if (value instanceof List)
		{
			return ((List)value).isEmpty();
		}
		else if (value instanceof Map)
		{
			return ((Map)value).isEmpty();
		}
		else if (value instanceof Object[])
		{
			return ((Object[])value).length == 0;
		}

		if (isTrim)
		{
			return value == null || value.toString().trim().equals("");
		}

		return value == null || value.toString().equals("");
	}
	public static boolean isEmpty(Object value)
	{
		return isEmpty(value, false);
	}

	public static boolean isNotEmpty(Object value, boolean isTrim)
	{
		return MybatisHelper.isEmpty(value, isTrim) == false;
	}
	public static boolean isNotEmpty(Object value)
	{
		return isNotEmpty(value, false);
	}



	public static boolean isNotNull(Object value)
	{
		return value != null;
	}
	public static boolean isNull(Object value)
	{
		return value == null;
	}
//


//	public static String splitElement(String value, String delim, int index)
//	{
////		Logger aLogger = LoggerFactory.getLogger(MybatisHelper.class);
//		String[] arr = TextHelper.split(value, delim);
////		if (aLogger.isDebugEnabled()) aLogger.debug("====> value=[{}], delim=[{}], index=[{}] : arr=[{}]", new Object[]{value, delim, index, arr});
//		if (arr == null || index < 0 || arr.length < index+1)
//		{
////			if (aLogger.isDebugEnabled()) aLogger.debug("====> aaa");
//			return null;
//		}
//
////		if (aLogger.isDebugEnabled()) aLogger.debug("====> bbb [{}]", arr[index]);
//		return arr[index];
//	}


	public static String getValue(DataMap<String,Object> param, String fieldName, boolean requireQuatation)
	{
		String strValue = param.getString(fieldName);
		if (strValue == null)
		{
			return null;
		}

		strValue = requireQuatation ? String.format("'%s'", strValue) : strValue;
		//System.out.println(String.format("====> %s", strValue));
		return strValue;
	}

	public static String getValue(DataMap<String,Object> param, String fieldName)
	{
		return getValue(param, fieldName, false);
	}






	public static boolean isListValue(Object value)
	{
		if (value == null)
		{
			return false;
		}
		if (value instanceof Collection)
		{
			return true;
		}
		else if (value instanceof Object[])
		{
			return true;
		}
		return false;
	}
	public static boolean isNotListValue(Object value)
	{
		return MybatisHelper.isListValue(value) == false;
	}


	public static boolean isNotEmptyField(Object value, String fieldName)
	{
		if (value == null) return false;

		if (value instanceof Map)
		{
			return ((Map)value).containsKey(fieldName);
		}
		return false;
	}
	public static boolean isEmptyField(Object value, String fieldName) {
		return MybatisHelper.isNotEmptyField(value, fieldName) == false;
	}

}
