/*
 * @(#)TextHelper.java
 */
package com.fliconz.fm.common.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.JSONException;


public class TextHelper
{
	/**
	* White space string.
	*
	* <pre><code>
	* - Space : (char)32, ' '
	* - Horizontal Tab : (char)9, '\t'
	* - Carriage Return : (char)13, '\r'
	* - Line Feed, New Line : (char)10, '\n'
	* - Form Feed, New Page : (char)12, '\f'
	* - Null : (char)0
	* - Backspace : (char)8</code></pre>
	*/
	public final static String WHITE_SPACE = new String( new char[] { ' ', '\t', '\r', '\n', '\f', (char)0, (char)8 } );


	/**
	* base character set name : &quot;ISO-8859-1&quot;
	*/
	public final static String BASE_CHAR_SET = "ISO-8859-1";


	/**
	* default array delimeter
	*/
	public final static String DEFAULT_ARRAY_DELIM = ",";


	//##############################################################################################################################

	protected TextHelper() {

	}


	//##############################################################################################################################


	public static int compareText(String str1, String str2) {
		return compareText(str1, str2, false);
	}


	public static int compareText(String str1, String str2, boolean toLowerCase) {
		if (str1 == null && str2 == null) {
			return 0;
		}

		else if (str1 == null && str2 != null) {
			return 1;
		}

		else if (str1 != null && str2 == null) {
			return -1;
		}

		return toLowerCase
			? str1.compareTo(str2)
			: str1.toLowerCase().compareTo( str2.toLowerCase() );
	}



	public static int compareNo(double no1, double no2) {
		if (no1 > no2) {
			return 1;
		}

		else if (no1 < no2) {
			return -1;
		}

		else {
			return 0;
		}
	}



	public static int compareNo(String no1, String no2) {
		if (no1 == null || no2 == null) {
			return -1;
		}

		double dNo1 = 0.0;
		double dNo2 = 0.0;

		try {
			dNo1 = Double.parseDouble(no1);
		}
		catch (Throwable e) {
			return -1;
		}


		try {
			dNo2 = Double.parseDouble(no2);
		}
		catch (Throwable e) {
			return 1;
		}

		return compareNo(dNo1, dNo2);
	}


	public static int byteLength(String value) {
		if (value == null) {
			return 0;
		}

		return value.getBytes().length;
	}

	//##############################################################################################################################

	/**
	* Returns a string, with leading and trailing whitespace omitted of the both side.
	* If the <code>value</code> is <code>null</code>, returns the empty string (&quot;&quot;).
	*
	* @param value a string
	* @return a string, with leading and trailing whitespace omitted of the both side.
	*/
	public static String trim(String value) {
		return value == null ? "" : value.trim();
	}

	/**
	* Returns a string, with leading and trailing whitespace omitted of the left side.
	* If the <code>value</code> is <code>null</code>, returns the empty string (&quot;&quot;).
	*
	* @param value a string
	* @return a string, with leading and trailing whitespace omitted of the left side.
	*/
	public static String ltrim(String value) {
		return ltrim(value, null);
	}

	/**
	* Returns a string, with leading and trailing whitespace omitted of the right side.
	* If the <code>value</code> is <code>null</code>, returns the empty string (&quot;&quot;).
	*
	* @param value a string
	* @return a string, with leading and trailing whitespace omitted of the right side.
	*/
	public static String rtrim(String value) {
		return rtrim(value, null);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------


	public static String trim(String value, String trimChars) {
		if (trimChars == null) {
			return value == null ? "" : value.trim();
		}

		return rtrim( ltrim(value, trimChars), trimChars );
	}


	public static String ltrim(String value, String trimChars) {
		if (value == null) {
			return "";
		}

		String strTrimChars = trimChars == null ? WHITE_SPACE : trimChars;
		for (int i=0; i<value.length(); i++) {
			if ( strTrimChars.indexOf( value.charAt(i) ) == -1 ) {
				return value.substring(i);
			}
		}

		return "";
	}


	public static String rtrim(String value, String trimChars) {
		if (value == null) {
			return "";
		}

		String strTrimChars = trimChars == null ? WHITE_SPACE : trimChars;
		for (int i=value.length()-1; i>=0; i--) {
			if ( strTrimChars.indexOf( value.charAt(i) ) == -1 ) {
				return value.substring(0, i);
			}
		}

		return "";
	}



	//##############################################################################################################################

	/**
	* If the <code>value</code> is <code>null</code>, returns the <code>defaultValue</code> otherwise return <code>value</code>.
	* Null 인경우 대체 String 을 사용.
	* @param value a string
	* @param defaultValue a string.
	* @return the value or <code>defaultValue</code>.
	*/
	public static String nvl(String value, String defaultValue) {
		return value == null
			? defaultValue
			: value;
	}

	/**
	* If the <code>value</code> is <code>null</code> or trimmed <code>value</code> is empty string (&quot;&quot;), return the <code>defaultValue</code>;
	* otherwise return the <code>value</code>.
	* empty String의 경우 디폴트로 치환.
	* @param value a string
	* @param defaultValue a string.
	* @return the <code>value</code> or <code>defaultValue</code>.
	*/
	public static String evl(String value, String defaultValue) {
		return isEmpty(value)
			? defaultValue
			: value;
	}


	public static boolean isEmpty(String value) {
		return value == null || value.trim().length() == 0;
	}

	public static boolean isNotEmpty(String value) {
		return TextHelper.isEmpty(value) == false;
	}
	//##############################################################################################################################

	/**
	* Returns the <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a string value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a left-padded string.
	*/
	public static String lpad(String value, int len, String padding) {
		StringBuilder aBuffer = new StringBuilder();
		padding(aBuffer, len - (value == null ? 0 : value.length()), padding);
		return aBuffer.append(value == null ? "" : value).toString();
	}

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a <code>int</code> value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a left-padded string.
	*/
	public static String lpad(int value, int len, String padding) {
		return lpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a <code>long</code> value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a left-padded string.
	*/
	public static String lpad(long value, int len, String padding) {
		return lpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a <code>float</code> value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a left-padded string.
	*/
	public static String lpad(float value, int len, String padding) {
		return lpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a <code>double</code> value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a left-padded string.
	*/
	public static String lpad(double value, int len, String padding) {
		return lpad( String.valueOf(value), len, padding);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> character.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a string value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a left-padded string.
	*/
	public static String lpad(String value, int len, char padding) {
		return lpad(value, len, String.valueOf(padding));
	}

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> character.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>int</code> value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a left-padded string.
	*/
	public static String lpad(int value, int len, char padding) {
		return lpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> character.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>long</code> value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a left-padded string.
	*/
	public static String lpad(long value, int len, char padding) {
		return lpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> character.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>float</code> value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a left-padded string.
	*/
	public static String lpad(float value, int len, char padding) {
		return lpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the string <code>value</code>, left-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> character.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>double</code> value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a left-padded string.
	*/
	public static String lpad(double value, int len, char padding) {
		return lpad( String.valueOf(value), len, padding);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	* Returns the <code>value</code>, left-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a string value.
	* @param len a padding length.
	* @return a left-padded string.
	*/
	public static String lpad(String value, int len) {
		return lpad(value, len, null);
	}

	/**
	* Returns the <code>value</code>, left-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>int</code> value.
	* @param len a padding length.
	* @return a left-padded string.
	*/
	public static String lpad(int value, int len) {
		return lpad(String.valueOf(value), len, null);
	}

	/**
	* Returns the <code>value</code>, left-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>long</code> value.
	* @param len a padding length.
	* @return a left-padded string.
	*/
	public static String lpad(long value, int len) {
		return lpad(String.valueOf(value), len, null);
	}

	/**
	* Returns the <code>value</code>, left-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>float</code> value.
	* @param len a padding length.
	* @return a left-padded string.
	*/
	public static String lpad(float value, int len) {
		return lpad(String.valueOf(value), len, null);
	}

	/**
	* Returns the <code>value</code>, left-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>double</code> value.
	* @param len a padding length.
	* @return a left-padded string.
	*/
	public static String lpad(double value, int len) {
		return lpad(String.valueOf(value), len, null);
	}


	//##############################################################################################################################

	private static void padding(StringBuilder sb, int len, String padding) {
		if (len <=0 ) {
			return;
		}
		if ( padding == null || "".equals(padding) ) {
			padding = " ";
		}

			int intBufferLen = 0;
		while (true) {
			sb.append(padding);
			intBufferLen = sb.length();

			if (intBufferLen == len) {
				break;
			}
			else if (intBufferLen > len) {
				sb.setLength(len);
				break;
			}
		}
	}


	//##############################################################################################################################

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a string value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a right-padded string.
	*/
	public static String rpad(String value, int len, String padding) {
		StringBuilder aBuffer = new StringBuilder();
		padding(aBuffer, len - (value == null ? 0 : value.length()), padding);

		return aBuffer.insert(0, value == null ? "" : value).toString();
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a <code>int</code> value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a right-padded string.
	*/
	public static String rpad(int value, int len, String padding) {
		return rpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a <code>long</code> value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a right-padded string.
	*/
	public static String rpad(long value, int len, String padding) {
		return rpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a <code>float</code> value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a right-padded string.
	*/
	public static String rpad(float value, int len, String padding) {
		return rpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	* If the <code>padding</code> value is <code>null</code> or empty string (&quot;&quot;), the <code>padding</code> defaults to &quot; &quot;, a single blank.
	*
	* @param value a <code>double</code> value.
	* @param len a padding length.
	* @param padding a padding string.
	* @return a right-padded string.
	*/
	public static String rpad(double value, int len, String padding) {
		return rpad( String.valueOf(value), len, padding);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a string value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a right-padded string.
	*/
	public static String rpad(String value, int len, char padding) {
		return rpad(value, len, String.valueOf(padding));
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>int</code> value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a right-padded string.
	*/
	public static String rpad(int value, int len, char padding) {
		return rpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>long</code> value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a right-padded string.
	*/
	public static String rpad(long value, int len, char padding) {
		return rpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>float</code> value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a right-padded string.
	*/
	public static String rpad(float value, int len, char padding) {
		return rpad( String.valueOf(value), len, padding);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of string in the <code>padding</code> string.
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>double</code> value.
	* @param len a padding length.
	* @param padding a padding character.
	* @return a right-padded string.
	*/
	public static String rpad(double value, int len, char padding) {
		return rpad( String.valueOf(value), len, padding);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a string value.
	* @param len a padding length.
	* @return a right-padded string.
	*/
	public static String rpad(String value, int len) {
		return rpad(value, len, null);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>int</code> value.
	* @param len a padding length.
	* @return a right-padded string.
	*/
	public static String rpad(int value, int len) {
		return rpad(String.valueOf(value), len, null);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>long</code> value.
	* @param len a padding length.
	* @return a right-padded string.
	*/
	public static String rpad(long value, int len) {
		return rpad(String.valueOf(value), len, null);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>float</code> value.
	* @param len a padding length.
	* @return a right-padded string.
	*/
	public static String rpad(float value, int len) {
		return rpad(String.valueOf(value), len, null);
	}

	/**
	* Returns the <code>value</code>, right-padded to length the <code>len</code> with the sequence of single blank string (&quot; &quot;).
	*
	* If the <code>value</code> length is longer than the <code>len</code>, this method returns the <code>value</code>.
	*
	* @param value a <code>double</code> value.
	* @param len a padding length.
	* @return a right-padded string.
	*/
	public static String rpad(double value, int len) {
		return rpad(String.valueOf(value), len, null);
	}



	//##############################################################################################################################


	public static List<String> splitList(String value, String delim, boolean emptyToNull)
	{
		List<String> resultList = null;
		if (value == null)
		{
			if (delim != null)
			{
				resultList = new LinkedList<String>();
			}
			return resultList;
		}

		if (delim == null || "".equals(delim))
		{
			delim = DEFAULT_ARRAY_DELIM;
		}

		if ( "".equals(value) )
		{
			resultList = new LinkedList<String>();
			resultList.add(emptyToNull ? null : "");
			return resultList;
		}

		String strValue = value;
		if (strValue.endsWith(delim) == false) {
			strValue = new StringBuilder().append(strValue).append(delim).toString();
		}

		int intTo = 0;
		int intFrom = 0;
		int intDelimLen = delim.length();
		resultList = new LinkedList<String>();

		String strToken = null;
		while ((intTo = strValue.indexOf(delim, intFrom)) >= 0)
		{
			strToken = strValue.substring(intFrom, intTo);
			resultList.add("".equals(strToken) && emptyToNull ? null : strToken);
			intFrom = intTo + intDelimLen;
		}

		return resultList;
	}


	public static List<String> splitList(String value, String delim) {
		return splitList(value, delim, true);
	}


	public static List<String> splitList(String value, boolean emptyToNull) {
		return splitList(value, (String)null, emptyToNull);
	}


	public static List<String> splitList(String value) {
		return splitList(value, (String)null, true);
	}

	public  static int indexOf(String[] arr, String val)
	{
		int idx = 0;
		for(String a:arr)
		{
			if(a.equals(val)) return idx;
			idx++;
		}
		return -1;

	}
	//##############################################################################################################################

	public static String[] split(String value, String delim, boolean emptyToNull) {
		if (value == null) {
			return delim == null ? null : new String[]{};
		}

		if (delim == null || "".equals(delim)) {
			delim = DEFAULT_ARRAY_DELIM;
		}

		if ( "".equals(value) ) {
			return new String[]{emptyToNull ? null : ""};
		}

		String strValue = value;
		//if (strValue.endsWith(delim) == false) {
			strValue = new StringBuilder().append(strValue).append(delim).toString();
		//}

		int intTo = 0;
		int intFrom = 0;
		int intDelimLen = delim.length();
		LinkedList<String> lstResult = new LinkedList<String>();

			String strToken = null;
		while ((intTo = strValue.indexOf(delim, intFrom)) >= 0) {
			strToken = strValue.substring(intFrom, intTo);
			lstResult.add("".equals(strToken) && emptyToNull ? null : strToken);
			intFrom = intTo + intDelimLen;
		}

		String[] arrResult = new String[lstResult.size()];
		lstResult.toArray(arrResult);

		return arrResult;
	}


	public static String[] split(String value, String delim) {
		return split(value, delim, true);
	}


	public static String[] split(String value, boolean emptyToNull) {
		return split(value, (String)null, emptyToNull);
	}


	public static String[] split(String value) {
		return split(value, (String)null, true);
	}


	//##############################################################################################################################
	/**
	 * delimiter를 없애고 문자열을 합쳐서 만들어줌.
	 *
	 */
	public static String join(String[] arr, String delim, boolean nullToEmpty) {
		if (arr == null || arr.length == 0) {
			return null;
		}

		if ( delim == null || "".equals(delim) ) {
			delim = DEFAULT_ARRAY_DELIM;
		}

		int intCnt = 0;
		StringBuilder sb = new StringBuilder();

		for (int i=0; i<arr.length; i++) {
			if (intCnt>0) {
				sb.append(delim);
			}
			if (arr[i] == null) {
				sb.append(nullToEmpty ? "" : "null");
			}
			else {
				sb.append(arr[i]);
			}
			intCnt++;
		}

		return sb.toString();
	}


	public static String join(String[] arr, String delim) {
		return join(arr, delim, false);
	}


	public static String join(String[] arr, boolean nullToEmpty) {
		return join(arr, (String)null, nullToEmpty);
	}


	public static String join(String[] arr) {
		return join(arr, (String)null, false);
	}


	public static String join(int[] arr, String delim) {
		if (arr == null || arr.length == 0) {
			return null;
		}

		if ( delim == null || "".equals(delim) ) {
			delim = DEFAULT_ARRAY_DELIM;
		}

		int intCnt = 0;
		StringBuilder sb = new StringBuilder();

		for (int i=0; i<arr.length; i++) {
			if (intCnt>0) {
				sb.append(delim);
			}
			sb.append(arr[i]);
			intCnt++;
		}

		return sb.toString();
	}

	public static String join(int[] arr) {
		return join(arr, null);
	}

	//##############################################################################################################################

	/**
	* Returns a new string resulting from replacing all occurrences of the <code>oldString</code> in this string with the <code>newString</code>.
	*
	* @param value a string.
	* @param oldString the old string.
	* @param newString the new string.
	* @return a string derived from this string by replacing every occurrence of <code>oldString</code> with <code>newString</code>.
	*/
	public static String replace(String value, String oldString, String newString) {
		int intFrom = 0;
		StringBuilder sb = new StringBuilder();
		int intTo = 0;
		do {
			intTo = value.indexOf(oldString, intFrom);
			if(intTo >= 0) {
				sb.append(value.substring(intFrom, intTo));
				sb.append(newString);
				intFrom = intTo + oldString.length();
			}
			else {
				sb.append(value.substring(intFrom));
				return sb.toString();
			}
		}
		while(true);
	}

//	public static String removeChar(String value, char removeChar)
//	{
//		if (value == null) {
//			return value;
//		}
//
//		int intFrom = 0;
//		int intIndex = 0;
//		StringBuilder sb = new StringBuilder();
//
//		while ( (intIndex = value.indexOf(removeChar, intFrom)) >= 0 ) {
//			sb.append( value.substring(intFrom, intIndex));
//			intFrom = intIndex+1;
//		}
//
//		if (intFrom > 0 && value.length() != intFrom) {
//			sb.append( value.substring(intFrom) );
//		}
//
//		return sb.toString();
//	}
//
//
//	public static String removeChar(String value, String removeChar)
//	{
//		if (value == null || removeChar == null || removeChar.length() == 0)
//		{
//			return value;
//		}
//
//		char[] arrChar = removeChar.toCharArray();
//		String strValue = value;
//
//		for (int i=0; i<arrChar.length; i++)
//		{
//			strValue = removeChar(strValue, arrChar[i]);
//		}
//
//		return strValue;
//	}



	//##############################################################################################################################

	/**
	* Returns a boolean with a value represented by the specified string.
	*
	* The boolean returned represents the value <code>true</code>,
	* if the <code>value</code> is not <code>null</code> and is equal, ignoring case, to the string "<code>true</code>".
	* If the <code>value</code> is <code>null</code>, returns the <code>defaultValue</code>.
	*
	* @param value a string to be parsed.
	* @param defaultValue
	* @return parsed <code>boolean</code> value or <code>defaultValue</code>.
	*/
	public static boolean parseBoolean(String value, boolean defaultValue) {
		return value == null ? defaultValue : Boolean.valueOf(value).booleanValue();
	}

	/**
	* Returns a boolean with a value represented by the specified string.
	*
	* The boolean returned represents the value <code>true</code>,
	* if the <code>value</code> is not <code>null</code> and is equal, ignoring case, to the string "<code>true</code>".
	* If the <code>value</code> is <code>null</code>, return <code>false</code>.
	*
	* @param value a string to be parsed.
	* @return parsed <code>boolean</code> value.
	*/
	public static boolean parseBoolean(String value) {
		return parseBoolean(value, false);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	* Parses the <code>value</code> as a signed decimal integer.
	*
	* The characters in the string must all be decimal digits,
	* except that the first character may be an ASCII minus sign '-' ('\u002D') to indicate a negative value.
	*
	* If the <code>value</code> is <code>null</code>, returns the <code>defaultValue</code>.
	*
	* @param value a string to be parsed.
	* @param defaultValue
	* @return parsed <code>int</code> value.
	*/
	public static int parseInt(String value, int defaultValue) {
//		try {
//			return value == null
//				? defaultValue
//				: NumberFormat.getInstance().parse(value).intValue();
//		}
//		catch (Throwable e) {
//			return defaultValue;
//		}
		try
		{
			return value == null ? defaultValue : (int)Double.parseDouble(value);
		}
		catch(Throwable e)
		{
			return defaultValue;
		}
	}

	/**
	* Parses the <code>value</code> as a signed decimal integer.
	*
	* The characters in the string must all be decimal digits,
	* except that the first character may be an ASCII minus sign '-' ('\u002D') to indicate a negative value.
	*
	* If the <code>value</code> is <code>null</code>, returns <code>0</code>.
	*
	* @param value a string to be parsed.
	* @return parsed <code>int</code> value.
	*/
	public static int parseInt(String value) {
		return parseInt(value, 0);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	* Parses the <code>value</code> as a signed decimal <code>long</code>.
	* The characters in the string must all be decimal digits,
	* except that the first character may be an ASCII minus sign '-' (\u002D') to indicate a negative value.
	*
	* If the <code>value</code> is <code>null</code>, returns the <code>defaultValue</code>.
	*
	* @param value a string to be parsed.
	* @param defaultValue
	* @return parsed <code>long</code> value.
	*/
	public static long parseLong(String value, long defaultValue) {
//		try {
//			return value == null
//				? defaultValue
//				: NumberFormat.getInstance().parse(value).longValue();
//		}
//		catch (Throwable e) {
//			return defaultValue;
//		}

		try
		{
			return value == null ? defaultValue : (long)Double.parseDouble(value);
		}
		catch(Throwable e)
		{
			return defaultValue;
		}
	}

	/**
	* Parses the <code>value</code> as a signed decimal <code>long</code>.
	* The characters in the string must all be decimal digits,
	* except that the first character may be an ASCII minus sign '-' (\u002D') to indicate a negative value.
	*
	* If the <code>value</code> is <code>null</code>, returns <code>0L</code>.
	*
	* @param value a string to be parsed.
	* @return parsed <code>long</code> value.
	*/
	public static long parseLong(String value) {
		return parseLong(value, 0L);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	* Parses the <code>value</code> as a signed decimal <code>float</code>.
	* The characters in the string must all be decimal digits,
	* except that the first character may be an ASCII minus sign '-' (\u002D') to indicate a negative value.
	*
	* If the <code>value</code> is <code>null</code>, returns the <code>defaultValue</code>.
	*
	* @param value a string to be parsed.
	* @param defaultValue
	* @return parsed <code>float</code> value.
	*/
	public static float parseFloat(String value, float defaultValue) {
//		try {
//			return value == null
//				? defaultValue
//				: NumberFormat.getInstance().parse(value).floatValue();
//		}
//		catch (Throwable e) {
//			return defaultValue;
//		}
		try
		{
			return value == null ? defaultValue : (float)Double.parseDouble(value);
		}
		catch(Throwable e)
		{
			return defaultValue;
		}
	}

	/**
	* Parses the <code>value</code> as a signed decimal <code>float</code>.
	* The characters in the string must all be decimal digits,
	* except that the first character may be an ASCII minus sign '-' (\u002D') to indicate a negative value.
	*
	* If the <code>value</code> is <code>null</code>, returns <code>0.0F</code>.
	*
	* @param value a string to be parsed.
	* @return parsed <code>float</code> value.
	*/
	public static float parseFloat(String value) {
		return parseFloat(value, 0.0F);
	}


	//---------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	* Parses the <code>value</code> as a signed decimal <code>double</code>.
	* The characters in the string must all be decimal digits,
	* except that the first character may be an ASCII minus sign '-' (\u002D') to indicate a negative value.
	*
	* If the <code>value</code> is <code>null</code>, returns the <code>defaultValue</code>.
	*
	* @param value a string to be parsed.
	* @param defaultValue
	* @return parsed <code>double</code> value.
	*/
	public static double parseDouble(String value, double defaultValue) {
//		try {
//			return value == null
//				? defaultValue
//				: NumberFormat.getInstance().parse(value).doubleValue();
//		}
//		catch (Throwable e) {
//			return defaultValue;
//		}
		try
		{
			return value == null ? defaultValue : Double.parseDouble(value);
		}
		catch(Throwable e)
		{
			return defaultValue;
		}
	}

	/**
	* Parses the <code>value</code> as a signed decimal <code>double</code>.
	* The characters in the string must all be decimal digits,
	* except that the first character may be an ASCII minus sign '-' (\u002D') to indicate a negative value.
	*
	* If the <code>value</code> is <code>null</code>, returns <code>0.0</code>.
	*
	* @param value a string to be parsed.
	* @return parsed <code>double</code> value.
	*/
	public static double parseDouble(String value) {
		return parseDouble(value, 0.0);
	}


	//##############################################################################################################################

	public static int maxValue(int no1, int no2)
	{
		if (no1 > no2)
		{
			return no1;
		}

		return no2;
	}

	public static int minValue(int no1, int no2)
	{
		if (no1 < no2)
		{
			return no1;
		}

		return no2;
	}



	public static boolean contains(int value, int... compareValue)
	{
		for(int c : compareValue)
		{
			if (value == c)
			{
				return true;
			}
		}
		return false;
	}

	public static boolean notContains(int value, int... compareValue)
	{
		for(int c : compareValue)
		{
			if (value == c)
			{
				return false;
			}
		}
		return true;
	}



//	public static boolean contains(Object value, Object... compareValue)
//	{
//		if (compareValue == null || compareValue.length <= 0)
//		{
//			return false;
//		}
//
//		for(Object c : compareValue)
//		{
//			if (value == null && c == null)
//			{
//				return true;
//			}
//			else if (value == null && c != null)
//			{
//				continue;
//			}
//			else if (value != null && c == null)
//			{
//				continue;
//			}
//			else if (value.equals(c))
//			{
//				return true;
//			}
//		}
//		return false;
//	}
//
//	public static boolean notContains(Object value, Object... compareValue)
//	{
//		if (compareValue == null || compareValue.length <= 0)
//		{
//			return false;
//		}
//
//		for(Object c : compareValue)
//		{
//			if (value == null && c == null)
//			{
//				return false;
//			}
//			else if (value == null && c != null)
//			{
//				continue;
//			}
//			else if (value != null && c == null)
//			{
//				continue;
//			}
//			else if (value.equals(c))
//			{
//				return false;
//			}
//		}
//		return true;
//	}


	//##############################################################################################################################

	/**
	* Formats a <code>int</code> number to the specific format string.
	*
	* @param no <code>int</code> number.
	* @param pattern number format string.
	* @return formatted number string.
	*/
	public static String format(int no, String pattern) {
		return new DecimalFormat(pattern).format(no);
	}

	/**
	* Formats a <code>long</code> number to the specific format string.
	*
	* @param no <code>long</code> number.
	* @param pattern number format string.
	* @return formatted number string.
	*/
	public static String format(long no, String pattern) {
		return new DecimalFormat(pattern).format(no);
	}

	/**
	* Formats a <code>float</code> number to the specific format string.
	*
	* @param no <code>float</code> number.
	* @param pattern number format string.
	* @return formatted number string.
	*/
	public static String format(float no, String pattern) {
		return new DecimalFormat(pattern).format(no);
	}

	/**
	* Formats a <code>double</code> number to the specific format string.
	*
	* @param no <code>double</code> number.
	* @param pattern number format string.
	* @return formatted number string.
	*/
	public static String format(double no, String pattern) {
		return new DecimalFormat(pattern).format(no);
	}

	/**
	 * 일정길이가 넘으면 잘라내고, 특정문자(truncText)를 붙여줌. (일정 자리수 이상시 ... 붙여주는 방식)
	 * */
	public static String trunc(String value, int trucLength, String truncText) {
		if (value == null || value.length() <= trucLength) {
			return value;
		}

		return truncText == null
			? new StringBuilder().append(value.substring(0, trucLength)).toString()
			: new StringBuilder().append(value.substring(0, trucLength)).append(truncText).toString();
	}

	//##############################################################################################################################

	/**
	 * premitive type을 포함한 문자열 클래스 이름에 대한 클래스를 로딩한다.
	 */
	public static Class loadClass(String className) throws ClassNotFoundException {
		if ( "boolean".equals(className) ) {
			return Boolean.TYPE;
		}

		else if ( "byte".equals(className) ) {
			return Byte.TYPE;
		}

		else if ( "char".equals(className) ){
			return Character.TYPE;
		}

		else if ( "double".equals(className) ){
			return Double.TYPE;
		}

		else if ( "float".equals(className) ){
			return Float.TYPE;
		}

		else if ( "int".equals(className) ){
			return Integer.TYPE;
		}

		else if ( "long".equals(className) ){
			return Long.TYPE;
		}

		else if ( "short".equals(className) ){
			return Short.TYPE;
		}

		return Class.forName( parseClassName(className) );
	}
	/**
	 * 문자열 클래스명에 대한 클래스명을 얻는다.
	 * 파라미터의 클래스 이름이 문자열 클래스인 경우 클래스를 로드할 수 있는 클래스 이름으로 변환한다.
	 * java.lang.Class.getName() 메소드 참고
	 *
	 * @param className
	 * @return
	 */
	public static String parseClassName(String className) {
		if ( "boolean".equals(className) ){
			return Boolean.TYPE.getName();
		}

		else if ( "byte".equals(className) ){
			return Byte.TYPE.getName();
		}

		else if ( "char".equals(className) ){
			return Character.TYPE.getName();
		}

		else if ( "double".equals(className) ){
			return Double.TYPE.getName();
		}

		else if ( "float".equals(className) ){
			return Float.TYPE.getName();
		}

		else if ( "int".equals(className) ){
			return Integer.TYPE.getName();
		}

		else if ( "long".equals(className) ){
			return Long.TYPE.getName();
		}

		else if ( "short".equals(className) ){
			return Short.TYPE.getName();
		}


		String strClassName = className;
		if ( className.indexOf('[') + 1 == className.indexOf(']') ){
			StringBuilder sb = new StringBuilder();
			strClassName = className.substring(className.indexOf('['));

			for (int i=0; i<strClassName.length(); i++) {
				if (strClassName.charAt(i) == '[') {
					sb.append("[");
				}
			}

			strClassName = className.substring(0, className.indexOf('['));

			if ( "boolean".equals(strClassName) ) {
				sb.append("Z");
			}

			else if ( "byte".equals(strClassName) ) {
				sb.append("B");
			}

			else if ( "char".equals(strClassName) ) {
				sb.append("C");
			}

			else if ( "double".equals(strClassName) ) {
				sb.append("D");
			}

			else if ( "float".equals(strClassName) ) {
				sb.append("F");
			}

			else if ( "int".equals(strClassName) ) {
				sb.append("I");
			}

			else if ( "long".equals(strClassName) ) {
				sb.append("J");
			}

			else if ( "short".equals(strClassName) ) {
				sb.append("S");
			}

			else {
				sb.append("L");
				sb.append(strClassName);
				sb.append(";");
			}

			strClassName = sb.toString();
		}

		return strClassName;
	}


	public static String getClassName(Class klass) {
		if ( !klass.isArray() ) {
			return klass.getName();
		}

		String strClassName = klass.getName();
		StringBuilder sb = new StringBuilder();
		int intIndex = 0;

		while (true) {
			if (strClassName.charAt(intIndex) == '[') {
				sb.append("[]");
				intIndex++;
			}

			else {
				break;
			}
		}

		char cPrefix = strClassName.charAt(intIndex);

		if (cPrefix == 'Z') {
			sb.insert(0, "boolean");
		}

		else if (cPrefix == 'B') {
			sb.insert(0, "byte");
		}

		else if (cPrefix == 'C') {
			sb.insert(0, "char");
		}

		else if (cPrefix == 'D') {
			sb.insert(0, "double");
		}

		else if (cPrefix == 'F') {
			sb.insert(0, "float");
		}

		else if (cPrefix == 'I') {
			sb.insert(0, "int");
		}

		else if (cPrefix == 'J') {
			sb.insert(0, "long");
		}

		else if (cPrefix == 'S') {
			sb.insert(0, "short");
		}

		//else if (cPrefix == 'L')

		else {
			sb.insert( 0, strClassName.substring(intIndex+1, strClassName.length()-1) );
		}

		return sb.toString();
	}


	public static String getDePackageClassName(Class klass) {
		String strClassName = getClassName(klass);
		int intLastDotPosition = strClassName.lastIndexOf('.');

		return intLastDotPosition == -1
			? strClassName
			: strClassName.substring(intLastDotPosition+1);
	}


	public static void toJavaMap(org.json.JSONObject obj, Map<String, Object> mm)
	{
		if (obj == null)
		{
			return;
		}

		for (Iterator it = obj.keys(); it.hasNext();)
		{
			String key = (String) it.next();
			Object aValue = null;
			try
			{
				aValue = obj.get(key);
			} catch (JSONException e)
			{
				continue;
			}

			if (aValue instanceof org.json.JSONObject)
			{
				Map<String, Object> aMap = new DataMap<String, Object>();
				TextHelper.toJavaMap((org.json.JSONObject) aValue, aMap);
				mm.put(key, aMap);
			}
			else if (aValue instanceof org.json.JSONArray)
			{
				List<Object> aList = new LinkedList<Object>();
				org.json.JSONArray arrayValue = (org.json.JSONArray) aValue;
				TextHelper.toJavaList(arrayValue, aList);
				mm.put(key, aList);
			}
			else
			{
				mm.put(key, aValue);
			}
		}
	}


	public static List<String> toList(String[] arr)
	{
		if (arr == null)
		{
			return null;
		}

		List<String> aList = new LinkedList<String>();
		for (int i=0; i<arr.length; i++)
		{
			aList.add(arr[i]);
		}
		return aList;
	}


	public static String[] toStringArray(List<Object> ll)
	{
		if (ll == null)
		{
			return null;
		}

		String[] arr = new String[ll.size()];
		for (int i=0; i<arr.length; i++)
		{
			arr[i] = ll.get(i) == null ? null : ll.get(i).toString();
		}

		return arr;
	}

	public static String convertHTMLText(String strHtml) {
		if (strHtml == null)
			return "";

		char[] arrStat = strHtml.toCharArray();
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arrStat.length; i++) {
			switch (arrStat[i]) {
			case '\n':
				sb.append("<br>");
				break;

			case '\r':
				sb.append("");
				break;

			case '\t':
				sb.append("&nbsp;&nbsp;&nbsp;");
				break;

			case '\"':
				sb.append("&quot;");
				break;

			case '<':
				sb.append("&lt;");
				break;

			case '>':
				sb.append("&gt;");
				break;

			case ' ':
				sb.append("&nbsp;");
				break;

			case '&':
				sb.append("&amp;");
				break;

			default:
				sb.append(arrStat[i]);
			}
		}

		return sb.toString();
	}

	public static String convertHTML(String strHtml) {
		if (strHtml == null)
			return "";

		char[] arrStat = strHtml.toCharArray();
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arrStat.length; i++) {
			switch (arrStat[i]) {
			case '\n':
				sb.append("<br>");
				break;

			case '\r':
				sb.append("");
				break;

			default:
				sb.append(arrStat[i]);
			}
		}

		return sb.toString();
	}

	/**
	* toJavaList converts JSON's array response into Java's List
	* @param ar
	* JSONArray to be converted to Java List
	* @param ll
	* Java List to hold the converted JSONArray response
	**/
	public static void toJavaList(org.json.JSONArray arrayValue, List<Object> ll)
	{
		if (arrayValue == null)
		{
			return;
		}

		//for (Iterator it = arrayValue.iterator(); it.hasNext();)
		for (int i=0; i<arrayValue.length(); i++)
		{
			//Object aValue = it.next();
			Object aValue = null;
			try {
				aValue = arrayValue.get(i);
			}
			catch (JSONException e)
			{
				continue;
			}

			if (aValue instanceof org.json.JSONObject)
			{
				Map<String, Object> aMap = new DataMap<String, Object>();
				TextHelper.toJavaMap((org.json.JSONObject) aValue, aMap);
				ll.add(aMap);
			}
			else if (aValue instanceof org.json.JSONArray)
			{
				List<Object> aList = new LinkedList<Object>();
				org.json.JSONArray aArray = (org.json.JSONArray) aValue;
				TextHelper.toJavaList(aArray, aList);
				ll.add(aList);
			}
			else
			{
				ll.add(aValue);
			}
		}
	}

	//2019.01.25:문자열 인코딩을 고려해서 BYTES단위 문자열 자르기
    public static String subStringBytes(String orgstr, int maxlen) {
        int DB_FIELD_LENGTH = maxlen;

        Charset utf8Charset = Charset.forName("UTF-8");
        CharsetDecoder cd = utf8Charset.newDecoder();

        try {
            byte[] sba = orgstr.getBytes("UTF-8");
            // Ensure truncating by having byte buffer = DB_FIELD_LENGTH
            ByteBuffer bb = ByteBuffer.wrap(sba, 0, DB_FIELD_LENGTH); // len in [B]
            CharBuffer cb = CharBuffer.allocate(DB_FIELD_LENGTH); // len in [char] <= # [B]
            // Ignore an incomplete character
            cd.onMalformedInput(CodingErrorAction.IGNORE);
            cd.decode(bb, cb, true);
            cd.flush(cb);
            orgstr = new String(cb.array(), 0, cb.position());
        } catch (UnsupportedEncodingException e) {
            System.err.println("### 지원하지 않는 인코딩입니다." + e);
        }

        return orgstr;
    }

    //2019.01.25:문자열 인코딩에 따라서 글자수 체크
    public static int charBytesLength(CharSequence sequence) {
        int count = 0;
        for (int i = 0, len = sequence.length(); i < len; i++) {
            char ch = sequence.charAt(i);

            if (ch <= 0x7F) {
                count++;
            } else if (ch <= 0x7FF) {
                count += 2;
            } else if (Character.isHighSurrogate(ch)) {
                count += 4;
                ++i;
            } else {
                count += 3;
            }
        }
        return count;
    }



	public static String appendJosa(String text, String josa) {
		if (text == null) {
			return null;
		}
		String txt = text.trim();
		int lastAsciiNum = txt.charAt(txt.length() - 1);
		int code = lastAsciiNum - 44032;

		// 한글이 아닐때
		if (code < 0 || code > 11171) {
			return String.format("%s%s", txt, josa);
		}

		boolean hasLast = code % 28 != 0;
		String resultJosa = null;

		if (josa == "은" || josa == "는") {
			resultJosa = hasLast ? "은" : "는";
		}
		else if (josa == "이" || josa == "가") {
			resultJosa = hasLast ? "이" : "가";
		}
		else if (josa == "을" || josa == "를") {
			resultJosa = hasLast ? "을" : "를";
		}
		else if (josa == "과" || josa == "와") {
			resultJosa = hasLast ? "과" : "와";
		}
		else {
			resultJosa = josa;
		}

		return String.format("%s%s", txt, resultJosa);
		/*
		System.out.println("=====> "+ TextHelper.appendJosa("김성욱", "은") +" 천재다.");
		System.out.println("=====> "+ TextHelper.appendJosa("김성욱", "이") +" 천재다.");
		System.out.println("=====> "+ TextHelper.appendJosa("김성욱", "을") +" 천재로 부르자.");
		System.out.println("=====> "+ TextHelper.appendJosa("김성욱", "과") +" 천재가 되자.");
		System.out.println();
		System.out.println("=====> "+ TextHelper.appendJosa("이광우", "은") +" 바보다.");
		System.out.println("=====> "+ TextHelper.appendJosa("이광우", "이") +" 바보다.");
		System.out.println("=====> "+ TextHelper.appendJosa("이광우", "을") +" 바보다.");
		System.out.println("=====> "+ TextHelper.appendJosa("이광우", "과") +" 바보다.");
		 */
	}

	public static String getJosa(String text, String josa) {
		if (text == null) {
			return null;
		}
		String txt = text.trim();
		int lastAsciiNum = txt.charAt(txt.length() - 1);
		int code = lastAsciiNum - 44032;

		// 한글이 아닐때
		if (code < 0 || code > 11171) {
			return String.format("%s%s", txt, josa);
		}

		boolean hasLast = code % 28 != 0;
		String resultJosa = null;

		if (josa == "은" || josa == "는") {
			resultJosa = hasLast ? "은" : "는";
		}
		else if (josa == "이" || josa == "가") {
			resultJosa = hasLast ? "이" : "가";
		}
		else if (josa == "을" || josa == "를") {
			resultJosa = hasLast ? "을" : "를";
		}
		else if (josa == "과" || josa == "와") {
			resultJosa = hasLast ? "과" : "와";
		}
		else {
			resultJosa = josa;
		}

		return resultJosa;
		/*
		System.out.println("=====> "+ TextHelper.appendJosa("김성욱", "은") +" 천재다.");
		System.out.println("=====> "+ TextHelper.appendJosa("김성욱", "이") +" 천재다.");
		System.out.println("=====> "+ TextHelper.appendJosa("김성욱", "을") +" 천재로 부르자.");
		System.out.println("=====> "+ TextHelper.appendJosa("김성욱", "과") +" 천재가 되자.");
		System.out.println();
		System.out.println("=====> "+ TextHelper.appendJosa("이광우", "은") +" 바보다.");
		System.out.println("=====> "+ TextHelper.appendJosa("이광우", "이") +" 바보다.");
		System.out.println("=====> "+ TextHelper.appendJosa("이광우", "을") +" 바보다.");
		System.out.println("=====> "+ TextHelper.appendJosa("이광우", "과") +" 바보다.");
		 */
	}



	public static void main(String[] args)
	{
		String format = "%2$s + %1$s = %3$s";
		String[] arr = {"1", "2", "3"};
		System.out.println(String.format(format, arr));
	}
}
