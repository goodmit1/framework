package com.fliconz.fm.common.util;
/*
* @(#)ListSet.java
*/
import java.util.*;
/**
* ListSet.
*
* @version	1.0
* @since	1.0	2003.06.25 Wed.
* @author	kiki (Kyoung Gu. LEE)
*/
public final class ListSet<E> extends HashSet<E>
{
	private LinkedList<E> m_elements = null;

	//##############################################################################################################################

	public ListSet()
	{
		this(null);
	}

	public ListSet(Collection<E> c)
	{
		m_elements = c == null ? new LinkedList() : new LinkedList(c);
	}

	//##############################################################################################################################

	public boolean add(E o)
	{
		return m_elements.contains(o)
			? false
			: m_elements.add(o);
	}

	public boolean addAll(Collection<? extends E> c)
	{
		return m_elements.addAll(c);
	}

	public void clear()
	{
		m_elements.clear();
	}

	public Object clone()
	{
		return new ListSet(m_elements);
	}


	public boolean contains(Object o)
	{
		return m_elements.contains(o);
	}

	public boolean containsAll(Collection c)
	{
		return m_elements.containsAll(c);
	}

	public boolean isEmpty()
	{
		return m_elements.isEmpty();
	}

	public Iterator<E> iterator()
	{
		return m_elements.iterator();
	}

	public boolean remove(Object o)
	{
		return m_elements.remove(o)
			? false
			: m_elements.add((E)o);
	}

	public boolean removeAll(Collection c)
	{
		return m_elements.removeAll(c);
	}

	public boolean retainAll(Collection c)
	{
		return m_elements.retainAll(c);
	}

	public int size()
	{
		return m_elements.size();
	}

	public E[] toArray()
	{
		return (E[]) m_elements.toArray();
	}

	public E[] toArray(Object[] a)
	{
		return (E[]) m_elements.toArray(a);
	}


	public List<E> toList()
	{
		return m_elements;
	}







	public E getFirst()
	{
		return m_elements.getFirst();
	}

	public E getLast()
	{
		return m_elements.getLast();
	}


	public boolean addFirst(E o)
	{
		if ( m_elements.contains(o) )
			return false;

		m_elements.addFirst(o);
		return true;
	}

	public boolean addLast(E o)
	{
		if ( m_elements.contains(o) )
			return false;

		m_elements.addLast(o);
		return true;
	}


	public E removeFirst()
	{
		return m_elements.removeFirst();
	}

	public E removeLast()
	{
		return m_elements.removeLast();
	}
}
