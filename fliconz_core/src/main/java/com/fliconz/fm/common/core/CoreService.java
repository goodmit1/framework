package com.fliconz.fm.common.core;


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


public class CoreService implements ApplicationContextAware
{
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());
	protected ApplicationContext appContext = null;

	public CoreService()
	{

	}

	public void setApplicationContext(ApplicationContext appContext)
	{
		this.appContext = appContext;
		if (theLogger.isDebugEnabled()) theLogger.debug("===> ApplicationContext=[{}]", appContext);
	}

	public ApplicationContext getApplicationContext()
	{
		return this.appContext;
	}



	public void publishApplicationEvent(String eventName, Map<String,Object> eventData) {
		this.getApplicationContext().publishEvent(new KGApplicationEvent(eventName, eventData));
	}
	public void publishApplicationEvent(String eventName, Map<String,Object> eventData, boolean isAsync) {
		this.getApplicationContext().publishEvent(new KGApplicationEvent(eventName, eventData, isAsync));
	}
	public void publishApplicationEvent(KGApplicationEvent eventData) {
		this.getApplicationContext().publishEvent(eventData);
	}



//	/**
//	* /WEB-INF/config-resource/resource-config.xml 파일에 설정된 resource, property의 값을 얻는다.
//	*
//	* @param request
//	* @param resourceName
//	* @param propertyName
//	*
//	* @return resourceName/propertyName에 대한 설정값, 해당 resource/property가 없는 경우 null을 리턴.
//	*/
//	protected final String getResourceString(HttpServletRequest request, String resourceName, String propertyName)
//    {
//		return ResourceHelper.getString(request, resourceName, propertyName);
//    }

//	/**
//	* /WEB-INF/config-resource/resource-config.xml 파일에 설정된 resource, property의 값을 얻는다.
//	*
//	* @param request
//	* @param resourceName
//	* @param propertyName
//	*
//	* @return resourceName/propertyName에 대한 설정값, 해당 resource/property가 없는 경우 defaultValue를 리턴.
//	*/
//	protected final String getResourceString(HttpServletRequest request, String resourceName, String propertyName, String defaultValue)
//    {
//		return ResourceHelper.getString(request, resourceName, propertyName, defaultValue);
//    }


//	protected final String getResourceString(String locale, String resourceName, String propertyName)
//    {
//		return ResourceHelper.getString(locale, resourceName, propertyName, null);
//    }
//
//	protected final String getResourceString(String locale, String resourceName, String propertyName, String defaultValue)
//    {
//		return ResourceHelper.getString(locale, resourceName, propertyName, defaultValue);
//    }
}
