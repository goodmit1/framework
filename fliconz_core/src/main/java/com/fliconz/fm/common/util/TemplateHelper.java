package com.fliconz.fm.common.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import freemarker.core.InvalidReferenceException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;


public class TemplateHelper
{

	public static void process(Reader in, Writer out, Object data) throws IOException, TemplateException
	{
		Configuration cfg = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
		Template aTemplate = new Template(TemplateHelper.class.getName(), in, cfg);
		aTemplate.process(data, out);
//        try
//        {
//            Configuration cfg = new Configuration();
//	        Template aTemplate = new Template(null, in, cfg);
//	        aTemplate.process(data, out);
//        }
//        catch (TemplateException e)
//        {
//            throw e;
//        }
//        catch (IOException e)
//        {
//			throw e;
//		}
	}


	public static void process(String template, Writer out, Object data) throws IOException, TemplateException
	{
		StringReader in = null;

		try
		{
			in = new StringReader(template);
			TemplateHelper.process(in, out, data);
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (Throwable e) {}
		}
	}


	public static String process(String template, Object data) throws IOException, TemplateException
	{
		StringWriter out = null;

		try
		{
			out = new StringWriter();
			TemplateHelper.process(template, out, data);

			return out.toString();
		}
		finally
		{
			try
			{
				if (out != null)
				{
					out.close();
				}
			}
			catch (Throwable e) {}
		}
	}
}
