/*
* @(#)GetDataFromDataTransferTEI.java
 */
package com.fliconz.fm.common.tag;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;

public class GetDataFromDataMapTEI extends TagExtraInfo
{
	public GetDataFromDataMapTEI() {
		super();
	}

	//##############################################################################################################################

	public VariableInfo[] getVariableInfo(TagData data) {
		boolean isDeclare = TextHelper.parseBoolean( data.getAttributeString("declare"), true );
		String strClass = (String)data.getAttribute("class");
		strClass = TextHelper.evl( TextHelper.trim(strClass), null ) == null ? "java.lang.Object" : strClass;
		if (strClass.equals("dataMap"))
		{
			strClass = DataMap.class.getName();
		}

		VariableInfo[] result = { new VariableInfo(data.getId(), strClass, isDeclare, VariableInfo.AT_BEGIN) };
		return result;
	}

	public boolean isValid(TagData data) {
		return true;
	}
}
