package com.fliconz.fm.common;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.common.core.KGApplicationEvent;
import com.fliconz.fm.common.util.TocContext;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.security.util.DBParamMap;
import com.fliconz.fm.security.util.DBResultMap;
import com.fliconz.fw.runtime.dao.BaseDAO;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;


public abstract class BaseService implements ApplicationContextAware {

	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	@Qualifier("commonDAO")
	private BaseDAO dao;



	public BaseDAO getDAO() {
		return dao;
	}
	public BaseDAO getWriteDAO() {
		if(PropertyManager.getBoolean("is_multi_db_use"))
		{
			try
			{
				BaseDAO dao = this.getDAO();
				if (dao.getConnection().isReadOnly() == false)
				{
					return dao;
				}
			}
			catch (Exception e)
			{
				throw new IllegalStateException(e.getMessage(), e);
			}

			try
			{
				return this.getApplicationContext().getBean(this.getClass()).getDAO();
			}
			catch(NoUniqueBeanDefinitionException e)
			{
				return ((BaseService)this.getApplicationContext().getBean(this.getClass().getName())).getDAO();
			}
		}
		else
		{

			return dao;
		}
	}
	public void setDAO(BaseDAO dao) {

	}

	private ApplicationContext appContext = null;

	public void setApplicationContext(ApplicationContext ctx) {
		this.appContext = ctx;
	}

	public ApplicationContext getApplicationContext() {
		return this.appContext;
	}



	public void publishApplicationEvent(String eventName, Map<String,Object> eventData) {
		this.getApplicationContext().publishEvent(new KGApplicationEvent(eventName, eventData));
	}
	public void publishApplicationEvent(String eventName, Map<String,Object> eventData, boolean isAsync) {
		this.getApplicationContext().publishEvent(new KGApplicationEvent(eventName, eventData, isAsync));
	}
	public void publishApplicationEvent(KGApplicationEvent eventData) {
		this.getApplicationContext().publishEvent(eventData);
	}





	/**
	 * 같은 패키지내 message_<언어코드>.properties에서 메시지를 가져와서 변수값을 replace해서 return한다.
	 * @param msgKey	메시지 키
	 * @param param	메시지 내 파라미터
	 * @return
	 */
	public String getMessage(String msgKey, String... param){
		String msg = getMessage(msgKey);
		return MessageFormat.format(msg, param);
	}

	/**
	 * 같은 패키지내 message_<언어코드>.properties에서 메시지를 가져온다.
	 * @param msgKey	메시지 키
	 * @return
	 */
	public String getMessage(String msgKey){
		return MessageBundle.getBundle(getMessageBase(), this.getLang()).getString(msgKey);
	}
	protected String getMessageBase(){
		int pos = this.getNameSpace().lastIndexOf(".");
		return this.getNameSpace().substring(0, pos) + ".message" ;
	}
	/**
	 * param의 값중에 date 형을 string으로 변환
	 * @param param
	 * @param k
	 */
	protected void setDate2Str(Map param, String k){
		Object v = param.get(k);
		if(v instanceof java.util.Date){
			param.put(k, DateUtil.convertStr((Date)v));
		}
	}

	public String getLang(){
		UserVO user = getUserVO();
		String lang = null;
		if(user != null){
			lang = user.getLangKnd();
			if(lang == null)
			{
				lang = user.getLocale();
			}
		}
		if(lang == null)
		{
			lang = "ko";
		}
		return lang;
	}


	protected UserVO getUserVO(){
		try{
			Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (obj instanceof UserVO) {
				return (UserVO)obj;
			} else {
				obj = SecurityContextHolder.getContext().getAuthentication().getDetails();
				if (obj instanceof UserVO) {
					return (UserVO)obj;
				}
				return null;
			}
		} catch(Exception e){
			return null;
		}
	}
    /**
     * 결과를 Map으로 return, TITLE필드 값이 value가 되고,  ID필드 값이 map의 key가 됨.
     * @param queryKey
     * @param param
     * @return
     * @throws Exception
     */
	public Map getListMap(String queryKey, Map param) throws Exception{
		addCommonParam(param);
		return getDAO().getListMap(queryKey, param);
	}
	protected abstract String getNameSpace();

	/**
	 * 쿼리 공통 파라미터
	 * @param param
	 */
	protected void addCommonParam(Map param){
		if (param == null) return;
		param.put("_SYSTEM_CODE_", PropertyManager.getInt("system.code",0));
		if(param.containsKey("BY_PASS_COMMON_PARAM")) return;
		UserVO user =  getUserVO();
		if(user != null){
			param.put("_LOGIN_ID_", user.getLoginId());
			param.put("_USER_ID_", user.getUserId());
			param.put("_USER_NM_", user.getUserName());
			param.put("_USER_IP_", user.getUserIP());
			param.put("_LANG_", user.getLangKnd());
			param.put("_USER_PGM_", user.getSelectedPrgmId());
			param.put("_IS_SUPER_ADMIN_", user.isSuperAdmin());
			param.put("_TEAM_CD_", user.getTeam().getTeamCd());
			param.put("_COMP_ID_", user.getTeam().getCompId());
		}else{
			TocContext aTocContext = TocContext.getInstance();
			Map<String,Object> comParam = aTocContext.getMapAttribute("com.flyingcontents.autolink_rider.base.TocController.commonParam");
			if (comParam != null && comParam.isEmpty() == false){
				param.putAll(comParam);
			}
//			else
//			{
//				SecurityContext aSecurityContext = SecurityContextHolder.getContext();
//				Authentication aAuth = aSecurityContext.getAuthentication();
//				if (aAuth != null && aAuth.getPrincipal() != null)
//				{
//					if (aAuth.getPrincipal() instanceof MemberData)
//					{
//						MemberData aMemberData = (MemberData)aAuth.getPrincipal();
//						param.put("_USER_ID_", aMemberData.getMemberNo());
//						param.put("_USER_NM_", aMemberData.getMemberName());
//					}
//					else if (aAuth.getPrincipal() instanceof RiderData)
//					{
//						RiderData aRiderData = (RiderData)aAuth.getPrincipal();
//						param.put("_USER_ID_", String.format("-%s", aRiderData.getRiderNo()));
//						param.put("_USER_NM_", aRiderData.getRiderName());
//					}
//				}
//			}
		}

		if(param.containsKey("_USER_IP_") == false){
			try
			{
				param.put("_USER_IP_", FMSecurityContextHelper.getFirstRemoteAddr());
			}
			catch(Exception ignore)
			{}
		}

		if (param.containsKey("_NOW_") == false){
			param.put("_NOW_", new java.util.Date());
		}

		if (param.containsKey("_SYSDATE_") == false){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			param.put("_SYSDATE_", sdf.format( (java.util.Date)param.get("_NOW_") ));
		}
	}
	protected boolean chkEncryptTable(String tableNm )
	{
		Set<String> cryptTables = PropertyManager.getInstance().getCryptTables();
		if(cryptTables.contains(tableNm))
		{
			return true;
		}

		return false;
	}
	protected boolean chkEncryptField(Map param )
	{
		if (param instanceof DBResultMap) return false;
		Set<String> cryptFields = PropertyManager.getInstance().getCryptFields();
		Set<String> keys = param.keySet();
		for(String k : keys)
		{
			if(cryptFields.contains(k))
			{
				return true;
			}
		}

		return false;
	}
	protected List<Map> getEncryptResult(String tableNm, List<Map> result)
	{
		if(chkEncryptTable(tableNm) && result.size()>0)
		{
			if(chkEncryptField(result.get(0)))
			{
				List result1 = new ArrayList();
				for(Map r:result)
				{
					DBResultMap r1 = new DBResultMap();
					putAll(r, r1);
					r1.setCryptYN(false);
					result1.add(r1);

				}
				return result1;
			}
		}
		return result;
	}
	private void putAll(Map src, Map desc)
	{
		  Iterator localIterator = src.entrySet().iterator();
	      while (localIterator.hasNext())
	      {
	        Map.Entry localEntry = (Map.Entry)localIterator.next();

	        desc.put( localEntry.getKey(),localEntry.getValue() );

	      }
	}
	protected  Map getEncryptResult(String tableNm,  Map  r)
	{
		if(!(r instanceof DBResultMap) && chkEncryptTable(tableNm))
		{

				DBResultMap r1 = new DBResultMap();
				putAll(r, r1);
				r1.setCryptYN(false);
				return r1;
		}
		return r;
	}
	protected Map getEncryptParam(String tableNm, Map param)
	{

		if(chkEncryptTable(tableNm))
		{
			DBParamMap newParam = new DBParamMap();
			putAll(param, newParam);

			return newParam;
		}

		return param;
	}
	protected int insertDBTable(String tableNm, Map param) throws Exception{
		Map newParam = getEncryptParam(tableNm,param);
		addCommonParam(newParam);
		return getWriteDAO().insert(String.format("%s.%s%s", getNameSpace(), "insert_", tableNm), newParam);
	}

	public List selectList(String tableNm, Map param) throws Exception{
		return selectList(tableNm, "list", param);

	}

	private List selectList(String tableNm, String prefix, Map param) throws Exception{
		addCommonParam(param);
		List<Map> result = getDAO().selectList(String.format("%s.%s%s", getNameSpace(), prefix + "_", tableNm), param);
		return this.getEncryptResult(tableNm, result);
	}
	public List selectList4Popup(String tableNm, Map param) throws Exception{
		return selectList(tableNm, "list_popup", param);

	}


	public Map selectInfo(String tableNm, Map param) throws Exception{
		addCommonParam(param);
		Map result =   getDAO().selectOne(String.format("%s.%s%s", getNameSpace(), "selectByPrimaryKey_", tableNm), param);
		if(result != null && result.size()>0)
		{
			return this.getEncryptResult(tableNm, result);
		}
		else
		{
			return result;
		}
	}

	public Object selectOneObjectByQueryKey(String namespace, String queryKey, Map param) throws Exception{
		addCommonParam(param);
		return   getDAO().getSqlSession().selectOne(String.format("%s.%s", namespace, queryKey), param);
	}

	public List selectListByQueryKey(String namespace, String queryKey, Map param) throws Exception{
		addCommonParam(param);
		List list =  getDAO().selectList(String.format("%s.%s", namespace, queryKey), param);
		return list;
	}

	public Map selectOneByQueryKey(String namespace, String queryKey, Map param) throws Exception{
		addCommonParam(param);
		Map result=  getDAO().selectOne(String.format("%s.%s", namespace, queryKey), param);
		if(result instanceof DBResultMap)
		{
			((DBResultMap)result).setCryptYN(false);
		}
		return result;
	}

	public int insertByQueryKey(String namespace, String queryKey, Map param) throws Exception{
		addCommonParam(param);
		return getWriteDAO().insert(String.format("%s.%s", namespace, queryKey), param);
	}

	public int updateByQueryKey(String namespace, String queryKey, Map param) throws Exception{
		addCommonParam(param);
		return getWriteDAO().update(String.format("%s.%s", namespace, queryKey), param);
	}

	public int  deleteByQueryKey(String namespace, String queryKey,   Map param) throws Exception{
		addCommonParam(param);
		return getWriteDAO().delete(String.format("%s.%s", namespace, queryKey), param);
	}

	public Object selectOneObjectByQueryKey(String queryKey, Map param) throws Exception{
		return this.selectOneObjectByQueryKey(getNameSpace(), queryKey, param);
	}

	public List selectListByQueryKey(String queryKey, Map param) throws Exception{
		return this.selectListByQueryKey(getNameSpace(), queryKey, param);
	}

	public Map selectOneByQueryKey(String queryKey, Map param) throws Exception{
		return this.selectOneByQueryKey(getNameSpace(), queryKey, param);
	}

	public int insertByQueryKey(String queryKey, Map param) throws Exception{
		return this.insertByQueryKey(getNameSpace(), queryKey, param);
	}

	public int updateByQueryKey(String queryKey, Map param) throws Exception{
		return this.updateByQueryKey(getNameSpace(), queryKey, param);
	}

	public int insertByQueryKey(String queryKey, Map param, boolean hasEncrypt) throws Exception{
		DBParamMap newParam = new DBParamMap();
		putAll(param, newParam);

		return this.insertByQueryKey(getNameSpace(), queryKey, newParam);
	}

	public int updateByQueryKey(String queryKey, Map param,boolean hasEncrypt) throws Exception{
		DBParamMap newParam = new DBParamMap();
		putAll(param, newParam);

		return this.updateByQueryKey(getNameSpace(), queryKey, newParam);
	}
	public int deleteByQueryKey(String queryKey, Map param) throws Exception{
		return this.deleteByQueryKey(getNameSpace(), queryKey, param);
	}

	protected int updateDBTable(String tableNm, Map param) throws Exception{
		Map newParam = getEncryptParam(tableNm,param);
		addCommonParam(newParam);

		return getWriteDAO().update(String.format("%s.%s%s", getNameSpace(), "update_", tableNm), newParam);
	}

	protected int updateDBTable(String tableNm, Map param, String... field) throws Exception{
		Map newParam1 = getEncryptParam(tableNm,param);
		Map newParam = new HashMap();
		MapUtil.copy(newParam1, newParam, field);
		addCommonParam(newParam);
		return getWriteDAO().update(String.format("%s.%s%s", getNameSpace(), "update_", tableNm), param);
	}

	protected int deleteDBTable(String tableNm, Map param) throws Exception{
		addCommonParam(param);
		return getWriteDAO().delete(String.format("%s.%s%s", getNameSpace(), "delete_", tableNm), param);
	}

	public int selectListCount(String tableNm, Map param) throws Exception {
		addCommonParam(param);
		return getDAO().selectNumber(String.format("%s.%s%s", getNameSpace(), "list_count_", tableNm), param);
	}

	public int selectCountByQueryKey(String queryKey, Map param) throws Exception {
		addCommonParam(param);
		return getDAO().selectNumber(String.format("%s.%s", getNameSpace(), queryKey), param);
	}
}
