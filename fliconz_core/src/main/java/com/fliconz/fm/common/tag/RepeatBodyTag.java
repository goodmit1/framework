/*
* @(#)RepeatBodyTag.java
 */
package com.fliconz.fm.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

public class RepeatBodyTag extends BodyTagSupport
{
	@SuppressWarnings("unused")
	private String m_strClassName = null;
	@SuppressWarnings("unused")
	private boolean m_isDeclare = true;
	private int m_intCount = 0;
	private int m_intMaxRow = -1;

	//##############################################################################################################################

	public RepeatBodyTag() {
		super();
	}

	//##############################################################################################################################

	/**
	* Release a class, iteration value, id, desc, declare attribute and other attribute value.
	*
	* Called on a Tag handler to release state.
	* The page compiler guarantees that JSP page implementation objects will invoke this method on all tag handlers,
	* but there may be multiple invocations on <code>doStartTag</code> and <code>doEndTag</code> in between.
	*/
	public void release() {
		super.release();
		super.setId(null);
		m_strClassName = null;
		m_isDeclare = true;
		m_intCount = 0;
		m_intMaxRow = -1;
	}

	//##############################################################################################################################

	public void setId(String id) {
		super.setId(id);
	}

	public void setClassName(String className) {
		m_strClassName = className;
	}

	public void setDeclare(boolean isDeclare) {
		m_isDeclare = isDeclare;
	}

	public void setMaxRow(int maxRow) {
		m_intMaxRow= maxRow;
	}

	protected int getCount() {
		return m_intCount;
	}


	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{
		RepeatTag aRepeatTag = (RepeatTag)TagSupport.findAncestorWithClass(this, RepeatTag.class);
		if ( aRepeatTag == null) {
			throw new JspTagException("parent repeat tag is not exist.");
		}
		aRepeatTag.setRepeated();

//		if ( aRepeatTag.isEmpty() || (m_intMaxRow >= 0 && m_intMaxRow > m_intCount) ) {
//			return SKIP_BODY;
//		}

		if ( aRepeatTag.isEmpty() || (m_intMaxRow < 0 || m_intCount < m_intMaxRow) == false)
		{
			return SKIP_BODY;
		}

		Object aElement = aRepeatTag.getEnumeration().nextElement();
		if (aElement == null) {
			pageContext.removeAttribute( super.getId() );
		}
		else
		{
			pageContext.setAttribute( super.getId(), aElement );
		}

		m_intCount++;
		return EVAL_BODY_BUFFERED;
	}

	public int doAfterBody() throws JspTagException
	{
		try
		{
			RepeatTag aRepeatTag = (RepeatTag)TagSupport.findAncestorWithClass(this, RepeatTag.class);
			if (aRepeatTag == null) {
				throw new JspTagException("parent repeat tag is not found");
			}

			bodyContent.writeOut(getPreviousOut());
			bodyContent.clearBody();

//			if ( aRepeatTag.getEnumeration().hasMoreElements() || (m_intMaxRow >= 0 && m_intMaxRow < m_intCount) ) {
			if ( aRepeatTag.getEnumeration().hasMoreElements() && (m_intMaxRow < 0 || m_intCount < m_intMaxRow) ) {
				Object aElement = aRepeatTag.getEnumeration().nextElement();
				if (aElement == null) {
					pageContext.removeAttribute( super.getId() );
				}
				else
				{
					pageContext.setAttribute( super.getId(), aElement );
				}

				m_intCount++;
				return EVAL_BODY_BUFFERED;
			}

			return SKIP_BODY;
		}
		catch(IOException e) {
			throw new JspTagException( e.getMessage() );
		}
	}
}
