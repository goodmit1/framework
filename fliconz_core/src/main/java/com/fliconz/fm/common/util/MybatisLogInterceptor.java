package com.fliconz.fm.common.util;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fliconz.fw.runtime.util.PropertyManager;
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;

@Intercepts
(
	{
		@Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})
	   ,@Signature(type = Executor.class, method = "query",  args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class})
	   ,@Signature(type = Executor.class, method = "query",  args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})
	}
)
public class MybatisLogInterceptor implements Interceptor
{
	private final static int PARAM_BIND_NONE = 0;
	private final static int PARAM_BIND_NAME = 1;
	private final static int PARAM_BIND_VALUE = 2;

	private XProperties m_pt = null;
	private int m_paramBind = PARAM_BIND_NONE;
	private boolean m_logParam = true;
	private boolean m_logElapseTime = true;
	private boolean m_useStatementIdAsLoggerName = false;

	private boolean m_emptyToNull = false;
	private boolean m_restoreEmptyToNull = false;
	private Map<String,String> m_traceTargetTables = null;
	public MybatisLogInterceptor()
	{
		m_pt = new XProperties();
	}



	private Object getParamValue(Map param, String name)
	{
		if (name.indexOf(".") < 0)
		{
			return param.get(name);
		}

		String[] arr = TextHelper.split(name, ".");
		if (arr == null || arr.length <= 0)
		{
			return null;
		}

		Map mm = param;
		Object value = null;
		for (String nn : arr)
		{
			value = mm.get(nn);
			if (value == null)
			{
				return value;
			}
			if (value instanceof Map)
			{
				mm = (Map)value;
			}
		}

		return value;
	}

	private boolean isEmptyValue(String value)
	{
		return value == null || value.trim().equals("");
	}

	private void emptyToNull(Map param, Map restoreMap)
	{
		Object[] arr = param.keySet().toArray();
		for (Object key : arr)
		{
			Object value = param.get(key);
			if (value instanceof String && isEmptyValue((String)value))
			{
				if (restoreMap != null) restoreMap.put(key,value);
				param.remove(key);
			}
		}

	}

	private void restoreEmptyToNull(Map param, Map restoreMap)
	{
		try
		{
			if (restoreMap != null)
			{
				param.putAll(restoreMap);
			}
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public Object intercept(Invocation invoc) throws Throwable
	{
//		====> MybatisLogInterceptor-aaa-000: interface org.apache.ibatis.executor.Executor
//		====> MybatisLogInterceptor-aaa-001: org.apache.ibatis.executor.CachingExecutor@40fb4ad6
//		====> MybatisLogInterceptor-aaa-002: class org.apache.ibatis.executor.CachingExecutor
//		====> MybatisLogInterceptor-bbb: false / true / true
//		====> MybatisLogInterceptor-ccc: interface org.apache.ibatis.executor.Executor

		// org.apache.ibatis.executor.Executor
//		System.out.println("====> MybatisLogInterceptor-aaa-000: "+invoc.getMethod().getDeclaringClass());
//		System.out.println("====> MybatisLogInterceptor-aaa-001: "+invoc.getTarget());
//		System.out.println("====> MybatisLogInterceptor-aaa-002: "+invoc.getTarget().getClass());
//		System.out.println(String.format("====> MybatisLogInterceptor-bbb: %b / %b / %b"
//				, LoggerFactory.getLogger(invoc.getMethod().getDeclaringClass()).isDebugEnabled()
//				, LoggerFactory.getLogger(java.sql.Statement.class).isDebugEnabled()
//				, LoggerFactory.getLogger(java.sql.PreparedStatement.class).isDebugEnabled()));

//		if (LoggerFactory.getLogger(invoc.getMethod().getDeclaringClass()).isDebugEnabled() == false ||
//			LoggerFactory.getLogger(java.sql.Statement.class).isDebugEnabled() == false ||
//			LoggerFactory.getLogger(java.sql.PreparedStatement.class).isDebugEnabled() == false)
//		{
//			//System.out.println("====> MybatisLogInterceptor-ccc: "+invoc.getMethod().getDeclaringClass());
//			return invoc.proceed();
//		}

//		if (LoggerFactory.getLogger(java.sql.Statement.class).isDebugEnabled() == false ||
//				LoggerFactory.getLogger(java.sql.PreparedStatement.class).isDebugEnabled() == false)
//		{
//			//System.out.println("====> MybatisLogInterceptor-ccc: "+invoc.getMethod().getDeclaringClass());
//			return invoc.proceed();
//		}
		if (LoggerFactory.getLogger(MybatisLogInterceptor.class).isDebugEnabled() == false)
		{
			//System.out.println("====> MybatisLogInterceptor-ccc: "+invoc.getMethod().getDeclaringClass());
			return invoc.proceed();
		}

		Object[] args = invoc.getArgs();
		MappedStatement ms = (MappedStatement)args[0];
		Object param = args[1];
		 
		BoundSql aBoundSql = ms.getBoundSql(param);

		Map restoreMap = null;
		if (param != null && param instanceof Map && this.m_emptyToNull)
		{
			if (this.m_restoreEmptyToNull)
			{
				restoreMap = new HashMap();
			}
			this.emptyToNull((Map)param, restoreMap);
		}

		//List<ParameterMapping> paramMppintList = boundSql.getParameterMappings();
		//getDeclaringClass()
//        for (int i=0; i<args.length; i++)
//        {
//        	System.out.println("====> MybatisLogInterceptor-ddd : "+i+". "+args[i]);
//        }


//        String str = ms.getId();
//        int index = str.indexOf(".");
//        str = index > 0 ? str.substring(0,index) : null;
//        if (str != null) str = m_pt.getString(str);
//
//        Logger aLogger = str == null ? LoggerFactory.getLogger(this.getClass()) : LoggerFactory.getLogger(str);
//        if (aLogger.isDebugEnabled() == false)
//        {
//			//System.out.println("====> MybatisLogInterceptor-eee: "+invoc.getMethod().getDeclaringClass());
//        	return invoc.proceed();
//        }

		//System.out.println("====> MappedStatement ID: "+ms.getId());
		String loggerName = ms.getId();
		if (this.m_useStatementIdAsLoggerName)
		{
			int lastDotIndex = ms.getId().lastIndexOf(".");
			if (lastDotIndex > 0)
			{
				loggerName = loggerName.substring(0, lastDotIndex);
			}
		}
		else
		{
			loggerName = this.getClass().getName();
		}
		Logger aLogger = LoggerFactory.getLogger(loggerName);
		if (aLogger.isDebugEnabled() == false)
		{
			Object resultValue =  invoc.proceed();
			this.restoreEmptyToNull(((Map)param), restoreMap);
			return resultValue;
		}

		//String sql = boundSql.getSql().trim();
		String sql = bindParamToSQL(param, aBoundSql);
		modifyTrace( ms.getId(), sql, param);
		StringBuilder sb = new StringBuilder();

		sb.append("==============================================================================================").append("\n");
		sb.append("id: ").append(ms.getId()).append("\n");

		if (param != null && param instanceof Map)
		{
			List<ParameterMapping> paramMppingList = aBoundSql.getParameterMappings();
			Map aMap = (Map)param;

			sb.append("--------------------------------------------------").append("\n");
			sb.append(sql).append("\n");
			sb.append("--------------------------------------------------").append("\n");

			if (m_logParam)
			{
				sb.append("parameter-map:").append("\n");

				for (int i=0; i<paramMppingList.size(); i++)
				{
					ParameterMapping aParameterMapping = paramMppingList.get(i);
					String name = aParameterMapping.getProperty();
					Object value = getParamValue(aMap, name);
					if (value == null)
					{
						value = aBoundSql.getAdditionalParameter(name);
					}
					sb.append(i+1).append(". [").append(name).append("]=[").append(value).append("]\n");
				}

				sb.append("--------------------------------------------------").append("\n");
				sb.append("param: ").append(param).append("\n");
			}
		}
		else
		{
			sb.append("--------------------------------------------------").append("\n");
			sb.append(sql).append("\n");
			sb.append("--------------------------------------------------").append("\n");
			sb.append("param: ").append(param).append("\n");
		}

		sb.append("==============================================================================================").append("\n");

		aLogger.debug(sb.toString().trim());
//        System.out.println("====================================");
//        for (int i=0; i<args.length; i++)
//        {
//            System.out.println(i+". "+args[i]);
//            System.out.println("-----");
//        }
//        System.out.println(invoc.getMethod().getName());
//        System.out.println("-----");
//        System.out.println(invoc.getTarget());
//        System.out.println("-----");
//        System.out.println(ms.getId());
//        System.out.println("-----");
//        System.out.println(boundSql.getSql());
//        System.out.println("-----");
//        System.out.println(boundSql.getParameterMappings());
//        System.out.println("-----");
//        System.out.println(boundSql.getParameterObject());
//        System.out.println("-----");
//        System.out.println(param);
//        System.out.println("-----");
//        System.out.println(ms.getId());
//        System.out.println("====================================");

		long nStart = System.currentTimeMillis();
		long nEnd = 0;
		try
		{
			Object resultValue =  invoc.proceed();
			nEnd = System.currentTimeMillis();
			this.restoreEmptyToNull(((Map)param), restoreMap);
			return resultValue;
		}
		finally
		{
			if (m_logElapseTime)
			{
				aLogger.debug("Elapse time [{}] : {} ms", ms.getId(), nEnd - nStart);
			}
		}
	}


	private String bindParamToSQL(Object param, BoundSql pBoundSql)
	{
		String sql = pBoundSql.getSql().trim();

		if ((param instanceof Map) == false)
		{
			return sql;
		}

		if (TextHelper.notContains(this.m_paramBind, PARAM_BIND_NAME, PARAM_BIND_VALUE))
		{
			return sql;
		}

		if (param == null)
		{
			return sql;
		}

		Map aMap = (Map)param;
		List<ParameterMapping> paramMppingList = pBoundSql.getParameterMappings();
		boolean qMarkReplaced = false;
		for (ParameterMapping aParameterMapping : paramMppingList)
		{
			String name = aParameterMapping.getProperty();
			Object value = getParamValue(aMap, name);

			if (this.m_paramBind == PARAM_BIND_VALUE)
			{
				if (value == null)
				{
					value = pBoundSql.getAdditionalParameter(name);
				}

				if (value == null)
				{
					sql = sql.replaceFirst("\\?", "null");
				}
				else
				{
					String strValue = value.toString();
					if (value instanceof Number)
					{
						sql = sql.replaceFirst("\\?", strValue);
					}
					else
					{
						strValue = value.toString();
						strValue = strValue.replace("$", "\\$");
						String _strValue = strValue.replace("?", "&#63;");
						if (qMarkReplaced == false && strValue.equals(_strValue) == false)
						{
							qMarkReplaced = true;
						}
						strValue = _strValue;
						strValue = String.format("'%s'", strValue);
						sql = sql.replaceFirst("\\?", strValue);
					}
				}
			}
			else if (this.m_paramBind == PARAM_BIND_NAME)
			{
				sql = sql.replaceFirst("\\?", String.format("#{%s}", name));
			}
		}
		if (qMarkReplaced)
		{
			sql = sql.replace("&#63;", "?");
		}

		return sql;
	}
	protected void modifyTrace(String id, String sql, Object param) {
		if(m_traceTargetTables != null && param instanceof Map && ((Map)param).get("_USER_NM_") != null ) {
			
			for(String t:m_traceTargetTables.keySet()) {
				if(id.indexOf("update_" + t)>=0 || id.indexOf("delete_" + t)>=0) {
					Logger aLogger = LoggerFactory.getLogger(t);
					String log = m_traceTargetTables.get(t) + "==>" + ((Map)param).get("_LOGIN_ID_") + " " + ((Map)param).get("_USER_NM_")  + " "+ ((Map)param).get("_USER_IP_")  + "\n" + sql;
					aLogger.info(log);
				}
			}
		}
	}

	@Override
	public Object plugin(Object target)
	{
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties pt)
	{
		this.m_useStatementIdAsLoggerName = false;
		if (pt != null)
		{
			this.m_pt.putAll(pt);
			this.m_logElapseTime = this.m_pt.getBoolean("logElapseTime", true);
			this.m_logParam = this.m_pt.getBoolean("logParam", true);
			this.m_useStatementIdAsLoggerName = this.m_pt.getBoolean("useStatementIdAsLoggerName", false);
			this.m_emptyToNull = this.m_pt.getBoolean("emptyToNull", false);
			this.m_restoreEmptyToNull = this.m_pt.getBoolean("restoreEmptyToNull", false);

			String paramBind = this.m_pt.getString("paramBind", "none");
			if (paramBind.equals("name"))
			{
				this.m_paramBind = PARAM_BIND_NAME;
			}
			else if (paramBind.equals("value"))
			{
				this.m_paramBind = PARAM_BIND_VALUE;
			}
			else
			{
				this.m_paramBind = PARAM_BIND_NONE;
			}
		}
		String[] arr = PropertyManager.getStringArray("db.modify.trace.targetTable");
		if(arr != null) {
			this.m_traceTargetTables = new HashMap();
			for(String s:arr) {
				String[] t = s.split("|");
				m_traceTargetTables.put(t[0], t.length==2 ? t[1]: t[0]);
			}
		}
	}

}
