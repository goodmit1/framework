package com.fliconz.fm.common.core;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartRequest;

import com.fliconz.fm.common.util.CodeHelper;
import com.fliconz.fm.common.util.DataKey;
import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.FileHelper;
import com.fliconz.fm.common.util.ResourceHelper;
import com.fliconz.fm.common.util.SecurityHelper;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.common.util.TimeHelper;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
import com.fliconz.fw.runtime.util.PropertyManager;

//@Controller
public class CoreController implements ApplicationContextAware
{
	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());
	private ApplicationContext m_ctx = null;

	public final static String MNV_JSON_VIEW = "jsonView";

	public final static String MNV_EXCEL_VIEW = "excelView";
	
	public static final String MNV_JSONP_VIEW = "jsonpView";

	@Override
	public void setApplicationContext(ApplicationContext ctx)
	{
		m_ctx = ctx;
		//if (theLogger.isDebugEnabled()) theLogger.debug("===> ServletContext: {}", ctx);
	}

	public ApplicationContext getApplicationContext()
	{
		return m_ctx;
	}


//	public final static String UF_ORIGINAL_FILE_NAME = "uploadFile.original_filename";
//	public final static String UF_CONTENT_TYPE = "uploadFile.content_type";
//	public final static String UF_FILE_SIZE = "uploadFile.file_size";
//	public final static String UF_SAVE_FILE_PATH = "uploadFile.filepath";
//	public final static String UF_SAVE_FILE_NAME = "uploadFile.save_filename";

	public final org.json.JSONObject makeJsonResponse(String resCode, String resMsg, Object resData)
	{
		try
		{
			//DataMap<String, Object> reqParam = (DataMap<String, Object>) resData;
			if (resData != null && resData instanceof java.util.Map)
			{
				java.util.Map mm = ((java.util.Map)resData);
				if (mm.containsValue(mm))
				{
					java.util.List<Object> keyList = new LinkedList<Object>();
					for (java.util.Iterator it = mm.keySet().iterator(); it.hasNext();)
					{
						Object key = it.next();
						Object value = mm.get(key);
						if (value == mm)
						{
							keyList.add(key);
						}
					}

					for (Object key : keyList)
					{
						mm.remove(key);
					}
				}
			}
			//reqParam.remove("_delivery_info_");
			org.json.JSONObject resJSON = new org.json.JSONObject();
			resJSON.put(DataKey.KEY_RESPONSE_CODE, resCode);
			resJSON.put(DataKey.KEY_RESPONSE_MESSAGE, resMsg);
			if (resData != null) resJSON.put(DataKey.KEY_RESPONSE_DATA, resData);

			return resJSON;
		}
		catch (org.json.JSONException e)
		{
			throw new RuntimeException(e);
		}
	}

	public final org.json.JSONObject makeJsonResponse(String resCode, String resMsg)
	{
		return makeJsonResponse(resCode, resMsg, null);
	}


//	public final org.json.JSONObject genJsonResponse(String resCode, String resMsg, Object resData) throws JSONException
//	{
//		org.json.JSONObject resJSON = new org.json.JSONObject();
//		resJSON.put(DataKey.KEY_RESPONSE_CODE, resCode);
//		resJSON.put(DataKey.KEY_RESPONSE_MESSAGE, resMsg);
//		if (resData != null) resJSON.put(DataKey.KEY_RESPONSE_DATA, resData);
//
//		return resJSON;
//	}
//
//	public final org.json.JSONObject genJsonResponse(String resCode, String resMsg) throws JSONException
//	{
//		return genJsonResponse(resCode, resMsg, null);
//	}




	protected final boolean isMultipartRequest(HttpServletRequest request)
	{
		return TextHelper.nvl(request.getContentType(), "").startsWith("multipart/form-data");
	}



//	/**
//	 * 웹브라우저 Refresh 방지를 위한 인증키를 특정키로 세션에 보관한다.
//	 * @param request
//	 * @param sessionAttibuteKey session attribute key
//	 * @return
//	 */
//	public final String setRefreshProtect(HttpServletRequest request, String sessionAttibuteKey)
//	{
//		String refreshProtect = UUID.randomUUID().toString().replace('-', (char)0);
//		request.getSession(true).setAttribute(sessionAttibuteKey, refreshProtect);
//		return refreshProtect;
//	}
//
//	/**
//	 * 웹브라우저 Refresh 방지를 위해 만들어진 인증키를 세션에서 얻는다.
//	 * @param request
//	 * @param sessionAttibuteKey session attribute key
//	 * @return
//	 */
//	public final String getRefreshProtect(HttpServletRequest request, String sessionAttibuteKey)
//	{
//		return (String)request.getSession(true).getAttribute(sessionAttibuteKey);
//	}
//
//	/**
//	 * 웹브라우저 Refresh 방지를 위해 만들어진 인증키를 세션에서 삭제한다.
//	 * @param request
//	 * @param sessionAttibuteKey session attribute key
//	 * @return
//	 */
//	public final void removeRefreshProtect(HttpServletRequest request, String sessionAttibuteKey)
//	{
//		request.getSession(true).removeAttribute(sessionAttibuteKey);
//	}

//	/**
//	 * 웹브라우저 Refresh 방지를 위한 요청 파라미터를 얻는다.
//	 * @param request
//	 * @return
//	 */
//	public final String getRefreshProtectParameter(HttpServletRequest request)
//	{
//		return request.getParameter(DataKey.KEY_REFRESH_PROTECT);
//	}

//	/**
//	 * 웹브라우저 Refresh 방지를 위한 요청 파라미터와 세션에 보관중인 인증키가 동일하지 체크한다.
//	 * @param request
//	 * @return
//	 */
//	public final boolean isValidRefreshProtect(HttpServletRequest request, String sessionAttibuteKey)
//	{
//		String refreshProtect_param = this.getRefreshProtectParameter(request);
//		String refreshProtect_session = this.getRefreshProtect(request, sessionAttibuteKey);
//		if (refreshProtect_param != null && refreshProtect_session != null && refreshProtect_param.equals(refreshProtect_session))
//		{
//			return true;
//		}
//
//		return true;
//	}


	/**
	 * multipart attach file을 임의 디렉토리에 업로드한다.
	 * 임의 디렉토리 하위에 일자별로 저장된다. (yyyyMMdd 형태의 디렉토리가 생성된다.)
	 *
	 * @param request
	 * @param saveDir
	 * @param tagName
	 * @return
	 * @throws IOException
	 */
	protected final DataMap<String,Object> saveUploadFile(HttpServletRequest request, String saveDir, String tagName) throws IOException
	{
		if (this.isMultipartRequest(request) == false)
		{
			return null;
		}

		//System.out.println(String.format("======> request-class=[%s]", request.getClass().getName()));
		//System.out.println(String.format("======> saveDir=[%s]", saveDir));
		//System.out.println(String.format("======> tagName=[%s]", tagName));

//		MultipartHttpServletRequest mReq = null;
//		String reqClass = (String)request.getAttribute(RequestParamInterceptor.KEY_REQUEST_CLASS);
//		System.out.println(String.format("======> reqClass=[%s]", reqClass));
//
//		if ((request instanceof MultipartHttpServletRequest) == false || (reqClass != null && request.getClass().getName().equals(reqClass) == false))
//		{
//			System.out.println("===> aaa");
//			mReq = new StandardMultipartHttpServletRequest(request);
//		}
//		else
//		{
//			System.out.println("===> bbb");
//			mReq = (MultipartHttpServletRequest)request;
//		}

		if ((request instanceof MultipartHttpServletRequest) == false)
		{
			return null;
		}
		MultipartHttpServletRequest mReq = (MultipartHttpServletRequest)request;




		//System.out.println(String.format("======> mReq-class=[%s]", mReq.getClass().getName()));
		//request.getClass().getName()
		MultipartFile mFile = mReq.getFile(tagName);
		if (mFile == null || mFile.isEmpty())
		{
			return null;
		}


		File saveFile = null;
		int retryCount = 0;

		String fileExt = FileHelper.getExtension(mFile.getOriginalFilename());
		String ymd = TimeHelper.getCurrentTime("yyyyMMdd");
		String hms = TimeHelper.getCurrentTime("HHmmss");
		String ym = ymd.substring(0,6);

		StringBuilder sbSubPath = new StringBuilder().append("/").append(ym);
		saveFile = new File(saveDir, sbSubPath.toString());
		if (saveFile.exists() == false)
		{
			saveFile.mkdirs();
		}

		do
		{
			sbSubPath.setLength(0);
			sbSubPath.append("/").append(ym);
			sbSubPath.append("/").append(ymd).append("_").append(hms).append("_").append(retryCount);

			if (fileExt != null)
			{
				sbSubPath.append(".").append(fileExt);
			}

			saveFile = new File(saveDir, sbSubPath.toString());
			retryCount++;
		}
		while ( saveFile.exists() );

		mFile.transferTo(saveFile);

		DataMap<String,Object> fileInfo = new DataMap<String,Object>();
		fileInfo.setLong("fileSize", mFile.getSize());
		fileInfo.setString("contentType", mFile.getContentType());
		fileInfo.setString("fileName", mFile.getOriginalFilename());
		fileInfo.setString("saveFileName", saveFile.getName());
		fileInfo.setString("saveFilePath", saveFile.getAbsolutePath());
		fileInfo.setString("saveFileSubPath", sbSubPath.toString());

		return fileInfo;
	}

	protected final boolean hasMultipartFile(HttpServletRequest request, String tagName)
	{
		if (this.isMultipartRequest(request) == false)
		{
			return false;
		}

		MultipartHttpServletRequest mReq = (MultipartHttpServletRequest)request;
		MultipartFile mFile = mReq.getFile(tagName);
		if (mFile == null || mFile.isEmpty())
		{
			return false;
		}

		return true;
	}


	private final static String KEY_REQUEST_JSON = new StringBuilder().append(CoreController.class.getName()).append(".requestJSON").toString();

	protected final org.json.JSONObject getJSONObjectFromRequestBody(HttpServletRequest req) throws IOException
	{
		return getJSONObjectFromRequestBody(req, true);
	}

	protected final org.json.JSONObject getJSONObjectFromRequestBody(HttpServletRequest req, boolean isEncoding) throws IOException
	{
		BufferedReader buf = null;
		try
		{
			if (this.isMultipartRequest(req))
			{
				//if (theLogger.isDebugEnabled()) theLogger.debug("aaa");
				return null;
			}

			org.json.JSONObject reqJSON = (org.json.JSONObject)req.getAttribute(KEY_REQUEST_JSON);
			if (reqJSON != null)
			{
				//if (theLogger.isDebugEnabled()) theLogger.debug("bbb");
				return reqJSON;
			}

			String line = null;
			StringBuilder sb = new StringBuilder();
			if (isEncoding == false && req.getCharacterEncoding() != null)
			{
				buf = new BufferedReader(new InputStreamReader(req.getInputStream(), req.getCharacterEncoding()));
			}
			else
			{
				buf = new BufferedReader(new InputStreamReader(req.getInputStream()));
			}

			while ((line = buf.readLine()) != null)
			{
				sb.append(line);
			}

			if (sb.length() == 0)
			{
				//if (theLogger.isDebugEnabled()) theLogger.debug("ccc");
				return null;
			}

			try
			{
				String jsonString = sb.toString();
				//if (theLogger.isDebugEnabled()) theLogger.debug("===> json string {}", jsonString);

				if (isEncoding)
				{
					jsonString = URLDecoder.decode(jsonString, "UTF-8");
				}
				//if (theLogger.isDebugEnabled()) theLogger.debug("===> decoded json string {}", jsonString);
				//reqJSON = (JSONObject)JSONValue.parse(jsonString);
				reqJSON = new org.json.JSONObject(jsonString);

				//if (theLogger.isDebugEnabled()) theLogger.debug("reqJSON : {}", reqJSON);
				if (reqJSON != null)
				{
					req.setAttribute(KEY_REQUEST_JSON, reqJSON);
				}

				return reqJSON;
			}
			catch (org.json.JSONException e)
			{
//				e.printStackTrace();
//				if (theLogger.isDebugEnabled())
//				{
//					theLogger.debug("ddd");
//				}
//				return reqJSON;
				throw new RuntimeException(e);
			}
		}
		finally
		{
			try
			{
				if (buf != null) buf.close();
			}
			catch (Exception e) {}
		}
	}




	/*private final void makeRequestParam(HttpServletRequest request)
	{
		DataMap<String,Object> reqParam = (DataMap<String,Object>)request.getAttribute(DataKey.KEY_REQUEST_PARAM);
		if (reqParam == null)
		{
			reqParam = new DataMap<String,Object>();
			request.setAttribute(DataKey.KEY_REQUEST_PARAM, reqParam);
		}

		boolean isPramPopulated = TextHelper.parseBoolean((String)request.getAttribute(RequestParamInterceptor.KEY_PARAM_POPULATED), false);
		if (isPramPopulated)
		{
			return;
		}

		if (isMultipartRequest(request) && (request instanceof MultipartRequest) == false)
		{
			if (theLogger.isWarnEnabled()) theLogger.warn("mutipart read canceled");
			return;
		}

		for (Enumeration<String> en = request.getParameterNames(); en.hasMoreElements();)
		{
			String key = en.nextElement();
			String[] arrValue = request.getParameterValues(key);
			String value = null;

			if (arrValue != null && arrValue.length > 1)
			{
				reqParam.put(key, this.getParameterValues(arrValue));
			}
			else
			{
				value = getParameter(request.getParameter(key));
				reqParam.put(key, value);
			}
		}

		request.setAttribute(RequestParamInterceptor.KEY_PARAM_POPULATED, "true");
	}*/

	
	public final DataMap<String,Object> getRequestParam(HttpServletRequest request)
	{
		RequestParamInterceptor.makeRequestParam(request);
		DataMap<String,Object> reqParam = (DataMap<String,Object>)request.getAttribute(DataKey.KEY_REQUEST_PARAM);

		return reqParam;
	}

//
//	protected final boolean isMultipartRequest(HttpServletRequest request)
//	{
//		return TextHelper.nvl(request.getContentType(), "").startsWith("multipart/form-data");
//	}



	public final boolean hasResultMessage(HttpServletRequest request)
	{
		List<String> resultMsgList = (List<String>)request.getAttribute(DataKey.KEY_RESULT_MESSAGE);

		return resultMsgList != null && resultMsgList.isEmpty() == false;
	}

	/**
	 * 결과 메시지를 설정한다.
	 *
	 * @param request
	 * @param msg message
	 */
	public final void setResultMessage(HttpServletRequest request, String msg)
	{
		if (msg == null)
		{
			request.removeAttribute(DataKey.KEY_RESULT_MESSAGE);
		}
		else
		{
			List<String> resultMsgList = (List<String>)request.getAttribute(DataKey.KEY_RESULT_MESSAGE);
			if (resultMsgList == null)
			{
				resultMsgList = new LinkedList<String>();
				request.setAttribute(DataKey.KEY_RESULT_MESSAGE, resultMsgList);
			}
			else
			{
				resultMsgList.clear();
			}

			resultMsgList.add(msg);
		}

	}


	/**
	 * 결과 메시지를 추가한다.
	 * 이전에 설정된 메시지가 있으면 새로운 라인에 메시지를 추가한다.
	 *
	 * @param request
	 * @param msg message
	 * @return 전체 메시지
	 */
	public final int addResultMessage(HttpServletRequest request, String msg)
	{
		List<String> resultMsgList = (List<String>)request.getAttribute(DataKey.KEY_RESULT_MESSAGE);
		if (resultMsgList == null)
		{
			resultMsgList = new LinkedList<String>();
			request.setAttribute(DataKey.KEY_RESULT_MESSAGE, resultMsgList);
		}

		resultMsgList.add(msg);

		return resultMsgList.size();
	}



	public final String getResultMessage(HttpServletRequest request)
	{
		List<String> list = (List<String>)request.getAttribute(DataKey.KEY_RESULT_MESSAGE);

		if(list==null || list.isEmpty()) {
			return null;
		}
		return list.get(list.size()-1);
	}



	// =========================================================================================================

	/**
	* /WEB-INF/config-resource/resource-config.xml 파일에 설정된 resource, property의 값을 얻는다.
	*
	* @param request
	* @param resourceName
	* @param propertyName
	*
	* @return resourceName/propertyName에 대한 설정값, 해당 resource/property가 없는 경우 null을 리턴.
	*/
	protected final String getResourceString(HttpServletRequest request, String resourceName, String propertyName)
	{
		return ResourceHelper.getString(request, resourceName, propertyName);
	}

	/**
	* /WEB-INF/config-resource/resource-config.xml 파일에 설정된 resource, property의 값을 얻는다.
	*
	* @param request
	* @param resourceName
	* @param propertyName
	*
	* @return resourceName/propertyName에 대한 설정값, 해당 resource/property가 없는 경우 defaultValue를 리턴.
	*/
	protected final String getResourceString(HttpServletRequest request, String resourceName, String propertyName, String defaultValue)
	{
		return ResourceHelper.getString(request, resourceName, propertyName, defaultValue);
	}



//	// =========================================================================================================
//
//	private final static String KEY_REQUEST_JSON = new StringBuilder().append(CoreAction.class.getName()).append(".requestJSON").toString();
//
//	protected final JSONObject getJSONObjectFromRequestBody(HttpServletRequest req) throws IOException
//	{
//		BufferedReader buf = null;
//		try
//		{
//			if (this.isMultipartRequest(req))
//			{
//				if (theLogger.isDebugEnabled()) theLogger.debug("aaa");
//				return null;
//			}
//
//			JSONObject reqJSON = (JSONObject)req.getAttribute(KEY_REQUEST_JSON);
//			if (reqJSON != null)
//			{
//				if (theLogger.isDebugEnabled()) theLogger.debug("bbb");
//				return reqJSON;
//			}
//
//			String line = null;
//			StringBuilder sb = new StringBuilder();
//			buf = new BufferedReader(new InputStreamReader(req.getInputStream()));
//
//			while ((line = buf.readLine()) != null)
//			{
//				sb.append(line);
//			}
//
//			if (sb.length() == 0)
//			{
//				if (theLogger.isDebugEnabled()) theLogger.debug("ccc");
//				return null;
//			}
//
//			try
//			{
//				String jsonString = sb.toString();
//				//if (theLogger.isDebugEnabled()) theLogger.debug("json string ==========>"+jsonString+"<====");
//
//				jsonString = URLDecoder.decode(jsonString, "UTF-8");
//				//if (theLogger.isDebugEnabled()) theLogger.debug("decoded json string ==========>"+jsonString+"<====");
//				reqJSON = (JSONObject)JSONSerializer.toJSON(jsonString);
//
//				//if (theLogger.isDebugEnabled()) theLogger.debug("reqJSON : "+reqJSON);
//				if (reqJSON != null)
//				{
//					req.setAttribute(KEY_REQUEST_JSON, reqJSON);
//				}
//
//				return reqJSON;
//			}
//			catch (Exception e)
//			{
//				e.printStackTrace();
//				if (theLogger.isDebugEnabled())
//				{
//					theLogger.debug("ddd");
//				}
//				return reqJSON;
//			}
//		}
//		finally
//		{
//			try
//			{
//				if (buf != null) buf.close();
//			}
//			catch (Exception e) {}
//		}
//	}
//
//
//






	protected final String getCodeValue(String codeGroupPath, String code, String defaultValue)
	{
		return CodeHelper.getStringValue(codeGroupPath, code, defaultValue);
	}
	protected final String getCodeValue(String codeGroupPath, String code)
	{
		return CodeHelper.getStringValue(codeGroupPath, code, null);
	}



	private final static String KEY_AUTH_NO_DATA = String.format("%s.authNoData", SecurityHelper.class.getName());

//	protected final String makeSecurityAuthNo(int secureLength)
//	{
//		return SecurityHelper.generateAuthNo(secureLength);
//	}

	/**
	 * 인증번호를 생성하고, 생성된 인증번호 정보를 HTTP Session 에 보관한다.
	 *
	 * @param request
	 * @param deviceNo 인증 단말기 번호 (휴대전화번호, 이메일주소 등)
	 * @param secureLength 인증번호 길이
	 * @return
	 */
	public final DataMap<String,Object> makeAuthNoData(HttpServletRequest request, String deviceNo, int secureLength, int limitSecond)
	{
		String authNo = SecurityHelper.generateAuthNo(secureLength);
		java.util.Date reqDate = new java.util.Date();

		DataMap<String,Object> authNoData = new DataMap<String,Object>();
		authNoData.setString("auth_no", authNo);
		authNoData.setString("device_no", deviceNo.replace("-", "").toLowerCase());
		authNoData.setInt("limit_second", limitSecond);

		authNoData.setString("req_time", TimeHelper.format(reqDate, "yyyyMMddHHmmss"));
		authNoData.setString("exp_time", TimeHelper.format(TimeHelper.addSecond(reqDate, limitSecond), "yyyyMMddHHmmss"));

		HttpSession aSession = request.getSession(true);
		aSession.setAttribute(KEY_AUTH_NO_DATA, authNoData);

		return authNoData;
	}

	public final void removeAuthNoData(HttpServletRequest request)
	{
		HttpSession aSession = request.getSession(false);
		if (aSession != null)
		{
			aSession.removeAttribute(KEY_AUTH_NO_DATA);
		}
	}


	/**
	 * 생성되어 HTTP Session에 보관 중인 인증번호 정보를 얻는다.
	 *
	 * @param request
	 * @return
	 */
	public final DataMap<String,Object> getAuthNoData(HttpServletRequest request)
	{
		HttpSession aSession = request.getSession(false);
		if (aSession == null)
		{
			return null;
		}

		DataMap<String,Object> authNoData = (DataMap<String,Object>)aSession.getAttribute(KEY_AUTH_NO_DATA);
		return authNoData;
	}

	/**
	 * 인증번호가 유효한지 여부를 얻는다.
	 *
	 * @param request
	 * @param authNo
	 * @return
	 */
	public final boolean isValidAuthNo(HttpServletRequest request, String deviceNo, String authNo)
	{
		if (TextHelper.isEmpty(deviceNo) || TextHelper.isEmpty(authNo))
		{
			return false;
		}

		DataMap<String,Object> authNoData = this.getAuthNoData(request);
		if (authNoData == null)
		{
			return false;
		}

		// check device no
		if (authNoData.isEmptyValue("device_no") || authNoData.getString("device_no").equals(deviceNo.replace("-", "").toLowerCase()) == false)
		{
			return false;
		}

		// check auth_no
		if (authNoData.getString("auth_no", "").equals(authNo) == false)
		{
			return false;
		}

		// check expire time
		int currentTime = TextHelper.parseInt(TimeHelper.getCurrentTime("yyyyMMddHHmmss"), -1);
		if (authNoData.getInt("exp_time", 0) >= currentTime)
		{
			return true;
		}

		// 유효기간이 지난 경우 삭제한다.
		this.removeAuthNoData(request);

		return false;
	}











	public final DataMap<String,Object> getCaptchaData(HttpServletRequest request)
	{
		HttpSession aSession = request.getSession();
		if (aSession == null)
		{
			return null;
		}

		DataMap<String,Object> captchaData = (DataMap<String,Object>)aSession.getAttribute(CaptchaServlet.KEY_CAPTCHA_DATA);
		return captchaData;
	}

	public final void removeCaptchaData(HttpServletRequest request)
	{
		HttpSession aSession = request.getSession();
		if (aSession == null)
		{
			return;
		}
		aSession.removeAttribute(CaptchaServlet.KEY_CAPTCHA_DATA);
	}

	public final String getCaptchaAnswer(HttpServletRequest request)
	{
		DataMap<String,Object> captchaData = this.getCaptchaData(request);
		return captchaData == null ? null : captchaData.getString(CaptchaServlet.KEY_CAPTCHA_ANSWER);
	}

	public final boolean isValidCaptchaAnswer(HttpServletRequest request, String answer)
	{
		String sessionAnswer = this.getCaptchaAnswer(request);
		if (TextHelper.isEmpty(sessionAnswer) || TextHelper.isEmpty(answer))
		{
			return false;
		}

		return sessionAnswer.equals(answer);
	}
}
