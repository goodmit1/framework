package com.fliconz.fm.common.core;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fliconz.fm.common.util.DataMap;

public class KGApplicationEventListener implements ApplicationListener<KGApplicationEvent> {

	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());
	private DataMap<String,List<KGApplicationEventDelegate<KGApplicationEvent>>> m_eventDelegateListMap = null;
	private Executor m_taskExecutor = null;



	public KGApplicationEventListener(Map<String,List<KGApplicationEventDelegate<KGApplicationEvent>>> eventDelegateListMap) {
		super();
		this.m_eventDelegateListMap = new DataMap<String,List<KGApplicationEventDelegate<KGApplicationEvent>>>();
		this.m_eventDelegateListMap.putAll(eventDelegateListMap);
		//if (theLogger.isDebugEnabled()) theLogger.debug("===> EventDelegateListMap :\n{}", eventDelegateListMap);
	}




	public void setTaskExecutor(Executor taskExecutor) {
		this.m_taskExecutor = taskExecutor;
	}

	protected Executor getTaskExecutor() {
		return this.m_taskExecutor;
	}





	@Override
	public void onApplicationEvent(KGApplicationEvent appEvent) {

		List<KGApplicationEventDelegate<KGApplicationEvent>> eventDelegateList = this.m_eventDelegateListMap.get(appEvent.getEventName());
		if (eventDelegateList == null || eventDelegateList.isEmpty()) {
			if (theLogger.isWarnEnabled()) theLogger.warn("\"{}\" ApplicationEventDelegate is not found.\n(ApplicationEvent={})", appEvent.getEventName(), appEvent);
			return;
		}

		if (theLogger.isDebugEnabled()) theLogger.debug("onApplicationEvent (event-name=[{}], event-object=[{}]).", appEvent.getEventName(), appEvent);
		int index = 0;
		//if (theLogger.isDebugEnabled()) theLogger.debug("===> BEGIN - onApplicationEvent Loop ({}) : \"{}\"", eventDelegateList.size(), appEvent.getEventName());
		for (final KGApplicationEventDelegate<KGApplicationEvent> aEventDelegate : eventDelegateList) {
			index++;
			//if (theLogger.isDebugEnabled()) theLogger.debug("===> BEGIN - onApplicationEvent ({}/{}) : \"{}\"", index, eventDelegateList.size(), appEvent.getEventName());
			try {
				if (appEvent.isAsync() == false) {
					aEventDelegate.onDelegatedApplicationEvent(appEvent);
					return;
				}

				Executor aTaskExecutor = getTaskExecutor();
				if (aTaskExecutor == null) {
					if (theLogger.isWarnEnabled()) theLogger.warn("\"{}\" event require a task-executor.", appEvent.getEventName());
					aEventDelegate.onDelegatedApplicationEvent(appEvent);
					return;
				}

				final KGApplicationEvent aAppEvent = appEvent;
				final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

				aTaskExecutor.execute(new Runnable() {
					public void run() {
						try {
							if (auth != null) {
								SecurityContextHolder.getContext().setAuthentication(auth);
							}
							aEventDelegate.onDelegatedApplicationEvent(aAppEvent);
						}
						finally {
						}
					}
				});
			}
			catch (Throwable e) {
				e.printStackTrace();
			}
			//if (theLogger.isDebugEnabled()) theLogger.debug("===> END - onApplicationEvent ({}/{}) : \"{}\"", index, eventDelegateList.size(), appEvent.getEventName());
		}
		//if (theLogger.isDebugEnabled()) theLogger.debug("===> END - onApplicationEvent Loop ({}) : \"{}\"", eventDelegateList.size(), appEvent.getEventName());
	}

}
