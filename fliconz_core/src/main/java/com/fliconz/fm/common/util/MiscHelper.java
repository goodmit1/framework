package com.fliconz.fm.common.util;

public class MiscHelper
{
	public static Throwable getRootCause(Throwable e)
	{
		if (e.getCause() == null)
		{
			return e;
		}

		return MiscHelper.getRootCause(e.getCause());
	}
}
