package com.fliconz.fm.common.geocode;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.LinkedList;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONException;
//import net.sf.json.JSONObject;
//import net.sf.json.JSONSerializer;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;



//http://dna.daum.net/griffin/do/DevDocs/read?bbsId=DevDocs&articleId=27
//		http://apis.daum.net/maps/addr2coord

//http://dna.daum.net/griffin/do/DevDocs/read?bbsId=DevDocs&articleId=26
//		http://apis.daum.net/maps/coord2addr


public class DaumGeocoder extends Geocoder
{
	private String m_mapAPIKey = null;

	public DaumGeocoder(String mapAPIKey)
	{
		super();
		m_mapAPIKey = mapAPIKey;
	}

	// 로컬 인증키 발급
	// https://apis.daum.net/register/apikey.daum?serviceid=local
	public String getAPIKey()
	{
		return m_mapAPIKey;
	}

	@Override
	public boolean canPaging(boolean isGeocode)
	{
		// 주소-->좌표 변환만 페이징 처리
		return isGeocode ? true : false;
	}

	@Override
	public DataMap<String,Object> geocode(GeoLocation addr) throws GeocodeException
	{
		// 주소 --> 좌표
		HttpClient aHttpClient = null;
		String responseString = null;
		org.json.JSONObject response = null;

		try
		{
			DataMap<String,Object> locAttr = addr.getAttributes();
			int pageno = locAttr.getInt(KEY_REQUEST_PAGE, 1);
			int pagesize = locAttr.getInt(KEY_REQUEST_COUNT, 30);
			String encoding = locAttr.getString(KEY_ENCODING,"UTF-8");

			StringBuilder sb = new StringBuilder()
					.append("https://dapi.kakao.com/v2/local/search/address.json?")
					.append("query=").append(URLEncoder.encode(TextHelper.nvl(addr.getAddress(), ""), encoding))
					.append("&page=").append(pageno)
					.append("&size=").append(pagesize)
					;

			aHttpClient = HttpClientBuilder.create().build();

			HttpGet httpGet = new HttpGet(sb.toString());
			httpGet.setHeader("Authorization", String.format("KakaoAK %s", m_mapAPIKey));

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			responseString = aHttpClient.execute(httpGet, responseHandler);

			if (theLogger.isDebugEnabled()) theLogger.debug("===> responseString: {}", responseString);

			response = new org.json.JSONObject( responseString );
			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (response.has("meta") && response.getJSONObject("meta").getInt("total_count") > 0)
			{
				int resultcnt = response.getJSONObject("meta").getInt("total_count");

				resultData.setString(KEY_RESPONSE_CODE, "ok");
				resultData.setString(KEY_RESPONSE_MESSAGE, "success");

				resultData.setInt(KEY_REQUEST_PAGE, pageno);
				resultData.setInt(KEY_RESPONSE_COUNT, resultcnt);
				resultData.setInt(KEY_TOTAL_COUNT, resultcnt);

				List<GeoLocation> areas = new LinkedList<GeoLocation>();
				JSONArray addresses = response.getJSONArray("documents");
				for (int i=0; i<addresses.length(); i++)
				{
					org.json.JSONObject address = addresses.getJSONObject(i);

					GeoLocation aLocation = new GeoLocation();
					aLocation.setAddress(address.getString("address_name"));

					//2019.12.09:지번, 도로명주소 추가
					if (address.has("address") && !address.isNull("address"))
						aLocation.setAreaAddress((DataMap)new ObjectMapper().readValue(((JSONObject)address.get("address")).toString(), DataMap.class));
					if (address.has("road_address") && !address.isNull("road_address"))
						aLocation.setRoadAddress((DataMap)new ObjectMapper().readValue(((JSONObject)address.get("road_address")).toString(), DataMap.class));

					aLocation.setLongitude((float)address.getDouble("x"));
					aLocation.setLatitude((float)address.getDouble("y"));

					areas.add(aLocation);
				}

				resultData.setObject(KEY_LOCATION_LIST, areas);
				return resultData;

			} else {
				resultData.setString(KEY_RESPONSE_CODE, "failed");
				resultData.setString(KEY_RESPONSE_MESSAGE, "failed");
				resultData.setInt(KEY_REQUEST_PAGE, pageno);
				resultData.setInt(KEY_RESPONSE_COUNT, 0);
				resultData.setInt(KEY_TOTAL_COUNT, 0);
			}

			return resultData;
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}

	@Override
	public DataMap<String,Object> reverseGeocode(GeoLocation coord) throws GeocodeException
	{
		//2018.10.13:이광우, 카카오API수정 적용
		// 좌표 --> 주소
		HttpClient aHttpClient = null;

		//2018.10.15:반환값 디버깅을 위해 TRY외부 선언
		String responseString = null;
		org.json.JSONObject response = null;

		try
		{
			StringBuilder sb = new StringBuilder()
				.append("https://dapi.kakao.com/v2/local/geo/coord2address.json?")
				.append("x=").append(coord.getLongitude())
				.append("&y=").append(coord.getLatitude())
				.append("&input_coord=WGS84")
				;

			aHttpClient = HttpClientBuilder.create().build();

			HttpGet httpGet = new HttpGet(sb.toString());
			httpGet.setHeader("Authorization", String.format("KakaoAK %s", m_mapAPIKey));

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			responseString = aHttpClient.execute(httpGet, responseHandler);

			if (theLogger.isDebugEnabled()) theLogger.debug("===> responseString: {}", responseString);

			//JSONObject response = (JSONObject) JSONSerializer.toJSON( responseString );
			response = new org.json.JSONObject( responseString );

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (response.has("meta") && response.getJSONObject("meta").getInt("total_count") > 0)
			{
				resultData.setString(KEY_RESPONSE_CODE, "ok");
				resultData.setString(KEY_RESPONSE_MESSAGE, "success");

				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, 1);
				resultData.setInt(KEY_TOTAL_COUNT, 1);

				List<GeoLocation> aList = new LinkedList<GeoLocation>();

				GeoLocation aLocation = new GeoLocation();

				JSONArray addresses = response.getJSONArray("documents");
				JSONObject address = (JSONObject)addresses.get(0);

				//2018.10.15:위경도로 도로명주소 지정이 되지 않는 경우 고려
				JSONObject roadaddr = null;
				if (address.get("road_address") instanceof JSONObject)
					roadaddr = address.getJSONObject("road_address");

				JSONObject areaaddr = address.getJSONObject("address");

				if (roadaddr == null)
					aLocation.setAddress(String.format("%s (%s)", areaaddr.get("address_name"), areaaddr.get("region_3depth_name")));
				else
					if (roadaddr.get("building_name") != null && !"".equals(roadaddr.get("building_name").toString().trim()))
						aLocation.setAddress(String.format("%s (%s, %s)", roadaddr.get("address_name"), areaaddr.get("region_3depth_name"), roadaddr.get("building_name")));
					else
						aLocation.setAddress(String.format("%s (%s)", roadaddr.get("address_name"), areaaddr.get("region_3depth_name")));

				//기본 구주소등록
				aLocation.setDefaultAddress((String)areaaddr.get("region_1depth_name"), (String)areaaddr.get("region_2depth_name"), (String)areaaddr.get("region_3depth_name"));

				aLocation.setLatitude(coord.getLatitude());
				aLocation.setLongitude(coord.getLongitude());

				aList.add(aLocation);
				resultData.setObject(KEY_LOCATION_LIST, aList);
			}
//			else if (response.has("error"))
//			{
//				org.json.JSONObject err = response.getJSONObject("error");
//				resultData.setString(KEY_RESPONSE_CODE, err.getString("code"));
//				resultData.setString(KEY_RESPONSE_MESSAGE, err.getString("message"));
//				resultData.setInt(KEY_TOTAL_COUNT, 0);
//			}
			else
			{
				resultData.setString(KEY_RESPONSE_CODE, "fail");
				resultData.setString(KEY_RESPONSE_MESSAGE, "fail");
				resultData.setInt(KEY_TOTAL_COUNT, 0);
			}

			return resultData;
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}
/*
WEB_INF_HOME=/Volumes/data/projects/SmartJM/development/web/smartjm/WebContent/WEB-INF
THIS_CLASSPATH=$WEB_INF_HOME/classes:$WEB_INF_HOME/lib/httpclient-4.0.3.jar:$WEB_INF_HOME/lib/httpcore-4.0.1.jar:$WEB_INF_HOME/lib/commons-logging-1.1.1.jar:$WEB_INF_HOME/lib/json-lib-2.3-jdk15.jar:$WEB_INF_HOME/lib/commons-lang-2.4.jar:$WEB_INF_HOME/lib/ezmorph-1.0.6.jar:$WEB_INF_HOME/lib/commons-collections-3.2.1.jar:$WEB_INF_HOME/lib/commons-beanutils-1.8.0.jar

java -cp $THIS_CLASSPATH com.fliconz.fm.common.geocode.DaumGeocoder
*/
	public static void main(String[] args) throws Exception
	{
		DaumGeocoder geocoder = new DaumGeocoder("b1b861826d1342317e85bde974dd82c55b0cfebc");
		GeoLocation aLocation = new GeoLocation();
		DataMap<String,Object> resultData = null;



		System.out.println("=========================");
		System.out.println(geocoder.getClass().getName());


		// http://apis.daum.net/maps/addr2coord?output=json&apikey=b1b861826d1342317e85bde974dd82c55b0cfebc&q=효자동&pageno=1&result=10
		// http://apis.daum.net/local/geo/addr2coord?output=json&apikey=b1b861826d1342317e85bde974dd82c55b0cfebc&q=효자동&pageno=1&result=10
		aLocation.setAddress("효자동");
		resultData = geocoder.geocode(aLocation);
		System.out.println("=========================");
		System.out.println("geocode : "+ resultData.toString());


		// http://apis.daum.net/maps/coord2addr?output=json&apikey=b1b861826d1342317e85bde974dd82c55b0cfebc&latitude=37.87015110&longitude=127.73687180&inputCoordSystem=WGS84
		// http://apis.daum.net/local/geo/coord2addr?output=json&apikey=b1b861826d1342317e85bde974dd82c55b0cfebc&latitude=37.87015110&longitude=127.73687180&inputCoordSystem=WGS84
		aLocation.setLatitude(37.87015110F);
		aLocation.setLongitude(127.73687180F);
		resultData = geocoder.reverseGeocode(aLocation);

		System.out.println();
		System.out.println("=========================");
		System.out.println("reverse geocode : "+ resultData.toString());
	}
}
