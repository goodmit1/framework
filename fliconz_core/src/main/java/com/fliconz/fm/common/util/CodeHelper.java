package com.fliconz.fm.common.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class CodeHelper
{
	/*
	 cd_key
	 cd_val
	 leaf_bit
	 code.list
	 */
	private static CodeHelper ms_codeHelper = null;
	private ListTable<String,Object> m_codeTable = null;

	private CodeHelper()
	{
		m_codeTable = new ListTable<String,Object>();
	}

	public static CodeHelper getInstance()
	{
		synchronized(CodeHelper.class)
		{
			if(ms_codeHelper == null)
			{
				ms_codeHelper = new CodeHelper();
			}
		}

		return ms_codeHelper;
	}

	public void init(DataMap<String,Object> codeData)
	{
		m_codeTable.clear();

		if (codeData != null)
		{
			//setCodeTable("", m_codeTable, codeData.getList("code.list"));
			setCodeTable(m_codeTable, codeData.getList("code.list"));
		}
	}

//	private String getIndent(int depth)
//	{
//		String depthString = "";
//		for (int i=0; i<depth; i++)
//		{
//			depthString = String.format("/%s", depthString);
//		}
//		return depthString;
//	}
	//private void setCodeTable(String parentPath, ListTable<String,Object> codeTable, List<DataMap<String,Object>> codeList)
	private void setCodeTable(ListTable<String,Object> codeTable, List<DataMap<String,Object>> codeList)
	{
		if (codeList == null || codeList.isEmpty())
		{
			return;
		}

		for (Iterator<DataMap<String,Object>> it = codeList.iterator(); it.hasNext();)
		{
			DataMap<String,Object> codeData = it.next();
			String codeKey = codeData.getString("cd_key");
			//System.out.println(String.format("===> codeData: %s = %s", codeKey, codeData));
			if (codeData.getInt("leaf_bit", 0) == 1)
			{
				codeData.remove("leaf_bit");
				codeData.remove("sort_priority");
				Code aCode = new Code(codeData);
				codeTable.put(codeKey, aCode);
				//codeTable.put(codeData.getString("cd_key"), codeData.getString("cd_val"));
				//if (theLogger.isDebugEnabled()) theLogger.debug("===> {}/{} = {}", parentPath, codeKey, aCode);
			}
			else
			{
				codeData.remove("leaf_bit");
				codeData.remove("sort_priority");
				List<DataMap<String,Object>> childCodeList = codeData.getList("code.list");
				if (childCodeList == null || childCodeList.isEmpty())
				{
					continue;
				}

				ListTable<String,Object> childCodeTable = new ListTable<String,Object>();
				//System.out.println(String.format("===> childCodeTable: %s = %s", codeKey, childCodeTable));
				codeTable.put(codeKey, childCodeTable);
				//setCodeTable(String.format("%s/%s", parentPath, codeKey), childCodeTable, childCodeList);
				setCodeTable(childCodeTable, childCodeList);
			}
		}
	}





	// ===============================================================================================

	public ListTable<String,Object> getCodeTable()
	{
		return m_codeTable;
	}



//	public static boolean isProductionMode()
//	{
//		return TextHelper.parseBoolean(System.getProperty("productionMode", "false"), false);
//	}





	public static ListTable<String,Object> getCodeTable(String codeKeyPath)
	{
		if (codeKeyPath.startsWith("/"))
		{
//			System.out.println("====> aaa-000");
			codeKeyPath = codeKeyPath.substring(1);
		}
		String[] arrCodeKey = TextHelper.split(codeKeyPath, "/", false);

		ListTable<String,Object> codeTable = CodeHelper.getInstance().getCodeTable();
		if (arrCodeKey == null)
		{
//			System.out.println("====> aaa");
			return codeTable;
		}

//		System.out.println("====> aaa-111 : ["+codeKeyPath+"]=["+ TextHelper.join(arrCodeKey, "/")+"]");
		for (int i=0; i<arrCodeKey.length; i++)
		{
//			System.out.println("====> bbb-000 : ["+(i)+"]=["+arrCodeKey[i]+"]");
			Object obj = codeTable.get(arrCodeKey[i]);
			if (obj == null || (obj instanceof ListTable) == false)
			{
//				System.out.println("====> bbb");
				return null;
			}
			codeTable = (ListTable<String,Object>)obj;
		}

//		System.out.println("====> ccc");
		return codeTable;
	}


	public static Code getCode(String codeKeyPath)
	{
		if (codeKeyPath.startsWith("/"))
		{
			codeKeyPath = codeKeyPath.substring(1);
		}
		String[] arrCodeKey = TextHelper.split(codeKeyPath, "/", false);
		if (arrCodeKey == null || arrCodeKey.length <= 0)
		{
			return null;
		}

		ListTable<String,Object> codeTable = CodeHelper.getInstance().getCodeTable();
		for (int i=0; i<arrCodeKey.length-1; i++)
		{
			Object obj = codeTable.get(arrCodeKey[i]);
			if (obj == null || (obj instanceof ListTable) == false)
			{
				return null;
			}
			codeTable = (ListTable<String,Object>)obj;
		}

		//return codeTable.getString(arrCodeKey[arrCodeKey.length-1]);
		Object obj = codeTable.get(arrCodeKey[arrCodeKey.length-1]);
		return obj == null || (obj instanceof Code) == false ? null : (Code)obj;
	}

	public static String getCodeValue(String codeKeyPath, String defaultValue)
	{
//		if (codeKeyPath.startsWith("/"))
//		{
//			codeKeyPath = codeKeyPath.substring(1);
//		}
//		String[] arrCodeKey = TextHelper.split(codeKeyPath, "/", false);
//		if (arrCodeKey == null || arrCodeKey.length <= 0)
//		{
//			return defaultValue;
//		}
//
//		ListTable<String,Object> codeTable = CodeHelper.getInstance().getCodeTable();
//		for (int i=0; i<arrCodeKey.length-1; i++)
//		{
//			Object obj = codeTable.get(arrCodeKey[i]);
//			if (obj == null || (obj instanceof ListTable) == false)
//			{
//				return defaultValue;
//			}
//			codeTable = (ListTable<String,Object>)obj;
//		}
//
//		return codeTable.getString(arrCodeKey[arrCodeKey.length-1]);
		Code aCode = CodeHelper.getCode(codeKeyPath);
		return aCode == null ? defaultValue : aCode.getValue();
	}

	public static String getCodeValue(String codeKeyPath)
	{
		return CodeHelper.getCodeValue(codeKeyPath, null);
	}



	public static String getCodeAttr(String codeKeyPath, String attrName, String defaultValue)
	{
		Code aCode = CodeHelper.getCode(codeKeyPath);
		return aCode == null ? defaultValue : aCode.getAttr(attrName, defaultValue);
	}

	public static String getCodeAttr(String codeKeyPath, String attrName)
	{
		Code aCode = CodeHelper.getCode(codeKeyPath);
		return aCode == null ? null : aCode.getAttr(attrName, null);
	}



	public static String getValue(String codeKeyPath, String subKey, String defaultValue)
	{
		return getStringValue(codeKeyPath, subKey, defaultValue);
	}

	public static String getValue(String codeKeyPath, int subKey, String defaultValue)
	{
		return getStringValue(codeKeyPath, subKey, defaultValue);
	}


	public static String getValue(String codeKeyPath, String subKey)
	{
		return getStringValue(codeKeyPath, subKey, null);
	}

	public static String getValue(String codeKeyPath, int subKey)
	{
		return getStringValue(codeKeyPath, subKey, null);
	}







	public static String getStringValue(String codeKeyPath, String subKey, String defaultValue)
	{
		ListTable<String,Object> codeTable = CodeHelper.getCodeTable(codeKeyPath);
		if (codeTable == null)
		{
			return defaultValue;
		}

		if (subKey == null)
		{
			return defaultValue;
		}

		Code aCode = (Code)codeTable.get(subKey);
		return aCode == null ? defaultValue : aCode.getValue();
	}

	public static String getStringValue(String codeKeyPath, int subKey, String defaultValue)
	{
		return getStringValue(codeKeyPath, String.valueOf(subKey), defaultValue);
	}


	public static String getStringValue(String codeKeyPath, String subKey)
	{
//		ListTable<String,Object> codeTable = CodeHelper.getCodeTable(codeKeyPath);
//		if (codeTable == null)
//		{
//			return null;
//		}
//
//		return codeTable.getString(subKey, null);
		return getStringValue(codeKeyPath, subKey, null);
	}

	public static String getStringValue(String codeKeyPath, int subKey)
	{
		return getStringValue(codeKeyPath, String.valueOf(subKey), null);
	}




	public static int getIntValue(String codeKeyPath, String subKey, int defaultValue)
	{
		ListTable<String,Object> codeTable = CodeHelper.getCodeTable(codeKeyPath);
		if (codeTable == null)
		{
			return defaultValue;
		}

		if (subKey == null)
		{
			return defaultValue;
		}

		Code aCode = (Code)codeTable.get(subKey);
		return aCode == null ? defaultValue : TextHelper.parseInt(aCode.getValue(), defaultValue);
	}




	public static float getFloatValue(String codeKeyPath, String subKey, float defaultValue)
	{
		ListTable<String,Object> codeTable = CodeHelper.getCodeTable(codeKeyPath);
		if (codeTable == null)
		{
			return defaultValue;
		}

		if (subKey == null)
		{
			return defaultValue;
		}

		Code aCode = (Code)codeTable.get(subKey);
		return aCode == null ? defaultValue : TextHelper.parseFloat(aCode.getValue(), defaultValue);
	}




	public static double getDoubleValue(String codeKeyPath, String subKey, double defaultValue)
	{
		ListTable<String,Object> codeTable = CodeHelper.getCodeTable(codeKeyPath);
		if (codeTable == null)
		{
			return defaultValue;
		}

		if (subKey == null)
		{
			return defaultValue;
		}

		Code aCode = (Code)codeTable.get(subKey);
		return aCode == null ? defaultValue : TextHelper.parseDouble(aCode.getValue(), defaultValue);
	}




	public static boolean getBooleanValue(String codeKeyPath, String subKey, boolean defaultValue)
	{
		ListTable<String,Object> codeTable = CodeHelper.getCodeTable(codeKeyPath);
		if (codeTable == null)
		{
			return defaultValue;
		}

		if (subKey == null)
		{
			return defaultValue;
		}

		Code aCode = (Code)codeTable.get(subKey);
		return aCode == null ? defaultValue : TextHelper.parseBoolean(aCode.getValue(), defaultValue);
	}




	private final static String TEMPLATE_START_STRING = "#{";
	private final static String TEMPLATE_END_STRING = "}";
	private final static String TEMPLATE_MIDDLE_STRING = "@";
	private final static int TEMPLATE_START_STRING_LENGTH = TEMPLATE_START_STRING.length();
	private final static int TEMPLATE_END_STRING_LENGTH = TEMPLATE_END_STRING.length();


	public static String parseTemplate(String template, Map args)
	{
		return CodeHelper.parseTemplate(template, args, "");
	}
	public static String parseTemplate(String template, Map args, String defaultValue)
	{
		String strTemplate = template;
		boolean isContinue = false;

		do
		{
			isContinue = false;
			int codeGroupStartIndex = strTemplate.indexOf(TEMPLATE_START_STRING);
			int codeGroupEndIndex = strTemplate.indexOf(TEMPLATE_END_STRING, codeGroupStartIndex);
			if (codeGroupStartIndex >= 0 && codeGroupEndIndex > TEMPLATE_START_STRING_LENGTH && codeGroupEndIndex > codeGroupStartIndex)
			{
				String codeString = strTemplate.substring(codeGroupStartIndex+TEMPLATE_START_STRING_LENGTH, codeGroupEndIndex);
				//System.out.println(String.format("==[%s]==", codeString));

				int codeGroupMiddleIndex = codeString.indexOf(TEMPLATE_MIDDLE_STRING);
				if (codeGroupMiddleIndex > 0)
				{
					String codeGroup = codeString.substring(0, codeGroupMiddleIndex);
					String codeKey = codeString.substring(codeGroupMiddleIndex+1);
					StringBuilder sb = new StringBuilder();
					sb.append(strTemplate.substring(0,codeGroupStartIndex));
					//sb.append(String.format("==[%s]@[%s]==", codeGroup, codeKey));
					Object aCodeValue = args.get(codeKey);
					sb.append(aCodeValue == null ? defaultValue : CodeHelper.getStringValue(codeGroup, aCodeValue.toString(), defaultValue));
					sb.append(strTemplate.substring(codeGroupEndIndex+TEMPLATE_END_STRING_LENGTH));
					strTemplate = sb.toString();
					isContinue = true;
				}
			}
		}
		while (isContinue);

		return strTemplate;
	}


//
//	public static boolean getCodeBooleanValue(String codeKeyPath, boolean defaultValue)
//	{
//		String codeValue = CodeHelper.getCodeValue(codeKeyPath, null);
//		return codeValue == null
//			? defaultValue
//			: TextHelper.parseBoolean(codeValue, defaultValue);
//	}
//
//	public static boolean getCodeBooleanValue(String codeKeyPath)
//	{
//		return getCodeBooleanValue(codeKeyPath, false);
//	}
//
//
//
//
//
//	public static int getCodeIntValue(String codeKeyPath, int defaultValue)
//	{
//		String codeValue = CodeHelper.getCodeValue(codeKeyPath, null);
//		return codeValue == null
//			? defaultValue
//			: TextHelper.parseInt(codeValue, defaultValue);
//	}
//
//	public static int getCodeIntValue(String codeKeyPath)
//	{
//		return getCodeIntValue(codeKeyPath, 0);
//	}
//
//
//
//
//	public static long getCodeLongValue(String codeKeyPath, long defaultValue)
//	{
//		String codeValue = CodeHelper.getCodeValue(codeKeyPath, null);
//		return codeValue == null
//			? defaultValue
//			: TextHelper.parseLong(codeValue, defaultValue);
//	}
//
//	public static long getCodeLongValue(String codeKeyPath)
//	{
//		return getCodeIntValue(codeKeyPath, 0);
//	}
//
//
//
//
//
//
//	public static float getCodeFloatValue(String codeKeyPath, float defaultValue)
//	{
//		String codeValue = CodeHelper.getCodeValue(codeKeyPath, null);
//		return codeValue == null
//			? defaultValue
//			: TextHelper.parseFloat(codeValue, defaultValue);
//	}
//
//	public static float getCodeFloatValue(String codeKeyPath)
//	{
//		return getCodeFloatValue(codeKeyPath, 0.0F);
//	}
//
//
//
//
//
//
//	public static double getCodeDoubleValue(String codeKeyPath, double defaultValue)
//	{
//		String codeValue = CodeHelper.getCodeValue(codeKeyPath, null);
//		return codeValue == null
//			? defaultValue
//			: TextHelper.parseDouble(codeValue, defaultValue);
//	}
//
//	public static double getCodeDoubleValue(String codeKeyPath)
//	{
//		return getCodeDoubleValue(codeKeyPath, 0.0);
//	}
//
//	public static String getCodeMsg(String codes,ListTable<String,Object> listTable){
//
//		String returnMsg = "-" ;
//
//		if(codes != null && !"".equals(codes)){
//			StringBuilder sb = new StringBuilder();
//
//			for (Iterator<String> it = listTable.keySet().iterator(); it.hasNext();){
//				String codeKey = it.next();
//				if(codes.contains(codeKey)){
//					sb.append(listTable.getString(codeKey));
//					sb.append(", ");
//				}
//			}
//
//			if (sb.length() > 0)
//			{
//				returnMsg = sb.substring(0,sb.length()-2).toString();
//			}
//		}
//
//		return returnMsg;
//	}

	public class Code
	{
//		String mm_codeKey = null;
//		String mm_codeValue = null;
		DataMap<String,Object> mm_attr = null;
		Object mm_xtraValue = null;

		public final static int CODE_TYPE_TEXT = 0;
		public final static int CODE_TYPE_NUMBER = 1;
		public final static int CODE_TYPE_BOOLEAN = 2;
		public final static int CODE_TYPE_LIST = 3;
		public final static int CODE_TYPE_PROPERTIES = 4;
		public final static int CODE_TYPE_JSON = 5;

		public String toString()
		{
			if (mm_xtraValue == null)
			{
				return String.format("[code=%s]", mm_attr == null ? "null" : mm_attr.toString());
			}

			return String.format("[code=%s, xtra=%s, attrs=%s]"
					, mm_attr == null ? "null" : mm_attr.toString()
					, mm_xtraValue == null ? "null" : mm_xtraValue.toString()
					, mm_attr == null ? "null" : mm_attr.toString());
		}
//		Code(String codeKey, String codeValue, DataMap<String,Object> attr)
//		{
//			mm_codeKey = codeKey;
//			mm_codeValue = codeValue;
//			mm_attr = attr;
//		}
//		Code(String codeKey, String codeValue)
//		{
//			this(codeKey, codeValue, null);
//		}
		Code(DataMap<String,Object> attr)
		{
			mm_attr = attr;
			if (attr == null)
			{
				mm_xtraValue = null;
			}
			else
			{
				int codeType = attr.getInt("cd_tp", CODE_TYPE_TEXT);
				String codeValue = attr.getString("cd_val");
				String xtraValue = attr.getString("xtra_val");
				switch (codeType)
				{
					// list string
					// value1,value2,value3
					case CODE_TYPE_LIST:
						if (codeValue != null)
						{
							// (List<String>)
							mm_xtraValue = TextHelper.splitList(xtraValue == null ? codeValue : xtraValue);
						}
						break;

					// properties string
					// key1=value1,key2=value2,key3=value3...
					case CODE_TYPE_PROPERTIES:
						if (codeValue != null)
						{
							// XProperties<String,String>
							mm_xtraValue = XProperties.split(xtraValue == null ? codeValue : xtraValue);
						}
						break;

					// json string
					case CODE_TYPE_JSON:
						if (codeValue != null)
						{
							// org.json.JSONObject
							try
							{
								mm_xtraValue = new org.json.JSONObject(xtraValue == null ? codeValue : xtraValue);
							}
							catch (org.json.JSONException e)
							{
								throw new IllegalStateException(e.getMessage(), e);
							}
						}
						break;

					default:
						mm_xtraValue = null;
						break;
				}
			}
		}

//		Code()
//		{
//			this(null);
//		}

		public String getKey()
		{
			return getAttr("cd_key");
		}
		public String getValue()
		{
			return getAttr("cd_val");
		}
		public int getIntValue(int defaultValue)
		{
			if (mm_attr == null)
			{
				return defaultValue;
			}

			return mm_attr.getInt("cd_val", defaultValue);
		}
		public int getIntValue()
		{
			return getIntValue(0);
		}

		public Object getXtraValue()
		{
			return mm_xtraValue;
		}
		public List<String> getXtraValueWithList()
		{
			return (List<String>)mm_xtraValue;
		}
		public XProperties getXtraValueWithProperties()
		{
			return (XProperties)mm_xtraValue;
		}
		public org.json.JSONObject getXtraValueWithJSONObject()
		{
			return (org.json.JSONObject)mm_xtraValue;
		}


		public String getAttr(String attrName, String defaultValue)
		{
			if (mm_attr == null)
			{
				return defaultValue;
			}

			return mm_attr.getString(attrName, defaultValue);
		}
		public String getAttr(String attrName)
		{
			return this.getAttr(attrName, null);
		}
	}
}
