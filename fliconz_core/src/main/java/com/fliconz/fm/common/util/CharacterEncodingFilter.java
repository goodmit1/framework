package com.fliconz.fm.common.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CharacterEncodingFilter extends org.springframework.web.filter.CharacterEncodingFilter
{
	private List<String> exceptPatternList = null;

	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

//	private String encoding;
//	private boolean forceEncoding = false;



	public CharacterEncodingFilter()
	{
		super();
		//if (theLogger.isDebugEnabled()) theLogger.debug("===> xxxx");
	}

	@Override
	protected void initFilterBean() throws ServletException
	{
		super.initFilterBean();

		this.exceptPatternList = new LinkedList<String>();
		FilterConfig fConfig = super.getFilterConfig();
		String exceptPattern = fConfig.getInitParameter("exceptPatern");
		if (exceptPattern == null) return;

		String[] arr = TextHelper.split(exceptPattern, "\n");
		for (int i=0; i<arr.length; i++)
		{
			arr[i] = arr[i].trim();
			if (TextHelper.isEmpty(arr[i]))
			{
				continue;
			}
			//if (theLogger.isDebugEnabled()) theLogger.debug("===> exceptPatern: [{}]", arr[i]);
			this.exceptPatternList.add(arr[i]);
		}
	}


//	@Override
//	public void setEncoding(String encoding)
//	{
//		this.encoding = encoding;
//	}
//
//	@Override
//	public void setForceEncoding(boolean forceEncoding)
//	{
//		this.forceEncoding = forceEncoding;
//	}
//
//
//	@Override
//	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException
//	{
//		if ((this.encoding != null) && ((this.forceEncoding) || (request.getCharacterEncoding() == null)))
//		{
//			request.setCharacterEncoding(this.encoding);
//			if (this.forceEncoding)
//			{
//				response.setCharacterEncoding(this.encoding);
//			}
//		}
//		filterChain.doFilter(request, response);
//	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException
	{
		if (this.exceptPatternList.isEmpty())
		{
			//if (theLogger.isDebugEnabled()) theLogger.debug("===> aaa");
			// APPLY : Spring CharacterEncodingFilter
			super.doFilterInternal(request, response, filterChain);
			return;
		}

		String requestPath = request.getRequestURI();
		//if (theLogger.isDebugEnabled()) theLogger.debug("requestPath-000=[{}]", requestPath);

		String contextPath = request.getContextPath();
		//if (theLogger.isDebugEnabled()) theLogger.debug("contextPath=[{}]", contextPath);

		if (contextPath.equals("") == false)
		{
			requestPath = requestPath.substring(contextPath.length());
		}
		//if (theLogger.isDebugEnabled()) theLogger.debug("requestPath-111=[{}]", requestPath);


		if (this.exceptPatternList.indexOf(requestPath) >= 0)
		{
			//if (theLogger.isDebugEnabled()) theLogger.debug("===> bbb");
			filterChain.doFilter(request, response);
			return;
		}

		for (Iterator<String> it = this.exceptPatternList.iterator(); it.hasNext();)
		{
			String exceptPattern = it.next();
			if ( HttpHelper.matchURL(exceptPattern, requestPath, true) )
			{
				//if (theLogger.isDebugEnabled()) theLogger.debug("===> ccc");
				filterChain.doFilter(request, response);
				return;
			}
		}

		//if (theLogger.isDebugEnabled()) theLogger.debug("===> ddd");
		// APPLY : Spring CharacterEncodingFilter
		super.doFilterInternal(request, response, filterChain);
	}
}
