package com.fliconz.fm.common.util;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.ClobTypeHandler;

public class FMClobTypeHandler extends ClobTypeHandler {
	  

	  @Override
	  public String getNullableResult(ResultSet rs, String columnName)
	      throws SQLException {
		  try {
			  return super.getNullableResult(rs, columnName);
		  }
		  catch(SQLException e) {
			  return rs.getString(columnName);
		  }
	  }

	  @Override
	  public String getNullableResult(ResultSet rs, int columnIndex)
	      throws SQLException {
		  try {
			  return super.getNullableResult(rs, columnIndex);
		  }
		  catch(SQLException e) {
			  return rs.getString(columnIndex);
		  }
	  }

	  @Override
	  public String getNullableResult(CallableStatement cs, int columnIndex)
	      throws SQLException {
		  try {
			  return super.getNullableResult(cs, columnIndex);
		  }
		  catch(SQLException e) {
			  return cs.getString(columnIndex);
		  }
	  }
}
