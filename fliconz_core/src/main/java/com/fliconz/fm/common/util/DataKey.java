package com.fliconz.fm.common.util;

public class DataKey {

//	public final static String DEFAULT_PAGE_LIST_COUNT = "DEFAULT_PAGE_LIST_CNT";
//	public final static String DEFAULT_PAGE_SKIP_COUNT = "DEFAULT_PAGE_SKIP_CNT";


	public final static String DEVICE_TYPE_PC = "pc";
	public final static String DEVICE_TYPE_MOBILE = "mobile";

//	public static final String SUPERVISOR_ID = "supervisor";
//	public static final String DEFAULT_USER_PASSWORD = "1111";

	//public final static String KEY_REQUEST_MAP = String.format("%s..request_map", DataKey.class.getName());
	public final static String KEY_REQUEST_PARAM = String.format("%s.request_param", DataKey.class.getName());
	public final static String KEY_RESULT_MESSAGE = String.format("%s.result_message", DataKey.class.getName());

//	public final static String KEY_REDIRECT_URL = String.format("%s.redirect_url", DataKey.class.getName());
	public final static String KEY_REDIRECT_URI = String.format("%s.redirect_uri", DataKey.class.getName());
	public final static String KEY_REDIRECT_PAGE = String.format("%s.redirect_page", DataKey.class.getName());

	public final static String KEY_EXCEL_OBJECT = String.format("%s.excel_object", DataKey.class.getName());
	public final static String KEY_JSON_OBJECT = String.format("%s.json_object", DataKey.class.getName());


//	public final static String KEY_DEVICE_TYPE = "__device_type";
	public final static String KEY_NEXT_URI = "__next_uri";
	public final static String KEY_NEXT_PAGE = "__next_page";
	public final static String KEY_OPEN_DIALOG = "__open_dialog_id";
//	public final static String KEY_REFRESH_PROTECT = "__refresh_protect";
	public final static String KEY_RESPONSE_CODE = "res_code";
	public final static String KEY_RESPONSE_MESSAGE = "res_msg";
	public final static String KEY_RESPONSE_DATA = "res_data";

	public final static String KEY_RESPONSE_NO = "res_no";
	public final static String KEY_RESPONSE_KEY = "res_key";


//	/**
//	 * key (attribute name).
//	 * Page Navigation > 레코드(행) 번호
//	 */
//	public final static String KEY_ROW_NO = "ROW_NO";

	/**
	 * key (attribute name).
	 * Page Navigation > 현재 페이지 시작 레코드(행) 번호
	 */
	public final static String KEY_START_ROW = "start_row";

	/**
	 * key (attribute name).
	 * Page Navigation > 현재 페이지 종료 레코드(행) 번호
	 */
	public final static String KEY_END_ROW = "end_row";


	//public final static String KEY_IS_REVERSE = "is_reverse";
//	public final static String KEY_START_PAGE = "start_page";
//	public final static String KEY_END_PAGE = "end_page";
//	public final static String KEY_TOTAL_PAGE = "total_page";

	/**
	 * key (attribute name).
	 * Page Navigation > 현재 페이지 번호
	 */
	public final static String KEY_PAGE_NO = "page_no";


	public final static String KEY_TOTAL_PAGE_NO = "total_page_no";

	public final static String KEY_IS_REVERSE = "is_reverse";

	/**
	 * key (attribute name).
	 * Page Navigation > 한 페이지당 표시할 레코드(행) 건수
	 */
//	public final static String KEY_ROW_COUNT = "row_cnt";
	public final static String KEY_ROW_COUNT = "row_cnt";

	/**
	 * key (attribute name).
	 * Page Navigation > 화면 하단 페이지 건수
	 */
	public final static String KEY_SCROLL_COUNT = "scroll_cnt";

	/**
	 * key (attribute name).
	 * Page Navigation > 전체 레코드(행) 건수
	 */
	public final static String KEY_TOTAL_COUNT = "total_cnt";


	/**
	 * key (attribute name).
	 * Page Navigation > 페이징 목록
	 */
	public final static String KEY_PAGE_LIST = "page_list";
	public static final String KEY_JSONP_CALLBACK = "jsonp_callback";
}
