/*
 * @(#)XMLProperties.java
 */
package com.fliconz.fm.common.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLProperties extends XProperties
{
	private String m_strName = null;
	private ServletContext m_servletContext = null;

	// property type attribute가 file인 경우, property name=file-path 형태로 보관한다.
	protected XProperties theMetaData = null;

	//##############################################################################################################################

	/**
	* Creates an empty property list with no default values.
	*/
	public XMLProperties()
	{
		super();
	}

	/**
	* Creates an empty property list with the specified defaults.
	*
	* <p>Default properties can not remove and reset (readonly).
	*
	* @param defaults the defaults.
	*/
	public XMLProperties(Properties defaults) {
		super(defaults);
	}


	public String getName()
	{
		return m_strName;
	}

	public void setName(String name)
	{
		m_strName = name;
	}



	public ServletContext getServletContext()
	{
		return m_servletContext;
	}

	public void setServletContext(ServletContext context)
	{
		m_servletContext = context;
		if (context != null)
		{
			theMetaData = new XProperties();
		}
	}



	//##############################################################################################################################


	/**
	* 해당 Reader로부터 xmp properties를 로딩한다.
	*
	* @param rdr Reader
	* @throws IOException
	*/
	@Override
	public synchronized void load(Reader rdr) throws IOException {
		try {
			SAXParserFactory aSAXParserFactory = SAXParserFactory.newInstance();
			aSAXParserFactory.setValidating(false);
			SAXParser aSAXParser = aSAXParserFactory.newSAXParser();

			XMLPropertiesHandler aHandler = new XMLPropertiesHandler(this);
			aHandler.setServletContext(getServletContext());

			InputSource aInputSource = new InputSource(rdr);
			aSAXParser.parse(aInputSource, aHandler);
			m_strName = aHandler.getPropertiesName();
		}
		catch (ParserConfigurationException e) {
			throw new IOException( e.getMessage() );
		}
		catch (SAXException e) {
			throw new IOException( e.getMessage() );
		}
	}

	/**
	* 해당 Reader로부터 xmp properties를 로딩한다.
	*
	* @param in InputStream
	* @throws IOException
	*/
	@Override
	public synchronized void load(InputStream in) throws IOException {
		load( new InputStreamReader(in) );
	}
//
//	/**
//	 * <pre>
//	 *
//	 * 개요 : 해당 Writer에 XML Properties를 저장한다.
//	 * 로직 :
//	 *
//	 * 비고 :
//	 *
//	 * @param pw PrintWriter
//	 * @param encoding encoding name
//	 */
//	public synchronized void store(Writer out, String encoding) {
//		store(out, encoding, null);
//	}


	/**
	 * <pre>
	 *
	 * 개요 : 해당 Writer에 XML Properties를 저장한다.
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param pw PrintWriter
	 * @param encoding encoding name
	 * @param comp 문자열 comparater
	 */
//	public synchronized void store(Writer out, String encoding, StringComparator comp) {
	@Override
	public synchronized void store(Writer out, String encoding) {
		PrintWriter pw = null;
		boolean isNewWriter = false;
		try {
			if (out instanceof PrintWriter) {
				pw = (PrintWriter) out;
				isNewWriter = false;
			}
			else {
				pw = new PrintWriter(out, true);
				isNewWriter = true;
			}

			XMLPropertiesHandler aHandler = new XMLPropertiesHandler(this);
			aHandler.setServletContext(getServletContext());
			aHandler.setPropertiesName(getName());
			aHandler.store(pw, encoding);
//			if (out instanceof OutputStreamWriter && ((OutputStreamWriter)out).getEncoding() != null) {
////				aHandler.store(pw, ((OutputStreamWriter)out).getEncoding(), comp);
//				aHandler.store(pw, ((OutputStreamWriter)out).getEncoding());
//			}
//			else {
////				aHandler.store(pw, encoding, comp);
//				aHandler.store(pw, encoding);
//			}
		}
		finally {
			if (isNewWriter && pw != null) {
				pw.close();
			}
		}
	}

//	public static void main(String[] args) throws Exception
//	{
//		InputStream in = null;
//		try
//		{
//			in = new FileInputStream("D:/xxx/test/property.xml");
//			XMLProperties pt = new XMLProperties();
//			pt.load(in);
//
//			for ( Enumeration en = pt.propertyNames(); en.hasMoreElements(); )
//			{
//				String strName = (String)en.nextElement();
//				System.out.println("["+strName+"]=["+pt.getProperty(strName)+"]");
//			}
//		}
//		finally
//		{
//			try
//			{
//				if (in != null) in.close();
//			}
//			catch (Exception e) {}
//		}
//	}







	@Override
	public synchronized void writeTo(Writer out, String header)
	{
		super.store(out, header);
	}


	@Override
	public synchronized void writeTo(Writer out)
	{
		super.store(out, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------


	@Override
	public synchronized void writeTo(OutputStream out, String header) throws IOException
	{
		super.store( new OutputStreamWriter(out), header);
	}

	@Override
	public synchronized void writeTo(OutputStream out) throws IOException
	{
		super.store(new OutputStreamWriter(out), null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	@Override
	public synchronized void writeTo(File f, String encoding) throws IOException
	{
		super.store(f, encoding);
	}


	@Override
	public synchronized void writeTo(File f) throws IOException
	{
		super.store(f, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------



	@Override
	public synchronized void writeTo(URL u, String encoding) throws IOException
	{
		super.store(u, encoding);
	}


	@Override
	public synchronized void writeTo(URL u) throws IOException
	{
		super.store(u, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------


	@Override
	public synchronized void writeTo(String path, String encoding) throws IOException
	{
		super.store(new File(path), encoding);
	}

	@Override
	public synchronized void writeTo(String path) throws IOException
	{
		super.store(new File(path), null);
	}

}
