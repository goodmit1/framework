package com.fliconz.fm.common.vo;

import java.io.Serializable;
import java.security.Timestamp;
import java.util.Map;

public abstract class BaseVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected Timestamp insTms;
	protected String insPgm;
	protected String insId;
	protected String insIp;
	protected Timestamp updTms;
	protected String updPgm;
	protected String updId;
	protected String updIp;
	protected String pkgNm;
	
	public BaseVO() { }
	
	public BaseVO(Map<String, Object> resultMap){
		
	}
	
	public Timestamp getInsTms() {
		return insTms;
	}
	public void setInsTms(Timestamp insTms) {
		this.insTms = insTms;
	}
	public String getInsPgm() {
		return insPgm;
	}
	public void setInsPgm(String insPgm) {
		this.insPgm = insPgm;
	}
	public String getInsId() {
		return insId;
	}
	public void setInsId(String insId) {
		this.insId = insId;
	}
	public String getInsIp() {
		return insIp;
	}
	public void setInsIp(String insIp) {
		this.insIp = insIp;
	}
	public Timestamp getUpdTms() {
		return updTms;
	}
	public void setUpdTms(Timestamp updTms) {
		this.updTms = updTms;
	}
	public String getUpdPgm() {
		return updPgm;
	}
	public void setUpdPgm(String updPgm) {
		this.updPgm = updPgm;
	}
	public String getUpdId() {
		return updId;
	}
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	public String getUpdIp() {
		return updIp;
	}
	public void setUpdIp(String updIp) {
		this.updIp = updIp;
	}
	public String getPkgNm() {
		return pkgNm;
	}
	public void setPkgNm(String pkgNm) {
		this.pkgNm = pkgNm;
	}
	
}
