package com.fliconz.fm.common.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.TemplateException;

public class MessageHelper
{
	private final Logger theLogger = LoggerFactory.getLogger(this.getClass());
	private static MessageHelper ms_msgHelper = null;
	private DataMap<String,Object> m_msgTable = null;

	private MessageHelper()
	{
		super();
		m_msgTable = new DataMap<String,Object>();
	}


	public static MessageHelper getInstance()
	{
		synchronized(MessageHelper.class)
		{
			if(ms_msgHelper == null)
			{
				ms_msgHelper = new MessageHelper();
			}
		}

		return ms_msgHelper;
	}


	public void init(DataMap<String,Object> msgData)
	{
		m_msgTable.clear();
		m_msgTable.putAll(msgData);
	}


	public String getMessage(String key, String defaultValue)
	{
		return this.m_msgTable.getString(key, defaultValue);
	}

	public String getMessage(String key)
	{
		return this.getMessage(key);
	}



	public String getTemplateMessage(String key, Object args, String defaultValue)
	{
		String template = this.m_msgTable.getString(key);
		if (template == null)
		{
			return defaultValue;
		}

		try
		{
			return TemplateHelper.process(template, args);
		}
		catch (IOException | TemplateException e)
		{
			if (theLogger.isWarnEnabled())
			{
				e.printStackTrace();
				theLogger.warn("Template processing is failed.");
			}
			return defaultValue;
		}
	}

	public String getTemplateMessage(String key, Object args)
	{
		return this.getTemplateMessage(key, args, null);
	}
}
