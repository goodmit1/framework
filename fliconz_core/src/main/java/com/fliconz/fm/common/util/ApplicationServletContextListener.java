package com.fliconz.fm.common.util;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

public class ApplicationServletContextListener implements ServletContextListener {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationServletContextListener.class);

	private ServletContext servletContext;

	@Override
	public void contextDestroyed(ServletContextEvent event) {
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		this.servletContext = event.getServletContext();
		logger.info("Servlet Context initialized");
	}

	public static ApplicationContext getWebApplicationContext(){
		WebApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
		return ctx;
	}

}
