package com.fliconz.fm.common.util;

import java.math.BigDecimal;

public class NumberHelper {
	
	public static long getLong(Object dc_rate1)
	{
		return getLong(dc_rate1, 0l);
	}
	public static long getLong(Object dc_rate1, long def)
	{
		if(dc_rate1 != null)
		{
			if(dc_rate1 instanceof Integer)
			{
				return ((Integer)dc_rate1 ).longValue();
			}
			else if(dc_rate1 instanceof BigDecimal)
			{
				return ((BigDecimal)(dc_rate1)).longValue();
			}
			else if(dc_rate1 instanceof Long)
			{
				return (long)(dc_rate1);
			}
			else
			{
				return Long.parseLong(dc_rate1.toString());
			}
		}
		return def;
	}
	public static float getFloat(Object dc_rate1)
	{
		return getFloat(dc_rate1, 0f);
	}
	public static float getFloat(Object dc_rate1, float def)
	{
		if(dc_rate1 != null)
		{
			if(dc_rate1 instanceof Integer)
			{
				return (int)(dc_rate1);
			}
			else if(dc_rate1 instanceof BigDecimal)
			{
				return ((BigDecimal)(dc_rate1)).floatValue();
			}
			else if(dc_rate1 instanceof Float)
			{
				return (float)(dc_rate1);
			}
			else
			{
				return Float.parseFloat(dc_rate1.toString());
			}
		}
		return def;
	}
	public static boolean in(int state, int... compare)
	{
		for(int c : compare)
		{
			if(state==c)
			{
				return true;
			}
		}
		return false;
	}
	public static boolean notIn(int state, int... compare)
	{
		for(int c : compare)
		{
			if(state==c)
			{
				return false;
			}
		}
		return true;
	}
	public static int getInt(Object dc_rate1)
	{
		return getInt(dc_rate1, 0);
	}

	public static int getInt(Object dc_rate1, int defaultValue)
	{
		if(dc_rate1 != null)
		{
			if(dc_rate1 instanceof Integer)
			{
				return (int)(dc_rate1);
			}
			else if(dc_rate1 instanceof Long)
			{
				return ((Long)(dc_rate1)).intValue();
			}
			else if(dc_rate1 instanceof Double)
			{
				return ((Double)(dc_rate1)).intValue();
			}
			else if(dc_rate1 instanceof BigDecimal)
			{
				return ((BigDecimal)(dc_rate1)).intValue();
			}
			else
			{
				if("".equals(dc_rate1.toString())){
					return defaultValue;
				}
				return Integer.parseInt(dc_rate1.toString());
			}
		}
		return defaultValue;
	}


	public static String getNanoTime()
	{
		String milliSecond = String.valueOf(System.currentTimeMillis());
		String nanoSecond = String.valueOf(System.nanoTime());
		int nanoSecondLength = nanoSecond.length();

		return String.format("%s%s", milliSecond, nanoSecond.substring(nanoSecondLength-6));
	}
}
