/*
 * @(#)ListEnumeration.java
 */
package com.fliconz.fm.common.util;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * <pre>
 * [개요]
 * List를 java.util.Enumeration, java.util.Iterator 인터페이스로 표현
 *
 * [상세설명]
 *
 *
 *
 * [변경이력]
 * 2008.06.11::이경구::최초작성
 *
 *
 * @author 이경구
 * </pre>
 */
public class ListEnumeration<E> implements Enumeration<E>, Iterator<E>
{
	private List<E> m_list = null;
	private boolean m_isReverse = false;

	private int m_intSize = 0;
	private int m_intIndex = 0;

	//##############################################################################################################################

	/**
	* list enumeration constructor.
	*
	* @param lst <code>java.util.List</code> interface.
	*/
	public ListEnumeration(List<E> lst) {
		this(lst, false);
	}

	/**
	* list enumeration constructor.
	*
	* @param lst <code>java.util.List</code> interface.
	* @param reverse 정렬 방향 (true 내림차순, false 오름차순)
	*/
	public ListEnumeration(List<E> lst, boolean reverse) {
		if (lst != null) {
			m_list = lst;
			m_isReverse = reverse;

			m_intIndex = ( m_intSize = m_list == null ? 0 : m_list.size() ) > 0
				? reverse ? m_intSize-1 : 0
				: -1;
		}
	}

	//##############################################################################################################################

	/**
	* Tests if this enumeration contains more elements.
	*
	* @return <code>true</code> if and only if this enumeration object contains at least one more element to provide; <code>false</code> otherwise.
	*/
	public boolean hasMoreElements() {
		return m_intIndex >= 0 && m_intIndex < m_intSize;
	}

	/**
	* Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
	*
	* @return the next element of this enumeration.
	* @exception <code>java.util.NoSuchElementException</code> if no more elements exist.
	*/
	public E nextElement() {
		if ( !hasMoreElements() ) {
			throw new NoSuchElementException("The next element is not found.");
		}

		return (E) m_list.get(m_isReverse ? m_intIndex-- : m_intIndex++);
	}

	//##############################################################################################################################

	/**
	* Calculates the number of times that this enumeration's nextElement method can be called before it generates an exception.
	* The current position is not advanced.
	*
	* @return the number of elements remaining in this enumeration.
	*/
	public int countElements() {
		return hasMoreElements()
			? m_isReverse ? m_intIndex+1 : m_intSize - m_intIndex
			: 0;
	}

	//##############################################################################################################################

	/**
	* Tests if this enumeration contains more elements.
	*
	* @return <code>true</code> if and only if this enumeration object contains at least one more element to provide; <code>false</code> otherwise.
	*/
	public boolean hasNext() {
		return hasMoreElements();
	}

	/**
	* Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
	*
	* @return the next element of this enumeration.
	* @exception <code>java.util.NoSuchElementException</code> if no more elements exist.
	*/
	public E next() {
		return nextElement();
	}

	/**
	 * Removes from the underlying collection the last element returned by the iterator (optional operation).
	 * This method can be called only once per call to next.
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling this method.
	 */
	public void remove() {
		throw new UnsupportedOperationException("remove() method is not supported");
	}
}
