package com.fliconz.fm.common.scheduler;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.quartz.Calendar;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.ScheduleBuilder;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.triggers.CoreTrigger;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.quartz.spi.MutableTrigger;
import org.quartz.spi.OperableTrigger;

import com.fliconz.fm.common.util.TextHelper;

public class XCronTrigger implements Targetable, CronTrigger, Serializable, Cloneable, Comparable<Trigger>, CoreTrigger, MutableTrigger, OperableTrigger, Trigger
{
//	/**
//	 * SLF4J (Standard Logging Facade for JAVA) logger
//	 */
//	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	private CronTriggerImpl m_cronTrigger = null;
	private List<String> m_lstTarget = null;
	private String m_targetName = null;

	public XCronTrigger(CronTriggerImpl trigger)
	{
		super();
		m_cronTrigger = trigger;
		m_lstTarget = new LinkedList<String>();
	}


	@Override
	public Iterator<String> getTargetNames()
	{
		return m_lstTarget.iterator();
	}

	@Override
	public boolean hasTarget(String targetName)
	{
		return m_lstTarget.contains(targetName);
	}



	@Override
	public String getTargets()
	{
		return m_targetName;
	}

	@Override
	public void setTargets(String targetName)
	{
		m_lstTarget.clear();
		m_targetName = targetName;

		if (targetName == null)
		{
			return;
		}
		m_targetName = targetName.trim();

		String[] arr = TextHelper.split(m_targetName, ",");
		if (arr == null || arr.length <= 0)
		{
			return;
		}

		for (int i=0; i<arr.length; i++)
		{
			if (TextHelper.isEmpty(arr[i])) continue;
			m_lstTarget.add(arr[i].trim());
		}
	}

	// ==========================================================================================

	@Override
	public int compareTo(Trigger other)
	{
		return m_cronTrigger.compareTo(other);
	}


	@Override
	public String getCalendarName()
	{
		return m_cronTrigger.getCalendarName();
	}


	@Override
	public String getDescription()
	{
		return m_cronTrigger.getDescription();
	}


	@Override
	public Date getEndTime()
	{
		return m_cronTrigger.getEndTime();
	}


	@Override
	public Date getFinalFireTime()
	{
		return m_cronTrigger.getFinalFireTime();
	}


	@Override
	public Date getFireTimeAfter(Date afterTime)
	{
		return m_cronTrigger.getFireTimeAfter(afterTime);
	}


	@Override
	public JobDataMap getJobDataMap()
	{
		return m_cronTrigger.getJobDataMap();
	}


	@Override
	public JobKey getJobKey()
	{
		return m_cronTrigger.getJobKey();
	}


	@Override
	public TriggerKey getKey()
	{
		return m_cronTrigger.getKey();
	}


	@Override
	public int getMisfireInstruction()
	{
		return m_cronTrigger.getMisfireInstruction();
	}


	@Override
	public Date getNextFireTime()
	{
		return m_cronTrigger.getNextFireTime();
	}


	@Override
	public Date getPreviousFireTime()
	{
		return m_cronTrigger.getPreviousFireTime();
	}


	@Override
	public int getPriority()
	{
		return m_cronTrigger.getPriority();
	}


	@Override
	public ScheduleBuilder<? extends Trigger> getScheduleBuilder()
	{
		return m_cronTrigger.getScheduleBuilder();
	}


	@Override
	public Date getStartTime()
	{
		return m_cronTrigger.getStartTime();
	}


	@Override
	public boolean mayFireAgain()
	{
		return m_cronTrigger.mayFireAgain();
	}


	@Override
	public String getCronExpression()
	{
		return m_cronTrigger.getCronExpression();
	}


	@Override
	public String getExpressionSummary()
	{
		return m_cronTrigger.getExpressionSummary();
	}


	@Override
	public TimeZone getTimeZone()
	{
		return m_cronTrigger.getTimeZone();
	}


	@Override
	public TriggerBuilder<CronTrigger> getTriggerBuilder()
	{
		return m_cronTrigger.getTriggerBuilder();
	}


	@Override
	public boolean hasAdditionalProperties()
	{
		return m_cronTrigger.hasAdditionalProperties();
	}


	@Override
	public Date computeFirstFireTime(Calendar cal)
	{
		return m_cronTrigger.computeFirstFireTime(cal);
	}


	@Override
	public CompletedExecutionInstruction executionComplete(JobExecutionContext context, JobExecutionException result)
	{
		return m_cronTrigger.executionComplete(context, result);
	}


	@Override
	public String getFireInstanceId()
	{
		return m_cronTrigger.getFireInstanceId();
	}


	@Override
	public void setFireInstanceId(String id)
	{
		m_cronTrigger.setFireInstanceId(id);

	}


	@Override
	public void setNextFireTime(Date nextFireTime)
	{
		m_cronTrigger.setNextFireTime(nextFireTime);
	}


	@Override
	public void setPreviousFireTime(Date previousFireTime)
	{
		m_cronTrigger.setPreviousFireTime(previousFireTime);
	}


	@Override
	public void triggered(Calendar cal)
	{
		m_cronTrigger.triggered(cal);
	}


	@Override
	public void updateAfterMisfire(Calendar cal)
	{
		m_cronTrigger.updateAfterMisfire(cal);
	}


	@Override
	public void updateWithNewCalendar(Calendar cal, long misfireThreshold)
	{
		m_cronTrigger.updateWithNewCalendar(cal, misfireThreshold);
	}


	@Override
	public void validate() throws SchedulerException
	{
		m_cronTrigger.validate();
	}


	@Override
	public void setCalendarName(String calendarName)
	{
		m_cronTrigger.setCalendarName(calendarName);
	}


	@Override
	public void setDescription(String description)
	{
		m_cronTrigger.setDescription(description);
	}


	@Override
	public void setEndTime(Date endTime)
	{
		m_cronTrigger.setEndTime(endTime);
	}


	@Override
	public void setJobDataMap(JobDataMap jobDataMap)
	{
		m_cronTrigger.setJobDataMap(jobDataMap);
	}


	@Override
	public void setJobKey(JobKey key)
	{
		m_cronTrigger.setJobKey(key);
	}


	@Override
	public void setKey(TriggerKey key)
	{
		m_cronTrigger.setKey(key);
	}


	@Override
	public void setMisfireInstruction(int misfireInstruction)
	{
		m_cronTrigger.setMisfireInstruction(misfireInstruction);
	}


	@Override
	public void setPriority(int priority)
	{
		m_cronTrigger.setPriority(priority);
	}


	@Override
	public void setStartTime(Date startTime)
	{
		m_cronTrigger.setStartTime(startTime);
	}

	@Override
	public Object clone()
	{
		XCronTrigger obj = new XCronTrigger((CronTriggerImpl)m_cronTrigger.clone());
		obj.setTargets(m_targetName);

		return obj;
	}
}
