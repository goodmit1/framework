/*
* @(#)GetPropertyTag.java
 */
package com.fliconz.fm.common.tag;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.fliconz.fm.common.util.DataKey;

public class DisplayResultMessageTag extends BodyTagSupport
{
	/**
	* Construct a <code>DisplayResultMessageTag</code>.
	*/
	public DisplayResultMessageTag()
	{
		super();
	}


	/**
	* Release a value attribute.
	*
	* Called on a Tag handler to release state.
	* The page compiler guarantees that JSP page implementation objects will invoke this method on all tag handlers,
	* but there may be multiple invocations on <code>doStartTag</code> and <code>doEndTag</code> in between.
	*/
	public void release()
	{
		super.release();
	}


	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{
		try
		{
			List<String> resultMsgList = (List<String>)pageContext.getRequest().getAttribute(DataKey.KEY_RESULT_MESSAGE);
			if (resultMsgList == null || resultMsgList.isEmpty())
			{
				return SKIP_BODY;
			}

			StringBuilder sb = new StringBuilder();
			for (Iterator<String> it = resultMsgList.iterator(); it.hasNext();)
			{
				sb.append(it.next());
				if (it.hasNext())
				{
					sb.append("\n");
				}
			}

			JspWriter jw = pageContext.getOut();
			jw.print("alert(\"");
			jw.print(sb.toString().replace("\r","\\r").replace("\n","\\n"));
			jw.print("\");");

			return SKIP_BODY;
		}
		catch (IOException e)
		{
			throw new JspTagException( e.getMessage() );
		}
	}

}
