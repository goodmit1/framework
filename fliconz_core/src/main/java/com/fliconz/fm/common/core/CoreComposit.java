package com.fliconz.fm.common.core;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.fliconz.fm.common.util.ResourceHelper;


public class CoreComposit implements ApplicationContextAware
{
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());
	private ApplicationContext m_appContext = null;

	public CoreComposit()
	{

	}

	@Override
	public final void setApplicationContext(ApplicationContext appContext)
	{
		m_appContext = appContext;
		if (theLogger.isDebugEnabled()) theLogger.debug("===> ApplicationContext=[{}]", getApplicationContext());

	}

	public final ApplicationContext getApplicationContext()
	{
		return m_appContext;
	}



	/**
	* /WEB-INF/config-resource/resource-config.xml 파일에 설정된 resource, property의 값을 얻는다.
	*
	* @param request
	* @param resourceName
	* @param propertyName
	*
	* @return resourceName/propertyName에 대한 설정값, 해당 resource/property가 없는 경우 null을 리턴.
	*/
	protected final String getResourceString(HttpServletRequest request, String resourceName, String propertyName)
	{
		return ResourceHelper.getString(request, resourceName, propertyName);
	}

	/**
	* /WEB-INF/config-resource/resource-config.xml 파일에 설정된 resource, property의 값을 얻는다.
	*
	* @param request
	* @param resourceName
	* @param propertyName
	*
	* @return resourceName/propertyName에 대한 설정값, 해당 resource/property가 없는 경우 defaultValue를 리턴.
	*/
	protected final String getResourceString(HttpServletRequest request, String resourceName, String propertyName, String defaultValue)
	{
		return ResourceHelper.getString(request, resourceName, propertyName, defaultValue);
	}


	protected final String getResourceString(String locale, String resourceName, String propertyName)
	{
		return ResourceHelper.getString(locale, resourceName, propertyName, null);
	}

	protected final String getResourceString(String locale, String resourceName, String propertyName, String defaultValue)
	{
		return ResourceHelper.getString(locale, resourceName, propertyName, defaultValue);
	}
}
