package com.fliconz.fm.common.core;

import java.lang.annotation.Annotation;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

public class RequestAttributeArgumentResolver implements WebArgumentResolver
{
//	/**
//	 * SLF4J (Standard Logging Facade for JAVA) logger
//	 */
//	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	public Object resolveArgument(MethodParameter param, NativeWebRequest request) throws Exception
	{
		Annotation[] paramAnns = param.getParameterAnnotations();
		Class<?> paramType = param.getParameterType();
//if (theLogger.isDebugEnabled()) theLogger.debug("==> resolveArgument : paramType=[{}]", paramType);

		for (Annotation paramAnn : paramAnns)
		{
			if (RequestAttribute.class.isInstance(paramAnn))
			{
//if (theLogger.isDebugEnabled()) theLogger.debug("==> resolveArgument : class=[{}], paramAnn=[{}]", paramAnn.getClass().getName(), paramAnn);

				RequestAttribute aRequestAttribute = (RequestAttribute)paramAnn;
				HttpServletRequest aRequest = (HttpServletRequest) request.getNativeRequest();
				Object attrValue = aRequest.getAttribute(aRequestAttribute.value());

//                if (result == null)
//                {
//                	result = reqAttr.defaultValue();
//                }


				if (attrValue == null && aRequestAttribute.required())
				{
					raiseMissingParameterException(aRequestAttribute.value(), paramType);
				}
				else
				{
					return attrValue;
				}
			}
		}

		return WebArgumentResolver.UNRESOLVED;
	}

	protected void raiseMissingParameterException(String paramName, Class<?> paramType) throws Exception
	{
		throw new IllegalStateException(
				new StringBuilder()
					.append("Missing parameter '")
					.append(paramName)
					.append("' of type [")
					.append(paramType.getName())
					.append("]")
					.toString()
				);
	}
}
