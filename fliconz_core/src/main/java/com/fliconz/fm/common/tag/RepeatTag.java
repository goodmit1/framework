/*
 * @(#)RepeatTag.java
 */
package com.fliconz.fm.common.tag;

import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.fliconz.fm.common.util.ArrayEnumeration;
import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.ListEnumeration;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.common.util.XEnumeration;


public class RepeatTag extends BodyTagSupport
{
//	private String m_strClass = null;
//	private boolean m_isDeclare = true;

	private Object m_oValue = null;
	private boolean m_isReverse = false;

	private Enumeration m_enum = null;
	private boolean m_isEmpty = false;
	private boolean m_isRepeated = false;


	//##############################################################################################################################

	/**
	* Construct a <code>RepeatTag</code>.
	*/
	public RepeatTag() {
		super();
	}

	//##############################################################################################################################

	/**
	* Release a class, iteration value, id, desc, declare attribute and other attribute value.
	*
	* Called on a Tag handler to release state.
	* The page compiler guarantees that JSP page implementation objects will invoke this method on all tag handlers,
	* but there may be multiple invocations on <code>doStartTag</code> and <code>doEndTag</code> in between.
	*/
	public void release() {
		super.release();
//		super.setId(null);
//		m_strClass = null;
//		m_isDeclare = true;

		m_oValue = null;
		m_isReverse = false;
		m_enum = null;
		m_isEmpty = false;
		m_isRepeated = false;
	}

	//##############################################################################################################################

	public void setValue(Object value) {
		m_oValue = value;
	}

	public void setReverse(boolean reverse) {
		m_isReverse = reverse;
	}

	protected void setRepeated() {
		m_isRepeated = true;
	}

	protected boolean isRepeated() {
		return m_isRepeated;
	}

//	public void setId(String id)
//	{
//		super.setId(id);
//	}
//
//	public void setClass(String klass)
//	{
//		m_strClass = klass;
//	}
//
//	public void setDeclare(boolean isDeclare)
//	{
//		m_isDeclare = isDeclare;
//	}

	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{
		m_enum = null;
		if (m_oValue == null) {
			m_isEmpty = true;
			return EVAL_BODY_BUFFERED;
		}

		if (m_oValue instanceof List) {
			m_enum = new ListEnumeration((List) m_oValue, m_isReverse);
		}

//		else if (m_oValue instanceof ListDataTransfer)
//		{
//			m_enum = ((ListDataTransfer)m_oValue).keys(m_isReverse);
//		}

		else if (m_oValue instanceof DataMap) {
			m_enum = ((DataMap)m_oValue).keys();
		}

		else if (m_oValue instanceof Collection) {
			if (m_isReverse) {
				throw new JspTagException("Could not descending.");
			}
			m_enum = new XEnumeration((Collection) m_oValue);
		}

		else if ( m_oValue.getClass().isArray() ) {
			setArray();
		}

		else if (m_oValue instanceof Map) {
			if (m_isReverse) {
				throw new JspTagException("Could not descending.");
			}
			m_enum = new XEnumeration((Map) m_oValue);
		}

		else if (m_oValue instanceof Enumeration) {
			if (m_isReverse) {
				throw new JspTagException("Could not descending.");
			}
			m_enum = (Enumeration) m_oValue;
		}

		else if (m_oValue instanceof Iterator) {
			if (m_isReverse) {
				throw new JspTagException("Could not descending.");
			}
			m_enum = new XEnumeration((Iterator) m_oValue);
		}

		else if (m_oValue instanceof String) {
			m_enum = new ArrayEnumeration( TextHelper.split((String)m_oValue, TextHelper.DEFAULT_ARRAY_DELIM, true), m_isReverse );
		}

		else
			throw new JspTagException("unsupported repeatable object.");


		m_isEmpty = !m_enum.hasMoreElements();
		//pageContext.setAttribute( super.getId(), getEnumeration() );

		return EVAL_BODY_BUFFERED;

	}

	public int doAfterBody() throws JspTagException
	{
		try
		{
			bodyContent.writeOut(getPreviousOut());
			bodyContent.clearBody();

			return SKIP_BODY;
		}
		catch(IOException e) {
			throw new JspTagException( e.getMessage() );
		}
	}


	//##############################################################################################################################

	protected Enumeration getEnumeration() {
		return m_enum;
	}

	protected boolean isEmpty() {
		return m_isEmpty;
	}

	private void setArray() {
		if (m_oValue instanceof int[]) {
			m_enum = new ArrayEnumeration((int[]) m_oValue, m_isReverse);
		}

		else if (m_oValue instanceof long[]) {
			m_enum = new ArrayEnumeration((long[]) m_oValue, m_isReverse);
		}

		else if (m_oValue instanceof short[]) {
			m_enum = new ArrayEnumeration((short[]) m_oValue, m_isReverse);
		}

		else if (m_oValue instanceof float[]) {
			m_enum = new ArrayEnumeration((float[]) m_oValue, m_isReverse);
		}

		else if (m_oValue instanceof double[]) {
			m_enum = new ArrayEnumeration((double[]) m_oValue, m_isReverse);
		}

		else if (m_oValue instanceof boolean[]) {
			m_enum = new ArrayEnumeration((boolean[]) m_oValue, m_isReverse);
		}

		else if (m_oValue instanceof char[]) {
			m_enum = new ArrayEnumeration((char[]) m_oValue, m_isReverse);
		}

		else if (m_oValue instanceof byte[]) {
			m_enum = new ArrayEnumeration((byte[]) m_oValue, m_isReverse);
		}
		else
		{
			m_enum = new ArrayEnumeration((Object[]) m_oValue, m_isReverse);
		}
	}
}
