package com.fliconz.fm.common.geocode;

import com.fliconz.fm.common.util.DataMap;

// DataMap<String,Object>을 상속받아야 JSONObject로 변환 가능
public class GeoLocation extends DataMap<String, Object>
// implements java.io.Serializable
{
	// private DataMap<String,Object> super = null;

	public GeoLocation() {
		super();
		// super = new DataMap<String,Object>();
		super.put("attributes", new DataMap<String, Object>());
	}

	public String getAddress() {
		return super.getString("address");
	}

	public DataMap getAreaAddress() {
		return (DataMap) super.get("area_address");
	}

	public DataMap getRoadAddress() {
		return (DataMap) super.get("road_address");
	}

	public String getDefaultAddress() {
		return super.getString("default_address");
	}

	public String getDefaultLevel1() {
		return super.getString("default_level1");
	}

	public String getDefaultLevel2() {
		return super.getString("default_level2");
	}

	public String getDefaultLevel3() {
		return super.getString("default_level3");
	}

	public float getLatitude() {
		return super.getFloat("latitude");
	}

	public float getLongitude() {
		return super.getFloat("longitude");
	}

	public float getAltitude() {
		return super.getFloat("altitude");
	}

	public void setAddress(Object address) {
		super.put("address", address);
	}

	public void setAreaAddress(DataMap address) {
		super.put("area_address", address);
	}

	public void setRoadAddress(DataMap address) {
		super.put("road_address", address);
	}

	public void setDefaultAddress(String level1, String level2, String level3) {
		setDefaultLevel1(level1);
		setDefaultLevel2(level2);
		setDefaultLevel3(level3);
		super.setString("default_address", String.format("%s %s %s", level1, level2, level3));
	}

	public void setDefaultLevel1(String address) {
		super.setString("default_level1", address);
	}

	public void setDefaultLevel2(String address) {
		super.setString("default_level2", address);
	}

	public void setDefaultLevel3(String address) {
		super.setString("default_level3", address);
	}

	public void setLatitude(float latitude) {
		super.setFloat("latitude", latitude);
	}

	public void setLongitude(float longitude) {
		super.setFloat("longitude", longitude);
	}

	public void setAltitude(float altitude) {
		super.setFloat("altitude", altitude);
	}

	public boolean hasAttributes() {
		return this.getAttributes().isNotEmpty();
	}

	public Object getAttribute(String name) {
		DataMap<String, Object> attrs = this.getAttributes();
		return attrs == null ? null : attrs.get(name);
	}

	public void removeAttribute(String name) {
		this.setAttribute(name, null);
	}

	public void setAttribute(String name, Object value) {
		DataMap<String, Object> attrs = this.getAttributes();
		if (value == null) {
			attrs.remove(name);
		} else {
			attrs.put(name, value);
		}
	}

	public void clearAttributes() {
		DataMap<String, Object> attrs = this.getAttributes();
		attrs.clear();
	}

	public DataMap<String, Object> getAttributes() {
		return (DataMap<String, Object>) super.get("attributes");
	}

	public void setAttributes(DataMap<String, Object> attrs) {
		super.put("attributes", attrs);
	}

	public String toString() {
		return super.toString();
	}
}
