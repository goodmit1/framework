package com.fliconz.fm.common.sms;

import java.util.Map;

public interface ISMS {
	public void send(String receiver, String title, String msg, Map btnFields) throws Exception;
	public void send(String receiver, String title, String msg) throws Exception;
}
