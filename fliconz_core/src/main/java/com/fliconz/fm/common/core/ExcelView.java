package com.fliconz.fm.common.core;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.fliconz.fm.common.util.DataKey;
import com.fliconz.fm.common.util.TextHelper;

public class ExcelView extends AbstractExcelView {

	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void buildExcelDocument(Map model, HSSFWorkbook wb, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		//if (theLogger.isDebugEnabled()) theLogger.debug("==> buildExcelDocument");
		AbstractExcelObject excelObject = (AbstractExcelObject) request.getAttribute(DataKey.KEY_EXCEL_OBJECT);
		if (theLogger.isDebugEnabled()) theLogger.debug("==> buildExcelDocument - excelObject: [{}]", excelObject);
		if (excelObject == null) {
			return;
		}

		String excelSheetName = excelObject.getExcelSheetName();
		//if (theLogger.isDebugEnabled()) theLogger.debug("==> excelSheetName: [{}]", excelSheetName);

		// if (StringUtil.isEmpty(excelObject.getFileName())) {
		// setUrl("report");
		// // response.setHeader("Content-Disposition",
		// // "attachment; filename=\"" + StringUtil.toISO8859("리포트", "UTF-8")
		// // + ".xls\";");
		// } else {
		// setUrl(excelObject.getFileName());
		// // setUrl(StringUtil.toISO8859(excelObject.getFileName(),
		// // "EUC-KR"));
		// // setUrl(StringUtil.toISO8859(excelObject.getFileName(), "UTF-8"));
		// // response.setHeader("Content-Disposition",
		// // "attachment; filename=\"" +
		// // StringUtil.toISO8859(excelObject.getFileName(), "UTF-8") +
		// // ".xls\";");
		// }
		if (TextHelper.isEmpty(getContentType()))
		{
			String attachContentType = "application/vnd.ms-excel";
			response.setContentType(String.format("%s;charset=%s", attachContentType, TextHelper.evl(request.getCharacterEncoding(), "utf-8")));
			//response.setHeader("Content-Type", String.format("%s;charset=8859_1", attachContentType));
		}
		else
		{
			response.setContentType(getContentType());
		}

		String client = request.getHeader("User-Agent");
		if (StringUtils.isEmpty(excelObject.getFileName())) {
			excelObject.setFileName("Report");
		}
		//String filename_ISO8859_1 = toISO8859(excelObject.getFileName(), "EUC-KR") + ".xls";
		String filename = String.format("%s.xls", excelObject.getFileName());
		//if (theLogger.isDebugEnabled()) theLogger.debug("==> filename: [{}]", filename);

		if (client.indexOf("MSIE 5.5") != -1) {
			//response.setHeader("Content-Disposition", "filename=\"" + filename_ISO8859_1 + "\";");
			response.setHeader("Content-Disposition", String.format("filename=\"%s\";", filename));
		} else {
			//response.setHeader("Content-Disposition", "attachment;filename=\"" + filename_ISO8859_1 + "\";");
			response.setHeader("Content-Disposition", String.format("attachment;filename=\"%s\";", filename));
		}
		response.setHeader("Content-Transfer-Encoding", "binary;");
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");


		//font 설정  by song

	  /*  HSSFRow header;
		HSSFRow hrow ;
		HSSFFont font = wb.createFont();
		   font.setFontHeightInPoints((short)10);
		   font.setFontName("돋움");
		   font.setBoldweight((short)1);
		HSSFFont font1 = wb.createFont();
		   font1.setFontHeightInPoints((short)10);
		   font1.setFontName("돋움");
		   // Fonts are set into a style so create a new one to use.
		   HSSFCellStyle style = wb.createCellStyle();
		   HSSFCellStyle style1 = wb.createCellStyle();
		   style.setFont(font);
		   style1.setFont(font1);
		*/
	   // HSSFSheet sheet = wb.createSheet();

		//if (theLogger.isDebugEnabled()) theLogger.debug("==> excelSheetName: [{}]", excelSheetName);
	   // wb.setSheetName(0, new String(excelSheetName.getBytes("utf-8"),"utf-16") );
		HSSFSheet sheet = wb.createSheet(excelSheetName);
	   // HSSFSheet sheet = wb.createSheet("한글테스트");

		int row = 0;
		List<String[]> headLineList = excelObject.getHeadLineList();
		if (headLineList != null && headLineList.isEmpty() == false)
		{
			for (String[] headLine : headLineList)
			{
				for (int i=0; i<headLine.length; i++)
				{
					HSSFCell cell = getCell(sheet, row, i);
					cell.setCellValue(headLine[i] == null ? "" : headLine[i]);
					if (theLogger.isDebugEnabled()) theLogger.debug("==> headLine-row: [{}]--[{}]", row, (headLine[i] == null ? "" : headLine[i]));
				}
				row++;
			}
		}

		List<String> titleList = excelObject.getExcelTitle();
		//if (theLogger.isDebugEnabled()) theLogger.debug("==> titleList: [{}]", titleList);
		HSSFCellStyle style = setBoldBackgroundGrey(wb);

		for (int i = 0; i < titleList.size(); i++) {
			HSSFCell cell = getCell(sheet, row, i);
			cell.setCellStyle(style);
			cell.setCellValue(titleList.get(i));
			//cell.setCellValue("한글목차");
		}

		List excelList = excelObject.getExcelList();
		for (int k = 0, len = excelList.size(); k < len; k++) {
			row++;
			//if (theLogger.isDebugEnabled()) theLogger.debug("==> row: [{}]", row);
			List<String> valueList = excelObject.getExcelValue(excelList.get(k), (k + 1));
			for (int i = 0; i < valueList.size(); i++) {
				getCell(sheet, row, i).setCellValue(valueList.get(i));
			}
		}// for
	}

	public HSSFCellStyle setBoldBackgroundGrey(HSSFWorkbook wb) {

		//if (theLogger.isDebugEnabled()) theLogger.debug("==> setBoldBackgroundGrey");
		HSSFCellStyle cellStyle = wb.createCellStyle();
		HSSFFont font = wb.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		//font.setColor(HSSFColor.WHITE.index);
		cellStyle.setFont(font);
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		//cellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
		cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		//cellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		// cellStyle.setFillBackgroundColor(new
		// HSSFColor.GREY_25_PERCENT().getIndex());
		// cellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
		// cellStyle.setFillBackgroundColor(HSSFColor.YELLOW.index);
		// cellStyle.setFillBackgroundColor(HSSFColor.LIGHT_TURQUOISE.index);
		// cellStyle.setFillPattern(HSSFCellStyle.THIN_BACKWARD_DIAG);
		// cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		return cellStyle;
	}

//    public static String toISO8859(String p, String encoding) {
//        try {
//            return new String(p.getBytes(encoding), "8859_1");
//        } catch (java.io.UnsupportedEncodingException uee) {
//            return uee.toString();
//        }
//    }
}
