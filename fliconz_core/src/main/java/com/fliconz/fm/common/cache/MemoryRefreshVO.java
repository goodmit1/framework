package com.fliconz.fm.common.cache;

import java.io.Serializable;

public class MemoryRefreshVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private String client;
	private String kubun;
	private boolean isActive;

	public MemoryRefreshVO(String client, String kubun, boolean isActive) {
		this.client = client;
		this.kubun = kubun;
		this.isActive = isActive;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getKubun() {
		return kubun;
	}

	public void setKubun(String kubun) {
		this.kubun = kubun;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}
