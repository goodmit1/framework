package com.fliconz.fm.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@Service
public class CodeHandler {
	@Autowired
	@Qualifier("commonDAO")
	protected
	CommonDAO dao ;
 
	private Map<String,Map> codeMap;

	public Map<String,Map> getAllList()
	{
		return codeMap;
	}

	@PostConstruct
	public void reload() throws Exception
	{


			String queryKey = PropertyManager.getString("common.code.customQueryKey", "com.fliconz.fm.common.selectCodeList");
			Map param = new HashMap();
			if(PropertyManager.getBoolean("support_multi_lang",false) )
			{
				param.put("MULTI_LANG_YN","Y");
			}
			List<Map> list = dao.selectList(queryKey, param);
			if (codeMap == null)
			{
				codeMap = new HashMap();
			}
			else
			{
				codeMap.clear();
			}

			for(Map m : list)
			{
				addMap(m);

			}
			
		 
	}
	
	private void addMap(Map m)
	{
		Map map = codeMap.get(m.get("GRP_ID") + "." + m.get("LANG_TYPE"));
		if(map == null)
		{
			map = new LinkedHashMap();
			codeMap.put(m.get("GRP_ID") + "." + m.get("LANG_TYPE"), map);
		}
		if(PropertyManager.getBoolean("code_key_first", false))
		{
			map.put( m.get("CD_ID"), m.get("CD_NM"));
		}
		else
		{
			map.put( m.get("CD_NM"), m.get("CD_ID"));
		}
		
	}
	public  Map  getCodeList(String grp, String lang)
	{
		return codeMap.get(grp + "." + lang);
	}

}
