package com.fliconz.fm.common.core;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.LocalizedResourceHelper;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.AbstractView;

import com.fliconz.fm.common.util.DataKey;
import com.fliconz.fm.common.util.TextHelper;

public class ExcelxView extends AbstractView {

	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	/** The content type for an Excel response */
	private static final String CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	/** The extension to look for existing templates */
	private static final String EXTENSION = ".xlsx";
	//private String url;

	public ExcelxView()
	{
		super();
		setContentType(CONTENT_TYPE);
	}


	@Override
	protected boolean generatesDownloadContent() {
		return true;
	}

	/**
	 * Renders the Excel view, given the specified model.
	 */
	@Override
	protected final void renderMergedOutputModel(
			Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		XSSFWorkbook workbook;
		ByteArrayOutputStream baos = createTemporaryOutputStream();
		/*if (this.url != null) {
			workbook = getTemplateSource(this.url, request);
		}
		else {*/
			workbook = new XSSFWorkbook();
			logger.debug("Created Excel Workbook from scratch");
		//}

		buildExcelDocument(model, workbook, request, response);

		// Set the content type.
		//response.setContentType(getContentType());

		// Should we set the content length here?
		// response.setContentLength(workbook.getBytes().length);

		// Flush byte array to servlet output stream.
		//ServletOutputStream out = response.getOutputStream();
		workbook.write(baos);
		writeToResponse(response, baos);
		//out.flush();
	}

	protected XSSFWorkbook getTemplateSource(String url, HttpServletRequest request) throws Exception
	{
		LocalizedResourceHelper helper = new LocalizedResourceHelper(getApplicationContext());
		Locale userLocale = RequestContextUtils.getLocale(request);
		Resource inputFile = helper.findLocalizedResource(url, EXTENSION, userLocale);

		// Create the Excel document from the source.
		if (logger.isDebugEnabled()) {
			logger.debug("Loading Excel workbook from " + inputFile);
		}
		//POIFSFileSystem fs = new POIFSFileSystem(inputFile.getInputStream());
		return new XSSFWorkbook(inputFile.getInputStream());
	}

	protected XSSFCell getCell(XSSFSheet sheet, int row, int col)
	{
		XSSFRow sheetRow = sheet.getRow(row);
		if (sheetRow == null) {
			sheetRow = sheet.createRow(row);
		}
		XSSFCell cell = sheetRow.getCell(col);
		if (cell == null) {
			cell = sheetRow.createCell(col);
		}
		return cell;
	}
	protected void setText(XSSFCell cell, String text)
	{
		cell.setCellType(XSSFCell.CELL_TYPE_STRING);
		cell.setCellValue(text);
	}


	private void buildExcelDocument(Map model, XSSFWorkbook wb, HttpServletRequest request, HttpServletResponse response) throws Exception
	{

		//if (theLogger.isDebugEnabled()) theLogger.debug("==> buildExcelDocument");
		AbstractExcelObject excelObject = (AbstractExcelObject)request.getAttribute(DataKey.KEY_EXCEL_OBJECT);
		if (theLogger.isDebugEnabled()) theLogger.debug("==> buildExcelDocument - excelObject: [{}]", excelObject);
		if (excelObject == null) {
			return;
		}

		String excelSheetName = excelObject.getExcelSheetName();
		//if (theLogger.isDebugEnabled()) theLogger.debug("==> excelSheetName: [{}]", excelSheetName);

		// if (StringUtil.isEmpty(excelObject.getFileName())) {
		// setUrl("report");
		// // response.setHeader("Content-Disposition",
		// // "attachment; filename=\"" + StringUtil.toISO8859("리포트", "UTF-8")
		// // + ".xls\";");
		// } else {
		// setUrl(excelObject.getFileName());
		// // setUrl(StringUtil.toISO8859(excelObject.getFileName(),
		// // "EUC-KR"));
		// // setUrl(StringUtil.toISO8859(excelObject.getFileName(), "UTF-8"));
		// // response.setHeader("Content-Disposition",
		// // "attachment; filename=\"" +
		// // StringUtil.toISO8859(excelObject.getFileName(), "UTF-8") +
		// // ".xls\";");
		// }
		if (TextHelper.isEmpty(getContentType()))
		{
			String attachContentType = CONTENT_TYPE;
			response.setContentType(String.format("%s;charset=%s", attachContentType, TextHelper.evl(request.getCharacterEncoding(), "utf-8")));
			//response.setHeader("Content-Type", String.format("%s;charset=8859_1", attachContentType));
		}
		else
		{
			response.setContentType(getContentType());
		}

		String client = request.getHeader("User-Agent");
		if (StringUtils.isEmpty(excelObject.getFileName())) {
			excelObject.setFileName("Report");
		}
		//String filename_ISO8859_1 = toISO8859(excelObject.getFileName(), "EUC-KR") + ".xls";
		String filename = String.format("%s.xls", excelObject.getFileName());
		//if (theLogger.isDebugEnabled()) theLogger.debug("==> filename: [{}]", filename);

		if (client.indexOf("MSIE 5.5") != -1) {
			//response.setHeader("Content-Disposition", "filename=\"" + filename_ISO8859_1 + "\";");
			response.setHeader("Content-Disposition", String.format("filename=\"%s\";", filename));
		} else {
			//response.setHeader("Content-Disposition", "attachment;filename=\"" + filename_ISO8859_1 + "\";");
			response.setHeader("Content-Disposition", String.format("attachment;filename=\"%s\";", filename));
		}
		response.setHeader("Content-Transfer-Encoding", "binary;");
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");


		//font 설정  by song

	  /*  XSSFRow header;
		XSSFRow hrow ;
		XSSFFont font = wb.createFont();
		   font.setFontHeightInPoints((short)10);
		   font.setFontName("돋움");
		   font.setBoldweight((short)1);
		XSSFFont font1 = wb.createFont();
		   font1.setFontHeightInPoints((short)10);
		   font1.setFontName("돋움");
		   // Fonts are set into a style so create a new one to use.
		   XSSFCellStyle style = wb.createCellStyle();
		   XSSFCellStyle style1 = wb.createCellStyle();
		   style.setFont(font);
		   style1.setFont(font1);
		*/
	   // XSSFSheet sheet = wb.createSheet();

		//if (theLogger.isDebugEnabled()) theLogger.debug("==> excelSheetName: [{}]", excelSheetName);
	   // wb.setSheetName(0, new String(excelSheetName.getBytes("utf-8"),"utf-16") );
		XSSFSheet sheet = wb.createSheet(excelSheetName);
	   // XSSFSheet sheet = wb.createSheet("한글테스트");

		int row = 0;
		List<String[]> headLineList = excelObject.getHeadLineList();
		if (headLineList != null && headLineList.isEmpty() == false)
		{
			for (String[] headLine : headLineList)
			{
				for (int i=0; i<headLine.length; i++)
				{
					XSSFCell cell = getCell(sheet, row, i);
					cell.setCellValue(headLine[i] == null ? "" : headLine[i]);
					if (theLogger.isDebugEnabled()) theLogger.debug("==> headLine-row: [{}]--[{}]", row, (headLine[i] == null ? "" : headLine[i]));
				}
				row++;
			}
		}

		List<String> titleList = excelObject.getExcelTitle();
		//if (theLogger.isDebugEnabled()) theLogger.debug("==> titleList: [{}]", titleList);
		XSSFCellStyle style = setBoldBackgroundGrey(wb);

		for (int i = 0; i < titleList.size(); i++) {
			XSSFCell cell = getCell(sheet, row, i);
			cell.setCellStyle(style);
			cell.setCellValue(titleList.get(i));
			//cell.setCellValue("한글목차");
		}

		List excelList = excelObject.getExcelList();
		for (int k = 0, len = excelList.size(); k < len; k++) {
			row++;
			if (theLogger.isDebugEnabled()) theLogger.debug("==> row: [{}]", row);
			List<String> valueList = excelObject.getExcelValue(excelList.get(k), (k + 1));
			for (int i = 0; i < valueList.size(); i++) {
				getCell(sheet, row, i).setCellValue(valueList.get(i));
			}
		}// for
	}

	public XSSFCellStyle setBoldBackgroundGrey(XSSFWorkbook wb) {

		//if (theLogger.isDebugEnabled()) theLogger.debug("==> setBoldBackgroundGrey");
		XSSFCellStyle cellStyle = wb.createCellStyle();
		XSSFFont font = wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		//font.setColor(XSSFColor.WHITE.index);
		cellStyle.setFont(font);
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		//cellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
		cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		//cellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

		// cellStyle.setFillBackgroundColor(new
		// XSSFColor.GREY_25_PERCENT().getIndex());
		// cellStyle.setFillBackgroundColor(XSSFColor.GREY_25_PERCENT.index);
		// cellStyle.setFillBackgroundColor(XSSFColor.YELLOW.index);
		// cellStyle.setFillBackgroundColor(XSSFColor.LIGHT_TURQUOISE.index);
		// cellStyle.setFillPattern(XSSFCellStyle.THIN_BACKWARD_DIAG);
		// cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

		return cellStyle;
	}

//    public static String toISO8859(String p, String encoding) {
//        try {
//            return new String(p.getBytes(encoding), "8859_1");
//        } catch (java.io.UnsupportedEncodingException uee) {
//            return uee.toString();
//        }
//    }
}
