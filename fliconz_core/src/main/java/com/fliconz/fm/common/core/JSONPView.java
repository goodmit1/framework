package com.fliconz.fm.common.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fliconz.fm.common.util.DataKey;

public class JSONPView extends JSONView{
 
	@Override
	protected void writeAfter(PrintWriter writer, Map<String, Object> model) {
	 
		writer.write(")");
	}
	@Override
	protected void writeBefore(PrintWriter writer, Map<String, Object> model) {
		
		Object callback = model.get(DataKey.KEY_JSONP_CALLBACK);
		writer.write(callback + "(");
	
	}
}
