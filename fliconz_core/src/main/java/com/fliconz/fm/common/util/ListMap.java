package com.fliconz.fm.common.util;
/*
* @(#)ListMap.java
*/
import java.util.*;
/**
* ListMap.
*
* @version	1.0
* @since	1.0	2003.06.25 Wed.
* @author	kiki (Kyoung Gu. LEE)
*/
public final class ListMap<K,V> extends HashMap<K,V>
{
	private ListSet<K> m_keySet = null;

	//##############################################################################################################################

	public ListMap()
	{
		this((Map<K,V>)null);
	}

	//public ListMap(Map<K,V> m)
	public ListMap(Map<? extends K,? extends V> m)
	{
		m_keySet = m == null ? new ListSet() : new ListSet( m.keySet() );
	}

	//##############################################################################################################################

	public void clear()
	{
		m_keySet.clear();
		super.clear();
	}

	public Object clone()
	{
		return new ListMap(this);
	}

	public void putAll(Map<? extends K,? extends V> m)
	{
		if (m == null) return;

		for ( Iterator<K> it = (Iterator<K>) m.keySet().iterator(); it.hasNext(); )
		{
			K o = it.next();
			super.put((K) o, m.get(o) );
			m_keySet.add(o);
		}
	}

	public Set keySet()
	{
		return m_keySet;
	}

	public V put(K key, V value)
	{
		V oldValue = super.put(key, value);
		m_keySet.add(key);
		return oldValue;
	}

	public V remove(Object key)
	{
		V oldValue = super.remove(key);
		m_keySet.remove(key);
		return oldValue;
	}










	public V putFirst(K key, V value)
	{
		V oldValue = this.remove(key);
		super.put(key, value);

		m_keySet.addFirst(key);
		return oldValue;
	}

	public V putLast(K key, V value)
	{
		V oldValue = this.remove(key);
		super.put(key, value);

		m_keySet.addLast(key);
		return oldValue;
	}


	public V removeFirst()
	{
		K oKey = m_keySet.getFirst();
		if (oKey == null) return null;

		return this.remove(oKey);
	}

	public V removeLast()
	{
		K oKey = m_keySet.getLast();
		if (oKey == null) return null;

		return this.remove(oKey);
	}













	public String getString(String key, String defaultValue)
	{
		Object aValue = super.get(key);
		if (aValue == null || aValue.equals("")) {
			return defaultValue;
		}

		return aValue.toString();
	}

	public String getString(String key)
	{
		return getString(key, null);
	}




	public boolean getBoolean(String key, boolean defaultValue)
	{
		Object aValue = super.get(key);
		if (aValue == null || aValue.equals(""))
		{
			return defaultValue;
		}

		if (aValue instanceof Boolean)
		{
			return ((Boolean) aValue).booleanValue();
		}

		return TextHelper.parseBoolean(aValue.toString(), defaultValue);
	}

	public boolean getBoolean(String key)
	{
		return getBoolean(key, false);
	}

	public int getInt(String key, int defaultValue)
	{
		Object aValue = super.get(key);
		if (aValue == null || aValue.equals(""))
		{
			return defaultValue;
		}

		if (aValue instanceof Number) {
			return ((Number) aValue).intValue();
		}

		return TextHelper.parseInt(aValue.toString(), defaultValue);
	}

	public int getInt(String key) {
		return getInt(key, 0);
	}

	public long getLong(String key, long defaultValue)
	{
		Object aValue = super.get(key);
		if (aValue == null || aValue.equals(""))
		{
			return defaultValue;
		}

		if (aValue instanceof Number)
		{
			return ((Number) aValue).longValue();
		}

		return TextHelper.parseLong(aValue.toString(), defaultValue);
	}

	public long getLong(String key) {
		return getLong(key, 0L);
	}

	public float getFloat(String key, float defaultValue)
	{
		Object aValue = super.get(key);
		if (aValue == null || aValue.equals(""))
		{
			return defaultValue;
		}

		if (aValue instanceof Number)
		{
			return ((Number) aValue).floatValue();
		}

		return TextHelper.parseFloat(aValue.toString(), defaultValue);
	}

	public float getFloat(String key)
	{
		return getFloat(key, 0.0F);
	}

	public double getDouble(String key, double defaultValue)
	{
		Object aValue = super.get(key);
		if (aValue == null || aValue.equals(""))
		{
			return defaultValue;
		}

		if (aValue instanceof Number)
		{
			return ((Number) aValue).doubleValue();
		}

		return TextHelper.parseDouble(aValue.toString(), defaultValue);
	}

	public double getDouble(String key)
	{
		return getDouble(key, 0.0);
	}



	public List getList(String key)
	{
		return (List)super.get(key);
	}

	public Object getObject(String key)
	{
		return super.get(key);
	}








	public Object setBoolean(String key, boolean value)
	{
		return super.put((K)key, (V)new Boolean(value));
	}

	public Object setInt(String key, int value)
	{
		return super.put((K)key, (V)new Integer(value));
	}

	public Object setLong(String key, long value)
	{
		return super.put((K)key, (V)new Long(value));
	}

	public Object setFloat(String key, float value)
	{
		return super.put((K)key, (V)new Float(value));
	}

	public Object setDouble(String key, double value)
	{
		return super.put((K)key, (V)new Double(value));
	}

	public Object setString(String key, String value)
	{
		return super.put((K)key, (V)value);
	}

	public Object setDataMap(String key, DataMap value)
	{
		return super.put((K)key, (V)value);
	}

	public Object setList(String key, List value)
	{
		return super.put((K)key, (V)value);
	}

	public Object setObject(String key, Object value)
	{
		return super.put((K)key, (V)value);
	}
}
