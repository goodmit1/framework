package com.fliconz.fm.common.util;

import java.util.*;

public class RangeEnumeration implements Enumeration, Iterator
{
	private int m_intFrom = 0;
	private int m_intTo = 0;
	private int m_intIncrease = 0;

	//##############################################################################################################################

	/**
	 * RangeEnumeration constructor
	 *
	 * @param from 숫자범위 시작
	 * @param to 숫자범위 종료
	 * @param increase 증가값
	 */
	public RangeEnumeration(int from, int to, int increase) {
		if (increase > 0) {
			if (from > to) {
				throw new IllegalArgumentException("from argument value must be greater than to argument value.");
			}
		}

		else if (increase < 0) {
			if (from < to) {
				throw new IllegalArgumentException("from argument value must be less than to argument value.");
			}
		}

		else {
			throw new IllegalArgumentException("increase argument value must be not zero.");
		}

		m_intFrom = from - increase;
		m_intTo = to;
		m_intIncrease = increase;
	}

	/**
	 * RangeEnumeration constructor
	 * 증가값은 1로한다.
	 *
	 * @param from 숫자범위 시작
	 * @param to 숫자범위 종료
	 */
	public RangeEnumeration(int from, int to) {
		this(from, to, 1);
	}

	//##############################################################################################################################

	/**
	* Tests if this enumeration contains more elements.
	*
	* @return <code>true</code> if and only if this enumeration object contains at least one more element to provide; <code>false</code> otherwise.
	*/
	public boolean hasMoreElements() {
		return m_intIncrease > 0
			? m_intFrom + m_intIncrease <= m_intTo
			: m_intFrom + m_intIncrease >= m_intTo;
	}

	/**
	* Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
	*
	* @return the next element of this enumeration.
	* @exception <code>java.util.NoSuchElementException</code> if no more elements exist.
	*/
	public Object nextElement() {
		int intValue = m_intFrom + m_intIncrease;
		if (m_intIncrease > 0) {
			if (intValue > m_intTo) {
				throw new NoSuchElementException();
			}
		}
		else {
			if (intValue < m_intTo) {
				throw new NoSuchElementException();
			}
		}

		m_intFrom = intValue;
		return new Integer(intValue);
	}



	/**
	* Tests if this enumeration contains more elements.
	*
	* @return <code>true</code> if and only if this enumeration object contains at least one more element to provide; <code>false</code> otherwise.
	*/
	public boolean hasNext() {
		return hasMoreElements();
	}

	/**
	* Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
	*
	* @return the next element of this enumeration.
	* @exception <code>java.util.NoSuchElementException</code> if no more elements exist.
	*/
	public Object next() {
		return nextElement();
	}

	/**
	 * Removes from the underlying collection the last element returned by the iterator (optional operation).
	 * This method can be called only once per call to next.
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling this method.
	 */
	public void remove() {
		throw new UnsupportedOperationException("remove() method is not supported");
	}
}
