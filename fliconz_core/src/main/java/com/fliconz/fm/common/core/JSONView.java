package com.fliconz.fm.common.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.AbstractView;

import com.fliconz.fm.common.util.DataKey;
import com.fliconz.fm.common.util.TextHelper;

public class JSONView extends AbstractView
{

	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());
	private String m_cacheControl = null;

//	@Override
//	public void setContentType(String contentType)
//	{
//		super.setContentType(contentType);
//	}
	public void setCacheControl(String cacheControl)
	{
		m_cacheControl = cacheControl;
	}

	@Override
	protected void renderMergedOutputModel(Map<String,Object> model, HttpServletRequest request, HttpServletResponse response) throws IOException
	//throws Exception
	{
		Object aValue = model.get(DataKey.KEY_JSON_OBJECT);
		if (aValue == null)
		{
			aValue = model.get("JSONObject");
		}
		if (aValue == null)
		{
			aValue = model.get("JSONArray");
		}
		if (aValue == null)
		{
			aValue = model.get("string");
		}

		//if (theLogger.isDebugEnabled()) theLogger.debug("===> model=[{}]", model);
		//if (theLogger.isDebugEnabled()) theLogger.debug("===> json=[{}] : [{}]", aValue==null ? "null" : aValue.getClass().getName(), aValue);

		//response.reset();
		response.resetBuffer();
		
		if (TextHelper.isNotEmpty(getContentType()))
		{
			response.setContentType(getContentType());
		}
		if (TextHelper.isNotEmpty(m_cacheControl))
		{
			response.setHeader("Cache-Control", m_cacheControl);
		}

		if (aValue == null)
		{
			return;
		}
		writeBefore(response.getWriter(), model);
		if (aValue instanceof org.json.JSONObject)
		{
			response.getWriter().write(((org.json.JSONObject)aValue).toString());
		}
		else if (aValue instanceof org.json.JSONArray)
		{
			response.getWriter().write(((org.json.JSONObject)aValue).toString());
		}
		else if (aValue instanceof org.json.simple.JSONObject)
		{
			response.getWriter().write(((org.json.simple.JSONObject)aValue).toString());
		}
		else if (aValue instanceof org.json.simple.JSONArray)
		{
			response.getWriter().write(((org.json.simple.JSONArray)aValue).toString());
		}
		else if (aValue instanceof java.util.Map)
		{
			response.getWriter().write(org.json.simple.JSONObject.toJSONString((java.util.Map)aValue));
		}
		else if (aValue instanceof java.util.List)
		{
			response.getWriter().write(org.json.simple.JSONArray.toJSONString((java.util.List)aValue));
		}
		else
		{
			//response.getWriter().write(org.json.JSONObject.valueToString(aValue));
			response.getWriter().write(new org.json.JSONObject(aValue).toString());
		}
		writeAfter(response.getWriter(), model);
	}

	protected void writeAfter(PrintWriter writer, Map<String, Object> model) {
	 
		
	}

	protected void writeBefore(PrintWriter writer, Map<String, Object> model) {
		
		
	}
}
