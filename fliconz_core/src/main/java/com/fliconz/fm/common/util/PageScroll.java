package com.fliconz.fm.common.util;

import java.io.Serializable;
import java.util.Enumeration;

public class PageScroll implements Serializable
{

	/**
	* Default row count per page : <code>50</code>.
	*/
	public final static int DEFAULT_ROW_COUNT = 10;

	/**
	* Default scroll count : <code>10</code>.
	*/
	public final static int DEFAULT_SCROLL_COUNT = 5;

	/**
	* Default javascript function name : <code>doChangePage</code>.
	*/
	public final static String DEFAULT_FUNCTION_NAME = "doChangePage";

	/**
	* default tag name.
	*/
	public final static String DEFAULT_TAG_NAME = "pageScroll";

	private int m_intViewPage = 0;
	private int m_intTotalRow = 0;
	private int m_intRowCount = 0;
	private int m_intScrollCount = 0;
	private String m_strFunctionName = null;
	private String m_strTagName = null;

	private int m_intTotalPage = 0;
	private int m_intScrollStartPage = 0;
	private int m_intScrollEndPage = 0;
	private int m_intTotalBlock = 0;
	private boolean m_isReverse = false;

	//##############################################################################################################################

	/**
	 * PageScroll constructor
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param totalRow 전체목록 건수
	 * @param rowCount 한페이지당 나열할 목록 건수
	 * @param scrollCount 페이지 스크롤로 나열할 페이지 건수
	 * @param reverse 목록 정렬방법 (오름차순-false/내림차순-true)
	 */
	public PageScroll(int viewPage, int totalRow, int rowCount, int scrollCount, boolean reverse) {
		m_intViewPage = viewPage <= 0 ? 1 : viewPage;
		m_intTotalRow = totalRow < 0 ? 0 : totalRow;
		m_intRowCount = rowCount <= 0 ? DEFAULT_ROW_COUNT : rowCount;
		m_intScrollCount = scrollCount <= 0 ? DEFAULT_SCROLL_COUNT : scrollCount;
		m_isReverse = reverse;

		calculate();
	}

	/**
	 * PageScroll constructor
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param totalRow 전체목록 건수
	 * @param rowCount 한페이지당 나열할 목록 건수
	 * @param scrollCount 페이지 스크롤로 나열할 페이지 건수
	 * @param reverse 목록 정렬방법 (오름차순-false/내림차순-true)
	 */
	public PageScroll(int viewPage, int totalRow, int rowCount, int scrollCount) {
		this(viewPage, totalRow, rowCount, scrollCount, false);
	}

	/**
	 * PageScroll constructor
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param totalRow 전체목록 건수
	 * @param rowCount 한페이지당 나열할 목록 건수
	 * @param reverse 목록 정렬방법 (오름차순-false/내림차순-true)
	 */
	public PageScroll(int viewPage, int totalRow, int rowCount, boolean reverse) {
		this(viewPage, totalRow, rowCount, 0, reverse);
	}

	/**
	 * PageScroll constructor
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param totalRow 전체목록 건수
	 * @param rowCount 한페이지당 나열할 목록 건수
	 */
	public PageScroll(int viewPage, int totalRow, int rowCount) {
		this(viewPage, totalRow, rowCount, 0, false);
	}

	/**
	 * PageScroll constructor
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param totalRow 전체목록 건수
	 * @param reverse 목록 정렬방법 (오름차순-false/내림차순-true)
	 */
	public PageScroll(int viewPage, int totalRow, boolean reverse) {
		this(viewPage, totalRow, DEFAULT_ROW_COUNT, 0, reverse);
	}

	/**
	 * PageScroll constructor
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param totalRow 전체목록 건수
	 */
	public PageScroll(int viewPage, int totalRow) {
		this(viewPage, totalRow, DEFAULT_ROW_COUNT, 0, false);
	}

	//##############################################################################################################################

	/**
	 * <pre>
	 * 개요 : 현재 페이지 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 현재 페이지 번호
	 */
	public int getViewPage() {
		return m_intViewPage;
	}

	// 현재 페이지의 행 갯수
	/**
	 * <pre>
	 * 개요 : 현재 페이지의 목록 건수를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 현재 페이지의 목록 건수
	 */
	public int getViewPageRow() {
		return isReverse()
			? getStartRow() - getEndRow() + 1
			: getEndRow() - getStartRow() + 1;
	}

	/**
	 * <pre>
	 * 개요 : 전체목록 건수를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 전체목록 건수
	 */
	public int getTotalRow() {
		return m_intTotalRow;
	}

	/**
	 * <pre>
	 * 개요 : 한페이지당 나열할 목록 건수를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 한페이지당 나열할 목록 건수
	 */
	public int getRowCount() {
		return m_intRowCount;
	}

	/**
	 * <pre>
	 * 개요 : 페이지 스크롤로 나열할 페이지 건수를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 페이지 스크롤로 나열할 페이지 건수
	 */
	public int getScrollCount() {
		return m_intScrollCount;
	}

	/**
	 * <pre>
	 * 개요 : 페이지 이동을 처리한 javascript function 이름을 얻는다.
	 *
	 * 로직 : Default - DEFAULT_FUNCTION_NAME
	 *
	 * 비고 : 해당 function은 이동할 대상 페이지 번호를 파라미터로 받는다.
	 *
	 * @return 페이지 이동을 처리한 javascript function 이름
	 */
	public String getFunctionName() {
		return m_strFunctionName == null ? DEFAULT_FUNCTION_NAME : m_strFunctionName;
	}

	/**
	 * <pre>
	 * 개요 : 페이지 이동을 INPUT tag(input/select) 로 처리하는 경우 해당 tag의 이름을 얻는다.
	 *
	 * 로직 : Default - DEFAULT_TAG_NAME
	 *
	 * 비고 :
	 *
	 * @return 페이지 이동을 INPUT tag(input/select) 로 처리하는 경우 해당 tag의 이름
	 */
	public String getTagName() {
		return m_strTagName == null ? DEFAULT_TAG_NAME : m_strTagName;
	}

	/**
	 * <pre>
	 * 개요 : 목록의 정렬방향(오름차순/내림차순)을 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 목록 정렬방법 (오름차순-false/내림차순-true)
	 */
	public boolean isReverse() {
		return m_isReverse;
	}







	/**
	 * <pre>
	 * 개요 : 페이지 이동을 처리한 javascript function 이름을 설정한다.
	 *
	 * 로직 :
	 *
	 * 비고 : 해당 function은 이동할 대상 페이지 번호를 파라미터로 받는다.
	 *
	 * @param functionName 페이지 이동을 처리한 javascript function 이름
	 */
	public void setFunctionName(String functionName) {
		m_strFunctionName = functionName;
	}

	/**
	 * <pre>
	 * 개요 : 페이지 이동을 INPUT tag(input/select) 로 처리하는 경우 해당 tag의 이름을 설정한다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param tagName 페이지 이동을 INPUT tag(input/select) 로 처리하는 경우 해당 tag의 이름
	 */
	public void setTagName(String tagName) {
		m_strTagName = tagName;
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * <pre>
	 * 개요 : 전체목록 건수가 0보다 큰지 여부를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 전체목록 건수가 0보다 큰지 여부
	 */
	public boolean isEmpty() {
		return m_intTotalRow == 0;
	}

	/**
	 * <pre>
	 * 개요 : 페이지 스크롤 범위의 페이지 번호 목록을 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 페이지 스크롤 범위의 페이지 번호 목록
	 */
	public Enumeration getScrollEnumeration() {
		return new RangeEnumeration( getScrollStartPage(), getScrollEndPage(), 1 );
	}


	/**
	 * <pre>
	 * 개요 : 전체목록에 대한 전체 페이지 건수를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 전체목록에 대한 전체 페이지 건수
	 */
	public int getTotalPage() {
		return m_intTotalPage;
	}

	/**
	 * <pre>
	 * 개요 : 페이지 스크롤 시작 페이지 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 페이지 스크롤 시작 페이지 번호
	 */
	public int getScrollStartPage() {
		return m_intScrollStartPage;
	}

	/**
	 * <pre>
	 * 개요 : 페이지 스크롤 종료 페이지 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 페이지 스크롤 종료 페이지 번호
	 */
	public int getScrollEndPage() {
		return m_intScrollEndPage;
	}


	/**
	 * <pre>
	 * 개요 : 페이지 스크롤 블럭 건수를 얻는다.
	 *
	 * 로직 : 페이지 스크롤 블럭 - 페이지 스크롤 집합
	 *
	 * 비고 :
	 *
	 * @return 페이지 스크롤 블럭 건수
	 */
	public int getTotalBlock() {
		return m_intTotalBlock;
	}

	/**
	 * <pre>
	 * 개요 : 첫페이지 번호(1)를 얻는다.
	 *
	 * 로직 : 항상 1
	 *
	 * 비고 :
	 *
	 * @return 첫페이지 번호
	 */
	public int getFirstPage() {
		return 1;
	}

	/**
	 * <pre>
	 * 개요 : 이전 블럭 시작 페이지 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 이전 블럭 시작 페이지 번호
	 */
	public int getPrevBlockPage() {
		return getScrollStartPage() <= 1
			? 1
			: getScrollStartPage() - 1;
	}

	/**
	 * <pre>
	 * 개요 : 이전 페이지 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 이전 페이지 번호
	 */
	public int getPrevPage() {
		return getViewPage() == 1
			? 1
			: getViewPage() - 1;
	}

	/**
	 * <pre>
	 * 개요 : 다음 페이지 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 다음 페이지 번호
	 */
	public int getNextPage() {
		return getViewPage() == getLastPage()
			? getLastPage()
			: getViewPage() + 1;
	}

	/**
	 * <pre>
	 * 개요 : 다음 블럭 시작 페이지 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 다음 블럭 시작 페이지 번호
	 */
	public int getNextBlockPage() {
		return getScrollEndPage() == getLastPage()
			? getScrollEndPage()
			: getScrollEndPage() + 1;
	}

	/**
	 * <pre>
	 * 개요 : 마지막 페이지 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 마지막 페이지 번호
	 */
	public int getLastPage() {
		return getTotalPage();
	}



	/**
	 * <pre>
	 * 개요 : 현재 블록 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param scrollCount 페이지 스크롤로 나열할 페이지 건수
	 * @return 현재 블록 번호
	 */
	public int getViewBlock(int viewPage, int scrollCount) {
		return (int)Math.ceil( (viewPage<=0 ? getViewPage() : viewPage) / (scrollCount<=0 ? getScrollCount() : scrollCount) );
	}

	/**
	 * <pre>
	 * 개요 : 현재 블록 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param viewPage 현재 페이지 번호
	 * @return 현재 블록 번호
	 */
	public int getViewBlock(int viewPage) {
		return getViewBlock( viewPage, getScrollCount() );
	}

	/**
	 * <pre>
	 * 개요 : 현재 블록 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 현재 블록 번호
	 */
	public int getViewBlock() {
		return getViewBlock( getViewPage(), getScrollCount() );
	}





	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 시작 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param reverse 오름차순-false/내림차순-true 구분
	 * @return 전체목록에서 현재 페이지의 시작 행 번호
	 */
	public int getStartRow(int viewPage, boolean reverse) {
		return getStartRow( getTotalRow(), viewPage, getRowCount(), reverse );
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 시작 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param viewPage 현재 페이지 번호
	 * @return 전체목록에서 현재 페이지의 시작 행 번호
	 */
	public int getStartRow(int viewPage) {
		return getStartRow( getTotalRow(), viewPage, getRowCount(), isReverse() );
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 시작 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param reverse 오름차순-false/내림차순-true 구분
	 * @return 전체목록에서 현재 페이지의 시작 행 번호
	 */
	public int getStartRow(boolean reverse) {
		return getStartRow( getTotalRow(), getViewPage(), getRowCount(), reverse );
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 시작 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 전체목록에서 현재 페이지의 시작 행 번호
	 */
	public int getStartRow() {
		return getStartRow( getTotalRow(), getViewPage(), getRowCount(), isReverse() );
	}




	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 마지막 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param viewPage 현재 페이지 번호
	 * @param reverse 오름차순-false/내림차순-true 구분
	 * @return 전체목록에서 현재 페이지의 마지막 행 번호
	 */
	public int getEndRow(int viewPage, boolean reverse) {
		return getEndRow( getTotalRow(), viewPage, getRowCount(), reverse );
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 마지막 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param viewPage 현재 페이지 번호
	 * @return 전체목록에서 현재 페이지의 마지막 행 번호
	 */
	public int getEndRow(int viewPage) {
		return getEndRow( getTotalRow(), viewPage, getRowCount(), isReverse() );
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 마지막 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param reverse 오름차순-false/내림차순-true 구분
	 * @return 전체목록에서 현재 페이지의 마지막 행 번호
	 */
	public int getEndRow(boolean reverse) {
		return getEndRow( getTotalRow(), getViewPage(), getRowCount(), reverse );
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 마지막 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @return 전체목록에서 현재 페이지의 마지막 행 번호
	 */
	public int getEndRow() {
		return getEndRow( getTotalRow(), getViewPage(), getRowCount(), isReverse() );
	}


	//##############################################################################################################################

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 시작 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param totalRow 전체목록 건수
	 * @param viewPage 현재 페이지 번호
	 * @param rowCount 한페이지당 나열할 목록 건수
	 * @param reverse 오름차순-false/내림차순-true 구분
	 * @return 전체목록에서 현재 페이지의 시작 행 번호
	 */
	public static int getStartRow(int totalRow, int viewPage, int rowCount, boolean reverse) {
		if (totalRow <= 0) {
			return 1;
		}

		int intStartRow = 0;
		if (reverse) {
			intStartRow = totalRow - ((viewPage-1) * rowCount) - 1;
			if (intStartRow < 0) {
				intStartRow = 0;
			}
		}

		else {
			intStartRow = ( (viewPage - 1) * rowCount );
		}

		intStartRow = intStartRow + 1;
//		if (intStartRow > totalRow)
//		{
//			intStartRow = totalRow;
//		}

		return intStartRow;
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 시작 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param totalRow 전체목록 건수
	 * @param viewPage 현재 페이지 번호
	 * @param rowCount 한페이지당 나열할 목록 건수
	 * @return 전체목록에서 현재 페이지의 시작 행 번호
	 */
	public static int getStartRow(int totalRow, int viewPage, int rowCount) {
		return getStartRow(totalRow, viewPage, rowCount, false);
	}


	public static int getStartRow(int viewPage, int rowCount) {
		return getStartRow(1, viewPage, rowCount, false);
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 마지막 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param totalRow 전체목록 건수
	 * @param viewPage 현재 페이지 번호
	 * @param rowCount 한페이지당 나열할 목록 건수
	 * @param reverse 오름차순-false/내림차순-true 구분
	 * @return 전체목록에서 현재 페이지의 마지막 행 번호
	 */
	public static int getEndRow(int totalRow, int viewPage, int rowCount, boolean reverse) {
		if (totalRow <= 0) {
			return 1;
		}

		int intStartRow = 0;
		int intEndRow = 0;
		if (reverse) {
			intStartRow = totalRow - ((viewPage-1) * rowCount);
			intEndRow = intStartRow - rowCount + 1;
			if (intEndRow <= 0) {
				intEndRow = 1;
			}
		}

		else {
			intStartRow = ( (viewPage - 1) * rowCount );
			intEndRow = intStartRow + rowCount;
			if (intEndRow > totalRow) {
				intEndRow = totalRow;
			}
		}

		return intEndRow;
	}

	/**
	 * <pre>
	 * 개요 : 전체목록에서 현재 페이지의 마지막 행 번호를 얻는다.
	 *
	 * 로직 :
	 *
	 * 비고 :
	 *
	 * @param totalRow 전체목록 건수
	 * @param viewPage 현재 페이지 번호
	 * @param rowCount 한페이지당 나열할 목록 건수
	 * @return 전체목록에서 현재 페이지의 마지막 행 번호
	 */
	public static int getEndRow(int totalRow, int viewPage, int rowCount) {
		return getEndRow(totalRow, viewPage, rowCount, false);
	}


	public static int getTotalPage(int totalRow, int rowCount) {
		return totalRow > 0
			? ( (int) (Math.ceil(totalRow / rowCount)) ) + ( (totalRow % rowCount) > 0 ? 1 : 0 )
			: 1;
	}




	//public void calculate()
	private void calculate() {
		if (m_intViewPage <= 0) {
			m_intViewPage = 1;
		}
		m_intTotalPage = getTotalPage(m_intTotalRow, m_intRowCount);

		if ( isEmpty() ) {
			m_intViewPage = 1;
			m_intScrollCount = 1;
			m_intScrollStartPage = 1;
			m_intScrollEndPage = 1;
			m_intTotalBlock = 1;
		}
		else {
			if (m_intViewPage > m_intTotalPage) {
				m_intViewPage = m_intTotalPage;
			}
			if (m_intScrollCount <=0 ) {
				m_intScrollCount = m_intTotalPage;
			}

			//scroll start page
			m_intScrollStartPage = (int) Math.ceil((m_intViewPage - 1) / m_intScrollCount);
			m_intScrollStartPage = (m_intScrollStartPage * m_intScrollCount) + 1;
			if (m_intScrollStartPage % m_intScrollCount == 0) {
				m_intScrollStartPage = m_intScrollStartPage - m_intScrollCount + 1 ;
			}

			//scroll end page.
			m_intScrollEndPage = (m_intTotalPage >= (m_intScrollCount + m_intScrollStartPage))
				? m_intScrollStartPage + m_intScrollCount - 1
				: m_intScrollStartPage + (m_intTotalPage - m_intScrollStartPage);

			m_intTotalBlock = (int)Math.ceil( ((double)m_intTotalPage) / ((double)m_intScrollCount) );
		}
	}
}
