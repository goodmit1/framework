package com.fliconz.fm.common.cache;

import com.fliconz.fm.exception.MemoryRefreshException;


public interface ICacheManager {

	public int reload(String kubin) throws MemoryRefreshException;

}
