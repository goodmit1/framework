/*
 * (@)# HttpHelper.java
 */
package com.fliconz.fm.common.util;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;


/**
 * <pre>
 * [개요]
 * HTTP 관련 유틸리티 클래스
 *
 * [상세설명]
 *
 * [변경이력]
 * 2008.06.11::이경구::최초작성
 *
 * @author 이경구
 * </pre>
 */
public class HttpHelper extends TextHelper
{


	/**
	* Html input tag value parsing mode.
	*/
	public final static int PARSING_MODE_INPUT_TEXT = 0;


	/**
	* Html textarea tag value parsing mode.
	*/
	public final static int PARSING_MODE_TEXT_AREA = 1;


	/**
	* Html presentation value parsing mode.
	*/
	public final static int PARSING_MODE_PRE_TEXT = 2;

	/**
	* Html value parsing mode.
	*/
	public final static int PARSING_MODE_HTML_TEXT = 3;

	/**
	* Html script value value parsing mode.
	*/
	public final static int PARSING_MODE_SCRIPT_VALUE = 4;


	//##############################################################################################################################

	/**
	* Construct a <code>HttpHelper</code>
	*/
	protected HttpHelper() {
		super();
	}

	//##############################################################################################################################


//	public static String[] splitRequestURI(HttpServletRequest request)
//	{
//		String[] arrPath = TextHelper.split(request.getRequestURI().substring(request.getContextPath().length()), "/");
//		return arrPath;
//	}
//	public static String[] splitRequestURI(String requestURI)
//	{
//		String[] arrPath = TextHelper.split(requestURI, "/");
//		return arrPath;
//	}

	public static String normalizeScriptValue(char value) {
		switch (value) {
			case '\"':
				return "\\\"";

			case '\'':
				return "\\\'";

			case '\n':
				return "\\n";

			case '\r':
				return "\\r";

			default	:
				return String.valueOf(value);
		}
	}

	/**
	* normalize a html input tag value character.
	* <pre>
	* '&lt;' --&gt; &quot;&amp;lt;&quot;
	* '&gt;' --&gt; &quot;&amp;gt;&quot;
	* '\&quot;' --&gt; &quot;&amp;quot;&quot;
	* '\'' --&gt; &quot;&amp;#x27;&quot;
	* '&amp;' --&gt; &quot;&amp;amp;&quot;
	* '\n' --&gt; &quot;\\n&quot;
	* '\r' --&gt; &quot;\\r&quot;</pre>
	*
	* @param value a html input tag value character.
	* @return parsed string.
	*/
	public static String normalizeInputText(char value) {
		switch (value) {
			case '<':
				return "&lt;";

			case '>':
				return "&gt;";

			case '\"':
				return "&quot;";

			case '\'':
				return "&#x27;";

			case '&':
				return "&amp;";

//			case '\n':
//				return "&#x0D";		//"\\n";
//
//			case '\r':
//				return "&#x0A";		//"\\r";

			default	:
				return String.valueOf(value);
		}
	}

	/**
	* normalize a html text area tag value character.
	* <pre>
	* '&lt;' --&gt; &quot;&amp;lt;&quot;
	* '&gt;' --&gt; &quot;&amp;gt;&quot;
	* '\&quot;' --&gt; &quot;&amp;quot;&quot;
	* '&amp;' --&gt; &quot;&amp;amp;&quot;</pre>
	*
	* @param value a html text area value character.
	* @return normalized string.
	*/
	public static String normalizeTextArea(char value) {
		switch (value) {
			case '<':
				return "&lt;";

			case '>':
				return "&gt;";

//			case '\'':
//				return "&apos;";

			case '\"':
				return "&quot;";

			case '&':
				return "&amp;";

			default	:
				return String.valueOf(value);
		}
	}

	/**
	* normalize a html presentation value character.
	* <pre>
	* '\&quot;' --&gt; &quot;&amp;quot;&quot;
	* ' ' --&gt; &quot;&amp;nbsp;&quot;
	* '&amp;' --&gt; &quot;&amp;amp;&quot;
	* '\t' --&gt; &quot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&quot;
	* '\n' --&gt; &quot;&lt;br&gt;${line.separator}&quot;
	* '\r' --&gt; &quot;&quot;</pre>
	*
	* @param value a html presentation value character.
	* @return normalized string.
	*/
	public static String normalizePreText(char value) {
		switch (value) {
//			case '<':
//				return "&lt;";
//
//			case '>':
//				return "&gt;";

			case '\n':
				return new StringBuilder()
					.append("<br>")
					.append(FileHelper.LINE_SEPARATOR)
					.toString();

			case '\r':
				return "";

			case '\t':
				return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

			case '\"':
				return "&quot;";

			case ' ':
				return "&nbsp;";

			case '&':
				return "&amp;";

			default	:
				return String.valueOf(value);
		}
	}

	/**
	* normalize a html value character.
	* <pre>
	* '&lt;' --&gt; &quot;&amp;lt;&quot;
	* '&gt;' --&gt; &quot;&amp;gt;&quot;
	* '\&quot;' --&gt; &quot;&amp;quot;&quot;
	* ' ' --&gt; &quot;&amp;nbsp;&quot;
	* '&amp;' --&gt; &quot;&amp;amp;&quot;
	* '\t' --&gt; &quot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&quot;
	* '\n' --&gt; &quot;&lt;br&gt;${line.separator}&quot;
	* '\r' --&gt; &quot;&quot;</pre>
	*
	* @param value a html value character.
	* @return normalized string.
	*/
	public static String normalizeHtmlText(char value) {
		switch (value) {
			case '\n':
				return new StringBuilder()
					.append("<br>")
					.append(FileHelper.LINE_SEPARATOR)
					.toString();

			case '\r':
				return "";

			case '\t':
				return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";

			case '\"':
				return "&quot;";

			case '<':
				return "&lt;";

//			case '\'':
//				return "&apos;";

			case '>':
				return "&gt;";

			case ' ':
				return "&nbsp;";

			case '&':
				return "&amp;";

			default	:
				return String.valueOf(value);
		}
	}

	public static String normalizeScriptValue(String value) {
		if (value == null) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		for (int i=0; i<value.length(); i++) {
			sb.append( normalizeScriptValue( value.charAt(i) ) );
		}

		return sb.toString();
	}

	/**
	* normalize a html input tag value string.
	*
	* @param value a html input tag value string.
	* @return normalized string
	* @see #normalizeInputText(char value)
	*/
	public static String normalizeInputText(String value) {
		if (value == null) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		for (int i=0; i<value.length(); i++) {
			sb.append( normalizeInputText( value.charAt(i) ) );
		}

		return sb.toString();
	}

	/**
	* normalize a html text area tag value string.
	*
	* @param value a html text area tag value string.
	* @return normalized string
	* @see #normalizeTextArea(char value)
	*/
	public static String normalizeTextArea(String value) {
		if (value == null) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		for (int i=0; i<value.length(); i++) {
			sb.append( normalizeTextArea( value.charAt(i) ) );
		}

		return sb.toString();
	}

	/**
	* normalize a html presentation value string.
	*
	* @param value a html presentation value string.
	* @return normalized string
	* @see #normalizePreText(char value)
	*/
	public static String normalizePreText(String value) {
		if (value == null) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		for (int i=0; i<value.length(); i++) {
			sb.append( normalizePreText( value.charAt(i) ) );
		}

		return sb.toString();
	}

	/**
	* normalize a html value string.
	*
	* @param value a html value string.
	* @return normalized string
	* @see #normalizeHtmlText(char value)
	*/
	public static String normalizeHtmlText(String value) {
		if (value == null) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		for (int i=0; i<value.length(); i++) {
			sb.append( normalizeHtmlText( value.charAt(i) ) );
		}

		return sb.toString();
	}


	/**
	* normalize a string value.
	*
	* @param value a string value.
	* @param mode parsing mode.
	* @return normalized string.
	*
	* @see #normalizeInputText(String value)
	* @see #normalizeInputText(String value)
	* @see #normalizeInputText(String value)
	* @see HttpResponse#INPUT_TEXT
	* @see HttpResponse#TEXT_AREA
	* @see HttpResponse#PRE_TEXT
	* @see HttpResponse#HTML_TEXT
	*/
	public static String normalizeText(String value, int mode) {
		switch (mode) {
			case PARSING_MODE_INPUT_TEXT:
				return normalizeInputText(value);

			case PARSING_MODE_TEXT_AREA:
				return normalizeTextArea(value);

			case PARSING_MODE_PRE_TEXT:
				return normalizePreText(value);

			case PARSING_MODE_HTML_TEXT:
				return normalizeHtmlText(value);

			default:
				return value;
		}
	}

	/**
	* normalize a character value.
	*
	* @param value a character value.
	* @param mode parsing mode.
	* @return normalized string.
	*
	* @see #normalizeInputText(char value)
	* @see #normalizeInputText(char value)
	* @see #normalizeInputText(char value)
	* @see HttpResponse#INPUT_TEXT
	* @see HttpResponse#TEXT_AREA
	* @see HttpResponse#PRE_TEXT
	* @see HttpResponse#HTML_TEXT
	*/
	public static String normalizeText(char value, int mode) {
		switch (mode) {
			case PARSING_MODE_INPUT_TEXT:
				return normalizeInputText(value);

			case PARSING_MODE_TEXT_AREA:
				return normalizeTextArea(value);

			case PARSING_MODE_PRE_TEXT:
				return normalizePreText(value);

			case PARSING_MODE_HTML_TEXT:
				return normalizeHtmlText(value);

			default:
				return String.valueOf(value);
		}
	}


	public static boolean isMobileDevice(HttpServletRequest request)
	{
		String userAgent = request.getHeader("User-Agent");
		if(userAgent == null) return false;
		if (userAgent.indexOf("iPhone") >= 0 || userAgent.indexOf("iPad") >= 0)
		{
			return true;
		}
		else if (userAgent.indexOf("Android") >= 0)
		{
			return true;
		}

		return false;
	}

	public static boolean isTabletDevice(HttpServletRequest request)
	{
		String userAgent = request.getHeader("User-Agent");
		if (userAgent.indexOf("iPad") >= 0)
		{
			return true;
		}/*
		else if (userAgent.indexOf("Android") >= 0)
		{
			return true;
		}
 */
		return false;
	}

	// sitemesh 에서 제공하는 com.opensymphony.module.sitemesh.mapper.PathMapper 클래스를 디컴파일했다..
	public static boolean matchURL(String pattern, String str, boolean isCaseSensitive)
	{
		char patArr[] = pattern.toCharArray();
		char strArr[] = str.toCharArray();
		int patIdxStart = 0;
		int patIdxEnd = patArr.length - 1;
		int strIdxStart = 0;
		int strIdxEnd = strArr.length - 1;
		boolean containsStar = false;
		int ix = 0;
		do
		{
			if(ix >= patArr.length)
				break;
			if(patArr[ix] == '*')
			{
				containsStar = true;
				break;
			}
			ix++;
		} while(true);
		if(!containsStar)
		{
			if(patIdxEnd != strIdxEnd)
				return false;
			for(int i = 0; i <= patIdxEnd; i++)
			{
				char ch = patArr[i];
				if(ch == '?')
					continue;
				if(isCaseSensitive && ch != strArr[i])
					return false;
				if(!isCaseSensitive && Character.toUpperCase(ch) != Character.toUpperCase(strArr[i]))
					return false;
			}

			return true;
		}
		if(patIdxEnd == 0)
			return true;
		char ch1;
		for(; (ch1 = patArr[patIdxStart]) != '*' && strIdxStart <= strIdxEnd; strIdxStart++)
		{
			if(ch1 != '?')
			{
				if(isCaseSensitive && ch1 != strArr[strIdxStart])
					return false;
				if(!isCaseSensitive && Character.toUpperCase(ch1) != Character.toUpperCase(strArr[strIdxStart]))
					return false;
			}
			patIdxStart++;
		}

		if(strIdxStart > strIdxEnd)
		{
			for(int i = patIdxStart; i <= patIdxEnd; i++)
				if(patArr[i] != '*')
					return false;

			return true;
		}


		char ch2;
		for(; (ch2 = patArr[patIdxEnd]) != '*' && strIdxStart <= strIdxEnd; strIdxEnd--)
		{
			if(ch2 != '?')
			{
				if(isCaseSensitive && ch2 != strArr[strIdxEnd])
					return false;
				if(!isCaseSensitive && Character.toUpperCase(ch2) != Character.toUpperCase(strArr[strIdxEnd]))
					return false;
			}
			patIdxEnd--;
		}

		if(strIdxStart > strIdxEnd)
		{
			for(int i = patIdxStart; i <= patIdxEnd; i++)
				if(patArr[i] != '*')
					return false;

			return true;
		}
		do
		{
			if(patIdxStart == patIdxEnd || strIdxStart > strIdxEnd)
				break;
			int patIdxTmp = -1;
			int ix1 = patIdxStart + 1;
			do
			{
				if(ix1 > patIdxEnd)
					break;
				if(patArr[ix1] == '*')
				{
					patIdxTmp = ix1;
					break;
				}
				ix1++;
			} while(true);
			if(patIdxTmp == patIdxStart + 1)
			{
				patIdxStart++;
				continue;
			}
			int patLength = patIdxTmp - patIdxStart - 1;
			int strLength = (strIdxEnd - strIdxStart) + 1;
			int foundIdx = -1;
			int ix9 = 0;
label0:
			do
			{
label1:
				{
					if(ix9 > strLength - patLength)
						break label0;
					for(int j = 0; j < patLength; j++)
					{
						char ch3 = patArr[patIdxStart + j + 1];
						if(ch3 != '?' && (isCaseSensitive && ch3 != strArr[strIdxStart + ix9 + j] || !isCaseSensitive && Character.toUpperCase(ch3) != Character.toUpperCase(strArr[strIdxStart + ix9 + j])))
							break label1;
					}

					foundIdx = strIdxStart + ix9;
					break label0;
				}
				ix9++;
			} while(true);
			if(foundIdx == -1)
				return false;
			patIdxStart = patIdxTmp;
			strIdxStart = foundIdx + patLength;
		} while(true);
		for(int patIdxTmp = patIdxStart; patIdxTmp <= patIdxEnd; patIdxTmp++)
			if(patArr[patIdxTmp] != '*')
				return false;

		return true;
	}

	public static String getRealPath(ServletContext ctx, String subPath)
	{
		try
		{
			java.io.File aFile = new java.io.File(ctx.getRealPath("/"));
			java.net.URL aURL = aFile.toURI().toURL();
			aURL = new java.net.URL(aURL, subPath);

			aFile = new java.io.File(aURL.toURI());
			return aFile.getAbsolutePath();
		}
		catch (java.net.MalformedURLException | java.net.URISyntaxException e)
		{
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	public static boolean isAjax(HttpServletRequest request){
		return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
	}

//	/**
//	* Returns a number of the page.
//	*
//	* @param totalRows total row count.
//	* @param row count per page
//	* @return calculated total page count.
//	*/
//	public static int getTotalPage(int totalRows, int rows)
//	{
//		return totalRows > 0
//			? ( (int) (Math.ceil(totalRows / rows)) ) + ( (totalRows % rows) > 0 ? 1 : 0 )
//			: 0;
//	}

//	public static int getRowNo(int rows, int currentPage, int lineNo)
//	{
//		return rows * (currentPage - 1) + lineNo;
//	}


//	/**
//	* Returns the query string that is contained in the request URL after the path.
//	*
//	* <p>This method returns <code>null</code> if the URL does not have a query string.
//	*
//	* <p>Same as the value of the CGI variable QUERY_STRING.
//	*
//	* If the isEncoding argument value is <code>true</code>, encode a parameter value; <code>java.net.URLEncoder.encode(String s)</code>.
//	*
//	* @param req javax.servlet.HttpServletRequest
//	* @param isEncoding if the <code>true</code>, encode a parameter value.
//	* @return a <code>String</code> containing the query string or <code>null</code> if the URL contains no query string.
//	* The value is not decoded by the container.
//	*/
//	public static String getQueryString(HttpServletRequest req, boolean isEncoding)
//	{
//		StringBuilder sb = new StringBuilder();
//		for (Enumeration en = req.getParameterNames(); en.hasMoreElements(); )
//		{
//			String name = (String) en.nextElement();
//			String[] arr = req.getParameterValues(name);
//			for (int i=0; i<arr.length;i++)
//			{
//				sb.append( name+"="+(isEncoding ? URLEncoder.encode(arr[i]) : arr[i]) );
//				sb.append("&");
//			}
//		}
//
//		int intLen = sb.length();
//		return (intLen==0) ? null : sb.substring(0, intLen-1);
//	}
//
//	/**
//	* Returns the encoded query string that is contained in the request URL after the path.
//	*
//	* <p>This method returns <code>null</code> if the URL does not have a query string.
//	*
//	* <p>Same as the value of the CGI variable QUERY_STRING.
//	*
//	* @param req javax.servlet.HttpServletRequest
//	* @return a <code>String</code> containing the query string or <code>null</code> if the URL contains no query string.
//	* The value is not decoded by the container.
//	*
//	* @see #getQueryString(HttpServletRequest req, boolean isEncoding)
//	*/
//	public static String getQueryString(HttpServletRequest req)
//	{
//		return getQueryString(req, true);
//	}


	/**
	* normalizes a query string passed from the client to the server and builds a <code>java.util.Hashtable</code> object with key-value pairs.
	*
	* <p>The query string should be in the form of a string packaged by the GET or POST method, that is,
	* it should have key-value pairs in the form <i>key=value</i>, with each pair separated from the next by a & character.
	*
	* <p>A key can appear more than once in the query string with different values.
	* However, the key appears only once in the hashtable,
	* with its value being an array of strings containing the multiple values sent by the query string.
	*
	* <p>The keys and values in the hashtable are stored in their decoded form,
	* so any + characters are converted to spaces,
	* and characters sent in hexadecimal notation (like <i>%xx</i>) are converted to ASCII characters.
	*
	* @param queryString a string containing the query to be normalized
	* @return a <code>java.util.Hashtable</code> object built from the normalized key-value pairs
	* @exception IllegalArgumentException if the query string is invalid.
	*/
	public static Map<String,Object> normalizeQueryString(String queryString)
	{
		HashMap<String,Object> htParam = new HashMap<String,Object>();
		if (queryString == null) return htParam;

		for ( StringTokenizer st = new StringTokenizer(queryString, "&"); st.hasMoreTokens(); )
		{
			String strPair = st.nextToken();
			int intIndex = strPair.indexOf("=");
			if (intIndex == -1) throw new IllegalArgumentException("incorrect query string.");

			String strName = strPair.substring(0, intIndex);
			String strValue = strPair.substring(intIndex+1);
			//String strValue = URLDecoder.decode( strPair.substring(intIndex+1) );

			if ( htParam.containsKey(strName) )
			{
				String[] arrOld = (String[]) htParam.get(strName);
				String[] arrNew = new String[ arrOld.length+1 ];
				System.arraycopy( arrOld, 0, arrNew, 0, arrOld.length );

				arrNew[arrOld.length] = strValue;
				htParam.put(strName, arrNew);
			}
			else
			{
				htParam.put(strName, new String[]{strValue} );
			}
		}

		return htParam;
	}

//	private static int getScope(String scopeName)
//	{
//		String strScopeName = scopeName.toLowerCase();
//		if ( strScopeName.equals("page") )
//			return PageContext.PAGE_SCOPE;
//
//		else if ( strScopeName.equals("request") )
//			return PageContext.REQUEST_SCOPE;
//
//		else if ( strScopeName.equals("session") )
//			return PageContext.SESSION_SCOPE;
//
//		else if ( strScopeName.equals("application") )
//			return PageContext.APPLICATION_SCOPE;
//
//		else
//			throw new IllegalArgumentException(
//				new StringBuilder()
//					.append(scopeName)
//					.append(" scope is illeagl argument.")
//					.toString()
//			);
//	}
//
//	public static Object getAttribute(PageContext ctx, String scopeName, String name)
//	{
//		return ctx.getAttribute( name, getScope(scopeName) );
//	}
//
//	public static Object setAttribute(PageContext ctx, String scopeName, String name, Object value)
//	{
//		int intScope = getScope(scopeName);
//		Object oldValue = ctx.getAttribute(name, intScope);
//
//		ctx.setAttribute(name, value, intScope);
//		return oldValue;
//	}
//
//	public static Object removeAttribute(PageContext ctx, String scopeName, String name)
//	{
//		int intScope = getScope(scopeName);
//		Object oldValue = ctx.getAttribute(name, intScope);
//
//		ctx.removeAttribute(name, intScope);
//		return oldValue;
//	}


//	public static Object findAttribute(PageContext ctx, String path) throws ServletException
//	{
//		Enumeration en = new StringTokenizer(path, ".");
//		if ( !en.hasMoreElements() ) throw new ServletException("path is empty.");
//
//		Object returnValue = null;
//		String strScope = (String)en.nextElement();
//
//		if ( strScope.equals("request") )
//		{
//			returnValue = ctx.getAttribute(strScope, PageContext.REQUEST_SCOPE);
//		}
//
//		else if ( strScope.equals("session") )
//		{
//			returnValue = ctx.getAttribute(strScope, PageContext.SESSION_SCOPE);
//		}
//
//		else if ( strScope.equals("pageContext") )
//		{
//			returnValue = ctx.getAttribute(strScope);
//		}
//
//		else if ( strScope.equals("application") )
//		{
//			returnValue = ctx.getAttribute(strScope, PageContext.APPLICATION_SCOPE);
//		}
//
//		else if ( strScope.equals("page") )
//		{
//			returnValue = ctx.getAttribute(strScope, PageContext.PAGE_SCOPE);
//		}
//
//		else
//		{
//			throw new ServletException(
//				new StringBuilder()
//					.append("unsupported scope [")
//					.append(strScope)
//					.append("].")
//					.toString()
//			);
//		}
//
//
//		while ( en.hasMoreElements() )
//		{
//			strScope = (String)en.nextElement();
//			String strMethod = new StringBuilder()
//				.append("get")
//				.append( strScope.substring(0,1).toUpperCase() )
//				.append( strScope.substring(1) )
//				.toString();
//
//			//method
//			try
//			{
//				Method aMethod = returnValue.getClass().getMethod(strMethod, null);
//				returnValue = aMethod.invoke(returnValue, null);
//				continue;
//			}
//			catch (NoSuchMethodException e)
//			{
//				throw new ServletException(e.getMessage(), e);
//			}
//			catch (SecurityException e)
//			{
//				throw new ServletException(e.getMessage(), e);
//			}
//			catch (IllegalAccessException e)
//			{
//				throw new ServletException(e.getMessage(), e);
//			}
//			catch (IllegalArgumentException e)
//			{
//				throw new ServletException(e.getMessage(), e);
//			}
//			catch (InvocationTargetException e)
//			{
//				throw new ServletException(e.getMessage(), e);
//			}
///*
//			//field
//			try
//			{
//				Field aField = returnValue.getClass().getField(strScope);
//				returnValue = aField.get(returnValue);
//				continue;
//			}
//			catch (NoSuchFieldException e)
//			{
//				throw new ServletException(
//					new StringBuilder()
//						.append("Could not find a method [")
//						.append(strMethod)
//						.append("] or ")
//						.append("field [")
//						.append(strScope)
//						.append("] from ")
//						.append(returnValue.getClass().getName())
//						.toString()
//				);
//			}
//*/
//		}
//
//		return returnValue;
//	}
}


































//	//##############################################################################################################################
//
//	/**
//	* Write a converted HTML input tag value string to the special charactor.
//	*
//	* <pre><code>
//	* &lt; --&gt; &amp;lt;
//	* &gt; --&gt; &amp;gt;
//	* &#x27; --&gt; &amp;#x27;
//	* &quot; --&gt; &amp;quot;
//	* &amp; --&gt; &amp;amp;
//	* \n (LF, ASCII code=10) --&gt; \\n
//	* \r (CR, ASCII code=13) --&gt; \\r</code></pre>
//	*
//	* @param value input text value string.
//	* @param out a writer
//	*
//	* @exception java.io.IOException writting failed.
//	*/
//	public static void printInputValue(Writer out, String value) throws IOException
//	{
//		if (value == null) return;
//
//		PrintWriter aWriter = (out instanceof PrintWriter) ? (PrintWriter)out : new PrintWriter(out, true);
//
//		int intLen = value.length();
//		synchronized(aWriter)
//		{
//			for (int i=0; i<intLen; i++)
//			{
//				char aChar = value.charAt(i);
//				switch (aChar)
//				{
//					case '<':
//						aWriter.print("&lt;");
//						break;
//
//					case '>':
//						aWriter.print("&gt;");
//						break;
//
//					case '\"':
//						aWriter.print("&quot;");
//						break;
//
//					case '\'':
//						aWriter.print("&#x27;");
//						break;
//
//					case '&':
//						aWriter.print("&amp;");
//						break;
//
//					case '\n':
//						aWriter.print("\\n");
//						break;
//
//					case '\r':
//						aWriter.print("\\r");
//						break;
//
//					default	:
//						aWriter.print(aChar);
//				}
//			}
//		}
//	}
//
//	/**
//	* @see #printInputValue(Writer out, String value)
//	*/
//	public static void printInputValue(ServletResponse res, String value) throws IOException
//	{
//		printInputValue(res.getWriter(), value);
//	}
//
//	/**
//	* @see #printInputValue(Writer out, String value)
//	*/
//	public static String normalizeInputValue(String value)
//	{
//		StringWriter sw = null;
//		try
//		{
//			sw = new StringWriter();
//			printInputValue( sw, value );
//
//			return sw.getBuffer().toString();
//		}
//		catch (IOException e)
//		{
//			return "";
//		}
//		finally
//		{
//			try
//			{
//				if (sw != null) sw.close();
//			}
//			catch (Exception e) {}
//		}
//	}
//
//	//##############################################################################################################################
//
//	/**
//	* Write a converted HTML textarea tab value string to the special charactor.
//	*
//	* <pre><code>
//	* &lt; --&gt; &amp;lt;
//	* &gt; --&gt; &amp;gt;
//	* &quot; --&gt; &amp;quot;
//	* &amp; --&gt; &amp;amp;</code></pre>
//	*
//	* @param value text area value string.
//	* @param out a writer
//	*
//	* @exception java.io.IOException writting failed.
//	*/
//	public static void printTextAreaValue(Writer out, String value) throws IOException
//	{
//		if (value == null) return;
//
//		PrintWriter aWriter = (out instanceof PrintWriter) ? (PrintWriter)out : new PrintWriter(out, true);
//
//		int intLen = value.length();
//		synchronized(aWriter)
//		{
//			for (int i=0; i<intLen; i++)
//			{
//				char aChar = value.charAt(i);
//				switch (aChar)
//				{
//					case '<':
//						aWriter.print("&lt;");
//						break;
//
//					case '>':
//						aWriter.print("&gt;");
//						break;
//
//					case '\"':
//						aWriter.print("&quot;");
//						break;
//
//					case '&':
//						aWriter.print("&amp;");
//						break;
//
//					default	:
//						aWriter.print(aChar);
//				}
//			}
//		}
//	}
//
//	/**
//	* @see #printTextAreaValue(Writer out, String value)
//	*/
//	public static void printTextAreaValue(ServletResponse res, String value) throws IOException
//	{
//		printTextAreaValue(res.getWriter(), value);
//	}
//
//	/**
//	* @see #printTextAreaValue(Writer out, String value)
//	*/
//	public static String normalizeTextAreaValue(String value)
//	{
//		StringWriter sw = null;
//		try
//		{
//			sw = new StringWriter();
//			printTextAreaValue( sw, value );
//
//			return sw.getBuffer().toString();
//		}
//		catch (IOException e)
//		{
//			return "";
//		}
//		finally
//		{
//			try
//			{
//				if (sw != null) sw.close();
//			}
//			catch (Exception e) {}
//		}
//	}
//
//	//##############################################################################################################################
//
//	/**
//	* Write a converted HTML string to the special charactor.
//	*
//	* <pre><code>
//	* &lt; --&gt; &amp;lt;
//	* &gt; --&gt; &amp;gt;
//	* &quot; --&gt; &amp;quot;
//	* &amp; --&gt; &amp;amp;
//	* \n --&gt; &gt;br&lt\n
//	* \t --&gt; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
//	* ' ' --&gt; &nbsp;</code></pre>
//	*
//	* @param out a writer
//	* @param value HTML string.
//	*
//	* @exception java.io.IOException writting failed.
//	*/
//	public static void printHtml(Writer out, String value) throws IOException
//	{
//		if (value == null) return;
//
//		PrintWriter aWriter = (out instanceof PrintWriter) ? (PrintWriter)out : new PrintWriter(out, true);
//
//		int intLen = value.length();
//		synchronized(aWriter)
//		{
//			for (int i=0; i<intLen; i++)
//			{
//				char aChar = value.charAt(i);
//				switch (aChar)
//				{
//					case '\n':
//						aWriter.println("<br>");
//						break;
//
//					case '\r':
//						break;
//
//					case '\t':
//						aWriter.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//						break;
//
//					case '\"':
//						aWriter.print("&quot;");
//						break;
//
//					case '<':
//						aWriter.print("&lt;");
//						break;
//
//					case '>':
//						aWriter.print("&gt;");
//						break;
//
//					case ' ':
//						aWriter.print("&nbsp;");
//						break;
//
//					case '&':
//						aWriter.print("&amp;");
//						break;
//
//					default	:
//						aWriter.print(aChar);
//				}
//			}
//		}
//	}
//
//	/**
//	* @see #printHtml(Writer out, String value)
//	*/
//	public static void printHtml(ServletResponse res, String value) throws IOException
//	{
//		printHtml(res.getWriter(), value);
//	}
//
//	/**
//	* @see #printHtml(Writer out, String value)
//	*/
//	public static String normalizeHtml(String value)
//	{
//		if (value == null) return "";
//
//		StringWriter sw = null;
//		try
//		{
//			sw = new StringWriter();
//			printHtml( sw, value );
//
//			return sw.getBuffer().toString();
//		}
//		catch (IOException e)
//		{
//			return "";
//		}
//		finally
//		{
//			try
//			{
//				if (sw != null) sw.close();
//			}
//			catch (Exception e) {}
//		}
//	}
//
//	//##############################################################################################################################
//
//	/**
//	* Write a converted HTML string to the special character string.
//	*
//	* <pre><code>
//	* &quot; --&gt; &amp;quot;
//	* &amp; --&gt; &amp;amp;
//	* \n --&gt; &gt;br&lt\n
//	* \t --&gt; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
//	* ' ' --&gt; &nbsp;</code></pre>
//	*
//	* @param out a writer
//	* @param value HTML string.
//	* @exception java.io.IOException writting failed.
//	*/
//	public static void writePre(Writer out, String value) throws IOException
//	{
//		if (value == null) return;
//
//		PrintWriter aWriter = (out instanceof PrintWriter) ? (PrintWriter)out : new PrintWriter(out, true);
//
//		int intLen = value.length();
//		synchronized(aWriter)
//		{
//			for (int i=0; i<intLen; i++)
//			{
//				char aChar = value.charAt(i);
//				switch (aChar)
//				{
//					case '\n':
//						aWriter.println("<br>");
//						break;
//
//					case '\r':
//						break;
//
//					case '\t':
//						aWriter.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//						break;
//
//					case '\"':
//						aWriter.print("&quot;");
//						break;
//
//					case ' ':
//						aWriter.print("&nbsp;");
//						break;
//
//					case '&':
//						aWriter.print("&amp;");
//						break;
//
//					default	:
//						aWriter.print(aChar);
//				}
//			}
//		}
//	}
//
//	/**
//	* @see #writePre(Writer out, String value)
//	*/
//	public static void writePre(ServletResponse res, String value) throws IOException
//	{
//		writePre(res.getWriter(), value);
//	}
//
//	/**
//	* @see #writePre(Writer out, String value)
//	*/
//	public static String normalizePre(String value)
//	{
//		if (value == null) return "";
//
//		StringWriter sw = null;
//		try
//		{
//			sw = new StringWriter();
//			writePre( sw, value );
//
//			return sw.getBuffer().toString();
//		}
//		catch (IOException e)
//		{
//			return "";
//		}
//		finally
//		{
//			try
//			{
//				if (sw != null) sw.close();
//			}
//			catch (Exception e) {}
//		}
//	}
