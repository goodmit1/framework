package com.fliconz.fm.common.core;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;


public class FileDownloadView extends AbstractView {


//	private final Log logger = LogFactory.getLog(this.getClass());
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@Override
	protected void renderMergedOutputModel(Map model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		File file = (File)model.get("file");

		String realName	= (String)model.get("realFileName");

		logger.debug("file Path : {}", file.getPath());
		logger.debug("file Name : {}", file.getName());

		response.setContentType("application/octet-stream");
		response.setContentLength((int)file.length());

		String userAgent = request.getHeader("User-Agent");

		boolean ie = userAgent.indexOf("MSIE") > -1;
		String fileName = null;

		// 실제 파일이름
		if(realName!=null && !realName.equals(""))
		{
			if(ie)
			{
				fileName = URLEncoder.encode(realName, "utf-8");

			} else
			{
				fileName = new String(realName.getBytes("utf-8"),"iso-8859-1");
			}

		} else
		{
			if(ie)
			{
				fileName = URLEncoder.encode(file.getName(), "utf-8");

			} else
			{
				fileName = new String(file.getName().getBytes("utf-8"),"iso-8859-1");
			}
		}

		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Disposition", "attachment;filename=\""+fileName+"\";");

		OutputStream out	= response.getOutputStream();
		FileInputStream fis	= null;

		try {

			fis	= new FileInputStream(file);
			FileCopyUtils.copy(fis, out);

		} catch(IOException e)
		{
			//e.printStackTrace();
			throw e;
		}
		finally
		{
			if(fis != null)
				fis.close();
		}
		out.flush();
	}

}
