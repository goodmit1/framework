package com.fliconz.fm.common.geocode;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;
//import net.sf.json.JSONObject;
//import net.sf.json.JSONSerializer;
import java.util.LinkedList;
import java.util.List;

// http://developer.yahoo.com/geo/placefinder/guide/index.html
/**
 * yahoo place finder
 */
public class YahooGeocoder extends Geocoder
{
	private String m_consumerKey = null;
	private boolean m_isReverseAddress = false;

	public YahooGeocoder(String apiKey, boolean isReverseAddress)
	{
		super();
		m_consumerKey = apiKey;
		m_isReverseAddress = isReverseAddress;
	}

	public YahooGeocoder(String apiKey)
	{
		this(apiKey, true);
	}

	public String getAPIKey()
	{
		return m_consumerKey;
	}
	public boolean isReverseAddress()
	{
		return m_isReverseAddress;
	}

//	http://where.yahooapis.com/geocode?appid=dj0yJmk9dnl5MFozQWdZdmEwJmQ9WVdrOVUwNU5PVWRTTkhFbWNHbzlPVGsxT0Rrd01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTkw&q=복정동&locale=ko_KR&start=0&count=100&flags=EJ
//	http://where.yahooapis.com/geocode?appid=dj0yJmk9dnl5MFozQWdZdmEwJmQ9WVdrOVUwNU5PVWRTTkhFbWNHbzlPVGsxT0Rrd01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTkw&location=37.66195262951018,126.94693092755365&locale=ko_KR&start=0&count=100&flags=EJ&gflags=R


	private String makeAddress(org.json.JSONObject item) throws JSONException
	{
		return isReverseAddress()
			? new StringBuilder()
				.append(item.getString("country")).append(" ")
				.append(item.getString("state")).append(" ")
				.append(item.getString("county")).append(" ")
				.append(item.getString("city")).append(" ")
				.append(item.getString("neighborhood"))
				.toString().trim()
			: new StringBuilder()
				.append(item.getString("neighborhood")).append(" ")
				.append(item.getString("city")).append(" ")
				.append(item.getString("county")).append(" ")
				.append(item.getString("state")).append(" ")
				.append(item.getString("country"))
				.toString().trim();
	}

	@Override
	public boolean canPaging(boolean isGeocode)
	{
		return true;
	}


	@Override
	public DataMap<String, Object> geocode(GeoLocation addr) throws GeocodeException
	{
		// 주소 --> 좌표
		HttpClient aHttpClient = null;

		try
		{
			DataMap<String,Object> locAttr = addr.getAttributes();
			int pageNo = locAttr.getInt(KEY_REQUEST_PAGE, 1);
			int rowCount = locAttr.getInt(KEY_REQUEST_COUNT, 10);
			String encoding = locAttr.getString(KEY_ENCODING,"UTF-8");

			StringBuilder sb = new StringBuilder()
				.append("http://where.yahooapis.com/geocode")
				.append("?appid=").append(TextHelper.nvl(getAPIKey(), ""))
				.append("&q=").append(URLEncoder.encode(TextHelper.nvl(addr.getAddress(), ""), encoding))
				.append("&locale=").append( locAttr.getString("locale", "ko_KR"))
				.append("&start=").append((pageNo-1)*rowCount)
				.append("&count=").append(pageNo*rowCount)
				.append("&flags=").append(locAttr.getString("flags", "EJ"));

			aHttpClient = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(sb.toString());

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);

			org.json.JSONObject response = new org.json.JSONObject( responseString );
			org.json.JSONObject resultSet = response.getJSONObject("ResultSet");
			String responseCode = TextHelper.nvl(resultSet.getString("Error"), "");
			String responseMessage = TextHelper.nvl(resultSet.getString("ErrorMessage"), "");

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (responseCode.equals("0") == false)
			{
				resultData.setString(KEY_RESPONSE_CODE, responseCode);
				resultData.setString(KEY_RESPONSE_MESSAGE, responseMessage);
				resultData.setInt(KEY_REQUEST_PAGE, pageNo);
				resultData.setInt(KEY_RESPONSE_COUNT, 0);
				resultData.setInt(KEY_TOTAL_COUNT, 0);
				return resultData;
			}


			List<GeoLocation> aList = new LinkedList<GeoLocation>();
			org.json.JSONArray arr = resultSet.getJSONArray("Results");
			for (int i=0; i<arr.length(); i++)
			{
				org.json.JSONObject item = arr.getJSONObject(i);
				GeoLocation aLocation = new GeoLocation();
				String address = makeAddress(item);

				aLocation.setAddress(address);
				aLocation.setLatitude((float)item.getDouble("latitude"));
				aLocation.setLongitude((float)item.getDouble("longitude"));
				aList.add(aLocation);
			}
//            for (Iterator<JSONObject> it = resultSet.getJSONArray("Results").iterator(); it.hasNext();)
//            {
//            	JSONObject item = it.next();
//            	GeoLocation aLocation = new GeoLocation();
//            	String address = makeAddress(item);
//
//            	aLocation.setAddress(address);
//            	aLocation.setLatitude((float)item.getDouble("latitude"));
//            	aLocation.setLongitude((float)item.getDouble("longitude"));
//            	aList.add(aLocation);
//            }

			resultData.setString(KEY_RESPONSE_CODE, "ok");
			resultData.setString(KEY_RESPONSE_MESSAGE, responseMessage);
			resultData.setObject(KEY_LOCATION_LIST, aList);

			int count = aList.size();
			resultData.setInt(KEY_REQUEST_PAGE, pageNo);
			resultData.setInt(KEY_RESPONSE_COUNT, count);
			resultData.setInt(KEY_TOTAL_COUNT, count);

			return resultData;
		}
		catch (UnsupportedEncodingException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}

	@Override
	public DataMap<String, Object> reverseGeocode(GeoLocation coord) throws GeocodeException
	{
		// 좌표 --> 주소
		HttpClient aHttpClient = null;

		try
		{
			DataMap<String,Object> locAttr = coord.getAttributes();
			int pageNo = locAttr.getInt(KEY_REQUEST_PAGE, 1);
			int rowCount = locAttr.getInt(KEY_REQUEST_COUNT, 10);
			String encoding = locAttr.getString(KEY_ENCODING,"UTF-8");

			StringBuilder sb = new StringBuilder()
				.append("http://where.yahooapis.com/geocode")
				.append("?appid=").append(TextHelper.nvl(getAPIKey(), ""))
				.append("&location=").append(coord.getLatitude()).append(",").append(coord.getLongitude())
				.append("&locale=").append( locAttr.getString("locale", "ko_KR"))
				.append("&start=").append((pageNo-1)*rowCount)
				.append("&count=").append(pageNo*rowCount)
				.append("&flags=").append(locAttr.getString("flags", "EJ"))
				.append("&gflags=").append(locAttr.getString("gflags", "R"));

			aHttpClient = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(sb.toString());

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);

			org.json.JSONObject response = new org.json.JSONObject( responseString );
			org.json.JSONObject resultSet = response.getJSONObject("ResultSet");
			String responseCode = TextHelper.nvl(resultSet.getString("Error"), "");
			String responseMessage = TextHelper.nvl(resultSet.getString("ErrorMessage"), "");

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (responseCode.equals("0") == false)
			{
				resultData.setString(KEY_RESPONSE_CODE, responseCode);
				resultData.setString(KEY_RESPONSE_MESSAGE, responseMessage);
				resultData.setInt(KEY_REQUEST_PAGE, pageNo);
				resultData.setInt(KEY_RESPONSE_COUNT, 0);
				resultData.setInt(KEY_TOTAL_COUNT, 0);
				return resultData;
			}


			List<GeoLocation> aList = new LinkedList<GeoLocation>();
			org.json.JSONArray arr = resultSet.getJSONArray("Results");
			for (int i=0; i<arr.length(); i++)
			{
				org.json.JSONObject item = arr.getJSONObject(i);
				GeoLocation aLocation = new GeoLocation();
				String address = makeAddress(item);

				aLocation.setAddress(address);
				aLocation.setLatitude((float)item.getDouble("latitude"));
				aLocation.setLongitude((float)item.getDouble("longitude"));
				aList.add(aLocation);
			}
//            for (Iterator<JSONObject> it = resultSet.getJSONArray("Results").iterator(); it.hasNext();)
//            {
//            	JSONObject item = it.next();
//            	GeoLocation aLocation = new GeoLocation();
//            	String address = makeAddress(item);
//
//            	aLocation.setAddress(address);
//            	aLocation.setLatitude((float)item.getDouble("latitude"));
//            	aLocation.setLongitude((float)item.getDouble("longitude"));
//            	aList.add(aLocation);
//            }

			resultData.setString(KEY_RESPONSE_CODE, "ok");
			resultData.setString(KEY_RESPONSE_MESSAGE, responseMessage);
			resultData.setObject(KEY_LOCATION_LIST, aList);

			int count = aList.size();
			resultData.setInt(KEY_REQUEST_PAGE, pageNo);
			resultData.setInt(KEY_RESPONSE_COUNT, count);
			resultData.setInt(KEY_TOTAL_COUNT, count);

			return resultData;
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}



/*
WEB_INF_HOME=/Volumes/data/projects/SmartJM/development/web/smartjm/WebContent/WEB-INF
THIS_CLASSPATH=$WEB_INF_HOME/classes:$WEB_INF_HOME/lib/httpclient-4.0.3.jar:$WEB_INF_HOME/lib/httpcore-4.0.1.jar:$WEB_INF_HOME/lib/commons-logging-1.1.1.jar:$WEB_INF_HOME/lib/json-lib-2.3-jdk15.jar:$WEB_INF_HOME/lib/commons-lang-2.4.jar:$WEB_INF_HOME/lib/ezmorph-1.0.6.jar:$WEB_INF_HOME/lib/commons-collections-3.2.1.jar:$WEB_INF_HOME/lib/commons-beanutils-1.8.0.jar

java -cp $THIS_CLASSPATH com.fliconz.fm.common.geocode.YahooGeocoder
*/
	public static void main(String[] args) throws Exception
	{
		YahooGeocoder geocoder = new YahooGeocoder("dj0yJmk9dnl5MFozQWdZdmEwJmQ9WVdrOVUwNU5PVWRTTkhFbWNHbzlPVGsxT0Rrd01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTkw");
		GeoLocation aLocation = new GeoLocation();
		DataMap<String,Object> resultData = null;



		System.out.println("=========================");
		System.out.println(geocoder.getClass().getName());

		// http://where.yahooapis.com/geocode?appid=dj0yJmk9dnl5MFozQWdZdmEwJmQ9WVdrOVUwNU5PVWRTTkhFbWNHbzlPVGsxT0Rrd01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTkw&q=효자동&locale=ko_KR&start=0&count=100&flags=EJ
		aLocation.setAddress("효자동");
		resultData = geocoder.geocode(aLocation);
		System.out.println("=========================");
		System.out.println("geocode : "+ resultData.toString());


		// http://where.yahooapis.com/geocode?appid=dj0yJmk9dnl5MFozQWdZdmEwJmQ9WVdrOVUwNU5PVWRTTkhFbWNHbzlPVGsxT0Rrd01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTkw&location=37.87015110,127.73687180&locale=ko_KR&start=0&count=100&flags=EJ&gflags=R
		aLocation.setLatitude(37.87015110F);
		aLocation.setLongitude(127.73687180F);
		resultData = geocoder.reverseGeocode(aLocation);

		System.out.println();
		System.out.println("=========================");
		System.out.println("reverse geocode : "+ resultData.toString());
	}
}
