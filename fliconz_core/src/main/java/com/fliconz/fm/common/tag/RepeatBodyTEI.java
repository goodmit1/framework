/*
 * @(#)RepeatBodyTEI.java
 */
package com.fliconz.fm.common.tag;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;


public class RepeatBodyTEI extends TagExtraInfo
{
	public RepeatBodyTEI() {
		super();
	}

	//##############################################################################################################################

	public VariableInfo[] getVariableInfo(TagData data) {
		boolean isDeclare = TextHelper.parseBoolean( data.getAttributeString("declare"), true );
		String strClassName = (String)data.getAttribute("className");
		strClassName = TextHelper.evl( TextHelper.trim(strClassName), null ) == null ? "java.lang.Object" : strClassName;
		if (strClassName.equals("dataMap"))
		{
			strClassName = DataMap.class.getName();
		}

		VariableInfo[] result = { new VariableInfo(data.getId(), strClassName, isDeclare, VariableInfo.NESTED) };
		return result;
	}

	public boolean isValid(TagData data) {
		return true;
	}
}
