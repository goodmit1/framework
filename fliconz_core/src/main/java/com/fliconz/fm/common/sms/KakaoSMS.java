package com.fliconz.fm.common.sms;

import java.util.HashMap;
import java.util.Map;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fliconz.fm.util.RequestHelper;

public class KakaoSMS implements ISMS {
//	Log log = LogFactory.getLog(this.getClass());
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/*
	 * HttpResponse response = Unirest.POST ("http://api.apistore.co.kr/kko/{apiVersion}/msg/{client_id}")
.header("x-waple-authorization", "고객 키")
field.put("phone", "01011112222")
field.put("callback", "01033334444")
field.put("reqdate", "20160517")
field.put("msg", "내용")
field.put("template_code", "01")
field.put("failed_type", "LMS")
field.put("url", "www.apistore.co.kr")
field.put("url_button_txt", "발송조회")
field.put("failed_subject", "API스토어")
field.put("failed_msg", "내용")
field.put("apiVersion", "1")
field.put("client_id", "apitest")
.asJson();
	 */


	String clientSecret;
	String sender;
	String serverUrl="http://api.apistore.co.kr/kko/1/msg/flyingcontents";
	String templateCode = "01";
		public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}



	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}




	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void send(String receiver, String title, String msg, Map btnFields) throws Exception
	{
		Map field = new HashMap();
		if(btnFields != null) field.putAll(btnFields);
		Map header = new HashMap();
		header.put("x-waple-authorization", this.getClientSecret());
		field.put("phone", receiver);
		field.put("callback", sender);
		field.put("msg", msg);
		if(field.get("btn_urls1") != null)
		{
			String[] arr = ((String)field.get("btn_urls1")).split(",");
			field.put("failed_msg", msg + "\n" + arr[0]);
		}
		else
		{
			field.put("failed_msg", msg);
		}

		field.put("template_code", this.getTemplateCode());
		String failed_msg = (String)field.get("failed_msg");
		if(failed_msg.length()>80)
		{
			field.put("failed_type", "LMS");
		}
		else
		{
			field.put("failed_type", "SMS");
		}

		field.put("failed_subject", title);

		//field.put("apiVersion", this.apiVersion);
		//field.put("client_id", this.getClientId());
		logger.debug("request ", field);
		JSONObject response = RequestHelper.postRequest(this.getServerUrl(), header, field);

		if(response.getString("result_code").equals("200"))
		{
			logger.debug("response ", response);
			return;
		}
		else
		{
			logger.error("response {}", response);
			throw new Exception("[" + response.getString("result_msg") + "]" + response.getString("result_msg"));
		}
	}

	@Override
	public void send( String receiver, String title, String msg) throws Exception {
		this.send( receiver, title, msg, null);

	}
}
