package com.fliconz.fm.common.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.fliconz.fm.common.util.TemplateHelper;
import com.fliconz.fm.common.util.TextHelper;

import freemarker.template.TemplateException;

public class MultiplePropertiesBundle extends ResourceBundle {
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	private static final String BUNDLE_EXTENSION = "properties";
	private static final Control UTF8_CONTROL = new UTF8Control();

	/**
	 * A Map containing the combined resources of all parts building this
	 * MultiplePropertiesResourceBundle.
	 */
	// private Map<String, Map> combined;
	private Set<String> m_baseNames = null;

	private List<String> m_locations = null;

	public MultiplePropertiesBundle() {
		super();
	}




	protected static class UTF8Control extends Control {
		public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader,
				boolean reload) throws IllegalAccessException, InstantiationException, IOException {
			// The below code is copied from default Control#newBundle()
			// implementation.
			// Only the PropertyResourceBundle line is changed to read the file
			// as UTF-8.
			String bundleName = toBundleName(baseName, locale);
			String resourceName = toResourceName(bundleName, BUNDLE_EXTENSION);
			ResourceBundle bundle = null;
			InputStream stream = null;
			if (reload) {
				URL url = loader.getResource(resourceName);
				if (url != null) {
					URLConnection connection = url.openConnection();
					if (connection != null) {
						connection.setUseCaches(false);
						stream = connection.getInputStream();
					}
				}
			} else {
				stream = loader.getResourceAsStream(resourceName);
			}
			if (stream != null) {
				try {
					bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
				} finally {
					stream.close();
				}
			}
			return bundle;
		}
	}


	public void setResourceLocations(List<String> locations) {
		this.m_locations = locations;
	}

	public void doLoad() {
		if (this.m_baseNames != null) {
			this.m_baseNames.clear();
			this.m_baseNames = null;
		}
	}

	@Override
	protected Object handleGetObject(String key) {
		if (key == null) {
			throw new NullPointerException();
		}
		this.findBaseNames();
		for (String base : this.m_baseNames) {
			try {
				String value = ResourceBundle.getBundle(base, getLocale(), UTF8_CONTROL).getString(key);
				if (value != null) {
					return value;
				}
			} catch (Exception e) {

			}
		}
		return null;
		/*
		 * loadBundlesOnce(); Map map = combined.get(key); if(map != null) {
		 * return map.get(getLocale().toString()); } return null;
		 */
	}

	@Override
	public Locale getLocale() {
		return Locale.getDefault();
		//return Locale.KOREA;
	}

	@Override
	public Enumeration<String> getKeys() {
		return parent.getKeys();
		/*
		 * loadBundlesOnce(); ResourceBundle parent = this.parent; return new
		 * sun.util.ResourceBundleEnumeration(combined.keySet(), (parent !=
		 * null) ? parent.getKeys() : null);
		 */
	}


	public Object getObject(String key, Object defaultValue) {
		Object resultValue = null;
		try {
			resultValue = super.getObject(key);
			if (resultValue == null) {
				return resultValue;
			}
		}
		catch (MissingResourceException e) {
			resultValue = defaultValue;
		}
		return resultValue;
	}
	public String getString(String key, String defaultValue) {
		String resultValue = null;
		try {
			resultValue = super.getString(key);
			if (resultValue == null) {
				return resultValue;
			}
		}
		catch (MissingResourceException e) {
			resultValue = defaultValue;
		}
		return resultValue;
	}

	public String getString(String key, Map args, String defaultValue) {
		String resultValue = null;
		try {
			resultValue = super.getString(key);
			resultValue = TemplateHelper.process(resultValue, args);
		}
		catch (MissingResourceException e) {
			resultValue = defaultValue;
		}
		catch (IOException e) {
			resultValue = defaultValue;
		}
		catch (TemplateException e) {
			e.printStackTrace();
		}
		return resultValue;
	}

	private void addBaseName(String path, String kubun) {
		int pos = path.indexOf(kubun);
		path = path.substring(pos + kubun.length() + 1);
		pos = path.lastIndexOf("_");
		path = path.substring(0, pos);
		path = path.replaceAll("\\\\", ".");
		path = path.replaceAll("/", ".");
		this.m_baseNames.add(path);
	}


	private void findBaseNames() {
		if (this.m_baseNames != null) {
			return;
		}

		this.m_baseNames = new HashSet<String>();
		if (this.m_locations == null || this.m_locations.isEmpty()) {
			return;
		}

		try {
			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			//Resource[] arrResource = resolver.getResources(PapaPropertyHolder.getPapaProperties().getProperty("com.multiplePropertiesBundle.message.location", "classpath:**/message_*.properties"));
			for (String resLocation : this.m_locations) {
				//if (theLogger.isDebugEnabled()) theLogger.debug("===> resource-location : [{}]", resLocation);
				if (TextHelper.isEmpty(resLocation)) {
					continue;
				}
				Resource[] arrResource = resolver.getResources(resLocation);
				for (Resource res : arrResource) {
					//if (theLogger.isDebugEnabled()) theLogger.debug("===> resource : [{}]", res.getURL().toString());
					try {
						String path = res.getFile().getPath();
						addBaseName(path, "classes");
					} catch (FileNotFoundException e) {
						String path = res.getURL().getFile();
						addBaseName(path, ".jar!");

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Object getObject(MultiplePropertiesBundle msgBundle, String key, Object defaultValue) {
		Object value = msgBundle.getObject(key, defaultValue);
		return value;
	}
	public static Object getObject(MultiplePropertiesBundle msgBundle, String key) {
		Object value = msgBundle.getObject(key);
		return value;
	}





	public static String getString(MultiplePropertiesBundle msgBundle, String key, String defaultValue) {
		String value = msgBundle.getString(key, defaultValue);
		return value;
	}
	public static String getString(MultiplePropertiesBundle msgBundle, String key) {
		String value = msgBundle.getString(key);
		return value;
	}




	public static String getString_replaceSpecial(MultiplePropertiesBundle msgBundle, String key, String defaultValue) {
		String value = msgBundle.getString(key, defaultValue);
		if (value != null) {
			value = value.replace("\r", "\\r").replace("\n", "\\n").replace("\\", "\\\\");
		}
		return value;
	}
	public static String getString_replaceLineFeedEntity(MultiplePropertiesBundle msgBundle, String key, String defaultValue) {
		String value = msgBundle.getString(key, defaultValue);
		if (value != null) {
			value = value.replace("\n", "&#10;");
		}
		return value;
	}
}
