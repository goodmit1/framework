/*
 * @(#)SAXSourceLocator.java
 */
package com.fliconz.fm.common.util;

import javax.xml.transform.SourceLocator;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.LocatorImpl;

public class SAXSourceLocator extends LocatorImpl implements SourceLocator
{
	/**
	 * SAXSourceLocator
	 *
	 * @param spe sax parse exception
	 */
	public SAXSourceLocator(SAXParseException spe) {
		super();
		super.setLineNumber( spe.getLineNumber() );
		super.setColumnNumber( spe.getColumnNumber() );
		super.setPublicId( spe.getPublicId() );
		super.setSystemId( spe.getSystemId() );
	}
}
