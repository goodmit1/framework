/*
 * (@)# XMLPropertiesLoader.java
 */
package com.fliconz.fm.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class XMLPropertiesLoader implements ServletContextListener
{
	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	public final static String KEY_CONTAINER = new StringBuilder().append(XMLProperties.class.getName()).append(".container").toString();

	public XMLPropertiesLoader() {

	}

/*
	<listener>
		<listener-class>com.fliconz.fm.common.util.XMLPropertiesLoader</listener-class>
	</listener>
	<context-param>
		<param-name>prop-config.json</param-name>
		<param-value>{"configPath":"/WEB-INF/prop-config","laguagePrefix":"ko"}</param-value>
	</context-param>



<?xml version="1.0" encoding="UTF-8"?>

<properties name="test">

	<property name="abc">가나다</property>

</properties>

*/

	//##############################################################################################################################

	public void contextInitialized(ServletContextEvent sce)
	{
		ServletContext ctx = sce.getServletContext();
		this.load(ctx);
	}

	public void load(ServletContext ctx)
	{
		try
		{
			String configString = ctx.getInitParameter("prop-config.json");
			if (configString == null) configString = "{\"configPath\":\"/WEB-INF/prop-config\"}";

			if (theLogger.isDebugEnabled()) theLogger.debug("configString : "+configString);
			//JSONObject jsonConfig = (JSONObject)JSONValue.parse(configString);
			org.json.JSONObject jsonConfig = new org.json.JSONObject(configString);

			Set<String> configSet = ctx.getResourcePaths((String)jsonConfig.get("configPath"));
			if (theLogger.isDebugEnabled()) theLogger.debug("configSet : "+configSet);

			if (configSet == null || configSet.isEmpty())
			{
				return;
			}

			Hashtable<String,XMLProperties> htContainer = new Hashtable<String,XMLProperties>();

			for (Iterator<String> it = configSet.iterator(); it.hasNext();)
			{
				String path = it.next();
				if (path.endsWith(".xml") == false)
				{
					continue;
				}
				this.loadXMLProperties(ctx, htContainer, path);
			}

			Hashtable<String,XMLProperties> oldContainer = (Hashtable<String,XMLProperties>)ctx.getAttribute(KEY_CONTAINER);
			if (oldContainer == null)
			{
				ctx.setAttribute(KEY_CONTAINER, htContainer);
			}
			else
			{
				oldContainer.clear();
				oldContainer.putAll(htContainer);
				htContainer.clear();
			}
		}
		catch (java.io.IOException e) {
			//System.out.println("===== xxxxxxxxxx");
			//e.printStackTrace();
			throw new IllegalStateException( e.getMessage() );
		}
		catch (org.json.JSONException e)
		{
			throw new IllegalStateException( e.getMessage() );
		}
	}


	private void loadXMLProperties(ServletContext ctx, Hashtable<String,XMLProperties> container, String path) throws IOException
	{
		InputStream in = null;

		try
		{
			in = ctx.getResourceAsStream(path);
			XMLProperties pt = new XMLProperties();
			pt.setServletContext(ctx);
			pt.load(in);

			container.put(pt.getName(), pt);


			if (theLogger.isDebugEnabled())
			{
				theLogger.debug("properties : "+path);
				pt.store(System.out);
			}
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (Exception e) {}
		}

	}


	public void contextDestroyed(ServletContextEvent sce)
	{
		ServletContext ctx = sce.getServletContext();
		ctx.removeAttribute(KEY_CONTAINER);
	}


}
