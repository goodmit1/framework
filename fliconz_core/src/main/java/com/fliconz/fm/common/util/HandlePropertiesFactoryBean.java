package com.fliconz.fm.common.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertiesFactoryBean;

public class HandlePropertiesFactoryBean extends PropertiesFactoryBean
{
	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	private final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	private Object handler = null;
	private String methodName = null;
	private Properties methodParam = null;

	public HandlePropertiesFactoryBean()
	{
		super();
	}



	public void setHandler(Object handler)
	{
		this.handler = handler;
		//if (this.theLogger.isDebugEnabled()) theLogger.debug("===> handler : {}", handler);
	}


	public void setMethodName(String methodName)
	{
		this.methodName = methodName;
		//if (this.theLogger.isDebugEnabled()) theLogger.debug("===> methodName : {}", methodName);
	}

	public void setMethodParam(Properties methodParam)
	{
		this.methodParam = methodParam;
		//if (this.theLogger.isDebugEnabled()) theLogger.debug("===> methodParam : {}", methodParam);
	}



	@Override
	protected Properties mergeProperties() //throws IOException
	{
		//if (theLogger.isWarnEnabled()) theLogger.warn("===> props : {}", props);

		try
		{
			Class aClass = this.handler.getClass();
			java.lang.reflect.Method aMethod = aClass.getMethod(this.methodName, Properties.class);

			//if (theLogger.isDebugEnabled()) theLogger.debug("===> methodParam : {}", methodParam);
			Object returnValue = aMethod.invoke(handler, this.methodParam);
			if (returnValue == null)
			{
				//if (theLogger.isDebugEnabled()) theLogger.debug("===> returnValue-000 : {}", returnValue);
				return new Properties();
			}

			//if (theLogger.isDebugEnabled()) theLogger.debug("===> returnValue-111 : {}", returnValue);
			return (Properties)returnValue;
		}
		catch (SecurityException e)
		{
			throw new IllegalStateException(e.getMessage(), e);
		}
		catch (NoSuchMethodException e)
		{
			throw new IllegalStateException(e.getMessage(), e);
		}
		catch (IllegalArgumentException e)
		{
			throw new IllegalStateException(e.getMessage(), e);
		}
		catch (IllegalAccessException e)
		{
			throw new IllegalStateException(e.getMessage(), e);
		}
		catch (InvocationTargetException e)
		{
			throw new IllegalStateException(e.getMessage(), e);
		}
	}
}
