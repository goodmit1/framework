package com.fliconz.fm.common.util;
/*
* @(#)XProperties.java
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
/**
* XProperties.
*
* @version	1.0
* @since	1.0	2003.03.26 Wed.
* @author	kiki (Kyoung Gu. LEE)
*/
public class XProperties extends Properties
{
	/**
	* Property key, value separator charactors : &quot;=: \t\r\n\f&quot;.
	*
	* <code>'=' : equals sign (char)61<br>
	* ':' : colon (char)58<br>
	* ' ' : space (char)32<br>
	* '\t' : horizontal tab (char)9<br>
	* '\r' : carrige return (char)13<br>
	* '\n' : line feed, new line (char)10<br>
	* '\f' : form feed, new page (char)12</code>
	*/
	public final static String KEY_VALUE_SEPARATORS = "=: \t\r\n\f";	//" \t\r\n\f";

	/**
	* Special save charactors : &quot;=: \t\r\n\f#!&quot;.
	*
	* <code>'=' : equals sign (char)61<br>
	* ':' :  colon (char)58<br>
	* ' ' : space (char)32<br>
	* '\t' : horizontal tab (char)9<br>
	* '\r' : carrige return (char)13<br>
	* '\n' : line feed, new line (char)10<br>
	* '\f' : form feed, new page (char)12<br>
	* '#' : number sign, sharp (char)35 -- remark<br>
	* '!' : exclamation mark (char)33 -- remark</code>
	*/
	public final static String SPECIAL_SAVE_CHARS = "=: \t\r\n\f#!";	//"\t\r\n\f#!";


	private final static String STRICT_KEY_VALUE_SEPARATORS = "=:";

//	/** A table of hex digits */
//	private final static char[] HEX_DIGIT = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F' };

	//##############################################################################################################################

	/**
	* Creates an empty property list with no default values.
	*/
	public XProperties()
	{
		super();
	}

	/*
	* Creates an empty property list with the specified defaults.
	*
	* <p>Default properties can not remove and reset (readonly).
	*
	* @param defaults the defaults.
	*/
	public XProperties(Properties defaults)
	{
		super(defaults);
	}

	//##############################################################################################################################

	/**
	* Reads a property list (key and element pairs) from the input stream.
	*
	* @param r a reader.
	*
	* @exception IOException if an error occurred when reading from the reader.
	*/
	@Override
	public synchronized void load(Reader r) throws IOException
	{
		BufferedReader in = r instanceof BufferedReader
			? (BufferedReader) r
			: new BufferedReader(r);

		while (true)
		{
			// Get next line
			String strLine = in.readLine();
			if (strLine == null) return;

			if (strLine.length() > 0)
			{
				// Continue lines that end in slashes if they are not comments
				char cFirst = strLine.charAt(0);
				if ((cFirst != '#') && (cFirst != '!'))
				{
					while (continueLine(strLine))
					{
						String strNextLine = in.readLine();
						if(strNextLine == null) strNextLine = new String("");

						String strLoppedLine = strLine.substring(0, strLine.length()-1);

						// Advance beyond whitespace on new line
						int startIndex=0;
						for(startIndex=0; startIndex<strNextLine.length(); startIndex++)
						{
							if (TextHelper.WHITE_SPACE.indexOf(strNextLine.charAt(startIndex)) == -1) break;
						}

						strNextLine = strNextLine.substring(startIndex,strNextLine.length());
						strLine = new String(strLoppedLine+strNextLine);
					}

					// Find start of key
					int intLen = strLine.length();
					int intKeyStart;
					for(intKeyStart=0; intKeyStart<intLen; intKeyStart++)
					{
						if(TextHelper.WHITE_SPACE.indexOf(strLine.charAt(intKeyStart)) == -1) break;
					}

					// Blank lines are ignored
					if (intKeyStart == intLen) continue;

					// Find separation between key and value
					int intSeparatorIndex;
					for(intSeparatorIndex=intKeyStart; intSeparatorIndex<intLen; intSeparatorIndex++)
					{
						char currentChar = strLine.charAt(intSeparatorIndex);
						if (currentChar == '\\')
							intSeparatorIndex++;
						else if(KEY_VALUE_SEPARATORS.indexOf(currentChar) != -1)
							break;
					}

					// Skip over whitespace after key if any
					int intValueIndex;
					for (intValueIndex=intSeparatorIndex; intValueIndex<intLen; intValueIndex++)
					{
						if (TextHelper.WHITE_SPACE.indexOf(strLine.charAt(intValueIndex)) == -1)
							break;
					}

					// Skip over one non whitespace key value separators if any
					if (intValueIndex < intLen)
					{
						if (STRICT_KEY_VALUE_SEPARATORS.indexOf(strLine.charAt(intValueIndex)) != -1)
							intValueIndex++;
					}

					// Skip over white space after other separators if any
					while (intValueIndex < intLen)
					{
						if (TextHelper.WHITE_SPACE.indexOf(strLine.charAt(intValueIndex)) == -1) break;
						intValueIndex++;
					}

					String strKey = strLine.substring(intKeyStart, intSeparatorIndex);
					String strValue = (intSeparatorIndex < intLen) ? strLine.substring(intValueIndex, intLen) : "";

					// Convert then store key and value
					strKey = loadConvert(strKey);
					strValue = loadConvert(strValue);
					put(strKey, strValue);
				}
			}
		}
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Reads a property list (key and element pairs) from the input stream.
	*
	* @param in an input stream.
	* @param encoding a charactor encoding.
	*
	* @exception IOException if an error occurred when reading from the input stream.
	*/
	public synchronized void load(InputStream in, String encoding) throws IOException
	{
		load( new InputStreamReader(in, encoding == null ? TextHelper.BASE_CHAR_SET : encoding) );
	}

	/**
	* Reads a property list (key and element pairs) from the input stream.
	*
	* <p>The stream is assumed to be using the {@link TextHelper#BASE_CHAR_SET} character encoding.
	*
	* @param in an input stream.
	*
	* @exception IOException if an error occurred when reading from the input stream.
	*/
	@Override
	public synchronized void load(InputStream in) throws IOException
	{
		load(in, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	public synchronized void load(File f, String encoding) throws IOException
	{
		FileInputStream in = null;
		try
		{
			in = new FileInputStream(f);
			load( new InputStreamReader(in, encoding == null ? TextHelper.BASE_CHAR_SET : encoding) );
		}
		finally
		{
			try
			{
				if (in != null) in.close();
			}
			catch (Exception e) {}
		}
	}

	public synchronized void load(File f) throws IOException
	{
		load(f, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	public synchronized void load(URL u, String encoding) throws IOException
	{
		InputStream in = null;
		try
		{
			in = u.openStream();
			load( new InputStreamReader(in, encoding == null ? TextHelper.BASE_CHAR_SET : encoding) );
		}
		finally
		{
			try
			{
				if (in != null) in.close();
			}
			catch (Exception e) {}
		}
	}

	public synchronized void load(URL u) throws IOException
	{
		load(u, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

//	public synchronized void load(String dir, String fileName, String encoding) throws IOException
//	{
//		load(new File(dir, fileName), encoding);
//	}
//
//	public synchronized void load(String dir, String fileName) throws IOException
//	{
//		load(new File(dir, fileName), null);
//	}


	public synchronized void load(String filePath, String encoding) throws IOException
	{
		load(new File(filePath), encoding);
	}

	public synchronized void load(String filePath) throws IOException
	{
		load(new File(filePath), null);
	}



	public synchronized void load(Class klass, String resourcePath, String encoding) throws IOException
	{
		InputStream in = null;

		try
		{
			in = klass.getResourceAsStream(resourcePath);
			if (in == null)
				throw new IOException(
					new StringBuffer(resourcePath)
						.append("resource is not found.")
						.toString()
				);

			load(in, encoding);
		}
		finally
		{
			try
			{
				if (in != null) in.close();
			}
			catch (Exception e) {}
		}
	}

	public synchronized void load(Class klass, String resourcePath) throws IOException
	{
		load(klass, resourcePath, null);
	}




	public synchronized void load(ServletContext ctx, String resourcePath, String encoding) throws IOException
	{
		InputStream in = null;

		try
		{
			in = ctx.getResourceAsStream(resourcePath);
			if (in == null)
				throw new IOException(
					new StringBuffer(resourcePath)
						.append("resource is not found.")
						.toString()
				);

			load(in, encoding);
		}
		finally
		{
			try
			{
				if (in != null) in.close();
			}
			catch (Exception e) {}
		}
	}

	public synchronized void load(ServletContext ctx, String resourcePath) throws IOException
	{
		load(ctx, resourcePath, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Writes this property list (key and element pairs) in this <code>XProperties</code> table to the writer in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param out a writer
	* @param header a description of the property list.
	*
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	@Override
	public synchronized void store(Writer out, String header)
	{
		PrintWriter pw = out instanceof PrintWriter
			? (PrintWriter) out
			: new PrintWriter(out, true);

		if (header != null)
		{
			pw.print("#");
			pw.println( remarkConvert(header) );
			pw.println();
		}

//		for ( Enumeration en = propertyNames( new LowerTextComparator() ); en.hasMoreElements(); )
		for ( Enumeration en = propertyNames(); en.hasMoreElements(); )
		{
			String name = (String)en.nextElement();
			pw.print( storeConvert(name) );
			pw.print("=");
			pw.println( storeConvert(getProperty(name), false) );
		}
	}

	/**
	* Writes this property list (key and element pairs) in this <code>XProperties</code> table to the writer in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param out an output stream
	*
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public synchronized void store(Writer out)
	{
		store(out, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Writes this property list (key and element pairs) in this <code>XProperties</code> table to the output stream in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param out an output stream
	* @param header a description of the property list.
	*
	* @see #store(OutputStream out)
	*
	* @exception IOException if writing this property list to the specified output stream throws an <tt>IOException</tt>.
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	@Override
	public synchronized void store(OutputStream out, String header) throws IOException
	{
		this.store( new OutputStreamWriter(out), header);
	}

	/**
	* Writes this property list (key and element pairs) in this <code>XProperties</code> table to the output stream in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param out an output stream
	*
	* @see #store(OutputStream out, String header)
	*
	* @exception IOException if writing this property list to the specified output stream throws an <tt>IOException</tt>.
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public synchronized void store(OutputStream out) throws IOException
	{
		this.store(new OutputStreamWriter(out), null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Write this property list (key and element pairs) in this <code>XProperties</code> table to the specified file in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param f a file object
	* @param encoding property key and value encoding name (default={@link TextHelper#BASE_CHAR_SET}).
	*
	* @see #store(File f)
	*
	* @exception IOException if writing this property list to the specified output stream throws an <tt>IOException</tt>.
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public synchronized void store(File f, String encoding) throws IOException
	{
		FileOutputStream out = null;
		try
		{
			out = new FileOutputStream(f);
			this.store( new OutputStreamWriter(out, encoding==null ? TextHelper.BASE_CHAR_SET : encoding) );
			out.flush();
		}
		finally
		{
			try
			{
				if (out != null) out.close();
			}
			catch (Exception e) {}
		}
	}

	/**
	* Write this property list (key and element pairs) in this <code>XProperties</code> table to the specified file in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param f a file object
	* @see #store(File f, String encoding)
	*
	* @exception IOException if writing this property list to the specified output stream throws an <tt>IOException</tt>.
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public synchronized void store(File f) throws IOException
	{
		this.store(f, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Write this property list (key and element pairs) in this <code>XProperties</code> table to the specified url in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param u an url object
	* @param encoding property key and value encoding name (default={@link TextHelper#BASE_CHAR_SET}).
	*
	* @see #store(URL u)
	*
	* @exception IOException if writing this property list to the specified output stream throws an <tt>IOException</tt>.
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public synchronized void store(URL u, String encoding) throws IOException
	{
		OutputStream out = null;
		try
		{
			URLConnection uc = u.openConnection();
			out = uc.getOutputStream();
			this.store( new OutputStreamWriter(out, encoding==null ? TextHelper.BASE_CHAR_SET : encoding) );
			out.flush();
		}
		finally
		{
			try
			{
				if (out != null) out.close();
			}
			catch (Exception e) {}
		}
	}

	/**
	* Write this property list (key and element pairs) in this <code>XProperties</code> table to the specified url in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param u an url object
	*
	* @see #store(URL u, String encoding)
	*
	* @exception IOException if writing this property list to the specified output stream throws an <tt>IOException</tt>.
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public synchronized void store(URL u) throws IOException
	{
		this.store(u, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Write this property list (key and element pairs) in this <code>XProperties</code> table to the specified file in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param path a file path.
	* @param encoding property key and value encoding name (default={@link TextHelper#BASE_CHAR_SET}).
	*
	* @see #store(String path)
	*
	* @exception IOException if writing this property list to the specified output stream throws an <tt>IOException</tt>.
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public synchronized void store(String path, String encoding) throws IOException
	{
		this.store(new File(path), encoding);
	}

	/**
	* Write this property list (key and element pairs) in this <code>XProperties</code> table to the specified file in a format suitable
	* for loading into a <code>XProperties</code> table using the <code>load</code> method.
	*
	* @param path a file path.
	*
	* @see #store(String path, String encoding)
	*
	* @exception IOException if writing this property list to the specified output stream throws an <tt>IOException</tt>.
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public synchronized void store(String path) throws IOException
	{
		this.store(new File(path), null);
	}




	public synchronized void writeTo(Writer out, String header)
	{
		this.store(out, header);
	}


	public synchronized void writeTo(Writer out)
	{
		this.store(out, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------


	public synchronized void writeTo(OutputStream out, String header) throws IOException
	{
		this.store( new OutputStreamWriter(out), header);
	}

	public synchronized void writeTo(OutputStream out) throws IOException
	{
		this.store(new OutputStreamWriter(out), null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	public synchronized void writeTo(File f, String encoding) throws IOException
	{
		this.store(f, encoding);
	}


	public synchronized void writeTo(File f) throws IOException
	{
		this.store(f, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------



	public synchronized void writeTo(URL u, String encoding) throws IOException
	{
		this.store(u, encoding);
	}


	public synchronized void writeTo(URL u) throws IOException
	{
		this.store(u, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------


	public synchronized void writeTo(String path, String encoding) throws IOException
	{
		this.store(new File(path), encoding);
	}

	public synchronized void writeTo(String path) throws IOException
	{
		this.store(new File(path), null);
	}


	//##############################################################################################################################

	/**
	* Prints this property list out to the specified print stream.
	*
	* <p>This method is useful for debugging.
	*
	* @param out a print stream.
	*
	* @see #list(PrintWriter pw)
	* @see #list()
	*
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	@Override
	public void list(PrintStream out)
	{
		out.println("## listing properties ##");
//		for ( Enumeration en = propertyNames( new LowerTextComparator() ); en.hasMoreElements(); )
		for ( Enumeration en = propertyNames(); en.hasMoreElements(); )
		{
			String name = (String)en.nextElement();
			out.print(name);
			out.print("=");
			out.println( getProperty(name) );
			if ( en.hasMoreElements() ) out.println();
		}
	}

	/**
	* Prints this property list out to the specified print writer.
	*
	* <p>This method is useful for debugging.
	*
	* @param out a print writer.
	*
	* @see #list(PrintStream ps)
	* @see #list()
	*
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	@Override
	public void list(PrintWriter out)
	{
		out.println("## listing properties ##");
//		for ( Enumeration en = propertyNames( new LowerTextComparator() ); en.hasMoreElements(); )
		for ( Enumeration en = propertyNames(); en.hasMoreElements(); )
		{
			String name = (String)en.nextElement();
			out.print(name);
			out.print("=");
			out.println( getProperty(name) );
			if ( en.hasMoreElements() ) out.println();
		}
	}


	/**
	* Prints this property list out to the <code>System.out</code>.
	*
	* @see #list(PrintStream ps)
	* @see #list(PrintWriter pw)
	*
	* @exception java.lang.ClassCastException  if this <code>XProperties</code> object contains any keys that are not <code>String</code>.
	*/
	public void list()
	{
		list(System.out);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Tests if this <code>XProperties</code> maps no keys to values.
	*
	* @return <code>true</code> if this <code>XProperties</code> maps no keys to values; <code>false</code> otherwise
	*/
	@Override
	public boolean isEmpty()
	{
		return super.defaults == null
			? super.isEmpty()
			: super.defaults.isEmpty() && super.isEmpty();
	}

	/**
	* Tests if this <code>XProperties</code> maps has a default properties object
	*
	* @return <code>true</code> if this <code>XProperties</code> maps has a default properties object; <code>false</code> otherwise
	*/
	public boolean hasDefaultProperties()
	{
		return super.defaults != null;
	}

	/**
	* Creates a shallow copy of this <code>XProperties</code>.
	*
	*<p>All the structure of the <code>XProperties</code> itself is copied, but the keys and values are not cloned.
	* This is a relatively expensive operation.
	*
	* @return a clone of the <code>XProperties</code>.
	*/
	@Override
	public Object clone()
	{
		XProperties propClone = super.defaults == null
			? new XProperties()
			: new XProperties( (Properties)defaults.clone() );

		for ( Enumeration en = super.keys(); en.hasMoreElements(); )
		{
			Object oKey = en.nextElement();
			propClone.put( oKey, super.get(oKey) );
		}

		return propClone;
	}


	//-----------------------------------------------------------------------------------------------------------------------------
//
//	/**
//	* Returns an enumeration of all the keys in this <code>XProperties</code>.
//	*
//	* @param sc a property key comparator.
//	* @return a property key enumeration.
//	*/
//	public Enumeration propertyNames(StringComparator sc)
//	{
//		if (sc == null) propertyNames();
//
//		LinkedList lst = new LinkedList();
//		for ( Enumeration en = super.keys(); en.hasMoreElements(); )
//		{
//			Object aKey = en.nextElement();
//			lst.add(aKey);
//		}
//
//		if ( super.defaults != null )
//		{
//			for ( Enumeration en = super.defaults.keys(); en.hasMoreElements(); )
//			{
//				Object aKey = en.nextElement();
//				if ( lst.indexOf(aKey) == -1 ) lst.add(aKey);
//			}
//		}
//
//		Collections.sort(lst, sc);
//		return new XEnumeration(lst);
//	}

	/**
	* Returns an enumeration of all the keys in this <code>XProperties</code>.
	*
	* @return a property key enumeration.
	*/
	@Override
	public Enumeration propertyNames()
	{
		return super.propertyNames();
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Tests if some key maps into the specified value in this <code>XProperties</code>.
	*
	* <p>This operation is more expensive than the containsKey method.
	* <p>Note that this method is identical in functionality to containsValue, (which is part of the Map interface in the collections framework).
	*
	* @param value a value to search for.
	* @return <code>true</code> if and only if some key maps to the value argument in this <code>XProperties</code> as determined by the equals method;
	* <code>false</code> otherwise.
	*/
	@Override
	public boolean contains(Object value)
	{
		return super.defaults == null
			? super.contains(value)
			: super.contains(value) || super.defaults.contains(value);
	}


//
//	/**
//	* Returns an enumeration of the values contained in this <code>XProperties</code>.
//	*
//	* @param sc property value comparator.
//	* @return a property value enumeration.
//	*/
//	public Enumeration propertyValues(StringComparator sc)
//	{
//		if (sc == null) return propertyValues();
//
//		LinkedList lst = new LinkedList();
//		if ( super.defaults != null ) lst.addAll( super.defaults.values() );
//		lst.addAll( super.values() );
//
//		Collections.sort(lst, sc);
//		return new XEnumeration( lst.iterator() );
//	}

	/**
	* Returns an enumeration of the values contained in this <code>XProperties</code>.
	*
	* @return a property value enumeration.
	*/
	public Enumeration propertyValues()
	{
		if ( super.defaults == null ) return super.elements();

		LinkedList lst = new LinkedList( super.defaults.values() );
		lst.addAll( super.values() );

		return new XEnumeration( lst.iterator() );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Tests if the specified object is a key in this <code>XProperties</code>.
	*
	* @param key possible key.
	* @return <code>true</code> if and only if the specified object is a key in this properties,
	*	as determined by the equals method; <code>false</code> otherwise.
	*
	* @see #hasValue(Object value)
	*/
	@Override
	public boolean containsKey(Object key)
	{
		return hasKey(key);
	}

	/**
	* Tests if the specified object is a key in this <code>XProperties</code>.
	*
	* @param key possible key.
	* @return <code>true</code> if and only if the specified object is a key in this properties,
	*	as determined by the equals method; <code>false</code> otherwise.
	*
	* @see #hasValue(Object value)
	*/
	public boolean hasKey(Object key)
	{
		return super.defaults == null
			? super.containsKey(key)
			: super.containsKey(key) || super.defaults.containsKey(key);
	}

	/**
	* <code>hasKey( String.valueOf(key) )</code>.
	*
	* @see #hasKey(Object key)
	*/
	public boolean hasKey(int key)
	{
		return hasKey( String.valueOf(key) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Returns true if this <code>XProperties</code> maps one or more keys to this value.
	*
	* <p>Note that this method is identical in functionality to contains (which predates the Map interface).
	*
	* @param value value whose presence in this Hashtable is to be tested.
	* @return <code>true</code> if this map maps one or more keys to the specified value.
	*
	* @see #hasKey(Object value)
	*/
	@Override
	public boolean containsValue(Object value)
	{
		return hasValue(value);
	}

	/**
	* Returns true if this <code>XProperties</code> maps one or more keys to this value.
	*
	* <p>Note that this method is identical in functionality to contains (which predates the Map interface).
	*
	* @param value value whose presence in this Hashtable is to be tested.
	* @return <code>true</code> if this map maps one or more keys to the specified value.
	*
	* @see #hasKey(Object key)
	*/
	public boolean hasValue(Object value)
	{
		return super.defaults == null
			? super.containsValue(value)
			: super.containsValue(value) || super.defaults.containsValue(value);
	}

	/**
	* <code>hasValue( String.valueOf(value) )</code>.
	*
	* @see #hasValue(Object value)
	*/
	public boolean hasValue(int value)
	{
		return hasValue( String.valueOf(value) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Searches for the property with the specified key in this property list.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found.
	*
	* @param key the property key.
	* @param defaultValue a default value.
	* @return the value in this property list with the specified key value.
	*/
	@Override
	public String getProperty(String key, String defaultValue)
	{
		Object aValue = getObjectValue(key, defaultValue);

		return aValue == null ? null : aValue.toString();
	}

	/**
	* Searches for the property with the specified key in this property list.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>null</code> if the property is not found.
	*
	* @param key the property key.
	* @return the value in this property list with the specified key value.
	*/
	@Override
	public String getProperty(String key)
	{
		Object aValue = getObjectValue(key, null);

		return aValue == null ? null : aValue.toString();
	}

	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>null</code> if the property is not found.
	*
	* @param key the property key.
	* @return the value in this property list with the specified key value.
	*/
	public String getProperty(int key, String defaultValue)
	{
		return getProperty( String.valueOf(key), defaultValue );
	}

	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>null</code> if the property is not found.
	*
	* @param key the property key.
	* @return the value in this property list with the specified key value.
	*/
	public String getProperty(int key)
	{
		return getProperty(key, null);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Searches for the property with the specified key in this property list.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found.
	*
	* @param key the property key.
	* @param defaultValue a default value.
	* @return the value in this property list with the specified key value.
	*
	* @see #getProperty(String key)
	*/
	public String getString(String key, String defaultValue)
	{
		return getProperty(key, defaultValue);
	}

	/**
	* Searches for the property with the specified key in this property list.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>null</code> if the property is not found.
	*
	* @param key the property key.
	* @return the value in this property list with the specified key value.
	*
	* @see #getProperty(String key)
	*/
	public String getString(String key)
	{
		return getProperty(key, (String)null);
	}

	/**
	* Searches for the property value (message format pattern) with the specified key in this property list and format a property value with a arg argument value.
	*
	* <p><code>java.text.MessageFormat.format(String pattern, Object[] arguments)</code>.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>null</code> if the property is not found.
	*
	* <p>If the args argument value is <code>null</code>, returns a pattern string (property value).
	*
	* @param key the property key.
	* @param args an message format pattern arguments.
	* @return the value in this property list with the specified key value.
	*/
	public String getString(String key, Object[] args)
	{
		String strValue = getProperty(key);
		return strValue == null
			? null
			: args == null ? strValue : MessageFormat.format(strValue, args);
	}



	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>null</code> if the property is not found.
	*
	* @param key the property key.
	* @return the value in this property list with the specified key value.
	*/
	public String getString(int key, String defaultValue)
	{
		return getProperty(key, defaultValue);
	}

	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>null</code> if the property is not found.
	*
	* @param key the property key.
	* @return the value in this property list with the specified key value.
	*/
	public String getString(int key)
	{
		return getProperty(key, (String)null);
	}

	/**
	* Searches for the property value (message format pattern) with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and format a property value with a arg argument value.
	*
	* <p><code>java.text.MessageFormat.format(String pattern, Object[] arguments)</code>.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>null</code> if the property is not found.
	*
	* <p>If the args argument value is <code>null</code>, returns a pattern string (property value).
	*
	* @param key the property key.
	* @param args an message format pattern arguments.
	* @return the value in this property list with the specified key value.
	*/
	public String getString(int key, Object[] args)
	{
		return getString( String.valueOf(key), args);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>boolean</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>boolean</code> value.
	* @return the parsed <code>boolean</code> value in this property list with the specified key value.
	*/
	public boolean getBoolean(String key, boolean defaultValue)
	{
		try
		{
			Object aValue = getObjectValue(key, null);

			if (aValue == null)
				return defaultValue;

			else if (aValue instanceof String)
				return Boolean.valueOf( (String)aValue ).booleanValue();

			else if (aValue instanceof Boolean)
				return ( (Boolean)aValue ).booleanValue();

			return defaultValue;
		}
		catch (Exception e)
		{
			return defaultValue;
		}
	}

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>boolean</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>false</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>boolean</code> value in this property list with the specified key value.
	*/
	public boolean getBoolean(String key)
	{
		return getBoolean(key, false);
	}


	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>boolean</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>boolean</code> value.
	* @return the parsed <code>boolean</code> value in this property list with the specified key value.
	*/
	public boolean getBoolean(int key, boolean defaultValue)
	{
		return getBoolean( String.valueOf(key), defaultValue );
	}

	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>boolean</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>false</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>boolean</code> value in this property list with the specified key value.
	*/
	public boolean getBoolean(int key)
	{
		return getBoolean( String.valueOf(key) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>int</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>int</code> value.
	* @return the parsed <code>int</code> value in this property list with the specified key value.
	*/
	public int getInt(String key, int defaultValue)
	{
		try
		{
			Object aValue = getObjectValue(key, null);

			return aValue == null
				? defaultValue
				: NumberFormat.getInstance().parse( aValue.toString() ).intValue();
		}
		catch (Exception e)
		{
			return defaultValue;
		}
	}

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>int</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>0</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>int</code> value in this property list with the specified key value.
	*/
	public int getInt(String key)
	{
		return getInt(key, 0);
	}


	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>int</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>int</code> value.
	* @return the parsed <code>int</code> value in this property list with the specified key value.
	*/
	public int getInt(int key, int defaultValue)
	{
		return getInt( String.valueOf(key), defaultValue );
	}

	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>int</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>0</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>int</code> value in this property list with the specified key value.
	*/
	public int getInt(int key)
	{
		return getInt( String.valueOf(key) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>long</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>long</code> value.
	* @return the parsed <code>long</code> value in this property list with the specified key value.
	*/
	public long getLong(String key, long defaultValue)
	{
		try
		{
			Object aValue = getObjectValue(key, null);

			return aValue == null
				? defaultValue
				: NumberFormat.getInstance().parse( aValue.toString() ).longValue();
		}
		catch (Exception e)
		{
			return defaultValue;
		}
	}

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>long</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>0L</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>long</code> value in this property list with the specified key value.
	*/
	public long getLong(String key)
	{
		return getLong(key, 0L);
	}


	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>long</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>long</code> value.
	* @return the parsed <code>long</code> value in this property list with the specified key value.
	*/
	public long getLong(int key, long defaultValue)
	{
		return getLong( String.valueOf(key), defaultValue );
	}

	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>long</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>0L</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>long</code> value in this property list with the specified key value.
	*/
	public long getLong(int key)
	{
		return getLong( String.valueOf(key) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>float</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>float</code> value.
	* @return the parsed <code>float</code> value in this property list with the specified key value.
	*/
	public float getFloat(String key, float defaultValue)
	{
		try
		{
			Object aValue = getObjectValue(key, null);

			return aValue == null
				? defaultValue
				: NumberFormat.getInstance().parse( aValue.toString() ).floatValue();
		}
		catch (Exception e)
		{
			return defaultValue;
		}
	}

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>float</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>0.0F</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>float</code> value in this property list with the specified key value.
	*/
	public float getFloat(String key)
	{
		return getFloat(key, 0.0F);
	}


	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>float</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>float</code> value.
	* @return the parsed <code>float</code> value in this property list with the specified key value.
	*/
	public float getFloat(int key, float defaultValue)
	{
		return getFloat( String.valueOf(key), defaultValue );
	}

	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>float</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>0.0F</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>float</code> value in this property list with the specified key value.
	*/
	public float getFloat(int key)
	{
		return getFloat( String.valueOf(key) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>double</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>double</code> value.
	* @return the parsed <code>double</code> value in this property list with the specified key value.
	*/
	public double getDouble(String key, double defaultValue)
	{
		try
		{
			Object aValue = getObjectValue(key, null);

			return aValue == null
				? defaultValue
				: NumberFormat.getInstance().parse( aValue.toString() ).doubleValue();
		}
		catch (Exception e)
		{
			return defaultValue;
		}
	}

	/**
	* Searches for the property with the specified key in this property list and returns a parsed <code>double</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>0.0</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>double</code> value in this property list with the specified key value.
	*/
	public double getDouble(String key)
	{
		return getDouble(key, 0.0);
	}


	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>double</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns the default value argument if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @param defaultValue a default <code>double</code> value.
	* @return the parsed <code>double</code> value in this property list with the specified key value.
	*/
	public double getDouble(int key, double defaultValue)
	{
		return getDouble( String.valueOf(key), defaultValue );
	}

	/**
	* Searches for the property with the specified <code>int</code> key (<code>String.valueOf(key)</code>) in this property list
	* and returns a parsed <code>double</code> value.
	*
	* <p>If the key is not found in this property list, the default property list, and its defaults, recursively, are then checked.
	* The method returns <code>0.0</code> if the property is not found or parsing failed.
	*
	* @param key the property key.
	* @return the parsed <code>double</code> value in this property list with the specified key value.
	*/
	public double getDouble(int key)
	{
		return getDouble( String.valueOf(key) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	public String[] getStringArray(String key, String delim, boolean skipNull)
	{
		Object aValue = getObjectValue(key, null);
		if (aValue == null) return null;

		return aValue instanceof String[]
			? (String[])aValue
			: TextHelper.split( aValue.toString(), delim, skipNull );
	}
	public String[] getStringArray(String key, char delim, boolean skipNull)
	{
		return this.getStringArray(key, delim, skipNull);
	}

	public String[] getStringArray(String key, boolean skipNull)
	{
		return getStringArray(key, ',', skipNull);
	}

	public String[] getStringArray(String key, char delim)
	{
		return getStringArray(key, delim, true);
	}

	public String[] getStringArray(String key, String delim)
	{
		return getStringArray(key, delim, true);
	}

	public String[] getStringArray(String key)
	{
		return getStringArray(key, ',', true);
	}


	public String[] getStringArray(int key, char delim, boolean skipNull)
	{
		return getStringArray( String.valueOf(key), delim, skipNull );
	}
	public String[] getStringArray(int key, String delim, boolean skipNull)
	{
		return getStringArray( String.valueOf(key), delim, skipNull );
	}

	public String[] getStringArray(int key, boolean skipNull)
	{
		return getStringArray( String.valueOf(key), ',', skipNull );
	}

	public String[] getStringArray(int key, char delim)
	{
		return getStringArray( String.valueOf(key), delim, true );
	}

	public String[] getStringArray(int key, String delim)
	{
		return getStringArray( String.valueOf(key), delim, true );
	}

	public String[] getStringArray(int key)
	{
		return getStringArray( String.valueOf(key), ',', true );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	public Properties getProperties(String key, char delim)
	{
		Object aValue = getObjectValue(key, null);
		if (aValue == null) return null;

		return aValue instanceof Properties
			? (Properties)aValue
			: split( aValue.toString(), delim );
	}

	public Properties getProperties(String key)
	{
		return getProperties(key, ',');
	}


	public Properties getProperties(int key, char delim)
	{
		return getProperties( String.valueOf(key), ',' );
	}

	public Properties getProperties(int key)
	{
		return getProperties( String.valueOf(key), ',' );
	}



	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Maps the specified key to the specified value in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* <p>If the value argument value is <code>null</code>, removes the key (and its corresponding value) from this <code>XProperties</code>.
	*
	* @param key a property key
	* @param value a property value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*/
	@Override
	public Object put(Object key, Object value)
	{
		return value == null ? super.remove(key) : super.put(key, value);
	}

	/**
	* Removes the key (and its corresponding value) from this <code>XProperties</code>. This method does nothing if the key is not in the <code>XProperties</code>.
	*
	* @param key the key that needs to be removed
	* @return the value to which the key had been mapped in this <code>XProperties</code>, or <code>null</code> if the key did not have a mapping
	*/
	public Object remove(String key)
	{
		return super.remove(key);
	}

	/**
	* Removes the <code>int</code> key (<code>String.valueOf(key)</code>) (and its corresponding value) from this <code>XProperties</code>.
	* This method does nothing if the key is not in the <code>XProperties</code>.
	*
	* @param key the key that needs to be removed
	* @return the value to which the key had been mapped in this <code>XProperties</code>, or <code>null</code> if the key did not have a mapping
	*/
	public Object remove(int key)
	{
		return super.remove( String.valueOf(key) );
	}

	/**
	* Copies all of the mappings from the specified Map to this <code>XProperties</code>.
	* These mappings will replace any mappings that this <code>XProperties</code> had for any of the keys currently in the specified Map.
	*
	* @param m Mappings to be stored in this <code>XProperties</code>.
	*/
	@Override
	public void putAll(Map m)
	{
		if (m == null || m.size() == 0) return;

		for ( Iterator it = m.keySet().iterator(); it.hasNext(); )
		{
			Object aKey = it.next();
			put( aKey, m.get(aKey) );
		}
	}

	/**
	* Calls the put method.
	* Provided for parallelism with the getProperty method.
	* Enforces use of strings for property keys and values
	*
	* @param key a property key.
	* @param value a property value.
	*
	* @see #put(Object key, Object value)
	*/
	@Override
	public Object setProperty(String key, String value)
	{
		return put(key, value);
	}

	/**
	* Calls the put method (<code>String.valueOf(key)</code>).
	*
	* @param key a property key.
	* @param value a property value.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setProperty(int key, String value)
	{
		return put( String.valueOf(key), value );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Calls the setProperty method.
	*
	* @param key a property key.
	* @param value a property value.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setString(String key, String value)
	{
		return put(key, value);
	}

	/**
	* Calls the setProperty method (<code>String.valueOf(key)</code>).
	*
	* @param key a property key.
	* @param value a property value.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setString(int key, String value)
	{
		return put( String.valueOf(key), value);
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Maps the specified key to the specified <code>boolean</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>boolean</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setBoolean(String key, boolean value)
	{
		return super.put( key, String.valueOf(value) );
	}

	/**
	* Maps the specified <code>int</code> key (<code>String.valueOf(key)</code>)
	* to the specified <code>boolean</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>boolean</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setBoolean(int key, boolean value)
	{
		return super.put( String.valueOf(key), String.valueOf(value) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Maps the specified key to the specified <code>int</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>int</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setInt(String key, int value)
	{
		return super.put( key, String.valueOf(value) );
	}

	/**
	* Maps the specified <code>int</code> key (<code>String.valueOf(key)</code>)
	* to the specified <code>int</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>int</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setInt(int key, int value)
	{
		return super.put( String.valueOf(key), String.valueOf(value) );
	}




	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Maps the specified key to the specified <code>long</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>long</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setLong(String key, long value)
	{
		return super.put( key, String.valueOf(value) );
	}

	/**
	* Maps the specified <code>int</code> key (<code>String.valueOf(key)</code>)
	* to the specified <code>long</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>long</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setLong(int key, long value)
	{
		return super.put( String.valueOf(key), String.valueOf(value) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Maps the specified key to the specified <code>float</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>float</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setFloat(String key, float value)
	{
		return super.put( key, String.valueOf(value) );
	}

	/**
	* Maps the specified <code>int</code> key (<code>String.valueOf(key)</code>)
	* to the specified <code>float</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>float</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setFloat(int key, float value)
	{
		return super.put( String.valueOf(key), String.valueOf(value) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	/**
	* Maps the specified key to the specified <code>double</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>double</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setDouble(String key, double value)
	{
		return super.put( key, String.valueOf(value) );
	}

	/**
	* Maps the specified <code>int</code> key (<code>String.valueOf(key)</code>)
	* to the specified <code>double</code> value (<code>String.valueOf(value)</code>) in this <code>XProperties</code>.
	* The key can not be <code>null</code>.
	*
	* <p>The value can be retrieved by calling the get method with a key that is equal to the original key.
	*
	* @param key a property key
	* @param value a <code>double</code> value
	* @return the previous value of the specified key in this <code>XProperties</code>, or <code>null</code> if it did not have one.
	*
	* @see #setProperty(String key, String value)
	*/
	public Object setDouble(int key, double value)
	{
		return super.put( String.valueOf(key), String.valueOf(value) );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	public Object setStringArray(String key, String[] value, char delim, boolean skipNull)
	{
		return put( key, TextHelper.join(value, String.valueOf(delim), skipNull) );
	}

	public Object setStringArray(String key, String[] value, boolean skipNull)
	{
		return put( key, TextHelper.join(value, skipNull) );
	}

	public Object setStringArray(String key, String[] value, char delim)
	{
		return put( key, TextHelper.join(value, String.valueOf(delim)) );
	}

	public Object setStringArray(String key, String[] value)
	{
		return put( key, TextHelper.join(value) );
	}



	public Object setStringArray(int key, String[] value, char delim, boolean skipNull)
	{
		return setStringArray(  String.valueOf(key), value, delim, skipNull );
	}

	public Object setStringArray(int key, String[] value, boolean skipNull)
	{
		return setStringArray( String.valueOf(key), value, skipNull );
	}

	public Object setStringArray(int key, String[] value, char delim)
	{
		return setStringArray( String.valueOf(key), value, delim );
	}

	public Object setStringArray(int key, String[] value)
	{
		return setStringArray( String.valueOf(key), value );
	}


	//-----------------------------------------------------------------------------------------------------------------------------

	public Object setProperties(String key, Properties value, char delim)
	{
		return put( key, join(value, delim) );
	}

	public Object setProperties(String key, Properties value)
	{
		return setProperties(key, value);
	}


	public Object setProperties(int key, Properties value, char delim)
	{
		return setProperties( String.valueOf(key), value, delim );
	}

	public Object setProperties(int key, Properties value)
	{
		return setProperties( String.valueOf(key), value );
	}


	//##############################################################################################################################

	public static Properties split(String props, char delim)
	{
		if (props == null) return null;

		XProperties pt = new XProperties();
		for (StringTokenizer st = new StringTokenizer(props, String.valueOf(delim)); st.hasMoreTokens(); )
		{
			String strToken = st.nextToken();
			int intIndex = strToken.indexOf("=");
			pt.setProperty(
				loadConvert( strToken.substring(0, intIndex) ),
				loadConvert( strToken.substring(intIndex+1) )
			);
		}

		return pt;
	}

	public static Properties split(String props)
	{
		return split(props, ',');
	}

	public static String join(Properties pt, char delim)
	{
		if (pt == null || pt.size() == 0) return null;

		StringBuffer sb = new StringBuffer();
		Enumeration en = pt.propertyNames();
		while ( en.hasMoreElements() )
		{
			String name = en.nextElement().toString();
			sb.append(delim);
			sb.append( name );
			sb.append("=");
			sb.append( pt.getProperty(name) );
		}

		return sb.substring(1);
	}

	public static String join(Properties pt)
	{
		return join(pt, ',');
	}

	//-----------------------------------------------------------------------------------------------------------------------------

	public static String storeConvert(String value, boolean escapeSpace)
	{
		if ( value == null ) return "";

		int intLen = value.length();
		StringBuffer sb = new StringBuffer();

		for(int x=0; x<intLen; x++)
		{
			char aChar = value.charAt(x);
			switch(aChar)
			{
				case ' ':
					if (x == 0 || escapeSpace) sb.append('\\');
					sb.append(' ');
					break;

				case '\\':
					sb.append("\\\\");
					break;

				case '\t':
					sb.append("\\t");
					break;

				case '\n':
					sb.append("\\n\\");
					sb.append(FileHelper.LINE_SEPARATOR);
					break;

				case '\r':
					sb.append("\\r\\");
					sb.append(FileHelper.LINE_SEPARATOR);
					break;

				case '\f':
					sb.append("\\f");
					break;

				default:
					if (SPECIAL_SAVE_CHARS.indexOf(aChar) != -1) sb.append('\\');
					sb.append(aChar);

//					sb.append( aChar );
//					if ( (aChar < 0x0020) || (aChar > 0x007e) )
//					{
//						sb.append('\\');
//						sb.append('u');
//						sb.append(toHex((aChar >> 12) & 0xF));
//						sb.append(toHex((aChar >>  8) & 0xF));
//						sb.append(toHex((aChar >>  4) & 0xF));
//						sb.append(toHex( aChar        & 0xF));
//					}
//					else
//					{
//						if (SPECIAL_SAVE_CHARS.indexOf(aChar) != -1) sb.append('\\');
//						sb.append(aChar);
//					}
			}
		}

		return sb.toString();
	}

	public static String storeConvert(String value)
	{
		return storeConvert(value, true);
	}

	/**
	* Remark a string value.
	*
	* @param value a string.
	* @return remarked string.
	*/
	public static String remarkConvert(String value)
	{
		if (value == null) return "null";

		int intLen = value.length();
		StringBuffer sb = new StringBuffer();
		//sb.append('#');

		for(int x=0; x<intLen; x++)
		{
			char aChar = value.charAt(x);
			switch(aChar)
			{
				case '\\':
					sb.append("\\\\");
					break;

				case '\t':
					sb.append("\\t");
					break;

				case '\n':
					sb.append("\\n\\");
					sb.append(FileHelper.LINE_SEPARATOR);
					sb.append('#');
					break;

				case '\r':
					sb.append("\\r\\");
					sb.append(FileHelper.LINE_SEPARATOR);
					sb.append('#');
					break;

				case '\f':
					sb.append("\\f");
					break;

				default:
					//if (SPECIAL_SAVE_CHARS.indexOf(aChar) != -1) sb.append('\\');
					sb.append(aChar);
			}
		}

		return sb.toString();
	}


	//and convert encoded &#92;uxxxx to unicode chars
	/*
	* Changes special saved chars to their original forms.
	*
	* @param value a string.
	* @return converted string.
	*/
	public static String loadConvert (String value)
	{
		char aChar;
		int intLen = value.length();
		StringBuffer sb = new StringBuffer(intLen);

		for(int x=0; x<intLen; )
		{
			aChar = value.charAt(x++);
			if (aChar == '\\')
			{
				aChar = value.charAt(x++);
				if(aChar == 'u')
				{
					// Read the xxxx
					int intValue=0;
					for (int i=0; i<4; i++)
					{
						aChar = value.charAt(x++);
						switch (aChar) {
							case '0':
							case '1':
							case '2':
							case '3':
							case '4':
							case '5':
							case '6':
							case '7':
							case '8':
							case '9':
								intValue = (intValue << 4) + aChar - '0';
								break;

							case 'a':
							case 'b':
							case 'c':
							case 'd':
							case 'e':
							case 'f':
								intValue = (intValue << 4) + 10 + aChar - 'a';
								break;

							case 'A':
							case 'B':
							case 'C':
							case 'D':
							case 'E':
							case 'F':
								intValue = (intValue << 4) + 10 + aChar - 'A';
								break;

							default:
								throw new IllegalArgumentException("Malformed \\uxxxx encoding.");
						}
					}

					sb.append((char)intValue);
				}
				else
				{
					if (aChar == 't')
						aChar = '\t';

					else if (aChar == 'r')
						aChar = '\r';

					else if (aChar == 'n')
						aChar = '\n';

					else if (aChar == 'f')
						aChar = '\f';

					sb.append(aChar);
				}
			}
			else
			{
				sb.append(aChar);
			}
		}

		return sb.toString();
	}

	//##############################################################################################################################

	private Object getObjectValue(Object key, Object defaultValue)
	{
		Object aValue = super.get(key);
		if (aValue == null)
		{
			if (super.defaults == null) return defaultValue;

			aValue = super.defaults.get(key);
			if (aValue == null && key instanceof String)
				aValue = super.defaults.getProperty( (String)key );

			return aValue == null
				? defaultValue
				: aValue;
		}

		return aValue;
	}


	/*
	* Returns true if the given line is a line that must be appended to the next line.
	*/
	private boolean continueLine (String line)
	{
		int slashCount = 0;
		int index = line.length() - 1;
		while( (index >= 0) && (line.charAt(index--) == '\\') )
		{
			slashCount++;
		}

		return (slashCount % 2 == 1);
	}

	public boolean isEmptyValue(String key)
	{
		return TextHelper.isEmpty(this.getProperty(key));
	}

	public boolean isNotEmptyValue(String key)
	{
		return this.isEmptyValue(key) == false;
	}

//	private boolean continueLine(String line)
//	{
//		int slashCount = 0;
//		int index = line.length() - 1;
//		while ( (index >= 0) && (line.charAt(index--) == '\\') )
//		{
//			slashCount++;
//		}
//
//		return (slashCount % 2 == 1);
//	}


	public Object addInt(String key, int value)
	{
		return this.setInt(key, this.getInt(key) + value);
	}

	public Object addLong(String key, long value)
	{
		return this.setLong(key, this.getLong(key) + value);
	}

	public Object addFloat(String key, float value)
	{
		return this.setFloat(key, this.getFloat(key) + value);
	}

	public Object addDouble(String key, double value)
	{
		return this.setDouble(key, this.getDouble(key) + value);
	}




	public Object addInt(int key, int value)
	{
		return this.setInt(key, this.getInt(key) + value);
	}

	public Object addLong(int key, long value)
	{
		return this.setLong(key, this.getLong(key) + value);
	}

	public Object addFloat(int key, float value)
	{
		return this.setFloat(key, this.getFloat(key) + value);
	}

	public Object addDouble(int key, double value)
	{
		return this.setDouble(key, this.getDouble(key) + value);
	}
}
