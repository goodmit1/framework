package com.fliconz.fm.common.scheduler;

//import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;

//import com.fliconz.fm.common.util.XProperties;

public class XJobDetail implements JobDetail
//, Cloneable, Serializable
{
	private JobDetail m_jobDetail = null;
//	private String m_serviceName = null;
//	private String m_serviceMethod = null;

//	private String m_schedulerServiceName = null;
//	private boolean m_schedulerLog = false;
	//private XProperties m_jobProp = null;

	public XJobDetail(JobDetail detail)
	{
		super();
		m_jobDetail = detail;
	}


	// ====================================================================================================

//	public void setServiceName(String serviceName)
//	{
//		m_serviceName = serviceName;
//	}
//
//	public void setServiceMethod(String serviceMethod)
//	{
//		m_serviceMethod = serviceMethod;
//	}




//	public void setSchedulerServiceName(String serviceName)
//	{
//		m_schedulerServiceName = serviceName;
//	}
//
//	public void setSchedulerLog(boolean schedulerLog)
//	{
//		m_schedulerLog = schedulerLog;
//	}



//	public String getServiceName()
//	{
//		return m_serviceName;
//	}
//
//	public String getServiceMethod()
//	{
//		return TextHelper.evl(m_serviceMethod, "executeBatch");
//	}
//
//	public boolean isSchedulerLog()
//	{
//		return m_schedulerLog;
//	}
//
//	public boolean getSchedulerLog()
//	{
//		return isSchedulerLog();
//	}
//
//	public String getSchedulerServiceName()
//	{
//		return m_schedulerServiceName;
//	}


//	public void setProperties(Properties prop)
//	{
//		if (prop instanceof XProperties)
//		{
//			m_jobProp = (XProperties)prop;
//		}
//		else
//		{
//			m_jobProp = new XProperties();
//			m_jobProp.putAll(prop);
//		}
//	}

//	public Properties getProperties()
//	{
//		return getXProperties();
//	}
//
//	public XProperties getXProperties()
//	{
//		if (m_jobProp == null)
//		{
//			m_jobProp = new XProperties();
//		}
//
//		return m_jobProp;
//	}



	// ====================================================================================================

	@Override
	public String getDescription()
	{
		return null;
	}

	@Override
	public JobBuilder getJobBuilder()
	{
		return m_jobDetail.getJobBuilder();
	}

	@Override
	public Class<? extends Job> getJobClass()
	{
		//return (Class<? extends Job>) this.getClass();
		return m_jobDetail.getJobClass();
	}

	@Override
	public JobDataMap getJobDataMap()
	{
		return m_jobDetail.getJobDataMap();
	}

	@Override
	public JobKey getKey()
	{
		return m_jobDetail.getKey();
	}

	@Override
	public boolean isConcurrentExectionDisallowed()
	{
		return m_jobDetail.isConcurrentExectionDisallowed();
	}

	@Override
	public boolean isDurable()
	{
		return m_jobDetail.isDurable();
	}

	@Override
	public boolean isPersistJobDataAfterExecution()
	{
		return m_jobDetail.isPersistJobDataAfterExecution();
	}

	@Override
	public boolean requestsRecovery()
	{
		return m_jobDetail.requestsRecovery();
	}


	@Override
	public Object clone()
	{
		XJobDetail obj = new XJobDetail((JobDetail)m_jobDetail.clone());
//		obj.setServiceName(m_serviceName);
//		obj.setServiceMethod(m_serviceMethod);
//		obj.setSchedulerLog(m_schedulerLog);
//		obj.setSchedulerServiceName(m_schedulerServiceName);

//		if (m_jobProp != null)
//		{
//			obj.setProperties((XProperties)m_jobProp.clone());
//		}

		return obj;
	}
}
