package com.fliconz.fm.common.core;

import java.util.Map;

import org.springframework.context.ApplicationEvent;

import com.fliconz.fm.common.util.DataMap;

public class KGApplicationEvent extends ApplicationEvent {

	private String m_eventName = null;
	private boolean m_isAsync = true;
	//private boolean m_isNewTransaction = false;

	public KGApplicationEvent(String eventName, Object source, boolean isAsync) {
		super(source);
		this.m_eventName = eventName;
		this.m_isAsync = isAsync;
	}

	public KGApplicationEvent(String eventName, Object source) {
		this(eventName, source, true);
	}

	public String getEventName() {
		return this.m_eventName;
	}

	public boolean isAsync() {
		return this.m_isAsync;
	}


	public DataMap getSourceAsDataMap() {
		return (DataMap)super.getSource();
	}

	public Map getSourceAsMap() {
		return (Map)super.getSource();
	}

}
