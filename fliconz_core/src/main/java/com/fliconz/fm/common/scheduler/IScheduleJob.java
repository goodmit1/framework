package com.fliconz.fm.common.scheduler;

import java.util.Date;

public interface IScheduleJob {

	public int doRun(String scheduleId, Date startDt) throws Exception;
}
