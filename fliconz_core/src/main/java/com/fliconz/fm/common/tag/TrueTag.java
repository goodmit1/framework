/*
 * @(#)TrueTag.java
 */
package com.fliconz.fm.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.fliconz.fm.common.util.TextHelper;


public class TrueTag extends BodyTagSupport
{
	private boolean m_isTrue = false;

	//##############################################################################################################################

	/**
	* Construct a <code>TrueTag</code>.
	*/
	public TrueTag() {
		super();
	}


	//##############################################################################################################################

	/**
	* Release a value attribute.
	*
	* Called on a Tag handler to release state.
	* The page compiler guarantees that JSP page implementation objects will invoke this method on all tag handlers,
	* but there may be multiple invocations on <code>doStartTag</code> and <code>doEndTag</code> in between.
	*/
	public void release() {
		super.release();
		m_isTrue = false;
	}


	/**
	* Set the value attribute <code>boolean</code>.
	*
	* @param value the value attribute.
	*/
	public void setValue(boolean value) {
		m_isTrue = value;
	}

	/**
	* Set the value attribute <code>java.lang.Boolean</code>.
	*
	* @param value the value attribute.
	*/
	public void setValue(Boolean value) {
		m_isTrue = value.booleanValue();
	}

	/**
	* Set the value attribute (<code>java.lang.String</code>).
	*
	* @param value the value attribute.
	*/
	public void setValue(String value) {
		m_isTrue = TextHelper.parseBoolean(value);
	}

	//##############################################################################################################################

	/**
	* If the value attribute is <code>true</code>, return <code>javax.servlet.jsp.tagext.EVAL_BODY_BUFFERED</code>;
	* otherwise return <code>javax.servlet.jsp.tagext.Tag.SKIP_BODY</code>
	*
	* @return <code>javax.servlet.jsp.tagext.EVAL_BODY_BUFFERED</code> or <code>javax.servlet.jsp.tagext.Tag.SKIP_BODY</code>
	*/
	public int doStartTag() {
		return (m_isTrue) ? EVAL_BODY_BUFFERED : SKIP_BODY;
	}

	/**
	* Process the body part of this tag.
	*
	* @return <code>javax.servlet.jsp.tagext.Tag.SKIP_BODY</code>
	*/
	public int doAfterBody() throws JspTagException
	{
		try
		{
			bodyContent.writeOut(bodyContent.getEnclosingWriter());
			return SKIP_BODY;
		}
		catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
	}
}
