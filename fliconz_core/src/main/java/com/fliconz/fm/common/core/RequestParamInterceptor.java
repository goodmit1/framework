package com.fliconz.fm.common.core;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartRequest;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fliconz.fm.common.util.DataKey;
import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.common.util.XProperties;
import com.fliconz.fw.runtime.util.PropertyManager;

public class RequestParamInterceptor extends HandlerInterceptorAdapter implements Filter
{
	/**
	 * SLF4J (Standard Logging Facade for JAVA) logger
	 */
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	protected final static String KEY_PARAM_POPULATED = String.format("%s.populated", RequestParamInterceptor.class.getName());
//	protected final static String KEY_REQUEST_CLASS = String.format("%s.request-class", RequestParamInterceptor.class.getName());
	private final static String KEY_PARAM_LOGGED = String.format("%s.logged", RequestParamInterceptor.class.getName());


	private Set<String> m_debugIgnoreURIList = null;
	private Set<String> m_ignoreParamList = null;

	private XProperties m_prop = null;

	public RequestParamInterceptor() {
		this(new Properties());
	}

	public RequestParamInterceptor(Properties prop) {
		this.m_prop = new XProperties(prop);
		m_debugIgnoreURIList = new HashSet<String>();
		m_ignoreParamList = new HashSet<String>();
		m_ignoreParamList.add("mem_pass");
		m_ignoreParamList.add("PASSWORD");
	}


	static String escapeTag(String value)
	{
		int len = value.length();
		StringWriter writer = new StringWriter();
		for(int i=0; i < len; i++)
		{
			char c = value.charAt(i);
			if(c=='<')
			{
				writer.write("&lt;");
			}
			else if(c=='>')
			{
				writer.write("&gt;");
			}
			else
			{
				writer.write(c);
			}
		}
		return writer.toString();
	}
	static String getParameter(String value)
	{

		if(PropertyManager.getBoolean("request.escape_tag", false))
		{
			value = escapeTag(value);
		}
		return value;
	}
	static String[] getParameterValues(String[] values)
	{

		if(PropertyManager.getBoolean("request.escape_tag", false))
		{
			for(int i=0; i < values.length; i++)
			{
				values[i] = escapeTag(values[i]);
			}
		}
		return values;
	}

	public void doInit()
	{

	}
	public void doDestroy()
	{

	}



	public void setDebugIgnoreURIList(List<String> ignoreList)
	{
		if (ignoreList == null || ignoreList.isEmpty())
		{
			return;
		}
		m_debugIgnoreURIList.clear();
		for (String uri : ignoreList)
		{
			uri = uri.trim();
			if (TextHelper.isEmpty(uri) == false)
			{
				m_debugIgnoreURIList.add(uri);
			}
		}

	}


	// ==============================================================================================
	// org.springframework.web.servlet.handler.HandlerInterceptorAdapter extension

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception
	{
//		if (theLogger.isDebugEnabled()) theLogger.debug("==> preHandle : handler=[{}]", handler);

//		for (Enumeration<String> en = request.getAttributeNames(); en.hasMoreElements();)
//		{
//			String attrName = en.nextElement();
//
//			if (theLogger.isDebugEnabled()) theLogger.debug("==> request-attribute - [{}]=[{}] ===> [{}]", attrName, request.getAttribute(attrName).getClass().getName(), request.getAttribute(attrName));
//
//		}
		return this.doHandle_main(request, response, handler);
	}



	// ==============================================================================================
	// javax.servlet.Filter implementation

	private Set makeSet(String list)
	{
		Set<String> result = new HashSet();
		if (TextHelper.isNotEmpty(list))
		{
			List<String> aDebugIgnoreRequesURIList = TextHelper.splitList(list, "\n");

			for (String uri : aDebugIgnoreRequesURIList)
			{
				uri = uri.trim();
				if (TextHelper.isEmpty(uri) == false)
				{
					result.add(uri);
				}
			}
		}
		return result;
	}
	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
		this.doInit();
		String debugIgnoreURIList = filterConfig.getInitParameter("debugIgnoreURIList");
		if(debugIgnoreURIList != null) m_debugIgnoreURIList = makeSet(debugIgnoreURIList);
		String ignoreParam = filterConfig.getInitParameter("ignoreParamList");
		if(ignoreParam != null) m_ignoreParamList  = makeSet(ignoreParam);
		String size = filterConfig.getInitParameter("paramSize");
		if(size != null)
		{
			try
			{
				paramSize = Integer.parseInt(size);
			}
			catch(Exception ignore)
			{

			}
		}
	}
	int paramSize= 5;
	@Override
	public void destroy()
	{
		this.doDestroy();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
//		if (theLogger.isDebugEnabled()) theLogger.debug("==> doFilter : chain=[{}]", chain);

//		HttpServletRequest aRequest = null;
//		if (this.isMultipartRequest((HttpServletRequest)request))
//		{
//			if (request instanceof MultipartRequest)
//			{
//				aRequest = (HttpServletRequest)request;
//			}
//			else
//			{
//				try
//				{
//					aRequest = new StandardMultipartHttpServletRequest((HttpServletRequest)request);
//					aRequest.setAttribute(RequestParamInterceptor.KEY_REQUEST_CLASS, aRequest.getClass().getName());
//				}
//				catch (Throwable e)
//				{
//					if (theLogger.isWarnEnabled()) theLogger.warn("mutipart read failed");
//					e.printStackTrace();
//					aRequest = (HttpServletRequest)request;
//					aRequest.removeAttribute(RequestParamInterceptor.KEY_REQUEST_CLASS);
//				}
//			}
//		}
//		else
//		{
//			aRequest = (HttpServletRequest)request;
//		}
//
//		boolean result = this.doHandle_main(aRequest, (HttpServletResponse)response, (Object)chain);
		boolean result = this.doHandle_main((HttpServletRequest)request, (HttpServletResponse)response, (Object)chain);
		if (result == false)
		{
			return;
		}

		//chain.doFilter(aRequest, response);
		chain.doFilter(request, response);
	}



	// ==============================================================================================

	private boolean doHandle_main(HttpServletRequest request, HttpServletResponse response, Object what)
	{
//		if (theLogger.isDebugEnabled()) theLogger.debug("==> doHandle_main - what : [{}]=[{}]", what.getClass().getName(), what);
//		if (theLogger.isDebugEnabled()) theLogger.debug("==> request-class : [{}]", request.getClass().getName());

		makeRequestParam(request );
		DataMap<String,Object> reqParam = (DataMap<String,Object>)request.getAttribute(DataKey.KEY_REQUEST_PARAM);

		boolean isPopulated = TextHelper.parseBoolean((String)request.getAttribute(RequestParamInterceptor.KEY_PARAM_POPULATED), false);
		boolean isLogged = TextHelper.parseBoolean((String)request.getAttribute(RequestParamInterceptor.KEY_PARAM_LOGGED), false);
		if (isPopulated && isLogged == false && theLogger.isDebugEnabled())
		{
			request.setAttribute(RequestParamInterceptor.KEY_PARAM_LOGGED, "true");
//			for (Enumeration<String> en = request.getAttributeNames(); en.hasMoreElements();)
//			{
//				String name = en.nextElement();
//				Object value = request.getAttribute(name);
//				theLogger.debug("=== request attribute ===> [{}] = [{}]", name, value);
//			}

			String requestURI = request.getRequestURI();
			if (request.getContextPath().equals("") == false && requestURI.indexOf(request.getContextPath()) == 0)
			{
				requestURI = requestURI.substring(request.getContextPath().length());
			}

			// debug ignore
			if (m_debugIgnoreURIList.contains(requestURI))
			{
				return true;
			}

			theLogger.debug(
				new StringBuilder()
					.append("start==============================================================================================").append("\n")

					.append("sessionId : [{}]"				).append("\n")
					.append("requestURI : [{}]"				).append("\n")
					.append("requestHeader : [{}]"				).append("\n")

					.append("remoteAddr : [{}]"			).append("\n")
					.append("param : [{}]"					).append("\n")
					.append("end==============================================================================================")
					.toString()
				, new Object[]{
						 request.getSession().getId()
						, request.getRequestURI()
						,this.getRequestHeaderMap(request)
						,request.getRemoteAddr()
						, m_prop.getBoolean("isLoggingReqParam", false) ? TextHelper.nvl(getParam(request), "") : "isLoggingReqParam=false"

					}
			);
		}

		return true;
	}

	private String getParam(HttpServletRequest request)
	{
		if(!TextHelper.isEmpty(request.getQueryString())) return request.getQueryString();
		Enumeration<String> names = request.getParameterNames();
		StringBuffer sb = new StringBuffer();
		int idx=0;
		while(names.hasMoreElements())
		{
			String name = names.nextElement();
			if(m_ignoreParamList != null && m_ignoreParamList.contains(name)) continue;
			if(idx>0) sb.append("&");
			sb.append(name).append("=").append(request.getParameter(name));
			idx++;
			if(idx==paramSize) break;
		}
		return sb.toString();
	}






	// ==============================================================================================

	public static boolean isMultipartRequest(HttpServletRequest request)
	{
		return TextHelper.nvl(request.getContentType(), "").startsWith("multipart/form-data");
	}

	public static void makeRequestParam(HttpServletRequest request)
	{
		DataMap<String,Object> reqParam = (DataMap<String,Object>)request.getAttribute(DataKey.KEY_REQUEST_PARAM);
		if (reqParam == null)
		{
			reqParam = new DataMap<String,Object>();
			request.setAttribute(DataKey.KEY_REQUEST_PARAM, reqParam);
		}

		boolean isPramPopulated = TextHelper.parseBoolean((String)request.getAttribute(RequestParamInterceptor.KEY_PARAM_POPULATED), false);
		if (isPramPopulated)
		{
			return;
		}

		if (isMultipartRequest(request) && (request instanceof MultipartRequest) == false)
		{
			//if (theLogger.isWarnEnabled()) theLogger.warn("mutipart read canceled");
			return;
		}

		for (Enumeration<String> en = request.getParameterNames(); en.hasMoreElements();)
		{
			String key = en.nextElement();
			String[] arrValue = request.getParameterValues(key);
			String value = null;

			if (arrValue != null && arrValue.length > 1)
			{
				//System.out.println(String.format("======> param-aaa: [%s]=[%s]", key, arrValue));
				reqParam.put(key,  getParameterValues(arrValue));
			}
			else
			{
				value = request.getParameter(key);
				reqParam.put(key, getParameter(value));
				//System.out.println(String.format("======> param-bbb: [%s]=[%s]", key, value));
			}
		}

//		if (isMultipartRequest(request) && request instanceof MultipartRequest)
//		{
//			Map<String,org.springframework.web.multipart.MultipartFile> mm = ((MultipartRequest)request).getFileMap();
//			for (String name : mm.keySet())
//			{
//				//System.out.println(String.format("======> file-param-aaa: [%s]=[%s]", name, mm.get(name)));
//			}
//		}

		request.setAttribute(RequestParamInterceptor.KEY_PARAM_POPULATED, "true");
	}



	private Map<String,String> getRequestHeaderMap(HttpServletRequest request)
	{
		Map<String,String> mm = new HashMap<String,String>();
		for (Enumeration<String> en = request.getHeaderNames(); en.hasMoreElements();)
		{
			String name = en.nextElement();
			mm.put(name, request.getHeader(name));
		}

		return mm;
	}


}
