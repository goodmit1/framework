package com.fliconz.fm.common.util;

import java.text.ParseException;

import com.ibm.icu.util.ChineseCalendar;

public class LunarHelper
{
	/**
	 * 양력일자를 음력일자로 변환한다.
	 *
	 * @param solarDate 양력일자 (yyyyMMdd)
	 * @return
	 * @throws ParseException
	 */
	public static String toLunarDate(String solarDate) throws ParseException
	{
		java.util.Date dt = TimeHelper.parseDate(solarDate, "yyyyMMdd");
		return LunarHelper.toLunarDate(dt);
	}



	/**
	 * 양력일자를 음력일자로 변환한다.
	 *
	 * @param dt
	 * @return
	 */
	public static String toLunarDate(java.util.Date dt)
	{
		if (dt == null)
		{
			return null;
		}

		ChineseCalendar aChineseCalendar = new ChineseCalendar();
		aChineseCalendar.setTimeInMillis(dt.getTime());

		String lunarDate = String.format("%s%s%s"
				, aChineseCalendar.get(ChineseCalendar.EXTENDED_YEAR)-2637
				, TextHelper.lpad(aChineseCalendar.get(ChineseCalendar.MONTH)+1, 2, '0')
				, TextHelper.lpad(aChineseCalendar.get(ChineseCalendar.DAY_OF_MONTH), 2, '0'));

		return lunarDate;
	}




	/**
	 * 음력일자를 양력일자로 변환한다.
	 *
	 * @param lunarDate 음렬일자 (yyyyMMdd)
	 * @return
	 */
	public static String toSolarDate(String lunarDate)
	{
		if (TextHelper.isEmpty(lunarDate))
		{
			return null;
		}

		lunarDate = lunarDate.trim();
		if (lunarDate.length() != 8)
		{
			throw new IllegalArgumentException("lunarDate lenght must be 8.");
		}

		ChineseCalendar aChineseCalendar = new ChineseCalendar();
		aChineseCalendar.set(ChineseCalendar.EXTENDED_YEAR, TextHelper.parseInt(lunarDate.substring(0,4))+2637);
		aChineseCalendar.set(ChineseCalendar.MONTH, TextHelper.parseInt(lunarDate.substring(4,6))-1);
		aChineseCalendar.set(ChineseCalendar.DAY_OF_MONTH, TextHelper.parseInt(lunarDate.substring(6,8)));

		java.util.Calendar aCalendar = java.util.Calendar.getInstance();
		aCalendar.setTimeInMillis(aChineseCalendar.getTimeInMillis());

		String solarDate = String.format("%s%s%s"
				, aCalendar.get(java.util.Calendar.YEAR)
				, TextHelper.lpad(aCalendar.get(java.util.Calendar.MONTH)+1, 2, '0')
				, TextHelper.lpad(aCalendar.get(java.util.Calendar.DAY_OF_MONTH), 2, '0'));

		return solarDate;
	}
}
