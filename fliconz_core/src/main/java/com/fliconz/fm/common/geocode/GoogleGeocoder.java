package com.fliconz.fm.common.geocode;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;
//import net.sf.json.JSONObject;
//import net.sf.json.JSONSerializer;
import java.util.LinkedList;
import java.util.List;

//http://code.google.com/intl/ko-KR/apis/maps/documentation/geocoding/

//http://code.google.com/apis/maps/documentation/geocoding/

//Supported Languages Spreadsheet.
//http://spreadsheets.google.com/pub?key=p9pdwsai2hDMsLkXsoM05KQ&gid=1

//http://maps.googleapis.com/maps/api/geocode/json?address=복정동&sensor=true


public class GoogleGeocoder extends Geocoder
{
	public GoogleGeocoder()
	{
		super();
	}


	@Override
	public boolean canPaging(boolean isGeocode)
	{
		return false;
	}

	@Override
	public DataMap<String, Object> geocode(GeoLocation addr) throws GeocodeException
	{
		// 주소 --> 좌표
		HttpClient aHttpClient = null;

		try
		{
			DataMap<String,Object> locAttr = addr.getAttributes();
//			int pageNo = locAttr.getInt(KEY_REQUEST_PAGE, 1);
//			int rowCount = locAttr.getInt(KEY_REQUEST_COUNT, 10);
			String encoding = locAttr.getString(KEY_ENCODING,"UTF-8");


			StringBuilder sb = new StringBuilder()
				.append("http://maps.googleapis.com/maps/api/geocode/json")
				.append("?address=").append(URLEncoder.encode(TextHelper.nvl(addr.getAddress(), ""), encoding))
				.append("&language=").append( locAttr.getString("language", "ko") )
				.append("&sensor=").append( locAttr.getBoolean("sensor", false) );
			if (locAttr.containsKey("bounds"))
			{
				sb.append("&bounds=").append( locAttr.getString("bounds") );
			}

			aHttpClient = HttpClientBuilder.create().build();;
			HttpGet httpGet = new HttpGet(sb.toString());

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);

			org.json.JSONObject response = new org.json.JSONObject( responseString );
			String responseCode = response == null ? "failed" : TextHelper.nvl(response.getString("status"), "");

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (responseCode.equals("OK") == false)
			{
				resultData.setString(KEY_RESPONSE_CODE, responseCode);
				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, 0);
				resultData.setInt(KEY_TOTAL_COUNT, 0);
				return resultData;
			}


			List<GeoLocation> aList = new LinkedList<GeoLocation>();
			org.json.JSONArray arr = response.getJSONArray("results");
			for (int i=0; i<arr.length(); i++)
			{
				org.json.JSONObject item = arr.getJSONObject(i);
				GeoLocation aLocation = new GeoLocation();
				aLocation.setAddress(item.getString("formatted_address"));

				org.json.JSONObject geoLocation = item.getJSONObject("geometry").getJSONObject("location");
				aLocation.setLatitude((float)geoLocation.getDouble("lat"));
				aLocation.setLongitude((float)geoLocation.getDouble("lng"));
				aList.add(aLocation);
			}
//            for (Iterator<JSONObject> it = response.getJSONArray("results").iterator(); it.hasNext();)
//            {
//            	JSONObject item = it.next();
//            	GeoLocation aLocation = new GeoLocation();
//            	aLocation.setAddress(item.getString("formatted_address"));
//
//            	JSONObject geoLocation = item.getJSONObject("geometry").getJSONObject("location");
//            	aLocation.setLatitude((float)geoLocation.getDouble("lat"));
//            	aLocation.setLongitude((float)geoLocation.getDouble("lng"));
//            	aList.add(aLocation);
//            }

			resultData.setString(KEY_RESPONSE_CODE, "ok");
			resultData.setString(KEY_RESPONSE_MESSAGE, "success");
			resultData.setObject(KEY_LOCATION_LIST, aList);

			int count = aList.size();
			resultData.setInt(KEY_REQUEST_PAGE, 1);
			resultData.setInt(KEY_RESPONSE_COUNT, count);
			resultData.setInt(KEY_TOTAL_COUNT, count);

			return resultData;
		}
		catch (UnsupportedEncodingException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}

	@Override
	public DataMap<String, Object> reverseGeocode(GeoLocation coord) throws GeocodeException
	{
		// 좌표 --> 주소
		HttpClient aHttpClient = null;

		try
		{
			DataMap<String,Object> locAttr = coord.getAttributes();
			StringBuilder sb = new StringBuilder()
				.append("http://maps.googleapis.com/maps/api/geocode/json")
				.append("?latlng=").append(coord.getLatitude()).append(",").append(coord.getLongitude())
				.append("&language=").append( locAttr.getString("language", "ko") )
				.append("&sensor=").append( locAttr.getBoolean("sensor", false) );

			aHttpClient = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(sb.toString());

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);

			org.json.JSONObject response = new org.json.JSONObject( responseString );
			String responseCode = response == null ? "failed" : TextHelper.nvl(response.getString("status"), "");

			DataMap<String,Object> resultData = new DataMap<String,Object>();
			if (responseCode.equals("OK") == false)
			{
				resultData.setString(KEY_RESPONSE_CODE, responseCode);
				resultData.setInt(KEY_REQUEST_PAGE, 1);
				resultData.setInt(KEY_RESPONSE_COUNT, 0);
				resultData.setInt(KEY_TOTAL_COUNT, 0);
				return resultData;
			}


			List<GeoLocation> aList = new LinkedList<GeoLocation>();
			org.json.JSONArray arr = response.getJSONArray("results");
			for (int i=0; i<arr.length(); i++)
			{
				org.json.JSONObject item = arr.getJSONObject(i);
				GeoLocation aLocation = new GeoLocation();
				aLocation.setAddress(item.getString("formatted_address"));

				org.json.JSONObject geoLocation = item.getJSONObject("geometry").getJSONObject("location");
				aLocation.setLatitude((float)geoLocation.getDouble("lat"));
				aLocation.setLongitude((float)geoLocation.getDouble("lng"));
				aList.add(aLocation);
			}
//            for (Iterator<JSONObject> it = response.getJSONArray("results").iterator(); it.hasNext();)
//            {
//            	JSONObject item = it.next();
//            	GeoLocation aLocation = new GeoLocation();
//            	aLocation.setAddress(item.getString("formatted_address"));
//
//            	JSONObject geoLocation = item.getJSONObject("geometry").getJSONObject("location");
//            	aLocation.setLatitude((float)geoLocation.getDouble("lat"));
//            	aLocation.setLongitude((float)geoLocation.getDouble("lng"));
//            	aList.add(aLocation);
//            }

			resultData.setString(KEY_RESPONSE_CODE, "ok");
			resultData.setString(KEY_RESPONSE_MESSAGE, "success");
			resultData.setObject(KEY_LOCATION_LIST, aList);

			int count = aList.size();
			resultData.setInt(KEY_REQUEST_PAGE, 1);
			resultData.setInt(KEY_RESPONSE_COUNT, count);
			resultData.setInt(KEY_TOTAL_COUNT, count);

			return resultData;
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (org.json.JSONException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}




// olleh test key
// test app id : 8100CDC3
// test key : T77ECFFEA1BCB5C

/*
WEB_INF_HOME=/Volumes/data/projects/SmartJM/development/web/smartjm/WebContent/WEB-INF
THIS_CLASSPATH=$WEB_INF_HOME/classes:$WEB_INF_HOME/lib/httpclient-4.0.3.jar:$WEB_INF_HOME/lib/httpcore-4.0.1.jar:$WEB_INF_HOME/lib/commons-logging-1.1.1.jar:$WEB_INF_HOME/lib/json-lib-2.3-jdk15.jar:$WEB_INF_HOME/lib/commons-lang-2.4.jar:$WEB_INF_HOME/lib/ezmorph-1.0.6.jar:$WEB_INF_HOME/lib/commons-collections-3.2.1.jar:$WEB_INF_HOME/lib/commons-beanutils-1.8.0.jar

java -cp $THIS_CLASSPATH com.fliconz.fm.common.geocode.GoogleGeocoder
*/
	public static void main(String[] args) throws Exception
	{
		GoogleGeocoder geocoder = new GoogleGeocoder();
		GeoLocation aLocation = new GeoLocation();
		DataMap<String,Object> resultData = null;


		System.out.println("=========================");
		System.out.println(geocoder.getClass().getName());

		// http://maps.googleapis.com/maps/api/geocode/json?address=효자동&language=ko&sensor=false
		aLocation.setAddress("서울 영등포구 신길동");
		resultData = geocoder.geocode(aLocation);
		System.out.println("=========================");
		System.out.println("geocode : "+ resultData.toString());




		// http://maps.googleapis.com/maps/api/geocode/json?latlng=37.87015110,127.73687180&language=ko&sensor=false
		aLocation.setLatitude(37.87015110F);
		aLocation.setLongitude(127.73687180F);
		resultData = geocoder.reverseGeocode(aLocation);

		System.out.println();
		System.out.println("=========================");
		System.out.println("reverse geocode : "+ resultData.toString());
	}

}
