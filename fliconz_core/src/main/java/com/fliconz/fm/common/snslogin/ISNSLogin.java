package com.fliconz.fm.common.snslogin;

import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.fliconz.fm.util.RequestHelper;
import com.fliconz.fw.runtime.util.PropertyManager;

public abstract class  ISNSLogin {
	HttpSession session;
	String clientId;
	String clientSecret;
	String adminKey;
	String redirectUrl;
	
	String access_token;
	String refresh_token;
	
	public ISNSLogin(HttpSession session, String clientId, String clientSecret, String redirectUrl) throws Exception
	{
		this(session, clientId, clientSecret, null, redirectUrl);

	}
	public ISNSLogin(HttpSession session, String clientId, String clientSecret, String adminKey, String redirectUrl) throws Exception
	{
		this.session = session;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.adminKey = adminKey;
	   	this.redirectUrl = URLEncoder.encode(redirectUrl,"utf-8");

	}
	public static ISNSLogin getInstance(HttpServletRequest request, String type, HttpSession session) throws Exception
	{
		String clientId = PropertyManager.getString("sns."+type+".clientId.rest", PropertyManager.getString("sns."+type+".clientId"));
		if(request.getParameter("testClientId") != null){
			clientId = request.getParameter("testClientId");
		}
		String clientSecret = PropertyManager.getString("sns."+type+".clientSecret");
		if(request.getParameter("testClientSecret") != null){
			clientSecret = request.getParameter("testClientSecret");
		}
		return ISNSLogin.getInstance(type, session, clientId, clientSecret, PropertyManager.getString("sns."+type+".redirectURL"));
	}
	
	public static ISNSLogin getInstance(String type, HttpSession session) throws Exception
	{
		return ISNSLogin.getInstance(type, session, PropertyManager.getString("sns."+type+".clientId.rest", PropertyManager.getString("sns."+type+".clientId")), PropertyManager.getString("sns."+type+".clientSecret"), PropertyManager.getString("sns."+type+".redirectURL"));
	}

	public static ISNSLogin getInstance(String snsId, HttpSession session, String clientId, String clientSecret, String redirectUrl) throws Exception
	{
		return ISNSLogin.getInstance(snsId, session, clientId, clientSecret, null, redirectUrl);
	}
	
	public static ISNSLogin getInstance(String snsId, HttpSession session, String clientId, String clientSecret, String adminKey, String redirectUrl) throws Exception
	{
		return (ISNSLogin)Class.forName("com.fliconz.fm.common.snslogin." + snsId + "Login").getConstructor(HttpSession.class, String.class, String.class, String.class, String.class).newInstance(session, clientId, clientSecret, adminKey, redirectUrl);
	}
	
	public String getFirstRequestUrl() throws Exception {

		 String state = this.getState();
		 session.setAttribute(clientId + "state",state);

	   	 return this.getFirstRequestUrl(state);

	}

	public String getDynamicAgreeRequestUrl(String state) throws Exception {

		 if(state == null) {
			 state = this.getState();
			 session.setAttribute(clientId + "state",state);
		 }
		 String scope = this.getScope();
	   	 return this.getDynamicAgreeRequestUrl(state, scope);

	}

	String getState()
	{
		 SecureRandom random = new SecureRandom();
	     return new BigInteger(130, random).toString(32);
	}
	public SNSUser getUserProfileByAccessToken(String sns_type, String access_token)
	{
		Map header = new HashMap();
		header.put("Authorization", "Bearer " + access_token);
		JSONObject json;
		try {
			json = RequestHelper.getJSON(this.getProfileUrl(access_token), header);
			if(json != null) {
				json.put("access_token", access_token);
				SNSUser user = this.getSNSUser(json);
				if(sns_type != null) user.setType(sns_type);
				return user;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return null;
	}
	public SNSUser getUserProfileByAccessToken(String access_token)
	{
		return getUserProfileByAccessToken(null, access_token);
	}
	public SNSUser getUserProfile(String state, String code)
	{
		if(state.equals(session.getAttribute(clientId + "state")))
		{

			String url = this.getAccessTokenUrl(state, code);
			JSONObject result;
			try {
				result = RequestHelper.getJSON(url, null);
				if(result != null)
				{
					String access_token = result.getString("access_token");
					if(access_token != null)
					{
						return getUserProfileByAccessToken(access_token);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;
	}
	
	protected void getTokenInfo(String code){
		String state = this.getState();
		session.setAttribute(clientId + "state",state);
		String url = this.getAccessTokenUrl(state, code);
		JSONObject result;
		try {
			result = RequestHelper.getJSON(url, null);
			if(result != null)
			{
				if(result.has("error") == false){
					access_token = result.getString("access_token");
					refresh_token = result.getString("refresh_token");
				} else {
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected abstract String getAccessTokeInfoUrl();
	
	protected abstract String getRefreshTokeUrl();
	
	public abstract JSONObject refreshToken(String access_token, String refresh_token) throws Exception;

	public String getAccessToken(String code)
	{
		if(access_token == null) getTokenInfo(code);
		return access_token;
	}
	

	public String getRefreshToken(String code)
	{
		if(refresh_token == null) getTokenInfo(code);
		return refresh_token;
	}

	protected SNSUser getSNSUser(JSONObject json)
	{
		SNSUser user = new SNSUser();
		user.id=json.getString("id");
		user.email = json.getString("email");
		user.name = json.getString("name");
		user.accessToken = json.getString("access_token");
		return user;
	}
	protected abstract String getAccessTokenUrl(String state, String code) ;
	protected abstract String getProfileUrl(String access_token) ;
	protected abstract String getFirstRequestUrl(String state);
	protected abstract String getDynamicAgreeRequestUrl(String state, String scope);
	protected abstract String getScope();
	protected abstract String getUnlinkUrl();
	
	public boolean unlink(String sns_id, String access_token) throws Exception {
		return false;
	}
}
