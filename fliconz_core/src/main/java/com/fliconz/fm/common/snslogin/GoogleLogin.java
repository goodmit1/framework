package com.fliconz.fm.common.snslogin;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;

public class GoogleLogin extends ISNSLogin{

	public GoogleLogin(HttpSession session, String clientId, String clientSecret, String redirectUrl) throws Exception {
		super(session, clientId, clientSecret, redirectUrl);
	}
	
	public GoogleLogin(HttpSession session, String clientId, String clientSecret, String adminKey, String redirectUrl) throws Exception {
		super(session, clientId, clientSecret, adminKey, redirectUrl);
	}

	@Override
	protected String getFirstRequestUrl(String state) {
		String url = "https://accounts.google.com/o/oauth2/auth?scpoe=https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email" +
				"&response_type=code&client_id=" + clientId + "&redirect_uri=" + this.redirectUrl + "&state=" + state;
	   	return url;
	}

	protected String getAccessTokenUrl(String state, String code ) {
		return "https://www.googleapis.com/oauth2/v4/token?grant_type=authorization_code&client_id="+ clientId + "&client_secret=" + clientSecret+ "&redirect_uri=" + this.redirectUrl + "&state=" + state+ "&code=" + code;
	}

	protected  String getProfileUrl(String access_token) {
		return "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=" + access_token;
	}


	@Override
	protected SNSUser getSNSUser(JSONObject json) {
		System.out.println(json);
		SNSUser user = new SNSUser();
		user.id = json.getString("user_id");
		user.email = json.getString("email");
		return user;
	}

	protected String getDynamicAgreeRequestUrl(String state, String scope) {
		return null;
	}

	public String getScope(){
		return "profile,account_email";
	}

	@Override
	protected String getUnlinkUrl() {
		return null;
	}
	
	@Override
	protected String getAccessTokeInfoUrl() {
		return null;
	}
	
	@Override
	protected String getRefreshTokeUrl() {
		return null;
	}
	
	public JSONObject refreshToken(String access_token, String refresh_token) throws Exception {
		return null;
	}
}
