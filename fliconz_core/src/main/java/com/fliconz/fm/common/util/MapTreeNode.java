package com.fliconz.fm.common.util;

import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;

public class MapTreeNode extends DefaultMutableTreeNode{
	public MapTreeNode(Map object)
	{
		super(object);
	}
	public Map getMap()
	{
		return (Map)super.getUserObject();
	}
	public MapTreeNode getParent()
	{
		return (MapTreeNode)super.getParent();
	}
}
