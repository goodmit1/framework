package com.fliconz.fm.common;

public class Constants {
	// Backing bean keys
	public static final String LOGIN_USER_KEY = "loginUser";
	public static final String LOGIN_USER_KEY_SCOPE = "sessionScope.";

	// Model object keys
	public static final String PROJECT_COORDINATOR_SCOPE = "applicationScope.";
	public static final String PROJECT_COORDINATOR_KEY = "projectCoordinator";

	public static final String STATUS_COORDINATOR_SCOPE = "applicationScope.";
	public static final String STATUS_COORDINATOR_KEY = "statusCoordinator";

	public static final String USER_COORDINATOR_SCOPE = "applicationScope.";
	public static final String USER_COORDINATOR_KEY = "userCoordinator";

	// Authorization
	public static final String ORIGINAL_VIEW_SCOPE = "sessionScope";
	public static final String ORIGINAL_VIEW_KEY = "originalTreeId";
	public static final String PROTECTED_DIR = "protected";
	public static final String EDIT_DIR = "protected/edit";
	public static final String LOGIN_VIEW = "/login/login.html";

	// Action outcomes
	public static final String SUCCESS_READONLY_OUTCOME = "success_readonly";
	public static final String SUCCESS_READWRITE_OUTCOME = "success";
	public static final String SUCCESS_OUTCOME = "success";

	public static final String CANCEL_READONLY_OUTCOME = "cancel_readonly";
	public static final String CANCEL_READWRITE_OUTCOME = "cancel_readwrite";
	public static final String CANCEL_OUTCOME = "cancel";

	public static final String FAILURE_OUTCOME = "login";
	public static final String ERROR_OUTCOME = "login";

	// Resource bundle keys
	public static final String BUNDLE_BASENAME = "messages";

	// For JDBC ResultSet example only
	public static final String RESULT_SET_PROJECT_COORDINATOR_SCOPE = "applicationScope.";
	public static final String RESULT_SET_PROJECT_COORDINATOR_KEY = "resultSetProjectCoordinator";

	public static final String AUTHORITY_METHOD = "AUTHORITY_METHOD";

	public static final String PDF_PATH_PREFIX = "C:/FM/was/apache-tomcat-8.0.18/logs/";

	//Cache Query Keys
	public static final String MAPPER_NS = "com.fliconz.fm.Base";

	public static final String FIRST_SUB_MENU_LIST_KEY = "firstSubMenuList";

	public static final String MENU_ALL_KEY = "allMenuList";

	public static final String ALL_LEAF_MENU_KEY = "allLeafMenuList";
	public static final String SUB_MENU_KEY = "subMenuList";

	public static final String ROOT_MENU_KEY = "getRootMenu";
	public static final String CHILDEREN_MENU_KEY = "getChildrenMenu";
	public static final String LIST_NL_KEY = "list_FM_NL";




}
