package com.fliconz.fm.common.rest;

public class KGRequestException extends Exception
{
	public KGRequestException()
	{
		super();
	}

	public KGRequestException(String msg)
	{
		super(msg);
	}

	public KGRequestException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

	public KGRequestException(Throwable cause)
	{
		super(cause);
	}
}
