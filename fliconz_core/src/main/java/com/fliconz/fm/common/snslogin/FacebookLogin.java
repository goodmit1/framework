package com.fliconz.fm.common.snslogin;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;

public class FacebookLogin extends ISNSLogin{

	public FacebookLogin(HttpSession session, String clientId, String clientSecret, String redirectUrl) throws Exception {
		super(session, clientId, clientSecret, redirectUrl);
	}
	
	public FacebookLogin(HttpSession session, String clientId, String clientSecret, String adminKey, String redirectUrl) throws Exception {
		super(session, clientId, clientSecret, adminKey, redirectUrl);
	}
	
	@Override
	protected String getFirstRequestUrl(String state) {

	   	 return "https://www.facebook.com/v2.10/dialog/oauth?response_type=code&client_id=" + clientId + "&redirect_uri=" + this.redirectUrl + "&state=" + state;

	}
	protected String getAccessTokenUrl(String state, String code )
	{
		return "https://graph.facebook.com/v2.10/oauth/access_token?client_id="+ clientId + "&client_secret=" + clientSecret+ "&redirect_uri=" + this.redirectUrl + "&state=" + state+ "&code=" + code;
	}
	protected  String getProfileUrl(String access_token) {
		return "https://graph.facebook.com/v2.10/me?fields=email,name&access_token=" + access_token;
	}

	protected String getDynamicAgreeRequestUrl(String state, String scope) {
		return null;
	}

	public String getScope(){
		return "profile,account_email";
	}

	@Override
	protected String getUnlinkUrl() {
		return null;
	}
	
	@Override
	protected String getAccessTokeInfoUrl() {
		return null;
	}
	
	@Override
	protected String getRefreshTokeUrl() {
		return null;
	}
	
	public JSONObject refreshToken(String access_token, String refresh_token) throws Exception {
		return null;
	}
}
