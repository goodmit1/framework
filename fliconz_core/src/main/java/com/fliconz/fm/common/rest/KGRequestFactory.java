package com.fliconz.fm.common.rest;

import java.util.Properties;

import com.fliconz.fm.common.util.TextHelper;

public class KGRequestFactory
{
	private Properties m_prop = null;
	public KGRequestFactory()
	{
		super();
		m_prop = new Properties();
	}

	public void setProperties(Properties prop)
	{
		if (prop != null)
		{
			m_prop.putAll(prop);
		}
	}

	public Properties getProperties()
	{
		return m_prop;
	}



	public KGRequest createRequest(Properties prop)
	{
		KGRequest aRequest = new KGRequest();
		aRequest.setSSL(prop.getProperty("ssl", "false").equals("true"));
		aRequest.setHost(prop.getProperty("host", "localhost"));
		aRequest.setPort(TextHelper.parseInt(prop.getProperty("port", "8080"), 8080));
		aRequest.setPath(prop.getProperty("path", "/gateway/reqeust.do"));
		aRequest.setEncoding(prop.getProperty("encoding", "UTF-8"));

		return aRequest;
	}

	public KGRequest createRequest()
	{
		return this.createRequest(m_prop);
	}

	public KGRequest createRequest(boolean isSSL, String host, int port, String path, String encoding)
	{
		KGRequest aRequest = new KGRequest();
		aRequest.setSSL(isSSL);
		aRequest.setHost(host);
		aRequest.setPort(port);
		aRequest.setPath(path);
		aRequest.setEncoding(encoding);

		return aRequest;
	}

	public KGRequest createRequest(boolean isSSL, String host, int port, String path)
	{
		return this.createRequest(isSSL, host, port, path, "UTF-8");
	}

	public KGRequest createRequest(String host, int port, String path)
	{
		return this.createRequest(false, host, port, path, "UTF-8");
	}
	public KGRequest createRequest(String host, int port, String path, String encoding)
	{
		return this.createRequest(false, host, port, path, encoding);
	}
}
