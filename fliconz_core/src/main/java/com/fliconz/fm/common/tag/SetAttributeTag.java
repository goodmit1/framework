/*
 * @(#)SetAttributeTag.java
 */
package com.fliconz.fm.common.tag;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.fliconz.fm.common.util.TextHelper;


public class SetAttributeTag extends BodyTagSupport
{
	private String m_strName = null;
	private Object m_oValue = null;
	private String m_strScopeName = null;

	//##############################################################################################################################

	/**
	* Construct a <code>SetAttributeTag</code>.
	*/
	public SetAttributeTag() {
		super();
	}


	//##############################################################################################################################

	/**
	* Release a value attribute.
	*
	* Called on a Tag handler to release state.
	* The page compiler guarantees that JSP page implementation objects will invoke this method on all tag handlers,
	* but there may be multiple invocations on <code>doStartTag</code> and <code>doEndTag</code> in between.
	*/
	public void release() {
		super.release();

		super.setId(null);
		m_strName = null;
		m_oValue = null;
		m_strScopeName = null;
	}


	public void setName(String name) {
		m_strName = TextHelper.evl( TextHelper.trim(name), null );
	}

	public void setScope(String scopeName) {
		m_strScopeName = TextHelper.evl( TextHelper.trim(scopeName), null );
	}

	public void setValue(Object value) {
		m_oValue = value;
	}

	public void setValue(int value) {
		m_oValue = new Integer(value);
	}

	public void setValue(boolean value) {
		m_oValue = new Boolean(value);
	}

	public void setValue(long value) {
		m_oValue = new Long(value);
	}

	public void setValue(float value) {
		m_oValue = new Float(value);
	}

	public void setValue(double value) {
		m_oValue = new Double(value);
	}

	//##############################################################################################################################

	public int doStartTag() throws JspTagException
	{
		if (m_strScopeName == null) {
			m_strScopeName = "request";
		}

		if (m_strName == null) {
			throw new JspTagException("name must be not null");
		}

		if ( m_strScopeName.equals("request") ) {
			if (m_oValue == null) {
				pageContext.removeAttribute(m_strName, PageContext.REQUEST_SCOPE);
			}
			else
			{
				pageContext.setAttribute(m_strName, m_oValue,PageContext.REQUEST_SCOPE);
			}
		}

		else if ( m_strScopeName.equals("session") ) {
			if (m_oValue == null) {
				pageContext.removeAttribute(m_strName, PageContext.SESSION_SCOPE);
			}

			else
			{
				pageContext.setAttribute(m_strName, m_oValue,PageContext.SESSION_SCOPE);
			}
		}

		else if ( m_strScopeName.equals("page") ) {
			if (m_oValue == null) {
				pageContext.removeAttribute(m_strName, PageContext.PAGE_SCOPE);
			}

			else
			{
				pageContext.setAttribute(m_strName, m_oValue,PageContext.PAGE_SCOPE);
			}
		}

		else if ( m_strScopeName.equals("application") ) {
			if (m_oValue == null) {
				pageContext.removeAttribute(m_strName, PageContext.APPLICATION_SCOPE);
			}

			else
			{
				pageContext.setAttribute(m_strName, m_oValue,PageContext.APPLICATION_SCOPE);
			}
		}

		else
		{
			throw new JspTagException(
				new StringBuilder()
					.append("unsupported scope [")
					.append( m_strScopeName )
					.append("].")
					.toString()
			);
		}

		return SKIP_BODY;	//EVAL_BODY_BUFFERED;
	}

//	public int doAfterBody() throws JspTagException
//	{
//		try
//		{
//			bodyContent.writeOut(bodyContent.getEnclosingWriter());
//
//			return SKIP_BODY;
//		}
//		catch (IOException e)
//		{
//			//Debug.trace(e);
//			throw new JspTagException(e.getMessage());
//		}
//	}
}
