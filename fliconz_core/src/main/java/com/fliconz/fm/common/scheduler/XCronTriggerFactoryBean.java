package com.fliconz.fm.common.scheduler;

import org.quartz.CronTrigger;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;

public class XCronTriggerFactoryBean extends CronTriggerFactoryBean
{
	private XCronTrigger m_cronTrigger = null;
	private String m_targetName = null;

	public XCronTriggerFactoryBean()
	{
		super();
	}


	public void setTargets(String targetName)
	{
		m_targetName = targetName;
		if (m_targetName == null)
		{
			return;
		}
		m_targetName = m_targetName.trim();
	}


	@Override
	public CronTrigger getObject()
	{
		if (m_cronTrigger == null)
		{
			m_cronTrigger = new XCronTrigger((CronTriggerImpl)super.getObject());
			m_cronTrigger.setTargets(m_targetName);
		}

		return m_cronTrigger;
	}



	@Override
	public Class<?> getObjectType()
	{
		return XCronTrigger.class;
	}
}
