package com.fliconz.fm.common.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.security.FMSecurityContextHelper;

/**
 * @author Seongwook
 * String: Key
 * Object: value
 * Elements: children
 */
public class TreeNodeMap extends DataMap<String, Object> implements Cloneable{

	Map nlNames = null;
	Map nlDescs = null;
	private static final long serialVersionUID = 1L;

	private List<TreeNodeMap> elements;

	public TreeNodeMap() {
		this.elements = new ArrayList<TreeNodeMap>();
		nlNames = new HashMap();
		nlDescs = new HashMap();
	}

	public void addNLNames(String locale, String nm)
	{
		nlNames.put(locale, nm);
	}
	public void addNLDescs(String locale, String desc)
	{
		nlDescs.put(locale, desc);
	}
	public int getMenuId() {
		return this.getInt("ID");
	}

	public String getMenuNm() {
		return this.getString("MENU_NM");
	}
	
	@Deprecated
	public String getMenuNmEn() {
		return this.getString("MENU_NM_EN");
	}

	public String getMenuNm(String locale) {
		if( locale==null || locale.equals("ko") ||"".equals(locale)) 
		{
			return getMenuNm();
		}
		return (String)nlNames.get(locale);
	}
	public String getMenuDesc(String locale) {
		if( locale==null || locale.equals("ko") ||"".equals(locale)) 
		{
			return this.getString("MENU_DESC");
		}
		return (String)nlDescs.get(locale);
	}
	
	 

	public String getTheme() {
		return this.getString("THEME");
	}

	public int getParentId() {
		return this.getInt("PARENT_ID");
	}

	public int getMenuOrder() {
		return this.getInt("MENU_ORDER");
	}

	public String getIcon() {
		return this.getString("ICON");
	}

	public boolean isRendered() {
		return "Y".equals(this.getString("RENDERED"));
	}

	public boolean isPopup() {
		return "Y".equals(this.getString("IS_POPUP"));
	}

	public int getPrgmId() {
		return this.getInt("PRGM_ID");
	}

	public String getPrgmPath() {
		return this.getString("PRGM_PATH");
	}

	public String getPageId() {
		return this.getString("PAGE_ID");
	}

	public boolean isEnabled() {
		return "Y".equals(this.getString("ENABLED"));
	}

	public String getPkgNm() {
		return this.getString("PKG_NM");
	}

	public boolean isMobile() {
		return "Y".equals(this.getString("MOBILE_YN"));
	}

	public boolean isCommon() {
		return "Y".equals(this.getString("COMMON_YN"));
	}

	public String getManualPath() {
		return this.getString("MANUAL_PATH");
	}

	public boolean isInternalOnly() {
		return "Y".equals(this.getString("INTERNAL_ONLY"));
	}

	public String getStyleClass(){
		return this.getString("StyleClass");
	}

	public void setElements(List<TreeNodeMap> elements) {
		this.elements = elements;
	}
	public void add(TreeNodeMap element){
		this.elements.add(element);
	}

	public void addAll(List<TreeNodeMap> newElements){
		this.elements.addAll(newElements);
	}

	public void remove(TreeNodeMap element){
		this.elements.remove(element);
	}

	public void removeAll(){
		this.elements.clear();
	}

	public List<TreeNodeMap> getElements() {
		return this.elements;
	}

	public boolean isSubNode(){
		return !isLeafNode();
	}

	public boolean isLeafNode(){
		return this.elements.size() == 0;
	}

	public String getValue() {
		return this.getMenuNm(FMSecurityContextHelper.getLang());
		
	}
	public String getLabel() {
		return this.getMenuNm(FMSecurityContextHelper.getLang());
		
	}

	public String toString(){
		String text = super.toString();
		return text.substring(0, text.length()-1) + ", ELEMENTS=" + elements + "}";
	}

	public TreeNodeMap deepCopy() throws CloneNotSupportedException{
		return this.deepCopy(false);
	}

	public TreeNodeMap deepCopy(boolean isDeepCopy) throws CloneNotSupportedException{
		TreeNodeMap copy = (TreeNodeMap)super.clone();
		List<TreeNodeMap> elements = new ArrayList<TreeNodeMap>();
		if(isDeepCopy){
			for(TreeNodeMap element: copy.elements){
				elements.add(element.deepCopy(isDeepCopy));
			}
			copy.elements = new ArrayList<TreeNodeMap>();
			copy.elements.addAll(elements);
		} else {
			copy.elements = new ArrayList<TreeNodeMap>();
		}
		return copy;
	}
}
