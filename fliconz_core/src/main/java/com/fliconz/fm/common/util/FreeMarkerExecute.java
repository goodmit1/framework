package com.fliconz.fm.common.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.TemplateModelException;
import freemarker.template.utility.Execute;

public class FreeMarkerExecute extends Execute
{
	protected final Logger theLogger = LoggerFactory.getLogger(this.getClass());
	//protected final static String ERROR_TEXT = "#ERROR#";

	protected class ExecuteError
	{
		public String toString()
		{
			return "#ERROR#";
		}
	}

	@Override
	public java.lang.Object exec(List args) throws TemplateModelException
	{
		if (theLogger.isDebugEnabled()) theLogger.debug("===> args: [{}]", args);
		if (args == null || args.isEmpty())
		{
			//return "#ERROR#";
			return new ExecuteError();
		}


		String cmd = (String)args.get(0);
		if (cmd.equals("CodeHelper.getStringValue"))
		{
			int argsCount = args.size();
			if (argsCount >= 3)
			{
				String codeKeyPath = (String)args.get(1);
				String subKey = (String)args.get(2);
				String defaultValue = argsCount >= 4 ? (String)args.get(3) : null;

				if (theLogger.isDebugEnabled()) theLogger.debug(String.format("===> CodeHelper.getStringValue : [%s] [%s] [%s]", codeKeyPath, subKey, defaultValue));
				String resultValue = CodeHelper.getStringValue(codeKeyPath, subKey, defaultValue);

				if (resultValue == null)
				{
					if (theLogger.isWarnEnabled()) theLogger.warn(String.format("===> [WARN] CodeHelper.getStringValue : [%s] [%s] [%s]", codeKeyPath, subKey, defaultValue));
					return "#ERROR#";
				}

				return resultValue;
			}
		}

//		else if (cmd.equals("SystemConfig.getString"))
//		{
//			if (argsCount >= 2)
//			{
//				String configId = (String)args.get(1);
//				String defaultValue = argsCount >= 3 ? (String)args.get(2) : null;
//
//				if (theLogger.isDebugEnabled()) theLogger.debug(String.format("===> SystemConfig.getString : [%s] [%s]", configId, defaultValue));
//				String resultValue = SystemConfig.getString(configId, defaultValue);
//
//				if (resultValue == null)
//				{
//					if (theLogger.isWarnEnabled()) theLogger.warn(String.format("===> [WARN] SystemConfig.getString : [%s] [%s]", configId, defaultValue));
//					return "#ERROR#";
//				}
//
//				return resultValue;
//			}
//		}

		//return "#ERROR#";
		return new ExecuteError();
	}
}
