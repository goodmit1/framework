package com.fliconz.fm.common.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class MockHttpServletResponse implements HttpServletResponse
{
	public MockHttpServletResponse()
	{
		super();
	}



	@Override
	public String getCharacterEncoding()
	{
		return null;
	}

	@Override
	public String getContentType()
	{
		return null;
	}

	@Override
	public ServletOutputStream getOutputStream() throws IOException
	{
		return null;
	}

	@Override
	public PrintWriter getWriter() throws IOException
	{
		return null;
	}

	@Override
	public void setCharacterEncoding(String charset)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void setContentLength(int len)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void setContentType(String type)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void setBufferSize(int size)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public int getBufferSize()
	{
		return 0;
	}

	@Override
	public void flushBuffer() throws IOException
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void resetBuffer()
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isCommitted()
	{
		return false;
	}

	@Override
	public void reset()
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void setLocale(Locale loc)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public Locale getLocale()
	{
		return null;
	}

	@Override
	public void addCookie(Cookie cookie)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean containsHeader(String name)
	{
		return false;
	}

	@Override
	public String encodeURL(String url)
	{
		return null;
	}

	@Override
	public String encodeRedirectURL(String url)
	{
		return null;
	}

	@Override
	public String encodeUrl(String url)
	{
		return null;
	}

	@Override
	public String encodeRedirectUrl(String url)
	{
		return null;
	}

	@Override
	public void sendError(int sc, String msg) throws IOException
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void sendError(int sc) throws IOException
	{
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public void sendRedirect(String location) throws IOException
	{
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public void setDateHeader(String name, long date)
	{
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public void addDateHeader(String name, long date)
	{
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public void setHeader(String name, String value)
	{
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public void addHeader(String name, String value)
	{
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public void setIntHeader(String name, int value)
	{
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public void addIntHeader(String name, int value)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void setStatus(int sc)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public void setStatus(int sc, String sm)
	{
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public int getStatus()
	{
		return 0;
	}

	@Override
	public String getHeader(String name)
	{
		return null;
	}

	@Override
	public Collection<String> getHeaders(String name)
	{
		return new LinkedList<String>();
	}

	@Override
	public Collection<String> getHeaderNames()
	{
		return new LinkedList<String>();
	}

}
