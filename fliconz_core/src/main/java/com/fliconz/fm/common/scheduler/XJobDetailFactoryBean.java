package com.fliconz.fm.common.scheduler;

//import java.util.Properties;

import org.quartz.JobDetail;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

//import com.fliconz.fm.common.util.XProperties;

public class XJobDetailFactoryBean extends JobDetailFactoryBean
{
//	private String m_serviceName = null;
//	private String m_serviceMethod = null;
//
//	private String m_schedulerServiceName = null;
//	private boolean m_schedulerLog = false;
//	private XProperties m_jobProp = null;

	public XJobDetailFactoryBean()
	{
		super();
	}



	// ====================================================================================================

//	public void setServiceName(String serviceName)
//	{
//		m_serviceName = serviceName;
//	}
//
//	public void setServiceMethod(String serviceMethod)
//	{
//		m_serviceMethod = serviceMethod;
//	}




//	public void setSchedulerServiceName(String serviceName)
//	{
//		m_schedulerServiceName = serviceName;
//	}
//
//	public void setSchedulerLog(boolean schedulerLog)
//	{
//		m_schedulerLog = schedulerLog;
//	}




//	public String getServiceName()
//	{
//		return m_serviceName;
//	}
//
//	public String getServiceMethod()
//	{
//		return TextHelper.evl(m_serviceMethod, "executeBatch");
//	}
//
//	public boolean isSchedulerLog()
//	{
//		return m_schedulerLog;
//	}
//
//	public boolean getSchedulerLog()
//	{
//		return isSchedulerLog();
//	}
//
//	public String getSchedulerServiceName()
//	{
//		return m_schedulerServiceName;
//	}


/*
	public void setProperties(Properties prop)
	{
		if (prop instanceof XProperties)
		{
			m_jobProp = (XProperties)prop;
		}
		else
		{
			m_jobProp = new XProperties();
			m_jobProp.putAll(prop);
		}
	}

	public Properties getProperties()
	{
		return getXProperties();
	}

	public XProperties getXProperties()
	{
		if (m_jobProp == null)
		{
			m_jobProp = new XProperties();
		}

		return m_jobProp;
	}
*/
	// ====================================================================================================

	private XJobDetail m_jobDetail = null;

	@Override
	public JobDetail getObject()
	{
		if (m_jobDetail == null)
		{
			m_jobDetail = new XJobDetail((JobDetail)super.getObject());
//			m_jobDetail.setServiceName(m_serviceName);
//			m_jobDetail.setServiceMethod(m_serviceMethod);
//			m_jobDetail.setSchedulerLog(m_schedulerLog);
//			m_jobDetail.setSchedulerServiceName(m_schedulerServiceName);
//			m_jobDetail.setProperties(m_jobProp);
		}

		return m_jobDetail;
	}


	@Override
	public Class<?> getObjectType()
	{
		return XJobDetail.class;
	}
}
