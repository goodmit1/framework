package com.fliconz.fm.common.geocode;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.LinkedList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.TextHelper;
//import net.sf.json.JSONObject;
//import net.sf.json.JSONSerializer;
//import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;

// http://dev.naver.com/openapi/apis/map/javascript/reference#Geocode


public class NaverGeocoder extends Geocoder
{
	private String m_mapAPIKey = null;

	public NaverGeocoder(String mapAPIKey)
	{
		super();
		m_mapAPIKey = mapAPIKey;
	}

	public String getAPIKey()
	{
		return m_mapAPIKey;
	}


	@Override
	public boolean canConvert(boolean isGeocode)
	{
		// 주소 --> 좌표만 반환하도록 한다.
		return isGeocode ? true : false;
	}

	@Override
	public boolean canPaging(boolean toCoordinate)
	{
		return false;
	}

	@Override
	public DataMap<String, Object> geocode(GeoLocation addr) throws GeocodeException
	{
		// 주소 --> 좌표
		HttpClient aHttpClient = null;
		StringReader aReader = null;

		try
		{
			DataMap<String,Object> locAttr = addr.getAttributes();
//			int pageNo = locAttr.getInt(KEY_REQUEST_PAGE, 1);
//			int rowCount = locAttr.getInt(KEY_REQUEST_COUNT, 10);
			String encoding = locAttr.getString(KEY_ENCODING,"UTF-8");

			StringBuilder sb = new StringBuilder()
				.append("http://maps.naver.com/api/geocode.php")
				.append("?key=").append(TextHelper.nvl(getAPIKey(), ""))
				.append("&query=").append(URLEncoder.encode(TextHelper.nvl(addr.getAddress(), ""), encoding))
				.append("&encoding=").append(encoding)
				.append("&coord=latlng");

			aHttpClient = HttpClientBuilder.create().build();
//			CookieStore aCookieStore = new BasicCookieStore();
//			aCookieStore.clear();
//		    //aCookieStore.addCookie(MyCookieStorageClass.getCookie());
//		    aHttpClient.setCookieStore(aCookieStore);

			HttpGet httpGet = new HttpGet(sb.toString());

//            HttpResponse response = aHttpClient.execute(httpGet);
			//System.out.println("["+sb.toString()+"]");

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseString = aHttpClient.execute(httpGet, responseHandler);
// System.out.println("["+responseString+"]");

			aReader = new StringReader(responseString);


			//System.out.println("aaa");
			SAXParserFactory aSAXParserFactory = SAXParserFactory.newInstance();
			aSAXParserFactory.setValidating(false);
			SAXParser aSAXParser = aSAXParserFactory.newSAXParser();

			//System.out.println("bbb");
			InputSource aInputSource = new InputSource(aReader);
			NaverGeocodeResultHandler aHandler = new NaverGeocodeResultHandler();
			aSAXParser.parse(aInputSource, aHandler);

			//System.out.println("ccc");
			return aHandler.mm_resultData;
		}
		catch (ClientProtocolException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (ParserConfigurationException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		catch (SAXException e)
		{
			throw new GeocodeException(e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}

			try
			{
				if (aReader != null)
				{
					aReader.close();
				}
			}
			catch (Exception e) {}
		}
	}

	@Override
	public DataMap<String, Object> reverseGeocode(GeoLocation coord) throws GeocodeException
	{
		// 좌표 --> 주소
		throw new GeocodeException("unsupported");
	}






	public class NaverGeocodeResultHandler extends DefaultHandler
	{
		DataMap<String,Object> mm_resultData = null;

		private LinkedList<String> mm_lstElement = null;
		private StringBuilder mm_sbText = null;
		private boolean mm_error = false;

		public NaverGeocodeResultHandler()
		{
			super();
			mm_resultData = new DataMap<String,Object>();
			mm_error = false;
		}




		public void startDocument() throws SAXException
		{
			//System.out.println("===> startDocument");
			super.startDocument();
			mm_lstElement = new LinkedList<String>();
			mm_sbText = new StringBuilder();
		}

		public void endDocument() throws SAXException
		{
			//System.out.println("===> endDocument");
			if (mm_lstElement != null)
			{
				mm_lstElement.clear();
				mm_lstElement = null;
			}

			if (mm_sbText != null)
			{
				mm_sbText.setLength(0);
				mm_sbText = null;
			}

			if (mm_error == false)
			{
				mm_resultData.setString(KEY_RESPONSE_CODE, "ok");
				mm_resultData.setString(KEY_RESPONSE_MESSAGE, "success");

				LinkedList<GeoLocation> aList = (LinkedList<GeoLocation>)mm_resultData.getObject(KEY_LOCATION_LIST);
				mm_resultData.setInt(KEY_REQUEST_PAGE, 1);
				mm_resultData.setInt(KEY_RESPONSE_COUNT, aList == null ? 0 : aList.size());
				mm_resultData.setInt(KEY_TOTAL_COUNT, aList == null ? 0 : aList.size());
			}

//			if (mm_resultData.isNotEmptyValue(KEY_LOCATION_LIST))
//			{
//				LinkedList<GeoLocation> aList = (LinkedList<GeoLocation>)mm_resultData.getObject(KEY_LOCATION_LIST);
//				List<DataMap<String,Object>> aList2 = new LinkedList<DataMap<String,Object>>();
//				for (GeoLocation loc : aList)
//				{
//					aList2.add(loc.toDataMap());
//				}
//				aList.clear();
//				aList = null;
//				mm_resultData.put(KEY_LOCATION_LIST, aList2);
//			}
			super.endDocument();
		}

		public void startElement(String namespaceURI, String localName, String qName, Attributes attrs) throws SAXException
		{
			mm_lstElement.add(qName);
			int intElementLevel = getElementLevel();
//            System.out.println("===> startElement : " + intElementLevel + " - "+ qName);
			switch (intElementLevel)
			{
				case 1:
					if ( "error".equals(qName) )
					{
						mm_resultData.clear();
						mm_error = true;
					}
				case 2:
					if (mm_error)
					{
						break;
					}
					if ( "item".equals(qName) )
					{
						LinkedList<GeoLocation> aList = (LinkedList<GeoLocation>)mm_resultData.getObject(KEY_LOCATION_LIST);
						if (aList == null)
						{
							aList = new LinkedList<GeoLocation>();
							mm_resultData.setObject(KEY_LOCATION_LIST, aList);
						}
						GeoLocation aLocation = new GeoLocation();
						aList.add(aLocation);
					}
					break;

				default:
					break;
			}
		}

/*
<geocode xmlns="naver:openapi">
	<userquery>
		<![CDATA[ 경기도성남시정자1동25-1 ]]>
	</userquery>
	<total>1</total>
	<item>
		<point>
			<x>321063</x>
			<y>529727</y>
		</point>
		<address>경기도 성남시 분당구 정자1동 25-1</address>
		<addrdetail>
			<sido>
				<![CDATA[ 경기도 ]]>
				<sigugun>
					<![CDATA[ 성남시 분당구 ]]>
					<dongmyun>
						<![CDATA[ 정자1동 ]]>
						<rest>
							<![CDATA[ 25-1 ]]>
						</rest>
					</dongmyun>
				</sigugun>
			</sido>
		</addrdetail>
	</item>
</geocode>
 */

		public void endElement(String namespaceURI, String localName, String qName) throws SAXException
		{
			try
			{
				int intElementLevel = getElementLevel();
				LinkedList<GeoLocation> aList = null;
//	            System.out.println("===> endElement : " + intElementLevel + " - "+ qName +" ["+mm_sbText.toString()+"]");

				switch (intElementLevel)
				{
					case 2:
						if (mm_error)
						{
							if ("error_code".equals(qName))
							{
								mm_resultData.setString(KEY_RESPONSE_CODE, mm_sbText.toString().trim());
							}
							else if ("message".equals(qName))
							{
								mm_resultData.setString(KEY_RESPONSE_MESSAGE, mm_sbText.toString().trim());
							}
							break;
						}
						if ( "total".equals(qName) )
						{
							int totalCount = TextHelper.parseInt(mm_sbText.toString().trim(), 0);
							if (totalCount <= 0)
							{
								mm_error = true;
								mm_resultData.setString(KEY_RESPONSE_CODE, "no-data");
								mm_resultData.setString(KEY_RESPONSE_MESSAGE, "no data");
							}
							mm_resultData.setInt(KEY_TOTAL_COUNT, totalCount);
						}
						break;

					case 3:
						if (mm_error)
						{
							break;
						}
						if ("address".equals(qName))
						{
							aList = (LinkedList<GeoLocation>)mm_resultData.getObject(KEY_LOCATION_LIST);
							GeoLocation aLocation = aList.getLast();
							aLocation.setAddress( mm_sbText.toString().trim() );
						}
						break;

					case 4:
						if (mm_error)
						{
							break;
						}
						if ("x".equals(qName))
						{
							aList = (LinkedList<GeoLocation>)mm_resultData.getObject(KEY_LOCATION_LIST);
							GeoLocation aLocation = aList.getLast();
							aLocation.setLongitude(TextHelper.parseFloat(mm_sbText.toString().trim()));
						}
						else if ("y".equals(qName))
						{
							aList = (LinkedList<GeoLocation>)mm_resultData.getObject(KEY_LOCATION_LIST);
							GeoLocation aLocation = aList.getLast();
							aLocation.setLatitude(TextHelper.parseFloat(mm_sbText.toString().trim()));
						}
						break;

					default:
						break;
				}

			}
			catch (IllegalStateException e)
			{
				throw e;
			}
			finally {
				mm_lstElement.remove(qName);
				mm_sbText.setLength(0);
			}
		}


		public void characters(char[] ch, int start, int len) throws SAXException
		{
			String strCurrentElementName = getElementName(0);
			int intElementLevel = getElementLevel();
			String text = null;

//            System.out.println("===> characters : " + intElementLevel + " - "+ strCurrentElementName);
			switch (intElementLevel)
			{
				case 2:
					if (mm_error)
					{
						if ("error_code".equals(strCurrentElementName) ||
							"message".equals(strCurrentElementName))
						{
							text = len == 0 ? null : new String(ch, start, len);
							if (text != null)
							{
								mm_sbText.append(text);
							}
						}
						break;
					}
					if ("total".equals(strCurrentElementName))
					{
						text = len == 0 ? null : new String(ch, start, len);
						if (text != null)
						{
							mm_sbText.append(text);
						}
					}
					break;

				case 3:
					if (mm_error)
					{
						break;
					}
					if ("address".equals(strCurrentElementName))
					{
						text = len == 0 ? null : new String(ch, start, len);
						if (text != null)
						{
							mm_sbText.append(text);
						}
					}
					break;

				case 4:
					if (mm_error)
					{
						break;
					}
					if ("x".equals(strCurrentElementName) ||
						"y".equals(strCurrentElementName))
					{
						text = len == 0 ? null : new String(ch, start, len);
						if (text != null)
						{
							mm_sbText.append(text);
						}
					}
					break;

				default:
					break;
			}
		}


		public void error(SAXParseException e) throws SAXException
		{
			mm_resultData.setString(KEY_RESPONSE_CODE, "failed");
			mm_resultData.setString(KEY_RESPONSE_MESSAGE, e.getMessage());
		}

		public void fatalError(SAXParseException e) throws SAXException
		{
			mm_resultData.setString(KEY_RESPONSE_CODE, "failed");
			mm_resultData.setString(KEY_RESPONSE_MESSAGE, e.getMessage());
		}

		public void warning(SAXParseException e) throws SAXException
		{
			//System.out.println("------------------------------------------------------------");
			e.printStackTrace();
			//System.out.println("------------------------------------------------------------");
		}

		private String getElementName(int back)
		{
			return mm_lstElement.isEmpty()
				? null
				: (String)mm_lstElement.get(mm_lstElement.size() - 1 - back);
		}

		private int getElementLevel()
		{
			return mm_lstElement.size();
		}
	}



/*
WEB_INF_HOME=/Volumes/data/projects/SmartJM/development/web/smartjm/WebContent/WEB-INF
THIS_CLASSPATH=$WEB_INF_HOME/classes:$WEB_INF_HOME/lib/httpclient-4.0.3.jar:$WEB_INF_HOME/lib/httpcore-4.0.1.jar:$WEB_INF_HOME/lib/commons-logging-1.1.1.jar:$WEB_INF_HOME/lib/json-lib-2.3-jdk15.jar:$WEB_INF_HOME/lib/commons-lang-2.4.jar:$WEB_INF_HOME/lib/ezmorph-1.0.6.jar:$WEB_INF_HOME/lib/commons-collections-3.2.1.jar:$WEB_INF_HOME/lib/commons-beanutils-1.8.0.jar

java -cp $THIS_CLASSPATH com.fliconz.fm.common.geocode.NaverGeocoder
*/
	public static void main(String[] args) throws Exception
	{
		NaverGeocoder geocoder = new NaverGeocoder("8473f956cf30bbd1d1adfe25c0c9a540");
		GeoLocation aLocation = new GeoLocation();
		DataMap<String,Object> resultData = null;



		System.out.println("=========================");
		System.out.println(geocoder.getClass().getName());

		// http://maps.naver.com/api/geocode.php?key=8473f956cf30bbd1d1adfe25c0c9a540&query=%ED%9A%A8%EC%9E%90%EB%8F%99&encoding=UTF-8&coord=latlng
		aLocation.setAddress("서울 용산구 동자동");
		resultData = geocoder.geocode(aLocation);
		System.out.println("=========================");
		System.out.println("geocode : "+ resultData.toString());


		System.out.println();
		System.out.println("=========================");
		System.out.println("reverse geocode : not supported");
	}
}
