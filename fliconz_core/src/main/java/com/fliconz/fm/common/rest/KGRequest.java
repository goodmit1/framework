package com.fliconz.fm.common.rest;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.cookie.ClientCookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.common.util.HttpHelper;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.common.util.XProperties;

public class KGRequest
{
	//public final static String KEY_API_KEY = "api_key";
	public final static String KEY_REQUEST_ID = "req_id";
	public final static String KEY_REQUEST_DATA = "req_data";

	public final static String KEY_RESPONSE_CODE = "res_code";
	public final static String KEY_RESPONSE_MESSAGE = "res_msg";
	public final static String KEY_RESPONSE_DATA = "res_data";

	//public final static String AUTH_HEADER = "xauth-sescm";
	private final static String DEFAULT_USER_AGENT = String.format("%s 1.0.0", KGRequest.class.getName());

	private final Logger theLogger = LoggerFactory.getLogger(this.getClass());

	public enum KGRequestType
	{
		FormType
		, JSONType
//		, JSONArrayType
		, NOTHING
	}
	public enum KGHttpMethod
	{
		PostMethod
		, PutMethod
		, PatchMethod
		, GetMethod
		, DeleteMethod
		, HeadMethod
		, TraceMethod
		, OptionsMethod
		, NOTHING
	}
	public enum KGResponseType
	{
		TextType
		, JSONType
		, JSONArrayType
		, NOTHING
	}

	private final static int DEFAULT_PORT = 80;
	private final static String DEFAULT_PATH = "/api/gateway/request.do";
	private final static String DEFAULT_ENCODING = "UTF-8";

	private boolean m_isSSL = false;
	private String m_host = null;
	private int m_port = DEFAULT_PORT;
	private String m_path = DEFAULT_PATH;
	private String m_query = null;
	private String m_encoding = DEFAULT_ENCODING;

	private KGHttpMethod m_httpMethod = KGHttpMethod.PostMethod;
	private KGRequestType m_requestType = KGRequestType.FormType;
	private KGResponseType m_responseType = KGResponseType.TextType;

	private KGRequest.ResponseListener m_responseListener = null;
	private List<ClientCookie> m_listCookie = null;
	private List<String> m_notEncodingParamNames = null;
	//private List<String> m_notEncodingHeaderNames = null;

	private DataMap<String,String> m_responseHeader = null;

	public KGRequest()
	{
		super();
		this.m_responseHeader = new DataMap<String,String>();
		this.reset();
	}


	public void reset() {
		this.m_isSSL = false;
		this.m_host = null;
		this.m_port = DEFAULT_PORT;
		this.m_path = DEFAULT_PATH;
		this.m_query = null;
		this.m_encoding = DEFAULT_ENCODING;

		this.m_responseListener = null;
		this.m_listCookie = null;
		this.m_notEncodingParamNames = null;
		//this.m_notEncodingHeaderNames = null;

		this.m_httpMethod = KGHttpMethod.PostMethod;
		this.m_requestType = KGRequestType.FormType;
		this.m_responseType = KGResponseType.TextType;

		this.m_responseHeader.clear();
	}



//	public KGRequest(String url) throws java.net.MalformedURLException
//	{
//		this();
//		java.net.URL aURL = new java.net.URL(url);
//		this.setSSL(aURL.getProtocol().toLowerCase().equals("https"));
//		this.setHost(aURL.getHost());
//		this.setPort(aURL.getPort());
//		this.setPath(aURL.getPath());
//	}


	public void setCookies(List<ClientCookie> cookies) {
		this.m_listCookie = cookies;
		//System.out.println("===> cookies : "+ cookies);
	}
	public void setResponseListener(KGRequest.ResponseListener responseListener) {
		this.m_responseListener = responseListener;
	}

	public void setSSL(boolean isSSL)
	{
		this.m_isSSL = isSSL;
	}

	public void setHost(String host)
	{
		this.m_host = host;
	}

	public void setPort(int port)
	{
		this.m_port = port;
	}

	public void setPath(String path)
	{
		this.m_path = path;
	}

	public void setQuery(String query)
	{
		this.m_query = query;
	}


	public void setRequestAddress(String requestAddress) throws MalformedURLException
	{
		//System.out.println("===> requestAddress: "+ requestAddress);
		URL aURL = new URL(requestAddress);
		if (aURL.getProtocol().toLowerCase().equals("http"))
		{
			this.setSSL(false);
		}
		else if (aURL.getProtocol().toLowerCase().equals("https"))
		{
			this.setSSL(true);
		}
		else
		{
			throw new IllegalArgumentException("unsupported protocol");
		}

		//System.out.println("===> getHost: "+ aURL.getHost());
		//System.out.println("===> getPort: "+ aURL.getPort());
		//System.out.println("===> getPath: "+ aURL.getPath());
		//System.out.println("===> getQuery: "+ aURL.getQuery());
		this.setHost(aURL.getHost());
		this.setPort(aURL.getPort());
		this.setPath(aURL.getPath());
		this.setQuery(aURL.getQuery());
	}
	public void setAPIAddress(String requestAddress) throws MalformedURLException {
		this.setRequestAddress(requestAddress);
	}

	public void setNotEncodingParamNames(List<String> notEncodingParamNames) {
		this.m_notEncodingParamNames = notEncodingParamNames;
	}
	public void setNotEncodingParamNames(String... notEncodingParamNames) {
		List<String> aList = java.util.Arrays.asList(notEncodingParamNames);
		this.setNotEncodingParamNames(aList);
	}

	//public void setNotEncodingHeaderNames(List<String> notEncodingHeaderNames) {
	//	this.m_notEncodingHeaderNames = notEncodingHeaderNames;
	//}
	//public void setNotEncodingHeaderNames(String... notEncodingHeaderNames) {
	//	List<String> aList = java.util.Arrays.asList(notEncodingHeaderNames);
	//	this.setNotEncodingHeaderNames(aList);
	//}

	public void setEncoding(String encoding)
	{
		this.m_encoding = encoding;
	}

	public void setHttpMethod(KGHttpMethod httpMethod)
	{
		this.m_httpMethod = httpMethod;
	}

	public void setRequestType(KGRequestType requestType)
	{
		this.m_requestType = requestType;
	}

	public void setResponseType(KGResponseType responseType)
	{
		this.m_responseType = responseType;
	}


	public boolean getSSL()
	{
		return this.isSSL();
	}
	public boolean isSSL()
	{
		return this.m_isSSL;
	}

	public String getHost()
	{
		return this.m_host;
	}

	public int getPort()
	{
		return this.m_port;
	}

	public String getPath()
	{
		return this.m_path;
	}

	public String getEncoding()
	{
		return this.m_encoding;
	}

	public KGHttpMethod getHttpMethod()
	{
		return this.m_httpMethod;
	}

	public KGRequestType getRequestType()
	{
		return this.m_requestType;
	}

	public KGResponseType getResponseType()
	{
		return this.m_responseType;
	}



	public Map<String,String> getResponseHeader() {
		return this.m_responseHeader;
	}


	public String getAPIAddress()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(this.m_isSSL ? "https" : "http");
		sb.append("://");
		sb.append(this.m_host);
		if (this.m_port > 0 && ((this.m_isSSL == false && this.m_port == 80) || (this.m_isSSL == true && this.m_port == 443)) == false)
		{
			sb.append(":").append(this.m_port);
		}
		sb.append(this.m_path);

		if (TextHelper.isNotEmpty(this.m_query)) {
			if (sb.indexOf("?") > 0) {
				sb.append(this.m_query);
			} else {
				sb.append("?").append(this.m_query);
			}
		}
		return sb.toString();
	}
//	public String getAPIAddress()
//	{
//		return this.getAPIAddress(null);
//	}

	public String getRequestAddress() {
		return this.getAPIAddress();
	}



	private String makeQueryString(Map<String,Object> param, String encoding) throws UnsupportedEncodingException
	{
		if (param == null || param.isEmpty())
		{
			return null;
		}

		StringBuilder sb = new StringBuilder();
		for (Iterator<String> it = param.keySet().iterator(); it.hasNext();)
		{
			String name = it.next();
			boolean isEncoding = this.m_notEncodingParamNames == null || this.m_notEncodingParamNames.contains(name) == false;

			String value = null;
			Object aValue = param.get(name);
			if (aValue == null) {
				continue;
			}
			if (aValue instanceof String[])
			{
				for (String vv : (String[])aValue)
				{
					if (vv == null) {
						continue;
					}
					value = isEncoding ? java.net.URLEncoder.encode(vv, encoding) : vv;
					sb.append("&").append(name).append("=").append(value);
				}
			}
			else if (aValue instanceof List)
			{
				for (Object vv : (List)aValue)
				{
					if (vv == null) {
						continue;
					}
					value = isEncoding ? java.net.URLEncoder.encode(vv.toString(), encoding) : vv.toString();
					sb.append("&").append(name).append("=").append(value);
				}
			}
			else
			{
				value = isEncoding ? java.net.URLEncoder.encode(aValue.toString(), encoding) : aValue.toString();
				sb.append("&").append(name).append("=").append(value);
			}
		}

		return sb.length() == 0 ? null : sb.substring(1);
	}


	public Object execute(Map<String,String> header, Map<String,Object> param) throws KGRequestException
	{
		if (this.getRequestType() == KGRequestType.FormType)
		{
			return this.doExecuteForm(header, param);
		}
		else if (this.getRequestType() == KGRequestType.JSONType)
		//else if (this.getRequestType() == KGRequestType.JSONType || this.getRequestType() == KGRequestType.JSONArrayType)
		{
			return this.doExecuteJSON(header, param);
		}
		else
		{
			throw new KGRequestException(String.format("unsupported at request type (reqeust-type=%d)", this.getRequestType()));
		}
	}

	public Object execute(Map<String,Object> param) throws KGRequestException
	{
		return execute(null, param);
	}

	private Object doExecuteForm(Map<String,String> header, Map<String,Object> param) throws KGRequestException
	{
		HttpClient aHttpClient = null;

		try
		{
			this.setRequestType(KGRequest.KGRequestType.FormType);

			if (this.m_listCookie != null) {
				BasicCookieStore cookieStore = new BasicCookieStore();
				for (ClientCookie cookie : this.m_listCookie) {
					cookieStore.addCookie(cookie);
					//System.out.println("===> cookie : "+ cookie);
				}
				aHttpClient = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
			}
			else {
				aHttpClient = HttpClientBuilder.create().build();
			}

			String encoding = this.getEncoding();
			if (TextHelper.isEmpty(encoding))
			{
				encoding = DEFAULT_ENCODING;
			}

			StringEntity aStringEntity = null;
			String urlString = this.getAPIAddress();
			String httpMethod = null;

			if (param != null && param.isEmpty() == false)
			{
//				if (httpMethod == KGHttpMethod.GetMethod)
//				{
//					throw new KGRequestException("paramter must be empty in GET method.");
//				}
//				else if (httpMethod == KGHttpMethod.DeleteMethod)
//				{
//					throw new KGRequestException("paramter must be empty in DELETE method.");
//				}
				if (this.getHttpMethod() == KGHttpMethod.GetMethod || this.getHttpMethod() == KGHttpMethod.DeleteMethod
						|| this.getHttpMethod() == KGHttpMethod.HeadMethod || this.getHttpMethod() == KGHttpMethod.TraceMethod
						|| this.getHttpMethod() == KGHttpMethod.OptionsMethod)
				{
					String queryString = this.makeQueryString(param, encoding);
					if (queryString != null)
					{
						if (urlString.indexOf('?') > 0)
						{
							urlString = String.format("%s&%s", urlString, queryString);
						}
						else
						{
							urlString = String.format("%s?%s", urlString, queryString);
						}
					}
				}
				else
				{
					List<NameValuePair> paramList = new LinkedList<NameValuePair>();
					for (Iterator<String> it = param.keySet().iterator(); it.hasNext();)
					{
						String name = it.next();
						Object aValue = param.get(name);
						if (aValue == null) {
							continue;
						}
						if (aValue instanceof String[])
						{
							for (String value : (String[])aValue)
							{
								paramList.add(new BasicNameValuePair(name, value));
							}
						}
						else if (aValue instanceof List)
						{
							for (Object value : (List)aValue)
							{
								paramList.add(new BasicNameValuePair(name, value.toString()));
							}
						}
						else
						{
							paramList.add(new BasicNameValuePair(name, aValue.toString()));
						}
					}

					aStringEntity = new UrlEncodedFormEntity(paramList, encoding);
				}
			}


			//System.out.println(String.format("===> urlString: [%s]", urlString));
			HttpRequestBase aRequest = null;
			if (this.getHttpMethod() == KGHttpMethod.PostMethod)
			{
				httpMethod = "post";
				aRequest = new HttpPost(urlString);
				if (aStringEntity != null)
				{
					((HttpPost)aRequest).setEntity(aStringEntity);
				}
			}
			else if (this.getHttpMethod() == KGHttpMethod.PutMethod)
			{
				httpMethod = "put";
				aRequest = new HttpPut(this.getAPIAddress());
				if (aStringEntity != null)
				{
					((HttpPut)aRequest).setEntity(aStringEntity);
				}
			}
			else if (this.getHttpMethod() == KGHttpMethod.PatchMethod)
			{
				httpMethod = "patch";
				aRequest = new HttpPatch(this.getAPIAddress());
				if (aStringEntity != null)
				{
					((HttpPatch)aRequest).setEntity(aStringEntity);
				}
			}
			else if (this.getHttpMethod() == KGHttpMethod.GetMethod)
			{
				httpMethod = "get";
				aRequest = new HttpGet(urlString);
			}
			else if (this.getHttpMethod() == KGHttpMethod.DeleteMethod)
			{
				httpMethod = "delete";
				aRequest = new HttpDelete(urlString);
			}
			else if (this.getHttpMethod() == KGHttpMethod.HeadMethod)
			{
				httpMethod = "head";
				aRequest = new HttpHead(urlString);
			}
			else if (this.getHttpMethod() == KGHttpMethod.TraceMethod)
			{
				httpMethod = "trace";
				aRequest = new HttpTrace(urlString);
			}
			else if (this.getHttpMethod() == KGHttpMethod.OptionsMethod)
			{
				httpMethod = "options";
				aRequest = new HttpOptions(urlString);
			}
			else
			{
				throw new KGRequestException(String.format("unsupported http method (http-method: %d).", this.getHttpMethod()));
			}

			aRequest.setHeader("User-Agent", KGRequest.DEFAULT_USER_AGENT);
			if (header != null && header.isEmpty() == false)
			{
				for (Iterator<String> it = header.keySet().iterator(); it.hasNext();)
				{
					String name = it.next();
					aRequest.setHeader(name, header.get(name));
					//boolean isEncoding = this.m_notEncodingHeaderNames == null || this.m_notEncodingHeaderNames.contains(name) == false;
					//String value = null;
					//if (isEncoding) {
					//	value = java.net.URLEncoder.encode(header.get(name), encoding);
					//}
					//else {
					//	value = header.get(name);
					//}
					//aRequest.setHeader(name, value);
				}
			}

			//String ipAddress = java.net.InetAddress.getLocalHost().getHostAddress();
			//String reqId = (String)param.get(KEY_REQUEST_ID);
			//System.out.println(String.format("===> [%s] / [%s]", this.getPath(), reqId));
			//String authKey = KGRequestHelper.makeAuthKey(httpMethod, this.getPath(), reqId);
			//System.out.println(String.format("===> [%s]", authKey));

			aRequest.setHeader("Content-Type", String.format("application/x-www-form-urlencoded; charset=%s", encoding));
			//aRequest.setHeader(AUTH_HEADER, authKey);
			//System.out.println("===> getParams: "+aRequest.getParams());

			HttpResponse aResponse = aHttpClient.execute(aRequest);
			if (theLogger.isDebugEnabled()) {
				StringBuilder sb = new StringBuilder();
				for (Header hdr : aRequest.getAllHeaders())
				//for (HeaderIterator it = aRequest.headerIterator(); it.hasNext();)
				{
					//Header aHeader = (Header)it.next();
					sb.append(String.format("[%s] = [%s]", hdr.getName(), hdr.getValue())).append("\n");
				}
				//System.out.printf("Request-Header : %s\n", sb.toString());
				theLogger.debug("Request-Header :\n{}", sb.toString());

				sb.setLength(0);
				for (Header hdr : aResponse.getAllHeaders()) {
					sb.append(String.format("[%s] = [%s]", hdr.getName(), hdr.getValue())).append("\n");
				}
				theLogger.debug("Response-Header :\n{}", sb.toString());
			}

			//System.out.println("===>"+aResponse.getStatusLine().getStatusCode());
			if (aResponse.getStatusLine().getStatusCode() != 200)
			{
				//Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
				throw new KGRequestException(aResponse.getStatusLine().toString());
			}

			this.m_responseHeader.clear();
			for (Header hdr : aResponse.getAllHeaders()) {
				this.m_responseHeader.put(hdr.getName(), hdr.getValue());
			}
			if (this.m_responseListener != null) {
				this.m_responseListener.onResponse(aResponse);
			}

			if (this.getResponseType() == KGResponseType.TextType || this.getResponseType() == KGResponseType.JSONType || this.getResponseType() == KGResponseType.JSONArrayType)
			{
				ResponseHandler<String> aResponseHandler = new BasicResponseHandler();
				String responseBody = aResponseHandler.handleResponse(aResponse);
				if (theLogger.isDebugEnabled()) theLogger.debug("responseBody: [{}]", responseBody);
				//System.out.println("===> responseBody: "+responseBody);

				if (this.getResponseType() == KGResponseType.JSONType)
				{
					org.json.JSONObject jsonResult = new org.json.JSONObject(responseBody);
					DataMap<String,Object> resultMap = new DataMap<String,Object>();
					TextHelper.toJavaMap(jsonResult, resultMap);

					return resultMap;
				}
				else if (this.getResponseType() == KGResponseType.JSONArrayType) {
					org.json.JSONArray resultJSONArray = new org.json.JSONArray(responseBody);
					List<Object> resultList = new LinkedList<Object>();
					TextHelper.toJavaList(resultJSONArray, resultList);
					return resultList;
				}
				else
				{
					return responseBody;
				}
			}
			throw new KGRequestException(String.format("unsupported response type (response-type=%d).", this.getResponseType()));
		}
		catch (org.json.JSONException e)
		{
			throw new KGRequestException(e.getMessage(), e);
		}
		catch (UnsupportedEncodingException e)
		{
			throw new KGRequestException(e.getMessage(), e);
		}
		catch (ClientProtocolException e)
		{
			throw new KGRequestException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new KGRequestException(e.getMessage(), e);
		}
//		catch (NoSuchAlgorithmException e)
//		{
//			throw new KGRequestException(e.getMessage(), e);
//		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}



	private Object doExecuteJSON(Map<String,String> header, Map<String,Object> param) throws KGRequestException
	{
		HttpClient aHttpClient = null;
		Object resultValue = null;

		try
		{
			this.setRequestType(KGRequest.KGRequestType.JSONType);

			if (this.m_listCookie != null) {
				BasicCookieStore cookieStore = new BasicCookieStore();
				for (ClientCookie cookie : this.m_listCookie) {
					cookieStore.addCookie(cookie);
					//System.out.println("===> cookie : "+ cookie);
				}
				aHttpClient = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
			}
			else {
				aHttpClient = HttpClientBuilder.create().build();
			}

			String encoding = this.getEncoding();
			if (TextHelper.isEmpty(encoding))
			{
				encoding = DEFAULT_ENCODING;
			}

			StringEntity aStringEntity = null;
			String urlString = this.getAPIAddress();
			String httpMethod = null;

			if (param != null && param.isEmpty() == false)
			{
//				if (httpMethod == KGHttpMethod.GetMethod)
//				{
//					throw new KGRequestException("paramter must be empty in GET method.");
//				}
//				else if (httpMethod == KGHttpMethod.DeleteMethod)
//				{
//					throw new KGRequestException("paramter must be empty in DELETE method.");
//				}
//				org.json.JSONObject jsonParam = new org.json.JSONObject(param);
//				aStringEntity = new StringEntity(jsonParam.toString(), encoding);
				if (this.getHttpMethod() == KGHttpMethod.GetMethod || this.getHttpMethod() == KGHttpMethod.DeleteMethod
						|| this.getHttpMethod() == KGHttpMethod.HeadMethod || this.getHttpMethod() == KGHttpMethod.TraceMethod
						|| this.getHttpMethod() == KGHttpMethod.OptionsMethod)
				{
					String queryString = this.makeQueryString(param, encoding);
					if (queryString != null)
					{
						if (urlString.indexOf('?') > 0)
						{
							urlString = String.format("%s&%s", urlString, queryString);
						}
						else
						{
							urlString = String.format("%s?%s", urlString, queryString);
						}
					}
				}
				else
				{
					org.json.JSONObject jsonParam = new org.json.JSONObject(param);
					aStringEntity = new StringEntity(jsonParam.toString(), encoding);
				}
			}


			//System.out.println(String.format("===> urlString: [%s]", urlString));
			HttpRequestBase aRequest = null;
			if (this.getHttpMethod() == KGHttpMethod.PostMethod)
			{
				httpMethod = "post";
				aRequest = new HttpPost(urlString);
				if (aStringEntity != null)
				{
					((HttpPost)aRequest).setEntity(aStringEntity);
				}
			}
			else if (this.getHttpMethod() == KGHttpMethod.PutMethod)
			{
				httpMethod = "put";
				aRequest = new HttpPut(this.getAPIAddress());
				if (aStringEntity != null)
				{
					((HttpPut)aRequest).setEntity(aStringEntity);
				}
			}
			else if (this.getHttpMethod() == KGHttpMethod.PatchMethod)
			{
				httpMethod = "patch";
				aRequest = new HttpPatch(this.getAPIAddress());
				if (aStringEntity != null)
				{
					((HttpPatch)aRequest).setEntity(aStringEntity);
				}
			}
			else if (this.getHttpMethod() == KGHttpMethod.GetMethod)
			{
				httpMethod = "get";
				aRequest = new HttpGet(urlString);
			}
			else if (this.getHttpMethod() == KGHttpMethod.DeleteMethod)
			{
				httpMethod = "delete";
				aRequest = new HttpDelete(urlString);
			}
			else if (this.getHttpMethod() == KGHttpMethod.HeadMethod)
			{
				httpMethod = "head";
				aRequest = new HttpHead(urlString);
			}
			else if (this.getHttpMethod() == KGHttpMethod.TraceMethod)
			{
				httpMethod = "trace";
				aRequest = new HttpTrace(urlString);
			}
			else if (this.getHttpMethod() == KGHttpMethod.OptionsMethod)
			{
				httpMethod = "options";
				aRequest = new HttpOptions(urlString);
			}
			else
			{
				throw new KGRequestException(String.format("unsupported http method (http-method: %d).", this.getHttpMethod()));
			}

			aRequest.setHeader("User-Agent", KGRequest.DEFAULT_USER_AGENT);
			if (header != null && header.isEmpty() == false)
			{
				for (Iterator<String> it = header.keySet().iterator(); it.hasNext();)
				{
					String name = it.next();
					aRequest.setHeader(name, header.get(name));
					//boolean isEncoding = this.m_notEncodingHeaderNames == null || this.m_notEncodingHeaderNames.contains(name) == false;
					//String value = null;
					//if (isEncoding) {
					//	value = java.net.URLEncoder.encode(header.get(name), encoding);
					//}
					//else {
					//	value = header.get(name);
					//}
					//aRequest.setHeader(name, value);
				}
			}

			//String reqId = (String)param.get(KEY_REQUEST_ID);
			//String ipAddress = java.net.InetAddress.getLocalHost().getHostAddress();
			//System.out.println(String.format("===> [%s] / [%s]", this.getPath(), reqId));
			//String authKey = KGRequestHelper.makeAuthKey(httpMethod, this.getPath(), reqId);
			//System.out.println(String.format("===> [%s]", authKey));

			aRequest.setHeader("Content-Type", String.format("application/json; charset=%s", encoding));
			//aRequest.setHeader(AUTH_HEADER, authKey);

			if (theLogger.isDebugEnabled())
			{
				StringBuilder sb = new StringBuilder();
				for (HeaderIterator it = aRequest.headerIterator(); it.hasNext();)
				{

					Header aHeader = (Header)it.next();
					sb.append(String.format("[%s] = [%s]", aHeader.getName(), aHeader.getValue())).append("\n");
				}
				//System.out.printf("Request-Header : %s\n", sb.toString());
				theLogger.debug("Request-Header :\n{}", sb.toString());
			}

			if (theLogger.isDebugEnabled()) theLogger.debug("Request-Param :\n{}", param);

			HttpResponse aResponse = aHttpClient.execute(aRequest);
			if (theLogger.isDebugEnabled())
			{
				StringBuilder sb = new StringBuilder();
				for (HeaderIterator it = aResponse.headerIterator(); it.hasNext();)
				{
					Header aHeader = (Header)it.next();
					sb.append(String.format("[%s] = [%s]", aHeader.getName(), aHeader.getValue())).append("\n");
				}
				//System.out.printf("Response-Header : %s\n", sb.toString());
				theLogger.debug("Response-Header :\n{}", sb.toString());
			}

			if (aResponse.getStatusLine().getStatusCode() != 200)
			{
				//Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
				throw new KGRequestException(aResponse.getStatusLine().toString());
			}


			this.m_responseHeader.clear();
			for (Header hdr : aResponse.getAllHeaders()) {
				this.m_responseHeader.put(hdr.getName(), hdr.getValue());
			}
			if (this.m_responseListener != null) {
				this.m_responseListener.onResponse(aResponse);
			}

			if (this.getResponseType() == KGResponseType.TextType || this.getResponseType() == KGResponseType.JSONType || this.getResponseType() == KGResponseType.JSONArrayType)
			{
				ResponseHandler<String> aResponseHandler = new BasicResponseHandler();
				String responseBody = aResponseHandler.handleResponse(aResponse);

				//theLogger.debug("Response-Body :\n{}", responseBody);
				if (this.getResponseType() == KGResponseType.JSONType)
				{
					org.json.JSONObject jsonResult = new org.json.JSONObject(responseBody);
					Map<String,Object> resultMap = new DataMap<String,Object>();
					TextHelper.toJavaMap(jsonResult, resultMap);

					return resultMap;
				}
				else if (this.getResponseType() == KGResponseType.JSONArrayType) {
					org.json.JSONArray resultJSONArray = new org.json.JSONArray(responseBody);
					List<Object> resultList = new LinkedList<Object>();
					TextHelper.toJavaList(resultJSONArray, resultList);
					return resultList;
				}
				else
				{
					return responseBody;
				}
			}
			throw new KGRequestException(String.format("unsupported response type (response-type=%d).", this.getResponseType()));
		}
		catch (org.json.JSONException e)
		{
			throw new KGRequestException(e.getMessage(), e);
		}
		catch (UnsupportedEncodingException e)
		{
			throw new KGRequestException(e.getMessage(), e);
		}
		catch (ClientProtocolException e)
		{
			throw new KGRequestException(e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new KGRequestException(e.getMessage(), e);
		}
//		catch (NoSuchAlgorithmException e)
//		{
//			throw new KGRequestException(e.getMessage(), e);
//		}
		finally
		{
			try
			{
				if (aHttpClient != null && aHttpClient instanceof java.io.Closeable)
				{
					((java.io.Closeable)aHttpClient).close();
				}
			}
			catch (Throwable e) {}
		}
	}


/*
	public Object doExecute(KGRequestType requestType, KGHttpMethod httpMethod, Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		if (requestType == KGRequestType.FormType)
		{
			return this.doExecuteForm(httpMethod, param, responseType);
		}
		else if (requestType == KGRequestType.JSONType)
		{
			return this.doExecuteJSON(httpMethod, param, responseType);
		}
		else
		{
			throw new KGRequestException(String.format("unsupported at request type (reqeust-type=%d)", requestType));
		}
	}





	public Object doPostForm(Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		return this.doExecuteForm(KGHttpMethod.PostMethod, param, responseType);
	}

	public Object doPostJSON(Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		return this.doExecuteJSON(KGHttpMethod.PostMethod, param, responseType);
	}





	public Object doGetForm(Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		return this.doExecuteForm(KGHttpMethod.GetMethod, param, responseType);
	}

	public Object doGetJSON(Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		return this.doExecuteJSON(KGHttpMethod.GetMethod, param, responseType);
	}





	public Object doPutForm(Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		return this.doExecuteForm(KGHttpMethod.PutMethod, param, responseType);
	}

	public Object doPutJSON(Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		return this.doExecuteJSON(KGHttpMethod.PutMethod, param, responseType);
	}




	public Object doDeleteForm(Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		return this.doExecuteForm(KGHttpMethod.DeleteMethod, param, responseType);
	}

	public Object doDeleteJSON(Map<String,Object> param, KGResponseType responseType) throws KGRequestException
	{
		return this.doExecuteJSON(KGHttpMethod.DeleteMethod, param, responseType);
	}

	*/


	public static KGRequest.KGHttpMethod parseHttpMethod(String method)
	{
		String httpMethod = method.toUpperCase();
		if (httpMethod.equals("POST"))
		{
			return KGRequest.KGHttpMethod.PostMethod;
		}
		else if (httpMethod.equals("PUT"))
		{
			return KGRequest.KGHttpMethod.PutMethod;
		}
		else if (httpMethod.equals("PATCH"))
		{
			return KGRequest.KGHttpMethod.PatchMethod;
		}
		else if (httpMethod.equals("GET"))
		{
			return KGRequest.KGHttpMethod.GetMethod;
		}
		else if (httpMethod.equals("DELETE"))
		{
			return KGRequest.KGHttpMethod.DeleteMethod;
		}
		else if (httpMethod.equals("HEAD"))
		{
			return KGRequest.KGHttpMethod.HeadMethod;
		}
		else if (httpMethod.equals("TRACE"))
		{
			return KGRequest.KGHttpMethod.TraceMethod;
		}
		else if (httpMethod.equals("OPTION"))
		{
			return KGRequest.KGHttpMethod.OptionsMethod;
		}

		return KGRequest.KGHttpMethod.NOTHING;
	}

	public static KGRequest.KGRequestType parseRequestType(String type)
	{
		String requestType = type.toUpperCase();
		if (requestType.equals("FORM"))
		{
			return KGRequest.KGRequestType.FormType;
		}
		else if (requestType.equals("JSON"))
		{
			return KGRequest.KGRequestType.JSONType;
		}
//			else if (requestType.equals("JSONArray"))
//			{
//				return KGRequest.KGRequestType.JSONArrayType;
//			}

		return KGRequest.KGRequestType.NOTHING;
	}

	public static KGRequest.KGResponseType parseResponseType(String type)
	{
		String responseType = type.toUpperCase();
		if (responseType.equals("TEXT"))
		{
			return KGRequest.KGResponseType.TextType;
		}
		else if (responseType.equals("JSON"))
		{
			return KGRequest.KGResponseType.JSONType;
		}
		else if (responseType.equals("JSONArray"))
		{
			return KGRequest.KGResponseType.JSONArrayType;
		}

		return KGRequest.KGResponseType.NOTHING;
	}


	public static void main(String[] args)
	{
		/*
		 RequestAddress	: request address (required)
		 RequestID		: request id  (optional)
		 HttpMethod		: http method  (default : post)
		 RequestType	: form | json (default : form)
		 ResponseType	: text : json (default : text)
		 RequestJSON	: json string (optional)
		 QueryString	: query string (optional)
		 Header			: key=value properties string (optional)
		 */
		try
		{
			String requestAddress = System.getProperty("RequestAddress");
			if (TextHelper.isEmpty(requestAddress))
			{
				//System.out.println("RequestAddress is not found.");
				System.exit(-1);
				return;
			}

			String requestID = System.getProperty("RequestID", null);
			String requestVersion = System.getProperty("RequestVersion", "1");
			String httpMethod = System.getProperty("HttpMethod", "post");
			String requestType = System.getProperty("RequestType", "form");
			String responseType = System.getProperty("ResponseType", "text");

			KGRequest aRequest = new KGRequest();
			aRequest.setRequestAddress(requestAddress);
			aRequest.setHttpMethod(KGRequest.parseHttpMethod(httpMethod));
			aRequest.setRequestType(KGRequest.parseRequestType(requestType));
			aRequest.setResponseType(KGRequest.parseResponseType(responseType));

			Map<String,Object> reqData = null;
			//Object reqData = null;
			if (aRequest.getRequestType() == KGRequest.KGRequestType.JSONType)
			{
				String requestJSON = System.getProperty("RequestJSON", null);
				if (TextHelper.isEmpty(requestJSON) == false)
				{
					org.json.JSONObject json = new org.json.JSONObject(requestJSON);
					// 요청 데이타 설정
					reqData = new HashMap<String,Object>();
					TextHelper.toJavaMap(json, reqData);
				}
			}
//				else if (aRequest.getRequestType() == KGRequest.KGRequestType.JSONArrayType)
//				{
//					String requestJSONArray = System.getProperty("RequestJSONArray", null);
//					if (TextHelper.isEmpty(requestJSONArray) == false)
//					{
//						org.json.JSONObject json = new org.json.JSONObject(requestJSONArray);
//						// 요청 데이타 설정
//						reqData = new HashMap<String,Object>();
//						TextHelper.toJavaMap(json, reqData);
//					}
//				}
			else if (aRequest.getRequestType() == KGRequest.KGRequestType.FormType)
			{
				String queryString = System.getProperty("QueryString", null);
				//System.out.println("queryString :"+ queryString);
				if (TextHelper.isEmpty(queryString) == false)
				{
					reqData = HttpHelper.normalizeQueryString(queryString);
				}
				//System.out.println("reqData :"+ reqData);
			}

			String headerString = System.getProperty("Header", null);
			Map headerMap = null;
			if (TextHelper.isEmpty(headerString) == false)
			{
				StringReader in = null;
				try
				{
					in = new StringReader(headerString);
					XProperties prop = new XProperties();
					prop.load(in);

					headerMap = new HashMap();
					headerMap.putAll(prop);
				}
				finally
				{
					try
					{
						if (in != null)
						{
							in.close();
						}
					}
					catch (Throwable e) {}
				}
			}

			Object resultValue = null;
			if (aRequest.getRequestType() == KGRequest.KGRequestType.JSONType)
			{
				DataMap<String,Object> param = new DataMap<String,Object>();
				param.put(KGRequest.KEY_REQUEST_ID, requestID);
				param.put(KGRequest.KEY_REQUEST_DATA, reqData);
				resultValue = aRequest.execute(headerMap, param);
			}
			else if (aRequest.getRequestType() == KGRequest.KGRequestType.FormType)
			{
				resultValue = aRequest.execute(headerMap, reqData);
			}
			//System.out.printf("ResultValue \n%s\n", resultValue);
		}
		catch (Throwable e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public interface ResponseListener {
		public void onResponse(HttpResponse response);
	}
}
