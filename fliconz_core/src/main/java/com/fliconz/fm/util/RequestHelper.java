package com.fliconz.fm.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
//import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.client.config.RequestConfig;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
//import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;



public class RequestHelper {
//	static Log log = LogFactory.getLog(RequestHelper.class);
	private final static Logger logger = LoggerFactory.getLogger(RequestHelper.class);
	public static final String REQUEST_SUCCESS = "000";
	public static final String REQUEST_FAIL = "001";
	public static final String SEND_FAIL = "002";

	public static JSONObject postJSON(String url,  Map<String, String> header,  Map<String, String> data) throws Exception
	{

		HttpPost method = new HttpPost(url);
		JSONObject json = new JSONObject(data);
		StringEntity requestEntity = new StringEntity(
			    json.toString(),
			    ContentType.APPLICATION_JSON);
		logger.debug("url: {}", url);
		logger.debug("request: {}", json.toString());

		method.setEntity(requestEntity);
		return send(method, header);


	}

	private static JSONObject send(HttpRequestBase method,  Map<String,String> header, ContentType contentType) throws Exception
	{
		long now= System.currentTimeMillis();
		try
		{
			int timeout = PropertyManager.getInt("requestHelper.timeout.sec");
			if(timeout>=0)
			{
				RequestConfig requestConfig = RequestConfig.custom()
						  .setSocketTimeout(timeout*1000)
						  .setConnectTimeout(timeout*1000)
						  .setConnectionRequestTimeout(timeout*1000)
						  .build();
				method.setConfig(requestConfig);
			}


			CloseableHttpClient httpclient = HttpClients.createDefault();




			if(header != null)
			{
				logger.debug("header: {}", header);
				for(String key : header.keySet())
				{
					method.addHeader(key, header.get(key));
				}
			}


			CloseableHttpResponse response = httpclient.execute(method);
			org.apache.http.HttpEntity entity = response.getEntity();
			String responseData = EntityUtils.toString(response.getEntity());
			logger.debug("response : {}", responseData);

			if(contentType == ContentType.APPLICATION_XML) {
				JSONObject result =  new JSONObject(responseData);
				EntityUtils.consume(entity);
				return result;
			} else {
				JSONObject result =  new JSONObject(responseData);
				EntityUtils.consume(entity);
				return result;
			}
		}
		catch(Exception e)
		{
			System.out.println( (new Date())+ "================= RequestHelper send error time (sec) : "  +   (System.currentTimeMillis()-now)/1000 );
			e.printStackTrace();
			throw e;
		}
	}

	private static JSONObject send(HttpRequestBase method,  Map<String,String> header) throws Exception {
		return send(method, header, ContentType.APPLICATION_JSON);
	}

	private static void addParam1(List list, String key, String[] data)
	{
		for(String v:data)
		{
			list.add(new BasicNameValuePair(key, v));
		}
	}
	private static void addParam1(List list, String key, Collection<String> data)
	{
		for(String v:data)
		{
			list.add(new BasicNameValuePair(key, v));
		}
	}
	private static void addParam(List list, String key, Object data)
	{
		if( data instanceof String[])
		{
			addParam1(list,key, (String[])data);
		}
		else if( data instanceof Collection)
		{
			addParam1(list,key, (Collection)data);
		}
		else
		{
			list.add(new BasicNameValuePair(key,  data.toString()));
		}
	}

	public static JSONObject postRequest(String url,  Map<String, String> header,  Map<String, Object> data, ContentType contentType) throws Exception
	{

		logger.debug("url: {}", url);
		HttpPost method = new HttpPost(url);
		List list = new ArrayList();
		for(String key : data.keySet())
		{
			Object v = data.get(key);
			if(v == null) continue;
			addParam(list, key, v);
		}
		UrlEncodedFormEntity requestEntity = new UrlEncodedFormEntity( list,"utf-8");
		logger.debug("request: {}", data);

		method.setEntity(requestEntity);

		return send(method, header, contentType);
	}

	public static JSONObject postRequest(String url,  Map<String, String> header,  Map<String, Object> data) throws Exception {
		return postRequest(url, header, data, ContentType.APPLICATION_JSON);
	}

	public static JSONObject sendEncodedRequest(String uri, RequestMethod method, String charset, Map<String, Object> param) {
		Iterator<String> it = param.keySet().iterator();

		String queryString = "";
		while(it.hasNext()){
			String key = it.next();
			if(param.get(key)==null) continue;
			String value = param.get(key).toString();
			String encstr = value;

			LogHelper.println("REQEUST-PARAM: " + String.format("{%s=%s}", key, value));

			try {
				encstr = URLEncoder.encode(value, TextHelper.isEmpty(charset) ? "UTF-8" : charset);
			} catch (UnsupportedEncodingException e) {
				encstr = "";
			}

			queryString += "&" + key + "=" + encstr;
		}
		return sendRequest(uri, queryString.replaceFirst("&", ""), method, charset, null);
	}

	public static JSONObject sendRequest(String uri, RequestMethod method, String charset, Map<String, Object> param) {
		Iterator<String> it = param.keySet().iterator();
		boolean encrequired = param.get("encrequired") != null ? (boolean)param.get("encrequired") : false;

		String queryString = "";
		while(it.hasNext()){
			String key = it.next();
			if(param.get(key)==null) continue;
			String value = param.get(key).toString();
			String encstr = value;

			if (encrequired)
				try {
					encstr = URLEncoder.encode(value, TextHelper.isEmpty(charset) ? "UTF-8" : charset);
				} catch (UnsupportedEncodingException e) {
					encstr = "";
				}

			queryString += "&" + key + "=" + encstr;
		}
		return sendRequest(uri, queryString.replaceFirst("&", ""), method, charset, null);
	}

	public static JSONObject sendRequest(String uri, RequestMethod method, String charset) {
		return sendRequest(uri, null, method, charset, null);
	}

	public static JSONObject sendJsonRequest(String uri, RequestMethod method, String charset, Map<String, Object> param) {
//		Iterator<String> it = param.keySet().iterator();
//		while(it.hasNext()){
//			String key = it.next();
//			if(param.get(key)==null) continue;
//			String value = param.get(key).toString();
//			String encstr = value;
//			try {
//				encstr = URLEncoder.encode(value, TextHelper.isEmpty(charset) ? "UTF-8" : charset);
//			} catch (UnsupportedEncodingException e) {
//				encstr = "";
//			}
//			param.put(key, encstr);
//		}

		JSONObject jparam = new JSONObject(param);
		return sendJsonRequest(uri, jparam.toString(), method, charset);
	}

	public static JSONObject sendJsonRequest(String uri, String jsonstr, RequestMethod method, String charset) {
		JSONObject json = null;
		try {
			LogHelper.println("----------------------------------------");
			LogHelper.println("URI: " + uri);
			LogHelper.println("METHOD: " + method);
			LogHelper.println("PARAM: " + jsonstr);
			LogHelper.println("----------------------------------------");

			URL address = new URL(uri);

			HttpURLConnection conn = (HttpURLConnection)address.openConnection();
			conn.setRequestMethod(method.toString());
			conn.setRequestProperty("Accept-Charset", TextHelper.isEmpty(charset) ? "UTF-8" : charset);
			conn.setRequestProperty("charset", TextHelper.isEmpty(charset) ? "UTF-8" : charset);
			conn.setRequestProperty("Content-Type", "application/json");

			if(jsonstr != null){
				conn.setDoOutput(true);
				DataOutputStream oswriter = new DataOutputStream(conn.getOutputStream());
				oswriter.writeBytes(jsonstr);
				oswriter.flush();
			}

			conn.connect();

			LogHelper.println("----------------------------------------");
			LogHelper.println("RES:" + conn.getResponseCode() + " " + conn.getResponseMessage());
			LogHelper.println("----------------------------------------");

			InputStream responseBodyStream = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(responseBodyStream, (charset == null || "".equals(charset) ? "utf-8" : charset)));

			StringBuffer responseBody = new StringBuffer();
			String read = null;
			while ((read = br.readLine()) != null){
				responseBody.append(new String(read.getBytes()));
			}
			br.close();

			conn.disconnect();

			LogHelper.println("----------------------------------------");
			LogHelper.println("[RESBODY]");
			LogHelper.println("----------------------------------------");
			LogHelper.println(responseBody);
			LogHelper.println("----------------------------------------");

			try {
				if(responseBody != null) {
					String resstr = responseBody.toString();
					//2018.12.11:JSONArray인 경우 추가
					if (resstr.startsWith("[")) {
						JSONArray jsons = new JSONArray(resstr);
						json = (JSONObject)jsons.get(0);
						json.put("results", jsons);
					} else
						json = new JSONObject(resstr);
				}

				if("{}".equals(responseBody.toString()) || json == null || (json != null && json.length() == 0)){
					json = makeResultJSON(json, REQUEST_SUCCESS, "NO_SERVER_RESPONSE_BODY", "요청이 성공적으로 완료되었습니다.");
				}
			} catch(Exception e){
				json = makeErrorJSON(json, REQUEST_FAIL, e.getMessage(), "요청 중 문제가 발생했습니다.");
			}
		} catch(Exception e){
			json = makeErrorJSON(json, SEND_FAIL, e.getMessage(), "요청 중 문제가 발생했습니다.");
		}

		return json;
	}

	public static JSONObject sendRequest(String uri, String querystr, RequestMethod method, String charset, ContentType contentType) {
		return sendRequest(uri, false, querystr, method, charset, contentType);
	}

	public static JSONObject sendIgnoreSSLRequest(String uri, String querystr, RequestMethod method, String charset, ContentType contentType) {
		return sendRequest(uri, true, querystr, method, charset, contentType);
	}

	public static JSONObject sendRequest(String uri, boolean ignoreSsl, String querystr, RequestMethod method, String charset, ContentType contentType) {
		JSONObject json = null;
		try {
			LogHelper.println("----------------------------------------");
			LogHelper.println("URI: " + uri);
			LogHelper.println("METHOD: " + method);
			LogHelper.println("PARAM: " + querystr);

			URL address = null;
			if (RequestMethod.GET == method) {
				address = new URL(uri + "?" + querystr);
				LogHelper.println("URL: " + uri + "?" + querystr);
			}
			else {
				address = new URL(uri);
			}

			if(uri.startsWith("https") && ignoreSsl) {
				ignoreSsl();
			}

			LogHelper.println("----------------------------------------");
			HttpURLConnection conn = (HttpURLConnection)address.openConnection();
			conn.setRequestMethod(method.toString());
			conn.setRequestProperty("Accept-Charset", TextHelper.isEmpty(charset) ? "UTF-8" : charset);
			conn.setRequestProperty("charset", TextHelper.isEmpty(charset) ? "UTF-8" : charset);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			if(RequestMethod.GET != method && querystr != null){
				conn.setDoOutput(true);
				DataOutputStream outputStreamWriter = new DataOutputStream(conn.getOutputStream());
				outputStreamWriter.writeBytes(querystr);
				outputStreamWriter.flush();
			}

			conn.connect();

			LogHelper.println("----------------------------------------");
			LogHelper.println("RES:" + conn.getResponseCode() + " " + conn.getResponseMessage());
			LogHelper.println("----------------------------------------");

			InputStream responseBodyStream = conn.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(responseBodyStream, (charset == null || "".equals(charset) ? "utf-8" : charset)));
			StringBuffer responseBody = new StringBuffer();
			String read = null;
			while ((read = br.readLine()) != null){
				responseBody.append(new String(read.getBytes()));
			}
			br.close();

			conn.disconnect();

			LogHelper.println("----------------------------------------");
			LogHelper.println("[RESBODY]");
			LogHelper.println("----------------------------------------");
			LogHelper.println(responseBody);
			LogHelper.println("----------------------------------------");

			try {
				if(responseBody != null) {
					String resstr = responseBody.toString();
					if(contentType == ContentType.APPLICATION_XML){
						json = XML.toJSONObject(resstr);
					} else {
						//2018.12.11:JSONArray인 경우 추가
						if (resstr.startsWith("[")) {
							JSONArray jsons = new JSONArray(resstr);
							json = (JSONObject)jsons.get(0);
							json.put("results", jsons);
						} else {
							json = new JSONObject(resstr);
						}
					}
				}

				if("{}".equals(responseBody.toString()) || json == null || (json != null && json.length() == 0)){
					json = makeResultJSON(json, REQUEST_SUCCESS, "NO_SERVER_RESPONSE_BODY", "요청이 성공적으로 완료되었습니다.");
				}
			} catch(Exception e){
				json = makeErrorJSON(json, REQUEST_FAIL, e.getMessage(), "요청 중 문제가 발생했습니다.");
			}
		} catch(Exception e){
			json = makeErrorJSON(json, SEND_FAIL, e.getMessage(), "요청 중 문제가 발생했습니다.");
		}

		return json;
	}

	private static JSONObject makeResultJSON(JSONObject json, String code, String type, String msg){
		if(json == null) json = new JSONObject();
		json.put("code", code);
		json.put("msg", msg);
		json.put("type", type);
		return json;
	}


	private static JSONObject makeErrorJSON(JSONObject json, String code, String type, String msg){
		if(json == null) json = new JSONObject();
		json.put("code", code);
		JSONObject error = new JSONObject();
		error.put("msg", msg);
		error.put("type", type);
		json.put("error", error);
		return json;
	}

	public static JSONObject getJSON(String url,  Map<String, String> header) throws Exception
	{
		HttpGet method = new HttpGet(url);

		return send(method, header);
	}

	private static void ignoreSsl() throws Exception{
        HostnameVerifier hv = new HostnameVerifier() {
        	public boolean verify(String urlHostName, SSLSession session){
        		return true;
        	}
        };
        trustAllHttpsCertificates();
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }

	private static void trustAllHttpsCertificates() throws Exception {
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		trustAllCerts[0] = new SSLTrustManager();
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	private static class SSLTrustManager implements TrustManager, X509TrustManager {
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public boolean isServerTrusted(X509Certificate[] certs) {
            return true;
        }

        public boolean isClientTrusted(X509Certificate[] certs) {
            return true;
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType)
                throws CertificateException {
            return;
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType)
                throws CertificateException {
            return;
        }
    }


}
