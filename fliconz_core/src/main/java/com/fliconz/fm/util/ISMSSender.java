package com.fliconz.fm.util;

import java.util.Map;

public interface ISMSSender {

	/**
	 * SMS 발송
	 * @param sender 발신번호
	 * @param receiver 수신번호 
	 * @param title 제목
	 * @param contents 내용
	 * @param param 기타 파라미터
	 * @throws Exception
	 */
	public void send(String sender, String receiver, String title, String contents, Map param ) throws Exception;
}
