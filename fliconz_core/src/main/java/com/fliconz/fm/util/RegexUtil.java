package com.fliconz.fm.util;

public class RegexUtil {

	private static final String EMAIL_REGEX = "^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@(?:\\w+\\.)+\\w{2,3}$";
	public static boolean matchEmail(String email){
		return email.matches(EMAIL_REGEX);
	}

	private static final String MOBILE_REGEX = "^((01[1|6|7|8|9])[1-9]+[0-9]{6,7})|(010[1-9][0-9]{7})$";
	public static boolean matchMobile(String email){
		return email.matches(MOBILE_REGEX);
	}
}
