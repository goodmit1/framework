package com.fliconz.fm.util;

import java.io.StringWriter;
import java.sql.Date;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.mail.javamail.JavaMailSender;

import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.security.FMSecurityContextHelper;

import com.fliconz.fw.runtime.util.PropertyManager;

public class DefaultMailSender implements IMailSender {


	protected JavaMailSender iMailSender;
	private String templatePath;
	private String domain;
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	private String fromAddress;
	public String getFromAddress() {
		return fromAddress;
	}


	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}


	public String getTemplatePath() {
		return templatePath;
	}


	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}


	public JavaMailSender getJavaMailSender() {
		return iMailSender;
	}


	public void setJavaMailSender(JavaMailSender mailSender) {
		this.iMailSender = mailSender;
	}

	@PostConstruct
	private void initVelocity()
	{



		String templatePath = PropertyManager.getString("path.tempate_base");
		if(templatePath==null){
			String filePath = this.getClass().getResource("/").getPath();
			templatePath = filePath + PropertyManager.getString("path.template_base", "");
		}

		Velocity.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, templatePath);
		Velocity.setProperty(Velocity.INPUT_ENCODING,"utf-8");
		Velocity.setProperty(Velocity.OUTPUT_ENCODING,"utf-8");
		Velocity.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.NullLogChute");
		Velocity.init();
	}
	private String getContent(String path, String lang, Map param)
	{
		VelocityContext context = new VelocityContext(param);

		if(domain != null)
		{
			param.put("domain", domain);
		}
		Template template =   Velocity.getTemplate(templatePath + "/" + path + "_" + lang + ".vm", "utf-8");
		StringWriter writer = new StringWriter();
		template.merge( context, writer );
		return writer.toString();
	}
	@Override
	public void send(String to, String title,
			String path, String lang, Map param) throws Exception {
		MimeMessage msg = iMailSender.createMimeMessage();
		msg.setSubject(title, "utf-8");
		msg.setContent(getContent(path,lang, param),  "text/html;charset=utf-8"  );
		msg.setRecipient(RecipientType.TO , new InternetAddress(to));
		if(fromAddress != null)
		{
			msg.setFrom( new InternetAddress( fromAddress ) );
		}
		else
		{
			msg.setFrom();
		}
		iMailSender.send(msg);

	}
	@Override
	public void send(String[] tos, String title, String path, String lang, Map param) throws Exception {
		MimeMessage msg = iMailSender.createMimeMessage();
		msg.setSubject(title, "utf-8");
		msg.setContent(getContent(path,lang, param),  "text/html;charset=utf-8"  );
		for(String to: tos){
			msg.addRecipient(RecipientType.TO, new InternetAddress(to));
		}
		
		
		if(fromAddress != null)
		{
			msg.setFrom( new InternetAddress( fromAddress ) );
		}
		else
		{
			msg.setFrom();
		}
		iMailSender.send(msg);

	}


}
