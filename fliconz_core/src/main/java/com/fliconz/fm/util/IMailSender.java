package com.fliconz.fm.util;

import java.util.Map;

public interface IMailSender {
	/**
	 * 메일 발송
	 * @param to 메일 주소
	 * @param title 제목
	 * @param contentsPath 템플릿
	 * @param lang 언어
	 * @param param 템플릿에서 쓸 parameter
	 * @throws Exception
	 */
	public void send( String to,  String title, String contentsPath, String lang, Map param ) throws Exception;
	
	/**
	 * 메일 발송
	 * @param tos 메일 주소
	 * @param title 제목
	 * @param contentsPath 템플릿
	 * @param lang 언어
	 * @param param 템플릿에서 쓸 parameter
	 * @throws Exception
	 */
	public void send( String[] tos,  String title, String contentsPath, String lang, Map param ) throws Exception;
}
