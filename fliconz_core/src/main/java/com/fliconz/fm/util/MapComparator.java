package com.fliconz.fm.util;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;

public class MapComparator implements Comparator<Map>{
	String field;
	boolean isDesc = false;
	public MapComparator(String field)
	{
		this.field = field;
	}
	public MapComparator(String field,boolean isDesc)
	{
		this.field = field;
		this.isDesc = isDesc;
	}

	@Override
	public int compare(Map o1, Map o2) {
		Object f1 = o1.get(field);
		Object f2 = o2.get(field);
		int result = 0;
		if(f1==null && f2==null) result = 0; 
		else if(f1==null && f2 != null)  result =  -1;
		else if(f1!=null && f2 == null) result =   1;
		else
		{
			if(f1 instanceof Integer)
			{
				result =  ((Integer)f1).compareTo((Integer)f2);
			}
			else if(f1 instanceof Long)
			{
				result =  ((Long)f1).compareTo((Long)f2);
			}
			else if(f1 instanceof BigDecimal)
			{
				result =  ((BigDecimal)f1).compareTo((BigDecimal)f2);
			}
			else if(f1 instanceof Float)
			{
				result =  ((Float)f1).compareTo((Float)f2);
			}
			else if(f1 instanceof Double)
			{
				result =  ((Double)f1).compareTo((Double)f2);
			}
			else
			{
				result =  (f1.toString()).compareTo(f2.toString());
			}
		}
		if(isDesc)
		{
			return result*-1;
		}
		else
		{
			return result;
		}
	}

}
