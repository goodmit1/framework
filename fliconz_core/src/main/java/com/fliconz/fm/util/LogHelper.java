package com.fliconz.fm.util;

import com.fliconz.fm.log.LogManager;
import com.fliconz.fw.runtime.util.PropertyManager;


public class LogHelper {

	protected static boolean quite = PropertyManager.getBoolean("loghelper.quite", false);

	public static void println(Object obj){
		LogHelper.println(obj, false);
	}

	public static void println(Object obj, boolean show){
		String msg = obj.toString();
		if(!LogHelper.quite || show) System.out.println(msg);
		else LogManager.info(msg);
	}

	public static void setQuited(boolean quite){
		LogHelper.quite = quite;
	}
}
