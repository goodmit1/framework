package com.fliconz.fm.chart;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GraphConfig {
	
	Axis x;
	Axis y;
	String zField;
	String nameField = null;
	
	List<Map> data;
	public static final String CHART_HICHART="hichart";
	public GraphConfig setX(Axis x)
	{
		this.x = x;
		return this;
	}
	public GraphConfig setY(Axis y)
	{
		this.y = y;
		return this;
	}
	
	public Map splitByGroupName()
	{
		Map<Object, List> result = new LinkedHashMap();
		
		for(Map m : data)
		{
			split(result, m.get(nameField==null?"name":nameField), m);
		}
		return result;
	}
	private void split(Map<Object, List> result, Object key, Map data)
	{
		List list = result.get(key);
		if(list == null)
		{
			list = new ArrayList();
			result.put(key, list);
		}
		list.add(data);
	}
	public GraphConfig setNameField(String groupNameField)
	{
		this.nameField = groupNameField;
		return this;
	}
	public GraphConfig setZField(String zField)
	{
		this.zField = zField;
		return this;
	}
	public GraphConfig setData(List<Map> data)
	{
		this.data = data;
		return this;
	}
	public Map gen(String chartKind, boolean isGroupByName, boolean isArray)
	{
		IGraphConfigGen gen = null;
		if(CHART_HICHART.equals(chartKind))
		{
			gen = new  HichartConfigGen();
		}
		return gen.toConfigMap(this,isGroupByName, isArray);
	}
}
