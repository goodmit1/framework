package com.fliconz.fm.chart;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HichartConfigGen implements IGraphConfigGen{

	private Map getAxisInfo(Axis axis)
	{
		Map result = new HashMap();
		
		if(axis.isDate)
		{
			result.put("type", "datetime");
			
		}
		else
		{
			result.put("min", axis.min);
		}
		String[] category = axis.getCategoryTitles();
		if(category != null)
		{
			result.put("categories", category);
		}
		return result;
	}
	
	@Override
	public Map toConfigMap(GraphConfig graphData, boolean isGroupByName, boolean isArray) {
		Map result = new HashMap();
		boolean isok = setXCategory(graphData);
		if(isok)
		{
			result.put("category_map", graphData.x.getCategoryTitleMap());
		}
		if(graphData.x != null) result.put("xAxis", getAxisInfo(graphData.x));
		if(graphData.y != null) result.put("yAxis", getAxisInfo(graphData.y));
		result.put("series", getSeries(graphData,isGroupByName, isArray));
		return result;
	}

	boolean setXCategory(GraphConfig graphData)
	{
		if(graphData.x != null && graphData.x.categoryFieldName != null)
		{
			Map categoryMap = new HashMap();
			for(Map m : graphData.data)
			{
				categoryMap.put(m.get(graphData.x.fieldName), m.get(graphData.x.categoryFieldName));
			}
			graphData.x.setCategory(categoryMap);
			return true;
		}
		return false;
	}
	List getSeries(GraphConfig graphData,boolean isGroupByName, boolean isArray)
	{
		List series = new ArrayList();
		if(isGroupByName)
		{
			Map<Object, List<Map>> splitMap = graphData.splitByGroupName();
			for(Object name: splitMap.keySet())
			{
				Map series1 = new HashMap();
				series1.put("name", name);
				if(!isArray)
				{
					series1.put("data",convertField(graphData, splitMap.get(name)));
				}
				else
				{
					series1.put("data",convertFieldArr(graphData, splitMap.get(name),false));
				}
				series.add(series1);
			}
		}
		else
		{
			Map series1 = new HashMap();
			
			if(!isArray)
			{
				series1.put("data",convertField(graphData, graphData.data));
			}
			else
			{
				series1.put("data",convertFieldArr(graphData, graphData.data, true));
			}
			series.add(series1);
			
		}
		return series;
	}
	List convertField(GraphConfig graphData, List<Map> list) {
		
		for(Map m : list)
		{
			convertField(m , "name", graphData.nameField);
			if(graphData.x != null)
			{
				if(graphData.x.categoryFieldName != null)
				{
					m.put("x", graphData.x.getCategoryIndex(m.get(graphData.x.fieldName)));
				}
				else
				{	
					convertField(m ,  graphData.x, "x");
				}
			}
			if(graphData.y != null) convertField(m ,  graphData.y, "y");
			convertField(m , "z", graphData.zField);
		}
		if(graphData.x != null && graphData.x.categoryFieldName != null)
		{
			Collections.sort(list, new Comparator<Map>(){

				@Override
				public int compare(Map o1, Map o2) {
					int x1 = (Integer)o1.get("x");
					int x2 = (Integer)o2.get("x");
					return x1-x2;
				}
				
			});
		}
		return list;
		
	}
	List convertFieldArr(GraphConfig graphData, List<Map> list, boolean includeNameField) {
		
		List result = new ArrayList();
		String[] categories = graphData.x.getCategoryTitles();
		if(categories == null)
		{
			for(Map m : list)
			{
				List data1 = new ArrayList();
				if(includeNameField && graphData.nameField != null)
				{
					data1.add( m.get(graphData.nameField));
				}
				if(graphData.x != null)
				{
					data1.add(getVal(graphData.x, m.get(graphData.x.fieldName)));
				}
				if(graphData.y != null)
				{
					data1.add(getVal(graphData.y, m.get(graphData.y.fieldName)));
				}
				if(graphData.zField != null)
				{
					data1.add(m.get(graphData.zField));
				}
					
				result.add(data1);
			}
		}
		else
		{
			Object[] data1 = new Object[categories.length];
			for(Map m : list)
			{
				
				Object y = getVal(graphData.y, m.get(graphData.y.fieldName));
				Object x = getVal(graphData.x, m.get(graphData.x.fieldName));
				data1[graphData.x.getCategoryIndex(x)] = y;
			}
			for(Object o : data1)
			{
				if(o==null)
				{
					result.add(0);
				}
				else
				{
					result.add(o);
				}
			}
		}
		return result;
		
	}
	public static Object toDate(String str, String pattern) throws Exception
	{
		SimpleDateFormat transFormat = new SimpleDateFormat(pattern);
		Date date =  transFormat.parse(str);
		return date.getTime();
	}
	Object getVal(Axis axis, Object val)
	{
		if(!axis.isDate)
		{
			return val;
		}
		else
		{
			
			try {
				if(val instanceof String)
				{
					return toDate((String)val, axis.dateFormat);
				}
				else if(val instanceof Date)
				{
					return ((Date)val).getTime();
				}
				else
				{
					return val;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return val;
			}
		
		}
	}
	void convertField(Map data, String tobeField, String orgField)
	{
		if(tobeField != null)
			data.put(tobeField, data.get(orgField));
	}
	void convertField(Map data, Axis fromAxis, String toField)
	{
		if(fromAxis != null)
			data.put(toField, getVal(fromAxis, data.get(fromAxis.fieldName)));
	}
}
