package com.fliconz.fm.chart;

import java.util.HashMap;
import java.util.Map;

public class Axis {
	//String title;
	boolean isDate = false;
	float min = 0;
	String fieldName;
	String dateFormat="yyyyMMdd";
	Map<Object, Integer> categoryIdxMap;
	String[] categoryTitle;
	String categoryFieldName;
	
	public Axis(String fieldName)
	{
		this.fieldName = fieldName;
	}
	public Axis(String fieldName, String categoryFieldName)
	{
		this.fieldName = fieldName;
		this.categoryFieldName =  categoryFieldName;
	}
	public Map getCategoryTitleMap()
	{
		Map result = new HashMap();
		for(Object key : categoryIdxMap.keySet())
		{
			String title = categoryTitle[categoryIdxMap.get(key)];
			result.put(title, key);
		};
		return result;
	}
	public void setCategory(Map<Object, String> category)
	{
		categoryTitle = new String[category.size()];
		categoryIdxMap = new HashMap();
		int idx = 0;
		for(Object key : category.keySet())
		{
			categoryIdxMap.put(key, idx);
			categoryTitle[idx++] = category.get(key);
		}
		
	}
	public int getCategoryIndex(Object key)
	{
		return categoryIdxMap.get(key);
	}
	public String[] getCategoryTitles()
	{
		return categoryTitle;
	}
	public Axis(String fieldName, float min)
	{
		this.fieldName = fieldName;
		this.min = min;
	}
	public Axis(String fieldName, boolean isDate, String dateFormat)
	{
		this.fieldName = fieldName;
		this.isDate = isDate;
		this.dateFormat = dateFormat;
		
	}
}
