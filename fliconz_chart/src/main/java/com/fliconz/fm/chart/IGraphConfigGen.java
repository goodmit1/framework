package com.fliconz.fm.chart;

import java.util.Map;

public interface IGraphConfigGen {

	Map toConfigMap(GraphConfig graphData, boolean isGroupByName, boolean isArray);
}
