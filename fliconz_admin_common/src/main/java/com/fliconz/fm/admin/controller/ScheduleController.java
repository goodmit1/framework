package com.fliconz.fm.admin.controller;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fliconz.fm.admin.schedule.ScheduleRunner;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
 

@RestController
@RequestMapping(value="schedule")
public class ScheduleController {

	@RequestMapping("/cancel")
	public String cancelAll( HttpServletRequest request) throws Exception
	{
		//UserDetail.getCurrentInstance(request);
		ScheduleRunner runner = (ScheduleRunner)SpringBeanUtil.getBean("scheduleRunner");
		runner.cancelAll();
		return "1";
		 
	}
	@RequestMapping("/start")
	public String start(HttpServletRequest request ) throws Exception
	{
		//UserDetail.getCurrentInstance(request);
		ScheduleRunner runner = (ScheduleRunner)SpringBeanUtil.getBean("scheduleRunner");
		runner.loadForce();
		return "1";
		 
	}
	@RequestMapping("/loadOne")
	public String loadOne(@RequestParam(value="id", defaultValue="") String id,HttpServletRequest request ) throws Exception
	{
		//UserDetail.getCurrentInstance(request);
		ScheduleRunner runner = (ScheduleRunner)SpringBeanUtil.getBean("scheduleRunner");
		runner.loadOne(id);
		return "1";
		 
	}
	@RequestMapping("/cancelOne")
	public String cancelOne(@RequestParam(value="id", defaultValue="") String id,HttpServletRequest request ) throws Exception
	{
		//UserDetail.getCurrentInstance(request);
		ScheduleRunner runner = (ScheduleRunner)SpringBeanUtil.getBean("scheduleRunner");
		runner.cancelOne(id);
		return "1";
		 
	}
	
}
