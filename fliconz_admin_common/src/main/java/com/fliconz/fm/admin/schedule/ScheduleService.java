package com.fliconz.fm.admin.schedule;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.common.BaseService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class ScheduleService extends BaseService{

	public final static String  NS = "com.fliconz.fm.admin.batch.Batch";
	@Override
	protected    String getNameSpace() { 
		return NS; 
	}


	public void cancelOnStart() throws Exception
	{
		this.updateByQueryKey("update_FM_BATCH_EXE_RESULT_cancel", new HashMap());
	}
	public void logScheduleStart(String scheduleId, Date startDt) throws Exception
	{
		Map param = new HashMap();
		param.put("BATCH_PRGM_ID", scheduleId);
		param.put("EXE_TMS", startDt);
		insertDBTable("FM_BATCH_EXE_RESULT", param);
		this.updateByQueryKey("update_FM_BATCH_PRGM_LAST_EXE_TMS", param);
	}
	public void logScheduleSuccess(String scheduleId, Date startDt, int rowInserted)
	{
		Map param = new HashMap();
		param.put("BATCH_PRGM_ID", scheduleId);
		param.put("EXE_TMS", startDt);
		param.put("EXE_FINISH_TMS", new Date());
		param.put("BATCH_EXE_RESULT_CD", "S");
		param.put("RESULT_MSG", rowInserted);
		try {
			updateDBTable("FM_BATCH_EXE_RESULT", param);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public void onAfterInsert(Map param) throws Exception
	{
		ScheduleRunner runner = (ScheduleRunner) SpringBeanUtil.getBean("scheduleRunner");
		if(runner != null)
		{
			runner.loadOneIncludeRemote(param.get("BATCH_PRGM_ID"));
		}
	}


	public void onAfterUpdate(Map param) throws Exception{
		ScheduleRunner runner = (ScheduleRunner) SpringBeanUtil.getBean("scheduleRunner");
		if(runner != null  )
		{
			if("Y".equals(param.get("USE_YN")))
			{
				runner.loadOneIncludeRemote(param.get("BATCH_PRGM_ID"));
			}
			else
			{
				runner.cancelOneIncludeRemote(param.get("BATCH_PRGM_ID"));
			}
		}
	}

	public void onAfterDelete(Map param) throws Exception
	{
		ScheduleRunner runner = (ScheduleRunner) SpringBeanUtil.getBean("scheduleRunner");
		if(runner != null)
		{
			runner.cancelOneIncludeRemote(param.get("BATCH_PRGM_ID"));
		}
	}


	public void logScheduleFail(String scheduleId, String classNm, String scheduleNm, Date startDt, String errMsg)
	{
		Map param = new HashMap();
		param.put("BATCH_PRGM_ID", scheduleId);
		param.put("BATCH_PRGM_NM", scheduleNm);
		param.put("CLASS_NM", classNm);
		param.put("EXE_TMS", startDt);
		param.put("EXE_FINISH_TMS", new Date());
		param.put("BATCH_EXE_RESULT_CD", "F");
		param.put("RESULT_MSG", errMsg != null ? (errMsg.length()>500 ? errMsg.substring(0, 100): errMsg) : "");
		try {
			updateDBTable("FM_BATCH_EXE_RESULT", param);
			//System.out.println(afterFailQuery);
			if(afterFailQuery != null && !"".equals(afterFailQuery))
			{	
				this.getDAO().update(afterFailQuery, param);
			}	
			//TODO SMS 보내기 CALL SBPD.CE_SMS_BATCH_SEND('1480406606995','SBLO.CY_CodecoMSKKICD'); 

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	String afterFailQuery;
	public String getCurrentRunServer()  
	{
		try
		{
			Map map = selectOneByQueryKey("get_run_server",  new HashMap());
			if(map != null && map.size()>0)
			{
				afterFailQuery = (String)map.get("BATCH_FAIL");
				return (String)map.get("BATCH_SVR");
			}
		}
		catch(Exception ignore)
		{
			ignore.printStackTrace();
		}
		return null;
	}
}
