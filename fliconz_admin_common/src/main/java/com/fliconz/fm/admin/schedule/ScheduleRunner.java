package com.fliconz.fm.admin.schedule;


import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import com.fliconz.fm.common.scheduler.IScheduleJob;
import com.fliconz.fm.util.RequestHelper;
import com.fliconz.fw.runtime.util.SpringBeanUtil;


public class ScheduleRunner {
	@Autowired
	private TaskScheduler scheduler;

	private String runServer;

	public String getRunServer() {
		return runServer;
	}

	public void setRunServer(String runServer) {
		this.runServer = runServer;
	}

	@Autowired
	ScheduleService service;
	Map<String, ScheduledFuture> jobMap;

	public String getServerName()
	{
		return System.getProperty("ServerName");
	}
	@PostConstruct
	public void load() throws Exception {
		String runServer1 = service.getCurrentRunServer();
		if(runServer1 != null)
		{
			runServer = runServer1;
		}
		System.out.println("ScheduleRunner load...(PostConstruct) : " + runServer);
		if(!this.isScheduleServer())
		{
			System.out.println("This is not schedule server");
			return;
		}
		service.cancelOnStart();
		loadForce();

	}
	public boolean isScheduleServer()
	{
		if(runServer != null && !runServer.equals(getServerName()))
		{
			return false;
		}
		return true;
	}
	public void stopSchedule() throws Exception
	{
		cancelAll();

	}

	public void loadForce() throws Exception
	{
		cancelAll();
		List<Map> list = service.selectListByQueryKey("list_run_target", new HashMap());
		jobMap = new HashMap();
		for (Map m : list) {
			try
			{
				Runnable task = getTaskRunner(m);
				CronTrigger trigger = new CronTrigger((String) m.get("CRON"));
				ScheduledFuture job = scheduler.schedule(task, trigger);
				System.out.print(m.get("CRON") + " ==> " + m.get("CLASS_NM"));
				jobMap.put( m.get("BATCH_PRGM_ID").toString(), job);
			}
			catch(Exception ignore)
			{
				ignore.printStackTrace();
			}

		}
	}

	public void cancelAllIncludeRemote() throws Exception
	{
		if(this.isScheduleServer())
		{
			cancelAll();
		}
		else
		{
			requestRemote("cancel",runServer, null);
		}
	}
	public void loadOneIncludeRemote(Object id) throws Exception
	{
		if(this.isScheduleServer())
		{
			loadOne(id);
		}
		else
		{
			requestRemote("loadOne",runServer, id);
		}
	}
	public void cancelOneIncludeRemote(Object id) throws Exception
	{
		if(this.isScheduleServer())
		{
			cancelOne(id);
		}
		else
		{
			requestRemote("cancelOne",runServer, id);
		}
	}
	public void loadAllIncludeRemote(String newServer) throws Exception
	{
		if(newServer.equals( getServerName()))
		{
			loadForce();
		}
		else
		{
			requestRemote("start",newServer, null);
		}
	}
	private void requestRemote(String method, String runServer, Object id) throws Exception
	{
		String url = "http://" + runServer + "/schedule/" + method + ".do";
		if(id != null)
		{
			url += "?id=" + id;
		}
		RequestHelper.getJSON( url, null);
	}
	public void loadOne(Object id) throws Exception
	{
		cancelOne(id);

		Map param = new HashMap();
		param.put("BATCH_PRGM_ID", id);
		Map m = service.selectOneByQueryKey("list_run_target", param);
		if(m != null && m.size()>0)
		{
			Runnable task = getTaskRunner(m);
			ScheduledFuture job = scheduler.schedule(task, new CronTrigger((String) m.get("CRON")));
			jobMap.put(m.get("BATCH_PRGM_ID").toString(), job);
		}

	}
	public void cancelOne(Object id) throws Exception
	{

		ScheduledFuture job = jobMap.get(id);
		if(job != null)
		{
			job.cancel(true);
			if(job.isCancelled())
			{
				System.out.println("schedule " + id + " canceled");
			}
			else
			{
				throw new Exception("cancel fail");
			}
		}
	}
	protected TaskRunner getTaskRunner(Map m) throws Exception {
		return new TaskRunner(m.get("BATCH_PRGM_ID").toString(), (String) m.get("CLASS_NM"), (String) m.get("BATCH_PRGM_NM"));
	}

	public void runOne(Object scheduleId) throws Exception {
		Map param = new HashMap();
		param.put("BATCH_PRGM_ID", scheduleId);
		Map info = service.selectInfo("SEW_BATCH_PRGM", param);

		if (info != null) {
			Thread task = getTaskRunner(info);
			task.start();
		}
	}

	public void reload() throws Exception {

		load();
	}
	public ScheduledFuture getJob(Object id)
	{
		return jobMap.get(id);
	}
	public Map getJobAll()
	{
		return jobMap;
	}
	public void cancelAll() throws Exception {
		if(jobMap == null) return;
		Collection<ScheduledFuture> jobs = jobMap.values();
		for (ScheduledFuture job : jobs) {
			job.cancel(true);
		}
		//jobMap.clear();

	}

	public int exec(String scheduleId, String className, Date startDt) throws Exception
	{
		IScheduleJob job;
		try
		{
			job = (IScheduleJob) Class.forName(className).newInstance();
		}
		catch(ClassNotFoundException e)
		{
			job = (IScheduleJob) SpringBeanUtil.getBean(className);
		}
		return job.doRun(scheduleId, startDt);
	}
	public class TaskRunner extends Thread {

		String scheduleId;
		String scheduleNm;
		String className;

		public TaskRunner(String scheduleId, String className , String scheduleNm) throws Exception {
			this.className = className;
			this.scheduleId = scheduleId;
			this.scheduleNm = scheduleNm;
		}

		@Override
		public void run() {
			if(isInterrupted()) return;
			Date startDt = new Date();
			System.out.println(new Date() + "==>" + className + " 배치 시작");
			try {
				service.logScheduleStart(scheduleId, startDt);
				
				int result = exec(scheduleId, className, startDt);
				service.logScheduleSuccess(scheduleId, startDt, result);
			} catch (Exception e) {
				e.printStackTrace();
				service.logScheduleFail(scheduleId, className, scheduleNm, startDt, e.getMessage());
			}

		}


	}
}
